import {
    HttpEvent,
    HttpEventType,
    HttpProgressEvent,
} from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatPaginator, MatTableDataSource } from '@angular/material';
import { Router } from '@angular/router';
import { DeleteConfirmDialogComponent } from '@app/_shared/dialogs/delete-confirm-dialog/delete-confirm-dialog.component';
import { CommonService } from '@core/services/common.service';
import { DataService } from '@core/services/data-service';
import { ToastrService } from '@core/services/toastr.service';
import { UserService } from '@core/services/user.service';
import { forkJoin, Subscription } from 'rxjs';

export const DATA_EXAMPLE = [
    {
        ngayTao: '20/10/2020',
        tenBienBan: 'Biên bản tịnh kho tháng 10',
        trangThai: 'ENABLE',
    },
    {
        ngayTao: '20/10/2020',
        tenBienBan: 'Biên bản tịnh kho tháng 10',
        trangThai: 'ENABLE',
    },
];

@Component({
    selector: 'app-pure-store-management',
    templateUrl: './pure-store-management.component.html',
    styleUrls: ['./pure-store-management.component.scss'],
})
export class PureStoreManagementComponent implements OnInit {
    dauMoiBepId: number;
    fieldSearch: any[] = [
        {
            type: 'DATETIME',
            referenceValue: 'ngayTao',
            name: 'Ngày tạo',
            defaultValue: '',
            required: '0',
            css: 'col-12 col-lg-6',
            appearance: 'legacy',
        },
    ];

    displayedColumnsOfField = [
        'ngayTao',
        'tenBienBan',
        'nguoiLapBienBan',
        'actions',
    ];
    userList: any[] = [];

    dataSource = new MatTableDataSource<any>([]);
    @ViewChild(MatPaginator) paginator!: MatPaginator;

    allowExec: boolean = false;
    subscription: Subscription;
    isLoading: boolean = false;

    constructor(
        private _router: Router,
        public dialog: MatDialog,
        private commonService: CommonService,
        private _userService: UserService,
        private _dataService: DataService,
    ) {
        let request = {
            id: localStorage.getItem('idMenu'),
            language: 'vi',
        };

        this.commonService.checkPermission(request).subscribe((response) => {
            if (JSON.parse(response.result).some((obj) => obj.key === 'EXEC')) {
                this.allowExec = true;
            }
        });

        this.subscription = this._dataService.getData().subscribe((message) => {
            this.isLoading = true;
            this.getInitData(message.id);
        });
    }

    ngOnInit() {
        this.dauMoiBepId = this._dataService.getDauMoiBepId();
        this.getInitData(this.dauMoiBepId);
    }

    getInitData(dauMoiBepId: number) {
        this.dauMoiBepId = dauMoiBepId;

        this.commonService
            .callDataAPIShort('/api/services/read/BienBanTinhKho/GetAll', {
                maxResultCount: 99999999,
                dauMoiBepId: dauMoiBepId,
            })
            .subscribe((response) => {
                this.dataSource = new MatTableDataSource<any>(
                    response.result.items,
                );
                this.dataSource.paginator = this.paginator;

                setTimeout(() => {
                    this.isLoading = false;
                }, 200);
            });

        this._userService.getUsers({}).subscribe((response) => {
            this.userList = response.result;
        });
    }

    getNameUserAllowId(userId: number) {
        let fullName;
        if (this.userList.length > 0) {
            const dataFound = this.userList.find((user) => user.id === userId);

            fullName = dataFound
                ? `${dataFound.surname} ${dataFound.name}`
                : '';
        }
        return fullName;
    }

    handleInputFilter(dataSearch: any) {
        const searchValue = dataSearch.trim().toLowerCase();

        if (!searchValue) {
            this.getInitData(this.dauMoiBepId);
        }
        this.commonService
            .callDataAPIShort('/api/services/read/BienBanTinhKho/GetAll', {
                maxResultCount: 99999999,
                criterias: [
                    {
                        propertyName: 'tenBienBan',
                        operation: 6,
                        value: dataSearch
                    },
                ],
                dauMoiBepId: this.dauMoiBepId,
            })
            .subscribe((response) => {
                this.dataSource.data = response.result.items;
            })
    }

    handleSearchAdvanced(dataAdvanced: any) {
        if (dataAdvanced.key === 'reset') {
            this.getInitData(this.dauMoiBepId);
        } else {
            let bodySubmit = {
                maxResultCount: 99999999,
                criterias: [
                    {
                        propertyName: dataAdvanced.ngayTao ? 'ngayTao' : '',
                        operation: dataAdvanced.ngayTao ? 'Equals' : 'Contains',
                        value: dataAdvanced.ngayTao
                            ? new Date(dataAdvanced.ngayTao._d).toISOString()
                            : '',
                    },
                ],
                dauMoiBepId: this._dataService.getDauMoiBepId(),
            };

            this.commonService
                .callDataAPIShort(
                    '/api/services/read/BienBanTinhKho/GetAll',
                    bodySubmit,
                )
                .subscribe((response) => {
                    this.dataSource = new MatTableDataSource<any>(
                        response.result.items,
                    );
                    this.dataSource.paginator = this.paginator;
                });
        }
    }

    action(element: any) {
        if (element) {
            this._router.navigate([
                `manage/app/category/bien-ban-tinh-kho/detail/${element.id}`,
            ]);
        } else {
            this._router.navigateByUrl(
                'manage/app/category/bien-ban-tinh-kho/add',
            );
        }
    }
}
