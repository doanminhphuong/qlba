import { SelectionModel } from '@angular/cdk/collections';
import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import {
    MatDialog,
    MatPaginator,
    MatTableDataSource,
    PageEvent,
} from '@angular/material';
import { DynamicDialogComponent } from '@app/components/dynamic-dialog-component/dynamic-dialog-component.component';
import { DeleteConfirmDialogComponent } from '@app/_shared/dialogs/delete-confirm-dialog/delete-confirm-dialog.component';
import { CategoryService } from '@core/services/category.service';
import { CommonService } from '@core/services/common.service';
import { FoodFuelService } from '@core/services/food-fuel.service';
import { ToastrService } from '@core/services/toastr.service';
import { Subject, Subscription } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';

@Component({
    selector: 'app-food-fuel-group',
    templateUrl: './food-fuel-group.component.html',
    styleUrls: ['./food-fuel-group.component.scss'],
})
export class FoodFuelGroupComponent implements OnInit, AfterViewInit {
    @ViewChild(MatPaginator) paginator!: MatPaginator;
    public applyFilter = new Subject<KeyboardEvent>();
    private subscription: Subscription;

    unitOptions: any[] = [];
    groupOptions: any[] = [];
    typeOptions: any[] = [];
    fieldSearch: any[] = [];

    displayedColumnsOfField: string[] = [
        'select',
        'maNhomLttpChatDot',
        'tenNhomLttpChatDot',
        'phanLoaiLttpChatDot',
        'status',
        'actions',
    ];

    dataSource = new MatTableDataSource<any>([]);
    selection = new SelectionModel<any>(true, []);

    // Pagination
    totalRows = 0;
    pageSize = 10;
    currentPage = 0;
    pageSizeOptions: number[] = [5, 10, 25, 50];
    dataSkipped: number = 0;

    isFiltering: boolean = false;

    allowExec: boolean = false;

    constructor(
        public dialog: MatDialog,
        private _toastrService: ToastrService,
        private _categoryService: CategoryService,
        private _foodFuelService: FoodFuelService,
        private _commonService: CommonService,
    ) {
        this.subscription = this.applyFilter
            .pipe(
                map((event) => (event.target as HTMLInputElement).value),
                debounceTime(500),
                distinctUntilChanged(),
            )
            .subscribe((filterValue) => this.handleInputFilter(filterValue));
    }

    ngOnInit() {
        let request = {
            id: localStorage.getItem('idMenu'),
            language: 'vi',
        };

        this._commonService.checkPermission(request).subscribe((response) => {
            if (JSON.parse(response.result).some((obj) => obj.key === 'EXEC')) {
                this.allowExec = true;
            } else {
                this.displayedColumnsOfField.splice(5, 1);
                this.displayedColumnsOfField.splice(0, 1);
            }

            this.getInitData();
        });

        this.getMultiCategories();
    }

    ngAfterViewInit() {
        this.dataSource.paginator = this.paginator;
    }

    getInitData(): void {
        const body = {
            maxResultCount: this.pageSize,
            skipCount: this.dataSkipped,
            sorting: 'Id',
        };

        this._foodFuelService.getAllGroupOfFoodFuel(body).subscribe((res) => {
            this.dataSource.data = res.result.items;
            setTimeout(() => {
                this.paginator.pageIndex = this.currentPage;
                this.paginator.length = res.result.totalCount;
            });

            this.selection.clear();
        });
    }

    pageChanged(event: PageEvent) {
        if (this.isFiltering) return;

        this.currentPage = event.pageIndex;
        this.dataSkipped = event.pageIndex * 10;

        this.getInitData();
    }

    handleInputFilter(filterValue) {
        if (!filterValue) {
            this.isFiltering = false;
            this.getInitData();
        } else {
            this.isFiltering = true;
            const bodyFilter = {
                maxResultCount: 9999,
                skipCount: 0,
                sorting: 'Id',
                criterias: [
                    {
                        propertyName: 'tenNhomLttpChatDot',
                        operation: 6,
                        value: filterValue.trim().toLowerCase(),
                    },
                ],
            };

            this._foodFuelService
                .getAllGroupOfFoodFuel(bodyFilter)
                .subscribe((res) => {
                    this.dataSource.data = res.result.items;
                });
        }
    }

    getMultiCategories() {
        this.getUnitOptions();
        this.getGroupOptions();
        this.getTypeOptions();
    }

    getBodyCategory(groupCode: string) {
        return {
            maxResultCount: 10,
            skipCount: 0,
            sorting: 'Code',
            criterias: [
                {
                    propertyName: 'GroupCode',
                    operation: 6,
                    value: groupCode,
                },
            ],
        };
    }

    getUnitOptions() {
        this._categoryService
            .getAllCategory(this.getBodyCategory('don-vi-tinh'))
            .subscribe((res) => {
                this.unitOptions = res.result.map((item) => ({
                    key: item.codeData,
                    name: item.name,
                }));
            });
    }

    getGroupOptions() {
        this._categoryService
            .getAllCategory(this.getBodyCategory('nhom-lttp-chat-dot'))
            .subscribe((res) => {
                this.groupOptions = res.result.map((item) => ({
                    key: item.codeData,
                    name: item.name,
                }));
            });
    }

    getTypeOptions() {
        this._categoryService
            .getAllCategory(this.getBodyCategory('phan-loai-lttp-chat-dot'))
            .subscribe((res) => {
                this.typeOptions = res.result.map((item) => ({
                    key: item.codeData,
                    name: item.name,
                }));
            });
    }

    getObjectElementUser(key, type) {
        let data;
        let dataList = [];

        if (
            this.unitOptions.length > 0 &&
            this.groupOptions.length > 0 &&
            this.typeOptions.length > 0
        ) {
            switch (type) {
                case 'UNIT':
                    dataList = this.unitOptions;
                    break;
                case 'GROUP':
                    dataList = this.groupOptions;
                    break;
                case 'TYPE':
                    dataList = this.typeOptions;
                    break;
            }
            let item = dataList.find((item) => item.key === key);
            data = item && item.name ? item.name : '';
        }

        return data;
    }

    /** Whether the number of selected elements matches the total number of rows. */
    isAllSelected() {
        const numSelected = this.selection.selected.length;
        const numRows = this.dataSource.data.length;
        return numSelected === numRows;
    }

    /** Selects all rows if they are not all selected; otherwise clear selection. */
    toggleAllRows() {
        if (this.isAllSelected()) {
            this.selection.clear();
            return;
        }

        this.selection.select(...this.dataSource.data);
    }

    /** The label for the checkbox on the passed row */
    checkboxLabel(row?: any): string {
        if (!row) {
            return `${this.isAllSelected() ? 'deselect' : 'select'} all`;
        }
        return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${
            row.index + 1
        }`;
    }

    guid = () => {
        let s4 = () => {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        };
        //return id of format 'aaaaaaaa'-'aaaa'-'aaaa'-'aaaa'-'aaaaaaaaaaaa'
        return (
            s4() +
            s4() +
            '-' +
            s4() +
            '-' +
            s4() +
            '-' +
            s4() +
            '-' +
            s4() +
            s4() +
            s4()
        );
    };

    openDialog(dataDialog: any): void {
        let fields = [
            {
                type: 'TEXT',
                referenceValue: 'maNhomLttpChatDot',
                name: 'Mã nhóm TP, CĐ',
                defaultValue: '',
                required: '1',
                pattern: '^[a-zA-Z0-9-]*$',
                icon: 'restaurant',
                css: 'col-12 col-lg-6',
            },
            {
                type: 'TEXT',
                referenceValue: 'tenNhomLttpChatDot',
                name: 'Tên nhóm TP, CĐ',
                defaultValue: '',
                required: '1',
                icon: 'restaurant',
                css: 'col-12 col-lg-6',
            },
            {
                type: 'SELECT',
                referenceValue: 'codeData',
                name: 'Phân loại LTTP, chất đốt',
                defaultValue: '',
                required: '1',
                options: [...this.typeOptions],
                css: 'col-12 col-lg-6',
            },
            {
                type: 'RADIO',
                referenceValue: 'status',
                name: 'Trạng thái',
                defaultValue: 'ENABLE',
                required: '0',
                options: [
                    { key: 'ENABLE', name: 'Hoạt động' },
                    { key: 'DISABLE', name: 'Tạm ngưng' },
                ],
                css: 'col-12 col-lg-6',
            },
            {
                type: 'TEXTAREA',
                referenceValue: 'value1',
                name: 'Mô tả',
                defaultValue: '',
                required: '0',
                icon: 'restaurant',
                css: 'col-12 col-lg-12',
            },
        ];

        let dialogRef = this.dialog.open(DynamicDialogComponent, {
            // disableClose: true,
            width: '70%',
            data: {
                datas: dataDialog,
                fields: fields,
                typeDialog: 'lttp',
            },
            panelClass: 'my-custom-dialog-class',
        });

        dialogRef.componentInstance.onSave.subscribe((formValues) => {
            const isSubmitted = !!formValues;

            let dataSubmit = {
                ...dataDialog,
                ...formValues,
            };

            if (!dataDialog) {
                this._foodFuelService
                    .createGroupOfFoodFuel(dataSubmit)
                    .subscribe(
                        (res) => dialogRef.close(isSubmitted),
                        (err) => this._toastrService.errorServer(err),
                    );
            } else {
                this._foodFuelService
                    .updateGroupOfFoodFuel(dataSubmit)
                    .subscribe(
                        (res) => dialogRef.close(isSubmitted),
                        (err) => this._toastrService.errorServer(err),
                    );
            }
        });

        dialogRef.componentInstance.handleDelete.subscribe((dataDelete) => {
            const isDeleted = !!dataDelete;
        });

        dialogRef.afterClosed().subscribe((result) => {
            if (!result) return;

            this.getInitData();

            if (!dataDialog) {
                this._toastrService.success('', 'Thêm thành công');
            } else {
                this._toastrService.success('', 'Cập nhật thành công');
            }
        });
    }

    createAction(): void {
        this.openDialog(null);
    }

    editAction(element: any): void {
        this.openDialog(element);
    }

    deleteAction(element: any): void {
        const dialogRef = this.dialog.open(DeleteConfirmDialogComponent, {
            width: '500px',
            data: element,
        });

        dialogRef.componentInstance.handleDelete.subscribe((dataDelete) => {
            const isSubmitted = !!dataDelete;

            this._foodFuelService.deleteGroupOfFoodFuel(dataDelete).subscribe(
                (res) => dialogRef.close(isSubmitted),
                (err) => this._toastrService.errorServer(err),
            );
        });

        dialogRef.afterClosed().subscribe((result) => {
            if (!result) return;

            this.getInitData();
            this._toastrService.success('', 'Xóa thành công');
        });
    }

    deleteMultiAction(): void {
        const dialogRef = this.dialog.open(DeleteConfirmDialogComponent, {
            width: '500px',
        });

        dialogRef.componentInstance.handleDelete.subscribe((dataDelete) => {
            const isSubmitted = !dataDelete;

            this.selection.selected.forEach((item) => {
                const dataDelete = {
                    id: item.id,
                };

                this._foodFuelService
                    .deleteGroupOfFoodFuel(dataDelete)
                    .subscribe(
                        (res) => dialogRef.close(isSubmitted),
                        (err) => this._toastrService.errorServer(err),
                    );
            });
        });

        dialogRef.afterClosed().subscribe((result) => {
            if (!result) return;

            this.getInitData();
            this._toastrService.success('', 'Xóa thành công');
        });
    }
}
