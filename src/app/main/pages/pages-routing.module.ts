import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BlankComponent } from '@app/components/blank/blank.component';
import { DynamicRouterComponent } from '@app/components/dynamic-router-component/dynamic-router-component.component';
import { PagesComponent } from './pages.component';

const routes: Routes = [
    {
        path: '',
        component: PagesComponent,
        children: [
            {
                path: 'home',
                loadChildren: () =>
                    import(
                        './financial-dashboard-page/finance-dashboard.module'
                    ).then((m) => m.FinanceDashboardModule),
                data: { preload: true },
            },
            {
                path: 'cham-com',
                loadChildren: () =>
                    import('./rice-dipping/rice-dipping.module').then(
                        (m) => m.RiceDippingModule,
                    ),
                data: { preload: true },
            },
            {
                path: 'thuc-don',
                loadChildren: () =>
                    import('./food-manager/food-manager.module').then(
                        (m) => m.FoodManagerModule,
                    ),
            },
            {
                path: 'dung-cu-cap-duong',
                loadChildren: () =>
                    import('./tool-manager/tool-manager.module').then(
                        (m) => m.ToolManagerModule,
                    ),
            },
            {
                path: 'quan-li-chi-an',
                loadChildren: () =>
                    import('./plans-food/plans-food.module').then(
                        (m) => m.PlansFoodModule,
                    ),
            },
            {
                path: 'kho',
                loadChildren: () =>
                    import('./store-manager/store-manager.module').then(
                        (m) => m.StoreManagerModule,
                    ),
            },
            {
                path: ':valueData',
                component: BlankComponent,
            },
            {
                path: 'category',
                children: [
                    {
                        path: ':valueData',
                        component: DynamicRouterComponent,
                    },
                ],
            },
            {
                path: '',
                redirectTo: 'home',
                pathMatch: 'full',
            },
        ],
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class PagesRoutingModule {}
