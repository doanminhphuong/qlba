import { SelectionModel } from '@angular/cdk/collections';
import {
    AfterViewInit,
    ChangeDetectorRef,
    Component,
    OnInit,
    ViewChild,
} from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatPaginator, MatTableDataSource } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { DeleteConfirmDialogComponent } from '@app/_shared/dialogs/delete-confirm-dialog/delete-confirm-dialog.component';
import { MaterialDialogComponent } from '@app/_shared/dialogs/material-dialog/material-dialog.component';
import { PriceService } from '@core/services/price.service';
import { ToastrService } from '@core/services/toastr.service';
import { Subject, Subscription } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';

export const MY_DATA = [
    {
        id: 'be0dca27-e54f-df38-2ab3-14d53820a4c4',
        maVTHH: 'MT',
        tenVTHH: 'Mùng tơi',
        donViTinh: 'Kg',
        giaMuaNgoai: 12500,
        giaTTSX: 10000,
        ghiChu: 'Bếp D14, B30',
    },
    {
        id: 'c184e9f0-7a6e-8dde-9db1-555c6f6bc394',
        maVTHH: 'MT',
        tenVTHH: 'Mùng tơi',
        donViTinh: 'Kg',
        giaMuaNgoai: 12500,
        giaTTSX: 10000,
        ghiChu: 'Bếp D14, B30',
    },
    {
        id: 'a92169e9-ef24-f051-08fb-e504ebad21e3',
        maVTHH: 'MT',
        tenVTHH: 'Mùng tơi',
        donViTinh: 'Kg',
        giaMuaNgoai: 12500,
        giaTTSX: 10000,
        ghiChu: 'Bếp D14, B30',
    },
    {
        id: '91e1e015-cc42-0a86-16b7-04517ca52c2e',
        maVTHH: 'MT',
        tenVTHH: 'Mùng tơi',
        donViTinh: 'Kg',
        giaMuaNgoai: 12500,
        giaTTSX: 10000,
        ghiChu: 'Bếp D14, B30',
    },
    {
        id: 'dd66f88e-4ce9-fe4e-1f7d-44da46dba6f8',
        maVTHH: 'MT',
        tenVTHH: 'Mùng tơi',
        donViTinh: 'Kg',
        giaMuaNgoai: 12500,
        giaTTSX: 10000,
        ghiChu: 'Bếp D14, B30',
    },
];

@Component({
    selector: 'app-adjustment-add-edit',
    templateUrl: './adjustment-add-edit.component.html',
    styleUrls: ['./adjustment-add-edit.component.scss'],
})
export class AdjustmentAddEditComponent implements OnInit, AfterViewInit {
    public applyFilter = new Subject<KeyboardEvent>();
    private subscription: Subscription;
    @ViewChild(MatPaginator) paginator!: MatPaginator;

    titleOnEdit: string;

    form: FormGroup;
    fields: any[] = [];

    displayedColumns: string[] = [
        'maLttpChatDot',
        'tenLttpChatDot',
        'donViTinh',
        'giaThiTruong',
        'giaTangGiaSanXuat',
        'value10',
        'actions',
    ];

    dataSource = new MatTableDataSource<any>([]);
    selection = new SelectionModel<any>(true, []);

    foodFuelList: any[] = [];

    recentlyTable: any;
    currentTable: any[] = [];
    adjustmentId: any;
    dataEdit: any;
    isEdit: boolean = false;
    isHasAdjustmentBefore: boolean = false;
    unitOptions: any[] = [];

    constructor(
        private readonly changeDetectorRef: ChangeDetectorRef,
        private _activatedRoute: ActivatedRoute,
        private _toastrService: ToastrService,
        private _router: Router,
        public dialog: MatDialog,
        private _priceService: PriceService,
    ) {
        this.subscription = this.applyFilter
            .pipe(
                map((event) => (event.target as HTMLInputElement).value),
                debounceTime(500),
                distinctUntilChanged(),
            )
            .subscribe((filterValue) => this.handleInputFilter(filterValue));
    }

    ngOnInit() {
        // Check if this is Create or Edit page
        this._activatedRoute.paramMap.subscribe((param) => {
            this.adjustmentId = +param.get('id');
            if (!this.adjustmentId) return;
            this.isEdit = true;
        });

        this.getInitForm();
        this.getInitData();
    }

    ngAfterViewChecked(): void {
        this.changeDetectorRef.detectChanges();
    }

    ngAfterViewInit(): void {}

    getInitData(): void {
        if (this.isEdit) {
            console.log('This is Edit Page !!!');
            this.getDataEdit();
        } else {
            console.log('This is Create Page !!!');
        }

        this.dataSource.paginator = this.paginator;
    }

    getInitForm(): void {
        this.fields = [
            {
                type: 'TEXT',
                referenceValue: 'maDotDieuChinhGia',
                name: 'Số chứng từ',
                defaultValue: '',
                required: '1',
                // icon: 'star',
                css: 'col-12 col-lg-6',
                appearance: 'legacy',
            },
            {
                type: 'SELECT',
                referenceValue: 'status',
                name: 'Trạng thái',
                defaultValue: 'ENABLE',
                options: [
                    { key: 'ENABLE', name: 'Hoạt động' },
                    { key: 'DISABLE', name: 'Tạm ngưng' },
                ],
                required: '0',
                css: 'col-12 col-lg-6',
                appearance: 'legacy',
            },
            {
                type: 'TEXT',
                referenceValue: 'tenDotDieuChinhGia',
                name: 'Tên phiếu',
                defaultValue: '',
                required: '1',
                // icon: 'assignment',
                css: 'col-12 col-lg-6',
                appearance: 'legacy',
            },
            {
                type: 'DATETIME',
                referenceValue: 'ngayHieuLuc',
                name: 'Ngày áp dụng',
                defaultValue: '',
                required: '1',
                css: 'col-12 col-lg-6',
                appearance: 'legacy',
            },
        ];

        let fieldsCtrls = {};
        let fieldList = [];

        for (let f of this.fields) {
            fieldList = [...fieldList, f];

            if (f.type === 'NUMBER') {
                fieldsCtrls[f.referenceValue] = new FormControl(
                    { value: f.defaultValue, disabled: f.disabled },
                    f.required === '1' ? [Validators.required] : [],
                );
            } else if (f.type !== 'CHECKBOX') {
                let validators = [
                    Validators.minLength(f.minLength),
                    Validators.maxLength(f.maxLength),
                    Validators.pattern(f.pattern),
                ];

                if (f.type === 'EMAIL') {
                    validators = [...validators, Validators.email];
                }

                fieldsCtrls[f.referenceValue] = new FormControl(
                    {
                        value: f.defaultValue || '',
                        disabled: f.disabled,
                    },

                    f.required === '1'
                        ? [...validators, Validators.required]
                        : [...validators],
                );
            } else {
                //if checkbox, it need multiple
                let opts = {};
                for (let opt of f.options) {
                    opts[opt.key] = new FormControl({
                        value: opt.value,
                        disabled: f.disabled,
                    });
                }
            }
        }

        this.form = new FormGroup(fieldsCtrls);
    }

    getDataEdit(): void {
        const dataEditId = {
            id: this.adjustmentId,
        };

        this._priceService
            .getPriceAdjustmentById(dataEditId)
            .subscribe((res) => {
                this.dataEdit = res.result;
                console.log('dataEdit:: ', this.dataEdit);

                // Get Title
                this.titleOnEdit = `Chỉnh sửa phiếu điều chỉnh giá '${this.dataEdit.maDotDieuChinhGia}'`;

                // Patch data to Form
                this.form.patchValue(this.dataEdit);

                // Patch data to Table (Details)
                this.currentTable = this.dataEdit.chiTiet.map((item) => ({
                    ...item,
                    ...item.lttpChatDot,
                }));
                this.dataSource.data = this.currentTable;
            });
    }

    // Handle Input Filter
    handleInputFilter(filterValue) {
        this.dataSource.filter = filterValue.trim().toLowerCase();
    }

    /** Whether the number of selected elements matches the total number of rows. */
    isAllSelected() {
        const numSelected = this.selection.selected.length;
        const numRows = this.dataSource.data.length;
        return numSelected === numRows;
    }

    /** Selects all rows if they are not all selected; otherwise clear selection. */
    toggleAllRows() {
        if (this.isAllSelected()) {
            this.selection.clear();
            return;
        }

        this.selection.select(...this.dataSource.data);
    }

    /** The label for the checkbox on the passed row */
    checkboxLabel(row?: any): string {
        if (!row) {
            return `${this.isAllSelected() ? 'deselect' : 'select'} all`;
        }
        return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${
            row.index + 1
        }`;
    }

    // GUID
    guid = () => {
        let s4 = () => {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        };
        //return id of format 'aaaaaaaa'-'aaaa'-'aaaa'-'aaaa'-'aaaaaaaaaaaa'
        return (
            s4() +
            s4() +
            '-' +
            s4() +
            '-' +
            s4() +
            '-' +
            s4() +
            '-' +
            s4() +
            s4() +
            s4()
        );
    };

    // Add row
    addRow(): void {
        const dialogRef = this.dialog.open(MaterialDialogComponent, {
            // disableClose: true,
            width: '70%',
            data: {
                type: 'ADJUSTMENT',
                currentTable: this.currentTable,
            },
            panelClass: 'add-field-dialog-class',
        });

        dialogRef.componentInstance.onSave.subscribe((data) => {
            const isSubmitted = !!data;

            this.currentTable = [...this.dataSource.data, ...data];
            this.dataSource.data = this.currentTable;

            dialogRef.close(isSubmitted);
        });

        dialogRef.afterClosed().subscribe((result) => {
            if (!result) return;
            this._toastrService.success('', 'Thêm thành công');
        });
    }

    // Delete row
    deleteAction(element: any): void {
        const dialogRef = this.dialog.open(DeleteConfirmDialogComponent, {
            width: '500px',
            data: element,
        });

        dialogRef.componentInstance.handleDelete.subscribe((dataDelete) => {
            const isSubmitted = !!dataDelete;
            dialogRef.close(isSubmitted);
        });

        dialogRef.afterClosed().subscribe((result) => {
            if (!result) return;
            const index: number = this.dataSource.data.indexOf(element);

            if (index !== -1) {
                this.dataSource.data.splice(index, 1);
            }

            this.currentTable = this.dataSource.data;
            this.dataSource.data = this.currentTable;
            this._toastrService.success('', 'Xóa thành công');
        });
    }

    // Back to list
    onBack(): void {
        this._router.navigate(['manage/app/category/gia-hoi-dong-gia']);
    }

    // Submit data
    onSubmit(): void {
        const dataSubmit = {
            ...this.dataEdit,
            ...this.form.value,
            chiTiet: [...this.currentTable],
        };

        console.log('dataSubmit:: ', dataSubmit);

        if (!this.isEdit) {
            this._priceService.createPriceAdjustment(dataSubmit).subscribe(
                (res) => {
                    this._router.navigate([
                        'manage/app/category/gia-hoi-dong-gia',
                    ]);
                    this._toastrService.success('', 'Tạo thành công');
                },
                (err) => this._toastrService.errorServer(err),
            );
        } else {
            this._priceService.updatePriceAdjustment(dataSubmit).subscribe(
                (res) => {
                    this._router.navigate([
                        'manage/app/category/gia-hoi-dong-gia',
                    ]);
                    this._toastrService.success('', 'Cập nhật thành công');
                },
                (err) => this._toastrService.errorServer(err),
            );
        }
    }
}
