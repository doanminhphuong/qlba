import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SummaryExportAddEditComponent } from './summary-export-add-edit.component';

describe('SummaryExportAddEditComponent', () => {
  let component: SummaryExportAddEditComponent;
  let fixture: ComponentFixture<SummaryExportAddEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SummaryExportAddEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SummaryExportAddEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
