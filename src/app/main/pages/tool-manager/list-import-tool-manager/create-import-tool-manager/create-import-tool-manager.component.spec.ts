import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateImportToolManagerComponent } from './create-import-tool-manager.component';

describe('CreateImportToolManagerComponent', () => {
  let component: CreateImportToolManagerComponent;
  let fixture: ComponentFixture<CreateImportToolManagerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateImportToolManagerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateImportToolManagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
