import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FoodManagerRoutingModule } from './food-manager-routing.module';
import { FoodManagerMainComponent } from './food-manager-main/food-manager-main.component';
import { MenuListManagerComponent } from './menu-list-manager/menu-list-manager.component';
import { DynamicCreateMenuComponent } from './dynamic-create-menu/dynamic-create-menu.component';
import {
    MatCheckboxModule,
    MatDatepickerModule,
    MatDividerModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatPaginatorModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatSelectModule,
    MatTableModule,
    MatTabsModule,
} from '@angular/material';
import { LayoutModule } from '@app/_shared/layout.module';
import { FoodAddEditComponent } from './food-manager-main/food-add-edit/food-add-edit.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SearchAdvancedComponent } from '@app/_shared/common/search-advanced/search-advanced.component';
import { MenuAddEditComponent } from './menu-list-manager/menu-add-edit/menu-add-edit.component';
import { FoodListDialogComponent } from './menu-list-manager/menu-add-edit/food-list-dialog/food-list-dialog.component';

@NgModule({
    declarations: [
        FoodManagerMainComponent,
        MenuListManagerComponent,
        DynamicCreateMenuComponent,
        FoodAddEditComponent,
        MenuAddEditComponent,
        FoodListDialogComponent,
    ],
    imports: [
        CommonModule,
        FoodManagerRoutingModule,
        MatIconModule,
        MatDividerModule,
        MatCheckboxModule,
        MatPaginatorModule,
        LayoutModule,
        MatProgressSpinnerModule,
        MatTableModule,
        FormsModule,
        ReactiveFormsModule,
        MatFormFieldModule,
        MatInputModule,
        MatRadioModule,
        MatExpansionModule,
        MatDatepickerModule,
        MatSelectModule,
        MatTabsModule,
    ],
    entryComponents: [SearchAdvancedComponent],
})
export class FoodManagerModule {}
