import { Component, EventEmitter, Inject, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
    selector: 'app-dialog-quanntitive-regulation',
    templateUrl: './dialog-quanntitive-regulation.component.html',
    styleUrls: ['./dialog-quanntitive-regulation.component.scss'],
})
export class DialogQuanntitiveRegulationComponent implements OnInit {
    forms: any[];
    fields;
    thisIsMyForm: FormGroup;
    onSave = new EventEmitter();

    constructor(
        private fb: FormBuilder,
        private dialogRef: MatDialogRef<DialogQuanntitiveRegulationComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any,
    ) {
        this.forms = data.reportResultCook;
    }

    ngOnInit() {
        this.thisIsMyForm = this.fb.group({
            formData: this.fb.array([]),
        });
        this.setupForm();
        if (this.data.dataRegulation) {
            this.thisIsMyForm.controls['formData'].patchValue(
                this.data.dataRegulation,
            );
        }
    }

    setupForm() {
        const controlArray = this.thisIsMyForm.get('formData') as FormArray;
        this.forms.forEach((form) => {
            let convertFormControl = this.removeAccents(form.ten);
            let formObject = {
                [convertFormControl]: ['', Validators.required],
            };

            controlArray.push(this.fb.group(formObject));
        });
    }

    removeAccents(str) {
        let text = str
            .normalize('NFD')
            .replace(/[\u0300-\u036f]/g, '')
            .replace(/đ/g, 'd')
            .replace(/Đ/g, 'D');

        return text.replace(/\s/g, '');
    }

    onSubmit() {
        let formValue = this.thisIsMyForm.value;
        // let dataEmit = formValue.formData.map((item) => Object.values(item)[0]);
        this.onSave.emit(formValue.formData);
        this.onNoClick();
    }

    onNoClick(): void {
        this.dialogRef.close();
    }
}
