import { Component, Input, OnInit } from '@angular/core';

@Component({
    selector: 'app-testing',
    templateUrl: './testing.component.html',
    styleUrls: ['./testing.component.scss'],
})
export class TestingComponent implements OnInit {

    // form render need these variable to render
    @Input() formRender: any;
    @Input() dataRenderForm: any;
    @Input() tabs: any[] = [];
    @Input() isFlatForm: any;

    constructor(
    ) { }

    ngOnInit() {
    }
}
