import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import {
    FormControl,
    FormGroup,
    FormGroupDirective,
    NgForm,
} from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material';

@Component({
    selector: 'fb-password',
    template: `
        <div [formGroup]="form">
            <ng-container
                *ngIf="!isFlatForm && tabCtrl && !subGroupCtrl"
                [formGroupName]="tabCtrl"
            >
                <ng-container [formGroupName]="groupCtrl">
                    <mat-form-field
                        appearance="outline"
                        [formGroupName]="getFormControlName()"
                    >
                        <mat-label>{{ field.name }}</mat-label>
                        <input
                            matInput
                            [type]="hide ? 'password' : 'text'"
                            [placeholder]="getPlaceholder()"
                            [value]="field.defaultValue"
                            [id]="field.referenceValue"
                            [name]="field.referenceValue"
                            formControlName="Value"
                            [required]="field.required === '1' ? true : false"
                            spellcheck="false"
                            [pattern]="field.pattern"
                            [maxLength]="
                                field.maxLength ? field.maxLength : 999
                            "
                            [readonly]="field.status === 'LOCK'"
                        />
                        <mat-icon matSuffix (click)="hide = !hide">{{
                            hide ? 'visibility_off' : 'visibility'
                        }}</mat-icon>
                        <!-- VALIDATORS -->
                        <mat-error>{{ getErrorMessage() }}</mat-error>
                    </mat-form-field>
                </ng-container>
            </ng-container>

            <ng-container
                *ngIf="!isFlatForm && tabCtrl && subGroupCtrl"
                [formGroupName]="tabCtrl"
            >
                <ng-container [formGroupName]="groupCtrl">
                    <ng-container [formGroupName]="subGroupCtrl">
                        <mat-form-field
                            appearance="outline"
                            [formGroupName]="getFormControlName()"
                        >
                            <mat-label>{{ field.name }}</mat-label>
                            <input
                                matInput
                                [type]="hide ? 'password' : 'text'"
                                [placeholder]="getPlaceholder()"
                                [value]="field.defaultValue"
                                [id]="field.referenceValue"
                                [name]="field.referenceValue"
                                formControlName="Value"
                                [required]="field.required === '1' ? true : false"
                                spellcheck="false"
                                [pattern]="field.pattern"
                                [maxLength]="
                                    field.maxLength ? field.maxLength : 999
                                "
                                [readonly]="field.status === 'LOCK'"
                            />
                            <mat-icon matSuffix (click)="hide = !hide">{{
                                hide ? 'visibility_off' : 'visibility'
                            }}</mat-icon>
                        </mat-form-field>
                    </ng-container>
                </ng-container>
            </ng-container>

            <mat-form-field *ngIf="isFlatForm || !tabCtrl" appearance="outline">
                <mat-label>{{ field.name }}</mat-label>
                <input
                    matInput
                    [type]="hide ? 'password' : 'text'"
                    [placeholder]="getPlaceholder()"
                    [value]="field.defaultValue"
                    [id]="field.referenceValue"
                    [name]="field.referenceValue"
                    [formControlName]="getFormControlName()"
                    [required]="field.required === '1' ? true : false"
                    spellcheck="false"
                    [pattern]="field.pattern"
                    [maxLength]="field.maxLength ? field.maxLength : 999"
                    [readonly]="field.status === 'LOCK'"
                />
                <mat-icon
                    style="cursor: pointer;"
                    matSuffix
                    (click)="hide = !hide"
                >
                    {{
                        hide ? 'visibility_off' : 'visibility'
                    }}
                </mat-icon>
                <!-- VALIDATORS -->
                <mat-error>{{ getErrorMessage() }}</mat-error>
            </mat-form-field>
        </div>
    `,
    styles: [
        `
            mat-form-field {
                width: 100%;
            }
        `,
    ],
})
export class PasswordComponent implements OnInit {
    @Input() field: any = {};
    @Input() form: FormGroup;
    @Input() tabCtrl: any;
    @Input() groupCtrl: any;
    @Input() subGroupCtrl: any;
    @Input() isFlatForm: boolean;

    hide = true;
    // matcher = new MyErrorStateMatcher();

    get fieldName() {
        if (!this.isFlatForm && this.tabCtrl) {
            if (!this.subGroupCtrl) {
                return this.form
                    .get(this.tabCtrl)
                    .get(this.groupCtrl)
                    .get(this.field.referenceValue)
                    .get('Value');
            } else {
                return this.form
                    .get(this.tabCtrl)
                    .get(this.groupCtrl)
                    .get(this.subGroupCtrl)
                    .get(this.field.referenceValue)
                    .get('Value');;
            }
        } else if (this.isFlatForm && this.tabCtrl) {
            if (!this.subGroupCtrl) {
                return this.form.controls[
                    `${this.tabCtrl}.${this.groupCtrl}.${this.field.referenceValue}`
                ];
            } else {
                return this.form.controls[
                    `${this.tabCtrl}.${this.groupCtrl}.${this.subGroupCtrl}.${this.field.referenceValue}`
                ];
            }
        } else {
            return this.form.controls[this.field.referenceValue];
        }
    }

    get isValid() {
        return this.fieldName.valid;
    }
    get isDirty() {
        return this.fieldName.dirty;
    }
    get isTouched() {
        return this.fieldName.touched;
    }

    constructor() { }

    ngOnInit() { }

    getPlaceholder() {
        return `Nhập vào ${this.field.name.toLowerCase()}`;
    }

    getFormControlName() {
        let formControlName = this.field.referenceValue;

        if (this.isFlatForm) {
            if (!this.subGroupCtrl) {
                formControlName = `${this.tabCtrl}.${this.groupCtrl}.${this.field.referenceValue}`;
            } else {
                formControlName = `${this.tabCtrl}.${this.groupCtrl}.${this.subGroupCtrl}.${this.field.referenceValue}`;
            }
        }

        return formControlName;
    }

    getErrorMessage() {
        let errorMessage = '';

        if (this.fieldName.hasError('required')) {
            errorMessage = `${this.field.name} không được để trống.`;
        } else if (this.fieldName.hasError('minlength')) {
            errorMessage = 'Phải nhập tối thiểu 5 ký tự';
        } else if (this.fieldName.hasError('maxlength')) {
            errorMessage = 'Nhập quá số ký tự cho phép.';
        } else if (this.fieldName.hasError('pattern')) {
            errorMessage = `${this.field.name} không hợp lệ.`;
        } else if (this.field.referenceValue === 'confirmNewPass') {
            if (this.fieldName.hasError('notSame')) {
                errorMessage = `Mật khẩu không trùng khớp`;
            }
        }

        return errorMessage;
    }
}

// export class MyErrorStateMatcher implements ErrorStateMatcher {
//     isErrorState( control: FormControl | null, form: FormGroupDirective | NgForm | null) {
//         const invalidCtrl = !!(control?.invalid && control?.parent?.dirty);
//         const invalidParent = !!(
//             control?.parent?.invalid && control?.parent?.dirty
//         );

//         return invalidCtrl || invalidParent;
//     }
// }
