import { SelectionModel } from '@angular/cdk/collections';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatPaginator, MatTableDataSource } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { DynamicDialogComponent } from '@app/components/dynamic-dialog-component/dynamic-dialog-component.component';
import { DeleteConfirmDialogComponent } from '@app/_shared/dialogs/delete-confirm-dialog/delete-confirm-dialog.component';
import { CategoryService } from '@core/services/category.service';
import { ToastrService } from '@core/services/toastr.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { InventoryService } from '@core/services/inventory.service';
import { FoodFuelService } from '@core/services/food-fuel.service';
import { MaintenanceDialogComponent } from '@app/_shared/dialogs/maintenance-dialog/maintenance-dialog.component';
import { Subject, Subscription } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';
import { DataService } from '@core/services/data-service';
import { CommonService } from '@core/services/common.service';

@Component({
    selector: 'app-maintenance-before',
    templateUrl: './maintenance-before.component.html',
    styleUrls: ['./maintenance-before.component.scss'],
})
export class MaintenanceBeforeComponent implements OnInit {
    @ViewChild(MatPaginator) paginator!: MatPaginator;
    public applyFilter = new Subject<KeyboardEvent>();
    private subscription: Subscription;

    currentDate: Date = new Date();

    displayedColumnsOfField: string[] = [
        'maDungCuCapDuong',
        'tenDungCuCapDuong',
        'donViTinh',
        'phanHang',
        'soLuong',
        'actions',
    ];

    dataSource = new MatTableDataSource<any>([]);
    selection = new SelectionModel<any>(true, []);
    form: FormGroup;
    fields: any[] = [];
    warehouseList: any[] = [];
    foodFuelList: any[] = [];

    inventoryList: any[] = [];
    totalPrice: number = 0;
    currentTable: any[] = [];
    unitOptions: any[] = [];
    typeOptions: any[] = [];

    idInventory: number;
    nameWarehouse;
    dauMoiBepId: number;
    khoNhanId: number;

    constructor(
        private _router: Router,
        public dialog: MatDialog,
        private _toastrService: ToastrService,
        private _categoryService: CategoryService,
        private _inventoryService: InventoryService,
        private _foodFuelService: FoodFuelService,
        private _activatedroute: ActivatedRoute,
        private _dataService: DataService,
        private _commonService: CommonService,
    ) {
        this.subscription = this.applyFilter
            .pipe(
                map((event) => (event.target as HTMLInputElement).value),
                debounceTime(500),
                distinctUntilChanged(),
            )
            .subscribe((filterValue) => this.handleInputFilter(filterValue));
    }

    ngOnInit() {
        this.dauMoiBepId = this._dataService.getDauMoiBepId();

        this._activatedroute.queryParams.subscribe((params) => {
            this.idInventory = parseInt(params.idInventory);
            this.nameWarehouse = params.nameInventory;
        });

        this.getInitData();
    }

    getInitData(): void {
        const bodyKhoDccd = {
            maxResultCount: 9999,
            // sorting: 'Id',
            criterias: [
                {
                    propertyName: 'dauMoiBepId',
                    operation: 0,
                    value: this.dauMoiBepId,
                },
            ],
        };

        this._commonService
            .callDataAPIShort('/api/services/read/KhoDccd/GetAll', bodyKhoDccd)
            .subscribe((res) => {
                const currentKhoDccd = res.result.items[0];
                this.khoNhanId = currentKhoDccd['id'];
                console.log('khoNhanId:: ', this.khoNhanId);

                this.getInitForm();
            });

        this.dataSource.paginator = this.paginator;
    }

    getInitForm(): void {
        if (this.idInventory) {
            this.fields = [
                {
                    type: 'DATETIME',
                    referenceValue: 'ngayNhap',
                    name: 'Ngày nhập kho',
                    defaultValue: '',
                    required: '1',
                    css: 'col-12 col-lg-6',
                    appearance: 'legacy',
                },
            ];
        } else {
            this.fields = [
                {
                    type: 'DATETIME',
                    referenceValue: 'ngayNhap',
                    name: 'Ngày nhập kho',
                    defaultValue: '',
                    required: '1',
                    css: 'col-12 col-lg-6',
                    appearance: 'legacy',
                },
                {
                    type: 'TEXT',
                    referenceValue: 'maPhieuNhap',
                    name: 'Mã phiếu nhập',
                    defaultValue: '',
                    required: '1',
                    css: 'col-12 col-lg-6',
                    appearance: 'legacy',
                },
            ];
        }

        let fieldsCtrls = {};
        let fieldList = [];

        for (let f of this.fields) {
            fieldList = [...fieldList, f];

            if (f.type === 'NUMBER') {
                fieldsCtrls[f.referenceValue] = new FormControl(
                    { value: f.defaultValue, disabled: f.disabled },
                    f.required === '1' ? [Validators.required] : [],
                );
            } else if (f.type !== 'CHECKBOX') {
                let validators = [
                    Validators.minLength(f.minLength),
                    Validators.maxLength(f.maxLength),
                    Validators.pattern(f.pattern),
                ];

                if (f.type === 'EMAIL') {
                    validators = [...validators, Validators.email];
                }

                fieldsCtrls[f.referenceValue] = new FormControl(
                    {
                        value: f.defaultValue || '',
                        disabled: f.disabled,
                    },

                    f.required === '1'
                        ? [...validators, Validators.required]
                        : [...validators],
                );

                if (f.type === 'DATETIME') {
                    fieldsCtrls['ngayNhap'].valueChanges.subscribe((val) => {
                        const KEY = 'NBD.DCCD';
                        let date = new Date(val);
                        let part3 = date
                            .toLocaleDateString('en-GB')
                            .replace(/[^0-9]/g, '');

                        let fullPath = KEY + '.' + part3;
                        this.form.patchValue({
                            maPhieuNhap: fullPath,
                        });

                        const dependField = fieldList.find(
                            (x) => x.dependenField === f.referenceValue,
                        );

                        if (!dependField) return;

                        fieldsCtrls[dependField.referenceValue].enable();
                    });
                }
            } else {
                //if checkbox, it need multiple
                let opts = {};
                for (let opt of f.options) {
                    opts[opt.key] = new FormControl({
                        value: opt.value,
                        disabled: f.disabled,
                    });
                }
            }
        }

        this.form = new FormGroup(fieldsCtrls);
    }

    guid = () => {
        let s4 = () => {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        };
        //return id of format 'aaaaaaaa'-'aaaa'-'aaaa'-'aaaa'-'aaaaaaaaaaaa'
        return (
            s4() +
            s4() +
            '-' +
            s4() +
            '-' +
            s4() +
            '-' +
            s4() +
            '-' +
            s4() +
            s4() +
            s4()
        );
    };

    // Handle Input Filter
    handleInputFilter(filterValue) {
        this.dataSource.data = this.currentTable.filter((item) =>
            item.tenDungCuCapDuong
                .toLowerCase()
                .includes(filterValue.trim().toLowerCase()),
        );
    }

    openDialog(dataDialog: any): void {
        console.log('dataDialog:: ', dataDialog);
        let fields = [
            {
                type: 'TEXT',
                referenceValue: 'maLttpChatDot',
                name: 'Mã VT, HH',
                defaultValue: '',
                required: '1',
                pattern: '^[a-zA-Z0-9-]*$',
                icon: 'code',
                disabled: true,
                css: 'col-12 col-lg-6',
            },
            {
                type: 'TEXT',
                referenceValue: 'tenLttpChatDot',
                name: 'Tên VT, HH',
                defaultValue: '',
                required: '1',
                icon: 'restaurant',
                disabled: true,
                css: 'col-12 col-lg-6',
            },
            {
                type: 'SELECT',
                referenceValue: 'phanLoai',
                name: 'Phân loại nguyên vật liệu',
                defaultValue: '',
                required: '0',
                options: [...this.typeOptions],
                disabled: true,
                css: 'col-12 col-lg-6',
            },
            {
                type: 'SELECT',
                referenceValue: 'donViTinh',
                name: 'Đơn vị tính',
                defaultValue: '',
                required: '0',
                options: [...this.unitOptions],
                disabled: true,
                css: 'col-12 col-lg-6',
            },
            {
                type: 'NUMBER',
                referenceValue: 'soluongTonKho',
                name: 'Số lượng',
                defaultValue: '',
                required: '1',
                icon: 'filter_vintage',
                css: 'col-12 col-lg-6',
            },
            {
                type: 'NUMBER',
                referenceValue: 'donGia',
                name: 'Đơn giá',
                defaultValue: '',
                required: '1',
                icon: 'filter_vintage',
                css: 'col-12 col-lg-6',
            },
        ];

        let dialogRef = this.dialog.open(DynamicDialogComponent, {
            // disableClose: true,
            width: '70%',
            data: {
                datas: dataDialog,
                fields: fields,
            },
            panelClass: 'my-custom-dialog-class',
        });

        dialogRef.componentInstance.onSave.subscribe((formValues) => {
            const isSubmitted = !!formValues;

            let dataSubmit = {
                ...dataDialog,
                ...formValues,
            };

            if (!dataDialog) {
                dataSubmit.id = this.guid();
                dataSubmit.creationTime = this.currentDate;

                this.dataSource.data = [...this.dataSource.data, dataSubmit];
            } else {
                const dataIndex = this.dataSource.data.findIndex(
                    (item) => item.id === dataDialog.id,
                );

                const currentData = [...this.dataSource.data];
                currentData.splice(dataIndex, 1, dataSubmit);
                this.dataSource.data = currentData;
                this.getTotalPrice();
            }
            dialogRef.close(isSubmitted);
        });

        dialogRef.componentInstance.handleDelete.subscribe((dataDelete) => {
            const isDeleted = !!dataDelete;
        });

        dialogRef.afterClosed().subscribe((result) => {
            if (!result) return;

            if (!dataDialog) {
                this._toastrService.success('', 'Thêm thành công');
            } else {
                this._toastrService.success('', 'Cập nhật thành công');
            }
        });
    }

    addRow(): void {
        const dialogRef = this.dialog.open(MaintenanceDialogComponent, {
            // disableClose: true,
            width: '70%',
            data: {
                type: 'INVENTORY_IMPORT',
                currentTable: this.currentTable,
            },
            panelClass: 'add-field-dialog-class',
        });

        dialogRef.componentInstance.onSave.subscribe((data) => {
            const isSubmitted = !!data;

            this.currentTable = [...this.dataSource.data, ...data].map(
                (item) => ({
                    ...item,
                    ton: item.soLuong,
                    tonDauKy: item.soLuong,
                }),
            );
            this.dataSource.data = this.currentTable;
            this.dataSource.paginator = this.paginator;

            // this.getTotalPrice();

            dialogRef.close(isSubmitted);
        });

        dialogRef.afterClosed().subscribe((result) => {
            if (!result) return;

            this._toastrService.success('', 'Thêm thành công');
        });
    }

    getTotalPrice() {
        this.dataSource.data.forEach((item) => {
            item.thanhTien = item.ton * item.donGia;
        });

        this.totalPrice = this.dataSource.data.reduce(
            (acc, item) => acc + item.thanhTien,
            0,
        );
    }

    editAction(element: any) {
        this.openDialog(element);
    }

    deleteAction(element: any): void {
        console.log('element:: ', element);

        const dialogRef = this.dialog.open(DeleteConfirmDialogComponent, {
            width: '500px',
            data: element,
        });

        dialogRef.componentInstance.handleDelete.subscribe((dataDelete) => {
            const isSubmitted = !!dataDelete;
            dialogRef.close(isSubmitted);
        });

        dialogRef.afterClosed().subscribe((result) => {
            if (!result) return;

            const index: number = this.currentTable.indexOf(element);
            if (index !== -1) {
                this.currentTable.splice(index, 1);
            }

            this.dataSource.data = this.currentTable;
            this.getTotalPrice();
            this._toastrService.success('', 'Xóa thành công');
        });
    }

    onBack(): void {
        this._router.navigate(['manage/app/category/kho-dung-cu-cap-duong/']);
    }

    onSubmit(): void {
        this.currentTable.forEach((item) => {
            item.khoId = this.khoNhanId;
        });

        console.log('currentTable:: ', this.currentTable);

        this._commonService
            .callDataAPIShort(
                '/api/services/write/TonKhoDccd/CreateList',
                this.currentTable,
            )
            .subscribe(
                (res) => {
                    this._toastrService.success('', 'Thêm thành công');
                    this._router.navigate([
                        'manage/app/category/kho-dung-cu-cap-duong',
                    ]);
                },
                (err) => this._toastrService.errorServer(err),
            );
    }
}
