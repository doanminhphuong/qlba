# Thông tin sản phẩm

URL: `http://qlba.tasp.vn/`

## Phiên bản hiện tại

-   Thêm: Trang quản lý bếp ăn
-   Cập nhật UI: Quân số ăn, tìm kiếm vật tư trong điều chỉnh giá,
    format ngày tháng năm khi xuất excel

## Lịch sử phiên bản

#### 1.10.2\*\* Ngày 18/12/2020

-   Thêm: Trang quản lý bếp ăn
-   Cập nhật UI: Quân số ăn, tìm kiếm vật tư trong điều chỉnh giá,
    format ngày tháng năm khi xuất excel

#### 1.10.1\*\* Ngày 16/12/2020

-   Fix: Fix bug trang châm cơm

#### 1.10.0\*\* Ngày 13/12/2020

-   Thêm: Xuất excel Báo cáo tài chính
-   Fix: Fix bug ở các trang: lịch sử xuất-nhập-tồn-tịnh-chuyển kho,
    tịnh kho, quản lý kho, chuyển kho, thực đơn, nhập tồn kho ban đầu,
-   Cập nhật UI: Quản lý số lượng,

#### 1.9.0\*\*Ngày 12/12/2020

-   Thêm: Xuất excel chi tiêu tiền ăn

#### 1.8.0\*\* Ngày 11/12/2020

-   Thêm: Xuất excel Chấm cơm (Mang sang)

#### 1.7.2\*\* Ngày 09/12/2020

-   Cập nhật: Cập nhật dữ liệu và giao diện ở trang Phiếu nhập, xuất, mua

#### 1.7.1\*\* Ngày 08/12/2020

-   Fix: Xuất Excel Chấm cơm

#### 1.7.0\*\* Ngày 07/12/2020

-   Thêm: Xuất Excel Chấm cơm

#### 1.6.0\*\* Ngày 06/12/2020

-   Thêm: Trang Báo cáo tài chính
-   Fix : Thay đổi dữ liệu table phiếu nhập kho, bug khi tích chọn tất cả quân nhân ở chấm cơm
-   Cập nhật: Chấm ăn thêm

#### 1.5.1\*\* Ngày 05/12/2020

-   Fix : Bug tính tổng số lượng khi chấm cơm

#### 1.5.0 \*\* Ngày 02/12/2020

-   Filter cho bảng chấm cơm

#### 1.4.0 \*\* Ngày 01/12/2020

-   Xử lý tích chấm cơm 1 lần cho tất cả trong 1 buổi 1 ngày
-   Fix: lý tích chấm cơm 1 lần cho tất cả trong 1 buổi 1 ngày

#### 1.3.0 \*\* Ngày 30/11/2020

-   Thêm chức năng tạo dự xuất
-   Fix: chọn đối tượng ở thực đơn tuần, hiển thị dữ liệu phiếu mua hàng chậm, lỗi page mua hàng

#### 1.2.0 \*\* Ngày 29/11/2020

-   Fix lỗi ở trang [phiếu mua hàng](http://git.tasp.vn/binhchungtt/qlbaweb/-/commit/5131a9627ceb6e8df906eaf364ec7658b7fba291).
-   Thêm tính năng xuất file Excel ở trang Xuất kho, Mua hàng

#### 1.1.1 \*\* Ngày 28/11/2020

-   Cập nhật lại API Nhập kho
-   Thêm tính năng xuất file Excel ở trang Nhập kho
-   Xử lý tích vào bảng chấm cơm

#### 1.0.0 \*\* START
