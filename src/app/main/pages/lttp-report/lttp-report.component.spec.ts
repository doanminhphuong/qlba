import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LTTPReportComponent } from './lttp-report.component';

describe('LTTPReportComponent', () => {
  let component: LTTPReportComponent;
  let fixture: ComponentFixture<LTTPReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LTTPReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LTTPReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
