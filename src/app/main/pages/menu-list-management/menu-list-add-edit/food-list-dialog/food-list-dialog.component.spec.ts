import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FoodListDialogComponent } from './food-list-dialog.component';

describe('FoodListDialogComponent', () => {
  let component: FoodListDialogComponent;
  let fixture: ComponentFixture<FoodListDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FoodListDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FoodListDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
