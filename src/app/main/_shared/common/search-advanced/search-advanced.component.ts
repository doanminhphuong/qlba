import {
    ChangeDetectorRef,
    Component,
    EventEmitter,
    Input,
    OnInit,
    Output,
} from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { FoodFuelService } from '@core/services/food-fuel.service';
import { Subject, Subscription } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';

@Component({
    selector: 'app-search-advanced',
    templateUrl: './search-advanced.component.html',
    styleUrls: ['./search-advanced.component.scss'],
})
export class SearchAdvancedComponent implements OnInit {
    @Input() function: Function;
    @Input() fieldSearch = [];
    @Input() label;
    @Input() placeHolder;
    @Output() getSearchInfo: EventEmitter<any> = new EventEmitter<any>();
    @Output() getSearchName: EventEmitter<any> = new EventEmitter<any>();
    formSearch: FormGroup;

    panelOpenState: boolean = false;

    applyFilter = new Subject<KeyboardEvent>();
    subscription: Subscription;

    searchCtrl: FormControl = new FormControl('');

    constructor(
        private readonly changeDetectorRef: ChangeDetectorRef,
        private _foodFuelService: FoodFuelService,
    ) {
        this.subscription = this.applyFilter
            .pipe(
                map((event) => (event.target as HTMLInputElement).value),
                debounceTime(500),
                distinctUntilChanged(),
            )
            .subscribe((filterValue) => this.getSearchName.emit(filterValue));
    }

    ngOnInit() {
        this.getInitForm();
    }

    getInitForm(): void {
        let fieldsCtrls = {};
        let fieldList = [];

        for (let f of this.fieldSearch) {
            fieldList = [...fieldList, f];

            if (f.type === 'NUMBER') {
                fieldsCtrls[f.referenceValue] = new FormControl(
                    { value: f.defaultValue, disabled: f.disabled },
                    f.required === '1' ? [Validators.required] : [],
                );
            } else if (f.type !== 'CHECKBOX') {
                let validators = [
                    Validators.minLength(f.minLength),
                    Validators.maxLength(f.maxLength),
                    Validators.pattern(f.pattern),
                ];

                if (f.type === 'EMAIL') {
                    validators = [...validators, Validators.email];
                }

                fieldsCtrls[f.referenceValue] = new FormControl(
                    {
                        value: f.defaultValue || '',
                        disabled: f.disabled,
                    },

                    f.required === '1'
                        ? [...validators, Validators.required]
                        : [...validators],
                );

                if (f.type === 'DATETIME') {
                    fieldsCtrls[f.referenceValue].valueChanges.subscribe(
                        (val) => {
                            const dependField = fieldList.find(
                                (x) => x.dependenField === f.referenceValue,
                            );

                            if (!dependField) return;

                            dependField.min = val;
                        },
                    );
                }

                if (f.type === 'SELECT') {
                    if (f.referenceValue === 'phanLoai') {
                        fieldsCtrls[f.referenceValue].valueChanges.subscribe(
                            (val) => {
                                if (val !== '') {
                                    const dependField = fieldList.find(
                                        (x) =>
                                            x.dependenField ===
                                            f.referenceValue,
                                    );

                                    if (!dependField) return;

                                    let body = {
                                        maxResultCount: 2147483647,
                                        criterias: [
                                            {
                                                propertyName: 'codeData',
                                                operation: 0,
                                                value: val,
                                            },
                                        ],
                                    };
                                    this._foodFuelService
                                        .getAllGroupOfFoodFuel(body)
                                        .subscribe((data) => {
                                            dependField.options =
                                                data.result.items.map(
                                                    (item) => ({
                                                        key: item.id,
                                                        name: item.tenNhomLttpChatDot,
                                                    }),
                                                );
                                            fieldsCtrls[
                                                dependField.referenceValue
                                            ].enable();
                                        });
                                }
                            },
                        );
                    }
                }
            } else {
                //if checkbox, it need multiple
                let opts = {};
                for (let opt of f.options) {
                    opts[opt.key] = new FormControl({
                        value: opt.value,
                        disabled: f.disabled,
                    });
                }
            }
        }

        this.formSearch = new FormGroup(fieldsCtrls);
    }

    getFields() {
        return this.fieldSearch;
    }

    ngAfterViewChecked(): void {
        this.changeDetectorRef.detectChanges();
    }

    onSubmit(): void {
        this.searchCtrl.reset();
        this.searchCtrl.disable();

        const dataSubmit = this.formSearch.value;

        this.getSearchInfo.emit(dataSubmit);
    }

    onDeleteSearchInfo(): void {
        let valueForm = this.formSearch.value;
        let keys = Object.keys(valueForm); // Get array of keys
        let result = {
            key: 'reset',
        };
        // Iterate all keys
        keys.forEach((key) => {
            result[key] = valueForm[key] = null;
        });

        if (
            this.formSearch.controls['nhomLttpChatDotId'] &&
            this.formSearch.controls['phanLoai']
        ) {
            this.formSearch
                .get('nhomLttpChatDotId')
                .disable({ onlySelf: true, emitEvent: true });

            this.formSearch.get('nhomLttpChatDotId').setValue('');
            this.formSearch
                .get('phanLoai')
                .reset('', { onlySelf: false, emitEvent: false });
        } else {
            this.formSearch.reset();
        }

        this.searchCtrl.enable();
        this.getSearchInfo.emit(result);
    }
}
