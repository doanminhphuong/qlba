import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EaterManagementComponent } from './eater-management.component';

describe('EaterManagementComponent', () => {
  let component: EaterManagementComponent;
  let fixture: ComponentFixture<EaterManagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EaterManagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EaterManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
