import { Injectable, OnInit } from '@angular/core';
import {
    ActivatedRouteSnapshot,
    CanActivate,
    Router,
    RouterStateSnapshot,
    UrlTree,
} from '@angular/router';
import { MenuService } from '@core/services/menu.service';
import { ToastrService } from '@core/services/toastr.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

const TOKEN_KEY = 'auth-token';

@Injectable({
    providedIn: 'root', // you can change to any level if needed
})
export class CanAccessRouterGuard implements CanActivate {
    currentUser;
    data;
    isPermission: boolean = false;

    constructor(
        private _router: Router,
        private _menuService: MenuService,
        private _toastrService: ToastrService,
    ) {}

    canActivate(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot,
    ):
        | Observable<boolean | UrlTree>
        | Promise<boolean | UrlTree>
        | boolean
        | UrlTree {
        const data = {
            id: next.routeConfig.path.slice(6),
            language: 'vi',
        };

        return this._menuService.checkPermission(data).pipe(
            map((item) => {
                if (item.result) {
                    return true;
                } else {
                    this._toastrService.error('', 'Không có quyền truy cập!');
                    this._router.navigate(['/']);
                    // this._router.navigate(['manage/app/category/trang-chu']);
                    return false;
                }
            }),
        );
    }
}
