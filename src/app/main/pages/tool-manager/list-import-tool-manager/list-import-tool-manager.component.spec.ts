import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListImportToolManagerComponent } from './list-import-tool-manager.component';

describe('ListImportToolManagerComponent', () => {
  let component: ListImportToolManagerComponent;
  let fixture: ComponentFixture<ListImportToolManagerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListImportToolManagerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListImportToolManagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
