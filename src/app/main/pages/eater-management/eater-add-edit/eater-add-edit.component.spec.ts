import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EaterAddEditComponent } from './eater-add-edit.component';

describe('EaterAddEditComponent', () => {
  let component: EaterAddEditComponent;
  let fixture: ComponentFixture<EaterAddEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EaterAddEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EaterAddEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
