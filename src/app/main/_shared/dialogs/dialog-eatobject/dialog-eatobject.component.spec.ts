import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogEatobjectComponent } from './dialog-eatobject.component';

describe('DialogEatobjectComponent', () => {
  let component: DialogEatobjectComponent;
  let fixture: ComponentFixture<DialogEatobjectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogEatobjectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogEatobjectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
