import { Component, OnInit } from '@angular/core';
import {
    animate,
    state,
    style,
    transition,
    trigger,
} from '@angular/animations';
import { resolve } from 'url';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, pairwise, startWith } from 'rxjs/operators';
import {
    DateAdapter,
    MatDatepicker,
    MatDatepickerInputEvent,
    MatTableDataSource,
    MAT_DATE_FORMATS,
    MAT_DATE_LOCALE,
} from '@angular/material';
import {
    MAT_MOMENT_DATE_ADAPTER_OPTIONS,
    MomentDateAdapter,
} from '@angular/material-moment-adapter';
import { MY_FORMATS } from '../inventory-import/import-add-edit/import-add-edit.component';
import { type } from 'os';
import { ExportExcelService } from '@core/services/export-excel.service';
import { CommonService } from '@core/services/common.service';
import { HttpErrorResponse } from '@angular/common/http';
import { ClassGetter } from '@angular/compiler/src/output/output_ast';
import { DataService } from '@core/services/data-service';
import { Moment } from 'moment';

export const MONTH_MODE_FORMATS = {
    parse: {
        dateInput: 'MM/YYYY',
    },
    display: {
        dateInput: 'MM/YYYY',
        monthYearLabel: 'MMM YYYY',
        dateA11yLabel: 'LL',
        monthYearA11yLabel: 'MMMM YYYY',
    },
};

@Component({
    selector: 'app-lttp-report',
    templateUrl: './lttp-report.component.html',
    styleUrls: ['./lttp-report.component.scss'],
    animations: [
        trigger('detailExpand', [
            state('collapsed', style({ height: '0px', minHeight: '0' })),
            state('expanded', style({ height: '*' })),
            transition(
                'expanded <=> collapsed',
                animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)'),
            ),
        ]),
    ],
    providers: [
        {
            provide: DateAdapter,
            useClass: MomentDateAdapter,
            deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS],
        },

        { provide: MAT_DATE_LOCALE, useValue: 'vi-VI' },
        { provide: MAT_DATE_FORMATS, useValue: MONTH_MODE_FORMATS },
        {
            provide: MAT_MOMENT_DATE_ADAPTER_OPTIONS,
            useValue: { useUtc: true },
        },
    ],
})
export class LTTPReportComponent implements OnInit {
    dauMoiBepId: number;
    dataSource = ELEMENT_DATA;
    columnsToDisplay = ['name', 'weight', 'symbol', 'position', 'description'];
    dataReport = new MatTableDataSource<any>([]);
    dataReportExportExcel;

    rfSearch: FormGroup;

    fileName: string;

    headerTable: string[] = [
        'tenLttpChatDot',
        'donViTinh',
        'soLuongThangTruoc',
        'nhapTrenBdMuaTT',
        'nhapTrenBdTGCB',
        'nhapDonViBdMuaTT',
        'nhapDonViBdTGCB',
        'soLuongCongNhap',
        'soLuongXuatAn',
        'soLuongXuatKhac',
        'soLuongCongXuat',
        'chuyenSangThangSau',
    ];

    pickerMonth = new FormControl(new Date(), Validators.required);
    filteredOptions: Observable<string[]>;
    minDate;

    constructor(
        private fb: FormBuilder,
        private exportExcelService: ExportExcelService,
        private commonService: CommonService,
        private dataService: DataService,
    ) { }

    ngOnInit() {
        this.dauMoiBepId = this.dataService.getDauMoiBepId();
        this.getInitData(this.dauMoiBepId);
        this.getInitForm();

        // this.onRfSearchChanges();
    }

    getInitData(dauMoiBepId: number) {
        this.fileName = `${new Date(this.pickerMonth.value).getMonth() + 1}`;
        const thang = new Date(this.pickerMonth.value).getMonth() + 1;
        const nam = new Date(this.pickerMonth.value).getFullYear();

        this.commonService
            .callDataAPIShort(
                '/api/services/read/BaoCao/GetBaoCaoSuDungLttpCd',
                {
                    dauMoiBepId,
                    thang,
                    nam,
                },
            )
            .subscribe(
                (res) => {
                    this.dataReport.data = res.result.danhSach;
                },
                (err: HttpErrorResponse) => {
                    console.log(err);
                    alert(err.message);
                },
            );
    }

    // getMindate() {
    //     let fromDay = this.rfSearch.get('fromDay').value;
    //     this.minDate = fromDay;
    // }

    addEvent(type: string, event: MatDatepickerInputEvent<Date>) {
        console.log(event);
    }

    isCheckValue = (value) => !value;

    onRfSearchChanges() {
        this.rfSearch
            .get('fromDay')
            .valueChanges.pipe(startWith(null as string), pairwise())
            .subscribe(([prev, next]: [any, any]) => {
                if (!this.isCheckValue(next)) {
                    this.rfSearch.get('toDay').enable();
                    this.minDate = new Date(next);
                }
            });
    }

    getInitForm() {
        this.rfSearch = this.fb.group({
            typeTime: '',
            unit: '',
            time: '',
            fromDay: '',
            toDay: [{ value: '', disabled: true }],
        });
    }

    onSubmit() {
        this.getInitData(this.dauMoiBepId);
    }
    resetForm() {
        this.rfSearch.reset();
        this.pickerMonth.reset();
    }

    exportExcel() {
        const thoiGian = new Date(this.pickerMonth.value);

        let bodyDataReportExcel = {
            thoiGian,
            chiTiet: this.dataReport.data,
            // thanhTienNgayTruoc: this.thanhTienNgayTruoc,
            // thanhTienCongNhap: this.thanhTienCongNhap,
            // thanhTienCongXuat: this.thanhTienCongXuat,
        };

        this.exportExcelService.generateFinancialReportExcelLTTP(
            bodyDataReportExcel,
            'Báo cáo sử dụng LTTP tháng ' + this.fileName,
        );
    }

    _monthSelectedHandler(
        chosenMonthDate: Moment,
        datepicker: MatDatepicker<Moment>,
    ) {
        datepicker.close();
        this.pickerMonth.setValue(chosenMonthDate);
    }

    renderToText(string: string) {
        let result = '';
        switch (string) {
            case 'tenLttpChatDot':
                result = 'Tên thực phẩm chất đốt';
                break;
            case 'donViTinh':
                result = 'ĐVT';
                break;
        }
        return result;
    }

    applyFilter(event: Event) {
        const filterValue = (event.target as HTMLInputElement).value;
        console.log(filterValue);
        this.dataReport.filter = filterValue.trim().toLowerCase();
    }
}

export interface PeriodicElement {
    name: string;
    position: number;
    weight: number;
    symbol: string;
    description: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
    {
        position: 1,
        name: 'Hydrogen',
        weight: 1.0079,
        symbol: 'H',
        description: `Hydrogen is a chemical element with symbol H and atomic number 1. With a standard
      atomic weight of 1.008, hydrogen is the lightest element on the periodic table.`,
    },
    {
        position: 2,
        name: 'Helium',
        weight: 4.0026,
        symbol: 'He',
        description: `Helium is a chemical element with symbol He and atomic number 2. It is a
      colorless, odorless, tasteless, non-toxic, inert, monatomic gas, the first in the noble gas
      group in the periodic table. Its boiling point is the lowest among all the elements.`,
    },
    {
        position: 3,
        name: 'Lithium',
        weight: 6.941,
        symbol: 'Li',
        description: `Lithium is a chemical element with symbol Li and atomic number 3. It is a soft,
      silvery-white alkali metal. Under standard conditions, it is the lightest metal and the
      lightest solid element.`,
    },
    {
        position: 4,
        name: 'Beryllium',
        weight: 9.0122,
        symbol: 'Be',
        description: `Beryllium is a chemical element with symbol Be and atomic number 4. It is a
      relatively rare element in the universe, usually occurring as a product of the spallation of
      larger atomic nuclei that have collided with cosmic rays.`,
    },
    {
        position: 5,
        name: 'Boron',
        weight: 10.811,
        symbol: 'B',
        description: `Boron is a chemical element with symbol B and atomic number 5. Produced entirely
      by cosmic ray spallation and supernovae and not by stellar nucleosynthesis, it is a
      low-abundance element in the Solar system and in the Earth's crust.`,
    },
    {
        position: 6,
        name: 'Carbon',
        weight: 12.0107,
        symbol: 'C',
        description: `Carbon is a chemical element with symbol C and atomic number 6. It is nonmetallic
      and tetravalent—making four electrons available to form covalent chemical bonds. It belongs
      to group 14 of the periodic table.`,
    },
    {
        position: 7,
        name: 'Nitrogen',
        weight: 14.0067,
        symbol: 'N',
        description: `Nitrogen is a chemical element with symbol N and atomic number 7. It was first
      discovered and isolated by Scottish physician Daniel Rutherford in 1772.`,
    },
    {
        position: 8,
        name: 'Oxygen',
        weight: 15.9994,
        symbol: 'O',
        description: `Oxygen is a chemical element with symbol O and atomic number 8. It is a member of
       the chalcogen group on the periodic table, a highly reactive nonmetal, and an oxidizing
       agent that readily forms oxides with most elements as well as with other compounds.`,
    },
    {
        position: 9,
        name: 'Fluorine',
        weight: 18.9984,
        symbol: 'F',
        description: `Fluorine is a chemical element with symbol F and atomic number 9. It is the
      lightest halogen and exists as a highly toxic pale yellow diatomic gas at standard
      conditions.`,
    },
    {
        position: 10,
        name: 'Neon',
        weight: 20.1797,
        symbol: 'Ne',
        description: `Neon is a chemical element with symbol Ne and atomic number 10. It is a noble gas.
      Neon is a colorless, odorless, inert monatomic gas under standard conditions, with about
      two-thirds the density of air.`,
    },
];

interface Food {
    value: string;
    viewValue: string;
}

interface Transaction {
    item: string;
    cost: number;
}

type TransactionMeal = {
    meal: string;
    cost: number;
    price: number;
};

export type Statistical = {
    value: number | string;
    title: string;
    icon: string;
    class?: string;
};
