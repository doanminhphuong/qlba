import { SelectionModel } from '@angular/cdk/collections';
import { Component, OnInit, ViewChild } from '@angular/core';
import {
    MatDialog,
    MatPaginator,
    MatTableDataSource,
    PageEvent,
} from '@angular/material';
import { Router } from '@angular/router';
import { DynamicDialogComponent } from '@app/components/dynamic-dialog-component/dynamic-dialog-component.component';
import { DeleteConfirmDialogComponent } from '@app/_shared/dialogs/delete-confirm-dialog/delete-confirm-dialog.component';
import { CategoryService } from '@core/services/category.service';
import { CommonService } from '@core/services/common.service';
import { DataService } from '@core/services/data-service';
import { PriceService } from '@core/services/price.service';
import { ToastrService } from '@core/services/toastr.service';
import { UserService } from '@core/services/user.service';
import { forkJoin, Subject, Subscription } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';

@Component({
    selector: 'app-purchase-management',
    templateUrl: './purchase-management.component.html',
    styleUrls: ['./purchase-management.component.scss'],
})
export class PurchaseManagementComponent implements OnInit {
    @ViewChild(MatPaginator) paginator!: MatPaginator;

    currentDate: Date = new Date();

    displayedColumnsOfField: string[] = [
        'maPhieuMuaHang',
        'nguoiMuaId',
        'ngayMua',
        'actions',
    ];

    dataSource = new MatTableDataSource<any>([]);
    selection = new SelectionModel<any>(true, []);

    inventoryList: any[] = [];
    totalPrice: number = 0;
    currentTable: any[] = [];
    unitOptions: any[] = [];
    typeOptions: any[] = [];

    fieldSearch: any[] = [
        {
            type: 'DATETIME',
            referenceValue: 'ngayMua',
            name: 'Ngày mua',
            defaultValue: '',
            required: '0',
            css: 'col-12 col-lg-12',
            appearance: 'legacy',
        },
        // {
        //     type: 'SELECT',
        //     referenceValue: 'trangThai',
        //     name: 'Trạng thái',
        //     defaultValue: '',
        //     options: [
        //         { key: 0, name: 'Lưu nháp' },
        //         { key: 1, name: 'Chờ duyệt' },
        //         { key: 2, name: 'Từ chối' },
        //         { key: 3, name: 'Đã duyệt' },
        //     ],
        //     css: 'col-12 col-lg-6',
        //     appearance: 'legacy',
        // },
    ];

    // Pagination
    totalRows = 0;
    pageSize = 10;
    currentPage = 0;
    pageSizeOptions: number[] = [5, 10, 25, 50];
    dataSkipped: number = 0;

    isFiltering: boolean = false;
    applyFilter = new Subject<KeyboardEvent>();
    subscription: Subscription;

    allowExec: boolean = false;
    isLoading: boolean = false;

    constructor(
        private _router: Router,
        public dialog: MatDialog,
        private _toastrService: ToastrService,
        private _categoryService: CategoryService,
        private _priceService: PriceService,
        private _dataService: DataService,
        private _commonService: CommonService,
    ) {
        this._commonService.checkPermission2().subscribe(
            (response) => {
                if (!response.result) return;

                const permissions = JSON.parse(response.result);
                console.log('permissions:: ', permissions);
                this.allowExec = permissions.some((obj) => obj.key === 'EXEC');

                if (!this.allowExec) {
                    this.displayedColumnsOfField.splice(3, 1);
                }
            },
            (error) => {
                this.displayedColumnsOfField.splice(3, 1);
            },
        );

        this.subscription = this.applyFilter
            .pipe(
                map((event) => (event.target as HTMLInputElement).value),
                debounceTime(500),
                distinctUntilChanged(),
            )
            .subscribe((filterValue) => this.handleInputFilter(filterValue));

        this.subscription = this._dataService.getData().subscribe((message) => {
            this.isLoading = true;
            this.getInitData(message.id);
        });
    }

    ngOnInit() {
        this.getMultiCategories();
        this.getInitData(this._dataService.getDauMoiBepId());
    }

    ngAfterViewInit() {
        this.dataSource.paginator = this.paginator;
    }

    getInitData(id: number): void {
        this.getAllPurchase(id);
    }

    getAllPurchase(id: number): void {
        const bodyPurchase = {
            maxResultCount: this.pageSize,
            skipCount: this.dataSkipped,
            sorting: 'ngayMua DESC',
            criterias: [
                {
                    propertyName: 'dauMoiBepId',
                    operation: 0,
                    value: id,
                },
            ],
        };

        this._priceService.getAllPurchase(bodyPurchase).subscribe((res) => {
            this.dataSource.data = res.result.items;
            setTimeout(() => {
                this.paginator.pageIndex = this.currentPage;
                this.paginator.length = res.result.totalCount;
            });
            setTimeout(() => {
                this.isLoading = false;
            }, 200);
        });
    }

    pageChanged(event: PageEvent) {
        this.currentPage = event.pageIndex;
        this.dataSkipped = event.pageIndex * 10;

        this.getAllPurchase(this._dataService.getDauMoiBepId());
    }

    getMultiCategories() {
        this.getUnitOptions();
        this.getTypeOptions();
    }

    getBodyCategory(groupCode: string) {
        return {
            maxResultCount: 10,
            skipCount: 0,
            sorting: 'Code',
            criterias: [
                {
                    propertyName: 'GroupCode',
                    operation: 6,
                    value: groupCode,
                },
            ],
        };
    }

    getUnitOptions() {
        this._categoryService
            .getAllCategory(this.getBodyCategory('don-vi-tinh'))
            .subscribe((res) => {
                this.unitOptions = res.result.map((item) => ({
                    key: item.codeData,
                    name: item.name,
                }));
            });
    }

    getTypeOptions() {
        this._categoryService
            .getAllCategory(this.getBodyCategory('phan-loai-lttp-chat-dot'))
            .subscribe((res) => {
                this.typeOptions = res.result.map((item) => ({
                    key: item.codeData,
                    name: item.name,
                }));
            });
    }

    /** Whether the number of selected elements matches the total number of rows. */
    isAllSelected() {
        const numSelected = this.selection.selected.length;
        const numRows = this.dataSource.data.length;
        return numSelected === numRows;
    }

    /** Selects all rows if they are not all selected; otherwise clear selection. */
    toggleAllRows() {
        if (this.isAllSelected()) {
            this.selection.clear();
            return;
        }

        this.selection.select(...this.dataSource.data);
    }

    /** The label for the checkbox on the passed row */
    checkboxLabel(row?: any): string {
        if (!row) {
            return `${this.isAllSelected() ? 'deselect' : 'select'} all`;
        }
        return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${
            row.index + 1
        }`;
    }

    guid = () => {
        let s4 = () => {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        };
        //return id of format 'aaaaaaaa'-'aaaa'-'aaaa'-'aaaa'-'aaaaaaaaaaaa'
        return (
            s4() +
            s4() +
            '-' +
            s4() +
            '-' +
            s4() +
            '-' +
            s4() +
            '-' +
            s4() +
            s4() +
            s4()
        );
    };

    createAction(): void {
        this._router.navigate(['manage/app/category/bang-ke-mua-hang/create']);
    }

    handleAction(element: any, typeAction): void {
        this._router.navigate([
            `manage/app/category/bang-ke-mua-hang/edit/${element.id}`,
            { typeAction },
        ]);
    }

    deleteAction(element: any): void {
        const dialogRef = this.dialog.open(DeleteConfirmDialogComponent, {
            width: '500px',
            data: element,
        });

        dialogRef.componentInstance.handleDelete.subscribe((dataDelete) => {
            const isSubmitted = !!dataDelete;

            this._priceService.deletePurchase(dataDelete).subscribe(
                (res) => dialogRef.close(isSubmitted),
                (err) => this._toastrService.errorServer(err),
            );
        });

        dialogRef.afterClosed().subscribe((result) => {
            if (!result) return;

            this.getInitData(this._dataService.getDauMoiBepId());
            this._toastrService.success('', 'Xóa thành công');
        });
    }

    // HANDLE SEARCH
    handleInputFilter(filterValue) {
        if (!filterValue) {
            this.isFiltering = false;
            this.getInitData(this._dataService.getDauMoiBepId());
        } else {
            this.isFiltering = true;
            let dataSearch = {
                maxResultCount: this.pageSize,
                skipCount: this.dataSkipped,
                criterias: [
                    {
                        propertyName: 'Search',
                        operation: 'OrEquals',
                        value: `[{"propertyName":"nguoiMua","operation":"Contains","value":"${filterValue}"}]`,
                    },
                    {
                        propertyName: 'dauMoiBepId',
                        operation: 0,
                        value: this._dataService.getDauMoiBepId(),
                    },
                ],
            };
            this._priceService.getAllPurchase(dataSearch).subscribe((res) => {
                this.dataSource.data = res.result.items;
                setTimeout(() => {
                    this.paginator.pageIndex = this.currentPage;
                    this.paginator.length = res.result.totalCount;
                });
            });
        }
    }

    handleSearchAdvanced(data: any) {
        if (data.key === 'reset') {
            this.isFiltering = false;
            this.getInitData(this._dataService.getDauMoiBepId());
        } else {
            this.isFiltering = true;
            let bodySubmit = {
                maxResultCount: this.pageSize,
                skipCount: this.dataSkipped,
                criterias: [
                    {
                        propertyName: data.ngayMua ? 'ngayMua' : '',
                        operation: data.ngayMua ? 'Equals' : 'Contains',
                        value: data.ngayMua
                            ? new Date(data.ngayMua._d).toISOString()
                            : '',
                    },
                    {
                        propertyName: 'dauMoiBepId',
                        operation: 0,
                        value: this._dataService.getDauMoiBepId(),
                    },
                ],
            };

            this._priceService.getAllPurchase(bodySubmit).subscribe((res) => {
                this.dataSource.data = res.result.items;
                setTimeout(() => {
                    this.paginator.pageIndex = this.currentPage;
                    this.paginator.length = res.result.totalCount;
                });
            });
        }
    }
}
