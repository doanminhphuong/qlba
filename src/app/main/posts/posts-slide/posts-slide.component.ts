import { Component, OnInit } from "@angular/core";
import {
  CriteriaRequestDto,
  PostClientServiceProxy,
  PostGuidGetDto
} from "@shared/service-proxies/cms-service-proxies";
import { NgbTimepickerConfig } from "@ng-bootstrap/ng-bootstrap";
import { NgbTimeStruct } from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: "app-posts-slide",
  templateUrl: "./posts-slide.component.html",
  styleUrls: ["./posts-slide.component.scss"]
})
export class PostsSlideComponent implements OnInit {
  posts: any;

  constructor(private postClientServiceProxy: PostClientServiceProxy) {}

  ngOnInit() {
    this.getPostsSlide();
  }

  getPostsSlide() {
    let input = new PostGuidGetDto();
    input.search = "HOMESLIDER";

    this.postClientServiceProxy.getPostByCode(input).subscribe((res) => {
      this.posts = res;
    });
  }
}
