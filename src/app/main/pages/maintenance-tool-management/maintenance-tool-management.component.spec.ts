import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MaintenanceToolManagementComponent } from './maintenance-tool-management.component';

describe('MaintenanceToolManagementComponent', () => {
  let component: MaintenanceToolManagementComponent;
  let fixture: ComponentFixture<MaintenanceToolManagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MaintenanceToolManagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MaintenanceToolManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
