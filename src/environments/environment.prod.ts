// "Production" enabled environment

export const environment = {
    production: true,
    hmr: false,
    appConfig: 'appconfig.json',
    baseUrl: 'http://qlba.tasp.vn',
    SWAGGER_URL: 'http://bepan.tasp.vn',
    SSO_URL: 'http://sso.tasp.vn',
    SYS_URL: 'http://sys.tasp.vn',
    DATA_URL: 'http://data.tasp.vn',
};
