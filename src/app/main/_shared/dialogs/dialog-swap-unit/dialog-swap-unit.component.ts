import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Unit } from '@app/pages/list-of-food-plans/list-of-food-plans-add-edit/dataJson';

export interface dataUnitModal {
    unit: number;
    listUnit: Unit[];
}

@Component({
    selector: 'app-dialog-swap-unit',
    templateUrl: './dialog-swap-unit.component.html',
    styleUrls: ['./dialog-swap-unit.component.scss'],
})
export class DialogSwapUnitComponent implements OnInit {
    constructor(
        public dialogRef: MatDialogRef<DialogSwapUnitComponent>,
        @Inject(MAT_DIALOG_DATA) public data: dataUnitModal,
    ) {}
    selectedValue: string = '';
    valueUnitSwapChange: number = Math.random() * 1000;
    datalistUnit = this.data.listUnit.filter(
        (item) => item.index !== this.data.unit,
    );
    currentUnit = this.data.listUnit.find(
        // If init unit is error, set temp value === 1. Current use Mock data
        (item) => item.index === this.data.unit,
    );
    bodyData = {
        selectedValue: this.selectedValue,
        valueUnitSwapChange: this.valueUnitSwapChange,
    };

    handleChangeUnitItem() {
        this.valueUnitSwapChange = Math.random() * 1000;
        this.bodyData = {
            ...this.bodyData,
            selectedValue: this.selectedValue,
            valueUnitSwapChange: this.valueUnitSwapChange,
        };
    }

    ngOnInit() {
        console.log('data::', this.data);
    }

    onNoClick(): void {
        this.dialogRef.close();
    }
}
