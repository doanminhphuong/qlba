import {
    AfterViewChecked,
    AfterViewInit,
    ChangeDetectorRef,
    Component,
    Inject,
    OnInit,
} from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { QLDKService } from '@core/services/qldk.service';
import { ToastrService } from '@core/services/toastr.service';

export interface DialogQldkData {
    name: string;
    fields: any[];
    datas: any;
}

@Component({
    selector: 'app-qldt-dialog',
    templateUrl: './qldk-dialog.component.html',
    styleUrls: ['./qldk-dialog.component.scss'],
})
export class QldkDialogComponent
    implements OnInit, AfterViewInit, AfterViewChecked
{
    form: FormGroup;
    fields: any[];
    fileName;

    isSubmitted: boolean = false;
    isEnableDelete: boolean = false;
    constructor(
        private readonly changeDetectorRef: ChangeDetectorRef,
        public dialog: MatDialog,
        public dialogRef: MatDialogRef<QldkDialogComponent>,
        private _toastrService: ToastrService,
        @Inject(MAT_DIALOG_DATA) public data: DialogQldkData,
        private qldkService: QLDKService,
    ) {}

    ngOnInit() {
        console.log('this.data', this.data);
        let fieldsCtrls = {};

        for (let f of this.data.fields) {
            if (f.type === 'NUMBER') {
                fieldsCtrls[f.referenceValue] = new FormControl(
                    f.defaultValue,
                    f.required === '1' ? [Validators.required] : [],
                );
            } else if (f.type !== 'CHECKBOX') {
                let validators = [
                    Validators.minLength(f.minLength),
                    Validators.maxLength(f.maxLength),
                ];
                if (f.type === 'EMAIL') {
                    validators = [...validators, Validators.email];
                }
                fieldsCtrls[f.referenceValue] = new FormControl(
                    f.defaultValue || '',
                    f.required === '1'
                        ? [...validators, Validators.required]
                        : [...validators],
                );
            } else {
                //if checkbox, it need multiple
                let opts = [];
                for (let index in f.options) {
                    opts[index] = new FormControl(false);
                }
                fieldsCtrls[f.referenceValue] = new FormArray(
                    opts,
                    f.required === '1' ? [Validators.required] : [],
                );
            }
        }

        this.form = new FormGroup(fieldsCtrls);
    }

    ngAfterViewInit(): void {
        const dataEdit = this.data.datas;
        if (dataEdit) {
            this.form.patchValue(this.data.datas);
        }
    }

    ngAfterViewChecked(): void {
        this.changeDetectorRef.detectChanges();
    }

    onCloseDialog(data?: any): void {
        this.dialogRef.close(data);
    }

    getFields() {
        return this.data.fields;
    }

    onDelete(): void {
        this.isEnableDelete = true;
    }

    onCancelDelete(): void {
        this.isEnableDelete = false;
    }

    // Get file
    onFileSelected(event: any) {
        const file: File = event.target.files[0];

        if (file) {
            this.fileName = file.name;

            console.log('file', file);
        }
    }

    handleDelete(): void {
        this.isSubmitted = true;

        const formValues = {
            id: this.data.datas.id,
        };

        this.qldkService.deleteQLDK(formValues).subscribe((res) => {
            this.onCloseDialog(this.isSubmitted);
        });
    }

    onSubmit(): void {
        this.isSubmitted = true;

        let formValues = {
            ...this.form.value,
        };

        // Data from table
        const initData = this.data.datas;

        console.log('this.form.value', this.form.value);

        if (initData) {
            formValues = {
                ...initData, // Old data
                ...this.form.value, // New data
            };
        }

        console.log('Form Submit: ', formValues);

        if (this.form.valid) {
            if (initData) {
                this.qldkService.updateQLDK(formValues).subscribe(
                    (res) => {
                        this.onCloseDialog(this.isSubmitted);
                    },
                    (err) => this._toastrService.errorServer(err),
                );
            } else {
                this.qldkService.createQLDK(formValues).subscribe(
                    (res) => {
                        this.onCloseDialog(this.isSubmitted);
                    },
                    (err) => this._toastrService.errorServer(err),
                );
            }
        }
    }
}
