import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CookManagerAddEditComponent } from './cook-manager-add-edit.component';

describe('CookManagerAddEditComponent', () => {
  let component: CookManagerAddEditComponent;
  let fixture: ComponentFixture<CookManagerAddEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CookManagerAddEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CookManagerAddEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
