import {
    animate,
    state,
    style,
    transition,
    trigger,
} from '@angular/animations';
import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatPaginator, MatTableDataSource } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { DynamicDialogComponent } from '@app/components/dynamic-dialog-component/dynamic-dialog-component.component';
import { DeleteConfirmDialogComponent } from '@app/_shared/dialogs/delete-confirm-dialog/delete-confirm-dialog.component';
import { MaterialDialogComponent } from '@app/_shared/dialogs/material-dialog/material-dialog.component';
import { PriceConvertPipe } from '@core/pipes/price-convert.pipe';
import { CategoryService } from '@core/services/category.service';
import { CommonService } from '@core/services/common.service';
import { DataService } from '@core/services/data-service';
import { FoodFuelService } from '@core/services/food-fuel.service';
import { FoodyService } from '@core/services/foody.service';
import { ToastrService } from '@core/services/toastr.service';
import { forkJoin, Subject, Subscription } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';

@Component({
    selector: 'app-food-add-edit',
    templateUrl: './food-add-edit.component.html',
    styleUrls: ['./food-add-edit.component.scss'],
    animations: [
        trigger('detailExpand', [
            state('collapsed', style({ height: '0px', minHeight: '0' })),
            state('expanded', style({ height: '*' })),
            transition(
                'expanded <=> collapsed',
                animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)'),
            ),
        ]),
    ],
})
export class FoodAddEditComponent implements OnInit {
    public applyFilter = new Subject<KeyboardEvent>();
    private subscription: Subscription;
    @ViewChild(MatPaginator) paginator!: MatPaginator;
    currentDate: Date = new Date();

    danhSachMucTienAn: any[] = [];
    titleOnEdit: string;

    form: FormGroup;
    fields: any[] = [];

    formPLG: FormGroup;
    fieldsPLG: any[] = [];

    dataSource2 = new MatTableDataSource<any>([]);

    dataSource = [];
    displayedColumns: string[] = [
        'maMonAn',
        'tenMonAn',
        'maLoaiMonAn',
        'monChinh',
        'actions',
    ];

    currentTable: any[] = [];
    monAnId: any;
    dataEdit: any;
    selectedMainFood: any;
    priceAdjustmentList: any[] = [];
    isActive: boolean = false;
    isSelectedMainFood: boolean = false;

    // Boolean
    isEdit: boolean = false;
    typeAction;

    objectList: any[] = [];
    typeOptions: any[] = [];
    foodFuelList: any[] = [];

    value: string;
    number1: number;
    number2: number;

    constructor(
        private readonly changeDetectorRef: ChangeDetectorRef,
        private _activatedRoute: ActivatedRoute,
        private _toastrService: ToastrService,
        private _router: Router,
        public dialog: MatDialog,
        private _categoryService: CategoryService,
        private _foodFuelService: FoodFuelService,
        private _foodyService: FoodyService,
        private _dataService: DataService,
        private _commonService: CommonService,
    ) {
        this.subscription = this.applyFilter
            .pipe(
                map((event) => (event.target as HTMLInputElement).value),
                debounceTime(500),
                distinctUntilChanged(),
            )
            .subscribe((filterValue) => this.handleInputFilter(filterValue));
    }

    ngOnInit() {
        // Check if this is Create or Edit page
        this._activatedRoute.paramMap.subscribe((param) => {
            this.monAnId = +param.get('id');
            if (!this.monAnId) return;
            this.isEdit = true;
        });

        this.getInitData();
    }

    ngAfterViewChecked(): void {
        this.changeDetectorRef.detectChanges();
    }

    ngAfterViewInit(): void {}

    getBodyCategory(groupCode: string) {
        return {
            maxResultCount: 10,
            skipCount: 0,
            sorting: 'Code',
            criterias: [
                {
                    propertyName: 'GroupCode',
                    operation: 6,
                    value: groupCode,
                },
            ],
        };
    }

    getInitData(): void {
        const dataTypeOfFood = this._categoryService.getAllCategory(
            this.getBodyCategory('loai-mon-an'),
        );

        const dataObject = this._categoryService.getAllCategory(
            this.getBodyCategory('doi-tuong-an'),
        );

        const dataFoodFuel = this._foodFuelService.getAllFoodFuel({
            maxResultCount: 9999,
        });

        const dataPriceOfEat = this._commonService.callDataAPIShort(
            '/api/services/read/DinhLuongAn/GetCurrent',
            {},
        );

        forkJoin([
            dataTypeOfFood,
            dataObject,
            dataFoodFuel,
            dataPriceOfEat,
        ]).subscribe((results) => {
            this.typeOptions = results[0].result.map((item) => ({
                key: item.number1,
                name: item.name,
            }));

            this.objectList = results[1].result;

            this.foodFuelList = results[2].result.items;

            this.danhSachMucTienAn = results[3].result.map((item) => ({
                mucTien: item.mucTienAn,
                dinhLuongSang: 0,
                dinhLuongTruaChieu: 0,
            }));

            if (this.typeOptions.length > 0) {
                this.getInitForm();
            }
        });
    }

    getInitForm(): void {
        this.fields = [
            {
                type: 'TEXT',
                referenceValue: 'tenMonAn',
                name: 'Tên món ăn',
                defaultValue: '',
                required: '1',
                css: 'col-12 col-lg-4',
                appearance: 'legacy',
            },
            {
                type: 'TEXT',
                referenceValue: 'maMonAn',
                name: 'Mã món ăn',
                defaultValue: '',
                required: '1',
                css: 'col-12 col-lg-4',
                appearance: 'legacy',
                dependenField: 'tenMonAn',
            },
            {
                type: 'SELECT',
                referenceValue: 'loaiMonAn',
                name: 'Loại món ăn',
                defaultValue: '',
                required: '1',
                options: [...this.typeOptions],
                css: 'col-12 col-lg-4',
                appearance: 'legacy',
            },
            {
                type: 'TEXTAREA',
                referenceValue: 'moTa',
                name: 'Mô tả',
                defaultValue: '',
                required: '0',
                css: 'col-12 col-lg-12',
                appearance: 'legacy',
            },
            {
                type: 'RADIO',
                referenceValue: 'status',
                name: 'Trạng thái',
                defaultValue: 'ENABLE',
                required: '0',
                options: [
                    { key: 'ENABLE', name: 'Hoạt động' },
                    { key: 'DISABLE', name: 'Tạm ngưng' },
                ],
                css: 'col-12 col-lg-6',
                appearance: 'legacy',
            },
        ];

        let fieldsCtrls = {};
        let fieldList = [];

        for (let f of this.fields) {
            fieldList = [...fieldList, f];

            if (f.type === 'NUMBER') {
                fieldsCtrls[f.referenceValue] = new FormControl(
                    { value: f.defaultValue, disabled: f.disabled },
                    f.required === '1' ? [Validators.required] : [],
                );
            } else if (f.type !== 'CHECKBOX') {
                let validators = [
                    Validators.minLength(f.minLength),
                    Validators.maxLength(f.maxLength),
                    Validators.pattern(f.pattern),
                ];

                if (f.type === 'EMAIL') {
                    validators = [...validators, Validators.email];
                }

                fieldsCtrls[f.referenceValue] = new FormControl(
                    {
                        value: f.defaultValue || '',
                        disabled: f.disabled,
                    },
                    f.required === '1'
                        ? [...validators, Validators.required]
                        : [...validators],
                );

                if (f.type === 'TEXT') {
                    fieldsCtrls[f.referenceValue].valueChanges.subscribe(
                        (val) => {
                            if (val !== '') {
                                const dependedField = fieldList.find(
                                    (x) => x.dependenField === f.referenceValue,
                                );
                                if (!dependedField) return;

                                if (val.length > 10) {
                                    fieldsCtrls[
                                        dependedField.referenceValue
                                    ].patchValue(
                                        val
                                            .normalize('NFD')
                                            .replace(/[\u0300-\u036f]/g, '')
                                            .match(/(^|\s)\w[0-9]{0,3}/g)
                                            .join('')
                                            .replace(/\s/g, '')
                                            .toUpperCase(),
                                    );
                                } else {
                                    fieldsCtrls[
                                        dependedField.referenceValue
                                    ].patchValue(
                                        val
                                            .normalize('NFD')
                                            .replace(/[\u0300-\u036f]/g, '')
                                            .replace(/\s/g, '')
                                            .toUpperCase(),
                                    );
                                }
                            }
                        },
                    );
                }
            } else {
                //if checkbox, it need multiple
                let opts = {};
                for (let opt of f.options) {
                    opts[opt.key] = new FormControl({
                        value: opt.value,
                        disabled: f.disabled,
                    });
                }
            }
        }

        this.form = new FormGroup(fieldsCtrls);

        this.fieldsPLG = [
            {
                type: 'NUMBER',
                referenceValue: 'p',
                name: 'Năng lượng P',
                defaultValue: '',
                required: '0',
                css: 'col-12 col-lg-4',
                appearance: 'legacy',
            },
            {
                type: 'NUMBER',
                referenceValue: 'l',
                name: 'Năng lượng L',
                defaultValue: '',
                required: '0',
                css: 'col-12 col-lg-4',
                appearance: 'legacy',
                dependenField: 'tenMonAn',
            },
            {
                type: 'NUMBER',
                referenceValue: 'g',
                name: 'Năng lượng G',
                defaultValue: '',
                required: '0',
                css: 'col-12 col-lg-4',
                appearance: 'legacy',
            },
        ];

        let fieldsPLGCtrls = {};
        let fieldPLGList = [];

        for (let f of this.fieldsPLG) {
            fieldPLGList = [...fieldPLGList, f];

            if (f.type === 'NUMBER') {
                fieldsPLGCtrls[f.referenceValue] = new FormControl(
                    { value: f.defaultValue, disabled: f.disabled },
                    f.required === '1' ? [Validators.required] : [],
                );
            } else if (f.type !== 'CHECKBOX') {
                let validators = [
                    Validators.minLength(f.minLength),
                    Validators.maxLength(f.maxLength),
                    Validators.pattern(f.pattern),
                ];

                if (f.type === 'EMAIL') {
                    validators = [...validators, Validators.email];
                }

                fieldsPLGCtrls[f.referenceValue] = new FormControl(
                    {
                        value: f.defaultValue || '',
                        disabled: f.disabled,
                    },
                    f.required === '1'
                        ? [...validators, Validators.required]
                        : [...validators],
                );
            } else {
                //if checkbox, it need multiple
                let opts = {};
                for (let opt of f.options) {
                    opts[opt.key] = new FormControl({
                        value: opt.value,
                        disabled: f.disabled,
                    });
                }
            }
        }

        this.formPLG = new FormGroup(fieldsPLGCtrls);

        if (this.isEdit) {
            this.getDataEdit();
        }
    }

    getDataEdit(): void {
        const dataEditId = {
            id: this.monAnId,
        };

        this._foodyService.getMonAnById(dataEditId).subscribe((res) => {
            this.dataEdit = res.result;

            // Get Title
            this.titleOnEdit = `Chỉnh sửa món ăn '${this.dataEdit.maMonAn}'`;

            // Patch data to Form
            this.form.patchValue(this.dataEdit);
            this.formPLG.patchValue(this.dataEdit);

            this.danhSachMucTienAn = this.dataEdit.dinhLuong;
            this.value = this.dataEdit.nguyenLieuChinh;
            this.number1 = this.dataEdit.phanTramSuDung;
            this.number2 = this.dataEdit.nhietLuongCho100Gam;

            // Convert data table
            const foodFuelIds = this.dataEdit.chiTiet
                .map((item) => item.lttpChatDotId)
                .filter((item, index, arr) => arr.indexOf(item) === index);
            const dataTable = this.foodFuelList.filter((item) =>
                foodFuelIds.includes(item.id),
            );
            dataTable.forEach((data) => {
                const childDatas = this.dataEdit.chiTiet.filter(
                    (child) => data.id === child.lttpChatDotId,
                );

                data.lttpChatDotId = data.id;
                delete data.id;
                data.dinhLuongList = [...childDatas];
                data.monChinh = childDatas[0].monChinh;

                data.dinhLuongList.forEach((dl) => {
                    dl.codeData = this.getObjectData(
                        dl.maNhomDoiTuong,
                        'codeData',
                    );
                    dl.name = this.getObjectData(dl.maNhomDoiTuong, 'name');
                });
            });

            // Patch data to Table (Details)
            this.currentTable = dataTable;
            this.dataSource = this.currentTable;
        });
    }

    selectMainFood(element) {
        this.dataSource.forEach((data) => {
            if (data.lttpChatDotId === element.lttpChatDotId) {
                return (data.monChinh = true);
            } else {
                return (data.monChinh = false);
            }
        });

        this.isSelectedMainFood = true;

        let bodySubmit = {
            maxResultCount: 99999999,
            criterias: [
                {
                    propertyName: 'GroupCode',
                    operation: 6,
                    value: 'dinh-luong-co-ban',
                },
                {
                    propertyName: 'number1',
                    operation: 0,
                    value: element.nhomLttpChatDotId,
                },
            ],
        };

        this._commonService
            .callDataAPIShort('/api/services/read/Category/GetList', bodySubmit)
            .subscribe((response) => {
                if (response.result.length > 0) {
                    this.value = response.result[0].value2;
                    this.number1 = response.result[0].number2;
                    this.number2 = response.result[0].number3;
                } else {
                    this.value = '';
                    this.number1 = 0;
                    this.number2 = 0;
                }
            });
    }

    getObjectData(id, objKey) {
        const dataFound = this.objectList.find((item) => item.id === id);
        return dataFound ? dataFound[objKey] : '';
    }

    // Handle Input Filter
    handleInputFilter(filterValue) {
        this.dataSource = this.currentTable.filter((item) =>
            item.tenLttpChatDot
                .toLowerCase()
                .includes(filterValue.trim().toLowerCase()),
        );
    }

    // GUID
    guid = () => {
        let s4 = () => {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        };
        //return id of format 'aaaaaaaa'-'aaaa'-'aaaa'-'aaaa'-'aaaaaaaaaaaa'
        return (
            s4() +
            s4() +
            '-' +
            s4() +
            '-' +
            s4() +
            '-' +
            s4() +
            '-' +
            s4() +
            s4() +
            s4()
        );
    };

    // Add row
    addRow(): void {
        const dialogRef = this.dialog.open(MaterialDialogComponent, {
            // disableClose: true,
            width: '70%',
            data: {
                type: 'FOODY',
                currentTable: this.currentTable,
            },
            panelClass: 'add-field-dialog-class',
        });

        dialogRef.componentInstance.onSave.subscribe((datas) => {
            const isSubmitted = !!datas;

            datas.forEach((data) => {
                const newObjectList = this.objectList.map((obj) => ({
                    codeData: obj.codeData,
                    name: obj.name,
                    maNhomDoiTuong: obj.id,
                    id: this.guid(),
                }));
                (data.monChinh = false),
                    (data.dinhLuongList = [...newObjectList]);
            });

            this.currentTable = [...this.currentTable, ...datas];
            this.dataSource = this.currentTable;

            dialogRef.close(isSubmitted);
        });

        dialogRef.afterClosed().subscribe((result) => {
            if (!result) return;
            this._toastrService.success('', 'Thêm thành công');
        });
    }

    // Delete row
    deleteAction(element: any): void {
        const dialogRef = this.dialog.open(DeleteConfirmDialogComponent, {
            width: '500px',
            data: element,
        });

        dialogRef.componentInstance.handleDelete.subscribe((dataDelete) => {
            const isSubmitted = !!dataDelete;
            dialogRef.close(isSubmitted);
        });

        dialogRef.afterClosed().subscribe((result) => {
            if (!result) return;

            this.currentTable = this.dataSource.filter(
                (item) => item.lttpChatDotId !== element.lttpChatDotId,
            );
            this.dataSource = this.currentTable;

            this._toastrService.success('', 'Xóa thành công');
        });
    }

    // Back to list
    onBack(): void {
        this._router.navigate(['app/thuc-don/quan-ly-mon-an']);
    }

    // Submit data
    onSubmit(): void {
        let chiTiet = this.dataSource;

        const dataSubmit = {
            ...this.dataEdit,
            ...this.form.value,
            ...this.formPLG.value,
            nguyenLieuChinh: this.value,
            phanTramSuDung: this.number1,
            nhietLuongCho100Gam: this.number2,
            chiTiet,
            dauMoiBepId: this._dataService.getDauMoiBepId(),
            dinhLuong: this.danhSachMucTienAn,
        };

        if (!this.isEdit) {
            this._foodyService.createMonAn(dataSubmit).subscribe(
                (res) => {
                    this._router.navigate(['app/thuc-don/quan-ly-mon-an']);
                    this._toastrService.success('', 'Thêm thành công');
                },
                (err) => this._toastrService.errorServer(err),
            );
        } else {
            this._foodyService.updateMonAn(dataSubmit).subscribe(
                (res) => {
                    this._router.navigate(['app/thuc-don/quan-ly-mon-an']);
                    this._toastrService.success('', 'Cập nhật thành công');
                },
                (err) => this._toastrService.errorServer(err),
            );
        }
    }
}
