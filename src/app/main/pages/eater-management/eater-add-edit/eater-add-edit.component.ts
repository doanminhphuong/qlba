import { SelectionModel } from '@angular/cdk/collections';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import {
    MatDialog,
    MatPaginator,
    MatTable,
    MatTableDataSource,
} from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { DeleteConfirmDialogComponent } from '@app/_shared/dialogs/delete-confirm-dialog/delete-confirm-dialog.component';
import { CategoryService } from '@core/services/category.service';
import { CommonService } from '@core/services/common.service';
import { ToastrService } from '@core/services/toastr.service';
import { UserService } from '@core/services/user.service';
import { forkJoin, Subject, Subscription } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';
import { EaterInfoDialogComponent } from './eater-info-dialog/eater-info-dialog.component';

import * as XLSX from 'xlsx';
import { DataService } from '@core/services/data-service';
import { environment } from 'environments/environment';
@Component({
    selector: 'app-eater-add-edit',
    templateUrl: './eater-add-edit.component.html',
    styleUrls: ['./eater-add-edit.component.scss'],
})
export class EaterAddEditComponent implements OnInit {
    form: FormGroup;
    formEater: FormGroup;
    field: any[] = [];
    fieldAddEater: any[] = [];
    currentDate: Date = new Date();
    listObjectEat: any[] = [];
    listObjectEatFilter: any[] = [];
    listOrganization: any[] = [];
    listPosition: any[] = [];
    listRank: any[] = [];
    dotQuanSoId: any;
    selected: string = 'all';

    displayedColumnsOfField: string[] = [
        'select',
        'stt',
        'tenQuanNhan',
        'capBac',
        'chucVu',
        'doiTuongAnId',
        'mucTienAn',
        'trangThai',
        'actions',
    ];
    dataSource = new MatTableDataSource<any>([]);

    applyFilter = new Subject<KeyboardEvent>();
    subscription: Subscription;
    selection = new SelectionModel<any>(true, []);

    infoListEater: any;
    detailListEater: any;
    isEdit: boolean = false;

    @ViewChild(MatPaginator) paginator!: MatPaginator;
    @ViewChild(MatTable) table: MatTable<any>;

    constructor(
        private router: Router,
        private _categoryService: CategoryService,
        private _userService: UserService,
        public dialog: MatDialog,
        private _toastrService: ToastrService,
        private commonService: CommonService,
        private _activatedroute: ActivatedRoute,
        private _dataService: DataService,
    ) {
        this.subscription = this.applyFilter
            .pipe(
                map((event) => (event.target as HTMLInputElement).value),
                debounceTime(500),
                distinctUntilChanged(),
            )
            .subscribe((filterValue) => this.handleSearch(filterValue));
    }

    /** Whether the number of selected elements matches the total number of rows. */
    isAllSelected() {
        const numSelected = this.selection.selected.length;
        const numRows = this.dataSource.data.length;
        return numSelected === numRows;
    }

    /** Selects all rows if they are not all selected; otherwise clear selection. */
    toggleAllRows() {
        if (this.isAllSelected()) {
            this.selection.clear();
            return;
        }

        this.selection.select(...this.dataSource.data);
    }

    /** The label for the checkbox on the passed row */
    checkboxLabel(row?: any): string {
        if (!row) {
            return `${this.isAllSelected() ? 'deselect' : 'select'} all`;
        }
        return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${
            row.index + 1
        }`;
    }

    ngOnInit() {
        this.initData();
        this.selection.clear();
    }

    initData() {
        this._activatedroute.paramMap.subscribe((params) => {
            this.dotQuanSoId = params.get('id');
            if (this.dotQuanSoId) {
                this.isEdit = true;
            }
        });

        let getListObjectEat = this.commonService.callDataAPIShort(
            '/api/services/read/DoiTuongAn/GetAll',
            { maxResultCount: 2147483647 },
        );
        let getListOrganization = this._userService.getOrganization({});
        let getListPosition = this._userService.getPositions({});
        let getListRank = this._categoryService.getAllCategory(
            this.getBodyCategory('cap-bac'),
        );

        forkJoin([
            getListObjectEat,
            getListOrganization,
            getListPosition,
            getListRank,
        ]).subscribe((response) => {
            this.listObjectEat = response[0].result.items.map((item) => ({
                key: item.id,
                code: item.codeData,
                name: item.name,
                mucTienAn: item.mucTienAn,
            }));

            console.log('this.listObjectEat', this.listObjectEat);

            this.listObjectEatFilter.push(
                {
                    key: 'all',
                    name: 'Tất cả',
                },
                ...this.listObjectEat,
            );

            this.listOrganization = response[1].result.map((item) => ({
                key: item.id,
                code: item.codeData,
                name: item.name,
            }));

            this.listPosition = response[2].result.map((item) => ({
                key: item.codeData,
                name: item.name,
            }));

            this.listRank = response[3].result.map((item) => ({
                key: item.codeData,
                name: item.name,
            }));

            this.field = [
                {
                    type: 'TEXT',
                    referenceValue: 'tenDot',
                    name: 'Nhập tên đợt bổ sung quân số ăn',
                    defaultValue: '',
                    required: '1',
                    css: 'col-12 col-lg-6',
                    appearance: 'legacy',
                },
                {
                    type: 'SELECT',
                    referenceValue: 'value1',
                    name: 'Chọn đơn vị',
                    defaultValue: '',
                    options: [...this.listOrganization],
                    css: 'col-12 col-lg-6',
                    appearance: 'legacy',
                    search: '1',
                    searchCtrl: 'searchOrganizationCtrl',
                    required: '1',
                },

                {
                    type: 'DATETIME',
                    referenceValue: 'tuNgay',
                    name: 'Dự kiến ăn từ ngày',
                    appearance: 'legacy',
                    defaultValue: '',
                    required: '1',
                    icon: 'filter_vintage',
                    css: 'col-12 col-lg-6',
                },
                {
                    type: 'DATETIME',
                    referenceValue: 'denNgay',
                    name: 'Đến ngày',
                    defaultValue: '',
                    dependenField: 'tuNgay',
                    required: '0',
                    appearance: 'legacy',
                    icon: 'filter_vintage',
                    css: 'col-12 col-lg-6',
                },
                {
                    type: 'RADIO',
                    referenceValue: 'trangThai',
                    name: 'Trạng thái',
                    defaultValue: true,
                    required: '0',
                    options: [
                        { key: true, name: 'Hoạt động' },
                        { key: false, name: 'Tạm ngưng' },
                    ],
                    css: 'col-12 col-lg-6',
                },
            ];
            this.initForm();

            this.fieldAddEater = [
                {
                    type: 'TEXT',
                    referenceValue: 'tenQuanNhan',
                    name: 'Nhập tên quân nhân',
                    defaultValue: '',
                    required: '1',
                    css: 'col-6 col-lg-6',
                    appearance: 'legacy',
                },
                {
                    type: 'SELECT',
                    referenceValue: 'doiTuongAnId',
                    name: 'Chọn đối tượng ăn',
                    required: '1',
                    defaultValue: '',
                    options: [...this.listObjectEat],
                    css: 'col-12 col-lg-6',
                    appearance: 'legacy',
                    search: '1',
                    searchCtrl: 'searchObjectEatCtrl',
                },
                {
                    type: 'SELECT',
                    referenceValue: 'chucVu',
                    name: 'Chọn chức vụ',
                    defaultValue: '',
                    options: [...this.listPosition],
                    css: 'col-12 col-lg-6',
                    appearance: 'legacy',
                    search: '1',
                    searchCtrl: 'searchPositionCtrl',
                    required: '0',
                },
                {
                    type: 'SELECT',
                    referenceValue: 'capBac',
                    name: 'Chọn cấp bậc',
                    defaultValue: '',
                    options: [...this.listRank],
                    css: 'col-12 col-lg-6',
                    appearance: 'legacy',
                    search: '1',
                    searchCtrl: 'searchRankCtrl',
                    required: '0',
                },
            ];
            this.initFormEater();
        });
    }

    getDataEdit(id: any) {
        let request = {
            maxResultCount: 99999999,
            criterias: [
                {
                    propertyName: 'dotQuanSoAnId',
                    operation: 'Equals',
                    value: id,
                },
            ],
        };
        let info = this.commonService.callDataAPIShort(
            '/api/services/read/DotQuanSoAn/Get',
            { id: id },
        );
        let detail = this.commonService.callDataAPIShort(
            '/api/services/read/ChiTietDotQuanSoAn/GetAll',
            request,
        );

        forkJoin([info, detail]).subscribe((response) => {
            this.infoListEater = response[0].result;
            this.detailListEater = response[1].result.items;
            this.dataSource.data = this.detailListEater;
            this.dataSource = new MatTableDataSource<any>(this.dataSource.data);
            this.dataSource.paginator = this.paginator;
            this.form.patchValue(this.infoListEater);
        });
    }

    getBodyCategory(groupCode: string) {
        return {
            maxResultCount: 10,
            skipCount: 0,
            sorting: 'Code',
            criterias: [
                {
                    propertyName: 'GroupCode',
                    operation: 6,
                    value: groupCode,
                },
            ],
        };
    }

    initForm() {
        let fieldsCtrls = {};
        let fieldList = [];

        for (let f of this.field) {
            fieldList = [...fieldList, f];

            if (f.type === 'NUMBER') {
                fieldsCtrls[f.referenceValue] = new FormControl(
                    { value: f.defaultValue, disabled: f.disabled },
                    f.required === '1' ? [Validators.required] : [],
                );
            } else if (f.type !== 'CHECKBOX') {
                let validators = [
                    Validators.minLength(f.minLength),
                    Validators.maxLength(f.maxLength),
                    Validators.pattern(f.pattern),
                ];

                if (f.type === 'EMAIL') {
                    validators = [...validators, Validators.email];
                }

                fieldsCtrls[f.referenceValue] = new FormControl(
                    {
                        value: f.defaultValue || '',
                        disabled: f.disabled,
                    },

                    f.required === '1'
                        ? [...validators, Validators.required]
                        : [...validators],
                );

                if (f.type === 'DATETIME') {
                    fieldsCtrls[f.referenceValue].valueChanges.subscribe(
                        (val) => {
                            const dependField = fieldList.find(
                                (x) => x.dependenField === f.referenceValue,
                            );

                            if (!dependField) return;

                            dependField.min = val;
                        },
                    );
                }
            } else {
                //if checkbox, it need multiple
                let opts = {};
                for (let opt of f.options) {
                    opts[opt.key] = new FormControl({
                        value: opt.value,
                        disabled: f.disabled,
                    });
                }
            }
        }

        this.form = new FormGroup(fieldsCtrls);

        if (this.isEdit === true) {
            this.getDataEdit(this.dotQuanSoId);
        }

        console.log('this.form', this.form);
    }

    initFormEater() {
        let fieldsCtrls = {};
        let fieldList = [];

        for (let f of this.fieldAddEater) {
            fieldList = [...fieldList, f];

            if (f.type === 'NUMBER') {
                fieldsCtrls[f.referenceValue] = new FormControl(
                    { value: f.defaultValue, disabled: f.disabled },
                    f.required === '1' ? [Validators.required] : [],
                );
            } else if (f.type !== 'CHECKBOX') {
                let validators = [
                    Validators.minLength(f.minLength),
                    Validators.maxLength(f.maxLength),
                    Validators.pattern(f.pattern),
                ];

                if (f.type === 'EMAIL') {
                    validators = [...validators, Validators.email];
                }

                fieldsCtrls[f.referenceValue] = new FormControl(
                    {
                        value: f.defaultValue || '',
                        disabled: f.disabled,
                    },

                    f.required === '1'
                        ? [...validators, Validators.required]
                        : [...validators],
                );

                if (f.type === 'DATETIME') {
                    fieldsCtrls[f.referenceValue].valueChanges.subscribe(
                        (val) => {
                            const dependField = fieldList.find(
                                (x) => x.dependenField === f.referenceValue,
                            );

                            if (!dependField) return;

                            dependField.min = val;
                        },
                    );
                }
            } else {
                //if checkbox, it need multiple
                let opts = {};
                for (let opt of f.options) {
                    opts[opt.key] = new FormControl({
                        value: opt.value,
                        disabled: f.disabled,
                    });
                }
            }
        }

        this.formEater = new FormGroup(fieldsCtrls);
    }

    getNameById(id: any, type: any) {
        let data;
        let dataList = [];

        if (
            this.listObjectEat.length > 0 &&
            this.listOrganization.length > 0 &&
            this.listPosition.length > 0 &&
            this.listRank.length > 0
        ) {
            switch (type) {
                case 'Eat':
                    dataList = this.listObjectEat;
                    break;
                case 'Organization':
                    dataList = this.listOrganization;
                    break;
                case 'Position':
                    dataList = this.listPosition;
                    break;
                case 'Rank':
                    dataList = this.listRank;
                    break;
            }
            let item = dataList.find((item) => item.key === id);
            data = item && item.name ? item.name : '';
        }

        return data;
    }

    getPrieById(id: any) {
        let data;
        if (this.listObjectEat.length > 0) {
            const item = this.listObjectEat.find((item) => item.key === id);
            data = item && item.mucTienAn ? item.mucTienAn : '';
        }

        return data;
    }

    convertCodeToId(code: any, type: any) {
        let dataCode;
        let dataList = [];

        if (this.listObjectEat.length > 0 && this.listOrganization.length > 0) {
            switch (type) {
                case 'Eat':
                    dataList = this.listObjectEat;
                    break;
                case 'Organization':
                    dataList = this.listOrganization;
                    break;
            }
            let item = dataList.find((item) => item.code === code);
            dataCode = item && item.key ? item.key : '';
        }

        return dataCode;
    }

    handleSearch(dataSearch: any) {
        this.dataSource.filter = dataSearch.toLowerCase();
    }

    handleSelectFilter(dataSelected: any) {
        if (dataSelected.value === 'all') {
            this.dataSource.filter = '';

            if (this.dataSource.paginator) {
                this.dataSource.paginator.firstPage();
            }
        } else {
            this.dataSource.filter = dataSelected.value;
        }
    }

    goBack() {
        this.router.navigateByUrl('manage/app/category/quan-ly-quan-so-an');
    }

    onSubmit() {
        let dataRequest = {
            ...this.infoListEater,
            ...this.form.value,
            dauMoiBepId: this._dataService.getDauMoiBepId(),
            chiTiet: [...this.dataSource.data],
        };

        if (this.isEdit) {
            this.commonService
                .callDataAPIShort(
                    '/api/services/write/DotQuanSoAn/Update',
                    dataRequest,
                )
                .subscribe(
                    (response) => {
                        if (response.success) {
                            this.router.navigateByUrl(
                                'manage/app/category/quan-ly-quan-so-an',
                            );
                            this._toastrService.success(
                                '',
                                'Cập nhật thành công',
                            );
                        }
                    },
                    (err) => this._toastrService.errorServer(err),
                );
        } else {
            this.commonService
                .callDataAPIShort(
                    '/api/services/write/DotQuanSoAn/Create',
                    dataRequest,
                )
                .subscribe(
                    (response) => {
                        if (response.success) {
                            this.router.navigateByUrl(
                                'manage/app/category/quan-ly-quan-so-an',
                            );
                            this._toastrService.success(
                                '',
                                'Tạo mới thành công',
                            );
                        }
                    },
                    (err) => this._toastrService.errorServer(err),
                );
        }
    }

    onFileSelected(event: any) {
        /* wire up file reader */
        console.log('event::', event);
        const target: DataTransfer = <DataTransfer>event.target;
        if (target.files.length !== 1) {
            throw new Error('Cannot use multiple files');
        }
        const reader: FileReader = new FileReader();
        reader.readAsBinaryString(target.files[0]);
        reader.onload = (e: any) => {
            /* create workbook */
            const binarystr: string = e.target.result;
            const wb: XLSX.WorkBook = XLSX.read(binarystr, { type: 'binary' });

            /* selected the first sheet */
            const wsname: string = wb.SheetNames[0];
            const ws: XLSX.WorkSheet = wb.Sheets[wsname];

            /* save data */
            const data = XLSX.utils.sheet_to_json(ws); // to get 2d array pass 2nd parameter as object {header: 1}

            let listDataFromFile = [];
            listDataFromFile = data.map((item) => ({
                capBac: item['Mã cấp bậc'],
                chucVu: item['Mã chức vụ'],
                doiTuongAnId: this.convertCodeToId(
                    item['Mã đối tượng ăn'],
                    'Eat',
                ),
                id: this.commonService.guid(),
                organizationId: this.form.value.value1,
                tenQuanNhan: item['Họ và tên'],
                trangThai: true,
            }));

            console.log('listDataFromFile', listDataFromFile);

            if (this.dataSource.data.length === 0) {
                this.dataSource.data = listDataFromFile;
                this.dataSource = new MatTableDataSource<any>(
                    this.dataSource.data,
                );

                this.dataSource.paginator = this.paginator;
            } else {
                this.dataSource.data =
                    this.dataSource.data.concat(listDataFromFile);
                this.dataSource = new MatTableDataSource<any>(
                    this.dataSource.data,
                );

                this.dataSource.paginator = this.paginator;
            }

            console.log('this.dataSource.data ::', this.dataSource.data);
        };
    }

    addEater() {
        let dataEater = {
            ...this.formEater.value,
            organizationId: this.form.value.value1,
            trangThai: true,
            id: this.commonService.guid(),
        };
        this.dataSource.data.push(dataEater);
        this.dataSource = new MatTableDataSource<any>(this.dataSource.data);

        this.dataSource.paginator = this.paginator;
        this.formEater.reset();
    }

    editEater(dataEdit: any, index: number) {
        let fields = [
            {
                type: 'TEXT',
                referenceValue: 'tenQuanNhan',
                name: 'Nhập tên quân nhân',
                defaultValue: '',
                required: '0',
                css: 'col-12 col-lg-12',
                appearance: 'legacy',
            },
            {
                type: 'SELECT',
                referenceValue: 'organizationId',
                name: 'Chọn đơn vị',
                defaultValue: '',
                options: [...this.listOrganization],
                css: 'col-12 col-lg-12',
                appearance: 'legacy',
                search: '1',
                searchCtrl: 'searchOrganizationCtrl',
                required: '0',
            },
            {
                type: 'SELECT',
                referenceValue: 'chucVu',
                name: 'Chọn chức vụ',
                defaultValue: '',
                options: [...this.listPosition],
                css: 'col-12 col-lg-12',
                appearance: 'legacy',
                search: '1',
                searchCtrl: 'searchPositionCtrl',
                required: '0',
            },
            {
                type: 'SELECT',
                referenceValue: 'capBac',
                name: 'Chọn cấp bậc',
                defaultValue: '',
                options: [...this.listRank],
                css: 'col-12 col-lg-12',
                appearance: 'legacy',
                search: '1',
                searchCtrl: 'searchRankCtrl',
                required: '0',
            },
            {
                type: 'SELECT',
                referenceValue: 'doiTuongAnId',
                name: 'Chọn đối tượng ăn',
                required: '0',
                defaultValue: '',
                options: [...this.listObjectEat],
                css: 'col-12 col-lg-12',
                appearance: 'legacy',
                search: '1',
                searchCtrl: 'searchObjectEatCtrl',
            },
            {
                type: 'RADIO',
                referenceValue: 'trangThai',
                name: 'Trạng thái',
                defaultValue: dataEdit.trangThai,
                options: [
                    { key: true, name: 'Hoạt động' },
                    { key: false, name: 'Tạm ngưng' },
                ],
                css: 'col-12 col-lg-12',
            },
        ];
        let dialogRef = this.dialog.open(EaterInfoDialogComponent, {
            width: '30%',
            data: {
                datas: dataEdit,
                fields: fields,
            },
            panelClass: 'my-custom-dialog-class',
        });

        dialogRef.componentInstance.onSave.subscribe((formValues) => {
            console.log('formValues::', formValues);
            const isSubmitted = !!formValues;
            let currenIndex = this.dataSource.data.findIndex(
                (item) => item.id === formValues.id,
            );
            this.dataSource.data[currenIndex] = { ...formValues };
            this.dataSource = new MatTableDataSource<any>(this.dataSource.data);

            this.dataSource.paginator = this.paginator;
            this.table.renderRows();

            dialogRef.close(isSubmitted);
        });

        dialogRef.afterClosed().subscribe((result) => {
            if (!result) return;
            this._toastrService.success('', 'Cập nhật thành công');
        });
    }

    deleteEater(dateDelete: any) {
        const dialogRef = this.dialog.open(DeleteConfirmDialogComponent, {
            width: '500px',
            data: dateDelete,
        });

        dialogRef.componentInstance.handleDelete.subscribe((dataDelete) => {
            const isSubmitted = !!dataDelete;

            this.dataSource.data = this.dataSource.data.filter(
                (person) => person.id != dateDelete.id,
            );
            dialogRef.close(isSubmitted);
        });

        dialogRef.afterClosed().subscribe((result) => {
            if (!result) return;
            this._toastrService.success('', 'Xóa thành công');
        });
    }

    deleteList() {
        const dialogRef = this.dialog.open(DeleteConfirmDialogComponent, {
            width: '500px',
            data: this.selection.selected,
        });

        dialogRef.componentInstance.handleDelete.subscribe((dataDelete) => {
            const isSubmitted = !!dataDelete;

            for (let item of this.selection.selected) {
                this.dataSource.data = this.dataSource.data.filter(
                    (person) => person.id != item.id,
                );
            }

            dialogRef.close(isSubmitted);
        });

        dialogRef.afterClosed().subscribe((result) => {
            if (!result) return;
            this.selection.clear();
            this._toastrService.success('', 'Xóa thành công');
        });
    }

    downloadExample() {
        let base_URL = environment.DATA_URL;

        if (base_URL.includes('data1')) {
            let url = `${base_URL}/api/File/GetFile?key=93d744c9-e9f8-4fe6-8254-e47aa2524202-b06d09f6-7ee5-468a-a816-e5a719d67032-Mẫu excel nhập quân số ăn.xlsx`;
            window.open(url, '_blank');
        } else {
            let url = `${base_URL}/api/File/GetFile?key=63ace7d5-ad2d-4422-be1f-ddc12c7cfad1-8d951a82-a5ab-4290-9b92-59f5ec343c5a-Mẫu excel nhập quân số ăn.xlsx`;
            window.open(url, '_blank');
        }
    }
}
