import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageBlankComponent } from './page-blank/page-blank.component';

const routes: Routes = [
    {
        path: '',
        component: PageBlankComponent,
        children: [
            {
                path: '',
                redirectTo: 'inbox',
                pathMatch: 'full',
            },
            {
                path: 'inbox',
                component: PageBlankComponent,
            },
            {
                path: ':valueData',
                component: PageBlankComponent,
            },
        ],
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class PageDynamicCategoryRoutingModule {}
