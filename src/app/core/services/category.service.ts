import { Observable } from 'rxjs';
import { Injectable, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { LogData } from '../interfaces/log-data';
import { UtilsService } from './utils.service';
import { tap } from 'rxjs/operators';
import { TokenStorageService } from './token.service';
import { environment } from 'environments/environment';

const TOKEN_KEY = 'auth-token';
const USER_KEY = 'auth-user';

@Injectable()
export class CategoryService implements OnInit {
    BASE_URL = `${environment.SWAGGER_URL}/api/services/`;
    ENDPOINT = 'read/Category/GetList';
    ENDPOINT_FORM = 'read/Form/GetList';

    constructor(
        private http: HttpClient,
        private utils: UtilsService,
        private _tokenStorageService: TokenStorageService,
    ) {}

    ngOnInit(): void {}

    getAllCategory(data: any): Observable<any> {
        let token = this._tokenStorageService.getToken();
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
        });

        let options = { headers: headers };

        const END_POINT = 'read/Category/GetList';

        return this.http.post<any>(
            `${this.BASE_URL}${END_POINT}`,
            data,
            options,
        );
    }

    createCategory(data: any): Observable<any> {
        let token = this._tokenStorageService.getToken();
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
        });

        let options = { headers: headers };

        const END_POINT = 'write/Category/Create';

        return this.http.post<any>(
            `${this.BASE_URL}${END_POINT}`,
            data,
            options,
        );
    }

    updateCategory(data: any): Observable<any> {
        let token = this._tokenStorageService.getToken();
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
        });

        let options = { headers: headers };

        const END_POINT = 'write/Category/Update';

        return this.http.post<any>(
            `${this.BASE_URL}${END_POINT}`,
            data,
            options,
        );
    }

    deleteCategory(id: any): Observable<any> {
        let token = this._tokenStorageService.getToken();
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
        });

        let options = { headers: headers };

        const END_POINT = 'write/Category/Delete';

        return this.http.post<any>(`${this.BASE_URL}${END_POINT}`, id, options);
    }

    getAllForm(data: any): Observable<any> {
        let token = this._tokenStorageService.getToken();
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
        });

        let options = { headers: headers };
        return this.http.post<any>(
            `${this.BASE_URL}${this.ENDPOINT_FORM}`,
            data,
            options,
        );
    }

    createForm(data: any): Observable<any> {
        let token = this._tokenStorageService.getToken();
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
        });
        let options = { headers: headers };
        const END_POINT = 'write/Form/Create';

        return this.http.post<any>(
            `${this.BASE_URL}${END_POINT}`,
            data,
            options,
        );
    }

    getFormByID(id: any): Observable<any> {
        let token = this._tokenStorageService.getToken();
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
        });
        let options = { headers: headers };
        const END_POINT = 'read/Form/Get';

        return this.http.post<any>(`${this.BASE_URL}${END_POINT}`, id, options);
    }

    updateForm(data: any): Observable<any> {
        let token = this._tokenStorageService.getToken();
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
        });

        let options = { headers: headers };

        const END_POINT = 'write/Form/Update';

        return this.http.post<any>(
            `${this.BASE_URL}${END_POINT}`,
            data,
            options,
        );
    }

    deleteForm(id: any): Observable<any> {
        let token = this._tokenStorageService.getToken();
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
        });
        let options = { headers: headers };
        const END_POINT = 'write/Form/Delete';

        return this.http.post<any>(`${this.BASE_URL}${END_POINT}`, id, options);
    }

    searchFieldFormName(data: any): Observable<any> {
        let token = this._tokenStorageService.getToken();
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
        });
        let options = { headers: headers };
        const END_POINT = 'read/Field/GetList';

        return this.http.post<any>(
            `${this.BASE_URL}${END_POINT}`,
            data,
            options,
        );
    }
}
