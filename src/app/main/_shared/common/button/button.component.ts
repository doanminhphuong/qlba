import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

export interface Button {
    type: string;
    matType: string;
    title: string;
    color: string;
    iconLeft: string;
    iconRight: string;
    isOnDelete?: boolean;
    isDisabled?: boolean;
    isNoneData?: boolean;
    onClick(): void;
}

@Component({
    selector: 'app-button',
    templateUrl: './button.component.html',
    styleUrls: ['./button.component.scss'],
})
export class ButtonComponent implements OnInit {
    @Input() public type: string;
    @Input() public matType: string;
    @Input() public title: string;
    @Input() public color: string;
    @Input() public matIcon: string;
    @Input() public iconLeft: string;
    @Input() public iconRight: string;
    @Input() public isDisabled: boolean;
    @Input() public colorBtn: string;
    @Output() public handleClick = new EventEmitter();

    constructor() {}

    ngOnInit() {}
}
