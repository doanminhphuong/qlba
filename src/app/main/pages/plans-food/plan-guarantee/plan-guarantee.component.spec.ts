import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanGuaranteeComponent } from './plan-guarantee.component';

describe('PlanGuaranteeComponent', () => {
  let component: PlanGuaranteeComponent;
  let fixture: ComponentFixture<PlanGuaranteeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlanGuaranteeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanGuaranteeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
