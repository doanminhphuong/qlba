import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogRejectStatusComponent } from './dialog-reject-status.component';

describe('DialogRejectStatusComponent', () => {
  let component: DialogRejectStatusComponent;
  let fixture: ComponentFixture<DialogRejectStatusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogRejectStatusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogRejectStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
