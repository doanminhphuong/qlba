import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
    selector: 'fb-email',
    template: `
        <div [formGroup]="form">
            <ng-container
                *ngIf="!isFlatForm && tabCtrl"
                [formGroupName]="tabCtrl"
            >
                <ng-container [formGroupName]="groupCtrl">
                    <mat-form-field
                        [appearance]="field.appearance"
                    >
                        <mat-label>{{ field.name }}</mat-label>
                        <input
                            matInput
                            type="email"
                            [placeholder]="getPlaceholder()"
                            [value]="field.defaultValue"
                            [id]="field.referenceValue"
                            [name]="field.referenceValue"
                            [formControlName]="getFormControlName()"
                            [required]="field.required === '1' ? true : false"
                            spellcheck="false"
                        />
                        <mat-icon matSuffix>{{ field.icon }}</mat-icon>
                        <!-- VALIDATORS -->
                        <mat-error>{{ getErrorMessage() }}</mat-error>
                    </mat-form-field>
                </ng-container>
            </ng-container>

            <mat-form-field
                *ngIf="isFlatForm || !tabCtrl"
                [appearance]="field.appearance"
            >
                <mat-label>{{ field.name }}</mat-label>
                <input
                    matInput
                    type="email"
                    [placeholder]="getPlaceholder()"
                    [value]="field.defaultValue"
                    [id]="field.referenceValue"
                    [name]="field.referenceValue"
                    [formControlName]="getFormControlName()"
                    [required]="field.required === '1' ? true : false"
                    spellcheck="false"
                />
                <mat-icon matSuffix>{{ field.icon }}</mat-icon>
                <!-- VALIDATORS -->
                <mat-error>{{ getErrorMessage() }}</mat-error>
            </mat-form-field>
        </div>
    `,
    styles: [
        `
            mat-form-field {
                width: 100%;
            }
        `,
    ],
})
export class EmailComponent implements OnInit {
    @Input() field: any = {};
    @Input() form: FormGroup;
    @Input() tabCtrl: any;
    @Input() groupCtrl: any;
    @Input() isFlatForm: boolean;

    get fieldName() {
        if (!this.isFlatForm && this.tabCtrl) {
            return this.form
                .get(this.tabCtrl)
                .get(this.groupCtrl)
                .get(this.field.referenceValue);
        } else if (this.isFlatForm && this.tabCtrl) {
            return this.form.controls[
                `${this.tabCtrl}.${this.groupCtrl}.${this.field.referenceValue}`
            ];
        } else {
            return this.form.controls[this.field.referenceValue];
        }
    }

    get isValid() {
        return this.fieldName.valid;
    }
    get isDirty() {
        return this.fieldName.dirty;
    }
    get isTouched() {
        return this.fieldName.touched;
    }

    constructor() { }

    ngOnInit() { }

    getPlaceholder() {
        return `Nhập vào ${this.field.name.toLowerCase()}`;
    }

    getFormControlName() {
        if (this.isFlatForm) {
            return `${this.tabCtrl}.${this.groupCtrl}.${this.field.referenceValue}`;
        } else {
            return this.field.referenceValue;
        }
    }

    getErrorMessage() {
        let errorMessage = '';

        if (this.fieldName.hasError('required')) {
            errorMessage = `${this.field.name} không được để trống.`;
        } else if (this.fieldName.hasError('email')) {
            errorMessage = `${this.field.name} không hợp lệ.`;
        }

        return errorMessage;
    }
}
