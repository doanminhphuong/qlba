import { Component } from '@angular/core';

@Component({
    selector: 'ngx-rice-dipping',
    // styleUrls: ['pages.component.scss'],
    template: `
        <div>
            <router-outlet></router-outlet>
        </div>
    `,
})
class RiceDippingComponent {
    //   menu = MENU_ITEMS;
}
