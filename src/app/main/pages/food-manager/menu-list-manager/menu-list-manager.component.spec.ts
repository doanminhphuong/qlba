import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuListManagerComponent } from './menu-list-manager.component';

describe('MenuListManagerComponent', () => {
  let component: MenuListManagerComponent;
  let fixture: ComponentFixture<MenuListManagerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenuListManagerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuListManagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
