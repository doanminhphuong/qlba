export const BASE_URL = `${environment.SWAGGER_URL}/api/services`;

import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { BehaviorSubject, forkJoin, Observable, Subject } from 'rxjs';
import { CommonService } from './common.service';
import { HoSoSucKhoeService } from './ho-so-suc-khoe.service';
@Injectable({
    providedIn: 'root',
})
export class DataService {
    private dauMoiBepId: number;

    tenBep: string;

    donViQuanLy: any;
    donViCapTrenTrucTiep: any;

    private thuKho: string;
    private troLyHauCan: string;
    private nguoiVietPhieu: string;
    private nguoiNhan: string;
    private nguoiDuyet: string;
    private quanLy: string;
    private bepTruong: string;
    private nguoiLapBaoCao: string;
    private trucBan: string;
    private thuTruongDonVi: string;
    private truongBanTaiChinh: string;

    public subjectBepID = new Subject<any>();
    public behaviorSubjectBepID$: BehaviorSubject<behaviorSubjectBepID> =
        new BehaviorSubject<behaviorSubjectBepID>({ id: null });

    constructor(
        private commonService: CommonService,
        private hsskService: HoSoSucKhoeService,
    ) {}

    // getOrganization

    saveDataBep(dataBep: any) {
        this.dauMoiBepId = dataBep.id;
        this.tenBep = dataBep.tenBep;
        this.thuKho = dataBep.thuKho;
        this.troLyHauCan = dataBep.troLyHauCan;
        this.nguoiVietPhieu = dataBep.nguoiVietPhieu;
        this.nguoiNhan = dataBep.nguoiNhan;
        this.nguoiDuyet = dataBep.nguoiDuyet;
        this.quanLy = dataBep.quanLy;
        this.bepTruong = dataBep.bepTruong;
        this.nguoiLapBaoCao = dataBep.nguoiLapBaoCao;
        this.trucBan = dataBep.trucBan;
        this.thuTruongDonVi = dataBep.thuTruongDonVi;
        this.truongBanTaiChinh = dataBep.truongBanTaiChinh;

        const dataDonViQuanLy = this.hsskService.getOrganization({
            id: dataBep.donViQuanLyId,
        });

        const dataDonViCapTren = this.hsskService.getOrganization({
            id: dataBep.donViCapTrenTrucTiepId,
        });

        forkJoin([dataDonViQuanLy, dataDonViCapTren]).subscribe((data) => {
            this.donViQuanLy = data[0].result;

            this.donViCapTrenTrucTiep = data[1].result;
        });
    }

    compactWhitespace = (str) => str.replace(/\s{2,}/g, ' ');

    sendData(message: string) {
        this.subjectBepID.next({ id: message });
        this.behaviorSubjectBepID$.next({ id: message });
    }

    clearData() {
        this.subjectBepID.next();
    }

    getData(): Observable<any> {
        return this.subjectBepID.asObservable();
    }

    getDauMoiBepId() {
        return this.dauMoiBepId;
    }

    changeDauMoiBep(id: any) {
        return (this.dauMoiBepId = id);
    }

    getDonViThuocQuanLy() {
        return this.donViQuanLy;
    }

    getTenBep() {
        return this.tenBep;
    }

    getTenDonViCapTrenTrucTiep() {
        return this.donViCapTrenTrucTiep;
    }

    getQuanLyBep() {
        return this.quanLy;
    }

    getTenThuTruongKyDuyet() {
        return this.thuTruongDonVi;
    }

    getThuKho() {
        return this.thuKho;
    }

    getTroLyHauCan() {
        return this.troLyHauCan;
    }

    getNguoiVietPhieu() {
        return this.nguoiVietPhieu;
    }

    getNguoiNhan() {
        return this.nguoiNhan;
    }

    getNguoiDuyet() {
        return this.nguoiDuyet;
    }

    getBepTruong() {
        return this.bepTruong;
    }

    getNguoiLapBaoCao() {
        return this.nguoiLapBaoCao;
    }

    getTrucBan() {
        return this.trucBan;
    }

    getTruongBanTaiChinh() {
        return this.truongBanTaiChinh;
    }
}

type behaviorSubjectBepID = {
    id: number | string;
};
