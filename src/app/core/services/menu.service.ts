import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { LogData } from '../interfaces/log-data';
import { UtilsService } from './utils.service';
import { tap } from 'rxjs/operators';
import { TokenStorageService } from './token.service';
import { environment } from 'environments/environment';

@Injectable()
export class MenuService {
    API_URL: any = `${environment.SYS_URL}/api`;
    API_ENDPOINT: any = '/services/SYS/read/Menu/GetList';

    constructor(
        private http: HttpClient,
        private utils: UtilsService,
        private _tokenStorageService: TokenStorageService,
    ) {}

    getData(params?: any): Observable<any> {
        let url = this.API_URL + this.API_ENDPOINT;
        if (params) {
            url += '?' + this.utils.urlParam(params);
        }
        return this.http.get(url).pipe(tap((message: LogData[]) => {}));
    }

    getMenus(menuData: any): Observable<any> {
        let token = this._tokenStorageService.getToken();
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
        });

        let options = { headers: headers };

        let url = this.API_URL + this.API_ENDPOINT;
        return this.http.post(url, menuData, options);
    }

    checkPermission(data: any): Observable<any> {
        let token = this._tokenStorageService.getToken();
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
        });

        let options = { headers: headers };

        let url =
            this.API_URL + '/services/SYS/read/MenuClient/CheckPermission';
        return this.http.post(url, data, options);
    }
}
