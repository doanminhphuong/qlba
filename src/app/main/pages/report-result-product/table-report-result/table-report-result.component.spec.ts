import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableReportResultComponent } from './table-report-result.component';

describe('TableReportResultComponent', () => {
  let component: TableReportResultComponent;
  let fixture: ComponentFixture<TableReportResultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableReportResultComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableReportResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
