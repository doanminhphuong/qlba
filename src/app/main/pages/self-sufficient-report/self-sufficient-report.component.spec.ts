import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { SelfSufficientReportComponent } from './self-sufficient-report.component';

describe('SelfSufficientReportComponent', () => {
    let component: SelfSufficientReportComponent;
    let fixture: ComponentFixture<SelfSufficientReportComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [SelfSufficientReportComponent],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(SelfSufficientReportComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
