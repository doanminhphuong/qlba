import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatPaginator, MatTableDataSource } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { CommonService } from '@core/services/common.service';
import { DataService } from '@core/services/data-service';
import { ExportExcelService } from '@core/services/export-excel.service';
import { InventoryService } from '@core/services/inventory.service';
import { ToastrService } from '@core/services/toastr.service';
import { UserService } from '@core/services/user.service';
import { forkJoin, Subject, Subscription } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';

@Component({
  selector: 'app-pure-add-edit',
  templateUrl: './pure-add-edit.component.html',
  styleUrls: ['./pure-add-edit.component.scss']
})
export class PureAddEditComponent implements OnInit {
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  form: FormGroup;
  field: any[] = [];
  warehouseList: any[] = [];
  public applyFilter = new Subject<KeyboardEvent>();
  private subscription: Subscription;
  dataSource = new MatTableDataSource<any>([]);
  idTinhKho: string;
  dataDetail: any;
  dataInfo: any;
  isPrinter: boolean = false;
  isEdit: boolean = false;


  displayedColumns: string[] = [
    'tenDccd',
    'donViTinh',
    'phanHang',
    'soLuong',
    'soLuongThucTe',
    'soLuongThua',
    'soLuongThieu',
    'ghiChu',
  ];

  userId: number | string;
  khoNhanId: number;
  dauMoiBepId: number;

  constructor(
    private _router: Router,
    private _userService: UserService,
    public dialog: MatDialog,
    private _toastrService: ToastrService,
    private _commonService: CommonService,
    private _activatedroute: ActivatedRoute,
    private _exportService: ExportExcelService,
    private _dataService: DataService,
  ) {
    this.subscription = this.applyFilter
      .pipe(
        map((event) => (event.target as HTMLInputElement).value),
        debounceTime(500),
        distinctUntilChanged(),
      )
      .subscribe((filterValue) => this.handleInputFilter(filterValue));
  }

  ngOnInit() {
    this._activatedroute.paramMap.subscribe((params) => {
      this.idTinhKho = params.get('id');
    });

    this.dauMoiBepId = this._dataService.getDauMoiBepId();
    this.getInitData();
  }

  getInitData() {
    const dataKhoDccd = this._commonService
      .callDataAPIShort('/api/services/read/KhoDccd/GetAll', {
        maxResultCount: 9999,
        // sorting: 'Id',
        criterias: [
          {
            propertyName: 'dauMoiBepId',
            operation: 0,
            value: this.dauMoiBepId,
          }
        ],
      })

    const dataCurrentUser = this._userService.getCurrentUsers();

    forkJoin([dataKhoDccd, dataCurrentUser]).subscribe((results) => {
      // Get khoId
      const currentKhoDccd = results[0].result.items[0];
      this.khoNhanId = currentKhoDccd['id'];

      // Get nguoiLapId
      const currentUser = results[1].result;
      this.userId = currentUser['id'];

      this.getInitForm();

      if (!this.idTinhKho) {
        this.showListDccdOfInventory(this.khoNhanId);
      }
    });
  }

  getInitForm() {
    this.field = [
      {
        type: 'TEXT',
        referenceValue: 'tenBienBan',
        name: 'Nhập tên biên bản',
        defaultValue: '',
        required: '1',
        css: 'col-12 col-lg-4',
        appearance: 'legacy',
        disabled: this.idTinhKho ? true : false,
      },
      {
        type: 'DATETIME',
        referenceValue: 'ngayTao',
        name: 'Ngày tịnh kho',
        defaultValue: '',
        required: '1',
        css: 'col-12 col-lg-4',
        appearance: 'legacy',
        disabled: this.idTinhKho ? true : false,
      },
    ];

    let fieldsCtrls = {};
    let fieldList = [];

    for (let f of this.field) {
      fieldList = [...fieldList, f];

      if (f.type === 'NUMBER') {
        fieldsCtrls[f.referenceValue] = new FormControl(
          { value: f.defaultValue, disabled: f.disabled },
          f.required === '1' ? [Validators.required] : [],
        );
      } else if (f.type !== 'CHECKBOX') {
        let validators = [
          Validators.minLength(f.minLength),
          Validators.maxLength(f.maxLength),
          Validators.pattern(f.pattern),
        ];

        if (f.type === 'EMAIL') {
          validators = [...validators, Validators.email];
        }

        fieldsCtrls[f.referenceValue] = new FormControl(
          {
            value: f.defaultValue || '',
            disabled: f.disabled,
          },

          f.required === '1'
            ? [...validators, Validators.required]
            : [...validators],
        );

        if (f.type === 'DATETIME') {
          fieldsCtrls[f.referenceValue].valueChanges.subscribe(
            (val) => {
              const dependField = fieldList.find(
                (x) => x.dependenField === f.referenceValue,
              );

              if (!dependField) return;

              dependField.min = val;
            },
          );
        }
      } else {
        //if checkbox, it need multiple
        let opts = {};
        for (let opt of f.options) {
          opts[opt.key] = new FormControl({
            value: opt.value,
            disabled: f.disabled,
          });
        }
      }
    }

    this.form = new FormGroup(fieldsCtrls);

    if (this.idTinhKho) {
      this.getDataDetail();
    }
  }

  getDataDetail() {
    this.isEdit = true;
    this._commonService
      .callDataAPIShort('/api/services/read/BienBanTinhKhoDccd/Get', {
        id: this.idTinhKho,
      })
      .subscribe((response) => {
        console.log('dataGetById:: ', response.result);
        this.dataInfo = response.result;
        this.dataDetail = response.result.chiTiet;
        console.log('chiTiet:: ', this.dataDetail);
        this.form.patchValue(this.dataInfo);
        this.dataSource = new MatTableDataSource<any>(this.dataDetail);
        this.dataSource.paginator = this.paginator;
      });
  }

  showListDccdOfInventory(khoId: number) {
    let request = {
      maxResultCount: 9999999,
      criterias: [
        {
          propertyName: 'khoId',
          operation: 0,
          value: khoId,
        },
      ],
    };

    this._commonService
      .callDataAPIShort('/api/services/read/TonKhoDccd/GetAll', request)
      .subscribe((result) => {
        let dataDetail = result.result.items.map((item) => ({
          tenDccd: item.tenDccd,
          donViTinh: item.donViTinh,
          phanHang: item.phanHang,
          soLuong: item.ton,
          soLuongThua: 0,
          soLuongThieu: 0,
          ghiChu: '',
          tonKhoDccdId: item.id,
        }));

        this.dataSource.data = dataDetail;
        this.dataSource.paginator = this.paginator;
      });
  }

  onBack() {
    this._router.navigateByUrl('manage/app/category/tinh-kho-dccd');
  }

  handleInputFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  onSubmit(buttonType: string) {
    let isError = false;

    let chiTiet = this.dataSource.data.map((item) => ({
      ...item,

      soLuongThua:
        item.soLuongThucTe > 0 && item.soLuongThucTe > item.soLuong
          ? item.soLuongThucTe - item.soLuong
          : 0,

      soLuongThieu:
        item.soLuongThucTe > 0 && item.soLuongThucTe < item.soLuong
          ? item.soLuong - item.soLuongThucTe
          : 0,
    }));

    for (const obj of chiTiet) {
      if (
        (!obj.soLuongThucTe && obj.soLuongThucTe !== 0) ||
        !Number.isInteger(obj.soLuongThucTe)
      ) {
        isError = true;
        this._toastrService.error('', 'Số lượng thực tế chưa nhập đủ');
        break;
      }

      if (isError) {
        return false;
      }
    }

    if (isError === false) {
      let dataSubmit = {
        ...this.form.value,
        khoId: this.khoNhanId,
        nguoiLapId: this.userId,
        chiTiet,
      };

      console.log('dataSubmit:: ', dataSubmit);

      this._commonService
        .callDataAPIShort(
          '/api/services/write/BienBanTinhKhoDccd/Create',
          dataSubmit,
        )
        .subscribe(
          (response) => {
            if (response.success) {
              this._router.navigate([
                'manage/app/category/tinh-kho-dccd',
              ]);
              this._toastrService.success('', 'Tạo thành công');

              if (buttonType === 'print') {
                this.print('printer');
              }
            }
          },
          (err) => this._toastrService.errorServer(err),
        );
    }
  }

  print(tableId: string) {
    let printContents, popupWin;
    printContents = document.getElementById(tableId).innerHTML;
    popupWin = window.open(
      '',
      '_blank',
      'top=0,left=0,height=auto,width=auto',
    );
    popupWin.document.open();
    popupWin.document.write(
      `<html>
                <style type="text/css">
                    body {font-family: 'Times New Roman', Times, serif; font-size: 14px}

                    table th, table td {
                        border:1px solid #000;
                        padding: 5px;
                    }

                    table {
                        margin:0;
                        padding:0;
                        border-spacing: 0;
                        border-collapse: collapse;
                    }

                    .text-left {
                        text-align: left;
                    }

                    .mt-10 {
                        margin-top: 10px
                    }

                    .no-color {
                        color: transparent
                    }

                    .float-left {
                        floadt: left;
                    }

                    .header {
                        margin-bottom: 10px
                    }

                    .px-20 {
                        padding: 0 20px;
                    }

                    .light {
                        font-weight: normal;
                    }

                    .indent-10 {
                        text-indent: 10px
                    }

                    .mt-20 {
                        margin-top: 20px
                    }

                    .mb-20 {
                        margin-bottom: 20px
                    }

                    .p-20-5 {
                        padding: 20px 5px
                    }

                    .ml-5 {
                        margin-left: 5px
                    }

                    .col-9, .col-md-9 {
                        width: 75%
                    }

                    .col-3, .col-md-3 {
                        width: 25%
                    }

                    .underline {
                        border-bottom-color: black;
                        border-bottom-width: 1px;
                        border-bottom-style: groove
                    }

                    .my-10 {
                        margin-top: 10px;
                        margin-bottom: 10px;
                    }

                    .bold {
                        font-weight: bold
                    }

                    .text-center {
                        text-align: center
                    }

                    .d-flex {
                        display: flex;
                    }

                    .justify-space-between {
                        justify-content: space-between;
                    }

                    .m-0 {
                        margin: 0;
                    }

                    .italic {
                        font-style: italic;
                    }

                    .mb-10 {
                        margin-bottom: 10px
                    }

                    .align-end {
                        align-items: end;
                    }

                    .w-60 {
                        width: 60%
                    }

                    .max-w {
                        width: 100%;
                    }

                    .align-center {
                        align-items: center;
                    }

                    .box-sig {
                        height: 100px; width: 100%
                    }

                    @media print { @page { size: A4 landscape; font-size: 14px} }
                </style>
                <head>
                    <title></title>
                </head>
                <body onload="window.print();window.close()">${printContents}</body>
            </html>`,
    );
    popupWin.document.close();
  }

  exportExcel() {
    let bodyDataExcel = {
      ...this.dataInfo,
    };
    this._exportService.generatePureStoreExcel(
      bodyDataExcel,
      `Tinh Kho ${this.form.get('tenBienBan').value}`,
    );
  }
}
