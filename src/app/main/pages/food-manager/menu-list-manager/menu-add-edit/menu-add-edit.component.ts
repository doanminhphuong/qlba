import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import {
    DateAdapter,
    MatDialog,
    MatTableDataSource,
    MAT_DATE_FORMATS,
    MAT_DATE_LOCALE,
} from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { CategoryService } from '@core/services/category.service';
import { forkJoin, Subscription } from 'rxjs';
import { DatePipe, Location } from '@angular/common';
import { ToastrService } from '@core/services/toastr.service';
import { DeleteConfirmDialogComponent } from '@app/_shared/dialogs/delete-confirm-dialog/delete-confirm-dialog.component';
import { FoodListDialogComponent } from './food-list-dialog/food-list-dialog.component';
import { MenuListService } from '@core/services/menu-list.service';
import {
    MAT_MOMENT_DATE_ADAPTER_OPTIONS,
    MomentDateAdapter,
} from '@angular/material-moment-adapter';
import * as moment from 'moment';
import { E } from '@angular/cdk/keycodes';
import { data } from 'jquery';
import { ExportExcelService } from '@core/services/export-excel.service';
import { CommonService } from '@core/services/common.service';
import { DataService } from '@core/services/data-service';

export const MY_FORMATS = {
    parse: {
        dateInput: 'DD/MM/YYYY', // this is how your date will be parsed from Input
    },
    display: {
        dateInput: 'DD/MM/YYYY', // this is how your date will get displayed on the Input
        monthYearLabel: 'MMMM YYYY',
        dateA11yLabel: 'LL',
        monthYearA11yLabel: 'MMMM YYYY',
    },
};

@Component({
    selector: 'app-menu-add-edit',
    templateUrl: './menu-add-edit.component.html',
    styleUrls: ['./menu-add-edit.component.scss'],
})
export class MenuAddEditComponent implements OnInit {
    menuId: any;
    isEdit: boolean = false;
    basicFields: any[] = [];
    form: FormGroup;
    listDetailMenu: any[] = [];
    listTypeOfFood: any[] = [];
    listUnits: any[] = [];
    listFood: any[] = [];
    listFoodAndTypeOfFood: any[] = [];
    listObjectEat: any[] = [];

    dataTableSource: any[] = [];

    dataEdit: any;

    displayedColumnsOfField = [
        'ma',
        'tenMonAn',
        'donViTinh',
        'loaiMonAn',
        'buoiAn',
    ];

    dataSource = new MatTableDataSource<any>([]);
    dateStart = new FormControl(Validators.required);
    dateEnd = new FormControl('');
    dataPickerEnd: string;
    isAddFood: boolean = false;
    minDate = new Date();
    meal = ['Sáng', 'Trưa', 'Chiều'];
    isPrinter: boolean = false;
    priceObject: any;
    listPrice: any[] = [];
    dateInPrinter: any;
    dateStartAndEndInPrinter: any;
    monthAndYearInPrinter: any;
    typeAction: string = '';
    allowExec: boolean = false;
    subscription: Subscription;

    constructor(
        private _activatedRoute: ActivatedRoute,
        private _categoryService: CategoryService,
        private _location: Location,
        public dialog: MatDialog,
        private _toastrService: ToastrService,
        private menuListService: MenuListService,
        private _router: Router,
        private _exportService: ExportExcelService,
        private _commonService: CommonService,
        private _dataService: DataService,
    ) {
        this._commonService.checkPermission2().subscribe((response) => {
            if (!response.result) return;

            const permissions = JSON.parse(response.result);
            this.allowExec = permissions.some((obj) => obj.key === 'EXEC');
        });

        this.subscription = this._dataService.getData().subscribe((message) => {
            this._dataService.changeDauMoiBep(message.id);
        });
    }

    ngOnInit() {
        this.getInitData();
        this.getTypeOfFood();
        this.getUnits();
        this.getFoods();
        this.getTypeOfFoodById();
    }

    getInitData() {
        // Check if this is Create or Edit page
        this._activatedRoute.paramMap.subscribe((param) => {
            this.menuId = +param.get('id');
            if (this.menuId) {
                this.isEdit = true;
            }
            this.typeAction = param.get('type');
        });

        this.menuListService.getObjectEat().subscribe((response) => {
            if (response.result.length > 0) {
                this.listObjectEat = response.result.map((item) => ({
                    key: item.nhomDoiTuongId,
                    name: item.nhomDoiTuong
                        ? item.nhomDoiTuong
                        : 'Mức tiền ăn ' + item.mucTienAn,
                    price: item.mucTienAn,
                    dinhLuongAnId: item.id,
                }));

                this.getInitForm();
            }
        });
    }

    myFilter = (d: any): boolean => {
        const day = d._d.getDay();
        // Prevent all days except Monday from being selected.
        return day === 1;
    };

    filterDay = (d: Date): boolean => {
        const day = new Date(d).getDay();
        let limit = [];
        this.listDetailMenu.map((item) => {
            if (item.chiTietNgay.length > 0) {
                limit.push(new Date(item.ngayAn).getDay());
            }
        });
        let valueDataFind = limit.find((x) => x === day);
        // Prevent day if it have a menu.
        return valueDataFind !== 0 ? !valueDataFind : !!valueDataFind;
    };

    getInitForm(): void {
        this.basicFields = [
            {
                type: 'MULTISELECT',
                referenceValue: 'hideValue',
                name: 'Mức tiền ăn',
                defaultValue: '',
                required: '1',
                options: [...this.listObjectEat],
                css: 'col-6 col-lg-6',
                appearance: 'legacy',
                selectionChange: (data) => this.handleSelectionObject(data),
            },
            {
                type: 'TEXT',
                referenceValue: 'status',
                name: 'Ghi chú',
                defaultValue: '',
                required: '0',
                css: 'col-6 col-lg-6',
                appearance: 'legacy',
            },
        ];
        let fieldsCtrls = {};
        let fieldList = [];

        for (let f of this.basicFields) {
            fieldList = [...fieldList, f];

            if (f.type === 'NUMBER') {
                fieldsCtrls[f.referenceValue] = new FormControl(
                    { value: f.defaultValue, disabled: f.disabled },
                    f.required === '1' ? [Validators.required] : [],
                );
            } else if (f.type !== 'CHECKBOX') {
                let validators = [
                    Validators.minLength(f.minLength),
                    Validators.maxLength(f.maxLength),
                    Validators.pattern(f.pattern),
                ];

                if (f.type === 'EMAIL') {
                    validators = [...validators, Validators.email];
                }

                fieldsCtrls[f.referenceValue] = new FormControl(
                    {
                        value: f.defaultValue,
                        disabled: f.disabled,
                    },
                    f.required === '1'
                        ? [...validators, Validators.required]
                        : [...validators],
                );
            } else {
                //if checkbox, it need multiple
                let opts = {};
                for (let opt of f.options) {
                    opts[opt.key] = new FormControl({
                        value: opt.value,
                        disabled: f.disabled,
                    });
                }

                fieldsCtrls[f.referenceValue] = new FormGroup(opts);
            }
        }

        this.form = new FormGroup(fieldsCtrls);

        if (this.isEdit) {
            this.getDataEdit();
        }
    }

    getDataEdit() {
        let menuList = this.menuListService.getMenuList({ id: this.menuId });
        forkJoin([menuList]).subscribe((response) => {
            if (response[0].success) {
                this.dataEdit = response[0].result;
                this.priceObject = response[0].result.hideValue
                    ? JSON.parse(response[0].result.hideValue)
                    : [];
                this.dateStart = new FormControl(response[0].result.tuNgay);
                this.dateEnd = new FormControl(response[0].result.denNgay);

                this.getArrayDates(this.dateStart, this.dateEnd);

                response[0].result.chiTiet.forEach((item) => {
                    this.listDetailMenu.forEach((menu) => {
                        if (
                            new Date(item.ngayAn).getDate() ===
                            new Date(menu.ngayAn).getDate()
                        ) {
                            menu.chiTietNgay = [...item.chiTietNgay];
                        }
                    });
                });

                this.isAddFood = true;
                let dataEdit = {
                    hideValue: this.priceObject,
                    status: response[0].result.status,
                };
                this.form.patchValue(dataEdit);

                this.checkPriceObject();
            }
        });
    }

    convertStringToNumber(input: string) {
        var numeric = Number(input);
        return numeric;
    }

    getTypeOfFood() {
        let bodyRequest = {
            maxResultCount: 9999999,
            sorting: 'Code',
            criterias: [
                {
                    propertyName: 'GroupCode',
                    operation: 6,
                    value: 'loai-mon-an',
                },
            ],
        };
        this._categoryService
            .getAllCategory(bodyRequest)
            .subscribe((response) => {
                if (response.success) {
                    this.listTypeOfFood = response.result.map((item) => ({
                        key: item.codeData,
                        name: item.name,
                    }));
                }
            });
    }

    getUnits() {
        let bodyRequest = {
            maxResultCount: 9999999,
            sorting: 'Code',
            criterias: [
                {
                    propertyName: 'GroupCode',
                    operation: 6,
                    value: 'don-vi-tinh',
                },
            ],
        };
        this._categoryService
            .getAllCategory(bodyRequest)
            .subscribe((response) => {
                if (response.success) {
                    this.listUnits = response.result.map((item) => ({
                        key: item.id,
                        name: item.name,
                    }));
                }
            });
    }

    getTypeOfFoodById() {
        let bodySubmit = {
            criterias: [
                {
                    propertyName: 'status',
                    operation: 'Equals',
                    value: 'ENABLE',
                },
            ],
        };
        this.menuListService
            .getAllFoodList(bodySubmit)
            .subscribe((response) => {
                if (response.success) {
                    this.listFoodAndTypeOfFood = response.result.items.map(
                        (item) => ({
                            idMonAn: item.id,
                            maLoaiMonAn: item.maLoaiMonAn,
                        }),
                    );
                }
            });
    }

    getFoods() {
        let bodySubmit = {
            maxResultCount: 9999999,
            criterias: [
                {
                    propertyName: 'status',
                    operation: 'Equals',
                    value: 'ENABLE',
                },
            ],
        };
        this.menuListService
            .getAllFoodList(bodySubmit)
            .subscribe((response) => {
                if (response.success) {
                    this.listFood = response.result.items.map((item) => ({
                        key: item.id,
                        name: item.tenMonAn,
                    }));
                }
            });
    }

    checkPriceObject() {
        let uniqueArray = [];

        if (this.listObjectEat.length > 0 && this.priceObject.length > 0) {
            this.listObjectEat.forEach((item) => {
                this.priceObject.forEach((object) => {
                    if (item.key === object) {
                        uniqueArray.push(item.price);
                        this.listPrice = uniqueArray.filter(function (
                            elem,
                            index,
                            self,
                        ) {
                            return index === self.indexOf(elem);
                        });
                    }
                });
            });
        }
    }

    checkMainDish(idMonAn: any) {
        let data;
        let dataList = [];

        if (this.listFoodAndTypeOfFood.length > 0) {
            dataList = this.listFoodAndTypeOfFood;
            let item = dataList.find((item) => item.idMonAn === idMonAn);
            data = item && item.maLoaiMonAn ? item.maLoaiMonAn : '';
        }
        return data;
    }

    compareObject(key, type) {
        let data;
        let dataList = [];

        if (this.listFood.length > 0 && this.listUnits.length > 0) {
            switch (type) {
                case 'FOOD':
                    dataList = this.listFood;
                    break;
                case 'UNIT':
                    dataList = this.listUnits;
                    break;
            }
            let item = dataList.find((item) => item.key === key);
            data = item && item.name ? item.name : '';
        }

        return data;
    }

    handleSelectionObject(data: any) {
        this.priceObject = [...data.value];
        this.checkPriceObject();
    }

    convertToJson(data) {
        return JSON.parse(data);
    }

    convertToMeal(key: any) {
        if (key === 'Sáng') {
            return 0;
        } else if (key === 'Trưa') {
            return 1;
        } else if (key === 'Chiều') {
            return 2;
        }
    }

    goBack() {
        this._location.back();
    }

    onSubmit() {
        if (this.isEdit) {
            let bodySubmit = {
                ...this.dataEdit,
                ...this.form.value,
                id: this.menuId,
                chiTiet: this.listDetailMenu,
                hideValue: JSON.stringify(this.form.value.hideValue),
            };

            this.menuListService.updateMenuList(bodySubmit).subscribe(
                (response) => {
                    if (response.success) {
                        this._router.navigate([
                            'manage/app/category/danh-sach-thuc-don',
                        ]);
                        this._toastrService.success('', 'Cập nhật thành công');
                    }
                },
                (err) => this._toastrService.errorServer(err),
            );
        } else {
            let parent = this.listObjectEat;
            let children = this.priceObject;

            let results = children.map((item) =>
                parent.find((parentItem) => parentItem.key.includes(item)),
            );
            let eatID = results;
            let result = '';
            for (let index = 0; index < eatID.length; index++) {
                let element = eatID[index];
                let pricevalue = element.price.toString().slice(0, 2);
                result += '-';
                result += pricevalue;
            }
            let bodySubmit = {
                ...this.form.value,
                hideValue: JSON.stringify(this.form.value.hideValue),
                tenThucDon: 'Tuần' + ' ' + moment(this.dateStart.value).week(),
                maThucDon:
                    moment(this.dateStart.value).year() +
                    ' - T' +
                    moment(this.dateStart.value).week() +
                    result,
                tuNgay: this.dateStart.value.toISOString(),
                denNgay: this.dateEnd.value,
                dauMoiBepId: this._dataService.getDauMoiBepId(),
                chiTiet: this.listDetailMenu,
            };

            this.menuListService.createMenuList(bodySubmit).subscribe(
                (response) => {
                    if (response.success) {
                        this._router.navigate([
                            'manage/app/category/danh-sach-thuc-don',
                        ]);
                        this._toastrService.success('', 'Thêm thành công');
                    }
                },
                (err) => this._toastrService.errorServer(err),
            );
        }
    }

    modalListMenu(dataDialog: any) {
        let fields = [];
        if (dataDialog) {
            fields = [
                {
                    type: 'DATETIME',
                    referenceValue: 'ngayAn',
                    name: 'Ngày ăn',
                    defaultValue: dataDialog.ngayAn,
                    required: '1',
                    css: 'col-7 col-lg-7',
                    min: dataDialog.ngayAn,
                    max: dataDialog.ngayAn,
                    appearance: 'legacy',
                },
            ];
        } else {
            fields = [
                {
                    type: 'DATETIME',
                    referenceValue: 'ngayAn',
                    name: 'Ngày ăn',
                    defaultValue: '',
                    required: '1',
                    css: 'col-7 col-lg-7',
                    min: this.dateStart.value,
                    max: this.dateEnd.value,
                    matDatepickerFilter: this.filterDay,
                    appearance: 'legacy',
                },
            ];
        }

        const dialogRef = this.dialog.open(FoodListDialogComponent, {
            width: '65%',
            data: {
                datas: dataDialog,
                fields: fields,
            },
            panelClass: 'my-custom-dialog-class',
        });

        dialogRef.componentInstance.onSave.subscribe((formValues) => {
            const isSubmitted = !!formValues;
            if (!dataDialog) {
                this.listDetailMenu.forEach((item) => {
                    if (
                        new Date(item.ngayAn).getDate() ===
                        new Date(formValues.ngayAn).getDate()
                    ) {
                        for (let childItem of formValues.chiTietNgay) {
                            item.chiTietNgay.push(childItem);
                        }
                    }
                });
            } else {
                for (let item of this.listDetailMenu) {
                    if (
                        new Date(item.ngayAn).getDate() ===
                        new Date(formValues.ngayAn).getDate()
                    ) {
                        item.chiTietNgay = [];
                        item.chiTietNgay = formValues.chiTietNgay;
                    }
                }
            }
            dialogRef.close(isSubmitted);
        });

        dialogRef.afterClosed().subscribe((result) => {
            if (!result) return;
            if (!dataDialog) {
                this._toastrService.success('', 'Thêm thành công');
            } else {
                this._toastrService.success('', 'Cập nhật thành công');
            }
        });
    }

    countMeal(arr: any, idMeal: any) {
        return arr.filter((x) => Number(x.buoiAn) === idMeal).length;
    }

    getEndDateOfWeek(event: any) {
        let last =
            new Date(new Date(event.value._d).setHours(0, 0, 0, 0)).getDate() -
            event.value._d.getDay() +
            7; // last day is the first day + 6
        this.dataPickerEnd = new Date(
            event.value._d.setDate(last),
        ).toISOString();
        this.dateEnd = new FormControl(this.dataPickerEnd);
        this.isAddFood = true;
        this.getArrayDates(this.dateStart, this.dateEnd);
    }

    convertDayToVN(dataConvert: any) {
        let day;
        switch (dataConvert) {
            case 'Monday':
                day = 'Thứ 2';
                break;
            case 'Tuesday':
                day = 'Thứ 3';
                break;
            case 'Wednesday':
                day = 'Thứ 4';
                break;
            case 'Thursday':
                day = 'Thứ 5';
                break;
            case 'Friday':
                day = 'Thứ 6';
                break;
            case 'Saturday':
                day = 'Thứ 7';
                break;
            case 'Sunday':
                day = 'Chủ nhật';
                break;
            default:
                break;
        }

        return day;
    }

    getArrayDates(startDate, stopDate) {
        let arrayDate = [];
        var currentDate = new Date(new Date(startDate.value));

        while (currentDate <= new Date(stopDate.value)) {
            arrayDate.push({
                ngayAn: new Date(currentDate).toISOString(),
                chiTietNgay: [],
            });
            currentDate = moment(currentDate).add(1, 'days').toDate();
        }
        this.listDetailMenu = arrayDate;
        this.dateInPrinter =
            ' ngày ' +
            moment(this.dateStart.value)
                .subtract(3, 'days')
                .toDate()
                .getDate() +
            ' tháng ' +
            `${
                moment(this.dateStart.value)
                    .subtract(3, 'days')
                    .toDate()
                    .getMonth() + 1
            }` +
            ' năm ' +
            moment(this.dateStart.value)
                .subtract(3, 'days')
                .toDate()
                .getFullYear();

        this.monthAndYearInPrinter =
            ' tháng ' +
            `${moment(this.dateStart.value).toDate().getMonth() + 1}` +
            ' năm ' +
            moment(this.dateStart.value).toDate().getFullYear();

        this.dateStartAndEndInPrinter =
            'Từ ngày ' +
            moment(this.dateStart.value).toDate().getDate() +
            ' tháng ' +
            `${moment(this.dateStart.value).toDate().getMonth() + 1}` +
            ' năm ' +
            moment(this.dateStart.value).toDate().getFullYear() +
            ' đến ngày ' +
            moment(this.dateEnd.value).toDate().getDate() +
            ' tháng ' +
            `${moment(this.dateStart.value).toDate().getMonth() + 1}` +
            ' năm ' +
            moment(this.dateEnd.value).toDate().getFullYear();
    }

    exportTable(tableId: string, name?: string) {
        let printContents, popupWin;
        printContents = document.getElementById(tableId).innerHTML;
        popupWin = window.open(
            '',
            '_blank',
            'top=0,left=0,height=auto,width=auto',
        );
        popupWin.document.open();
        popupWin.document.write(
            `
          <html>
          <style type="text/css">
              body {font-family: 'Times New Roman', Times, serif, font-size: 14px}

              table th, table td {
                  border:1px solid #000;
                  padding: 5px;
              }

              table {
                  margin:0;
                  padding:0;
                  border-spacing: 0;
                  border-collapse: collapse;
              }

              .text-left {
                  text-align: left;
              }

              .mt-10 {
                  margin-top: 10px
              }

              .float-left {
                  floadt: left;
              }

              .header {
                  margin-bottom: 10px
              }

              .px-20 {
                  padding: 0 20px;
              }

              .mt-20 {
                  margin-top: 20px
              }

              .mb-20 {
                  margin-bottom: 20px
              }

              .p-20-5 {
                  padding: 20px 5px
              }

              .ml-5 {
                  margin-left: 5px
              }

              .col-9, .col-md-9 {
                  width: 75%
              }

              .col-3, .col-md-3 {
                  width: 25%
              }

              .underline {
                  border-bottom-color: black;
                  border-bottom-width: 1px;
                  border-bottom-style: groove
              }

              .my-10 {
                  margin-top: 10px;
                  margin-bottom: 10px;
              }

              .bold {
                  font-weight: bold
              }

              .text-center {
                  text-align: center
              }

              .d-flex {
                  display: flex;
              }

              .justify-space-between {
                  justify-content: space-between;
              }

              .w-20 {
                  width: 20%
              }

              .m-0 {
                  margin: 0;
              }

              .italic {
                  font-style: italic;
              }

              .mb-10 {
                  margin-bottom: 10px
              }

              .w-60 {
                  width: 60%
              }

              .align-center {
                  align-items: center;
              }

              .box-sig {
                  height: 100px; width: 100%
              }

              .rotate {
                  /* Safari */
                  -webkit-transform: rotate(-90deg);

                  /* Firefox */
                  -moz-transform: rotate(-90deg);

                  /* IE */
                  -ms-transform: rotate(-90deg);

                  /* Opera */
                  -o-transform: rotate(-90deg);
                              }

              @media print { @page { size: A4 landscape; } }
          </style>
          <head>
              <title></title>
          </head>
          <body onload="window.print();window.close()">${printContents}</body>
          </html>`,
        );
        popupWin.document.close();
    }

    exportExcel() {
        let bodyDataExcel = {
            data: this.listDetailMenu,
            dateStartAndEndInPrinter: this.dateStartAndEndInPrinter,
            listPrice: this.listPrice,
            listFood: this.listFood,
            dateInPrinter: this.dateInPrinter,
            monthAndYearInPrinter: this.monthAndYearInPrinter,
        };

        let result = '';
        for (let index = 0; index < this.listPrice.length; index++) {
            let element = this.listPrice[index];
            let pricevalue = element.toString().slice(0, 2);
            result += '.';
            result += pricevalue;
        }

        let nameFile = `TĐ.T${moment(this.dateStart.value).week()}${result}`;

        this._exportService.generateMenuExcel(
            bodyDataExcel,
            `${nameFile}.xlsx`,
        );
    }
}
