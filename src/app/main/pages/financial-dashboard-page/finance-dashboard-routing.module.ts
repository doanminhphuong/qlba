import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FinanceDashboardPageComponent } from './finance-dashboard-page/finance-dashboard-page.component';

const routes: Routes = [
    {
        path: '',
        component: FinanceDashboardPageComponent,
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class FinanceDashboardRoutingModule {}
