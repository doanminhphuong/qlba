import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FieldExportComponent } from './field-export.component';

describe('FieldExportComponent', () => {
  let component: FieldExportComponent;
  let fixture: ComponentFixture<FieldExportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FieldExportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FieldExportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
