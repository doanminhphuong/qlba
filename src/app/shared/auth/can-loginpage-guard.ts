import { Injectable } from '@angular/core';
import {
    CanActivate,
    ActivatedRouteSnapshot,
    RouterStateSnapshot,
    UrlTree,
    Router,
} from '@angular/router';
import { TokenStorageService } from '@core/services/token.service';
import { Observable } from 'rxjs';

const TOKEN_KEY = 'auth-token';

@Injectable({
    providedIn: 'root', // you can change to any level if needed
})
export class CanLoginpageGuard implements CanActivate {
    constructor(
        private _tokenStorageService: TokenStorageService,
        private _router: Router,
    ) {}
    canActivate(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot,
    ):
        | Observable<boolean | UrlTree>
        | Promise<boolean | UrlTree>
        | boolean
        | UrlTree {
        let tokenLocale = localStorage.getItem(TOKEN_KEY);

        if (tokenLocale) {
            this._router.navigate(['/']);
            // this._router.navigate(['manage/app/category/luong-thuc-thuc-pham-chat-dot']);
            return false;
        } else {
            return true; // replace with actual logic
        }
    }
}
