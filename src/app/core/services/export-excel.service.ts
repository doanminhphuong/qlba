import { Injectable } from '@angular/core';
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
import * as ExcelJS from 'exceljs/dist/exceljs.min.js';
import { ConvertNumToStrPipe } from '@app/pages/purchase-management/purchase-add-edit/convert-num-to-str.pipe';
import { data } from 'jquery';
import { values } from 'lodash';
import { formatDate } from '@angular/common';
import { DataService } from './data-service';
import { CdkRow } from '@angular/cdk/table';
import { G } from '@angular/cdk/keycodes';
declare const ExcelJS: any;

@Injectable({
    providedIn: 'root',
})
export class ExportExcelService {
    currentDate: Date = new Date();

    constructor(
        private convertNumToStr: ConvertNumToStrPipe,
        private _dataService: DataService,
    ) { }

    fileType =
        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
    fileExtension = '.xlsx';

    public exportExcel(jsonData: any[], fileName: string): void {
        const ws: XLSX.WorkSheet = XLSX.utils.json_to_sheet(jsonData);
        const wb: XLSX.WorkBook = {
            Sheets: { data: ws },
            SheetNames: ['data'],
        };

        const excelBuffer: any = XLSX.write(wb, {
            bookType: 'xlsx',
            type: 'array',
        });
        this.saveExcelFile(excelBuffer, fileName);
    }

    public generateExcel(data, filename): void {
        let { chiTiet } = data;
        let options = {
            filename,
            useStyles: true,
            useSharedStrings: true,
        };
        let textMoney = 'Thành tiền';
        let locationTotal = '';
        let workbook = new ExcelJS.Workbook(options);

        const title = [
            `${this._dataService
                .getTenDonViCapTrenTrucTiep()
                .name.toUpperCase()}`,
            '',
            'PHIẾU NHẬP KHO',
            '',
            '',
            '',
            `Mẫu 26: PNX-TMKH/QN21`,
            '',
        ];
        const title2 = [
            `${this._dataService.getDonViThuocQuanLy().name.toUpperCase()}`,
            '',
            '',
            '',
            '',
            '',
            'Số:...',
        ];

        let listColumnKey = [
            'TT',
            'tenLttpChatDot',
            'donViTinh',
            'soLuongPhaiNhap',
            'soLuongThucNhap',
            'donGia',
            'thanhTien',
            'ghiChu',
        ];
        let listColumnKey2 = ['', '', '', 'Phải nhập', 'Thực nhập', '', '', ''];
        let dataDetail = chiTiet.map((item, index) =>
            listColumnKey.map((E) => {
                if (E === 'TT') {
                    return index + 1;
                }
                if (item[E] == null) {
                    return ' ';
                }

                return item[E];
            }),
        );
        let totalRowTable = new Array(dataDetail[0].length).fill('', 0);
        totalRowTable[1] = 'Cộng';
        totalRowTable[dataDetail[0].length - 2] = data.tongTien;
        dataDetail.push(new Array(dataDetail[0].length).fill('', 0));
        dataDetail.push(totalRowTable);

        let listColumnTitle = listColumnKey.map((item) => {
            switch (item) {
                case 'TT':
                    return 'TT';
                case 'tenLttpChatDot':
                    return 'Tên, quy cách vật tư sản phẩm';
                case 'donViTinh':
                    return 'ĐVT';
                case 'donGia':
                    return 'Giá lẻ';
                case 'thanhTien':
                    return 'Thành tiền (Đồng)';
                case 'ghiChu':
                    return 'Ghi chú';
                case 'soLuongPhaiNhap':
                    return 'SỐ LƯỢNG';

                default:
                    this.getColumnTitle(item);
            }
        });
        let alignmentCenter = {
            vertical: 'middle',
            horizontal: 'center',
        };

        let fontDefault = {
            name: 'Times New Roman',
            family: 1,
            size: 13,
        };

        let fontBold = {
            ...fontDefault,
            bold: true,
        };

        let fontTextMediumBold = {
            size: 14,
            ...fontBold,
        };

        var worksheet = workbook.addWorksheet(filename, {
            pageSetup: {
                paperSize: 9,
                orientation: 'portrait',
                fitToPage: true,
            }, //portrait,landscape
        });
        worksheet.pageSetup.printTitlesRow = '7:8';

        let titleRow = worksheet.addRow(title);
        let titleRow2 = worksheet.addRow(title2);
        worksheet.mergeCells('A1:B1');
        worksheet.mergeCells('A2:B2');
        worksheet.mergeCells('C1:F2');
        worksheet.mergeCells('G1:H1');
        worksheet.mergeCells('G2:H2');

        worksheet.addRow([]);
        worksheet.addRow(['', `Kho nhận hàng: ${data.khoNhanName}`]).font =
            fontDefault;
        worksheet.addRow([
            '',
            `Có giá trị hết ngày ${this.getDayMonthYear(
                data.ngayHetHan,
                'day',
            )}/${this.getDayMonthYear(
                data.ngayHetHan,
                'month',
            )}/${this.getDayMonthYear(data.ngayHetHan, 'year')}`,
        ]).font = fontDefault;

        worksheet.getCell('G2').alignment = {
            vertical: 'middle',
            horizontal: 'center',
        };
        worksheet.getCell('C4').alignment = {
            vertical: 'middle',
            horizontal: 'center',
        };
        titleRow.alignment = {
            vertical: 'middle',
            horizontal: 'center',
        };

        worksheet.addRow([]);

        //Add Header Row
        let headerRow = worksheet.addRow(listColumnTitle);
        let headerRow2 = worksheet.addRow(listColumnKey2);

        let listColumnHeaderRow = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H'];
        //Merge header
        for (let index = 0; index < listColumnHeaderRow.length; index++) {
            let currentItem = listColumnHeaderRow[index];
            if (currentItem == 'D' || currentItem == 'E') {
                if (currentItem == 'D') {
                    worksheet.mergeCells('D7:E7');
                }

                let itemStyle = worksheet.getCell(`${currentItem}8`);
                // itemStyle.fill = {
                //     type: 'pattern',
                //     pattern: 'solid',
                //     fgColor: { argb: 'FFFFFF00' },
                //     bgColor: { argb: 'FF0000FF' },
                // };
                itemStyle.border = {
                    top: { style: 'thin' },
                    left: { style: 'thin' },
                    bottom: { style: 'thin' },
                    right: { style: 'thin' },
                };

                continue;
            }
            worksheet.mergeCells(`${currentItem}7:${currentItem}8`);
        }
        // worksheet.mergeCells('A8:A9');
        // Cell Style : Fill and Border
        headerRow.eachCell((cell, number) => {
            cell.border = {
                top: { style: 'thin' },
                left: { style: 'thin' },
                bottom: { style: 'thin' },
                right: { style: 'thin' },
            };

            cell.alignment = {
                wrapText: true,
                vertical: 'middle',
                horizontal: 'center',
            };

            cell.font = {
                ...fontBold,
            };
        });

        headerRow2.eachCell((cell, number) => {
            cell.border = {
                top: { style: 'thin' },
                left: { style: 'thin' },
                bottom: { style: 'thin' },
                right: { style: 'thin' },
            };

            cell.alignment = {
                wrapText: true,
                vertical: 'middle',
                horizontal: 'center',
            };

            cell.font = {
                ...fontBold,
            };
        });

        // Add Data and Conditional Formatting
        dataDetail.forEach((d, index, array) => {
            let row = worksheet.addRow(d);
            let qty = row.getCell(6);

            row.eachCell((cell, number) => {
                // Default
                cell.alignment = {
                    wrapText: true,
                    vertical: 'middle',
                };

                if (number === 1) {
                    cell.alignment = {
                        wrapText: true,
                        vertical: 'middle',
                        horizontal: 'center',
                    };
                }
                if (number > 3 && number < 6) {
                    cell.numFmt = '""#,##0.0;[Red]-""#,##0.000';
                }
                if (number > 5 && number < 8) {
                    cell.numFmt = '""#,##0;[Red]-""#,##0.000';
                }
                cell.font = fontDefault;

                //Last row table change style border
                if (index === array.length - 1) {
                    cell.border = {
                        top: { style: 'thin' },
                        left: { style: 'thin' },
                        bottom: { style: 'thin' },
                        right: { style: 'thin' },
                    };

                    cell.font = {
                        ...fontBold,
                    };
                } else {
                    cell.border = {
                        top: { style: 'dotted' },
                        left: { style: 'thin' },
                        bottom: { style: 'dotted' },
                        right: { style: 'thin' },
                    };
                }
            });
        });

        worksheet.addRow([]);

        worksheet.addRow(['', `Tổng nhập ${chiTiet.length} mặt hàng`]).font =
            fontDefault;

        //TransformDate max value is 1b, If more value can have bug
        let newRowTotal = worksheet.addRow([
            '',
            `${textMoney}: ${this.transformDate(data.tongTien.toString())}`,
        ]);
        newRowTotal.font = fontDefault;

        worksheet.addRow([
            '',
            `Ngày ${this.getDayMonthYear(
                data.ngayHetHan,
                'day',
            )} tháng ${this.getDayMonthYear(
                data.ngayHetHan,
                'month',
            )} năm ${this.getDayMonthYear(data.ngayHetHan, 'year')}`,
            '',
            '',
            '',
            '',
            `Ngày ${this.getDayMonthYear(
                data.ngayHetHan,
                'day',
            )} tháng ${this.getDayMonthYear(
                data.ngayHetHan,
                'month',
            )} năm ${this.getDayMonthYear(data.ngayHetHan, 'year')}`,
        ]).font = fontDefault;
        let row = worksheet.addRow([
            '',
            'Người viết phiếu',
            null,
            'Người nhận',
            null,
            '',
            'Người duyệt',
        ]);
        row.font = fontBold;
        row.alignment = {
            vertical: 'middle',
            horizontal: 'center',
        };

        worksheet.addRow([]);
        worksheet.addRow([]);
        worksheet.addRow([]);
        worksheet.addRow([]);
        worksheet.addRow([]);
        let row2 = worksheet.addRow([
            '',
            `${data.nguoiNhap}`,
            '',
            `${data.nguoiNhan}`,
            null,
            '',
            `${this._dataService.getNguoiDuyet()
                ? this._dataService.getNguoiDuyet()
                : ''
            }`,
        ]);
        row2.font = fontBold;
        row2.alignment = {
            vertical: 'middle',
            horizontal: 'center',
        };

        let rowsSkip = 8 + dataDetail.length;
        worksheet.mergeCells(`D${rowsSkip + 5}:E${rowsSkip + 5}`);
        worksheet.mergeCells(`G${rowsSkip + 5}:H${rowsSkip + 5}`);

        worksheet.mergeCells(`D${rowsSkip + 11}:E${rowsSkip + 11}`);
        worksheet.mergeCells(`G${rowsSkip + 11}:H${rowsSkip + 11}`);

        // worksheet.mergeCells('B6:H6');

        worksheet.getColumn(1).width = 5;
        worksheet.getColumn(2).width = 30;
        // worksheet.getColumn(3).width = 20;
        worksheet.getColumn(4).width = 15;
        worksheet.getColumn(5).width = 15;
        worksheet.getColumn(6).width = 15;
        worksheet.getColumn(7).width = 15;
        worksheet.getColumn(8).width = 15;
        worksheet.getColumn(9).width = 20;

        //FILE CSS
        titleRow.eachCell((cell, number) => {
            cell.alignment = alignmentCenter;
            cell.font = fontDefault;
        });

        titleRow2.eachCell((cell, number) => {
            cell.alignment = alignmentCenter;

            if (number === 1) {
                cell.font = {
                    ...fontBold,
                    underline: true,
                };
            }
            if (number === 3) {
                cell.font = {
                    ...fontBold,
                    size: 18,
                    underline: false,
                };
            }
            if (number > 6) {
                cell.font = { ...fontDefault };
            }
        });

        worksheet.getCell('A2').font = {
            ...fontBold,
            underline: true,
        };

        worksheet.getCell('G2').font = {
            ...fontDefault,
        };

        const excelBuffer: any = workbook.xlsx.writeBuffer();
        workbook.xlsx.writeBuffer().then(function (buffer) {
            // Done buffering
            //   this.saveExcelFile(buffer, fileName)
            //
            const data: Blob = new Blob([buffer], {
                type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            });
            FileSaver.saveAs(data, filename);
        });
    }

    public generateExportWarehouseExcel(data, filename): void {
        let { chiTiet } = data;
        let options = {
            filename,
            useStyles: true,
            useSharedStrings: true,
        };
        let textMoney = 'Thành tiền';
        let locationTotal = '';
        let workbook = new ExcelJS.Workbook(options);

        const title = [
            `${this._dataService
                .getTenDonViCapTrenTrucTiep()
                .name.toUpperCase()}`,
            '',
            'PHIẾU XUẤT KHO',
            '',
            '',
            '',
            'Biểu SS14-QN10',
        ];
        const title2 = [
            `${this._dataService.getDonViThuocQuanLy().name.toUpperCase()}`,
            '',
            '',
            '',
            '',
            '',
            'Quyển số:……………….',
        ];

        let listColumnKey = [
            'TT',
            'tenLttpChatDot',
            'donViTinh',
            'soLuongPhaiXuat',
            'soLuongThucXuat',
            'donGia',
            'thanhTien',
            'ghiChu',
        ];
        let listColumnKey2 = ['', '', '', 'Phải xuất', 'Thực xuất', '', '', ''];
        let dataDetail = chiTiet.map((item, index) =>
            listColumnKey.map((E) => {
                if (E === 'TT') {
                    return index + 1;
                }
                if (item[E] == null) {
                    return ' ';
                }

                return item[E];
            }),
        );
        let totalRowTable = new Array(dataDetail[0].length).fill('', 0);
        totalRowTable[1] = 'Tổng cộng';
        totalRowTable[dataDetail[0].length - 2] = data.tongTien;
        dataDetail.push(new Array(dataDetail[0].length).fill('', 0));
        dataDetail.push(totalRowTable);

        let listColumnTitle = listColumnKey.map((item) => {
            switch (item) {
                case 'tenLttpChatDot':
                    return 'Tên, quy cách vật tư sản phẩm';
                case 'donViTinh':
                    return 'ĐVT';
                case 'donGia':
                    return 'Giá lẻ';
                case 'thanhTien':
                    return 'Thành tiền (Đồng)';
                case 'ghiChu':
                    return 'Ghi chú';
                case 'soLuongPhaiXuat':
                    return 'Số lượng';
                default:
                    return this.getColumnTitle(item);
            }
        });
        let alignmentCenter = {
            vertical: 'middle',
            horizontal: 'center',
        };

        let fontDefault = {
            name: 'Times New Roman',
            family: 1,
            size: 13,
        };

        let fontBold = {
            ...fontDefault,
            bold: true,
        };

        let fontTextMediumBold = {
            size: 14,
            ...fontBold,
        };

        var worksheet = workbook.addWorksheet(filename, {
            pageSetup: {
                paperSize: 9,
                orientation: 'portrait',
                fitToPage: true,
            }, //portrait,landscape
        });
        worksheet.pageSetup.printTitlesRow = '11:12';

        let titleRow = worksheet.addRow(title);
        let titleRow2 = worksheet.addRow(title2);
        worksheet.mergeCells('A1:B1');
        worksheet.mergeCells('A2:B2');
        worksheet.mergeCells('C1:F2');
        worksheet.mergeCells('G1:H1');
        worksheet.mergeCells('G2:H2');

        worksheet.addRow(['', '', '', '', '', '', 'Số:……………………']);
        worksheet.mergeCells('G3:H3');

        worksheet.addRow([
            '',
            '',
            `Có giá trị hết: Ngày ${this.getDayMonthYear(
                data.ngayHetHan,
                'day',
            )} tháng ${this.getDayMonthYear(
                data.ngayHetHan,
                'month',
            )} năm ${this.getDayMonthYear(data.ngayHetHan, 'year')}`,
        ]).font = fontDefault;
        worksheet.mergeCells('C4:F4');
        worksheet.addRow([]);
        worksheet.addRow([
            '',
            `Họ, tên người nhận: ${data.nguoiNhan ? data.nguoiNhan : ''}`,
        ]).font = fontDefault;
        worksheet.addRow(['', `Đơn vị: ${data.donViBoPhan}`]).font =
            fontDefault;
        worksheet.addRow([
            '',
            `Lý do sử dụng: ${data.lyDoSuDung[0].name}`,
        ]).font = fontDefault;
        worksheet.addRow(['', `Nhận tại kho: ${data.nhanTaiKho}`]).font =
            fontDefault;

        worksheet.addRow([]);

        worksheet.getCell('G2').alignment = {
            vertical: 'middle',
            horizontal: 'center',
        };
        worksheet.getCell('C4').alignment = {
            vertical: 'middle',
            horizontal: 'center',
        };

        //Add Header Row
        let headerRow = worksheet.addRow(listColumnTitle);
        let headerRow2 = worksheet.addRow(listColumnKey2);
        worksheet.mergeCells('D11:E11');
        worksheet.mergeCells('A11:A12');
        worksheet.mergeCells('B11:B12');
        worksheet.mergeCells('C11:C12');
        worksheet.mergeCells('F11:F12');
        worksheet.mergeCells('G11:G12');
        worksheet.mergeCells('H11:H12');
        // Cell Style : Fill and Border
        headerRow.eachCell((cell, number) => {
            cell.border = {
                top: { style: 'thin' },
                left: { style: 'thin' },
                bottom: { style: 'thin' },
                right: { style: 'thin' },
            };

            cell.alignment = {
                wrapText: true,
                vertical: 'middle',
                horizontal: 'center',
            };

            cell.font = {
                ...fontBold,
            };
        });

        headerRow2.eachCell((cell, number) => {
            cell.border = {
                top: { style: 'thin' },
                left: { style: 'thin' },
                bottom: { style: 'thin' },
                right: { style: 'thin' },
            };

            cell.alignment = {
                wrapText: true,
                vertical: 'middle',
                horizontal: 'center',
            };

            cell.font = {
                ...fontBold,
            };
        });

        // Add Data and Conditional Formatting
        dataDetail.forEach((d, index, array) => {
            let row = worksheet.addRow(d);
            let qty = row.getCell(6);

            row.eachCell((cell, number) => {
                cell.alignment = {
                    wrapText: true,
                    vertical: 'middle',
                };

                if (number === 1) {
                    cell.alignment = {
                        wrapText: true,
                        vertical: 'middle',
                        horizontal: 'center',
                    };
                }
                if (number > 3 && number < 6) {
                    cell.numFmt = '""#,##0.0;[Red]-""#,##0.000';
                }
                if (number > 5 && number < 8) {
                    cell.numFmt = '""#,##0;[Red]-""#,##0.000';
                }
                cell.font = fontDefault;

                //Last row table change style border
                if (index === array.length - 1) {
                    cell.border = {
                        top: { style: 'thin' },
                        left: { style: 'thin' },
                        bottom: { style: 'thin' },
                        right: { style: 'thin' },
                    };

                    cell.font = {
                        ...fontBold,
                    };

                    if (number === 2) {
                        cell.alignment = alignmentCenter;
                    }
                } else {
                    cell.border = {
                        top: { style: 'dotted' },
                        left: { style: 'thin' },
                        bottom: { style: 'dotted' },
                        right: { style: 'thin' },
                    };
                }
            });
        });

        worksheet.addRow([]);

        worksheet.addRow([
            '',
            'Tổng xuất:',
            `${chiTiet.length} mặt hàng`,
        ]).font = fontDefault;

        //TransformDate max value is 1b, If more value can have bug
        let newRowTotal = worksheet.addRow([
            '',
            'Tổng số tiền (viết bằng chữ):',
            `${this.transformDate(data.tongTien.toString())}`,
        ]);
        newRowTotal.font = fontDefault;
        worksheet.addRow([
            '',
            'Ngày giao nhận:',
            `Ngày ${this.getDayMonthYear(
                data.ngayHetHan,
                'day',
            )} tháng ${this.getDayMonthYear(
                data.ngayHetHan,
                'month',
            )} năm ${this.getDayMonthYear(data.ngayHetHan, 'year')}`,
            '',
            '',
        ]).font = fontDefault;

        // Ngày ký báo cáo
        let dateSignRow = worksheet.addRow([
            '',
            '',
            '',
            '',
            '',
            '',
            `Ngày ${this.getDayMonthYear(
                this.currentDate,
                'day',
            )} tháng ${this.getDayMonthYear(
                this.currentDate,
                'month',
            )} năm ${this.getDayMonthYear(this.currentDate, 'year')}`,
        ]);
        dateSignRow.font = {
            ...fontDefault,
            italic: true,
        };
        dateSignRow.alignment = alignmentCenter;

        const rowsSkip = 12 + dataDetail.length;
        worksheet.mergeCells(`C${rowsSkip + 2}:H${rowsSkip + 2}`);
        worksheet.mergeCells(`C${rowsSkip + 3}:H${rowsSkip + 3}`);
        worksheet.mergeCells(`C${rowsSkip + 4}:H${rowsSkip + 4}`);
        worksheet.mergeCells(`G${rowsSkip + 5}:H${rowsSkip + 5}`);

        let signatureRow1 = worksheet.addRow([
            '',
            'Người viết phiếu',
            ' Người nhận',
            '',
            ' Thủ kho',
            '',
            'Người duyệt ',
            '',
        ]);
        signatureRow1.font = fontBold;
        signatureRow1.alignment = alignmentCenter;

        worksheet.addRow([]);
        worksheet.addRow([]);
        worksheet.addRow([]);
        worksheet.addRow([]);
        worksheet.addRow([]);

        let signatureRow2 = worksheet.addRow([
            '',
            `${!this.isEmty(this._dataService.getNguoiVietPhieu())
                ? this._dataService.getNguoiVietPhieu()
                : ''
            }`,
            `${!this.isEmty(data.nguoiNhan) ? data.nguoiNhan : ''}`,
            '',
            `${!this.isEmty(this._dataService.getThuKho())
                ? this._dataService.getThuKho()
                : ''
            }`,
            '',
            `${!this.isEmty(this._dataService.getTenThuTruongKyDuyet())
                ? this._dataService.getTenThuTruongKyDuyet()
                : ''
            }`,
            '',
        ]);
        signatureRow2.font = fontBold;
        signatureRow2.alignment = alignmentCenter;

        worksheet.mergeCells(`C${rowsSkip + 6}:D${rowsSkip + 6}`);
        worksheet.mergeCells(`C${rowsSkip + 12}:D${rowsSkip + 12}`);
        worksheet.mergeCells(`E${rowsSkip + 6}:F${rowsSkip + 6}`);
        worksheet.mergeCells(`E${rowsSkip + 12}:F${rowsSkip + 12}`);
        worksheet.mergeCells(`G${rowsSkip + 6}:H${rowsSkip + 6}`);
        worksheet.mergeCells(`G${rowsSkip + 12}:H${rowsSkip + 12}`);

        worksheet.addRow([]);
        worksheet.addRow([]);
        let trucBanRow1 = worksheet.addRow(['', 'Trực ban']);
        trucBanRow1.font = fontBold;
        trucBanRow1.alignment = alignmentCenter;
        worksheet.addRow([]);
        worksheet.addRow([]);
        worksheet.addRow([]);
        worksheet.addRow([]);
        worksheet.addRow([]);
        let trucBanRow2 = worksheet.addRow([
            '',
            `${this._dataService.getTrucBan()}`,
        ]);
        trucBanRow2.font = fontBold;
        trucBanRow2.alignment = alignmentCenter;

        // Setting length of columns
        worksheet.getColumn(1).width = 5;
        worksheet.getColumn(2).width = 35;
        worksheet.getColumn(4).width = 15;
        worksheet.getColumn(5).width = 15;
        worksheet.getColumn(6).width = 15;
        worksheet.getColumn(7).width = 20;
        worksheet.getColumn(8).width = 15;

        //FILE CSS

        titleRow.alignment = alignmentCenter;
        titleRow2.alignment = alignmentCenter;

        worksheet.getCell('A1').font = {
            ...fontDefault,
        };
        worksheet.getCell('C1').font = {
            ...fontBold,
            size: 18,
        };
        worksheet.getCell('A2').font = {
            ...fontBold,
            underline: true,
        };
        worksheet.getCell('G1').font = {
            ...fontDefault,
        };
        worksheet.getCell('G2').font = {
            ...fontDefault,
        };
        worksheet.getCell('G3').font = {
            ...fontDefault,
        };
        worksheet.getCell('G3').alignment = alignmentCenter;

        const excelBuffer: any = workbook.xlsx.writeBuffer();
        workbook.xlsx.writeBuffer().then(function (buffer) {
            // Done buffering
            //   this.saveExcelFile(buffer, fileName)
            //
            const data: Blob = new Blob([buffer], {
                type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            });
            FileSaver.saveAs(data, filename);
        });
    }

    public generateExportChamComExcel(
        dataNecessary,
        chiTietChamCom,
        filename,
    ): void {
        let options = {
            filename,
            useStyles: true,
            useSharedStrings: true,
        };

        let workbook = new ExcelJS.Workbook(options);

        const { tenDonVi, startDate, endDate, isChamAn } = dataNecessary;

        const SYMBOL = isChamAn ? 'x' : 'o';

        const title = [
            'TRƯỜNG SĨ QUAN THÔNG TIN',
            '',
            '',
            '',
            '',
            '',
            `BẢNG CHẤM CƠM ${isChamAn ? 'ĂN' : 'KHÔNG ĂN'} TẠI ĐƠN VỊ`,
            '',
            '',
            '',
            '',
            '',
        ];
        const title2 = [
            `ĐƠN VỊ: ${tenDonVi}`,
            '',
            '',
            '',
            '',
            '',
            `Tháng ${startDate.getMonth() + 1} năm ${startDate.getFullYear()}`,
            '',
            '',
            '',
            '',
            '',
        ];

        let dayColumns1 = [];
        let dayColumns2 = [];
        for (let d = startDate.getDate(); d <= endDate.getDate(); d++) {
            const days = [d, `${d}T`, `${d}C`];
            dayColumns1 = [...dayColumns1, ...days];

            const STC = ['S', 'T', 'C'];
            dayColumns2 = [...dayColumns2, ...STC];
        }

        let listColumnKey = ['TT', 'ngayChamCom', ...dayColumns1];
        let listColumnKey2 = ['', 'Họ và tên', ...dayColumns2];

        let listColumnTitle = listColumnKey.map((item) =>
            this.getColumnTitle(item),
        );

        const listKeysRender = listColumnKey.map((item) => {
            if (!isNaN(item)) {
                return `${item}S`;
            }
            return item;
        });

        let dataDetail = [];

        const chiTietPaginator = this.paginate(chiTietChamCom, 31);

        const getTotalChamCom = (session: string, page: number) => {
            const datas = this.paginate(chiTietChamCom, 31 * (page + 1));

            const total = datas[0].reduce((acc, cur) => {
                const filters = Object.keys(cur).filter(
                    (key) => key === session && cur[key] === SYMBOL,
                );
                return acc.concat(filters);
            }, []);

            return total.length;
        };

        const getTotalAllChamCom = (session: string) => {
            const total = chiTietChamCom.reduce((acc, cur) => {
                const filters = Object.keys(cur).filter(
                    (key) => key === session && cur[key] === SYMBOL,
                );
                return acc.concat(filters);
            }, []);

            return total.length;
        };

        chiTietPaginator.forEach((data, page, array) => {
            const dataPerPage = chiTietPaginator[page].map((item, index) =>
                listKeysRender.map((E) => {
                    if (E === 'TT') {
                        return 31 * page + index + 1;
                    }
                    if (!item[E]) {
                        return '';
                    }

                    return item[E];
                }),
            );

            if (page === array.length - 1) {
                let tongCongRow = new Array(dataPerPage[0].length).fill('', 0);
                tongCongRow[1] = 'Tổng cộng';
                listKeysRender
                    .filter((k) => k !== 'TT' && k !== 'ngayChamCom')
                    .forEach((k, i) => {
                        const result = getTotalAllChamCom(k);
                        tongCongRow[i + 2] = result > 0 ? result : '';
                    });
                dataPerPage.push(tongCongRow);
            } else {
                // Thêm hàng Cộng
                let congRow = new Array(dataPerPage[0].length).fill('', 0);
                congRow[1] = 'Cộng';
                let mangSangRow = new Array(dataPerPage[0].length).fill('', 0);
                mangSangRow[1] = 'Mang sang';
                listKeysRender
                    .filter((k) => k !== 'TT' && k !== 'ngayChamCom')
                    .forEach((k, i) => {
                        const result = getTotalChamCom(k, page);
                        congRow[i + 2] = result > 0 ? result : '';
                        mangSangRow[i + 2] = result > 0 ? result : '';
                    });
                dataPerPage.push(congRow);
                dataPerPage.push(mangSangRow);
            }
            dataDetail = [...dataDetail, ...dataPerPage];
        });

        let alignmentCenter = {
            vertical: 'middle',
            horizontal: 'center',
        };

        let fontDefault = {
            name: 'Times New Roman',
            family: 1,
            size: 13,
        };

        let fontBold = {
            ...fontDefault,
            bold: true,
        };

        let fontTextMediumBold = {
            size: 14,
            ...fontBold,
        };

        var worksheet = workbook.addWorksheet(filename, {
            pageSetup: {
                paperSize: 9,
                orientation: 'portrait',
                fitToPage: true,
            }, //portrait,landscape
        });
        let titleRow = worksheet.addRow(title);
        let titleRow2 = worksheet.addRow(title2);
        worksheet.mergeCells('A1:F1');
        worksheet.mergeCells('A2:F2');
        worksheet.mergeCells('G1:Z1');
        worksheet.mergeCells('G2:Z2');

        worksheet.addRow([]);
        worksheet.addRow([]);

        titleRow.alignment = {
            vertical: 'middle',
            horizontal: 'center',
        };

        //Add Header Row
        let headerRow = worksheet.addRow(listColumnTitle);
        let headerRow2 = worksheet.addRow(listColumnKey2);

        const dynamicColumns = dayColumns1.map((d, i) =>
            this.columnToLetter(i + 3),
        );
        const paginateDynamicColumns = this.paginate(dynamicColumns, 3);

        worksheet.mergeCells('A5:A6');
        paginateDynamicColumns.forEach((arr, index) => {
            worksheet.mergeCells(`${arr[0]}5:${arr[arr.length - 1]}5`);
        });

        // Cell Style : Fill and Border
        headerRow.eachCell((cell, number) => {
            cell.border = {
                top: { style: 'thin' },
                left: { style: 'thin' },
                bottom: { style: 'thin' },
                right: { style: 'thin' },
            };

            cell.alignment = {
                wrapText: true,
                vertical: 'middle',
                horizontal: 'center',
            };

            cell.font = {
                ...fontBold,
            };
        });

        headerRow2.eachCell((cell, number) => {
            cell.border = {
                top: { style: 'thin' },
                left: { style: 'thin' },
                bottom: { style: 'thin' },
                right: { style: 'thin' },
            };

            cell.alignment = {
                wrapText: true,
                vertical: 'middle',
                horizontal: 'center',
            };

            if (number === 1) {
                cell.font = {
                    ...fontBold,
                };
            } else {
                cell.font = {
                    ...fontDefault,
                };
            }
        });

        const listDataDetails = this.paginate(dataDetail, 33);

        // Add Data and Conditional Formatting
        listDataDetails.forEach((dataDetail, indexOfList, arrayOfList) => {
            dataDetail.forEach((d, i, array) => {
                let row = worksheet.addRow(d);
                let qty = row.getCell(6);

                row.eachCell((cell, number) => {
                    if (number === 1 || number > 2) {
                        cell.alignment = {
                            wrapText: true,
                            vertical: 'middle',
                            horizontal: 'center',
                        };
                    }

                    cell.font = fontDefault;

                    if (
                        i === array.length - 1 ||
                        (i === array.length - 2 &&
                            indexOfList !== arrayOfList.length - 1)
                    ) {
                        cell.border = {
                            top: { style: 'thin' },
                            left: { style: 'thin' },
                            bottom: { style: 'thin' },
                            right: { style: 'thin' },
                        };

                        if (number === 2) {
                            cell.alignment = {
                                wrapText: false,
                                vertical: 'middle',
                                horizontal: 'center',
                            };

                            if (
                                i === array.length - 2 ||
                                (i === array.length - 1 &&
                                    indexOfList === arrayOfList.length - 1)
                            ) {
                                cell.font = {
                                    ...fontBold,
                                };
                            }
                        }
                    } else {
                        cell.border = {
                            top: { style: 'dotted' },
                            left: { style: 'thin' },
                            bottom: { style: 'dotted' },
                            right: { style: 'thin' },
                        };
                    }
                });
            });
        });

        // worksheet.mergeCells('B6:H6');

        worksheet.getColumn(1).width = 5;
        worksheet.getColumn(2).width = 30;
        dayColumns1.forEach((item, index) => {
            worksheet.getColumn(index + 3).width = 5;
        });

        //FILE CSS
        titleRow.alignment = alignmentCenter;
        titleRow2.alignment = alignmentCenter;

        titleRow.font = {
            ...fontDefault,
        };

        worksheet.getCell('G1').font = {
            ...fontBold,
        };

        worksheet.getCell('A2').font = {
            ...fontBold,
            underline: true,
        };

        worksheet.getCell('G2').font = {
            ...fontDefault,
        };

        const excelBuffer: any = workbook.xlsx.writeBuffer();
        workbook.xlsx.writeBuffer().then(function (buffer) {
            // Done buffering
            //   this.saveExcelFile(buffer, fileName)
            //
            const data: Blob = new Blob([buffer], {
                type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            });
            FileSaver.saveAs(data, filename);
        });
    }

    public generateExportAnThemExcel(
        dataNecessary,
        chiTietAnThem,
        lyDoAnThem,
        filename,
    ): void {
        let options = {
            filename,
            useStyles: true,
            useSharedStrings: true,
        };
        let textMoney = 'Thành tiền';
        let locationTotal = '';
        let workbook = new ExcelJS.Workbook(options);

        const EXCEL_TITLE =
            lyDoAnThem === 0 ? 'ỐM' : lyDoAnThem === 1 ? 'LỄ' : 'LÀM NHIỆM VỤ';

        const { tenDonVi, startDate, endDate } = dataNecessary;

        const title = [
            `${this._dataService
                .getTenDonViCapTrenTrucTiep()
                .name.toUpperCase()}`,
            '',
            '',
            '',
            '',
            '',
            `BẢNG CHẤM CƠM ĂN THÊM KHI ${EXCEL_TITLE} TẠI ĐƠN VỊ`,
            '',
            '',
            '',
            '',
            '',
        ];
        const title2 = [
            `${this._dataService.getDonViThuocQuanLy().name.toUpperCase()}`,
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
        ];
        const title3 = [
            '',
            '',
            '',
            '',
            '',
            '',
            `Tháng ${startDate.getMonth() + 1} năm ${startDate.getFullYear()}`,
            '',
            '',
            '',
            '',
            '',
        ];

        let dayColumns1 = [];
        let dayColumns2 = [];
        for (let d = startDate.getDate(); d <= endDate.getDate(); d++) {
            dayColumns1.push(d);
            dayColumns2.push(d.toString());
        }

        let listColumnKey = [
            'STT',
            'tenQuanNhan',
            ...dayColumns1,
            'totalAnThem',
        ];
        let listColumnKey2 = ['', '', ...dayColumns2, ''];

        let listColumnTitle = listColumnKey.map((item, index) => {
            if (index === 2) {
                return 'Ngày trong tháng';
            } else {
                return this.getColumnTitle(item);
            }
        });

        let dataDetail = chiTietAnThem.map((item, index) =>
            listColumnKey.map((E) => {
                if (E === 'STT') {
                    return ('0' + (index + 1)).slice(-2);
                }
                if (!item[E]) {
                    return '';
                }

                return item[E];
            }),
        );

        let alignmentCenter = {
            vertical: 'middle',
            horizontal: 'center',
        };

        let fontDefault = {
            name: 'Times New Roman',
            family: 1,
            size: 13,
        };

        let fontBold = {
            ...fontDefault,
            bold: true,
        };

        let fontTextMediumBold = {
            size: 14,
            ...fontBold,
        };

        var worksheet = workbook.addWorksheet(filename, {
            pageSetup: {
                paperSize: 9,
                orientation: 'portrait',
                fitToPage: true,
            }, //portrait,landscape
        });
        let titleRow = worksheet.addRow(title);
        let titleRow2 = worksheet.addRow(title2);
        let titleRow3 = worksheet.addRow(title3);
        worksheet.mergeCells('A1:F1');
        worksheet.mergeCells('A2:F2');
        worksheet.mergeCells('G1:Z2');
        worksheet.mergeCells('G3:Z3');

        worksheet.addRow([]);

        //Add Header Row
        let headerRow = worksheet.addRow(listColumnTitle);
        let headerRow2 = worksheet.addRow(listColumnKey2);

        const dynamicColumns = dayColumns1.map((d, i) =>
            this.columnToLetter(i + 3),
        );
        const mergeColStart = dynamicColumns[0];
        const mergeColEnd = dynamicColumns[dynamicColumns.length - 1];
        const lastCol = this.columnToLetter(dynamicColumns.length + 3);

        worksheet.mergeCells('A5:A6');
        worksheet.mergeCells('B5:B6');
        worksheet.mergeCells(`${lastCol}5:${lastCol}6`);
        worksheet.mergeCells(`${mergeColStart}5:${mergeColEnd}5`);

        // Cell Style : Fill and Border
        headerRow.eachCell((cell, number) => {
            cell.border = {
                top: { style: 'thin' },
                left: { style: 'thin' },
                bottom: { style: 'thin' },
                right: { style: 'thin' },
            };

            cell.alignment = {
                wrapText: true,
                vertical: 'middle',
                horizontal: 'center',
            };

            cell.font = {
                ...fontBold,
            };
        });

        headerRow2.eachCell((cell, number) => {
            cell.border = {
                top: { style: 'thin' },
                left: { style: 'thin' },
                bottom: { style: 'thin' },
                right: { style: 'thin' },
            };

            cell.alignment = {
                wrapText: true,
                vertical: 'middle',
                horizontal: 'center',
            };

            if (number > 2 && number < dynamicColumns.length + 3) {
                cell.font = {
                    ...fontDefault,
                };
            } else {
                cell.font = {
                    ...fontBold,
                };
            }
        });

        // Add Data and Conditional Formatting
        dataDetail.forEach((d, index, array) => {
            let row = worksheet.addRow(d);
            let qty = row.getCell(6);

            row.eachCell((cell, number) => {
                if (number === 1 || number > 2) {
                    cell.alignment = {
                        wrapText: true,
                        vertical: 'middle',
                        horizontal: 'center',
                    };
                }
                if (number > 3 && number < 6) {
                    cell.numFmt = '""#,##0.0;[Red]-""#,##0.000';
                }
                if (number > 5 && number < 8) {
                    cell.numFmt = '""#,##0;[Red]-""#,##0.000';
                }
                cell.font = fontDefault;

                //Last row table change style border
                if (index === array.length - 1) {
                    cell.border = {
                        top: { style: 'dotted' },
                        left: { style: 'thin' },
                        bottom: { style: 'thin' },
                        right: { style: 'thin' },
                    };
                } else {
                    cell.border = {
                        top: { style: 'dotted' },
                        left: { style: 'thin' },
                        bottom: { style: 'dotted' },
                        right: { style: 'thin' },
                    };
                }
            });
        });

        const rowsSkip = 6 + dataDetail.length;

        worksheet.addRow([]);
        worksheet.addRow([]);

        // NGÀY THÁNG NĂM KÝ
        worksheet.getCell(
            `AA${rowsSkip + 3}`,
        ).value = `Ngày ${this.getDayMonthYear(
            this.currentDate,
            'day',
        )} tháng ${this.getDayMonthYear(
            this.currentDate,
            'month',
        )} năm ${this.getDayMonthYear(this.currentDate, 'year')}`;
        worksheet.getCell(`AA${rowsSkip + 3}`).font = {
            ...fontDefault,
            italic: true,
        };
        worksheet.getCell(`AA${rowsSkip + 3}`).alignment = alignmentCenter;
        worksheet.mergeCells(`AA${rowsSkip + 3}:AF${rowsSkip + 3}`);

        // CHỨC DANH
        worksheet.getCell(`A${rowsSkip + 4}`).value = 'NGƯỜI LẬP BÁO CÁO';
        worksheet.getCell(`A${rowsSkip + 4}`).font = fontBold;
        worksheet.getCell(`A${rowsSkip + 4}`).alignment = alignmentCenter;
        worksheet.mergeCells(`A${rowsSkip + 4}:F${rowsSkip + 4}`);

        worksheet.getCell(`AA${rowsSkip + 4}`).value = 'THỦ TRƯỞNG ĐƠN VỊ';
        worksheet.getCell(`AA${rowsSkip + 4}`).font = fontBold;
        worksheet.getCell(`AA${rowsSkip + 4}`).alignment = alignmentCenter;
        worksheet.mergeCells(`AA${rowsSkip + 4}:AF${rowsSkip + 4}`);

        worksheet.addRow([]);
        worksheet.addRow([]);
        worksheet.addRow([]);
        worksheet.addRow([]);
        worksheet.addRow([]);

        // TÊN CHỨC DANH
        worksheet.getCell(
            `A${rowsSkip + 10}`,
        ).value = `${this._dataService.getNguoiLapBaoCao()}`;
        worksheet.getCell(`A${rowsSkip + 10}`).font = fontBold;
        worksheet.getCell(`A${rowsSkip + 10}`).alignment = alignmentCenter;
        worksheet.mergeCells(`A${rowsSkip + 10}:F${rowsSkip + 10}`);

        worksheet.getCell(
            `AA${rowsSkip + 10}`,
        ).value = `${this._dataService.getTenThuTruongKyDuyet()}`;
        worksheet.getCell(`AA${rowsSkip + 10}`).font = fontBold;
        worksheet.getCell(`AA${rowsSkip + 10}`).alignment = alignmentCenter;
        worksheet.mergeCells(`AA${rowsSkip + 10}:AF${rowsSkip + 10}`);

        worksheet.getColumn(1).width = 10;
        worksheet.getColumn(2).width = 30;
        dynamicColumns.forEach((col, index) => {
            worksheet.getColumn(index + 3).width = 5;
        });

        //FILE CSS
        titleRow.alignment = alignmentCenter;
        titleRow2.alignment = alignmentCenter;
        titleRow3.alignment = alignmentCenter;
        titleRow3.font = fontDefault;

        worksheet.getCell('A1').font = {
            ...fontDefault,
        };

        worksheet.getCell('A2').font = {
            ...fontBold,
            underline: true,
        };

        worksheet.getCell('G1').font = {
            ...fontBold,
            size: 18,
        };

        const excelBuffer: any = workbook.xlsx.writeBuffer();
        workbook.xlsx.writeBuffer().then(function (buffer) {
            // Done buffering
            //   this.saveExcelFile(buffer, fileName)
            //
            const data: Blob = new Blob([buffer], {
                type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            });
            FileSaver.saveAs(data, filename);
        });
    }

    public generateExportBaoCaoExcel(dataDays, filename): void {
        let options = {
            filename,
            useStyles: true,
            useSharedStrings: true,
        };
        let workbook = new ExcelJS.Workbook(options);

        const { startDate, endDate } = dataDays;

        const title = [
            'TRƯỜNG SĨ QUAN THÔNG TIN',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            'Mẫu: B03/T',
        ];
        const title2 = ['TIỂU ĐOÀN 28', '', '', '', '', '', '', '', '', '', ''];
        const title3 = ['', '', '', '', 'BÁO CÁO CHI TIÊU TIỀN ĂN'];
        const title4 = [
            '',
            '',
            '',
            '',
            `Tháng ${startDate.getMonth() + 1} năm ${startDate.getFullYear()}`,
        ];
        const title5 = [
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            'Đơn vị tính: đồng',
        ];

        let listColumnKey = [
            'Đơn vị',
            'Đối tượng',
            'Quân số',
            'M.ăn 65.000đ/người',
            '',
            '',
            'M.ăn 81.000đ/người',
            '',
            '',
            'M.ăn 85.000đ/người',
            '',
            '',
            'Quân số không ăn',
            '',
            '',
        ];
        let listColumnKey2 = [
            '',
            '',
            '',
            'Quân số ăn',
            '',
            '',
            'Quân số ăn',
            '',
            '',
            'Quân số ăn',
            '',
            '',
            'Bệnh viện',
            'Bệnh xá',
            'Khác',
        ];
        let listColumnKey3 = [
            '',
            '',
            '',
            'S',
            'T',
            'C',
            'S',
            'T',
            'C',
            'S',
            'T',
            'C',
            '',
            '',
            '',
        ];

        let listColumnTitle = listColumnKey.map((item) => {
            return this.getColumnTitle(item);
        });

        let alignmentCenter = {
            vertical: 'middle',
            horizontal: 'center',
        };

        let fontDefault = {
            name: 'Times New Roman',
            family: 1,
            size: 12,
        };

        let fontBold = {
            ...fontDefault,
            bold: true,
        };

        let fontTextMediumBold = {
            size: 14,
            ...fontBold,
        };

        var worksheet = workbook.addWorksheet(filename, {
            pageSetup: {
                paperSize: 9,
                orientation: 'portrait',
                fitToPage: true,
            }, //portrait,landscape
        });
        let titleRow = worksheet.addRow(title);
        let titleRow2 = worksheet.addRow(title2);
        let titleRow3 = worksheet.addRow(title3);
        let titleRow4 = worksheet.addRow(title4);
        worksheet.addRow([]);
        let titleRow5 = worksheet.addRow(title5);

        worksheet.mergeCells('A1:D1');
        worksheet.mergeCells('A2:D2');
        worksheet.mergeCells('E3:J3');
        worksheet.mergeCells('E4:J4');
        worksheet.mergeCells('K1:N1');
        worksheet.mergeCells('K6:N6');

        // Row 1
        titleRow.alignment = alignmentCenter;
        titleRow.font = fontDefault;
        // Row 2
        titleRow2.alignment = alignmentCenter;
        titleRow2.font = fontBold;
        // Row 3
        titleRow3.alignment = alignmentCenter;
        titleRow3.font = fontBold;
        // Row 4
        titleRow4.alignment = alignmentCenter;
        titleRow4.font = fontDefault;
        // Row 5
        titleRow5.alignment = alignmentCenter;
        titleRow5.font = fontDefault;

        //Add Header Row
        let headerRow = worksheet.addRow(listColumnTitle);
        let headerRow2 = worksheet.addRow(listColumnKey2);
        let headerRow3 = worksheet.addRow(listColumnKey3);
        worksheet.mergeCells('A7:A9');
        worksheet.mergeCells('B7:B9');
        worksheet.mergeCells('C7:C9');
        worksheet.mergeCells('D7:F7');
        worksheet.mergeCells('D8:F8');
        worksheet.mergeCells('G7:I7');
        worksheet.mergeCells('G8:I8');
        worksheet.mergeCells('J7:L7');
        worksheet.mergeCells('J8:L8');
        worksheet.mergeCells('M7:O7');
        worksheet.mergeCells('M8:M9');
        worksheet.mergeCells('N8:N9');
        worksheet.mergeCells('O8:O9');

        // Cell Style : Fill and Border
        headerRow.eachCell((cell, number) => {
            cell.border = {
                top: { style: 'thin' },
                left: { style: 'thin' },
                bottom: { style: 'thin' },
                right: { style: 'thin' },
            };

            cell.alignment = {
                wrapText: true,
                vertical: 'middle',
                horizontal: 'center',
            };

            cell.font = {
                ...fontBold,
            };
        });

        headerRow2.eachCell((cell, number) => {
            cell.border = {
                top: { style: 'thin' },
                left: { style: 'thin' },
                bottom: { style: 'thin' },
                right: { style: 'thin' },
            };

            cell.alignment = {
                wrapText: true,
                vertical: 'middle',
                horizontal: 'center',
            };

            cell.font = {
                ...fontBold,
            };
        });

        headerRow3.eachCell((cell, number) => {
            cell.border = {
                top: { style: 'thin' },
                left: { style: 'thin' },
                bottom: { style: 'thin' },
                right: { style: 'thin' },
            };

            cell.alignment = {
                wrapText: true,
                vertical: 'middle',
                horizontal: 'center',
            };

            cell.font = {
                ...fontBold,
            };
        });

        //FILE CSS
        worksheet.getColumn(2).width = 25;
        worksheet.getColumn(3).width = 10;
        worksheet.getColumn(13).width = 10;
        worksheet.getColumn(14).width = 10;
        worksheet.getColumn(15).width = 10;

        worksheet.getCell('A2').font = {
            ...fontBold,
            underline: true,
        };

        worksheet.getCell('K1').font = {
            ...fontDefault,
            size: 10,
            italic: true,
        };

        const excelBuffer: any = workbook.xlsx.writeBuffer();
        workbook.xlsx.writeBuffer().then(function (buffer) {
            // Done buffering
            //   this.saveExcelFile(buffer, fileName)
            //
            const data: Blob = new Blob([buffer], {
                type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            });
            FileSaver.saveAs(data, filename);
        });
    }

    public generateExportChiTieuExcel(
        dataNecessary,
        dataChiTieuTienAn,
        filename,
    ): void {
        let options = {
            filename,
            useStyles: true,
            useSharedStrings: true,
        };
        let workbook = new ExcelJS.Workbook(options);

        const { tenDonVi, startDate } = dataNecessary;

        const title = [
            `${this._dataService
                .getTenDonViCapTrenTrucTiep()
                .name.toUpperCase()}`,
            '',
            'BÁO CÁO CHI TIÊU TIỀN ĂN',
            '',
            '',
            '',
            'Mẫu: B03/T',
        ];
        const title2 = [
            `${this._dataService.getDonViThuocQuanLy().name.toUpperCase()}`,
            '',
            '',
            '',
            '',
            '',
            '',
        ];
        const title3 = [
            '',
            '',
            `Tháng ${startDate.getMonth() + 1} năm ${startDate.getFullYear()}`,
        ];
        const title4 = ['', '', '', '', '', '', 'Đơn vị tính: đồng'];

        let listColumnKey = [
            'STT',
            'NỘI DUNG',
            'Đơn vị đã chi',
            '',
            '',
            'Xác nhận thanh toán',
            '',
            '',
        ];
        let listColumnKey2 = [
            '',
            '',
            'Định xuất',
            'Số ngày ăn',
            'Thành tiền',
            'Định xuất',
            'Số ngày ăn',
            'Thành tiền',
        ];

        let listColumnTitle = listColumnKey.map((item) => {
            return this.getColumnTitle(item);
        });

        const { danhSachChiTieuAn, danhSachChiTieuAnThem } = dataChiTieuTienAn;

        const getDataAn = (mucTienAn: number) => {
            const index = mucTienAn === 65000 ? 1 : mucTienAn === 81000 ? 2 : 3;
            const tenAn =
                mucTienAn === 65000
                    ? 'Ăn CBBB của HSQ-CS'
                    : mucTienAn === 65000
                        ? 'Ăn QBC của HSQ-CS'
                        : 'Ăn QBC của HSQ-CS';

            let result: any = [
                [index, `${tenAn}`, '', '', '', '', '', ''],
                ['', `Mức ăn: ${mucTienAn} đồng`, '', '', '', '', '', ''],
            ];

            const found = danhSachChiTieuAn.find(
                (ds) => ds.mucTienAn === mucTienAn,
            );

            if (!found) {
                result.push([
                    '',
                    '       0 đ/c x 0 ngày',
                    mucTienAn,
                    0,
                    0,
                    '',
                    '',
                    '',
                ]);
            } else {
                const dataAn = found['danhSachNgayAn'].reduce((acc, cur) => {
                    const dataRow = [
                        [
                            '',
                            `       ${cur.quanSo} đ/c x ${cur.soNgay} ngày  = ${cur.tongSoNgay} ngày`,
                            mucTienAn,
                            cur.tongSoNgay,
                            cur.thanhTien,
                            '',
                            '',
                            '',
                        ],
                    ];
                    return acc.concat(dataRow);
                }, []);

                result = [...result, ...dataAn];
            }

            return result;
        };

        const getDataAnThem = (loaiAnThem: number) => {
            const index = loaiAnThem === 0 ? 4 : loaiAnThem === 1 ? 5 : 6;
            const tenAnThem =
                loaiAnThem === 0
                    ? 'Ăn thêm khi ốm'
                    : loaiAnThem === 1
                        ? 'Ăn thêm khi lễ'
                        : 'Ăn thêm khi làm nhiệm vụ';

            let result = [[index, `${tenAnThem}`, '', '', '', '', '', '']];

            const found = danhSachChiTieuAnThem.find(
                (ds) => ds.loaiAnThem === loaiAnThem,
            );

            if (!found) {
                result.push(['', '', '', '', '', '', '', '']);
            } else {
                const dataAnThem = found['danhSachMucAn'].reduce((acc, cur) => {
                    const dataRow = [
                        [
                            '',
                            `Mức ăn: ${cur.mucTienAn} đồng`,
                            '',
                            '',
                            '',
                            '',
                            '',
                            '',
                        ],
                        [
                            '',
                            `       ${cur.quanSo} đ/c = ${cur.tongSoNgay} ngày`,
                            cur.mucTienAn,
                            cur.tongSoNgay,
                            cur.thanhTien,
                            '',
                            '',
                            '',
                        ],
                    ];

                    return acc.concat(dataRow);
                }, []);

                result = [...result, ...dataAnThem];
            }

            return result;
        };

        const dataAn = [
            ...getDataAn(65000),
            ...getDataAn(81000),
            ...getDataAn(85000),
        ];

        const dataAnThem = [
            ...getDataAnThem(0),
            ...getDataAnThem(1),
            ...getDataAnThem(2),
        ];

        let dataDetail = [
            ...dataAn,
            ...dataAnThem,
            ['', 'Tổng cộng', '', '', '', '', '', ''],
        ];

        let alignmentCenter = {
            vertical: 'middle',
            horizontal: 'center',
        };

        let fontDefault = {
            name: 'Times New Roman',
            family: 1,
            size: 13,
        };

        let fontBold = {
            ...fontDefault,
            bold: true,
        };

        let fontTextMediumBold = {
            size: 14,
            ...fontBold,
        };

        var worksheet = workbook.addWorksheet(filename, {
            pageSetup: {
                paperSize: 9,
                orientation: 'portrait',
                fitToPage: true,
            }, //portrait,landscape
        });
        worksheet.pageSetup.printTitlesRow = '6:7';

        let titleRow = worksheet.addRow(title);
        let titleRow2 = worksheet.addRow(title2);
        let titleRow3 = worksheet.addRow(title3);
        worksheet.addRow([]);
        let titleRow4 = worksheet.addRow(title4);

        worksheet.mergeCells('A1:B1');
        worksheet.mergeCells('A2:B2');
        worksheet.mergeCells('C1:F2');
        worksheet.mergeCells('C3:F3');
        worksheet.mergeCells('G1:H1');
        worksheet.mergeCells('G5:H5');

        // Row 1
        titleRow.alignment = alignmentCenter;
        titleRow.font = fontDefault;
        // Row 2
        titleRow2.alignment = alignmentCenter;
        titleRow2.font = fontBold;
        // Row 3
        titleRow3.alignment = alignmentCenter;
        titleRow3.font = fontDefault;
        // Row 4
        titleRow4.alignment = alignmentCenter;
        titleRow4.font = fontDefault;

        //Add Header Row
        let headerRow = worksheet.addRow(listColumnTitle);
        let headerRow2 = worksheet.addRow(listColumnKey2);
        worksheet.mergeCells('A6:A7');
        worksheet.mergeCells('B6:B7');
        worksheet.mergeCells('C6:E6');
        worksheet.mergeCells('F6:H6');

        // Cell Style : Fill and Border
        headerRow.eachCell((cell, number) => {
            cell.border = {
                top: { style: 'thin' },
                left: { style: 'thin' },
                bottom: { style: 'thin' },
                right: { style: 'thin' },
            };

            cell.alignment = {
                wrapText: true,
                vertical: 'middle',
                horizontal: 'center',
            };

            cell.font = {
                ...fontBold,
            };
        });

        headerRow2.eachCell((cell, number) => {
            cell.border = {
                top: { style: 'thin' },
                left: { style: 'thin' },
                bottom: { style: 'thin' },
                right: { style: 'thin' },
            };

            cell.alignment = {
                wrapText: true,
                vertical: 'middle',
                horizontal: 'center',
            };

            cell.font = {
                ...fontBold,
            };
        });

        // Add Data and Conditional Formatting
        dataDetail.forEach((d, index, array) => {
            let row = worksheet.addRow(d);
            let qty = row.getCell(6);

            row.eachCell((cell, number) => {
                cell.font = fontDefault;
                if (number === 1) {
                    cell.alignment = {
                        wrapText: true,
                        vertical: 'middle',
                        horizontal: 'center',
                    };
                }

                if (number > 2) {
                    cell.numFmt = '""#,##0;[Red]-""#,##0.000';
                }

                //Last row table change style border
                if (index === array.length - 1) {
                    cell.border = {
                        top: { style: 'thin' },
                        left: { style: 'thin' },
                        bottom: { style: 'thin' },
                        right: { style: 'thin' },
                    };

                    if (number === 2) {
                        cell.alignment = {
                            wrapText: true,
                            vertical: 'middle',
                            horizontal: 'center',
                        };
                    }

                    cell.font = {
                        ...fontBold,
                    };
                } else {
                    cell.border = {
                        top: { style: 'dotted' },
                        left: { style: 'thin' },
                        bottom: { style: 'dotted' },
                        right: { style: 'thin' },
                    };
                }
            });
        });

        const rowsSkipped = 7 + dataDetail.length;

        // GET TOTAL
        worksheet.getCell(`E${rowsSkipped - 1}`).value = {
            formula: `SUM(E9:E${rowsSkipped - 2})`,
        };
        const totalChiTieuAn = danhSachChiTieuAn.reduce((acc, cur) => {
            const total = cur.danhSachNgayAn.reduce(
                (total, item) => total + item.thanhTien,
                0,
            );

            return acc + total;
        }, 0);

        const totalChiTieuAnThem = danhSachChiTieuAnThem.reduce((acc, cur) => {
            const total = cur.danhSachMucAn.reduce(
                (total, item) => total + item.thanhTien,
                0,
            );

            return acc + total;
        }, 0);

        //FILE CSS
        worksheet.getColumn(1).width = 6;
        worksheet.getColumn(2).width = 40;
        worksheet.getColumn(3).width = 15;
        worksheet.getColumn(4).width = 15;
        worksheet.getColumn(5).width = 20;
        worksheet.getColumn(6).width = 15;
        worksheet.getColumn(7).width = 15;
        worksheet.getColumn(8).width = 20;

        // ĐƠN VỊ
        worksheet.getCell('A2').font = {
            ...fontBold,
            underline: true,
        };

        // TIÊU ĐỀ
        worksheet.getCell('C1').font = {
            ...fontBold,
            size: 18,
        };

        // MẪU
        worksheet.getCell('G1').font = {
            ...fontDefault,
            italic: true,
        };

        worksheet.addRow([]);
        worksheet.addRow([
            '',
            '1. Ấn định số tiền thanh toán là: ',
            `${this.transformDate(
                (totalChiTieuAn + totalChiTieuAnThem).toString(),
            )}`,
            '',
            '',
            '',
            '',
            '',
        ]).font = fontDefault;
        worksheet.addRow(['', '2. Giải thích: ', '', '', '', '', '', '']).font =
            fontDefault;
        worksheet.addRow([
            '',
            '',
            '',
            '',
            '',
            '',
            `Ngày ${this.getDayMonthYear(
                this.currentDate,
                'day',
            )} tháng ${this.getDayMonthYear(
                this.currentDate,
                'month',
            )} năm ${this.getDayMonthYear(this.currentDate, 'year')}`,
            '',
        ]).font = {
            ...fontDefault,
            italic: true,
        };

        let signatureRow1 = worksheet.addRow([
            '',
            'NGƯỜI LẬP BÁO CÁO',
            '',
            '',
            '',
            '',
            'THỦ TRƯỞNG ĐƠN VỊ',
            '',
        ]);
        signatureRow1.font = fontBold;
        signatureRow1.alignment = alignmentCenter;

        worksheet.getCell(`G${rowsSkipped + 4}`).alignment = alignmentCenter;

        worksheet.mergeCells(`C${rowsSkipped + 2}:H${rowsSkipped + 2}`);
        worksheet.mergeCells(`C${rowsSkipped + 3}:H${rowsSkipped + 3}`);
        worksheet.mergeCells(`G${rowsSkipped + 4}:H${rowsSkipped + 4}`);
        worksheet.mergeCells(`G${rowsSkipped + 5}:H${rowsSkipped + 5}`);
        worksheet.addRow([]);
        worksheet.addRow([]);
        worksheet.addRow([]);
        worksheet.addRow([]);
        worksheet.addRow([]);

        let signatureRow2 = worksheet.addRow([
            '',
            `${this._dataService.getNguoiLapBaoCao()}`,
            '',
            '',
            '',
            '',
            `${this._dataService.getTenThuTruongKyDuyet()}`,
            '',
        ]);
        signatureRow2.font = fontBold;
        signatureRow2.alignment = alignmentCenter;
        worksheet.mergeCells(`G${rowsSkipped + 11}:H${rowsSkipped + 11}`);

        worksheet.addRow([]);
        worksheet.addRow([]);

        worksheet.addRow([
            '',
            'Xét duyệt số tiền được thanh toán là:……………………………………………………………………………………………………………………………',
            '',
            '',
            '',
            '',
            '',
            '',
        ]).font = fontDefault;
        worksheet.addRow([
            '',
            '……………………………………………………………………………………………………………………………………………………………………………………………',
        ]).font = fontDefault;
        worksheet.addRow([]);
        worksheet.addRow([
            '',
            '',
            '',
            '',
            '',
            '',
            'TRƯỞNG BAN TÀI CHÍNH',
            '',
        ]).font = fontBold;
        worksheet.mergeCells(`B${rowsSkipped + 14}:H${rowsSkipped + 14}`);
        worksheet.mergeCells(`B${rowsSkipped + 15}:H${rowsSkipped + 15}`);
        worksheet.addRow([]);
        worksheet.addRow([]);
        worksheet.mergeCells(`G${rowsSkipped + 17}:H${rowsSkipped + 17}`);
        worksheet.getCell(`G${rowsSkipped + 17}`).alignment = alignmentCenter;

        const excelBuffer: any = workbook.xlsx.writeBuffer();
        workbook.xlsx.writeBuffer().then(function (buffer) {
            // Done buffering
            //   this.saveExcelFile(buffer, fileName)
            //
            const data: Blob = new Blob([buffer], {
                type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            });
            FileSaver.saveAs(data, filename);
        });
    }

    public generatePurchaseManagementExcel(data, filename): void {
        let { chiTiet } = data;
        let options = {
            filename,
            useStyles: true,
            useSharedStrings: true,
        };
        let textMoney = 'Thành tiền';
        let locationTotal = '';
        let workbook = new ExcelJS.Workbook(options);

        const title = [
            `${this._dataService
                .getTenDonViCapTrenTrucTiep()
                .name.toUpperCase()}`,
            '',
            'BẢNG KÊ MUA HÀNG',
            '',
            '',
            'Mẫu số: C34 - HĐ',
            '',
        ];
        const title2 = [
            `${this._dataService.getDonViThuocQuanLy().name.toUpperCase()}`,
            '',
            `Ngày ${this.getDayMonthYear(
                data.ngayMua,
                'day',
            )} tháng ${this.getDayMonthYear(
                data.ngayMua,
                'month',
            )} năm ${this.getDayMonthYear(data.ngayMua, 'year')}`,
            '',
            '',
            'Số:…………..',
        ];

        let listColumnKey = [
            'TT',
            'tenLttpChatDot',
            'diaChiMuaHangName',
            'donViTinh',
            'soLuong',
            'donGia',
            'thanhTien',
        ];
        let dataDetail = chiTiet.map((item, index) =>
            listColumnKey.map((E) => {
                if (E === 'TT') {
                    return index + 1;
                }
                if (item[E] == null) {
                    return ' ';
                }

                return item[E];
            }),
        );

        let totalRowTable = new Array(dataDetail[0].length).fill('', 0);
        totalRowTable[1] = 'CỘNG';
        totalRowTable[dataDetail[0].length - 1] = data.tongTien;
        dataDetail.push(new Array(dataDetail[0].length).fill('', 0));
        dataDetail.push(totalRowTable);

        let listColumnTitle = listColumnKey.map((item) => {
            switch (item) {
                case 'tenLttpChatDot':
                    return 'Tên quy cách, phẩm chất, vật tư hàng hóa, dụng cụ';
                case 'donViTinh':
                    return 'ĐVT';
                default:
                    return this.getColumnTitle(item);
            }
        });
        let alignmentCenter = {
            vertical: 'middle',
            horizontal: 'center',
        };

        let fontDefault = {
            name: 'Times New Roman',
            family: 1,
            size: 13,
        };

        let fontBold = {
            ...fontDefault,
            bold: true,
        };

        let fontTextMediumBold = {
            size: 14,
            ...fontBold,
        };

        var worksheet = workbook.addWorksheet(filename, {
            pageSetup: {
                paperSize: 9,
                orientation: 'portrait',
                fitToPage: true,
            }, //portrait,landscape
        });
        worksheet.pageSetup.printTitlesRow = '10:11';

        let titleRow = worksheet.addRow(title);
        let titleRow2 = worksheet.addRow(title2);
        worksheet.mergeCells('A1:B1');
        worksheet.mergeCells('A2:B2');
        worksheet.mergeCells('C1:E1');
        worksheet.mergeCells('C2:E2');
        worksheet.mergeCells('F1:G1');
        worksheet.mergeCells('F2:G2');

        worksheet.addRow([]);
        worksheet.addRow([]);
        worksheet.addRow([]);

        worksheet.addRow(['', '', '', '', '', 'Quyển số:…………..']).font =
            fontDefault;

        worksheet.addRow([
            '',
            `Họ tên người mua:`,
            `${data.nguoiMua}`,
            '',
            '',
            'Nợ:…………..',
        ]).font = fontDefault;

        worksheet.addRow([
            '',
            `Bộ phận (phòng ban):`,
            `${data.boPhan}`,
            '',
            '',
            'Có:…………..',
        ]).font = fontDefault;

        worksheet.mergeCells('F6:G6');
        worksheet.mergeCells('F7:G7');
        worksheet.mergeCells('F8:G8');

        worksheet.addRow([]);

        //Add Header Row
        let headerRow = worksheet.addRow(listColumnTitle);
        // Cell Style : Fill and Border
        headerRow.eachCell((cell, number) => {
            cell.border = {
                top: { style: 'thin' },
                left: { style: 'thin' },
                bottom: { style: 'thin' },
                right: { style: 'thin' },
            };

            cell.alignment = {
                wrapText: true,
                vertical: 'middle',
                horizontal: 'center',
            };

            cell.font = {
                ...fontBold,
            };
        });

        let headerRow2 = worksheet.addRow(['A', 'B', 'C', 'D', '1', '2', '3']);
        headerRow2.eachCell((cell, number) => {
            cell.border = {
                top: { style: 'thin' },
                left: { style: 'thin' },
                bottom: { style: 'thin' },
                right: { style: 'thin' },
            };

            cell.alignment = {
                wrapText: true,
                vertical: 'middle',
                horizontal: 'center',
            };

            cell.font = {
                ...fontBold,
            };
        });

        // Add Data and Conditional Formatting
        dataDetail.forEach((d, index, array) => {
            let row = worksheet.addRow(d);
            let qty = row.getCell(6);

            row.eachCell((cell, number) => {
                // Default
                cell.alignment = {
                    wrapText: true,
                    vertical: 'middle',
                };

                if (number === 1) {
                    cell.alignment = {
                        wrapText: true,
                        vertical: 'middle',
                        horizontal: 'center',
                    };
                }
                if (number > 3 && number < 6) {
                    cell.numFmt = '""#,##0.0;[Red]-""#,##0.000';
                }
                if (number > 5 && number < 8) {
                    cell.numFmt = '""#,##0;[Red]-""#,##0.000';
                }
                cell.font = fontDefault;

                //Last row table change style border
                if (index === array.length - 1) {
                    cell.border = {
                        top: { style: 'thin' },
                        left: { style: 'thin' },
                        bottom: { style: 'thin' },
                        right: { style: 'thin' },
                    };

                    cell.font = {
                        ...fontBold,
                    };

                    if (number === 2) {
                        cell.alignment = alignmentCenter;
                    }
                } else {
                    cell.border = {
                        top: { style: 'dotted' },
                        left: { style: 'thin' },
                        bottom: { style: 'dotted' },
                        right: { style: 'thin' },
                    };
                }
            });
        });

        const rowsSkip = 11 + dataDetail.length;

        worksheet.addRow([]);

        worksheet.addRow([
            '',
            'Tổng số tiền bằng chữ:',
            `${this.transformDate(data.tongTien.toString())}`,
        ]).font = fontDefault;

        worksheet.addRow([
            '',
            '* Ghi chú:…………………………………………………………………………………………………………….',
        ]).font = fontDefault;

        worksheet.addRow([]);

        // Hàng ngày tháng năm ký
        let dateSignRow = worksheet.addRow([
            '',
            '',
            '',
            '',
            '',
            `Ngày ${this.getDayMonthYear(
                this.currentDate,
                'day',
            )} tháng ${this.getDayMonthYear(
                this.currentDate,
                'month',
            )} năm ${this.getDayMonthYear(this.currentDate, 'year')}`,
        ]);
        dateSignRow.alignment = alignmentCenter;
        dateSignRow.font = {
            ...fontDefault,
            italic: true,
        };

        // Hàng danh mục ký
        let signatureRow = worksheet.addRow([
            '',
            'Người mua ',
            'Quản lý',
            'Trợ lý hậu cần',
            ' ',
            'Thủ trường đơn vị',
        ]);
        signatureRow.alignment = alignmentCenter;
        signatureRow.font = fontBold;

        worksheet.mergeCells(`C${rowsSkip + 2}:G${rowsSkip + 2}`);
        worksheet.mergeCells(`B${rowsSkip + 3}:G${rowsSkip + 3}`);
        worksheet.mergeCells(`D${rowsSkip + 6}:E${rowsSkip + 6}`);
        worksheet.mergeCells(`F${rowsSkip + 5}:G${rowsSkip + 5}`);
        worksheet.mergeCells(`F${rowsSkip + 6}:G${rowsSkip + 6}`);

        worksheet.addRow([]);
        worksheet.addRow([]);
        worksheet.addRow([]);
        worksheet.addRow([]);
        worksheet.addRow([]);

        // Hàng chữ ký
        let signatureRow2 = worksheet.addRow([
            '',
            `${!this.isEmty(data.nguoiMua) ? data.nguoiMua : ''}`,
            `${!this.isEmty(this._dataService.getQuanLyBep())
                ? this._dataService.getQuanLyBep()
                : ''
            }`,
            `${!this.isEmty(this._dataService.getTroLyHauCan())
                ? this._dataService.getTroLyHauCan()
                : ''
            }`,
            '',
            `${!this.isEmty(this._dataService.getTenThuTruongKyDuyet())
                ? this._dataService.getTenThuTruongKyDuyet()
                : ''
            }`,
        ]);
        signatureRow2.alignment = alignmentCenter;
        signatureRow2.font = fontBold;

        worksheet.mergeCells(`D${rowsSkip + 12}:E${rowsSkip + 12}`);
        worksheet.mergeCells(`F${rowsSkip + 12}:G${rowsSkip + 12}`);

        // worksheet.mergeCells('B6:H6');

        worksheet.getColumn(1).width = 5;
        worksheet.getColumn(2).width = 30;
        worksheet.getColumn(3).width = 40;
        worksheet.getColumn(4).width = 15;
        worksheet.getColumn(5).width = 15;
        worksheet.getColumn(6).width = 15;
        worksheet.getColumn(7).width = 15;
        worksheet.getColumn(8).width = 15;
        // worksheet.getColumn(9).width = 20;

        //FILE CSS
        titleRow.alignment = alignmentCenter;
        titleRow2.alignment = alignmentCenter;

        // Trường sĩ quan thông tin
        worksheet.getCell('A1').font = {
            ...fontDefault,
        };

        // TIÊU ĐỀ
        worksheet.getCell('C1').font = {
            ...fontBold,
            size: 18,
        };

        // ĐƠN VỊ
        worksheet.getCell('A2').font = {
            ...fontBold,
            underline: true,
        };

        // Ngày ... tháng ... năm ...
        worksheet.getCell('C2').font = {
            ...fontDefault,
            italic: true,
        };
        worksheet.getCell('C2').alignment = {
            vertical: 'middle',
            horizontal: 'center',
        };

        // Mẫu số, Biểu, ....
        worksheet.getCell('G1').font = {
            ...fontDefault,
            italic: true,
        };
        worksheet.getCell('G2').font = {
            ...fontDefault,
        };

        // Quyển số, Nợ, Có, ...
        worksheet.getCell('G6').alignment = alignmentCenter;
        worksheet.getCell('G7').alignment = alignmentCenter;
        worksheet.getCell('G8').alignment = alignmentCenter;

        const excelBuffer: any = workbook.xlsx.writeBuffer();
        workbook.xlsx.writeBuffer().then(function (buffer) {
            // Done buffering
            //   this.saveExcelFile(buffer, fileName)
            //
            const data: Blob = new Blob([buffer], {
                type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            });
            FileSaver.saveAs(data, `${filename}.xlsx`);
        });
    }

    public generatePureStoreExcel(data, filename): void {
        let { chiTiet } = data;
        let options = {
            filename,
            useStyles: true,
            useSharedStrings: true,
        };
        let textMoney = 'Thành tiền';
        let locationTotal = '';
        let workbook = new ExcelJS.Workbook(options);

        let lengthTable = chiTiet.length;
        let min = 5,
            medium = 15,
            large = 20;

        let listColumnKey = [
            'TT',
            'tenLttpChatDot',
            'donViTinh',
            'donGia',
            'soLuong',
            'thanhTien',
            'soLuongThucTe',
            'thanhTienThucTe',
            'soLuongThua',
            'thanhTienThua',
            'soLuongThieu',
            'thanhTienThieu',
            'ghiChu',
        ];
        let dataDetail = chiTiet.map((item, index) =>
            listColumnKey.map((E) => {
                if (E === 'TT') {
                    return index + 1;
                }
                if (!item[E]) {
                    return ' ';
                }

                return item[E];
            }),
        );
        let thanhTienTS = chiTiet.map((item) => item.thanhTien);
        let thanhTienTK = chiTiet.map((item) => item.thanhTienThucTe);
        let totalRowTable = new Array(listColumnKey.length).fill('', 0);
        dataDetail.push(new Array(dataDetail[0].length).fill('', 0)); //Empty row
        dataDetail.push(totalRowTable);

        let listColumnTitle = listColumnKey.map((item) => {
            switch (item) {
                case 'soLuong':
                    return 'CÒN TRÊN SỔ (TS)';
                case 'soLuongThucTe':
                    return 'THỰC KIỂM KHO (TK)	';
                case 'soLuongThua':
                    return 'SO SÁNH';
                case 'donViTinh':
                    return 'ĐVT';
                case 'tenLttpChatDot':
                    return 'TÊN LƯƠNG THỰC THỰC PHẨM CHẤT ĐỐT';
                case 'donGia':
                    return 'Đơn giá';

                default:
                    return this.getColumnTitle(item);
            }
        });
        let alignmentCenter = {
            vertical: 'middle',
            horizontal: 'center',
            wrapText: true,
        };

        let fontDefault = {
            name: 'Times New Roman',
            family: 1,
            size: 13,
        };

        let fontBold = {
            ...fontDefault,
            bold: true,
        };

        var worksheet = workbook.addWorksheet(filename, {
            pageSetup: {
                paperSize: 9,
                orientation: 'portrait',
                fitToPage: true,
            }, //portrait,landscape
        });
        worksheet.pageSetup.printTitlesRow = '6:8';

        // TRƯỜNG
        worksheet.getCell('A1').value = `${this._dataService.getTenDonViCapTrenTrucTiep().name.toUpperCase()}`;
        worksheet.getCell('A1').font = fontDefault;
        worksheet.getCell('A1').alignment = alignmentCenter;
        worksheet.mergeCells('A1:B1');

        // ĐƠN VỊ
        worksheet.getCell('A2').value = `${this._dataService.getDonViThuocQuanLy().name.toUpperCase()}`;
        worksheet.getCell('A2').font = {
            ...fontBold,
            underline: true,
        };
        worksheet.getCell('A2').alignment = alignmentCenter;
        worksheet.mergeCells('A2:B2');

        // TIÊU ĐỀ
        worksheet.getCell('C1').value = 'BIÊN BẢN TỊNH KHO BÀN GIAO';
        worksheet.getCell('C1').font = {
            ...fontBold,
            size: 18,
        };
        worksheet.getCell('C1').alignment = alignmentCenter;
        worksheet.mergeCells('C1:J2');

        worksheet.getCell('C3').value = `Hôm nay ngày ${this.getDayMonthYear(
            data.ngayTao,
            'day',
        )} tháng ${this.getDayMonthYear(
            data.ngayTao,
            'month',
        )} năm ${this.getDayMonthYear(
            data.ngayTao,
            'year',
        )}, chúng tôi đã cân đong lại lương thực, thực phẩm, chất đốt tồn kho cuối tháng`;
        worksheet.getCell('C3').font = fontDefault;
        worksheet.getCell('C3').alignment = alignmentCenter;
        worksheet.mergeCells('C3:J3');

        worksheet.addRow([]);

        worksheet.getCell('L5').value = 'ĐVT: đồng';
        worksheet.getCell('L5').font = {
            ...fontDefault,
            italic: true,
        };
        worksheet.getCell('L5').alignment = alignmentCenter;

        //Add Header Row
        let headerRowEmpty = new Array(listColumnTitle.length).fill('', 0);
        let head2 = [...headerRowEmpty];
        head2[4] = 'Số lượng';
        head2[5] = 'Thành tiền';
        head2[6] = 'Số lượng';
        head2[7] = 'Thành tiền';
        head2[8] = 'Thừa';
        head2[10] = 'Thiếu';
        let head3 = [...headerRowEmpty];
        head3[8] = 'Số lượng';
        head3[9] = 'Thành tiền';
        head3[10] = 'Số lượng';
        head3[11] = 'Thành tiền';
        let headerRow = worksheet.addRow(listColumnTitle);
        let headerRow2 = worksheet.addRow(head2);
        let headerRow3 = worksheet.addRow(head3);

        //Merged title row 1
        worksheet.mergeCells('E6:F6');
        worksheet.mergeCells('G6:H6');
        worksheet.mergeCells('I6:L6');

        worksheet.mergeCells('A6:A8');
        worksheet.mergeCells('B6:B8');
        worksheet.mergeCells('C6:C8');
        worksheet.mergeCells('D6:D8');
        //Merged title row 2
        worksheet.mergeCells('I7:J7');
        worksheet.mergeCells('K7:L7');

        worksheet.mergeCells('E7:E8');
        worksheet.mergeCells('F7:F8');
        worksheet.mergeCells('G7:G8');
        worksheet.mergeCells('H7:H8');
        worksheet.mergeCells('M6:M8');
        //Merged title row Done

        // Hàng đầu tiên của bảng
        headerRow.eachCell((cell, number) => {
            cell.border = {
                top: { style: 'thin' },
                left: { style: 'thin' },
                bottom: { style: 'thin' },
                right: { style: 'thin' },
            };

            cell.alignment = {
                ...alignmentCenter,
            };

            cell.font = {
                ...fontBold,
            };
        });

        // Hàng 2 của bảng
        headerRow2.eachCell((cell, number) => {
            cell.border = {
                top: { style: 'thin' },
                left: { style: 'thin' },
                bottom: { style: 'thin' },
                right: { style: 'thin' },
            };

            cell.alignment = {
                ...alignmentCenter,
            };

            if (number > 4 && number <= 12) {
                cell.font = {
                    ...fontDefault,
                };
            } else {
                cell.font = {
                    ...fontBold,
                };
            }
        });

        // Hàng 3 của bảng
        headerRow3.eachCell((cell, number) => {
            cell.border = {
                top: { style: 'thin' },
                left: { style: 'thin' },
                bottom: { style: 'thin' },
                right: { style: 'thin' },
            };

            cell.alignment = {
                ...alignmentCenter,
            };

            if (number > 4 && number <= 12) {
                cell.font = {
                    ...fontDefault,
                };
            } else {
                cell.font = {
                    ...fontBold,
                };
            }
        });

        // Add Data and Conditional Formatting
        dataDetail.forEach((d, index, array) => {
            if (index === dataDetail.length - 1) {
                d[1] = 'Cộng';
                d[5] = this.sumWithInitial(thanhTienTS);
                d[7] = this.sumWithInitial(thanhTienTK);
            }

            let row = worksheet.addRow(d);
            let qty = row.getCell(6);

            row.eachCell((cell, number) => {
                // Default
                cell.font = fontDefault;

                cell.alignment = {
                    wrapText: true,
                    vertical: 'middle',
                };

                if (number === 1) {
                    cell.alignment = {
                        wrapText: true,
                        vertical: 'middle',
                        horizontal: 'center',
                    };
                }

                if (number > 2 && !this.isEven(number)) {
                    cell.numFmt = '""#,##0.0;[Red]-""#,##0.000';
                }
                if (number > 2 && this.isEven(number)) {
                    cell.numFmt = '""#,##0;[Red]-""#,##0.000';
                }

                //Last row table change style border
                if (index === array.length - 1) {
                    cell.border = {
                        top: { style: 'thin' },
                        left: { style: 'thin' },
                        bottom: { style: 'thin' },
                        right: { style: 'thin' },
                    };

                    cell.font = {
                        ...fontBold,
                    };

                    if (number === 2) {
                        cell.alignment = alignmentCenter;
                    }
                } else {
                    cell.border = {
                        top: { style: 'dotted' },
                        left: { style: 'thin' },
                        bottom: { style: 'dotted' },
                        right: { style: 'thin' },
                    };
                }
            });
        });

        worksheet.addRow([]);

        worksheet.addRow([
            '',
            `II. LÝ DO THỪA, THIẾU VÀ Ý KIẾN GIẢI QUYẾT ………………………………………………………………………………………………………………………………………………………………………………..`,
        ]).font = fontBold;
        worksheet.addRow([
            '',
            '…………………………………………………………………………………………………………………………………………………………………………………………………………………………………………………………………………………………………..',
        ]).font = fontBold;
        worksheet.addRow([
            '',
            `Chúng tôi đã kiểm tra và xác nhận phần ghi chép này là đúng.`,
        ]).font = fontDefault;

        //TransformDate max value is 1b, If more value can have bug

        worksheet.addRow([]);
        let newRowTotal = worksheet.addRow([
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            `Ngày ${this.getDayMonthYear(
                this.currentDate,
                'day',
            )} tháng ${this.getDayMonthYear(
                this.currentDate,
                'month',
            )} năm ${this.getDayMonthYear(this.currentDate, 'year')}`,
        ]);
        newRowTotal.alignment = alignmentCenter;
        newRowTotal.font = {
            ...fontDefault,
            italic: true,
        };

        let signatureRow = worksheet.addRow([
            '',
            'QUẢN LÝ',
            '',
            '',
            'TỔ KINH TẾ',
            '',
            '',
            '',
            '',
            'CHỈ HUY ĐƠN VỊ',
        ]);
        signatureRow.alignment = alignmentCenter;
        signatureRow.font = fontBold;

        worksheet.addRow([]);
        worksheet.addRow([]);
        worksheet.addRow([]);
        worksheet.addRow([]);
        worksheet.addRow([]);

        let signatureRow2 = worksheet.addRow([
            '',
            `${this._dataService.getQuanLyBep()}`,
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            `${this._dataService.getTenThuTruongKyDuyet()}`,
        ]);
        signatureRow2.alignment = alignmentCenter;
        signatureRow2.font = fontBold;

        const rowsSkip = 8 + dataDetail.length + 1;

        // GET TOTAL
        worksheet.getCell(`L${rowsSkip - 1}`).value = {
            formula: `SUM(L9:L${rowsSkip - 2})`,
        };

        worksheet.mergeCells(`B${rowsSkip + 1}:M${rowsSkip + 1}`);
        worksheet.mergeCells(`B${rowsSkip + 2}:M${rowsSkip + 2}`);
        worksheet.mergeCells(`B${rowsSkip + 3}:M${rowsSkip + 3}`);
        worksheet.mergeCells(`J${rowsSkip + 5}:M${rowsSkip + 5}`);
        // worksheet.mergeCells(`B${rowsSkip + 6}:D${rowsSkip + 6}`);
        worksheet.mergeCells(`E${rowsSkip + 6}:H${rowsSkip + 6}`);
        worksheet.mergeCells(`J${rowsSkip + 6}:M${rowsSkip + 6}`);
        worksheet.mergeCells(`E${rowsSkip + 12}:H${rowsSkip + 12}`);
        worksheet.mergeCells(`J${rowsSkip + 12}:M${rowsSkip + 12}`);

        worksheet.getColumn(1).width = min;
        worksheet.getColumn(2).width = 40;
        worksheet.getColumn(3).width = 10;
        worksheet.getColumn(4).width = medium;
        worksheet.getColumn(5).width = 12;
        worksheet.getColumn(6).width = medium;
        worksheet.getColumn(7).width = 12;
        worksheet.getColumn(8).width = medium;
        worksheet.getColumn(9).width = 12;
        worksheet.getColumn(10).width = medium;
        worksheet.getColumn(11).width = 12;
        worksheet.getColumn(12).width = medium;
        worksheet.getColumn(13).width = medium;
        // worksheet.getColumn(9).width = 20;

        const excelBuffer: any = workbook.xlsx.writeBuffer();
        workbook.xlsx.writeBuffer().then(function (buffer) {
            // Done buffering
            //   this.saveExcelFile(buffer, fileName)
            //
            const data: Blob = new Blob([buffer], {
                type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            });
            FileSaver.saveAs(data, filename);
        });
    }

    public generateFinancialReportExcel(data, filename): void {
        let {
            congKhaiTaiChinh,
            nhapXuatTrongNgay,
            thanhTienCongNhap,
            thanhTienCongXuat,
            thanhTienNgayTruoc,
        } = data;
        let {
            buGiaGao,
            cong,
            congChi,
            congThu,
            thieuHomTruoc,
            thuCuaKhach,
            thuaHomTruoc,
            thuaTrongNgay,
            tienDaSuDung,
            truyLinh,
            danhSachAnThem,
        } = congKhaiTaiChinh;
        let chiTiet = nhapXuatTrongNgay;
        let ngayTao = new Date();
        let options = {
            filename,
            useStyles: true,
            useSharedStrings: true,
        };

        const NUMBER_LINE_CONSTAIN = 37;
        let initialRowDetai = new Array(NUMBER_LINE_CONSTAIN).fill('', 0);
        let textMoney = 'Thành tiền';
        let locationTotal = '';
        let workbook = new ExcelJS.Workbook(options);

        let lengthTable = data.length;
        let min = 5,
            medium = 15,
            large = 20;

        let listColumnKey = [
            'id',
            'tenLttpChatDot',
            'donViTinh',
            'donGia',
            'soLuongNgayTruoc',
            'thanhTienNgayTruoc',
            'nhapTrenBdMuaTT',
            'nhapTrenBdTGCB',
            'nhapDonViBdMuaTT',
            'nhapDonViBdTGCB',
            'thanhTienNhap',
            'soLuongCongNhap',
            'thanhTienNhap',
            'soLuongXuatAn',
            'thanhTienXuatAn',
            'soLuongXuatKhac',
            'thanhTienXuatKhac',
            'soLuongCongXuat',
            'thanhTienCongXuat',
        ];
        let dataDetail = chiTiet
            ? chiTiet.map((item, index) =>
                listColumnKey.map((E) => {
                    if (E === 'id') {
                        return index + 1;
                    }
                    if (!item[E]) {
                        return ' ';
                    }

                    return item[E];
                }),
            )
            : [];

        // let thanhTienTK = chiTiet.map((item) => item.thanhTienThucTe);
        let totalRowTable = new Array(listColumnKey.length).fill('', 0);
        if (dataDetail.length > 0) {
            dataDetail.push(new Array(dataDetail[0].length).fill('', 0)); //Empty row
            dataDetail.push(totalRowTable);
        }

        let numberForLoop = NUMBER_LINE_CONSTAIN - dataDetail.length;

        for (let index = 0; index < numberForLoop; index++) {
            dataDetail.push(new Array(listColumnKey.length).fill('', 0)); //Empty row
        }

        let listColumnTitle = listColumnKey.map((item) => {
            switch (item) {
                // case 'soLuong':
                //     return 'CÒN TRÊN SỔ (TS)';
                // case 'soLuongThucTe':
                //     return 'THỰC KIỂM KHO (TK)	';
                // case 'soLuongThua':
                //     return 'SO SÁNH';
                // case 'donViTinh':
                //     return 'DVT';

                default:
                    return this.getColumnTitle(item);
            }
        });
        let alignmentCenter = {
            vertical: 'middle',
            horizontal: 'center',
        };

        let fontDefault = {
            name: 'Times New Roman',
            family: 1,
            size: 13,
        };

        let fontBold = {
            ...fontDefault,
            bold: true,
        };

        var worksheet = workbook.addWorksheet(filename, {
            pageSetup: {
                paperSize: 9,
                orientation: 'landscape',
                fitToPage: true,
            }, //portrait,landscape
        });
        worksheet.pageSetup.printTitlesRow = '6:9';

        // THÊM TÊN CHỦ QUẢN
        worksheet.getCell('A1').value = `${this._dataService.getTenDonViCapTrenTrucTiep().name.toUpperCase()}`;
        worksheet.getCell('A1').alignment = alignmentCenter;
        worksheet.getCell('A1').font = fontDefault;
        worksheet.mergeCells('A1:D1');

        // THÊM TÊN ĐƠN VỊ QUẢN LÝ
        worksheet.getCell('A2').value = `${this._dataService.getDonViThuocQuanLy().name.toUpperCase()}`;
        worksheet.getCell('A2').alignment = alignmentCenter;
        worksheet.getCell('A2').font = {
            ...fontBold,
            underline: true,
        };
        worksheet.mergeCells('A2:D2');

        // THÊM TIÊU ĐỀ
        worksheet.getCell('E1').value = 'QUYẾT TOÁN SỬ DỤNG NGÂN SÁCH QUÂN NHU';
        worksheet.getCell('E1').alignment = alignmentCenter;
        worksheet.getCell('E1').font = {
            ...fontBold,
            size: 18,
        };
        worksheet.mergeCells('E1:R2');

        worksheet.getCell('E3').value = `Kèm theo quyết toán số: ngày ${this.getDayMonthYear(
            ngayTao,
            'day',
        )} tháng ${this.getDayMonthYear(
            ngayTao,
            'month',
        )} năm ${this.getDayMonthYear(ngayTao, 'year')}`;
        worksheet.getCell('E3').alignment = alignmentCenter;
        worksheet.getCell('E3').font = fontDefault;
        worksheet.mergeCells('E3:R3');

        worksheet.addRow([]);

        // THÊM ĐVT
        worksheet.getCell('T5').value = 'ĐVT: đồng';
        worksheet.getCell('T5').alignment = alignmentCenter;
        worksheet.getCell('T5').font = {
            ...fontDefault,
            italic: true,
        };
        worksheet.mergeCells('T5:U5');

        //Add Header Row
        let headerRowEmpty = new Array(listColumnTitle.length).fill('', 0);
        let head1 = [
            'TT',
            'Tên LT-TP, chất đốt',
            'ĐVT',
            'Đơn giá',
            'Ngày trước chuyển qua',
            '',
            'Nhập trong ngày',
            '',
            '',
            '',
            '',
            'Cộng nhập',
            '',
            'Xuất ăn trong ngày	',
            '',
            'Xuất khác	',
            '',
            'Cộng xuất',
            '',
            `Công khai tài chính \n ${this.getDayMonthYear(
                data['thoiGian'],
                'day',
            )} tháng ${this.getDayMonthYear(
                data['thoiGian'],
                'month',
            )} năm ${this.getDayMonthYear(data['thoiGian'], 'year')}`,
            '',
        ];
        let head2 = [...headerRowEmpty];
        head2 = [
            '',
            '',
            '',
            '',
            'Số lượng',
            'Thành tiền',
            'Số lượng',
            '',
            '',
            '',
            'Thành tiền',
            'Số lượng',
            'Thành tiền',
            'Số lượng',
            'Thành tiền',
            'Số lượng',
            'Thành tiền',
            'Số lượng',
            'Thành tiền',

            '',
        ];

        let head3 = [...headerRowEmpty];
        head3[6] = 'Trên đảm bảo';
        head3[8] = 'Đơn vị đảm bảo';

        let head4 = [...headerRowEmpty];
        head4[6] = 'Mua TT';
        head4[7] = 'TG - CB';
        head4[8] = 'Mua TT';
        head4[9] = 'TG - CB';

        let headerRow = worksheet.addRow(head1);
        let headerRow2 = worksheet.addRow(head2);
        let headerRow3 = worksheet.addRow(head3);
        let headerRow4 = worksheet.addRow(head4);

        //Merged title row 1
        worksheet.mergeCells('E6:F6');
        worksheet.mergeCells('G6:K6');
        worksheet.mergeCells('L6:M6');
        worksheet.mergeCells('N6:O6');
        worksheet.mergeCells('P6:Q6');
        worksheet.mergeCells('R6:S6');

        worksheet.mergeCells('A6:A9');
        worksheet.mergeCells('B6:B9');
        worksheet.mergeCells('C6:C9');
        worksheet.mergeCells('D6:D9');
        worksheet.mergeCells('T6:U9');
        // //Merged title row 2
        worksheet.mergeCells('G7:J7');

        worksheet.mergeCells('E7:E9');
        worksheet.mergeCells('F7:F9');
        worksheet.mergeCells('K7:K9');
        worksheet.mergeCells('L7:L9');
        worksheet.mergeCells('M7:M9');
        worksheet.mergeCells('N7:N9');
        worksheet.mergeCells('O7:O9');
        worksheet.mergeCells('P7:P9');
        worksheet.mergeCells('Q7:Q9');
        worksheet.mergeCells('R7:R9');
        worksheet.mergeCells('S7:S9');
        //Merged title row 3
        worksheet.mergeCells('G8:H8');
        worksheet.mergeCells('I8:J8');

        //Merged title row Done

        headerRow.eachCell((cell, number) =>
            this.headerTitleCenter(cell, number),
        );
        headerRow2.eachCell((cell, number) =>
            this.headerTitleCenter(cell, number),
        );
        headerRow3.eachCell((cell, number) =>
            this.headerTitleCenter(cell, number),
        );
        headerRow4.eachCell((cell, number) =>
            this.headerTitleCenter(cell, number),
        );

        let dataTaxes = this.convertDataListTaxes(congKhaiTaiChinh.danhSachThu);
        let tempArrayItem = [
            {
                title: 'Công: ',
                value: cong,
            },
            {
                title: this.convertKeyString('thuaHomTruoc'),
                value: thuaHomTruoc,
            },
            {
                title: this.convertKeyString('buGiaGao'),
                value: buGiaGao,
            },
            {
                title: this.convertKeyString('danhSachAnThem'),
                value: this.totalListEarMore(danhSachAnThem),
            },
            {
                title: this.convertKeyString('truyLinh'),
                value: truyLinh,
            },
            {
                title: this.convertKeyString('thuCuaKhach'),
                value: thuCuaKhach,
            },
            {
                title: this.convertKeyString('congThu') + ' :',
                value: congThu,
            },
            {
                title: 'Chi',
                value: '',
            },
            {
                title: this.convertKeyString('tienDaSuDung'),
                value: tienDaSuDung,
            },
            {
                title: this.convertKeyString('thieuHomTruoc'),
                value: thieuHomTruoc,
            },
            {
                title: this.convertKeyString('congChi') + ' :',
                value: congChi,
            },
            {
                title: 'So sánh',
                value: '',
            },
            {
                title: this.convertKeyString('thuaTrongNgay'),
                value: thuaTrongNgay,
            },
        ];
        let temIndexLoopExpan = 0;
        let temIndexLoopExpanData = 0;
        let r1 = 0,
            r2 = 1,
            r3 = 2,
            r4 = 3;
        let list1 = [0],
            list2 = [1],
            list3 = [2],
            list4 = [3];
        let textCase = '';
        let startNumber = 0;

        dataDetail.forEach((d, index, array) => {
            if (index === 0) {
                d.push(...['Thu', '']);
            } else if (index === 1) {
                for (let ids = 0; ids < dataTaxes.length; ids++) {
                    if (list1.includes(ids)) {
                        textCase = '- Mức';
                    }
                    if (list2.includes(ids)) {
                        textCase = '- Sáng';
                    }
                    if (list3.includes(ids)) {
                        textCase = '- Trưa';
                    }
                    if (list4.includes(ids)) {
                        textCase = '- Chiều';
                    }

                    const element = dataTaxes[ids];
                    let contentElement = element.map((ele, index) => {
                        return textCase + '::' + ele;
                    });
                    temIndexLoopExpan++;
                    array[ids + index].push(...contentElement);

                    r1 += 4;
                    r2 += 4;
                    r3 += 4;
                    r4 += 4;
                    list1.push(r1);
                    list2.push(r2);
                    list3.push(r3);
                    list4.push(r4);
                }
            } else if (index > temIndexLoopExpan) {
                temIndexLoopExpanData = index;

                if (temIndexLoopExpanData < array.length) {
                    if (startNumber > tempArrayItem.length - 1) {
                        array[temIndexLoopExpanData].push(...['', '']);
                    } else {
                        array[temIndexLoopExpanData].push(
                            ...[
                                tempArrayItem[startNumber].title,
                                tempArrayItem[startNumber].value,
                            ],
                        );
                        startNumber++;
                    }
                }
            } else {
            }

            if (index === dataDetail.length - 1) {
                d[1] = 'Cộng';
                d[5] = thanhTienNgayTruoc;
                d[12] = thanhTienCongNhap;
                d[18] = thanhTienCongXuat;
            }

            let row = worksheet.addRow(d);
            // row.addPageBreak();
            let qty = row.getCell(6);

            row.eachCell((cell, number) => {
                // Default
                cell.alignment = {
                    wrapText: true,
                    vertical: 'middle',
                };

                cell.font = fontDefault;

                if (number === 1) {
                    cell.alignment = {
                        wrapText: true,
                        vertical: 'middle',
                        horizontal: 'center',
                    };
                }

                // if (number !== 2) {
                //     cell.alignment = {
                //         wrapText: true,
                //     };
                // }

                if (number > 2 && !this.isEven(number)) {
                    cell.numFmt = '""#,##0;[Red]-""#,##0.000';
                }
                if (number > 2 && this.isEven(number)) {
                    cell.numFmt = '""#,##0;[Red]-""#,##0.000';
                }

                //Last row table change style border
                if (index === array.length - 1) {
                    cell.border = {
                        top: { style: 'thin' },
                        left: { style: 'thin' },
                        bottom: { style: 'thin' },
                        right: { style: 'thin' },
                    };

                    cell.font = fontBold;

                    if (number === 2) {
                        cell.alignment = alignmentCenter;
                    }
                } else {
                    cell.border = {
                        top: { style: 'dotted' },
                        left: { style: 'thin' },
                        bottom: { style: 'dotted' },
                        right: { style: 'thin' },
                    };
                }
            });
        });

        // this.autoWidth(worksheet);

        // worksheet.getCell('T8').value = 'Thu';
        // worksheet.getCell('T9').value = '- Mức:        - Mức:';
        // worksheet.getCell('T10').value = '+ Sáng:        + Sáng:';
        // worksheet.getCell('T11').value = '+ Trưa:        + Trưa:';
        // worksheet.getCell('T12').value = '+ Chiều:       + Chiều:';
        // worksheet.getCell('T13').value = '- Mức:        - Mức:';
        // worksheet.getCell('T14').value = '+ Sáng:        + Sáng:';
        // worksheet.getCell('T15').value = '+ Trưa:        + Trưa:';
        // worksheet.getCell('T16').value = '+ Chiều:       + Chiều:';
        // worksheet.getCell('T17').value = 'Cộng';
        // worksheet.getCell('T18').value = '- Thừa hôm trước:';
        // worksheet.getCell('T19').value = '- Bù giá gạo:';
        // worksheet.getCell('T20').value = '- Ăn thêm:';
        // worksheet.getCell('T21').value = '- Truy lĩnh:';
        // worksheet.getCell('T22').value = '- Thu của khách:';
        // worksheet.getCell('T23').value = 'Cộng thu: ';
        // worksheet.getCell('T24').value = 'Chi: ';
        // worksheet.getCell('T25').value = '- Tiền đẫ sử dụng:';
        // worksheet.getCell('T26').value = '- Thiếu hôm trước:';
        // worksheet.getCell('T27').value = 'Cộng chi: ';
        // worksheet.getCell('T28').value = '- So sánh:';
        // worksheet.getCell('T29').value = '- Thừa trong ngày:';
        // worksheet.getCell('T29').value = '- Thừa trong ngày:';
        // worksheet.getColumn('T').alignment = {
        //     wrapText: true,
        // };
        //TransformDate max value is 1b, If more value can have bug

        const rowsSkip = 9 + dataDetail.length;

        worksheet.addRow([]);
        worksheet.addRow([]);

        // NGÀY THÁNG NĂM KÝ
        worksheet.getCell(`S${rowsSkip + 3}`).value =
            `Ngày ${this.getDayMonthYear(
                this.currentDate,
                'day',
            )} tháng ${this.getDayMonthYear(
                this.currentDate,
                'month',
            )} năm ${this.getDayMonthYear(this.currentDate, 'year')}`;
        worksheet.getCell(`S${rowsSkip + 3}`).font = {
            ...fontDefault,
            italic: true,
        };
        worksheet.getCell(`S${rowsSkip + 3}`).alignment = alignmentCenter;
        worksheet.mergeCells(`S${rowsSkip + 3}:U${rowsSkip + 3}`);

        // CHỨC DANH
        worksheet.getCell(`A${rowsSkip + 4}`).value = 'QUẢN LÝ';
        worksheet.getCell(`A${rowsSkip + 4}`).font = fontBold;
        worksheet.getCell(`A${rowsSkip + 4}`).alignment = alignmentCenter;
        worksheet.mergeCells(`A${rowsSkip + 4}:E${rowsSkip + 4}`);

        worksheet.getCell(`J${rowsSkip + 4}`).value = 'TRỰC BAN';
        worksheet.getCell(`J${rowsSkip + 4}`).font = fontBold;
        worksheet.getCell(`J${rowsSkip + 4}`).alignment = alignmentCenter;
        worksheet.mergeCells(`J${rowsSkip + 4}:L${rowsSkip + 4}`);

        worksheet.getCell(`S${rowsSkip + 4}`).value = 'CHỈ HUY ĐƠN VỊ';
        worksheet.getCell(`S${rowsSkip + 4}`).font = fontBold;
        worksheet.getCell(`S${rowsSkip + 4}`).alignment = alignmentCenter;
        worksheet.mergeCells(`S${rowsSkip + 4}:U${rowsSkip + 4}`);

        worksheet.addRow([]);
        worksheet.addRow([]);
        worksheet.addRow([]);
        worksheet.addRow([]);
        worksheet.addRow([]);

        // TÊN CHỨC DANH
        worksheet.getCell(`A${rowsSkip + 10}`).value = `${this._dataService.getQuanLyBep()}`;
        worksheet.getCell(`A${rowsSkip + 10}`).font = fontBold;
        worksheet.getCell(`A${rowsSkip + 10}`).alignment = alignmentCenter;
        worksheet.mergeCells(`A${rowsSkip + 10}:E${rowsSkip + 10}`);

        worksheet.getCell(`J${rowsSkip + 10}`).value = `${this._dataService.getTrucBan()}`;
        worksheet.getCell(`J${rowsSkip + 10}`).font = fontBold;
        worksheet.getCell(`J${rowsSkip + 10}`).alignment = alignmentCenter;
        worksheet.mergeCells(`J${rowsSkip + 10}:L${rowsSkip + 10}`);

        worksheet.getCell(`S${rowsSkip + 10}`).value = `${this._dataService.getTenThuTruongKyDuyet()}`;
        worksheet.getCell(`S${rowsSkip + 10}`).font = fontBold;
        worksheet.getCell(`S${rowsSkip + 10}`).alignment = alignmentCenter;
        worksheet.mergeCells(`S${rowsSkip + 10}:U${rowsSkip + 10}`);

        // worksheet.mergeCells('B6:H6');
        //Setup print pagelayout
        worksheet.getColumn(1).width = min;
        worksheet.getColumn(1).alignment = alignmentCenter;
        worksheet.getColumn('B').width = 50;
        worksheet.getColumn('D').width = 15;
        worksheet.getColumn('E').width = 15;
        worksheet.getColumn('F').width = 20;
        worksheet.getColumn('T').width = 20;
        worksheet.getColumn('U').width = 20;
        worksheet.getColumn('K').width = 20;
        worksheet.getColumn('L').width = 15;
        worksheet.getColumn('M').width = 20;
        worksheet.getColumn('N').width = 15;
        worksheet.getColumn('O').width = 20;
        worksheet.getColumn('P').width = 15;
        worksheet.getColumn('S').width = 20;
        worksheet.getColumn('R').width = 15;
        worksheet.getColumn('Q').width = 20;
        // worksheet.getColumn(2).width = large;
        // worksheet.getColumn(3).width = 10;
        // worksheet.getColumn(4).width = medium;
        // worksheet.getColumn(5).width = 10;
        // worksheet.getColumn(6).width = medium;
        // worksheet.getColumn(7).width = 10;
        // worksheet.getColumn(8).width = medium;
        // worksheet.getColumn(9).width = 10;
        // worksheet.getColumn(10).width = medium;
        // worksheet.getColumn(11).width = 10;
        // worksheet.getColumn(12).width = medium;
        // worksheet.getColumn(13).width = medium;
        // worksheet.getColumn(9).width = 20;

        //FILE CSS

        const excelBuffer: any = workbook.xlsx.writeBuffer();
        workbook.xlsx.writeBuffer().then(function (buffer) {
            // Done buffering
            //   this.saveExcelFile(buffer, fileName)
            //
            const data: Blob = new Blob([buffer], {
                type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            });
            FileSaver.saveAs(data, filename);
        });
    }

    public generateExportQuantitiveRegulationExcel(data, filename): void {
        let { dataCook, defaultCookCheck } = data;
        let options = {
            filename,
            useStyles: true,
            useSharedStrings: true,
        };
        let workbook = new ExcelJS.Workbook(options);

        let alignmentCenter = {
            vertical: 'middle',
            horizontal: 'center',
        };

        let fontDefault = {
            name: 'Times New Roman',
            family: 1,
            size: 13,
        };

        let fontBold = {
            ...fontDefault,
            bold: true,
        };

        let fontTitleBlue = {
            name: 'Times New Roman',
            family: 1,
            size: 12,
            bold: true,
            color: { argb: 'ffffffff' },
        };

        var worksheet = workbook.addWorksheet(filename, {
            pageSetup: {
                paperSize: 9,
                orientation: 'portrait',
                fitToPage: true,
            }, //portrait,landscape
        });
        worksheet.pageSetup.printTitlesRow = '5:7';

        // THÊM TÊN CHỦ QUẢN
        worksheet.getCell('A1').value = `${this._dataService.getTenDonViCapTrenTrucTiep().name.toUpperCase()}`;
        worksheet.getCell('A1').alignment = alignmentCenter;
        worksheet.getCell('A1').font = fontDefault;
        worksheet.mergeCells('A1:C1');

        // THÊM TÊN ĐƠN VỊ QUẢN LÝ
        worksheet.getCell('A2').value = `${this._dataService.getDonViThuocQuanLy().name.toUpperCase()}`;
        worksheet.getCell('A2').alignment = alignmentCenter;
        worksheet.getCell('A2').font = {
            ...fontBold,
            underline: true,
        };
        worksheet.mergeCells('A2:C2');

        // THÊM TIÊU ĐỀ
        worksheet.getCell('D1').value = 'KẾT QUẢ BẢO ĐẢM ĐỊNH LƯỢNG LTTP, CHẤT ĐỐT';
        worksheet.getCell('D1').alignment = alignmentCenter;
        worksheet.getCell('D1').font = {
            ...fontBold,
            size: 18,
        };
        worksheet.mergeCells('D1:S2');

        // THÊM THÁNG NĂM BÁO CÁO
        worksheet.getCell('D3').value = `Tháng ${this.getDayMonthYear(
            data.thoiGian,
            'month',
        )} năm ${this.getDayMonthYear(data.thoiGian, 'year')}`;
        worksheet.getCell('D3').alignment = alignmentCenter;
        worksheet.getCell('D3').font = fontDefault;
        worksheet.mergeCells('D3:S3');

        worksheet.addRow([]);

        let firstRowOnTable = ['Ngày', '', '', 'Tên lương thực thực phẩm, chất đốt'];
        let titleNameCook = ['', ''];
        let titleUnitCook = ['', ''];
        let averageSum = ['', 'Cộng bình quân'];
        let averageSumOnlyNumber = [];
        let compareStatusText = [];
        let totalDay = dataCook[0].ketQua.length;

        for (const index in dataCook) {
            titleNameCook.push(dataCook[index].ten);
            titleUnitCook.push(dataCook[index].donViTinh);
            averageSum.push(dataCook[index].congBinhQuan);
            averageSumOnlyNumber.push(dataCook[index].congBinhQuan);
        }

        firstRowOnTable[1] = titleNameCook[2];
        let headerRow1 = worksheet.addRow(firstRowOnTable);
        headerRow1.eachCell((cell, number) =>
            this.headerTitleCenter(cell, number)
        );

        let cellTitleNameCook = worksheet.addRow(titleNameCook);
        cellTitleNameCook.eachCell((cell, number) =>
            this.headerTitleCenter(cell, number)
        );

        let styleMainBlue = {
            fill: {
                type: 'pattern',
                pattern: 'solid',
                bgColor: { argb: 'ff4179b6' },
                fgColor: { argb: 'ff4179b6' },
            },
            font: fontTitleBlue,
            border: {
                top: { style: 'thin', color: { argb: 'ffffffff' } },
                left: { style: 'thin', color: { argb: 'ffffffff' } },
                bottom: { style: 'thin', color: { argb: 'ffffffff' } },
                right: { style: 'thin', color: { argb: 'ffffffff' } },
            },
        };
        cellTitleNameCook.alignment = {
            vertical: 'middle',
            horizontal: 'center',
            textRotation: -90,
            // wrapText: true
        };

        // cellTitleNameCook.font = styleMainBlue.font;
        // cellTitleNameCook.fill = styleMainBlue.fill;
        // cellTitleNameCook.border = styleMainBlue.border;

        let unitsRow = titleUnitCook.map((item, index, array) => {
            if (index === 1) {
                return 'ĐVT';
            }

            if (index === array.length - 1) {
                return 'Kcal';
            }

            if (item === null) {
                return '';
            }

            return item;
        });

        let cellTitleNameCookUnit = worksheet.addRow(unitsRow);
        cellTitleNameCookUnit.eachCell((cell, number) =>
            this.headerTitleCenter(cell, number),
        );

        worksheet.mergeCells('A5:A7');
        worksheet.mergeCells('B5:C6');
        worksheet.mergeCells('B7:C7');

        // Dynamical Merge Cells
        const dynamicColumns = unitsRow
            .filter((unit, index) => index >= 3)
            .map((unit, index) =>
                this.columnToLetter(index + 4),
            );
        const startPosMerge = dynamicColumns[0];
        const endPosMerge = dynamicColumns[dynamicColumns.length - 1];

        worksheet.mergeCells(`${startPosMerge}5:${endPosMerge}5`);

        let detailsRender = [];

        for (let index = 0; index < totalDay; index++) {
            let actualExport = [];
            let averageQuantification = [];
            for (const key in dataCook) {
                let curentDataKey = dataCook[key].ketQua[index];
                let { xuatAnThucTe, dinhLuongBinhQuan } = curentDataKey;

                const countXuatAn = !xuatAnThucTe ? '' : xuatAnThucTe;
                const countDinhLuongBinhQuan = !dinhLuongBinhQuan ? '' : dinhLuongBinhQuan;

                actualExport.push(countXuatAn);
                averageQuantification.push(countDinhLuongBinhQuan);
            }

            detailsRender = [
                ...detailsRender,
                [
                    index + 1,
                    'Xuất ăn thực tế',
                    ...actualExport
                ],
                [
                    '',
                    'Định lượng bình quân',
                    ...averageQuantification,
                ]
            ];

            // worksheet.addRow([index + 1, 'Xuất ăn thực tế', ...actualExport]);
            // worksheet.addRow([
            //     '',
            //     'Định lượng bình quân',
            //     ...averageQuantification,
            // ]).fill = {
            //     type: 'pattern',
            //     pattern: 'solid',
            //     bgColor: { argb: 'ffdddddd' },
            //     fgColor: { argb: 'ffdddddd' },
            // };
        }

        for (let index = 0; index < averageSumOnlyNumber.length; index++) {
            const elementX = averageSumOnlyNumber[index];
            const elementY = defaultCookCheck[index];
            if (elementX > elementY) {
                compareStatusText.push('Vượt');
            } else if (elementX < elementY) {
                compareStatusText.push('Thấp');
            } else {
                compareStatusText.push('Đạt');
            }
        }

        let dataDetail = [
            ...detailsRender,
            averageSum,
            [
                '',
                'Định lượng quy định',
                ...defaultCookCheck,
            ],
            [
                '',
                'So sánh đánh giá',
                ...compareStatusText,
            ],
        ];

        // Add Data and Conditional Formatting
        dataDetail.forEach((d, index, array) => {
            let row = worksheet.addRow(d);

            row.eachCell((cell, number) => {
                // Default
                cell.alignment = {
                    wrapText: true,
                    vertical: 'middle',
                };

                cell.font = fontDefault;

                if (number === 1) {
                    cell.alignment = {
                        wrapText: true,
                        vertical: 'middle',
                        horizontal: 'center',
                    };
                }
                // if (number > 3 && number < 6) {
                //     cell.numFmt = '""#,##0.0;[Red]-""#,##0.000';
                // }
                // if (number > 5 && number < 8) {
                //     cell.numFmt = '""#,##0;[Red]-""#,##0.000';
                // }

                //Last row table change style border
                if (index >= array.length - 3) {
                    cell.border = {
                        top: { style: 'thin' },
                        left: { style: 'thin' },
                        bottom: { style: 'thin' },
                        right: { style: 'thin' },
                    };

                    cell.font = {
                        ...fontBold,
                    };

                    if (number === 2) {
                        cell.alignment = alignmentCenter;
                    }

                    if (index === array.length - 1) {
                        cell.alignment = alignmentCenter;
                    }
                } else {
                    cell.border = {
                        top: { style: 'dotted' },
                        left: { style: 'thin' },
                        bottom: { style: 'dotted' },
                        right: { style: 'thin' },
                    };
                }
            });
        });

        const rowsSkip = 7 + dataDetail.length;

        worksheet.addRow([]);
        worksheet.addRow([]);

        // NGÀY THÁNG NĂM KÝ
        worksheet.getCell(
            `T${rowsSkip + 3}`,
        ).value = `Ngày ${this.getDayMonthYear(
            this.currentDate,
            'day',
        )} tháng ${this.getDayMonthYear(
            this.currentDate,
            'month',
        )} năm ${this.getDayMonthYear(this.currentDate, 'year')}`;
        worksheet.getCell(`T${rowsSkip + 3}`).font = {
            ...fontDefault,
            italic: true,
        };
        worksheet.getCell(`T${rowsSkip + 3}`).alignment = alignmentCenter;
        worksheet.mergeCells(`T${rowsSkip + 3}:W${rowsSkip + 3}`);

        // CHỨC DANH
        worksheet.getCell(`A${rowsSkip + 4}`).value = 'NGƯỜI LẬP BÁO CÁO';
        worksheet.getCell(`A${rowsSkip + 4}`).font = fontBold;
        worksheet.getCell(`A${rowsSkip + 4}`).alignment = alignmentCenter;
        worksheet.mergeCells(`A${rowsSkip + 4}:D${rowsSkip + 4}`);

        worksheet.getCell(`T${rowsSkip + 4}`).value = 'THỦ TRƯỞNG ĐƠN VỊ';
        worksheet.getCell(`T${rowsSkip + 4}`).font = fontBold;
        worksheet.getCell(`T${rowsSkip + 4}`).alignment = alignmentCenter;
        worksheet.mergeCells(`T${rowsSkip + 4}:W${rowsSkip + 4}`);

        worksheet.addRow([]);
        worksheet.addRow([]);
        worksheet.addRow([]);
        worksheet.addRow([]);
        worksheet.addRow([]);

        // TÊN CHỨC DANH
        worksheet.getCell(
            `A${rowsSkip + 10}`,
        ).value = `${this._dataService.getNguoiLapBaoCao()}`;
        worksheet.getCell(`A${rowsSkip + 10}`).font = fontBold;
        worksheet.getCell(`A${rowsSkip + 10}`).alignment = alignmentCenter;
        worksheet.mergeCells(`A${rowsSkip + 10}:D${rowsSkip + 10}`);

        worksheet.getCell(
            `T${rowsSkip + 10}`,
        ).value = `${this._dataService.getTenThuTruongKyDuyet()}`;
        worksheet.getCell(`T${rowsSkip + 10}`).font = fontBold;
        worksheet.getCell(`T${rowsSkip + 10}`).alignment = alignmentCenter;
        worksheet.mergeCells(`T${rowsSkip + 10}:W${rowsSkip + 10}`);

        // rowAverageSum.border = styleMainBlue.border;
        // rowAverageSum.font = {
        //     ...styleMainBlue.font,
        //     color: { argb: 'ffffffff' },
        // };
        // rowAverageSum.fill = styleMainBlue.fill;

        // rowDefaultCookCheck.border = styleMainBlue.border;
        // rowDefaultCookCheck.font = {
        //     ...styleMainBlue.font,
        //     color: { argb: 'fff1c40f' },
        // };
        // rowDefaultCookCheck.fill = styleMainBlue.fill;

        // rowCompareStatusText.border = styleMainBlue.border;
        // rowCompareStatusText.font = styleMainBlue.font;
        // rowCompareStatusText.font = {
        //     ...styleMainBlue.font,
        //     color: { argb: 'ff1deb20' },
        // };
        // rowCompareStatusText.fill = styleMainBlue.fill;

        const currentRowIdx = worksheet.rowCount; // Find out how many rows are there currently
        const endColumnIdx = worksheet.columnCount;
        // merge by start row, start column, end row, end column
        // worksheet.mergeCells(1, 1, 1, endColumnIdx);
        // titleRow.height = 42;
        // worksheet.mergeCells('A2:A3');

        // Add column headers and define column keys and widths
        // Note: these column structures are a workbook-building convenience only,
        // apart from the column width, they will not be fully persisted.
        // const columns = [];
        // for (const index in data) {
        //     columns.push({
        //         header: data[index].ten,
        //         key: this.removeAccents(data[index].ten),
        //         width: 15,
        //     });
        // }

        // console.log('columns::', columns);
        // worksheet.columns = columns;

        // for (const index in data) {
        //     worksheet.addRow({ _id: '1', name: 'Mitchell Starc' });
        // }

        //Update or add dynamic columns
        //Get the empty columns from worksheet. You can get the empty columns number by using `columns` array length
        //For this you have to track all inserted columns in worksheet
        //This will return the next empty columns
        // Cell Style : Fill and Border



        worksheet.getColumn('B').width = 25;
        const excelBuffer: any = workbook.xlsx.writeBuffer();
        workbook.xlsx.writeBuffer().then(function (buffer) {
            // Done buffering
            //   this.saveExcelFile(buffer, fileName)
            //
            const data: Blob = new Blob([buffer], {
                type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            });
            FileSaver.saveAs(data, filename);
        });
    }

    private autoWidth = (worksheet, minimalWidth = 5) => {
        worksheet.columns.forEach((column) => {
            let maxColumnLength = 0;
            column.eachCell({ includeEmpty: true }, (cell) => {
                maxColumnLength = Math.max(
                    maxColumnLength,
                    minimalWidth,
                    cell.value ? cell.value.toString().length : 0,
                );
            });
            column.width = maxColumnLength + 2;
        });
    };

    public generateFinancialReportExcelLTTP(data, filename): void {
        let options = {
            filename,
            useStyles: true,
            useSharedStrings: true,
        };
        let workbook = new ExcelJS.Workbook(options);

        let listColumnKey = [
            'TT',
            'tenLttpChatDot',
            'donViTinh',
            'soLuongThangTruoc',
            'nhapTrenBdMuaTT',
            'nhapTrenBdTGCB',
            'nhapDonViBdMuaTT',
            'nhapDonViBdTGCB',
            'soLuongCongNhap',
            'soLuongXuatAn',
            'soLuongXuatKhac',
            'soLuongCongXuat',
            'chuyenSangThangSau',
            'ghiChu',
        ];

        let dataDetail = data.chiTiet
            ? data.chiTiet.map((item, index) =>
                listColumnKey.map((E) => {
                    if (E === 'TT') {
                        return index + 1;
                    }

                    if (!item[E]) {
                        return '';
                    }

                    return item[E];
                }),
            )
            : [];

        let listColumnTitle = listColumnKey.map((item) => {
            switch (item) {
                case 'tenLTTP':
                    return 'Tên thực phẩm, chất đốt';
                case 'soLuongNgayTruoc':
                    return 'Tháng trước chuyển qua';
                case 'nhapTrongNgayThanhTien':
                    return 'Thừa thiếu chuyển sang tháng sau';
                case 'dvt':
                    return 'ĐVT';

                default:
                    return this.getColumnTitle(item);
            }
        });
        let alignmentCenter = {
            vertical: 'middle',
            horizontal: 'center',
        };

        let fontDefault = {
            name: 'Times New Roman',
            family: 1,
            size: 13,
        };

        let fontBold = {
            ...fontDefault,
            bold: true,
        };

        var worksheet = workbook.addWorksheet(filename, {
            pageSetup: {
                paperSize: 9,
                orientation: 'portrait',
                fitToPage: true,
                // printArea: 'A:J',
            }, //portrait,landscape
        });
        worksheet.pageSetup.printTitlesRow = '6:8';

        // THÊM TÊN CHỦ QUẢN
        worksheet.getCell('A1').value = `${this._dataService
            .getTenDonViCapTrenTrucTiep()
            .name.toUpperCase()}`;
        worksheet.getCell('A1').alignment = alignmentCenter;
        worksheet.getCell('A1').font = fontDefault;
        worksheet.mergeCells('A1:D1');

        // THÊM TÊN ĐƠN VỊ QUẢN LÝ
        worksheet.getCell('A2').value = `${this._dataService
            .getDonViThuocQuanLy()
            .name.toUpperCase()}`;
        worksheet.getCell('A2').alignment = alignmentCenter;
        worksheet.getCell('A2').font = {
            ...fontBold,
            underline: true,
        };
        worksheet.mergeCells('A2:D2');

        // THÊM TIÊU ĐỀ
        worksheet.getCell('E1').value = 'BÁO CÁO SỬ DỤNG THỰC PHẨM - CHẤT ĐỐT';
        worksheet.getCell('E1').alignment = alignmentCenter;
        worksheet.getCell('E1').font = {
            ...fontBold,
            size: 18,
        };
        worksheet.mergeCells('E1:L2');

        // THÊM NGÀY THÁNG
        (worksheet.getCell('E3').value = `Tháng ${this.getDayMonthYear(
            data.thoiGian,
            'month',
        )} năm ${this.getDayMonthYear(data.thoiGian, 'year')}`),
            (worksheet.getCell('E3').alignment = alignmentCenter);
        worksheet.getCell('E3').font = fontDefault;
        worksheet.mergeCells('E3:L3');

        worksheet.addRow([]);
        worksheet.addRow([]);

        //Add Header Row
        let headerRowEmpty = new Array(listColumnTitle.length).fill('', 0);
        let head1 = [
            'TT',
            'Tên thực phẩm, chất đốt',
            'ĐVT',
            'Tháng trước chuyển qua',
            'Nhập trong kỳ',
            '',
            '',
            '',
            '',
            'Xuất trong kỳ',
            '',
            '',
            'Thừa thiếu chuyển sang tháng sau',
            'Ghi chú',
        ];
        let head2 = [...headerRowEmpty];
        head2 = [
            '',
            '',
            '',
            '',
            'Trên BĐ',
            '',
            'Đơn vị tự BĐ',
            '',
            'Cộng',
            'Xuất chi ăn',
            'Xuất khác',
            'Cộng',
            '',
        ];

        let head3 = [...headerRowEmpty];
        head3 = [
            '',
            '',
            '',
            '',
            'Mua TT',
            'TGSX',
            'Mua TT',
            'TGSX',
            '',
            '',
            '',
            '',
            '',
        ];

        let headerRow = worksheet.addRow(head1);
        let headerRow2 = worksheet.addRow(head2);
        let headerRow3 = worksheet.addRow(head3);

        //Merged title row 1
        worksheet.mergeCells('A6:A8');
        worksheet.mergeCells('B6:B8');
        worksheet.mergeCells('C6:C8');
        worksheet.mergeCells('D6:D8');
        worksheet.mergeCells('E6:I6');
        worksheet.mergeCells('J6:L6');
        worksheet.mergeCells('M6:M8');
        worksheet.mergeCells('N6:N8');

        // //Merged title row 2
        worksheet.mergeCells('E7:F7');
        worksheet.mergeCells('G7:H7');
        worksheet.mergeCells('I7:I8');
        worksheet.mergeCells('J7:J8');
        worksheet.mergeCells('K7:K8');
        worksheet.mergeCells('L7:L8');
        //Merged title row Done

        headerRow.eachCell((cell, number) =>
            this.headerTitleCenter(cell, number),
        );
        headerRow2.eachCell((cell, number) =>
            this.headerTitleCenter(cell, number),
        );
        headerRow3.eachCell((cell, number) =>
            this.headerTitleCenter(cell, number),
        );

        dataDetail.forEach((d, index, array) => {
            let row = worksheet.addRow(d);
            let qty = row.getCell(6);

            row.eachCell((cell, number) => {
                //Default
                cell.alignment = {
                    wrapText: true,
                    vertical: 'middle',
                };
                cell.font = fontDefault;

                if (number === 1) {
                    cell.alignment = {
                        wrapText: true,
                        vertical: 'middle',
                        horizontal: 'center',
                    };
                }

                if (number > 2 && !this.isEven(number)) {
                    cell.numFmt = '""#,##0.0;[Red]-""#,##0.000';
                }
                if (number > 2 && this.isEven(number)) {
                    cell.numFmt = '""#,##0;[Red]-""#,##0.000';
                }

                //Last row table change style border
                if (index === array.length - 1) {
                    cell.border = {
                        top: { style: 'dotted' },
                        left: { style: 'thin' },
                        bottom: { style: 'thin' },
                        right: { style: 'thin' },
                    };
                } else {
                    cell.border = {
                        top: { style: 'dotted' },
                        left: { style: 'thin' },
                        bottom: { style: 'dotted' },
                        right: { style: 'thin' },
                    };
                }
            });
        });

        // PHẦN KÝ TÊN CHỨC DANH
        const rowsSkip = 8 + dataDetail.length;

        worksheet.addRow([]);
        worksheet.addRow([]);

        // NGÀY THÁNG NĂM KÝ
        worksheet.getCell(
            `L${rowsSkip + 3}`,
        ).value = `Ngày ${this.getDayMonthYear(
            this.currentDate,
            'day',
        )} tháng ${this.getDayMonthYear(
            this.currentDate,
            'month',
        )} năm ${this.getDayMonthYear(this.currentDate, 'year')}`;
        worksheet.getCell(`L${rowsSkip + 3}`).font = {
            ...fontDefault,
            italic: true,
        };
        worksheet.getCell(`L${rowsSkip + 3}`).alignment = alignmentCenter;
        worksheet.mergeCells(`L${rowsSkip + 3}:N${rowsSkip + 3}`);

        // CHỨC DANH
        worksheet.getCell(`A${rowsSkip + 4}`).value = 'NGƯỜI LẬP BÁO CÁO';
        worksheet.getCell(`A${rowsSkip + 4}`).font = fontBold;
        worksheet.getCell(`A${rowsSkip + 4}`).alignment = alignmentCenter;
        worksheet.mergeCells(`A${rowsSkip + 4}:D${rowsSkip + 4}`);

        worksheet.getCell(`L${rowsSkip + 4}`).value = 'THỦ TRƯỞNG ĐƠN VỊ';
        worksheet.getCell(`L${rowsSkip + 4}`).font = fontBold;
        worksheet.getCell(`L${rowsSkip + 4}`).alignment = alignmentCenter;
        worksheet.mergeCells(`L${rowsSkip + 4}:N${rowsSkip + 4}`);

        worksheet.addRow([]);
        worksheet.addRow([]);
        worksheet.addRow([]);
        worksheet.addRow([]);
        worksheet.addRow([]);

        // TÊN CHỨC DANH
        worksheet.getCell(
            `A${rowsSkip + 10}`,
        ).value = `${this._dataService.getNguoiLapBaoCao()}`;
        worksheet.getCell(`A${rowsSkip + 10}`).font = fontBold;
        worksheet.getCell(`A${rowsSkip + 10}`).alignment = alignmentCenter;
        worksheet.mergeCells(`A${rowsSkip + 10}:D${rowsSkip + 10}`);

        worksheet.getCell(
            `L${rowsSkip + 10}`,
        ).value = `${this._dataService.getTenThuTruongKyDuyet()}`;
        worksheet.getCell(`L${rowsSkip + 10}`).font = fontBold;
        worksheet.getCell(`L${rowsSkip + 10}`).alignment = alignmentCenter;
        worksheet.mergeCells(`L${rowsSkip + 10}:N${rowsSkip + 10}`);

        worksheet.getColumn(1).width = 5;
        worksheet.getColumn(2).width = 30;
        worksheet.getColumn(4).width = 15;
        worksheet.getColumn(5).width = 15;
        worksheet.getColumn(6).width = 15;
        worksheet.getColumn(7).width = 15;
        worksheet.getColumn(8).width = 15;
        worksheet.getColumn(9).width = 15;
        worksheet.getColumn(10).width = 15;
        worksheet.getColumn(11).width = 15;
        worksheet.getColumn(12).width = 15;
        worksheet.getColumn(13).width = 20;
        worksheet.getColumn(14).width = 20;

        //FILE CSS
        const excelBuffer: any = workbook.xlsx.writeBuffer();
        workbook.xlsx.writeBuffer().then(function (buffer) {
            // Done buffering
            //   this.saveExcelFile(buffer, fileName)
            //
            const data: Blob = new Blob([buffer], {
                type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            });
            FileSaver.saveAs(data, filename);
        });
    }

    public generateSelfSufficientReportExcel(data, filename): void {
        let options = {
            filename,
            useStyles: true,
            useSharedStrings: true,
        };
        let workbook = new ExcelJS.Workbook(options);

        let listColumnKey = [
            'index',
            'tenLttpChatDot',
            'donViTinh',
            'soLuong',
            'soLuongTGSX',
            'soLuongMuaTT',
            'tiLeTuTuc',
            'donGiaTGSX',
            'donGiaMuaTT',
            'thanhTienTGSX',
            'thanhTienMuaTT',
            'tienChenhLech',
        ];

        let dataDetail = data.chiTiet
            ? data.chiTiet.map((item, index) =>
                listColumnKey.map((E) => {
                    if (!item[E]) {
                        return ' ';
                    }

                    return item[E];
                }),
            )
            : [];

        // Thêm hàng tổng cộng vào trong bảng chi tiết
        let totalRowTable = new Array(dataDetail[0].length).fill('', 0);
        totalRowTable[1] = 'Cộng';
        dataDetail.push(new Array(dataDetail[0].length).fill('', 0));
        dataDetail.push(totalRowTable);

        let listColumnTitle = listColumnKey.map((item) => {
            switch (item) {
                case 'tenLttpChatDot':
                    return 'Tên thực phẩm, chất đốt';
                case 'soLuongNgayTruoc':
                    return 'Tháng trước chuyển qua';
                case 'nhapTrongNgayThanhTien':
                    return 'Thừa thiếu chuyển sang tháng sau';
                case 'dvt':
                    return 'ĐVT';

                default:
                    return this.getColumnTitle(item);
            }
        });

        let alignmentCenter = {
            vertical: 'middle',
            horizontal: 'center',
        };

        let fontDefault = {
            name: 'Times New Roman',
            family: 1,
            size: 13,
        };

        let fontBold = {
            ...fontDefault,
            bold: true,
        };

        var worksheet = workbook.addWorksheet(filename, {
            pageSetup: {
                paperSize: 9,
                orientation: 'portrait',
                fitToPage: true,
                // printArea: 'A:J',
            }, //portrait,landscape
        });
        worksheet.pageSetup.printArea = '6:7';

        // THÊM TÊN CHỦ QUẢN
        worksheet.getCell('A1').value = `${this._dataService.getTenDonViCapTrenTrucTiep().name.toUpperCase()}`;
        worksheet.getCell('A1').alignment = alignmentCenter;
        worksheet.getCell('A1').font = fontDefault;
        worksheet.mergeCells('A1:C1');

        // THÊM TÊN ĐƠN VỊ QUẢN LÝ
        worksheet.getCell('A2').value = `${this._dataService.getDonViThuocQuanLy().name.toUpperCase()}`;
        worksheet.getCell('A2').alignment = alignmentCenter;
        worksheet.getCell('A2').font = {
            ...fontBold,
            underline: true,
        };
        worksheet.mergeCells('A2:C2');

        // THÊM TIÊU ĐỀ
        worksheet.getCell('D1').value = 'BÁO CÁO TỰ TÚC THỰC PHẨM CHẤT ĐỐT';
        worksheet.getCell('D1').alignment = alignmentCenter;
        worksheet.getCell('D1').font = {
            ...fontBold,
            size: 18,
        };
        worksheet.mergeCells('D1:J2');

        // THÊM THÁNG NĂM BÁO CÁO
        worksheet.getCell('D3').value = `Tháng ${this.getDayMonthYear(
            data.thoiGian,
            'month',
        )} năm ${this.getDayMonthYear(data.thoiGian, 'year')}`;
        worksheet.getCell('D3').alignment = alignmentCenter;
        worksheet.getCell('D3').font = fontDefault;
        worksheet.mergeCells('D3:J3');

        worksheet.addRow([]);

        // THÊM ĐVT
        worksheet.getCell('K5').value = 'ĐVT: đồng';
        worksheet.getCell('K5').alignment = alignmentCenter;
        worksheet.getCell('K5').font = {
            ...fontDefault,
            italic: true,
        };
        worksheet.mergeCells('K5:L5');

        //Add Header Row
        let headerRowEmpty = new Array(listColumnTitle.length).fill('', 0);
        let head1 = [
            'TT',
            'Tên thực phẩm, chất đốt',
            'ĐVT',
            'Số lượng',
            'Trong đó',
            '',
            'Tỉ lệ tự túc (%)',
            'Đơn giá',
            '',
            'Thành tiền',
            '',
            'Tiền chênh lệch (TT - TGSX)',
        ];
        let head2 = [...headerRowEmpty];
        head2 = [
            '',
            '',
            '',
            '',
            'TGSX',
            'Mua TT',
            '',
            'TGSX',
            'Mua TT',
            'TGSX',
            'Mua TT',
            '',
        ];

        let headerRow = worksheet.addRow(head1);
        let headerRow2 = worksheet.addRow(head2);

        //Merged title row 1
        worksheet.mergeCells('A6:A7');
        worksheet.mergeCells('B6:B7');
        worksheet.mergeCells('C6:C7');
        worksheet.mergeCells('D6:D7');
        worksheet.mergeCells('E6:F6');
        worksheet.mergeCells('G6:G7');
        worksheet.mergeCells('H6:I6');
        worksheet.mergeCells('J6:K6');
        worksheet.mergeCells('L6:L7');

        headerRow.eachCell((cell, number) =>
            this.headerTitleCenter(cell, number),
        );
        headerRow2.eachCell((cell, number) =>
            this.headerTitleCenter(cell, number),
        );

        dataDetail.forEach((d, index, array) => {
            let row = worksheet.addRow(d);
            let qty = row.getCell(6);

            // generateSelfSufficientReportExcel
            row.eachCell((cell, number) => {
                // Default
                cell.alignment = {
                    wrapText: true,
                    vertical: 'middle',
                };
                cell.font = fontDefault;

                if (number === 1 || number === 3) {
                    cell.alignment = {
                        wrapText: true,
                        vertical: 'middle',
                        horizontal: 'center',
                    };
                }

                // Format Các cột số lượng
                if (number >= 4 && number <= 7) {
                    cell.numFmt = '""#,##0.0;[Red]-""#,##0.000';
                }

                if (number > 7) {
                    cell.numFmt = '""#,##0;[Red]-""#,##0.000';
                }

                //Last row table change style border
                if (index === array.length - 1) {
                    cell.border = {
                        top: { style: 'thin' },
                        left: { style: 'thin' },
                        bottom: { style: 'thin' },
                        right: { style: 'thin' },
                    };

                    cell.font = fontBold;

                    if (number === 2) {
                        cell.alignment = alignmentCenter;
                    }
                } else {
                    cell.border = {
                        top: { style: 'dotted' },
                        left: { style: 'thin' },
                        bottom: { style: 'dotted' },
                        right: { style: 'thin' },
                    };
                }
            });
        });

        // GET TOTAL
        const rowsSkip = 7 + dataDetail.length;

        // Tính tổng tiền theo cột
        worksheet.getCell(`J${rowsSkip}`).value = {
            formula: `SUM(J8:J${rowsSkip - 1})`,
        };
        worksheet.getCell(`K${rowsSkip}`).value = {
            formula: `SUM(K8:K${rowsSkip - 1})`,
        };

        worksheet.addRow([]);
        worksheet.addRow([]);

        // NGÀY THÁNG NĂM KÝ
        worksheet.getCell(`J${rowsSkip + 3}`).value =
            `Ngày ${this.getDayMonthYear(
                this.currentDate,
                'day',
            )} tháng ${this.getDayMonthYear(
                this.currentDate,
                'month',
            )} năm ${this.getDayMonthYear(this.currentDate, 'year')}`;
        worksheet.getCell(`J${rowsSkip + 3}`).font = {
            ...fontDefault,
            italic: true,
        };
        worksheet.getCell(`J${rowsSkip + 3}`).alignment = alignmentCenter;
        worksheet.mergeCells(`J${rowsSkip + 3}:L${rowsSkip + 3}`);

        // CHỨC DANH
        worksheet.getCell(`A${rowsSkip + 4}`).value = 'NGƯỜI LẬP BÁO CÁO';
        worksheet.getCell(`A${rowsSkip + 4}`).font = fontBold;
        worksheet.getCell(`A${rowsSkip + 4}`).alignment = alignmentCenter;
        worksheet.mergeCells(`A${rowsSkip + 4}:C${rowsSkip + 4}`);

        worksheet.getCell(`J${rowsSkip + 4}`).value = 'THỦ TRƯỞNG ĐƠN VỊ';
        worksheet.getCell(`J${rowsSkip + 4}`).font = fontBold;
        worksheet.getCell(`J${rowsSkip + 4}`).alignment = alignmentCenter;
        worksheet.mergeCells(`J${rowsSkip + 4}:L${rowsSkip + 4}`);

        worksheet.addRow([]);
        worksheet.addRow([]);
        worksheet.addRow([]);
        worksheet.addRow([]);
        worksheet.addRow([]);

        // TÊN CHỨC DANH
        worksheet.getCell(`A${rowsSkip + 10}`).value = `${this._dataService.getNguoiLapBaoCao()}`;
        worksheet.getCell(`A${rowsSkip + 10}`).font = fontBold;
        worksheet.getCell(`A${rowsSkip + 10}`).alignment = alignmentCenter;
        worksheet.mergeCells(`A${rowsSkip + 10}:C${rowsSkip + 10}`);

        worksheet.getCell(`J${rowsSkip + 10}`).value = `${this._dataService.getTenThuTruongKyDuyet()}`;
        worksheet.getCell(`J${rowsSkip + 10}`).font = fontBold;
        worksheet.getCell(`J${rowsSkip + 10}`).alignment = alignmentCenter;
        worksheet.mergeCells(`J${rowsSkip + 10}:L${rowsSkip + 10}`);

        worksheet.getColumn(1).width = 5;
        worksheet.getColumn(2).width = 30;
        worksheet.getColumn(3).width = 10;
        worksheet.getColumn(4).width = 10;
        worksheet.getColumn(5).width = 15;
        worksheet.getColumn(6).width = 15;
        worksheet.getColumn(7).width = 20;
        worksheet.getColumn(8).width = 15;
        worksheet.getColumn(9).width = 15;
        worksheet.getColumn(10).width = 15;
        worksheet.getColumn(11).width = 15;
        worksheet.getColumn(12).width = 25;

        //FILE CSS
        const excelBuffer: any = workbook.xlsx.writeBuffer();
        workbook.xlsx.writeBuffer().then(function (buffer) {
            // Done buffering
            //   this.saveExcelFile(buffer, fileName)
            //
            const data: Blob = new Blob([buffer], {
                type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            });
            FileSaver.saveAs(data, `${filename}.xlsx`);
        });
    }

    public generateImportTotalReportExcel(data, filename): void {
        let options = {
            filename,
            useStyles: true,
            useSharedStrings: true,
        };

        let alignmentCenter = {
            vertical: 'middle',
            horizontal: 'center',
        };

        let fontDefault = {
            name: 'Times New Roman',
            family: 1,
            size: 13,
        };

        let fontBold = {
            ...fontDefault,
            bold: true,
        };

        let fontTextMediumBold = {
            size: 13,
            ...fontBold,
        };

        let workbook = new ExcelJS.Workbook(options);

        const title = [
            `${this._dataService
                .getTenDonViCapTrenTrucTiep()
                .name.toUpperCase()}`,
            '',
            '',
            'PHIẾU TỔNG NHẬP KHO',
            '',
            '',
            '',
            '',
            '',
            'Mẫu 26: PNX-TMKH/QN21',
        ];
        const title2 = [
            `${this._dataService.getDonViThuocQuanLy().name.toUpperCase()}`,
            ``,
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            'Số:...',
        ];

        let listColumnKey = [
            'TT',
            'tenLttpChatDot',
            'donViTinh',
            'soLuongPhaiNhap',
            'soLuongTrenBdMua',
            'soLuongTrenBdTg',
            'soLuongDonViBdMua',
            'soLuongDonViBdTg',
            'giaLe',
            'thanhTien',
            'ghiChu',
        ];
        let listColumnKey2 = [
            '',
            '',
            '',
            'Phải nhập',
            'Thực nhập',
            '',
            '',
            '',
            '',
            '',
            '',
        ];
        let listColumnKey3 = [
            '',
            '',
            '',
            '',
            'Trên BĐ',
            '',
            'Đơn vị BĐ',
            '',
            '',
            '',
            '',
        ];
        let listColumnKey4 = [
            '',
            '',
            '',
            '',
            'Mua TT',
            'TG, CB',
            'Mua TT',
            'TG, CB',
            '',
            '',
            '',
        ];
        let dataDetail = data.chiTiet.map((item, index) =>
            listColumnKey.map((E) => {
                if (E === 'TT') {
                    return index + 1;
                }

                if (item[E] == null || item[E] == '') {
                    return '';
                }

                return item[E];
            }),
        );
        let totalRowTable = new Array(dataDetail[0].length).fill('', 0);
        totalRowTable[1] = 'Cộng';
        totalRowTable[dataDetail[0].length - 2] = data.cong;
        dataDetail.push(new Array(dataDetail[0].length).fill('', 0));
        dataDetail.push(totalRowTable);

        let listColumnTitle = listColumnKey.map((item) => {
            switch (item) {
                case 'donViTinh':
                    return 'ĐVT';
                case 'tenLttpChatDot':
                    return 'Tên, qui cách vật tư sản phẩm';
                case 'giaLe':
                    return 'Giá lẻ';
                case 'ghiChu':
                    return 'Ghi chú';
                case 'thanhTien':
                    return 'Thành tiền (Đồng)';

                default:
                    return this.getColumnTitle(item);
            }
        });

        var worksheet = workbook.addWorksheet(filename, {
            pageSetup: {
                paperSize: 9,
                orientation: 'portrait',
                fitToPage: true,
            }, //portrait,landscape
        });

        worksheet.pageSetup.printTitlesRow = '7:10';
        worksheet.pageSetup.margins = {
            left: 0.6,
            right: 0.6,
            top: 0.6,
            bottom: 0.6,
            header: 0.8,
            footer: 0.8,
        };

        let titleRow = worksheet.addRow(title);
        let titleRow2 = worksheet.addRow(title2);
        titleRow.alignment = alignmentCenter;
        titleRow2.alignment = alignmentCenter;

        worksheet.mergeCells('A1:C1');
        worksheet.mergeCells('A2:C2');
        worksheet.mergeCells('D1:I2');
        worksheet.mergeCells('J1:K1');
        worksheet.mergeCells('J2:K2');

        worksheet.addRow();
        worksheet.addRow(['', `Kho nhận hàng: ${data.khoNhanName.name}`]).font =
            fontDefault;

        worksheet.addRow([
            '',
            `Có giá trị hết Ngày ${this.getDayMonthYear(
                data.ngayHetHan,
                'day',
            )} tháng ${this.getDayMonthYear(
                data.ngayHetHan,
                'month',
            )} năm ${this.getDayMonthYear(data.ngayHetHan, 'year')}`,
        ]).font = fontDefault;

        // worksheet.getCell('G2').alignment = {
        //     vertical: 'middle',
        //     horizontal: 'center',
        // };

        // worksheet.getCell('C4').alignment = {
        //     vertical: 'middle',
        //     horizontal: 'center',
        // };

        worksheet.addRow([]);

        //Add Header Row
        let headerRow = worksheet.addRow(listColumnTitle);
        let headerRow2 = worksheet.addRow(listColumnKey2);
        let headerRow3 = worksheet.addRow(listColumnKey3);
        let headerRow4 = worksheet.addRow(listColumnKey4);

        worksheet.mergeCells('D7:H7');
        worksheet.mergeCells('A7:A10');
        worksheet.mergeCells('B7:B10');
        worksheet.mergeCells('C7:C10');
        worksheet.mergeCells('D8:D10');
        worksheet.mergeCells('E8:H8');
        worksheet.mergeCells('E9:F9');
        worksheet.mergeCells('G9:H9');
        worksheet.mergeCells('I7:I10');
        worksheet.mergeCells('J7:J10');
        worksheet.mergeCells('K7:K10');

        headerRow.eachCell((cell, number) => {
            cell.border = {
                top: { style: 'thin' },
                left: { style: 'thin' },
                bottom: { style: 'thin' },
                right: { style: 'thin' },
            };

            cell.alignment = {
                wrapText: true,
                vertical: 'middle',
                horizontal: 'center',
            };

            cell.font = {
                ...fontBold,
            };
        });

        headerRow2.eachCell((cell, number) => {
            cell.border = {
                top: { style: 'thin' },
                left: { style: 'thin' },
                bottom: { style: 'thin' },
                right: { style: 'thin' },
            };

            cell.alignment = {
                wrapText: true,
                vertical: 'middle',
                horizontal: 'center',
            };

            cell.font = {
                ...fontBold,
            };
        });

        headerRow3.eachCell((cell, number) => {
            cell.border = {
                top: { style: 'thin' },
                left: { style: 'thin' },
                bottom: { style: 'thin' },
                right: { style: 'thin' },
            };

            cell.alignment = {
                wrapText: true,
                vertical: 'middle',
                horizontal: 'center',
            };

            cell.font = {
                ...fontBold,
            };
        });

        headerRow4.eachCell((cell, number) => {
            cell.border = {
                top: { style: 'thin' },
                left: { style: 'thin' },
                bottom: { style: 'thin' },
                right: { style: 'thin' },
            };

            cell.alignment = {
                wrapText: true,
                vertical: 'middle',
                horizontal: 'center',
            };

            cell.font = {
                ...fontBold,
            };
        });

        // Add Data and Conditional Formatting
        dataDetail.forEach((d, index, array) => {
            let row = worksheet.addRow(d);
            let qty = row.getCell(6);

            row.eachCell((cell, number) => {
                // Default
                cell.font = fontDefault;

                cell.alignment = {
                    wrapText: true,
                    vertical: 'middle',
                };

                if (number === 1) {
                    cell.alignment = {
                        wrapText: true,
                        vertical: 'middle',
                        horizontal: 'center',
                    };
                } else if (number === 2) {
                    cell.alignment = {
                        wrapText: true,
                        vertical: 'middle',
                        horizontal: 'left',
                    };
                }
                if (number > 3 && number <= 8) {
                    cell.numFmt = '""#,##0.0;[Red]-""#,##0.000';
                }
                if (number >= 9 && number < 11) {
                    cell.numFmt = '""#,##0;[Red]-""#,##0.000';
                }

                //Last row table change style border
                if (index === array.length - 1) {
                    cell.border = {
                        top: { style: 'thin' },
                        left: { style: 'thin' },
                        bottom: { style: 'thin' },
                        right: { style: 'thin' },
                    };

                    cell.font = {
                        ...fontBold,
                    };

                    if (number === 2) {
                        cell.alignment = alignmentCenter;
                    }
                } else {
                    cell.border = {
                        top: { style: 'dotted' },
                        left: { style: 'thin' },
                        bottom: { style: 'dotted' },
                        right: { style: 'thin' },
                    };
                }
            });
        });

        worksheet.addRow([]);

        let rowTotalFooter = worksheet.addRow([
            '',
            `Tổng nhập: ${data.soLuongMatHang} mặt hàng`,
        ]);
        rowTotalFooter.font = fontDefault;

        let rowTotalFooterSecond = worksheet.addRow([
            '',
            `Thành tiền: ${this.transformDate(data.cong.toString())}`,
        ]);
        rowTotalFooterSecond.font = fontDefault;

        let rowDate = worksheet.addRow([
            '',
            `Ngày ${this.getDayMonthYear(
                data.ngayHetHan,
                'day',
            )} tháng ${this.getDayMonthYear(
                data.ngayHetHan,
                'month',
            )} năm ${this.getDayMonthYear(data.ngayHetHan, 'year')}`,
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            `Ngày ${this.getDayMonthYear(
                data.ngayHetHan,
                'day',
            )} tháng ${this.getDayMonthYear(
                data.ngayHetHan,
                'month',
            )} năm ${this.getDayMonthYear(data.ngayHetHan, 'year')}`,
        ]);
        rowDate.font = fontDefault;
        rowDate.alignment = alignmentCenter;

        let row = worksheet.addRow([
            '',
            'Người viết phiếu',
            '',
            'Trực ban',
            '',
            '',
            'Người nhận',
            '',
            '',
            'Người duyệt',
        ]);
        row.font = fontBold;
        row.alignment = alignmentCenter;

        worksheet.addRow([]);
        worksheet.addRow([]);
        worksheet.addRow([]);
        worksheet.addRow([]);
        worksheet.addRow([]);
        let row2 = worksheet.addRow([
            '',
            `${data.nguoiVietPhieu}`,
            '',
            `${data.trucBan}`,
            '',
            '',
            `${data.nguoiNhan}`,
            '',
            '',
            `${data.nguoiDuyet}`,
        ]);
        row2.font = fontBold;
        row2.alignment = alignmentCenter;

        let skipRow = dataDetail.length + 10;

        worksheet.mergeCells(`J${skipRow + 4}:K${skipRow + 4}`);
        worksheet.mergeCells(`D${skipRow + 5}:F${skipRow + 5}`);
        worksheet.mergeCells(`G${skipRow + 5}:I${skipRow + 5}`);
        worksheet.mergeCells(`J${skipRow + 5}:K${skipRow + 5}`);
        worksheet.mergeCells(`D${skipRow + 11}:F${skipRow + 11}`);
        worksheet.mergeCells(`G${skipRow + 11}:I${skipRow + 11}`);
        worksheet.mergeCells(`J${skipRow + 11}:K${skipRow + 11}`);

        worksheet.getColumn(1).width = 5;
        worksheet.getColumn(2).width = 30;
        worksheet.getColumn(3).width = 7;
        worksheet.getColumn(4).width = 10;
        worksheet.getColumn(5).width = 7;
        worksheet.getColumn(6).width = 7;
        worksheet.getColumn(7).width = 7;
        worksheet.getColumn(8).width = 7;
        worksheet.getColumn(9).width = 15;
        worksheet.getColumn(10).width = 15;
        worksheet.getColumn(11).width = 20;

        worksheet.getCell('A1').font = fontDefault;
        worksheet.getCell('A2').font = {
            ...fontBold,
            underline: true,
        };
        worksheet.getCell('D1').font = {
            ...fontBold,
            size: 18,
        };
        worksheet.getCell('J1').font = fontDefault;
        worksheet.getCell('J2').font = fontDefault;

        const excelBuffer: any = workbook.xlsx.writeBuffer();
        workbook.xlsx.writeBuffer().then(function (buffer) {
            // Done buffering
            //   this.saveExcelFile(buffer, fileName)
            //
            const data: Blob = new Blob([buffer], {
                type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            });
            FileSaver.saveAs(data, filename);
        });
    }

    public generateExportTotalReportExcel(data, filename): void {
        let options = {
            filename,
            useStyles: true,
            useSharedStrings: true,
        };
        let workbook = new ExcelJS.Workbook(options);

        let alignmentCenter = {
            vertical: 'middle',
            horizontal: 'center',
        };

        let fontDefault = {
            name: 'Times New Roman',
            family: 1,
            size: 13,
        };

        let fontBold = {
            ...fontDefault,
            bold: true,
        };

        let fontTextMediumBold = {
            size: 14,
            ...fontBold,
        };

        const title = [
            `${this._dataService
                .getTenDonViCapTrenTrucTiep()
                .name.toUpperCase()}`,
            '',
            'PHIẾU TỔNG XUẤT KHO',
            '',
            '',
            '',
            'Biểu SS14-QN10',
        ];
        const title2 = [
            `${this._dataService.getDonViThuocQuanLy().name.toUpperCase()}`,
            '',
            '',
            '',
            '',
            '',
            'Quyển số:……………….',
        ];

        let listColumnKey = [
            'TT',
            'tenLttpChatDot',
            'donViTinh',
            'soLuongPhaiXuat',
            'soLuongThucXuat',
            'donGia',
            'thanhTien',
            'ghiChu',
        ];
        let listColumnKey2 = ['', '', '', 'Phải xuất', 'Thực xuất', '', '', ''];

        let dataDetail = data.chiTiet.map((item, index) =>
            listColumnKey.map((E) => {
                if (E === 'TT') {
                    return index + 1;
                }

                if (item[E] == null || item[E] == '') {
                    return '';
                }

                if (item[E] === 'thanhTien') {
                    return item['soLuongThucXuat'] * item['donGia'];
                }

                return item[E];
            }),
        );

        let totalRowTable = new Array(dataDetail[0].length).fill('', 0);
        totalRowTable[1] = 'Tổng cộng';
        totalRowTable[dataDetail[0].length - 2] = data.tongTien;
        dataDetail.push(new Array(dataDetail[0].length).fill('', 0));
        dataDetail.push(totalRowTable);

        let listColumnTitle = listColumnKey.map((item) => {
            switch (item) {
                case 'tenLttpChatDot':
                    return 'Tên, quy cách vật tư sản phẩm';
                case 'donViTinh':
                    return 'ĐVT';
                case 'donGia':
                    return 'Giá lẻ';
                case 'thanhTien':
                    return 'Thành tiền (Đồng)';
                case 'ghiChu':
                    return 'Ghi chú';
                case 'soLuongPhaiXuat':
                    return 'Số lượng';
                default:
                    return this.getColumnTitle(item);
            }
        });

        var worksheet = workbook.addWorksheet(filename, {
            pageSetup: {
                paperSize: 9,
                orientation: 'portrait',
                fitToPage: true,
            }, //portrait,landscape
        });
        worksheet.pageSetup.printTitlesRow = '10:11';

        let titleRow = worksheet.addRow(title);
        let titleRow2 = worksheet.addRow(title2);
        worksheet.mergeCells('A1:B1');
        worksheet.mergeCells('A2:B2');
        worksheet.mergeCells('C1:F2');
        worksheet.mergeCells('G1:H1');
        worksheet.mergeCells('G2:H2');

        worksheet.addRow(['', '', '', '', '', '', 'Số:……………………']);
        worksheet.mergeCells('G3:H3');
        worksheet.addRow([
            '',
            '',
            `Có giá trị hết: Ngày ${this.getDayMonthYear(
                data.ngayHetHan,
                'day',
            )} tháng ${this.getDayMonthYear(
                data.ngayHetHan,
                'month',
            )} năm ${this.getDayMonthYear(data.ngayHetHan, 'year')}`,
        ]).font = fontDefault;
        worksheet.mergeCells('C4:F4');

        worksheet.addRow([]);

        worksheet.addRow([
            '',
            `Họ, tên người nhận: ${data.nguoiNhan ? data.nguoiNhan : ''}`,
        ]).font = fontDefault;
        worksheet.addRow(['', `Lí do sử dụng: ${data.lyDoSuDung.name}`]).font =
            fontDefault;
        worksheet.addRow(['', `Nhận tại kho: ${data.khoNhanName.name}`]).font =
            fontDefault;

        worksheet.getCell('G2').alignment = {
            vertical: 'middle',
            horizontal: 'center',
        };

        worksheet.getCell('C4').alignment = {
            vertical: 'middle',
            horizontal: 'center',
        };

        worksheet.addRow([]);

        //Add Header Row
        let headerRow = worksheet.addRow(listColumnTitle);
        let headerRow2 = worksheet.addRow(listColumnKey2);

        worksheet.mergeCells('D10:E10');
        worksheet.mergeCells('A10:A11');
        worksheet.mergeCells('B10:B11');
        worksheet.mergeCells('C10:C11');
        worksheet.mergeCells('F10:F11');
        worksheet.mergeCells('G10:G11');
        worksheet.mergeCells('H10:H11');

        headerRow.eachCell((cell, number) => {
            cell.border = {
                top: { style: 'thin' },
                left: { style: 'thin' },
                bottom: { style: 'thin' },
                right: { style: 'thin' },
            };

            cell.alignment = {
                wrapText: true,
                vertical: 'middle',
                horizontal: 'center',
            };

            cell.font = {
                ...fontBold,
            };
        });

        headerRow2.eachCell((cell, number) => {
            cell.border = {
                top: { style: 'thin' },
                left: { style: 'thin' },
                bottom: { style: 'thin' },
                right: { style: 'thin' },
            };

            cell.alignment = {
                wrapText: true,
                vertical: 'middle',
                horizontal: 'center',
            };

            cell.font = {
                ...fontBold,
            };
        });

        // Add Data and Conditional Formatting
        dataDetail.forEach((d, index, array) => {
            let row = worksheet.addRow(d);
            let qty = row.getCell(6);

            row.eachCell((cell, number) => {
                cell.alignment = {
                    wrapText: true,
                    vertical: 'middle',
                };

                if (number === 1) {
                    cell.alignment = {
                        wrapText: true,
                        vertical: 'middle',
                        horizontal: 'center',
                    };
                } else if (number === 2) {
                    cell.alignment = {
                        wrapText: true,
                        vertical: 'middle',
                        horizontal: 'left',
                    };
                }

                if (number === 4 || number === 5) {
                    cell.numFmt = '""#,##0.0;[Red]-""#,##0.000';
                }

                if (number > 5) {
                    cell.numFmt = '""#,##0;[Red]-""#,##0.000';
                }
                cell.font = fontDefault;

                //Last row table change style border
                if (index === array.length - 1) {
                    cell.border = {
                        top: { style: 'thin' },
                        left: { style: 'thin' },
                        bottom: { style: 'thin' },
                        right: { style: 'thin' },
                    };

                    cell.font = {
                        ...fontBold,
                    };

                    if (number === 2) {
                        cell.alignment = alignmentCenter;
                    }
                } else {
                    cell.border = {
                        top: { style: 'thin' },
                        left: { style: 'thin' },
                        bottom: { style: 'thin' },
                        right: { style: 'thin' },
                    };
                }
            });
        });

        worksheet.addRow([]);

        worksheet.addRow([
            '',
            'Tổng xuất:',
            `${data.soLuongMatHang} mặt hàng`,
        ]).font = fontDefault;

        //TransformDate max value is 1b, If more value can have bug
        let newRowTotal = worksheet.addRow([
            '',
            'Tổng số tiền (viết bằng chữ):',
            `${this.transformDate(data.tongTien.toString())}`,
        ]);
        newRowTotal.font = fontDefault;
        worksheet.addRow([
            '',
            'Ngày giao nhận:',
            `Ngày ${this.getDayMonthYear(
                data.ngayHetHan,
                'day',
            )} tháng ${this.getDayMonthYear(
                data.ngayHetHan,
                'month',
            )} năm ${this.getDayMonthYear(data.ngayHetHan, 'year')}`,
            '',
            '',
        ]).font = fontDefault;

        // Ngày ký báo cáo
        let dateSignRow = worksheet.addRow([
            '',
            '',
            '',
            '',
            '',
            '',
            `Ngày ${this.getDayMonthYear(
                this.currentDate,
                'day',
            )} tháng ${this.getDayMonthYear(
                this.currentDate,
                'month',
            )} năm ${this.getDayMonthYear(this.currentDate, 'year')}`,
        ]);
        dateSignRow.font = {
            ...fontDefault,
            italic: true,
        };
        dateSignRow.alignment = alignmentCenter;

        const rowsSkip = 11 + dataDetail.length;
        worksheet.mergeCells(`C${rowsSkip + 2}:H${rowsSkip + 2}`);
        worksheet.mergeCells(`C${rowsSkip + 3}:H${rowsSkip + 3}`);
        worksheet.mergeCells(`C${rowsSkip + 4}:H${rowsSkip + 4}`);
        worksheet.mergeCells(`G${rowsSkip + 5}:H${rowsSkip + 5}`);

        let signatureRow1 = worksheet.addRow([
            '',
            'Người viết phiếu',
            ' Người nhận',
            '',
            ' Thủ kho',
            '',
            'Người duyệt ',
            '',
        ]);
        signatureRow1.font = fontBold;
        signatureRow1.alignment = alignmentCenter;

        worksheet.addRow([]);
        worksheet.addRow([]);
        worksheet.addRow([]);
        worksheet.addRow([]);
        worksheet.addRow([]);

        let signatureRow2 = worksheet.addRow([
            '',
            `${!this.isEmty(this._dataService.getQuanLyBep())
                ? this._dataService.getQuanLyBep()
                : ''
            }`,
            `${!this.isEmty(data.nguoiNhan) ? data.nguoiNhan : ''}`,
            '',
            `${!this.isEmty(this._dataService.getThuKho())
                ? this._dataService.getThuKho()
                : ''
            }`,
            '',
            `${!this.isEmty(this._dataService.getTenThuTruongKyDuyet())
                ? this._dataService.getTenThuTruongKyDuyet()
                : ''
            }`,
            '',
        ]);
        signatureRow2.font = fontBold;
        signatureRow2.alignment = alignmentCenter;

        worksheet.mergeCells(`C${rowsSkip + 6}:D${rowsSkip + 6}`);
        worksheet.mergeCells(`C${rowsSkip + 12}:D${rowsSkip + 12}`);
        worksheet.mergeCells(`E${rowsSkip + 6}:F${rowsSkip + 6}`);
        worksheet.mergeCells(`E${rowsSkip + 12}:F${rowsSkip + 12}`);
        worksheet.mergeCells(`G${rowsSkip + 6}:H${rowsSkip + 6}`);
        worksheet.mergeCells(`G${rowsSkip + 12}:H${rowsSkip + 12}`);

        worksheet.addRow([]);
        worksheet.addRow([]);
        let trucBanRow1 = worksheet.addRow(['', 'Trực ban']);
        trucBanRow1.font = fontBold;
        trucBanRow1.alignment = alignmentCenter;
        worksheet.addRow([]);
        worksheet.addRow([]);
        worksheet.addRow([]);
        worksheet.addRow([]);
        worksheet.addRow([]);
        let trucBanRow2 = worksheet.addRow([
            '',
            `${this._dataService.getTrucBan()}`,
        ]);
        trucBanRow2.font = fontBold;
        trucBanRow2.alignment = alignmentCenter;

        // Setting length of columns
        worksheet.getColumn(1).width = 5;
        worksheet.getColumn(2).width = 35;
        worksheet.getColumn(4).width = 15;
        worksheet.getColumn(5).width = 15;
        worksheet.getColumn(6).width = 15;
        worksheet.getColumn(7).width = 20;
        worksheet.getColumn(8).width = 15;

        //FILE CSS
        titleRow.alignment = alignmentCenter;
        titleRow2.alignment = alignmentCenter;

        // TRƯỜNG SĨ QUAN THÔNG TIN
        worksheet.getCell('A1').font = {
            ...fontDefault,
        };

        // TIÊU ĐỀ
        worksheet.getCell('C1').font = {
            ...fontBold,
            size: 18,
        };

        // ĐƠN VỊ
        worksheet.getCell('A2').font = {
            ...fontBold,
            underline: true,
        };

        // BIỂU, QUYỂN SỐ, SỐ....
        worksheet.getCell('G1').font = {
            ...fontDefault,
        };
        worksheet.getCell('G2').font = {
            ...fontDefault,
        };
        worksheet.getCell('G3').font = {
            ...fontDefault,
        };
        worksheet.getCell('G3').alignment = alignmentCenter;

        // worksheet.getCell('C16').numFmt = '""#,##0.000;[Red]-""#,##0.000';
        // worksheet.getCell('C16').font = {
        //     ...fontBold,
        // };

        const excelBuffer: any = workbook.xlsx.writeBuffer();
        workbook.xlsx.writeBuffer().then(function (buffer) {
            // Done buffering
            //   this.saveExcelFile(buffer, fileName)
            //
            const data: Blob = new Blob([buffer], {
                type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            });
            FileSaver.saveAs(data, filename);
        });
    }

    public generateDuXuatExcel(data, filename): void {
        let { chiTiet } = data;
        // let ngayTao = new Date();
        let options = {
            filename,
            useStyles: true,
            useSharedStrings: true,
        };

        // let lengthTable = chiTiet.length;
        // const title = new Array(lengthTable).fill('', 0);
        let workbook = new ExcelJS.Workbook(options);

        let min = 5,
            medium = 15,
            large = 20;

        let listColumnKey = [
            'tenLttpChatDot',
            'donViTinh',
            'donGia',
            'soLuongTrenBdMua',
            'soLuongTrenBdTg',
            'soLuongDonViBdMua',
            'soLuongDonViBdTg',
            'thanhTien',
            'duXuat',
            'thanhTienDuXuat',
        ];
        let arr = [];
        let arr2 = [];
        let dataDetailMenu = [];
        let dataDetailMenuTomorrow = [];
        let dataDetailPrice = [];

        data.chiTietDanhSachLttp.forEach((chiTiet) =>
            chiTiet.danhSach.forEach((element) => {
                if (
                    chiTiet.buoi === 'Sáng' ||
                    chiTiet.buoi === 'Trưa' ||
                    chiTiet.buoi === 'Chiều' ||
                    chiTiet.buoi === 'Gạo + than + gia vị'
                ) {
                    arr.push({
                        buoi: chiTiet.buoi,
                        ...element,
                    });
                } else {
                    arr2.push({
                        buoi: chiTiet.buoi,
                        ...element,
                    });
                }
            }),
        );

        let thanhTien = arr.map((item) => item.duXuat * item.donGia);
        let thanhTienHomSau = arr2.map((item) => item.duXuat * item.donGia);
        let thanhTienDuNhap =
            this.sum(arr, 'thanhTien') + this.sum(arr2, 'thanhTien');

        data.chiTietDanhSachLttp.forEach((e) => {
            if (e.buoi === 'Gạo + than + gia vị') {
                dataDetailMenu.push(['', '', '', '', '', '', '', '', '', '']);
                e.danhSach.forEach((e2) => {
                    dataDetailMenu.push([
                        e2.tenLttpChatDot,
                        e2.donViTinh,
                        e2.donGia,
                        e2.soLuongTrenBdMua > 0 ? e2.soLuongTrenBdMua : '',
                        e2.soLuongTrenBdTg > 0 ? e2.soLuongTrenBdTg : '',
                        e2.soLuongDonViBdMua > 0 ? e2.soLuongDonViBdMua : '',
                        e2.soLuongDonViBdTg > 0 ? e2.soLuongDonViBdTg : '',
                        e2.soLuongTrenBdMua > 0
                            ? e2.soLuongTrenBdMua * e2.donGia
                            : e2.soLuongTrenBdTg > 0
                                ? e2.soLuongTrenBdTg * e2.donGia
                                : e2.soLuongDonViBdMua > 0
                                    ? e2.soLuongDonViBdMua * e2.donGia
                                    : e2.soLuongDonViBdTg > 0
                                        ? e2.soLuongDonViBdTg * e2.donGia
                                        : 0,
                        e2.duXuat,
                        e2.duXuat * e2.donGia,
                    ]);
                });
                dataDetailMenu.push(['', '', '', '', '', '', '', '', '', '']);
            }
        });

        data.chiTietDanhSachLttp.forEach((e) => {
            if (e.buoi === 'Sáng' || e.buoi === 'Trưa' || e.buoi === 'Chiều') {
                dataDetailMenu.push([
                    e.buoi,
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                ]);
                e.danhSach.forEach((e2) => {
                    dataDetailMenu.push([
                        e2.tenLttpChatDot,
                        e2.donViTinh,
                        e2.donGia,
                        e2.soLuongTrenBdMua > 0 ? e2.soLuongTrenBdMua : '',
                        e2.soLuongTrenBdTg > 0 ? e2.soLuongTrenBdTg : '',
                        e2.soLuongDonViBdMua > 0 ? e2.soLuongDonViBdMua : '',
                        e2.soLuongDonViBdTg > 0 ? e2.soLuongDonViBdTg : '',
                        e2.soLuongTrenBdMua > 0
                            ? e2.soLuongTrenBdMua * e2.donGia
                            : e2.soLuongTrenBdTg > 0
                                ? e2.soLuongTrenBdTg * e2.donGia
                                : e2.soLuongDonViBdMua > 0
                                    ? e2.soLuongDonViBdMua * e2.donGia
                                    : e2.soLuongDonViBdTg > 0
                                        ? e2.soLuongDonViBdTg * e2.donGia
                                        : 0,
                        e2.duXuat,
                        e2.duXuat * e2.donGia,
                    ]);
                });
                dataDetailMenu.push(['', '', '', '', '', '', '', '', '', '']);
            }
        });

        data.chiTietDanhSachLttp.forEach((e) => {
            if (e.buoi === 'Sáng hôm sau') {
                dataDetailMenuTomorrow.push([
                    e.buoi,
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                ]);
                e.danhSach.forEach((e2) => {
                    dataDetailMenuTomorrow.push([
                        e2.tenLttpChatDot,
                        e2.donViTinh,
                        e2.donGia,
                        e2.soLuongTrenBdMua > 0 ? e2.soLuongTrenBdMua : '',
                        e2.soLuongTrenBdTg > 0 ? e2.soLuongTrenBdTg : '',
                        e2.soLuongDonViBdMua > 0 ? e2.soLuongDonViBdMua : '',
                        e2.soLuongDonViBdTg > 0 ? e2.soLuongDonViBdTg : '',
                        e2.soLuongTrenBdMua > 0
                            ? e2.soLuongTrenBdMua * e2.donGia
                            : e2.soLuongTrenBdTg > 0
                                ? e2.soLuongTrenBdTg * e2.donGia
                                : e2.soLuongDonViBdMua > 0
                                    ? e2.soLuongDonViBdMua * e2.donGia
                                    : e2.soLuongDonViBdTg > 0
                                        ? e2.soLuongDonViBdTg * e2.donGia
                                        : 0,
                        e2.duXuat,
                        e2.duXuat * e2.donGia,
                    ]);
                });
            }
        });

        data.chiTietDuXuat.forEach((e) => {
            dataDetailPrice.push([
                `Mức ăn: ${this.transformCurrency(e.mucTien)}đ/người/ngày`,
                null,
                null,
                null,
            ]);
            e.chiTietGaoTienAn.forEach((e2) => {
                dataDetailPrice.push([
                    e2.buoi === 0 ? 'S' : e2.buoi === 1 ? 'T' : 'C',
                    e2.quanSo,
                    e2.mucTienAn,
                    e2.tongTienAn,
                ]);
            });
        });

        let dataTotal = [];
        data.chiTietDuXuat.forEach((data) => {
            dataTotal.push(this.sum(data.chiTietGaoTienAn, 'tongTienAn'));
        });

        let sum = dataTotal.reduce((partialSum, a) => partialSum + a, 0);

        dataDetailPrice.push(['Cộng', null, null, sum]);

        let dataEatBonus = [];

        data.chiTietDuXuat.forEach((e) => {
            e.chiTietAnThem.forEach((e2) => {
                dataEatBonus.push([
                    e2.loai === 0
                        ? 'Ăn thêm ốm'
                        : e2.loai === 1
                            ? 'Ăn thêm lễ'
                            : 'Ăn thêm LNV',
                    '',
                    e2.quanSo,
                    e2.tongTienAnThem,
                ]);
            });
        });

        dataDetailPrice.push(
            dataEatBonus.reduce(
                (curr, ele) => {
                    if (ele[0] === 'Ăn thêm ốm') {
                        if (ele[2] > 0) {
                            curr[2] += ele[2];
                        } else {
                            curr[2] = null;
                        }
                        if (ele[3] > 0) {
                            curr[3] += ele[3];
                        } else {
                            curr[3] = null;
                        }
                    }
                    return curr;
                },
                ['Ăn thêm ốm', null, 0, 0],
            ),
        );

        dataDetailPrice.push(
            dataEatBonus.reduce(
                (curr, ele) => {
                    if (ele[0] === 'Ăn thêm lễ') {
                        if (ele[2] > 0) {
                            curr[2] += ele[2];
                        } else {
                            curr[2] = null;
                        }
                        if (ele[3] > 0) {
                            curr[3] += ele[3];
                        } else {
                            curr[3] = null;
                        }
                    }
                    return curr;
                },
                ['Ăn thêm lễ', null, 0, 0],
            ),
        );

        dataDetailPrice.push(
            dataEatBonus.reduce(
                (curr, ele) => {
                    if (ele[0] === 'Ăn thêm LNV') {
                        if (ele[2] > 0) {
                            curr[2] += ele[2];
                        } else {
                            curr[2] = null;
                        }
                        if (ele[3] > 0) {
                            curr[3] += ele[3];
                        } else {
                            curr[3] = null;
                        }
                    }
                    return curr;
                },
                ['Ăn thêm LNV', null, 0, 0],
            ),
        );

        dataDetailPrice.push([
            'Thừa hôm trước',
            null,
            null,
            data.thuaThieuHomTruoc > 0 ? data.thuaThieuHomTruoc : null,
        ]);

        dataDetailPrice.push(['', '', '', '']);

        dataDetailPrice.push(['Cộng thu', null, null, data.tongTien]);
        dataDetailPrice.push(['Tiền đã sử dụng', null, null, data.daSuDung]);
        dataDetailPrice.push([
            'Thiếu hôm trước',
            null,
            null,
            data.thuaThieuHomTruoc < 0 ? data.thuaThieuHomTruoc : null,
        ]);
        dataDetailPrice.push(['Cộng chi', null, null, data.congChi]);
        dataDetailPrice.push([
            'Thừa trong ngày',
            null,
            null,
            data.thuaThieuTrongNgay,
        ]);

        dataDetailPrice.push(['', '', '', '']);

        dataDetailPrice.push(['2. Phần gạo', null, null, null]);

        data.chiTietDuXuat.forEach((e) => {
            dataDetailPrice.push([
                `Mức ăn: ${this.sum(e.chiTietGaoTienAn, 'dinhLuongGao') * 1000
                }g/người/ngày`,
                null,
                null,
                null,
            ]);
            e.chiTietGaoTienAn.forEach((e2) => {
                dataDetailPrice.push([
                    e2.buoi === 0 ? 'S' : e2.buoi === 1 ? 'T' : 'C',
                    e2.quanSo,
                    e2.dinhLuongGao,
                    e2.tongGao,
                ]);
            });
        });

        let dataTotalRice = [];
        data.chiTietDuXuat.forEach((data) => {
            dataTotalRice.push(this.sum(data.chiTietGaoTienAn, 'tongGao'));
        });

        let sumRice = dataTotalRice.reduce(
            (partialSum, a) => partialSum + a,
            0,
        );
        dataDetailPrice.push(['Cộng', null, null, Math.round(sumRice)]);

        dataDetailPrice.push(['', '', '', '']);

        let totalNexMorning = [];
        data.chiTietDuXuat.forEach((data) => {
            data.chiTietGaoTienAn.forEach((chiTiet) => {
                if (chiTiet.buoi === 2) {
                    totalNexMorning.push(chiTiet.quanSo++);
                }
            });
        });

        let sumNexMorning = totalNexMorning.reduce(
            (partialSum, a) => partialSum + a,
            0,
        );

        let totalRevenue = [];
        data.chiTietDuXuat.forEach((data) => {
            data.chiTietGaoTienAn.forEach((chiTiet) => {
                if (chiTiet.buoi === 2) {
                    totalRevenue.push(chiTiet.tongTienAn++);
                }
            });
        });

        let sumNexRevenue = totalRevenue.reduce(
            (partialSum, a) => partialSum + a,
            0,
        );

        dataDetailPrice.push([
            '3. Dự xuất chi ăn sáng hôm sau',
            null,
            null,
            null,
        ]);
        dataDetailPrice.push(['Quân số', null, null, sumNexMorning]);
        dataDetailPrice.push(['Tổng thu', null, null, sumNexRevenue]);
        dataDetailPrice.push(['Gạo, than, gas, gia vị', null, null, '']);
        dataDetailPrice.push(['Chi khác', null, null, '']);
        dataDetailPrice.push([
            'Thực phẩm',
            null,
            null,
            this.sumWithInitial(thanhTienHomSau),
        ]);
        dataDetailPrice.push(['Tổng chi', null, null, '']);
        dataDetailPrice.push(['So sánh', null, null, '']);

        dataDetailPrice = dataDetailPrice.concat.apply([], dataDetailPrice);

        let totalRowTable = new Array(listColumnKey.length).fill('', 0);
        dataDetailMenu.push(new Array(dataDetailMenu[0].length).fill('', 0));
        //Empty row
        if (dataDetailMenu.length + dataDetailMenuTomorrow.length < 48) {
            let length = dataDetailMenu.length + dataDetailMenuTomorrow.length;
            let missing = 48 - length;
            for (let i = 0; i <= missing; i++) {
                dataDetailMenu.push(totalRowTable);
            }
        }

        dataDetailMenuTomorrow.push(
            new Array(dataDetailMenuTomorrow[0].length).fill('', 0),
        ); //Empty row
        dataDetailMenuTomorrow.push(totalRowTable);

        let listColumnTitle = listColumnKey.map((item) => {
            switch (item) {
                case 'tenLttpChatDot':
                    return 'Tên LT-TP, Chất đốt';
                case 'duXuat':
                    return 'Xuất trong ngày';
                case 'soLuongTrenBdMua':
                    return 'Nhập trong ngày';
                case 'donViTinh':
                    return 'ĐVT';

                default:
                    return this.getColumnTitle(item);
            }
        });

        let alignmentCenter = {
            vertical: 'middle',
            horizontal: 'center',
        };

        let alignmentDefault = {
            vertical: 'middle',
        };

        let fontDefault = {
            name: 'Times New Roman',
            family: 1,
            size: 13,
        };

        let fontBold = {
            ...fontDefault,
            bold: true,
        };

        let fontTextMediumBold = {
            size: 14,
            ...fontBold,
        };

        var worksheet = workbook.addWorksheet(filename, {
            pageSetup: {
                paperSize: 9,
                orientation: 'portrait',
                fitToPage: true,
            }, //portrait,landscape
        });

        worksheet.pageSetup.printTitlesRow = '7:9';

        worksheet.addRow([]);

        let titleRow = worksheet.addRow([
            `${this._dataService
                .getTenDonViCapTrenTrucTiep()
                .name.toUpperCase()}`,
            '',
            'BẢNG DỰ XUẤT LƯƠNG THỰC-THỰC PHẨM, CHẤT ĐỐT',
        ]);

        titleRow.eachCell((cell, number) => {
            if (number === 1) {
                cell.font = fontDefault;
            }

            if (number === 3) {
                cell.font = {
                    ...fontBold,
                    size: 18,
                };
            }
        });

        titleRow.alignment = alignmentCenter;

        let titleRow2 = worksheet.addRow([
            `${this._dataService.getDonViThuocQuanLy().name.toUpperCase()}`,
            '',
            `Ngày ${this.getDayMonthYear(
                data.ngayDuXuat,
                'day',
            )} tháng ${this.getDayMonthYear(
                data.ngayDuXuat,
                'month',
            )} năm ${this.getDayMonthYear(data.ngayDuXuat, 'year')}`,
        ]);

        titleRow2.eachCell((cell, number) => {
            if (number === 1) {
                cell.font = {
                    ...fontBold,
                    underline: true,
                };
            }

            if (number === 3) {
                cell.font = {
                    ...fontBold,
                };
            }
        });
        titleRow2.alignment = alignmentCenter;

        worksheet.addRow([]);
        worksheet.addRow([]);

        worksheet.addRow([
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            'ĐVT: đồng',
        ]).font = { ...fontDefault, italic: true };

        let headerRowEmpty = new Array(listColumnTitle.length).fill('', 0);

        let head2 = [...headerRowEmpty];
        head2[3] = 'Trên BĐ';
        head2[4] = '';
        head2[5] = 'Đơn vị BĐ';
        head2[6] = '';
        head2[7] = 'Thành tiền';
        head2[8] = 'SL';
        head2[9] = 'Thành tiền';
        head2[10] = `Ngày ${this.getDayMonthYear(
            data.ngayDuXuat,
            'day',
        )} tháng ${this.getDayMonthYear(
            data.ngayDuXuat,
            'month',
        )} năm ${this.getDayMonthYear(data.ngayDuXuat, 'year')}`;

        let head3 = [...headerRowEmpty];
        head3[3] = 'Mua TT';
        head3[4] = 'TGSX';
        head3[5] = 'Mua TT';
        head3[6] = 'TGSX';
        head3[7] = '';
        head3[8] = '';
        head3[9] = '';
        head3[10] = '1. Phần tiền';

        let headerRow = worksheet.addRow([...listColumnTitle, 'DỰ XUẤT']);
        let headerRow2 = worksheet.addRow(head2);
        let headerRow3 = worksheet.addRow(head3);

        worksheet.mergeCells('C2:L2');
        worksheet.mergeCells('C3:L3');
        worksheet.mergeCells('A7:A9');
        worksheet.mergeCells('B7:B9');
        worksheet.mergeCells('C7:C9');
        worksheet.mergeCells('D7:H7');
        worksheet.mergeCells('D8:E8');
        worksheet.mergeCells('F8:G8');
        worksheet.mergeCells('H8:H9');
        worksheet.mergeCells('I7:J7');
        worksheet.mergeCells('I8:I9');
        worksheet.mergeCells('J8:J9');
        worksheet.mergeCells('K7:N7');
        worksheet.mergeCells('K8:N8');
        worksheet.mergeCells('K9:N9');

        headerRow.eachCell((cell, number) =>
            this.headerTitleCenter(cell, number),
        );
        headerRow2.eachCell((cell, number) =>
            this.headerTitleCenter(cell, number),
        );
        headerRow3.eachCell((cell, number) =>
            this.headerTitleCenter(cell, number),
        );

        // Add data
        dataDetailMenu.forEach((d, index, array) => {
            if (index === dataDetailMenu.length - 1) {
                d[0] = 'Cộng';
                d[9] = this.sumWithInitial(thanhTien);
            }

            let row = worksheet.addRow(d);
            let qty = row.getCell(6);

            row.eachCell((cell, number) => {
                if (
                    number === 4 ||
                    number === 5 ||
                    number === 6 ||
                    number === 7 ||
                    number === 9
                ) {
                    cell.numFmt = '""#.##0;[Red]-""#.##0.000';
                }
                if (number === 3 || number === 8 || number === 10) {
                    cell.numFmt = '""#,##0;[Red]-""#,##0';
                }
                cell.font = fontDefault;

                //Last row table change style border
                if (index === array.length - 1) {
                    cell.border = {
                        top: { style: 'thin' },
                        left: { style: 'thin' },
                        bottom: { style: 'thin' },
                        right: { style: 'thin' },
                    };

                    cell.font = {
                        ...fontBold,
                    };
                } else {
                    cell.border = {
                        top: { style: 'dotted' },
                        left: { style: 'thin' },
                        bottom: { style: 'dotted' },
                        right: { style: 'thin' },
                    };
                }

                if (
                    cell.value === 'Sáng' ||
                    cell.value === 'Trưa' ||
                    cell.value === 'Chiều'
                ) {
                    cell.font = fontBold;
                }

                cell.alignment = alignmentDefault;
            });
        });

        dataDetailMenuTomorrow.forEach((d, index, array) => {
            if (index === dataDetailMenuTomorrow.length - 1) {
                d[0] = 'Cộng';
                d[7] = thanhTienDuNhap;
                d[9] = this.sumWithInitial(thanhTienHomSau);
            }

            let row = worksheet.addRow(d);
            let qty = row.getCell(6);

            row.eachCell((cell, number) => {
                if (
                    number === 4 ||
                    number === 5 ||
                    number === 6 ||
                    number === 7 ||
                    number === 9
                ) {
                    cell.numFmt = '""#.##0;[Red]-""#.##0.000';
                }
                if (number === 3 || number === 8 || number === 10) {
                    cell.numFmt = '""#,##0;[Red]-""#,##0';
                }
                cell.font = fontDefault;

                //Last row table change style border
                if (index === array.length - 1) {
                    cell.border = {
                        top: { style: 'thin' },
                        left: { style: 'thin' },
                        bottom: { style: 'thin' },
                        right: { style: 'thin' },
                    };

                    cell.font = {
                        ...fontBold,
                    };

                    cell.numFmt = '""#,##0;[Red]-""#,##0';
                } else {
                    cell.border = {
                        top: { style: 'dotted' },
                        left: { style: 'thin' },
                        bottom: { style: 'dotted' },
                        right: { style: 'thin' },
                    };
                }

                if (cell.value === 'Sáng hôm sau') {
                    cell.value = 'Dự xuất chi ăn sáng hôm sau';
                    cell.font = fontBold;
                }

                cell.alignment = alignmentDefault;
            });
        });

        const lastRow = worksheet.lastRow._number;
        const row = this.selectRange(worksheet, `K10:N${lastRow}`);

        dataDetailPrice.forEach((data, indexData) => {
            row.forEach((cell, indexCell) => {
                if (indexData === indexCell) {
                    cell.value = data;

                    if (this.isFloat(data)) {
                        cell.numFmt = '""#,##0.00;[Red]-""#,##0.000';
                    }

                    if (this.isInt(data)) {
                        cell.numFmt = '""#,##0;[Red]-""#,##0.000';
                    }

                    cell.border = {
                        top: { style: 'dotted' },
                        left: { style: 'thin' },
                        bottom: { style: 'dotted' },
                        right: { style: 'thin' },
                    };

                    cell.font = fontDefault;
                } else {
                    cell.border = {
                        top: { style: 'dotted' },
                        left: { style: 'thin' },
                        bottom: { style: 'dotted' },
                        right: { style: 'thin' },
                    };
                }

                cell.alignment = alignmentDefault;
            });
        });

        worksheet.getColumn('A').width = 40;
        worksheet.getColumn('C').width = 20;
        worksheet.getColumn('H').width = 20;
        worksheet.getColumn('J').width = 15;
        worksheet.getColumn('K').width = 15;
        worksheet.getColumn('L').width = 20;
        worksheet.getColumn('N').width = 15;
        worksheet.addRow([]);
        let rowDate = worksheet.addRow([
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            `Ngày ${this.getDayMonthYear(
                data.ngayDuXuat,
                'day',
            )} tháng ${this.getDayMonthYear(
                data.ngayDuXuat,
                'month',
            )} năm ${this.getDayMonthYear(data.ngayDuXuat, 'year')}`,
        ]);
        rowDate.font = {
            ...fontDefault,
            italic: true,
        };
        rowDate.alignment = {
            ...alignmentCenter,
        };
        worksheet.addRow([]);
        let rowChucDanh = worksheet.addRow([
            '',
            'Thủ trưởng đơn vị',
            null,
            '',
            '',
            '',
            'Trợ lý hậu cần',
            null,
            '',
            '',
            'Bếp trưởng',
        ]);
        rowChucDanh.font = fontBold;
        rowChucDanh.alignment = {
            ...alignmentCenter,
        };

        worksheet.addRow([]);
        worksheet.addRow([]);
        worksheet.addRow([]);
        worksheet.addRow([]);
        worksheet.addRow([]);

        let rowChucDanh2 = worksheet.addRow([
            '',
            `${this._dataService.getTenThuTruongKyDuyet()
                ? this._dataService.getTenThuTruongKyDuyet()
                : ''
            }`,
            null,
            '',
            '',
            '',
            `${this._dataService.getTroLyHauCan()
                ? this._dataService.getTroLyHauCan()
                : ''
            }`,
            null,
            '',
            '',
            `${this._dataService.getBepTruong()
                ? this._dataService.getBepTruong()
                : ''
            }`,
        ]);
        rowChucDanh2.font = fontBold;
        rowChucDanh2.alignment = {
            ...alignmentCenter,
        };

        let skipRow = dataDetailMenu.length + dataDetailMenuTomorrow.length + 6;
        worksheet.mergeCells(`K${skipRow + 5}:L${skipRow + 5}`);
        worksheet.mergeCells(`B${skipRow + 7}:C${skipRow + 7}`);
        worksheet.mergeCells(`G${skipRow + 7}:H${skipRow + 7}`);
        worksheet.mergeCells(`K${skipRow + 7}:L${skipRow + 7}`);
        worksheet.mergeCells(`B${skipRow + 13}:C${skipRow + 13}`);
        worksheet.mergeCells(`G${skipRow + 13}:H${skipRow + 13}`);
        worksheet.mergeCells(`K${skipRow + 13}:L${skipRow + 13}`);

        const excelBuffer: any = workbook.xlsx.writeBuffer();
        workbook.xlsx.writeBuffer().then(function (buffer) {
            // Done buffering
            //   this.saveExcelFile(buffer, fileName)
            //
            const data: Blob = new Blob([buffer], {
                type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            });
            FileSaver.saveAs(data, filename);
        });
    }

    public generateExportChiaSuatCom(dataNecessary, data, filename): void {
        let options = {
            filename,
            useStyles: true,
            useSharedStrings: true,
        };
        let workbook = new ExcelJS.Workbook(options);

        const { ngayXuatBaoCao } = dataNecessary;

        const title = [
            `${this._dataService
                .getTenDonViCapTrenTrucTiep()
                .name.toUpperCase()}`,
            '',
            '',
            '',
            'BÁO CÁO CHIA SUẤT CƠM',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
        ];

        const title2 = [
            '',
            '',
            '',
            '',
            `Ngày ${this.getDayMonthYear(
                ngayXuatBaoCao,
                'day',
            )} tháng ${this.getDayMonthYear(
                ngayXuatBaoCao,
                'month',
            )} năm ${this.getDayMonthYear(ngayXuatBaoCao, 'year')}`,
            '',
            '',
            '',
            '',
            '',
            '',
            '',
        ];

        let listMucTienAn = [81000];
        let dataDetail = [];

        data.forEach((object) => {
            listMucTienAn.push(object.mucTienAn);
        });

        listMucTienAn = listMucTienAn
            .filter((elem, index, self) => index === self.indexOf(elem))
            .sort((a, b) => a - b);

        const listDonViId = data
            .map((item) => item.donViId)
            .filter((item, index, arr) => arr.indexOf(item) === index);

        const chiTietDonVi = listDonViId.map((donViId) => {
            const filters = data.filter((item) => item['donViId'] === donViId);

            return {
                donViId,
                soLuongDoiTuong: filters.length,
            };
        });

        let listColumnKey = [
            'donViName',
            'doiTuongName',
            'quanSo',
            '',
            '',
            '',
            'quanSoKhongAn',
            '',
            '',
        ];

        listMucTienAn.forEach((mucTienAn, index) => {
            const dataImport = [
                `M.ăn ${this.transformCurrency(mucTienAn)}đ/người`,
                '',
                '',
            ];

            listColumnKey.splice(3 * index + 3, 0, ...dataImport);
        });

        listColumnKey.join();

        let listColumnKey2 = [
            '',
            '',
            '',
            'Bàn 6',
            'Xuất lẻ',
            '',
            'Bệnh viện',
            'Bệnh xá',
            'Khác',
        ];

        listMucTienAn.forEach((mucTienAn) => {
            listColumnKey2.splice(3, 0, 'Quân số ăn', '', '');
        });

        listColumnKey2.join();

        let listColumnKey3 = ['', '', '', '', '', '', '', '', ''];

        listMucTienAn.forEach((mucTienAn) => {
            listColumnKey3.splice(3, 0, 'sang', 'trua', 'chieu');
        });

        listColumnKey3.join();

        const mucTienAnTransfer = listMucTienAn.reduce((acc, cur, index) => {
            const data = [
                `key${3 * (index + 2) - 3}`,
                `key${3 * (index + 2) - 2}`,
                `key${3 * (index + 2) - 1}`,
            ];

            return acc.concat(data);
        }, []);

        const getSoLuongKhongAn = (dsKhongAn, lyDo) => {
            let soLuong = 0;

            const found = dsKhongAn.find((ds) => ds.lyDo === lyDo);

            soLuong = found ? found['quanSo'] : 0;
            return soLuong;
        };

        data.forEach((item, index) => {
            let objInfos = [item.donViName, item.doiTuongName, item.quanSo];

            listMucTienAn.forEach((mucTienAn, viTri, danhSach) => {
                if (item.mucTienAn === mucTienAn) {
                    objInfos.splice(
                        3 * viTri + 3,
                        0,
                        item.sang,
                        item.trua,
                        item.chieu,
                    );
                } else {
                    objInfos.splice(3 * viTri + 3, 0, '', '', '');
                }

                if (viTri === danhSach.length - 1) {
                    objInfos.push(
                        '',
                        '',
                        '',
                        getSoLuongKhongAn(item.danhSachKhongAn, 'Bệnh viện'),
                        getSoLuongKhongAn(item.danhSachKhongAn, 'Bệnh xá'),
                        getSoLuongKhongAn(item.danhSachKhongAn, 'Khác'),
                    );
                }
            });

            dataDetail = [...dataDetail, objInfos];
        });

        const chiTietSoLuong = dataDetail.reduce((acc, cur, index) => {
            const soLuongObj = cur.reduce((obj, ele, i) => {
                return {
                    ...obj,
                    [`key${i}`]: !ele ? 0 : ele,
                };
            }, {});

            return acc.concat(soLuongObj);
        }, []);

        dataDetail.push([
            'Tổng cộng',
            '',
            '',
            ...mucTienAnTransfer.map((mucTienAn, index) =>
                this.sum(chiTietSoLuong, mucTienAn),
            ),
            '',
            '',
            '',
            ...Object.keys(chiTietSoLuong[0])
                .filter((key, index, arr) => index >= arr.length - 3)
                .map((key, index) => this.sum(chiTietSoLuong, key)),
        ]);

        dataDetail.push([
            'Tổng quân số',
            '',
            this.sum(data, 'quanSo'),
            ...mucTienAnTransfer.map((mucTienAn, index) => ''),
            '',
            '',
            '',
            '',
            '',
            '',
        ]);

        let listColumnTitle = listColumnKey.map((item) => {
            switch (item) {
                case 'donViName':
                    return 'Đơn vị';
                case 'doiTuongName':
                    return 'Đối tượng';
                case 'quanSo':
                    return 'Quân số';
                case 'quanSoKhongAn':
                    return 'Quân số không ăn';

                default:
                    return this.getColumnTitle(item);
            }
        });

        let listColumnTitle3 = listColumnKey3.map((item) => {
            switch (item) {
                case 'sang':
                    return 'S';
                case 'trua':
                    return 'T';
                case 'chieu':
                    return 'C';

                default:
                    return this.getColumnTitle(item);
            }
        });

        let alignmentCenter = {
            vertical: 'middle',
            horizontal: 'center',
        };

        let fontDefault = {
            name: 'Times New Roman',
            family: 1,
            size: 13,
        };

        let fontBold = {
            ...fontDefault,
            bold: true,
        };

        let fontTextMediumBold = {
            size: 14,
            ...fontBold,
        };

        // workbook.creator = 'Me';
        // workbook.lastModifiedBy = 'Her';
        // workbook.created = new Date(1985, 8, 30);
        // workbook.modified = new Date();
        // workbook.lastPrinted = new Date(2016, 9, 27);
        // create a sheet with red tab colour
        var worksheet = workbook.addWorksheet(filename, {
            pageSetup: {
                paperSize: 9,
                orientation: 'portrait',
                fitToPage: true,
            }, //portrait,landscape
        });

        let titleRow = worksheet.addRow(title);

        worksheet.addRow([]);
        let titleRow2 = worksheet.addRow(title2);
        worksheet.mergeCells('A1:D1');
        worksheet.mergeCells('E1:O2');
        worksheet.mergeCells('E3:O3');
        worksheet.addRow([]);

        //Add Header Row
        let headerRow = worksheet.addRow(listColumnTitle);
        let headerRow2 = worksheet.addRow(listColumnKey2);
        let headerRow3 = worksheet.addRow(listColumnTitle3);

        const dynamicColumns = listMucTienAn
            .reduce(
                (acc, cur) => {
                    const data = [`${cur}s`, `${cur}t`, `${cur}c`];

                    return acc.concat(data);
                },
                ['', '', '', '', '', ''],
            )
            .map((item, index) => this.columnToLetter(index + 4));
        const paginateDynamicCols = this.paginate(dynamicColumns, 3);

        worksheet.mergeCells('A5:A7');
        worksheet.mergeCells('B5:B7');
        worksheet.mergeCells('C5:C7');
        paginateDynamicCols.forEach((item, index, arr) => {
            worksheet.mergeCells(`${item[0]}5:${item[item.length - 1]}5`);
            if (index < arr.length - 2) {
                worksheet.mergeCells(`${item[0]}6:${item[item.length - 1]}6`);
            }
        });

        headerRow.eachCell((cell, number) => {
            cell.border = {
                top: { style: 'thin' },
                left: { style: 'thin' },
                bottom: { style: 'thin' },
                right: { style: 'thin' },
            };

            cell.alignment = {
                wrapText: true,
                vertical: 'middle',
                horizontal: 'center',
            };

            cell.font = {
                ...fontBold,
            };
        });

        headerRow2.eachCell((cell, number) => {
            cell.border = {
                top: { style: 'thin' },
                left: { style: 'thin' },
                bottom: { style: 'thin' },
                right: { style: 'thin' },
            };

            cell.alignment = {
                wrapText: true,
                vertical: 'middle',
                horizontal: 'center',
            };

            cell.font = {
                ...fontBold,
            };
        });

        headerRow3.eachCell((cell, number) => {
            cell.border = {
                top: { style: 'thin' },
                left: { style: 'thin' },
                bottom: { style: 'thin' },
                right: { style: 'thin' },
            };

            cell.alignment = {
                wrapText: true,
                vertical: 'middle',
                horizontal: 'center',
            };

            cell.font = {
                ...fontBold,
            };
        });

        // Add Data and Conditional Formatting
        dataDetail.forEach((d, index, array) => {
            let row = worksheet.addRow(d);
            let qty = row.getCell(6);

            row.eachCell((cell, number) => {
                if (number === 1) {
                    cell.alignment = {
                        wrapText: true,
                        vertical: 'middle',
                        horizontal: 'center',
                    };
                } else if (number === 2) {
                    cell.alignment = {
                        wrapText: true,
                        vertical: 'middle',
                        horizontal: 'left',
                    };
                } else if (number >= 3) {
                    cell.alignment = {
                        wrapText: true,
                        vertical: 'middle',
                        horizontal: 'center',
                    };
                }

                cell.font = fontDefault;

                //Last row table change style border
                cell.border = {
                    top: { style: 'thin' },
                    left: { style: 'thin' },
                    bottom: { style: 'thin' },
                    right: { style: 'thin' },
                };

                if (index >= array.length - 2) {
                    if (number === 1) {
                        cell.alignment = {
                            wrapText: true,
                            vertical: 'middle',
                            horizontal: 'left',
                        };
                    }
                }
            });
        });

        const rowsSkip = 7 + dataDetail.length;

        worksheet.addRow([]);
        worksheet.addRow([]);

        // NGÀY THÁNG NĂM KÝ
        worksheet.getCell(
            `P${rowsSkip + 3}`,
        ).value = `Ngày ${this.getDayMonthYear(
            this.currentDate,
            'day',
        )} tháng ${this.getDayMonthYear(
            this.currentDate,
            'month',
        )} năm ${this.getDayMonthYear(this.currentDate, 'year')}`;
        worksheet.getCell(`P${rowsSkip + 3}`).font = {
            ...fontDefault,
            italic: true,
        };
        worksheet.getCell(`P${rowsSkip + 3}`).alignment = alignmentCenter;
        worksheet.mergeCells(`P${rowsSkip + 3}:R${rowsSkip + 3}`);

        // CHỨC DANH
        worksheet.getCell(`A${rowsSkip + 4}`).value = 'NGƯỜI LẬP BÁO CÁO';
        worksheet.getCell(`A${rowsSkip + 4}`).font = fontBold;
        worksheet.getCell(`A${rowsSkip + 4}`).alignment = alignmentCenter;
        worksheet.mergeCells(`A${rowsSkip + 4}:C${rowsSkip + 4}`);

        worksheet.getCell(`P${rowsSkip + 4}`).value = 'THỦ TRƯỞNG ĐƠN VỊ';
        worksheet.getCell(`P${rowsSkip + 4}`).font = fontBold;
        worksheet.getCell(`P${rowsSkip + 4}`).alignment = alignmentCenter;
        worksheet.mergeCells(`P${rowsSkip + 4}:R${rowsSkip + 4}`);

        worksheet.addRow([]);
        worksheet.addRow([]);
        worksheet.addRow([]);
        worksheet.addRow([]);
        worksheet.addRow([]);

        // TÊN CHỨC DANH
        worksheet.getCell(
            `A${rowsSkip + 10}`,
        ).value = `${this._dataService.getNguoiLapBaoCao()}`;
        worksheet.getCell(`A${rowsSkip + 10}`).font = fontBold;
        worksheet.getCell(`A${rowsSkip + 10}`).alignment = alignmentCenter;
        worksheet.mergeCells(`A${rowsSkip + 10}:C${rowsSkip + 10}`);

        worksheet.getCell(
            `P${rowsSkip + 10}`,
        ).value = `${this._dataService.getTenThuTruongKyDuyet()}`;
        worksheet.getCell(`P${rowsSkip + 10}`).font = fontBold;
        worksheet.getCell(`P${rowsSkip + 10}`).alignment = alignmentCenter;
        worksheet.mergeCells(`P${rowsSkip + 10}:R${rowsSkip + 10}`);

        //FILE CSS
        titleRow.alignment = alignmentCenter;
        titleRow.font = fontDefault;
        titleRow2.alignment = alignmentCenter;
        titleRow2.font = fontDefault;

        worksheet.getCell('E1').font = {
            ...fontBold,
            size: 18,
        };

        worksheet.getColumn(1).width = 20;
        worksheet.getColumn(2).width = 25;
        paginateDynamicCols.forEach((item, index, arr) => {
            if (index === arr.length - 1) {
                item.forEach((col, i) => {
                    worksheet.getColumn(col).width = 15;
                });
            }
        });

        let startPos = 8;

        chiTietDonVi.forEach((ct, i) => {
            const count = ct['soLuongDoiTuong'];
            const endPos = startPos + count - 1;
            worksheet.mergeCells(`A${startPos}:A${endPos}`);
            startPos = endPos + 1;
        });

        const excelBuffer: any = workbook.xlsx.writeBuffer();
        workbook.xlsx.writeBuffer().then(function (buffer) {
            // Done buffering
            //   this.saveExcelFile(buffer, fileName)
            //
            const data: Blob = new Blob([buffer], {
                type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            });
            FileSaver.saveAs(data, `${filename}.xlsx`);
        });
    }

    public generateMenuExcel(data, filename): void {
        let options = {
            filename,
            useStyles: true,
            useSharedStrings: true,
        };
        let workbook = new ExcelJS.Workbook(options);

        let min = 5,
            medium = 15,
            large = 20;

        let listColumnKey = [
            'Bữa ăn',
            ...data.data.map(
                (item) =>
                    'Ngày ' +
                    this.getDayMonthYear(item.ngayAn, 'day') +
                    '/' +
                    this.getDayMonthYear(item.ngayAn, 'month') +
                    '/' +
                    this.getDayMonthYear(item.ngayAn, 'year'),
            ),
        ];

        let alignmentCenter = {
            vertical: 'middle',
            horizontal: 'center',
        };

        let fontDefault = {
            name: 'Times New Roman',
            family: 1,
            size: 14,
        };

        let fontBold = {
            ...fontDefault,
            bold: true,
        };

        let fontTextMediumBold = {
            size: 14,
            ...fontBold,
        };

        var worksheet = workbook.addWorksheet(filename, {
            pageSetup: {
                paperSize: 9,
                orientation: 'portrait',
                fitToPage: true,
            }, //portrait,landscape
        });

        let title = [
            `Đơn vị ${this._dataService.getDonViThuocQuanLy().name}`,
            '',
            '',
            '',
            '',
            'CỘNG HOÀ XÃ HỘI CHỦ NGHĨA VIỆT NAM',
            '',
            '',
        ];
        let cssTitle = worksheet.addRow(title);
        cssTitle.font = fontBold;
        cssTitle.alignment = alignmentCenter;

        let title2 = [
            'PHÊ DUYỆT',
            '',
            '',
            '',
            '',
            'Độc lập - Tự do - Hạnh phúc',
            '',
            '',
        ];
        let cssTitle2 = worksheet.addRow(title2);
        cssTitle2.font = fontBold;
        cssTitle2.alignment = alignmentCenter;

        let title3 = [`Ngày   ${data.monthAndYearInPrinter}`, ''];
        let cssTitle3 = worksheet.addRow(title3);
        cssTitle3.alignment = alignmentCenter;
        cssTitle3.font = fontDefault;

        let title4 = [
            'HIỆU TRƯỞNG',
            '',
            '',
            '',
            '',
            `Khánh Hoà, ${data.dateInPrinter}`,
            '',
            '',
        ];
        let cssTitle4 = worksheet.addRow(title4);
        cssTitle4.alignment = alignmentCenter;
        cssTitle4.font = fontDefault;

        worksheet.addRow();

        let title5 = ['', '', '', 'THỰC ĐƠN TUẦN', '', '', '', ''];
        let cssTitle5 = worksheet.addRow(title5);
        cssTitle5.font = fontBold;
        cssTitle5.alignment = alignmentCenter;

        let listObjectEat = [];

        data.listPrice.forEach((object) => {
            listObjectEat.push(
                `${this.transformCurrency(object)}đ/người/ngày;`,
            );
        });

        let title6 = [
            '',
            '',
            '',
            `( Đối tượng ăn: ${listObjectEat} )`,
            '',
            '',
            '',
            '',
        ];
        let cssTitle6 = worksheet.addRow(title6);
        cssTitle6.alignment = alignmentCenter;
        cssTitle6.font = fontDefault;

        let title7 = [
            '',
            '',
            '',
            `${data.dateStartAndEndInPrinter}`,
            '',
            '',
            '',
            '',
        ];
        let cssTitle7 = worksheet.addRow(title7);
        cssTitle7.alignment = alignmentCenter;
        cssTitle7.font = fontDefault;

        worksheet.addRow();

        const mornings = this.getTimeSection(
            data.data,
            0,
            'Sáng',
            data.listFood,
        );
        const halfDays = this.getTimeSection(
            data.data,
            1,
            'Trưa',
            data.listFood,
        );
        const evenings = this.getTimeSection(
            data.data,
            2,
            'Chiều',
            data.listFood,
        );

        let dataDetail = [...mornings, ...halfDays, ...evenings];

        //Add Header Row
        let headerRowEmpty = new Array(listColumnKey.length).fill('', 0);
        let head2 = [...headerRowEmpty];
        head2[1] = 'Thứ 2';
        head2[2] = 'Thứ 3';
        head2[3] = 'Thứ 4';
        head2[4] = 'Thứ 5';
        head2[5] = 'Thứ 6';
        head2[6] = 'Thứ 7';
        head2[7] = 'Chủ nhật';

        let headerRow = worksheet.addRow(listColumnKey);
        let headerRow2 = worksheet.addRow(head2);

        //Merged title row 2
        worksheet.mergeCells('A1:B1');
        worksheet.mergeCells('F1:H1');
        worksheet.mergeCells('A2:B2');
        worksheet.mergeCells('F2:H2');
        worksheet.mergeCells('A3:B3');
        worksheet.mergeCells('A4:B4');
        worksheet.mergeCells('F4:H4');
        worksheet.mergeCells('D6:F6');
        worksheet.mergeCells('D7:F7');
        worksheet.mergeCells('D8:F8');
        worksheet.mergeCells('A10:A11');

        headerRow.eachCell((cell, number) => {
            (cell.font = fontBold),
                (cell.border = {
                    top: { style: 'thin' },
                    left: { style: 'thin' },
                    bottom: { style: 'thin' },
                    right: { style: 'thin' },
                }),
                (cell.alignment = {
                    wrapText: true,
                    vertical: 'middle',
                    horizontal: 'center',
                });
        });
        headerRow2.eachCell((cell, number) => {
            (cell.font = fontBold),
                (cell.border = {
                    top: { style: 'thin' },
                    left: { style: 'thin' },
                    bottom: { style: 'thin' },
                    right: { style: 'thin' },
                }),
                (cell.alignment = {
                    wrapText: true,
                    vertical: 'middle',
                    horizontal: 'center',
                });
        });

        // Add Data and Conditional Formatting
        dataDetail.forEach((d, index, array) => {
            let row = worksheet.addRow(d);

            row.eachCell((cell, number) => {
                if (number === 1) {
                    cell.alignment = {
                        vertical: 'middle',
                        horizontal: 'center',
                    };

                    cell.font = fontBold;
                } else {
                    cell.alignment = {
                        vertical: 'middle',
                        horizontal: 'left',
                    };

                    cell.font = fontDefault;
                }

                cell.border = {
                    top: { style: 'thin' },
                    left: { style: 'thin' },
                    bottom: { style: 'thin' },
                    right: { style: 'thin' },
                };
            });
        });

        let bottomTitle = ['', '', '', '', '', '', 'PHÓ CHỦ NHIỆM HẬU CẦN', ''];
        let cssBottomTitle = worksheet.addRow(bottomTitle);
        cssBottomTitle.font = fontBold;

        let bottomTitle2 = ['Ghi chú:', '', '', '', '', '', '', ''];
        let cssBottomTitle2 = worksheet.addRow(bottomTitle2);
        cssBottomTitle2.font = fontBold;

        worksheet.getColumn(1).width = 15;
        worksheet.getColumn(2).width = 25;
        worksheet.getColumn(3).width = 25;
        worksheet.getColumn(4).width = 25;
        worksheet.getColumn(5).width = 25;
        worksheet.getColumn(6).width = 25;
        worksheet.getColumn(7).width = 25;
        worksheet.getColumn(8).width = 25;

        //FILE CSS
        worksheet.getCell('A4').font = fontBold;

        const excelBuffer: any = workbook.xlsx.writeBuffer();
        workbook.xlsx.writeBuffer().then(function (buffer) {
            // Done buffering
            //   this.saveExcelFile(buffer, fileName)
            //
            const data: Blob = new Blob([buffer], {
                type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            });
            FileSaver.saveAs(data, filename);
        });
    }

    private getTimeSection = (data, timeNumber, name, listFood) => {
        const buoi = ['Sáng', 'Trưa', 'Chiều'];
        let max = 0;
        // tim max mon an
        data.forEach((e) => {
            let chiTietNgay = e.chiTietNgay;
            chiTietNgay.forEach((ct) => {
                if (ct.buoiAn === buoi[timeNumber]) {
                    if (ct.danhSach.length > max) {
                        max = ct.danhSach.length;
                    }
                }
            });
        });
        // push vo tung mang
        let trs = [];
        for (let i = 0; i < max; i++) {
            let tds = [];
            if (i == 0) {
                tds.push(name);
            } else {
                tds.push('');
            }
            data.forEach((item, index) => {
                // check nhung ngay co hoac ko co data
                if (!item['chiTietNgay'][timeNumber]) {
                    tds.push('');
                    return;
                }
                if (!item['chiTietNgay'][timeNumber]['danhSach']) {
                    tds.push('');
                    return;
                }
                if (item['chiTietNgay'][timeNumber]['danhSach'][i]) {
                    let dish = listFood.find(
                        (food) =>
                            food.key ===
                            item['chiTietNgay'][timeNumber]['danhSach'][i][
                            'monAnId'
                            ],
                    );
                    tds.push(dish.name);
                } else {
                    tds.push('');
                }
            });
            trs.push(tds);
        }
        return trs;
    };

    private getColumnTitle(column: string) {
        let listTitle = {
            TT: 'TT',
            tenLttpChatDot: 'TÊN, QUI CÁCH, KÝ MÃ HIỆU VẬT TƯ',
            donViTinh: 'Đơn vị tính',
            soLuongPhaiNhap: 'SỐ LƯỢNG',
            soLuongPhaiXuat: 'SỐ LƯỢNG',
            soLuong: 'Số lượng',
            diaChiMuaHangName: 'Tên người bán hoặc địa chỉ mua hàng',
            soLuongThucNhap: '',
            donGia: 'Đơn giá',
            thanhTien: 'Thành tiền',
            thanhTienNhap: 'Thành tiền',
            ghiChu: 'GHI CHÚ',
            // duXuat: 'DỰ XUẤT',
            // soLuongThucTe: 'GHI CHÚ',
            // thanhTienThucTe: 'GHI CHÚ',
            // soLuongThua: 'GHI CHÚ',
            // thanhTienThua: 'GHI CHÚ',
            // soLuongThieu: 'GHI CHÚ',
            // thanhTienThieu: 'GHI CHÚ',
            tenQuanNhan: 'Họ và tên',
            totalAnThem: 'Cộng',
            ngayChamCom: 'Ngày',
        };
        if (!listTitle.hasOwnProperty(column)) {
            return column;
        }

        return listTitle[column];
    }

    private isEmty = (value) => !value;

    private isEven = (value) => !(value % 2);

    private isInt(n) {
        return Number(n) === n && n % 1 === 0;
    }

    private isFloat(n) {
        return Number(n) === n && n % 1 !== 0;
    }

    private transformDate(value) {
        return this.convertNumToStr.transform(value);
    }

    private sumWithInitial(listItem, initialValue = 0) {
        return listItem.reduce(
            (accumulator, currentValue) => accumulator + currentValue,
            initialValue,
        );
    }

    private getDayMonthYear(date, type) {
        let time = new Date(date);
        let day = time.getDate();
        let dayTemp = ('0' + day.toString()).slice(-2);
        let month = time.getMonth();
        let monthTemp = ('0' + (month + 1).toString()).slice(-2);
        let year = time.getFullYear();

        switch (type) {
            case 'day':
                return dayTemp;
            case 'month':
                return monthTemp;
            case 'year':
                return year;

            default:
                return time;
        }
    }

    private headerTitleCenter = (cell, number) => {
        cell.border = {
            top: { style: 'thin' },
            left: { style: 'thin' },
            bottom: { style: 'thin' },
            right: { style: 'thin' },
        };

        cell.alignment = {
            wrapText: true,
            vertical: 'middle',
            horizontal: 'center',
        };

        cell.font = {
            name: 'Times New Roman',
            family: 1,
            size: 13,
            bold: true,
        };
    };

    private convertDataListTaxes(array) {
        if (array.length === 0) {
            // Fake data when array no value, hot fix -----> NEED UPDATE WHEN DEPLOY
            array = [
                {
                    mucTienAn: 65000,
                    thanhTienSang: 105,
                    thanhTienTrua: 13000,
                    thanhTienChieu: 1365000,
                },
                {
                    mucTienAn: 27222,
                    thanhTienSang: 111,
                    thanhTienTrua: 1300110,
                    thanhTienChieu: 1411,
                },
                {
                    mucTienAn: 1,
                    thanhTienSang: 2,
                    thanhTienTrua: 3,
                    thanhTienChieu: 5,
                },
                {
                    mucTienAn: 27222,
                    thanhTienSang: 111,
                    thanhTienTrua: 1300110,
                    thanhTienChieu: 1411,
                },
            ];
        } else {
            array = array.map((item) => {
                return {
                    mucTienAn: item.mucTienAn,
                    thanhTienSang: item.thanhTienSang,
                    thanhTienTrua: item.thanhTienTrua,
                    thanhTienChieu: item.thanhTienChieu,
                };
            });
        }

        let initialTaxes = {
            mucTienAn: '',
            thanhTienSang: '',
            thanhTienTrua: '',
            thanhTienChieu: '',
        };

        let lengthArr = array.length;
        if (lengthArr % 2 !== 0) {
            array.push(initialTaxes);
        }

        const allTaxes = array.reduce(
            (accumulator, currentValue) => [
                ...accumulator,
                ...Object.values(currentValue),
            ],
            [],
        );

        let temArray = [];
        const chunkSize = allTaxes.length / 2;
        for (let i = 0; i < allTaxes.length / 2; i++) {
            const chunk = [allTaxes[i], allTaxes[i + chunkSize]];
            // do whatever
            temArray.push(chunk);
        }

        return temArray;
    }

    private convertKeyString(key) {
        let objKey = {
            thieuHomTruoc: '- Thiếu Hôm Trước',
            buGiaGao: '- Bù Giá Gạo',
            cong: 'Cộng',
            congChi: 'Cộng Chi',
            congThu: 'Cộng Thu',
            thuCuaKhach: '- Thu Của Khách',
            thuaHomTruoc: '- Thừa Hôm Trước',
            thuaTrongNgay: '- Thừa Trong Ngày',
            tienDaSuDung: '- Tiền Đã Sử Dụng',
            truyLinh: '- Truy Lĩnh',
            danhSachAnThem: '- Ăn thêm: ',
        };

        if (key in objKey) {
            return objKey[key];
        } else {
            return key;
        }
    }

    private sum(array, key) {
        const initialValue = 0;
        const sumWithInitial = array.reduce(
            (accumulator, currentValue) => accumulator + currentValue[key],
            initialValue,
        );

        return sumWithInitial;
    }

    private totalListEarMore(list) {
        if (list.length === 0) {
            return 0;
        }
        list = list.map((item) => {
            return {
                ...item,
                sum: this.sum(item.mucTienAnThem, 'thanhTien'),
            };
        });
        return this.sum(list, 'sum');
    }

    private saveExcelFile(buffer: any, fileName: string): void {
        const data: Blob = new Blob([buffer], { type: this.fileType });
        FileSaver.saveAs(data, fileName + this.fileExtension);
    }

    // Tách mảng lớn thành các mảng nhỏ hơn theo số lượng mong muốn
    private paginate(arr, size) {
        return arr.reduce((acc, val, i) => {
            let idx = Math.floor(i / size);
            let page = acc[idx] || (acc[idx] = []);
            page.push(val);

            return acc;
        }, []);
    }

    private transformCurrency(currency: number) {
        return currency.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.');
    }

    public removeAccents(str) {
        let text = str
            .normalize('NFD')
            .replace(/[\u0300-\u036f]/g, '')
            .replace(/đ/g, 'd')
            .replace(/Đ/g, 'D');

        return text.replace(/\s/g, '');
    }
    selectRange = (sheet: ExcelJS.Worksheet, rangeCell: string) => {
        const [startCell, endCell] = rangeCell.split(':');

        const [endCellColumn, endRow] = endCell.match(
            /[a-z]+|[^a-z]+/gi,
        ) as string[];
        const [startCellColumn, startRow] = startCell.match(
            /[a-z]+|[^a-z]+/gi,
        ) as string[];

        let endColumn = sheet.getColumn(endCellColumn);
        let startColumn = sheet.getColumn(startCellColumn);

        if (!endColumn) throw new Error('End column not found');
        if (!startColumn) throw new Error('Start column not found');

        const endColumnNumber = endColumn.number;
        const startColumnNumber = startColumn.number;

        const cells = [];
        for (let y = parseInt(startRow); y <= parseInt(endRow); y++) {
            const row = sheet.getRow(y);

            for (let x = startColumnNumber; x <= endColumnNumber; x++) {
                cells.push(row.getCell(x));
            }
        }

        return cells;
    };

    private columnToLetter(column) {
        var temp,
            letter = '';
        while (column > 0) {
            temp = (column - 1) % 26;
            letter = String.fromCharCode(temp + 65) + letter;
            column = (column - temp - 1) / 26;
        }
        return letter;
    }

    private letterToColumn(letter) {
        var column = 0,
            length = letter.length;
        for (var i = 0; i < length; i++) {
            column +=
                (letter.charCodeAt(i) - 64) * Math.pow(26, length - i - 1);
        }
        return column;
    }

    private removeLast3Characters(str) {
        str = str.toString();
        str = str.slice(0, -3);
        str = parseInt(str);
        return str;
    }
}
