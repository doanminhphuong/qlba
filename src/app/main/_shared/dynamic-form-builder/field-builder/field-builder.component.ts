import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
    selector: 'field-builder',
    templateUrl: './field-builder.component.html',
})
export class FieldBuilderComponent implements OnInit {
    @Input() field: any;
    @Input() form: any;

    @Input() tabCtrl: any;
    @Input() groupCtrl: any;
    @Input() isFlatForm: any;

    get isValid() {
        return this.form.controls[this.field.codeData].valid;
    }
    get isDirty() {
        return this.form.controls[this.field.codeData].dirty;
    }
    get isTouched() {
        return this.form.controls[this.field.codeData].touched;
    }

    constructor() { }

    ngOnInit() { }
}
