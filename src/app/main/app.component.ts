import {
    Component,
    OnInit,
    AfterViewInit,
    ElementRef,
    ViewChild,
    Output,
    EventEmitter,
} from '@angular/core';
import { LoadingBarService } from '@ngx-loading-bar/core';
import {
    NavigationEnd,
    NavigationStart,
    Router,
    NavigationCancel,
    ResolveEnd,
    ActivatedRoute,
    ActivatedRouteSnapshot,
} from '@angular/router';
import { AppSessionService } from '@shared/session/app-session.service';
import { HoSoSucKhoeService } from '@core/services/ho-so-suc-khoe.service';
import { Menu } from './_shared/header/header.component';
import { MenuService } from '@core/services/menu.service';
import { MatSidenav } from '@angular/material';
import { Title } from '@angular/platform-browser';
import { resolveSoa } from 'dns';
import { filter, map } from 'rxjs/operators';

@Component({
    selector: 'app-main',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit, AfterViewInit {
    @ViewChild('header') header: ElementRef;
    @ViewChild('body') body: ElementRef;
    @ViewChild('sidenav') sidenav: MatSidenav;

    currentUser;
    opened: boolean;
    menus: Menu[];
    menuItemList: any[] = [];
    menuHeader: any[] = [];
    dauMoiBepId: number;
    dataDauMoiBep: any;

    isNewCook: boolean;
    timeLeft: number;
    hasBackdrop: boolean = false;
    hasOpenDraw: boolean = true;
    modeDraw: string = 'side';

    breadcrumbList: Array<any> = [];

    constructor(
        private readonly _router: Router,
        private readonly _title: Title,
        public readonly loader: LoadingBarService,
        public readonly _session: AppSessionService,
        public _hssk: HoSoSucKhoeService,
        public _menuService: MenuService,
        private activatedRoute: ActivatedRoute,
    ) {}

    ngOnInit(): void {
        this._router.events.subscribe((event) => {
            if (event instanceof NavigationStart) {
                this.loader.start();
            }
            if (
                event instanceof NavigationEnd ||
                event instanceof NavigationCancel
            ) {
                this.loader.complete();
            }
        });

        this.getInitDataMenu();
        this.setPageTitle();
        // this.listenRouting();
    }

    ngAfterViewInit() {}

    getInitDataMenu() {
        const data = {
            maxResultCount: 999,
            skipCount: 0,
            sorting: 'Code',
            criterias: [
                {
                    propertyName: 'GroupCode',
                    operation: 6,
                    value: 'bep-an',
                },
            ],
        };

        this._menuService.getMenus(data).subscribe((data: any) => {
            this.menus = data.result;

            let topMenu = this.menus.filter(
                (menu) => JSON.parse(menu.hideValue).keys[0] === 'TOP',
            );
            let leftMenu = this.menus.filter(
                (menu) => JSON.parse(menu.hideValue).keys[0] === undefined,
            );

            this.menuItemList = this.list_to_tree(leftMenu);
            this.menuHeader = this.list_to_tree(topMenu);

            console.log('menuItemList::', this.menuItemList);
        });
    }

    list_to_tree(list) {
        var map = {},
            node,
            roots = [],
            i;

        for (i = 0; i < list.length; i += 1) {
            map[list[i].id] = i; // initialize the map
            list[i].children = []; // initialize the children

            let hiddenValue = JSON.parse(list[i].hideValue);
            list[i].icon = hiddenValue['icon'];
        }

        for (i = 0; i < list.length; i += 1) {
            node = list[i];

            if (node.parentId !== null) {
                // if you have dangling branches check that map[node.parentId] exists
                const isExisting = list[map[node.parentId]];

                if (isExisting) {
                    list[map[node.parentId]].children.push(node);
                }
            } else {
                roots.push(node);
            }
        }
        return roots;
    }

    private setPageTitle(): void {
        const defaultPageTitle = 'Default Page Title';

        this._router.events
            .pipe(
                filter((event) => event instanceof NavigationEnd),
                map(() => {
                    let child = this.activatedRoute.firstChild;

                    if (!child) {
                        return (
                            this.activatedRoute.snapshot.data.title ||
                            defaultPageTitle
                        );
                    }

                    while (child.firstChild) {
                        child = child.firstChild;
                    }

                    if (child.snapshot.data.title) {
                        return child.snapshot.data.title || defaultPageTitle;
                    }
                }),
            )
            .subscribe((title: string) => this._title.setTitle(title));
    }

    showLayout(event: any) {
        this.dauMoiBepId = event;
    }

    showAlert(event: any) {
        this.isNewCook = event;
        this.timeLeft = 5;

        if (this.isNewCook === false) return;

        setInterval(() => {
            if (this.timeLeft > 0) {
                this.timeLeft--;
            } else {
                this._router.navigateByUrl('manage/app/category/ql-bepan');
                this.isNewCook = false;
                localStorage.setItem('isNewCook', 'true');
            }
        }, 1000);
    }

    toggleDraw(value) {
        this.hasOpenDraw = !value;
    }
    togglehasOpenDraw() {
        this.hasOpenDraw = !this.hasOpenDraw;
    }

    listenRouting() {
        let routerUrl: string, routerList: Array<any>, target: any;
        this._router.events.subscribe((router: any) => {
            routerUrl = router.urlAfterRedirects;
            console.log('routerUrl::', routerUrl);
            if (routerUrl && typeof routerUrl === 'string') {
                // 初始化breadcrumb
                target = this.menus;
                this.breadcrumbList.length = 0;
                // 取得目前routing url用/區格, [0]=第一層, [1]=第二層 ...etc
                routerList = routerUrl.slice(1).split('/');
                routerList.forEach((router, index) => {
                    // 找到這一層在menu的路徑和目前routing相同的路徑
                    target = target.find(
                        (page) => page.path.slice(2) === router,
                    );
                    // 存到breadcrumbList到時後直接loop這個list就是麵包屑了
                    this.breadcrumbList.push({
                        name: target.name,
                        // 第二層開始路由要加上前一層的routing, 用相對位置會造成routing錯誤
                        path:
                            index === 0
                                ? target.path
                                : `${
                                      this.breadcrumbList[index - 1].path
                                  }/${target.path.slice(2)}`,
                    });

                    // 下一層要比對的目標是這一層指定的子頁面
                    if (index + 1 !== routerList.length) {
                        target = target.children;
                    }
                });

                console.log('breadcrumbList::', this.breadcrumbList);
            }
        });
    }

    // setupTitleListenner() {
    //     this._router.events
    //         .pipe(filter((ev) => ev instanceof ResolveEnd))
    //         .subscribe((ev: ResolveEnd) => {
    //             const { data } = getDeepestChildSnapshot(ev.state.root);
    //             if (data && data.title) {
    //                 this._title.setTitle(data.title);
    //             }
    //         });
    // }
}

// function getDeepestChildSnapshot(snapshot: ActivatedRouteSnapshot) {
//     let deepestChild = snapshot.firstChild;
//     while (deepestChild?.firstChild != null) {
//         deepestChild = deepestChild.firstChild;
//     }

//     return deepestChild || snapshot;
// }
