import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ToolManagerRoutingModule } from './tool-manager-routing.module';
import { ToolManagerMainComponent } from './tool-manager-main/tool-manager-main.component';
import { ToolManagerStoreComponent } from './tool-manager-store/tool-manager-store.component';
import { ListImportToolManagerComponent } from './list-import-tool-manager/list-import-tool-manager.component';
import { SearchAdvancedComponent } from '@app/_shared/common/search-advanced/search-advanced.component';
import {
    MatButtonModule,
    MatDatepickerModule,
    MatDividerModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatOptionModule,
    MatPaginatorModule,
    MatProgressSpinnerModule,
    MatSelectModule,
    MatTableModule,
} from '@angular/material';
import { LayoutModule } from '@app/_shared/layout.module';
import { InventoryDetailsDialogComponent } from './tool-manager-store/inventory-details-dialog/inventory-details-dialog.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CreateImportToolManagerComponent } from './list-import-tool-manager/create-import-tool-manager/create-import-tool-manager.component';

@NgModule({
    declarations: [
        ToolManagerMainComponent,
        ToolManagerStoreComponent,
        CreateImportToolManagerComponent,
        ListImportToolManagerComponent,
        InventoryDetailsDialogComponent,
    ],
    imports: [
        CommonModule,
        ToolManagerRoutingModule,
        MatProgressSpinnerModule,
        LayoutModule,
        MatTableModule,
        MatIconModule,
        MatPaginatorModule,
        MatDividerModule,
        MatDatepickerModule,
        FormsModule,
        ReactiveFormsModule,
        MatFormFieldModule,
        MatSelectModule,
        MatOptionModule,
        MatButtonModule,
        MatInputModule,
    ],
    entryComponents: [SearchAdvancedComponent, InventoryDetailsDialogComponent],
})
export class ToolManagerModule {}
