import { SelectionModel } from '@angular/cdk/collections';
import { Component, Inject, OnDestroy, OnInit, ViewChild } from '@angular/core';
import {
    MatDialogRef,
    MatPaginator,
    MatSort,
    MatTableDataSource,
    MAT_DIALOG_DATA,
} from '@angular/material';
import { CategoryService } from '@core/services/category.service';
import { EventEmitter } from '@angular/core';
import { of, Subject, Subscription } from 'rxjs';
import { debounceTime, distinctUntilChanged, map, mergeMap, delay } from 'rxjs/operators';

export interface UserData {
    id: string;
    name: string;
    progress: string;
    fruit: string;
}

export interface FieldElement {
    select: number;
    code: string;
    name: string;
    type: string;
    required: string;
    referenceValue: string;
    status: string;
    id?: string;
}

const ELEMENT_DATA: FieldElement[] = [
    // {
    //   code: "string",
    //   name: "string",
    //   type: "string",
    //   required: "string",
    //   referenceValue: "string",
    //   status: "string"
    // }
];

@Component({
    selector: 'app-add-field-dialog',
    templateUrl: './add-field-dialog.component.html',
    styleUrls: ['./add-field-dialog.component.scss'],
})
export class AddFieldDialogComponent implements OnInit, OnDestroy {
    displayedColumns: string[] = [
        'select',
        'code',
        'name',
        'type',
        'required',
        'referenceValue',
        'status',
    ];
    dataSource = new MatTableDataSource<FieldElement>(ELEMENT_DATA);
    selection = new SelectionModel<FieldElement>(true, []);
    onSave = new EventEmitter();

    @ViewChild(MatPaginator) paginator!: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    public applyFilter = new Subject<KeyboardEvent>();
    private subscription: Subscription;

    constructor(
        public dialogRef: MatDialogRef<AddFieldDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any,
        private _categoryService: CategoryService,
    ) {
        this.subscription = this.applyFilter.pipe(
            map(event => (event.target as HTMLInputElement).value),
            debounceTime(500),
            distinctUntilChanged(),
        ).subscribe(filterValue => this.getDataFilter(filterValue));
    }

    ngOnDestroy(): void {
        this.subscription.unsubscribe();
    }

    guid = () => {
        let s4 = () => {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        };
        //return id of format 'aaaaaaaa'-'aaaa'-'aaaa'-'aaaa'-'aaaaaaaaaaaa'
        return (
            s4() +
            s4() +
            '-' +
            s4() +
            '-' +
            s4() +
            '-' +
            s4() +
            '-' +
            s4() +
            s4() +
            s4()
        );
    };

    ngOnInit() { }

    ngAfterViewInit() {
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
    }

    /** Whether the number of selected elements matches the total number of rows. */
    isAllSelected() {
        const numSelected = this.selection.selected.length;
        const numRows = this.dataSource.data.length;
        return numSelected === numRows;
    }

    /** Selects all rows if they are not all selected; otherwise clear selection. */
    toggleAllRows() {
        if (this.isAllSelected()) {
            this.selection.clear();
            return;
        }

        this.selection.select(...this.dataSource.data);
    }

    /** The label for the checkbox on the passed row */
    checkboxLabel(row?: FieldElement): string {
        if (!row) {
            return `${this.isAllSelected() ? 'deselect' : 'select'} all`;
        }
        // return `${this.selection.isSelected(row) ? "deselect" : "select"} row ${row.position + 1}`;
        return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.select + 1
            }`;
    }

    getDataFilter(filterValue) {
        this.dataSource.filter = filterValue.trim().toLowerCase();
        let dataSearch = {
            maxResultCount: 100,
            criterias: [
                {
                    propertyName: 'Search',
                    operation: 'OrEquals',
                    value: `[{"propertyName":"Name","operation":"Contains","value":"${filterValue}"}]`,
                },
            ],
            sorting: 'Number1',
            tenantId: 1,
            language: 'vi',
        };
        this._categoryService
            .searchFieldFormName(dataSearch)
            .subscribe((res: any) => {
                // console.log("data search:", res);
                this.dataSource = new MatTableDataSource<FieldElement>(
                    res.result,
                );
                this.dataSource.paginator = this.paginator;
            });

        if (this.dataSource.paginator) {
            this.dataSource.paginator.firstPage();
        }
    }

    onCloseDialog(data?: any): void {
        this.dialogRef.close(data);
    }

    selectCutrent(): void {
        console.log(this.selection.selected);
    }

    onSubmit() {
        this.selection.selected.map((item) => {
            item.id = this.guid();
        });

        this.onSave.emit(this.selection.selected);
        this.onCloseDialog();
    }
}
