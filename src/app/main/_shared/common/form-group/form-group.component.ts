import { Component, Input, OnInit } from "@angular/core";
import { FormArray, FormBuilder, FormGroup, Validators } from "@angular/forms";

@Component({
  selector: "app-form-group",
  templateUrl: "./form-group.component.html",
  styleUrls: ["./form-group.component.scss"]
})
export class FormGroupComponent implements OnInit {
  @Input("filed") filed: any[];
  GroupDataFieldForm: FormGroup;

  constructor(private fb: FormBuilder) {}

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    console.log("filed Form Group::", this.filed);
    const controlArray = this.filed.map((c) => {
      console.log("c.type::", c.type);
      let validatorEmail =
        c.type === "audio" || c.type === "video"
          ? [c.defaultValue, [Validators.required, Validators.email]]
          : c.defaultValue;
      return this.fb.group({
        name: c.name,
        code: c.code,
        type: c.type,
        required: c.required,
        sizeCol: "" + c.sizeCol,
        defaultValue: validatorEmail
      });
    });

    this.GroupDataFieldForm = this.fb.group({
      fieldForm: new FormArray(controlArray)
    });
  }

  get fieldForm() {
    return (this.GroupDataFieldForm.get("fieldForm") as FormArray).controls;
  }

  get defaultValue() {
    return this.fieldForm[0].get("defaultValue");
  }

  getErrorMessage() {
    if (this.defaultValue.hasError("required")) {
      return "You must enter a value";
    }

    return this.defaultValue.hasError("email") ? "Not a valid email" : "";
  }

  submitForm() {
    console.log("Submit form", this.GroupDataFieldForm.value);
    // console.log("fieldForm", this.fieldForm.value);
    // console.log("this.filed", this.filed);
  }

  renderCol(sizeCol) {
    console.log("sizeCol::", sizeCol);
  }
}
