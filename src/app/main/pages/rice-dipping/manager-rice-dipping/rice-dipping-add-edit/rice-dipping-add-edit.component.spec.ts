import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RiceDippingAddEditComponent } from './rice-dipping-add-edit.component';

describe('RiceDippingAddEditComponent', () => {
  let component: RiceDippingAddEditComponent;
  let fixture: ComponentFixture<RiceDippingAddEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RiceDippingAddEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RiceDippingAddEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
