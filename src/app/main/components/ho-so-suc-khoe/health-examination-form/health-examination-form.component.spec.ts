import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HealthExaminationFormComponent } from './health-examination-form.component';

describe('HealthExaminationFormComponent', () => {
  let component: HealthExaminationFormComponent;
  let fixture: ComponentFixture<HealthExaminationFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HealthExaminationFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HealthExaminationFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
