## Phân Quyền:

Cách 1: Khi vào mỗi URL call API check Permission

-   Link: http://sys.tasp.vn/api/services/SYS/read/MenuClient/CheckPermission
-   Ưu điểm: Code đang hoạt động
-   Nhược: Bad code

Cách 2: call API tất cả quyền, sau đó mỗi URL sẽ check ở local chứ không call API như cách 1

-   Link: http://sso.tasp.vn/api/services/sso/read/SSO/GetUser
-   Ưu điểm: Giảm tải load API
-   Nhược: Hiện tại chưa biết cách xử lý dữ liệu, để check quyền cụ thể cho mỗi tài khoản, giao diện (thiếu Document)
