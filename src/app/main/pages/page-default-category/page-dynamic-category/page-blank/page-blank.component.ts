import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-page-blank',
    templateUrl: './page-blank.component.html',
    styleUrls: ['./page-blank.component.scss'],
})
export class PageBlankComponent implements OnInit {
    constructor(private _activatedRoute: ActivatedRoute) {
        console.log(this._activatedRoute);
        this._activatedRoute.paramMap.subscribe((params) => {
            let data = params.get('valueData');
            console.log('data::', data);
            console.log('params::', params);
        });
    }

    ngOnInit() {}
}
