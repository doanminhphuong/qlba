import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
    selector: 'fb-checkbox',
    template: `
        <div [formGroup]="form">
            <ng-container *ngIf="!isFlatForm && tabCtrl" [formGroupName]="tabCtrl">
                <ng-container [formGroupName]="groupCtrl">
                    <div class="wrapper" [formGroupName]="getFormControlName()">
                        <label for="">{{ field.name }}</label>
                        <div
                            class="checkbox__item"
                            *ngFor="let opt of field.options"
                        >
                            <mat-checkbox [formControlName]="opt.key">
                                {{ opt.name }}
                            </mat-checkbox>
                        </div>
                    </div>
                </ng-container>
            </ng-container>

            <div
                *ngIf="isFlatForm || !tabCtrl"
                class="wrapper"
                [formGroupName]="getFormControlName()"
            >
                <label *ngIf="field.name" for="">{{ field.name }}</label>
                <div class="checkbox__item" *ngFor="let opt of field.options">
                    <mat-checkbox [formControlName]="opt.key">
                        {{ opt.name }}
                    </mat-checkbox>
                </div>
            </div>
        </div>
    `,
    styles: [
        `
            label {
                max-width: 300px;
                font-weight: 600;
            }

            /* .wrapper {
                display: flex;
                align-items: center;
                justify-content: space-between;
            }

            .checkbox__item + .checkbox__item {
                margin-left: 50px;
            }

            label {
                max-width: 300px;
                font-weight: 600;
            } */
        `,
    ],
})
export class CheckBoxComponent implements OnInit {
    @Input() field: any = {};
    @Input() form: FormGroup;
    @Input() tabCtrl: any;
    @Input() groupCtrl: any;
    @Input() isFlatForm: boolean;

    get isValid() {
        return this.form.controls[this.field.referenceValue].valid;
    }
    get isDirty() {
        return this.form.controls[this.field.referenceValue].dirty;
    }

    getFormControlName() {
        if (this.isFlatForm) {
            return `${this.tabCtrl}.${this.groupCtrl}.${this.field.referenceValue}`;
        } else {
            return this.field.referenceValue;
        }
    }

    ngOnInit(): void { }
}
