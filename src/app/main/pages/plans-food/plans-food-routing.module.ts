import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PlanGuaranteeAddEditComponent } from './plan-guarantee/plan-guarantee-add-edit/plan-guarantee-add-edit.component';
import { PlanGuaranteeComponent } from './plan-guarantee/plan-guarantee.component';
import { PlanSpendAddEditComponent } from './plan-spend/plan-spend-add-edit/plan-spend-add-edit.component';
import { PlanSpendComponent } from './plan-spend/plan-spend.component';

const routes: Routes = [
    {
        path: 'du-chi-ngay-ke-tiep',
        // component: PlanSpendComponent,
        children: [
            {
                path: '',
                component: PlanSpendComponent,
            },
            {
                path: 'create',
                component: PlanSpendAddEditComponent,
            },
            {
                path: 'edit/:id',
                component: PlanSpendAddEditComponent,
            },
        ],
    },
    {
        path: 'ke-hoach-LTTP-chat-dot',
        // component: PlanGuaranteeComponent,
        children: [
            {
                path: '',
                component: PlanGuaranteeComponent,
            },
            {
                path: 'add',
                component: PlanGuaranteeAddEditComponent,
            },
            {
                path: 'edit/:id',
                component: PlanGuaranteeAddEditComponent,
            },
        ],
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class PlansFoodRoutingModule {}
