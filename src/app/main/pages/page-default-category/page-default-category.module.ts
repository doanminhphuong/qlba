import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PageDefaultCategoryRoutingModule } from './page-default-category-routing.module';
import { MatProgressSpinnerModule } from '@angular/material';
import { PageDynamicComponent } from './page-dynamic-category/page-dynamic/page-dynamic.component';
// import { PageBlankComponent } from './page-dynamic-category/page-blank/page-blank.component';

@NgModule({
    declarations: [PageDynamicComponent],
    imports: [
        CommonModule,
        MatProgressSpinnerModule,
        PageDefaultCategoryRoutingModule,
    ],
})
export class PageDefaultCategoryModule {}
