import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BoardRiceDetailComponent } from './board-rice-detail.component';

describe('BoardRiceDetailComponent', () => {
  let component: BoardRiceDetailComponent;
  let fixture: ComponentFixture<BoardRiceDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BoardRiceDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BoardRiceDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
