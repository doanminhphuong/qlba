// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `angular-cli.json`.

export const environment = {
    production: false,
    hmr: false,
    appConfig: 'appconfig.json',
    baseUrl: 'http://localhost:4200',
    SWAGGER_URL: 'http://bepan.tasp.vn',
    SSO_URL: 'http://sso.tasp.vn',
    SYS_URL: 'http://sys.tasp.vn',
    DATA_URL: 'http://data.tasp.vn',
};
