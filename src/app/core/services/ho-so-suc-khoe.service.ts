import { Observable } from 'rxjs';
import { Injectable, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { LogData } from '../interfaces/log-data';
import { UtilsService } from './utils.service';
import { tap } from 'rxjs/operators';
import { TokenStorageService } from './token.service';
import { environment } from 'environments/environment';

const TOKEN_KEY = 'auth-token';
const USER_KEY = 'auth-user';

@Injectable()
export class HoSoSucKhoeService implements OnInit {
    BASE_URL = `${environment.SWAGGER_URL}/api/services/`;

    // Get data user from sso
    BASE_URL_SSO = `${environment.SSO_URL}/api/services/sso/`;

    constructor(
        private http: HttpClient,
        private utils: UtilsService,
        private _tokenStorageService: TokenStorageService,
    ) {}

    ngOnInit(): void {}

    getUsers(data: any): Observable<any> {
        let token = this._tokenStorageService.getToken();
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
        });

        let options = { headers: headers };

        const END_POINT = 'read/User/GetUsers';

        return this.http.post<any>(
            `${this.BASE_URL_SSO}${END_POINT}`,
            data,
            options,
        );
    }

    getOrganization(data: any): Observable<any> {
        let token = this._tokenStorageService.getToken();
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
        });

        let options = { headers: headers };

        const END_POINT = 'read/Organization/Get';

        return this.http.post<any>(
            `${this.BASE_URL_SSO}${END_POINT}`,
            data,
            options,
        );
    }

    getPermissionByUser(data: any): Observable<any> {
        let token = this._tokenStorageService.getToken();
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
        });

        let options = { headers: headers };

        const END_POINT = 'read/SSO/GetUser';

        return this.http.post<any>(
            `${this.BASE_URL_SSO}${END_POINT}`,
            data,
            options,
        );
    }

    createHealthExaminationFormDetail(data: any): Observable<any> {
        let token = this._tokenStorageService.getToken();
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
        });

        let options = { headers: headers };

        const END_POINT = 'write/HealthExaminationFormDetail/Create';

        return this.http.post<any>(
            `${this.BASE_URL}${END_POINT}`,
            data,
            options,
        );
    }

    getCurrentUsers(): Observable<any> {
        let token = this._tokenStorageService.getToken();
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
        });

        let options = { headers: headers };

        const END_POINT = 'read/User/GetCurrentUser';

        return this.http.post<any>(
            `${this.BASE_URL_SSO}${END_POINT}`,
            null,
            options,
        );
    }

    getCurrentOrganizations(): Observable<any> {
        let token = this._tokenStorageService.getToken();
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
        });

        let options = { headers: headers };

        const END_POINT = 'read/Organization/GetCurrentOrganizations';

        return this.http.post<any>(
            `${this.BASE_URL_SSO}${END_POINT}`,
            null,
            options,
        );
    }

    getChildrenOrganizations(data: any): Observable<any> {
        let token = this._tokenStorageService.getToken();
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
        });

        let options = { headers: headers };

        const END_POINT = 'read/Organization/GetAllChildren';

        return this.http.post<any>(
            `${this.BASE_URL_SSO}${END_POINT}`,
            data,
            options,
        );
    }
}
