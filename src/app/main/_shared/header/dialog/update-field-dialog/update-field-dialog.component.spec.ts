import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateFieldDialogComponent } from './update-field-dialog.component';

describe('UpdateFieldDialogComponent', () => {
  let component: UpdateFieldDialogComponent;
  let fixture: ComponentFixture<UpdateFieldDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateFieldDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateFieldDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
