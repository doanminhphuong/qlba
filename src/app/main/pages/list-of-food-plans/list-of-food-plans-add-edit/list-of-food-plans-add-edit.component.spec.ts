import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListOfFoodPlansAddEditComponent } from './list-of-food-plans-add-edit.component';

describe('ListOfFoodPlansAddEditComponent', () => {
  let component: ListOfFoodPlansAddEditComponent;
  let fixture: ComponentFixture<ListOfFoodPlansAddEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListOfFoodPlansAddEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListOfFoodPlansAddEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
