import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StoreExportRoutingModule } from './store-export-routing.module';
import { CustomerExportComponent } from './customer-export/customer-export.component';
import { ListExportComponent } from './list-export/list-export.component';
import { FieldExportComponent } from './field-export/field-export.component';
import { SummaryExportComponent } from './summary-export/summary-export.component';
import { LayoutModule } from '@app/_shared/layout.module';
import {
    MatCheckboxModule,
    MatDividerModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatPaginatorModule,
    MatProgressSpinnerModule,
    MatTableModule,
} from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ListExportAddEditComponent } from './list-export/list-export-add-edit/list-export-add-edit.component';
import { SummaryExportAddEditComponent } from './summary-export/summary-export-add-edit/summary-export-add-edit.component';

@NgModule({
    declarations: [
        CustomerExportComponent,
        ListExportComponent,
        FieldExportComponent,
        SummaryExportComponent,
        ListExportAddEditComponent,
        SummaryExportAddEditComponent,
    ],
    imports: [
        CommonModule,
        StoreExportRoutingModule,
        LayoutModule,
        MatIconModule,
        MatDividerModule,
        MatPaginatorModule,
        MatProgressSpinnerModule,
        MatTableModule,
        FormsModule,
        ReactiveFormsModule,
        MatFormFieldModule,
        MatInputModule,
        MatCheckboxModule,
    ],
})
export class StoreExportModule {}
