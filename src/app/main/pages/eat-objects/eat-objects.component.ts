import { SelectionModel } from '@angular/cdk/collections';
import { Component, OnInit, ViewChild } from '@angular/core';
import {
    MatDialog,
    MatPaginator,
    MatTableDataSource,
    PageEvent,
} from '@angular/material';
import { Router } from '@angular/router';
import { DynamicDialogComponent } from '@app/components/dynamic-dialog-component/dynamic-dialog-component.component';
import { DeleteConfirmDialogComponent } from '@app/_shared/dialogs/delete-confirm-dialog/delete-confirm-dialog.component';
import { CategoryService } from '@core/services/category.service';
import { ToastrService } from '@core/services/toastr.service';
import { UserService } from '@core/services/user.service';
import { forkJoin, Subscription } from 'rxjs';
import { FieldService } from '@core/services/field.service';
import { SwapunitService } from '@core/services/swapunit.service';
import { EatobjectsService } from '@core/services/eatobjects.service';
import { QuantityService } from '@core/services/quantity.service';
import { CommonService } from '@core/services/common.service';
import { isNgTemplate } from '@angular/compiler';
import { DataService } from '@core/services/data-service';
@Component({
    selector: 'app-eat-objects',
    templateUrl: './eat-objects.component.html',
    styleUrls: ['./eat-objects.component.scss'],
})
export class EatObjectsComponent implements OnInit {
    @ViewChild(MatPaginator) paginator!: MatPaginator;

    displayedColumns: string[] = ['codeData', 'name', 'mucTienAn', 'actions'];

    currentDate: Date = new Date();

    displayedColumnsOfField: string[] = [
        'codeData',
        'name',
        'mucTienAn',
        'actions',
    ];

    dataSource = new MatTableDataSource<any>([]);
    selection = new SelectionModel<any>(true, []);
    userList: any[] = [];
    dataList: any;
    inventoryList: any[] = [];
    totalPrice: number = 0;
    currentTable: any[] = [];
    swapunitOptions: any[] = [];
    EatObjectOptions: any[] = [];
    typeOptions: any[] = [];
    unitOptions: any[] = [];
    mealrateOptions: any[] = [];

    valueData: string;

    listMealRate: any[] = [];

    // Pagination
    totalRows = 0;
    pageSize = 10;
    currentPage = 0;
    pageSizeOptions: number[] = [5, 10, 25, 50];
    dataSkipped: number = 0;

    typeDialog: string;
    isFiltering: boolean = false;
    indexUnit: number;
    isLoading: boolean = false;
    subscription: Subscription;

    constructor(
        private _router: Router,
        public dialog: MatDialog,
        private _fieldService: FieldService,
        private _toastrService: ToastrService,
        private _categoryService: CategoryService,
        private _quantitveService: QuantityService,
        private _commonService: CommonService,
        private _swapunitService: SwapunitService,
        private _eatobjectsService: EatobjectsService,
        private _userService: UserService,
        private _dataService: DataService,
    ) {
        this.subscription = this._dataService.getData().subscribe((message) => {
            this.isLoading = true;
            this.getInitData(message.id);
        });
    }

    ngOnInit() {
        this.getInitData(this._dataService.getDauMoiBepId());
        this.getMultiCategories();
    }

    ngAfterViewInit() {
        this.dataSource.paginator = this.paginator;
    }

    getInitData(id: number): void {
        this.getAllEatObject(id);
    }

    getAllEatObject(id: number): void {
        const bodyEatObjects = {
            maxResultCount: this.pageSize,
            skipCount: this.dataSkipped,
            criterias: [
                {
                    propertyName: 'dauMoiBepId',
                    operation: 0,
                    value: id,
                },
            ],
        };
        this._eatobjectsService
            .getAllEatObject(bodyEatObjects)
            .subscribe((res) => {
                this.dataSource.data = res.result.items;
                setTimeout(() => {
                    this.paginator.pageIndex = this.currentPage;
                    this.paginator.length = res.result.totalCount;
                });

                setTimeout(() => {
                    this.isLoading = false;
                }, 200);
            });
    }

    pageChanged(event: PageEvent) {
        this.currentPage = event.pageIndex;
        this.dataSkipped = event.pageIndex * 10;

        this.getAllEatObject(this._dataService.getDauMoiBepId());
    }

    getMultiCategories() {
        this.getMealRateOptions();
        this.getTypeOptions();
    }

    getBodyCategory(groupCode: string) {
        return {
            maxResultCount: 10,
            skipCount: 0,
            sorting: 'Code',
            criterias: [
                {
                    propertyName: 'GroupCode',
                    operation: 6,
                    value: groupCode,
                },
            ],
        };
    }

    getMealRateOptions() {
        this._commonService
            .callDataAPIShort('/api/services/read/DinhLuongAn/GetAll')
            .subscribe((res) => {
                this.mealrateOptions = res.result.items.map((item) => ({
                    key: item.mucTienAn,
                    name: item.mucTienAn,
                }));
            });
    }

    getTypeOptions() {
        this._categoryService
            .getAllCategory(this.getBodyCategory('doi-tuong-an'))
            .subscribe((res) => {
                this.typeOptions = res.result.map((item) => ({
                    key: item.codeData,
                    name: item.name,
                }));
            });
    }

    guid = () => {
        let s4 = () => {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        };
        //return id of format 'aaaaaaaa'-'aaaa'-'aaaa'-'aaaa'-'aaaaaaaaaaaa'
        return (
            s4() +
            s4() +
            '-' +
            s4() +
            '-' +
            s4() +
            '-' +
            s4() +
            '-' +
            s4() +
            s4() +
            s4()
        );
    };

    openDialog(dataDialog: any): void {
        let fields = [];
        this.typeDialog = 'doi-tuong-an';
        fields = [
            {
                type: 'TEXT',
                referenceValue: 'codeData',
                name: 'Mã đối tượng ăn',
                defaultValue: '',
                required: '1',
                css: 'col-12 col-lg-4',
            },
            {
                type: 'TEXT',
                referenceValue: 'name',
                name: 'Tên đối tượng ăn',
                defaultValue: '',
                required: '1',
                css: 'col-12 col-lg-4',
            },

            {
                type: 'SELECT',
                referenceValue: 'mucTienAn',
                name: 'Mức tiền ăn',
                defaultValue: '',
                required: '1',
                css: 'col-12 col-lg-4',
                options: [...this.mealrateOptions],
            },
        ];
        // DialogSwapunitComponent
        let dialogRef = this.dialog.open(DynamicDialogComponent, {
            // disableClose: true,
            width: '70%',
            data: {
                datas: dataDialog,
                fields: fields,
                typeDialog: this.typeDialog,
            },
            panelClass: 'my-custom-dialog-class',
        });

        dialogRef.componentInstance.onSave.subscribe((formValues) => {
            const isSubmitted = !!formValues;
            let dauMoiBepId = this._dataService.getDauMoiBepId();
            let dataSubmit = {
                ...dataDialog,
                ...formValues,
                dauMoiBepId,
            };

            // Handle dialog for Category
            if (!dataDialog) {
                this._eatobjectsService.createEatObject(dataSubmit).subscribe(
                    (res) => dialogRef.close(isSubmitted),
                    (err) => this._toastrService.errorServer(err),
                );
            } else {
                this._eatobjectsService.updateEatObject(dataSubmit).subscribe(
                    (res) => dialogRef.close(isSubmitted),
                    (err) => this._toastrService.errorServer(err),
                );
            }
        });
        dialogRef.afterClosed().subscribe((result) => {
            if (!result) return;

            this.getInitData(this._dataService.getDauMoiBepId());

            if (!dataDialog) {
                this._toastrService.success('', 'Thêm thành công');
            } else {
                this._toastrService.success('', 'Cập nhật thành công');
            }
        });
    }

    editAction(element: any): void {
        this.openDialog(element);
    }

    createAction(): void {
        this.openDialog(null);
    }

    // handleAction(element: any, typeAction): void {
    //     this._router.navigate([
    //         `manage/app/category/bang-ke-mua-hang/edit/${element.id}`,
    //         { typeAction },
    //     ]);
    // }

    deleteAction(element: any): void {
        const dialogRef = this.dialog.open(DeleteConfirmDialogComponent, {
            width: '500px',
            data: element,
        });

        dialogRef.componentInstance.handleDelete.subscribe((dataDelete) => {
            const isSubmitted = !!dataDelete;

            this._eatobjectsService.deleteEatObject(dataDelete).subscribe(
                (res) => dialogRef.close(isSubmitted),
                (err) => this._toastrService.errorServer(err),
            );
        });

        dialogRef.afterClosed().subscribe((result) => {
            if (!result) return;

            this.getInitData(this._dataService.getDauMoiBepId());
            this._toastrService.success('', 'Xóa thành công');
        });
    }
}
