import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { BoardRiceComponent } from './board-rice/board-rice.component';
import { ManagerRiceDippingComponent } from './manager-rice-dipping/manager-rice-dipping.component';
import { RiceDippingRoutingModule } from './rice-dipping-routing.module';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
    MatButtonModule,
    MatCheckboxModule,
    MatDividerModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatMenuModule,
    MatOptionModule,
    MatPaginatorModule,
    MatProgressSpinnerModule,
    MatSelectModule,
    MatTableModule,
    MatTabsModule,
    MatTooltipModule,
} from '@angular/material';
import { LayoutModule } from '@app/_shared/layout.module';
import { NgxPaginationModule } from 'ngx-pagination';
import { BoardRiceDetailComponent } from './board-rice/board-rice-detail/board-rice-detail.component';
import { EaterInfoDialogComponent } from './manager-rice-dipping/eater-info-dialog/eater-info-dialog.component';
import { RiceDippingAddEditComponent } from './manager-rice-dipping/rice-dipping-add-edit/rice-dipping-add-edit.component';
import { SearchComponent } from '@app/_shared/common/search/search.component';

@NgModule({
    declarations: [
        ManagerRiceDippingComponent,
        BoardRiceComponent,
        RiceDippingAddEditComponent,
        EaterInfoDialogComponent,
        BoardRiceDetailComponent,
    ],
    imports: [
        CommonModule,
        RiceDippingRoutingModule,
        MatIconModule,
        MatDividerModule,
        MatPaginatorModule,
        MatProgressSpinnerModule,
        MatTableModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatOptionModule,
        MatCheckboxModule,
        MatMenuModule,
        MatTabsModule,
        MatTooltipModule,
        NgxPaginationModule,
        MatButtonModule,

        FormsModule,
        ReactiveFormsModule,
        LayoutModule,
    ],
    entryComponents: [SearchComponent],
})
export class RiceDippingModule {}
