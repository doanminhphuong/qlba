import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListExportAddEditComponent } from './list-export-add-edit.component';

describe('ListExportAddEditComponent', () => {
  let component: ListExportAddEditComponent;
  let fixture: ComponentFixture<ListExportAddEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListExportAddEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListExportAddEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
