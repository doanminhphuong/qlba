import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogSwapUnitComponent } from './dialog-swap-unit.component';

describe('DialogSwapUnitComponent', () => {
  let component: DialogSwapUnitComponent;
  let fixture: ComponentFixture<DialogSwapUnitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogSwapUnitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogSwapUnitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
