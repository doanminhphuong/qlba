import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ManagerRiceDippingComponent } from './manager-rice-dipping/manager-rice-dipping.component';
import { BoardRiceComponent } from './board-rice/board-rice.component';
import { RiceDippingAddEditComponent } from './manager-rice-dipping/rice-dipping-add-edit/rice-dipping-add-edit.component';
import { BoardRiceDetailComponent } from './board-rice/board-rice-detail/board-rice-detail.component';

const routes: Routes = [
    {
        path: 'quan-ly-cham-com',
        children: [
            {
                path: '',
                component: BoardRiceComponent,
            },
            {
                path: ':id',
                component: BoardRiceDetailComponent,
            },
        ],
    },
    {
        path: 'quan-ly-quan-so-an',
        children: [
            {
                path: '',
                component: ManagerRiceDippingComponent,
            },
            {
                path: 'create',
                component: RiceDippingAddEditComponent,
            },
            {
                path: 'edit/:id',
                component: RiceDippingAddEditComponent,
            },
        ],
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class RiceDippingRoutingModule {}
