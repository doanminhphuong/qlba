import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatPaginator, MatTableDataSource } from '@angular/material';
import { DynamicDialogComponent } from '@app/components/dynamic-dialog-component/dynamic-dialog-component.component';
import { DeleteConfirmDialogComponent } from '@app/_shared/dialogs/delete-confirm-dialog/delete-confirm-dialog.component';
import { CircularService } from '@core/services/circular-service';
import { CommonService } from '@core/services/common.service';
import { DataService } from '@core/services/data-service';
import { ToastrService } from '@core/services/toastr.service';
import { forkJoin } from 'rxjs';

export const DATA_EXAMPLE = [
    {
        soThongTu: '168/2021/TT-BQP',
        tenThongTu: 'Quy định tiêu chuẩn, định lượng ăn và mức ...',
        ngayBanHanh: '29/08/2022',
        trangThai: '1',
    },
    {
        soThongTu: '168/2021/TT-BQP',
        tenThongTu: 'Quy định tiêu chuẩn, định lượng ăn và mức ...',
        ngayBanHanh: '29/08/2022',
        trangThai: '1',
    },
];

@Component({
    selector: 'app-basic-quantification',
    templateUrl: './basic-quantification.component.html',
    styleUrls: ['./basic-quantification.component.scss'],
})
export class BasicQuantificationComponent implements OnInit {
    form: FormGroup;
    field: any[] = [];

    displayedColumnsOfField = [
        'index',
        'lttp',
        'dvt',
        'usedPercent',
        'used100',
        'actions',
    ];

    dataSource = new MatTableDataSource<any>([]);
    @ViewChild(MatPaginator) paginator!: MatPaginator;
    allowExec: boolean = false;
    isEdit: boolean = false;
    currentTable: any[] = [];
    listLTTP: any[] = [];
    listĐVT: any[] = [];
    listLTTPCurrent: any[] = [];
    dataEdit: any;

    currenSelectedLTTP: any;
    currenSelectedĐVT: any;

    constructor(
        public dialog: MatDialog,
        private _toastrService: ToastrService,
        private circularService: CircularService,
        private commonService: CommonService,
    ) {
        // let request = {
        //     id: localStorage.getItem('idMenu'),
        //     language: 'vi',
        // };
        // this.commonService.checkPermission(request).subscribe((response) => {
        //     if (JSON.parse(response.result).some((obj) => obj.key === 'EXEC')) {
        //         this.allowExec = true;
        //     }
        // });
    }

    ngOnInit() {
        this.getInit();
    }

    getInit() {
        let getDataLTTP = this.commonService.callDataAPIShort(
            '/api/services/read/NhomLttpChatDot/GetAll',
            {
                maxResultCount: 99999999,
            },
        );

        let getDataĐVT = this.commonService.callDataAPIShort(
            '/api/services/read/Category/GetList',
            {
                maxResultCount: 99999999,
                criterias: [
                    {
                        propertyName: 'GroupCode',
                        operation: 6,
                        value: 'don-vi-tinh',
                    },
                ],
            },
        );

        let getDataInit = this.commonService.callDataAPIShort(
            '/api/services/read/Category/GetList',
            {
                maxResultCount: 99999999,
                criterias: [
                    {
                        propertyName: 'GroupCode',
                        operation: 6,
                        value: 'dinh-luong-co-ban',
                    },
                ],
            },
        );

        forkJoin([getDataLTTP, getDataĐVT, getDataInit]).subscribe(
            (response) => {
                this.listLTTP = response[0].result.items.map((item) => ({
                    key: item.id,
                    name: item.tenNhomLttpChatDot,
                }));

                this.listLTTPCurrent = this.listLTTP;

                this.listĐVT = response[1].result.map((item) => ({
                    key: item.codeData,
                    name: item.name,
                }));

                this.getInitData();
            },
        );
    }

    getInitData() {
        this.commonService
            .callDataAPIShort('/api/services/read/Category/GetList', {
                maxResultCount: 99999999,
                criterias: [
                    {
                        propertyName: 'GroupCode',
                        operation: 6,
                        value: 'dinh-luong-co-ban',
                    },
                ],
            })
            .subscribe((respone) => {
                this.currentTable = respone.result;

                this.dataSource = new MatTableDataSource<any>(
                    this.currentTable,
                );

                this.dataSource.paginator = this.paginator;

                if (respone.result.length > 0) {
                    this.listLTTP = this.listLTTPCurrent;

                    respone.result.forEach((data) => {
                        this.listLTTP = this.listLTTP.filter(
                            (item) => item.key !== data.number1,
                        );
                    });
                } else {
                    this.listLTTP = this.listLTTPCurrent;
                }

                this.field = [
                    {
                        type: 'SELECT',
                        referenceValue: 'number1',
                        name: 'Chọn nhóm lương thực thực phẩm, chất đốt',
                        defaultValue: '',
                        required: '1',
                        css: 'col-12 col-lg-3',
                        options: [...this.listLTTP],
                        appearance: 'legacy',
                        search: '1',
                        searchCtrl: 'searchCtrl',
                    },
                    {
                        type: 'SELECT',
                        referenceValue: 'value1',
                        name: 'Chọn đơn vị tính',
                        defaultValue: '',
                        required: '1',
                        css: 'col-12 col-lg-3',
                        options: [...this.listĐVT],
                        appearance: 'legacy',
                        search: '1',
                        searchCtrl: 'searchCtrl',
                    },
                    {
                        type: 'NUMBER',
                        referenceValue: 'number2',
                        name: '% sử dụng được',
                        defaultValue: '',
                        required: '1',
                        css: 'col-12 col-lg-3',
                        appearance: 'legacy',
                    },
                    {
                        type: 'NUMBER',
                        referenceValue: 'number3',
                        name: 'Nhiệt lượng trên 100g sử dụng',
                        defaultValue: '',
                        required: '1',
                        css: 'col-12 col-lg-3',
                        appearance: 'legacy',
                    },
                ];

                this.getInitForm();
            });
    }

    getInitForm(data?: any) {
        let fieldsCtrls = {};
        let fieldList = [];

        for (let f of this.field) {
            fieldList = [...fieldList, f];

            if (f.type === 'NUMBER' || f.type === 'PASSWORD') {
                fieldsCtrls[f.referenceValue] = new FormControl(
                    { value: f.defaultValue, disabled: f.disabled },
                    f.required === '1' ? [Validators.required] : [],
                );
            } else if (f.type !== 'CHECKBOX') {
                let validators = [
                    Validators.minLength(f.minLength),
                    Validators.maxLength(f.maxLength),
                    Validators.pattern(f.pattern),
                ];

                if (f.type === 'EMAIL') {
                    validators = [...validators, Validators.email];
                }

                fieldsCtrls[f.referenceValue] = new FormControl(
                    {
                        value: f.defaultValue || '',
                        disabled: f.disabled,
                    },
                    f.required === '1'
                        ? [...validators, Validators.required]
                        : [...validators],
                );
            } else {
                //if checkbox, it need multiple
                let opts = {};
                for (let opt of f.options) {
                    opts[opt.key] = new FormControl({
                        value: opt.value,
                        disabled: f.disabled,
                    });
                }
                fieldsCtrls[f.referenceValue] = new FormGroup(opts);
            }
        }

        this.form = new FormGroup(fieldsCtrls);

        if (data) {
            this.isEdit = true;
        } else {
            this.isEdit = false;
        }
    }

    guid = () => {
        let s4 = () => {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        };
        //return id of format 'aaaaaaaa'-'aaaa'-'aaaa'-'aaaa'-'aaaaaaaaaaaa'
        return (
            s4() +
            s4() +
            '-' +
            s4() +
            '-' +
            s4() +
            '-' +
            s4() +
            '-' +
            s4() +
            s4() +
            s4()
        );
    };

    goBackFormCreate() {
        this.field = [
            {
                type: 'SELECT',
                referenceValue: 'number1',
                name: 'Chọn nhóm lương thực thực phẩm, chất đốt',
                defaultValue: '',
                required: '1',
                css: 'col-12 col-lg-3',
                options: [...this.listLTTP],
                appearance: 'legacy',
                search: '1',
                searchCtrl: 'searchCtrl',
            },
            {
                type: 'SELECT',
                referenceValue: 'value1',
                name: 'Chọn đơn vị tính',
                defaultValue: '',
                required: '1',
                css: 'col-12 col-lg-3',
                options: [...this.listĐVT],
                appearance: 'legacy',
                search: '1',
                searchCtrl: 'searchCtrl',
            },
            {
                type: 'NUMBER',
                referenceValue: 'number2',
                name: '% sử dụng được',
                defaultValue: '',
                required: '1',
                css: 'col-12 col-lg-3',
                appearance: 'legacy',
            },
            {
                type: 'NUMBER',
                referenceValue: 'number3',
                name: 'Nhiệt lượng trên 100g sử dụng',
                defaultValue: '',
                required: '1',
                css: 'col-12 col-lg-3',
                appearance: 'legacy',
            },
        ];
        this.getInitForm();
    }

    onSubmit() {
        let nameLTTP = this.listLTTP.filter(
            (value) => value.key === this.form.value.number1,
        );

        let nameĐVT = this.listĐVT.filter(
            (value) => value.key === this.form.value.value1,
        );

        let bodySubmit = {
            ...this.form.value,
            codeData: this.guid(),
            index: this.currentTable.length + 1,
            value2: nameLTTP[0].name,
            value3: nameĐVT[0].name,
            groupCode: 'dinh-luong-co-ban',
        };

        this.commonService
            .callDataAPIShort('/api/services/write/Category/Create', bodySubmit)
            .subscribe((response) => {
                if (response.success) {
                    this.form.reset();
                    this._toastrService.success('', 'Tạo thành công');
                    this.getInitData();
                }
            });
    }

    editData(data: any) {
        let tempList = [];
        let tempList2 = [];
        this.dataEdit = data;

        tempList = this.listLTTPCurrent.filter(
            ({ key: id1 }) =>
                !this.currentTable.some(({ number1: id2 }) => id2 === id1),
        );

        tempList2 = this.listLTTPCurrent.filter(
            (item) => item.key === data.number1,
        );

        this.field = [
            {
                type: 'SELECT',
                referenceValue: 'number1',
                name: 'Chọn nhóm lương thực thực phẩm, chất đốt',
                defaultValue: data.number1,
                required: '1',
                css: 'col-12 col-lg-3',
                options: [...tempList, ...tempList2],
                appearance: 'legacy',
                search: '1',
                searchCtrl: 'searchCtrl',
            },
            {
                type: 'SELECT',
                referenceValue: 'value1',
                name: 'Chọn đơn vị tính',
                defaultValue: data.value1,
                required: '1',
                css: 'col-12 col-lg-3',
                options: [...this.listĐVT],
                appearance: 'legacy',
                search: '1',
                searchCtrl: 'searchCtrl',
            },
            {
                type: 'NUMBER',
                referenceValue: 'number2',
                name: '% sử dụng được',
                defaultValue: data.number2,
                required: '1',
                css: 'col-12 col-lg-3',
                appearance: 'legacy',
            },
            {
                type: 'NUMBER',
                referenceValue: 'number3',
                name: 'Nhiệt lượng trên 100g sử dụng',
                defaultValue: data.number3,
                required: '1',
                css: 'col-12 col-lg-3',
                appearance: 'legacy',
            },
        ];

        this.getInitForm(data);
    }

    submitDataEdit() {
        let nameLTTP = this.listLTTPCurrent.filter(
            (value) => value.key === this.form.value.number1,
        );

        let nameĐVT = this.listĐVT.filter(
            (value) => value.key === this.form.value.value1,
        );
        let bodyEdit = {
            ...this.dataEdit,
            ...this.form.value,
            value2: nameLTTP[0].name,
            value3: nameĐVT[0].name,
        };

        this.commonService
            .callDataAPIShort('/api/services/write/Category/Update', bodyEdit)
            .subscribe((response) => {
                if (response.success) {
                    this.goBackFormCreate();
                    this._toastrService.success('', 'Chỉnh sửa thành công');
                    this.getInitData();
                }
            });
    }

    deleteData(data: any) {
        const dialogRef = this.dialog.open(DeleteConfirmDialogComponent, {
            width: '500px',
            data: data,
        });

        dialogRef.componentInstance.handleDelete.subscribe((dataDelete) => {
            const isSubmitted = !!dataDelete;

            this.commonService
                .callDataAPIShort(
                    '/api/services/write/Category/Delete',
                    dataDelete,
                )
                .subscribe(
                    (res) => dialogRef.close(isSubmitted),
                    (err) => this._toastrService.errorServer(err),
                );
        });

        dialogRef.afterClosed().subscribe((result) => {
            if (!result) return;

            this.getInitData();
            this._toastrService.success('', 'Xóa thành công');
        });
    }

    // handleSearch(data: any) {
    //     this.dataSource.data = this.currentTable.filter((item) =>
    //         item.soThongTu.toLowerCase().includes(data.trim().toLowerCase()),
    //     );
    // }

    // handleSearchAdvanced(data: any) {
    //     if (data.key === 'reset') {
    //         this.getInitData();
    //     } else {
    //         let bodySubmit = {
    //             criterias: [
    //                 {
    //                     propertyName: data.ngayCoHieuLuc ? 'ngayCoHieuLuc' : '',
    //                     operation: data.ngayCoHieuLuc ? 'Equals' : 'Contains',
    //                     value: data.ngayCoHieuLuc
    //                         ? new Date(data.ngayCoHieuLuc._d).toISOString()
    //                         : '',
    //                 },
    //                 {
    //                     propertyName:
    //                         data.trangThai !== null && data.trangThai !== ''
    //                             ? 'trangThai'
    //                             : '',
    //                     operation:
    //                         data.trangThai !== null && data.trangThai !== ''
    //                             ? 'Equals'
    //                             : 'Contains',
    //                     value:
    //                         data.trangThai !== null && data.trangThai !== ''
    //                             ? data.trangThai
    //                             : '',
    //                 },
    //             ],
    //         };
    //         this.circularService
    //             .getAllCirculars(bodySubmit)
    //             .subscribe((response) => {
    //                 if (response.success) {
    //                     this.dataSource = new MatTableDataSource<any>(
    //                         response.result.items,
    //                     );
    //                     this.dataSource.paginator = this.paginator;
    //                 }
    //             });
    //     }
    // }
}
