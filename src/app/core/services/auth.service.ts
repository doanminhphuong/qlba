import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AppConsts } from '@shared/AppConsts';
import { environment } from 'environments/environment';
import { Observable, of, Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { TokenStorageService } from './token.service';

interface User {
    userNameOrEmailAddress: string;
    password: string;
}
const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
};

@Injectable({
    providedIn: 'root',
})
export class AppAuthService {
    constructor(
        private http: HttpClient,
        private tokenStorageService: TokenStorageService,
    ) {}
    BASE_URL = `${environment.SSO_URL}/api/`;

    setSession(token) {
        this.tokenStorageService.saveToken(token);
    }

    login(userNameOrEmailAddress: string, password: string): Observable<any> {
        return this.http
            .post<User>(
                `${this.BASE_URL}TokenAuth/Authenticate`,
                {
                    userNameOrEmailAddress,
                    password,
                },
                httpOptions,
            )
            .pipe(
                map((res: any) => {
                    let token = res.result.accessToken;
                    this.setSession(token);
                }),
            );
    }

    logout(reload?: boolean): void {
        abp.auth.clearToken();
        if (reload !== false) {
            location.href = AppConsts.appBaseHref;
        }
    }
}
