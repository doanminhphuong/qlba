import {
    ChangeDetectorRef,
    Component,
    EventEmitter,
    Inject,
    OnInit,
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
    selector: 'app-update-field-dialog',
    templateUrl: './update-field-dialog.component.html',
    styleUrls: ['./update-field-dialog.component.scss'],
})
export class UpdateFieldDialogComponent implements OnInit {
    onSave = new EventEmitter();
    title: string;
    UpdateFieldForm: FormGroup;

    constructor(
        public dialogRef: MatDialogRef<UpdateFieldDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any,
        private fb: FormBuilder,
    ) { }

    ngOnInit() {
        this.initForm();
        this.UpdateFieldForm.patchValue(this.data.data);
        if (this.data.type === 'AddGroupData') {
            this.title = 'Thêm Tab';
        } else if (this.data.type === 'AddGroupFieldData') {
            this.title = 'Thêm mới';
        } else {
            this.title = `Chỉnh sửa ${this.data.data.name}`;
        }
    }

    onNoClick(): void {
        this.dialogRef.close('');
    }

    initForm() {
        this.UpdateFieldForm = this.fb.group({
            required: [
                '0',
                // Validators.compose([
                //     Validators.required,
                //     Validators.minLength(1),
                //     // this.NoWhitespaceValidator(),
                // ]),
            ],
            css: [''],
            format: [''],
            validate: [''],
            dependField: [''],
            formula: [''],
            formulaView: [''],
            category: ['0'], //BE yeu cau hard code
            id: [this.guid()],
        });
    }

    get name() {
        return this.UpdateFieldForm.get('name');
    }

    guid = () => {
        let s4 = () => {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        };
        //return id of format 'aaaaaaaa'-'aaaa'-'aaaa'-'aaaa'-'aaaaaaaaaaaa'
        return (
            s4() +
            s4() +
            '-' +
            s4() +
            '-' +
            s4() +
            '-' +
            s4() +
            '-' +
            s4() +
            s4() +
            s4()
        );
    };

    onSubmit() {
        console.log('data action', this.data.data);
        // console.log('UpdateFieldForm::', this.UpdateFieldForm.value);
        let dataSubmit = {
            ...this.data.data,
            ...this.UpdateFieldForm.value,
        };

        // if (this.UpdateFieldForm.valid) {
        //     this.onSave.emit(dataSubmit);
        // } else {
        //     alert('no valid form');
        // }
    }

    onDelete() {
        let dataSubmit = {
            data: this.data.data,
            action: 'DELETE',
        };

        this.onSave.emit(dataSubmit);

        this.dialogRef.close('');
    }
}
