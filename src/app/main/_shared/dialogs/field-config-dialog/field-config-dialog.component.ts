import {
    AfterViewChecked,
    AfterViewInit,
    ChangeDetectorRef,
    Component,
    Inject,
    OnInit,
} from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FieldService } from '@core/services/field.service';
import { ToastrService } from '@core/services/toastr.service';

export interface DialogData {
    name: string;
    fields: any[];
    datas: any;
    groupCode: string;
}

@Component({
    selector: 'app-field-dialog',
    templateUrl: './field-config-dialog.component.html',
    styleUrls: ['./field-config-dialog.component.scss'],
})
export class FieldConfigDialogComponent
    implements OnInit, AfterViewInit, AfterViewChecked {
    form: FormGroup;
    fields: any[];

    isSubmitted: boolean = false;
    isEnableDelete: boolean = false;

    tabs = [
        {
            label: 'Thông tin',
            icon: 'fa fa-pencil-square-o',
            groupCode: 'THONGTIN',
        },
        {
            label: 'Cài đặt',
            icon: 'fa fa-sliders',
            groupCode: 'CAIDAT',
        },
    ];

    constructor(
        private readonly changeDetectorRef: ChangeDetectorRef,
        private _toastrService: ToastrService,
        private _fieldService: FieldService,
        public dialog: MatDialog,
        public dialogRef: MatDialogRef<FieldConfigDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: DialogData,
    ) { }

    ngOnInit() {
        let fieldsCtrls = {};

        for (let f of this.data.fields) {
            if (f.type === 'NUMBER') {
                fieldsCtrls[f.name] = new FormControl(
                    f.value,
                    f.required ? [Validators.required] : [],
                );
            } else if (f.type !== 'CHECKBOX') {
                let validators = [
                    Validators.minLength(f.minLength),
                    Validators.maxLength(f.maxLength),
                    Validators.pattern(f.pattern),
                ];

                if (f.type === 'EMAIL') {
                    validators = [...validators, Validators.email];
                }

                fieldsCtrls[f.name] = new FormControl(
                    f.value || '',
                    f.required
                        ? [...validators, Validators.required]
                        : [...validators],
                );
            } else {
                //if checkbox, it need multiple
                let opts = [];
                for (let index in f.options) {
                    opts[index] = new FormControl(false);
                }
                fieldsCtrls[f.name] = new FormArray(
                    opts,
                    f.required ? [Validators.required] : [],
                );
            }
        }

        this.form = new FormGroup(fieldsCtrls);
    }

    ngAfterViewInit(): void {
        const dataEdit = this.data.datas;
        if (dataEdit) {
            this.form.patchValue(this.data.datas);
        }
    }

    ngAfterViewChecked(): void {
        this.changeDetectorRef.detectChanges();
    }

    getFields() {
        return this.data.fields;
    }

    onCloseDialog(data?: any): void {
        this.dialogRef.close(data);
    }

    onDelete(): void {
        this.isEnableDelete = true;
    }

    onCancelDelete(): void {
        this.isEnableDelete = false;
    }

    handleDelete(): void {
        this.isSubmitted = true;

        const formValues = {
            id: this.data.datas.id,
        };

        this._fieldService
            .deleteField(formValues)
            .subscribe((res) => this.onCloseDialog(this.isSubmitted));
    }

    replaceWhiteSpace(): void {
        for (let f of this.data.fields) {
            if (f.type === 'TEXT' || f.type === 'TEXTAREA') {
                const initField = this.form.get(f.name);

                if (initField.value === null) {
                    initField.setValue('');
                } else {
                    const valueAfterReplace = initField.value
                        .replace(/\s+/g, ' ')
                        .trim();
                    initField.setValue(valueAfterReplace);
                }
            }
        }
    }

    onSubmit(): void {
        this.replaceWhiteSpace();

        this.isSubmitted = true;

        let formValues = this.form.value;

        // Data from table
        const initData = this.data.datas;

        if (initData) {
            formValues = {
                ...initData, // Old data
                ...this.form.value, // New data
            };
        }

        if (this.form.valid) {
            if (initData) {
                this._fieldService.updateField(formValues).subscribe(
                    (res) => {
                        this.onCloseDialog(this.isSubmitted);
                    },
                    (err) => this._toastrService.errorServer(err),
                );
            } else {
                this._fieldService.createField(formValues).subscribe(
                    (res) => {
                        this.onCloseDialog(this.isSubmitted);
                    },
                    (err) => this._toastrService.errorServer(err),
                );
            }
        }
    }
}
