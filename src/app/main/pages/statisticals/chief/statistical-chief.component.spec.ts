import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StatisticalChiefComponent } from './statistical-chief.component';

describe('StatisticalChiefComponent', () => {
    let component: StatisticalChiefComponent;
    let fixture: ComponentFixture<StatisticalChiefComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [StatisticalChiefComponent],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(StatisticalChiefComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
