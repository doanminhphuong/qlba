import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EatObjectsComponent } from './eat-objects.component';

describe('EatObjectsComponent', () => {
  let component: EatObjectsComponent;
  let fixture: ComponentFixture<EatObjectsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EatObjectsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EatObjectsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
