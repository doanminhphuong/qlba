import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material';
import { DialogQuanntitiveRegulationComponent } from '@app/_shared/dialogs/dialog-quanntitive-regulation/dialog-quanntitive-regulation.component';
import { ReportResultCook } from '../report-result-product.component';

@Component({
    selector: 'app-table-report-result',
    templateUrl: './table-report-result.component.html',
    styleUrls: ['./table-report-result.component.scss'],
})
export class TableReportResultComponent implements OnInit {
    @Input() dataTable: ReportResultCook[];
    @Input() tempDataTable: ReportResultCook[];
    @Input() reportResultCookDefaultCheck: number[];
    @Input() isLoadingResults: boolean;
    @Input() listDayinMonth: number;

    @Output() cookDefaultChecked = new EventEmitter<any[]>();

    quantitativeRegulationList;
    quantitativeRegulationTable: any[];
    constructor(public dialog: MatDialog) {}

    roundNumberFloatNumber(number, float) {
        return Math.round(number * float) / float;
    }

    getLengthColspan() {
        return this.dataTable.length - 2;
    }

    quantitativeRegulation(): void {
        const dialogRef = this.dialog.open(
            DialogQuanntitiveRegulationComponent,
            {
                width: '70%',
                data: {
                    reportResultCook: this.dataTable,
                    dataRegulation: this.quantitativeRegulationList,
                },
            },
        );

        dialogRef.componentInstance.onSave.subscribe((formValues) => {
            const isSubmitted = !!formValues;
            this.quantitativeRegulationList = [...formValues];
            let _quantitativeRegulationTable = [...formValues];
            this.quantitativeRegulationTable = _quantitativeRegulationTable.map(
                (item) => Object.values(item)[0] || 0,
            );

            this.cookDefaultChecked.emit(this.quantitativeRegulationTable);
        });

        dialogRef.afterClosed().subscribe((result) => {});
    }

    ngOnInit() {}
}
