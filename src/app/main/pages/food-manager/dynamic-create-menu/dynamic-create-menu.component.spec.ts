import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DynamicCreateMenuComponent } from './dynamic-create-menu.component';

describe('DynamicCreateMenuComponent', () => {
  let component: DynamicCreateMenuComponent;
  let fixture: ComponentFixture<DynamicCreateMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DynamicCreateMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DynamicCreateMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
