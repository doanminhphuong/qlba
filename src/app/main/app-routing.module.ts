import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CanAccessRouterGuard } from '@shared/auth/can-access-router-guard';
import { CanHomepageCallAPIGuard } from '@shared/auth/can-homepage-call-api-guard';
import { CanHomepageGuard } from '@shared/auth/can-homepage-guard';
import { CanLoginpageGuard } from '@shared/auth/can-loginpage-guard';
import { AppComponent } from './app.component';
import { LoginComponent } from './auth/login/login.component';
import { SetTokenComponent } from './auth/set-token/set-token.component';
// import { DynamicRouterComponent } from './components/dynamic-router-component/dynamic-router-component.component';
// import { CanAccessRouterGuard } from '@shared/auth/can-access-router-guard';
// import { FinancialDashboardComponent } from './pages/financial-report/financial-dashboard/financial-dashboard.component';
const routes: Routes = [
    {
        path: '',
        component: AppComponent,
        canActivate: [CanHomepageGuard, CanHomepageCallAPIGuard],
        children: [
            {
                path: 'app',
                loadChildren: () =>
                    import('./pages/pages.module').then((m) => m.PagesModule),
                // data: { preload: true },
                canActivate: [CanAccessRouterGuard],
            },
            {
                path: '',
                redirectTo: 'app',
                pathMatch: 'full',
            },
        ],
        data: {
            title: 'AppComponent ',
        },
    },

    {
        path: 'login',
        component: LoginComponent,
        canActivate: [CanLoginpageGuard],
    },
    {
        path: 'set-token',
        component: SetTokenComponent,
        canActivate: [CanLoginpageGuard],
    },

    // {
    //     path: '**',
    //     redirectTo: 'manage/app/category/trang-chu',
    // },
    // { path: "**", redirectTo: "manager" }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class AppRoutingModule {}
