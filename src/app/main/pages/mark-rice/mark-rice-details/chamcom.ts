const chamCom = {
    "chiTiet": [
        {
            "chiTietDotQuanSoAnId": "cb975d9c-2d2f-4563-85c5-08dabbd9d8af",
            "buoiAn": "[\"s\",\"t\"]",
            "chamComId": 5
        },
        {
            "chiTietDotQuanSoAnId": "15af6472-a65d-457f-85c4-08dabbd9d8af",
            "buoiAn": "[\"s\",\"t\"]",
            "chamComId": 5
        },
        {
            "chiTietDotQuanSoAnId": "1a2f997b-a935-4182-85c3-08dabbd9d8af",
            "buoiAn": "[\"s\",\"t\"]",
            "chamComId": 5
        },
        {
            "chiTietDotQuanSoAnId": "f6bad921-d586-4640-85c2-08dabbd9d8af",
            "buoiAn": "[\"s\",\"t\"]",
            "chamComId": 5
        }
    ],
    "tenChamCom": "11/2022",
    "ngayBatDau": "2022-11-01T00:00:00",
    "ngayKetThuc": "2022-11-30T00:00:00",
    "chamKhongAn": true,
    "value1": null,
    "value2": null,
    "value3": null,
    "value4": null,
    "value5": null,
    "value6": null,
    "value7": null,
    "value8": null,
    "value9": null,
    "value10": null,
    "number1": 0,
    "number2": 0,
    "number3": 0,
    "number4": 0,
    "number5": 0,
    "number6": 0,
    "number7": 0,
    "number8": 0,
    "number9": 0,
    "number10": 0,
    "name": null,
    "code": null,
    "codeData": null,
    "valueData": null,
    "category": null,
    "permissions": null,
    "language": null,
    "tenantId": 1,
    "status": "ENABLE",
    "documentId": null,
    "documentStatus": null,
    "search": null,
    "hideValue": null,
    "creatorUserId": 408,
    "creationTime": "2022-10-27T09:35:31.0077898",
    "notificationTitle": null,
    "notificationGroup": null,
    "notificationData": null,
    "id": 5,
    "ngayAn": "2022-11-02"
};

export default chamCom;
