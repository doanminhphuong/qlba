import {
    animate,
    state,
    style,
    transition,
    trigger,
} from '@angular/animations';
import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatPaginator, MatTableDataSource } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { DeleteConfirmDialogComponent } from '@app/_shared/dialogs/delete-confirm-dialog/delete-confirm-dialog.component';
import { MaterialDialogComponent } from '@app/_shared/dialogs/material-dialog/material-dialog.component';
import { CommonService } from '@core/services/common.service';
import { DataService } from '@core/services/data-service';
import { ToastrService } from '@core/services/toastr.service';
import { Subject, Subscription } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';

@Component({
    selector: 'app-plan-guarantee-add-edit',
    templateUrl: './plan-guarantee-add-edit.component.html',
    styleUrls: ['./plan-guarantee-add-edit.component.scss'],
    animations: [
        trigger('detailExpand', [
            state('collapsed', style({ height: '0px', minHeight: '0' })),
            state('expanded', style({ height: '*' })),
            transition(
                'expanded <=> collapsed',
                animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)'),
            ),
        ]),
    ],
})
export class PlanGuaranteeAddEditComponent implements OnInit {
    form: FormGroup;
    @ViewChildren('paginatorList') paginator = new QueryList<MatPaginator>();
    danhSachMonAn = [
        {
            buoiAn: 'Gạo + than + gia vị',
            danhSach: new MatTableDataSource<any>([]),
            tongThu: 0,
        },
        {
            buoiAn: 'Sáng',
            danhSach: new MatTableDataSource<any>([]),
            tongThu: 0,
        },
        {
            buoiAn: 'Trưa',
            danhSach: new MatTableDataSource<any>([]),
            tongThu: 0,
        },
        {
            buoiAn: 'Chiều',
            danhSach: new MatTableDataSource<any>([]),
            tongThu: 0,
        },
        {
            buoiAn: 'Sáng hôm sau',
            danhSach: new MatTableDataSource<any>([]),
            tongThu: 0,
        },
    ];
    public applyFilter = new Subject<KeyboardEvent>();
    private subscription: Subscription;

    displayedColumnsOfNVL: string[] = [
        'tenLttpChatDot',
        'donViTinh',
        'donGia',
        'hienCo',
        'soLuongTrenBdMua',
        'soLuongTrenBdTg',
        'soLuongDonViBdMua',
        'soLuongDonViBdTg',
        'thanhTien',
        'actions',
    ];

    isEdit: boolean = false;
    isFinishedGetData: boolean = false;
    isLoading: boolean = false;
    fields: any[] = [];
    listProposed: any[] = [];
    listInventory: any[] = [];
    currentOpenedItemId: number;
    dataEdit: any[] = [];
    totalSum: number = 0;
    idPlan: number;
    titleOnEdit: string;

    constructor(
        private _commonService: CommonService,
        private _dataService: DataService,
        private _router: Router,
        private _toastrService: ToastrService,
        public dialog: MatDialog,
        private _activatedRoute: ActivatedRoute,
    ) {
        this.subscription = this.applyFilter
            .pipe(
                map((event) => (event.target as HTMLInputElement).value),
                debounceTime(500),
                distinctUntilChanged(),
            )
            .subscribe((filterValue) => this.handleInputFilter(filterValue));
    }

    ngOnInit() {
        this._activatedRoute.paramMap.subscribe((param) => {
            this.idPlan = +param.get('id');
            if (!this.idPlan) return;
            this.isEdit = true;
        });

        setTimeout(() => this.getDataInit(), 1000);
    }

    getDataInit() {
        this._commonService
            .callDataAPIShort('/api/services/read/Kho/GetAll', {
                maxResultCount: 9999999,
            })
            .subscribe((response) => {
                this.listInventory = response.result.items.map((item) => ({
                    key: item.id,
                    name: item.tenKho,
                }));
                this.getInitForm();
            });
    }

    getInitForm(): void {
        // Check if in current date is greater than 17:00 then validate date + 1
        var date = new Date();

        // add a day
        date.setDate(date.getDate() + 1);

        let validateDate = new Date().getHours() >= 17 ? date : new Date();
        this.fields = [
            {
                type: 'DATETIME',
                referenceValue: 'ngayKeHoach',
                name: 'Chọn ngày kế hoạch đảm bảo',
                defaultValue: '',
                required: '1',
                css: 'col-12 col-lg-3',
                appearance: 'legacy',
                // disabled:
                //     this.allowExec === false || this.isPublished === true
                //         ? true
                //         : false,
                min: validateDate,
            },
            {
                type: 'TEXT',
                referenceValue: 'maKeHoach',
                name: 'Nhập mã kế hoạch đảm bảo',
                defaultValue: '',
                required: '1',
                dependenField: 'ngayKeHoach',
                css: 'col-12 col-lg-3',
                appearance: 'legacy',
                // disabled:
                //     this.allowExec === false || this.isPublished === true
                //         ? true
                //         : false,
            },
            {
                type: 'TEXT',
                referenceValue: 'tenKeHoach',
                name: 'Nhập tên kế hoạch đảm bảo',
                defaultValue: '',
                required: '1',
                css: 'col-12 col-lg-3',
                appearance: 'legacy',
                // disabled:
                //     this.allowExec === false || this.isPublished === true
                //         ? true
                //         : false,
            },
            {
                type: 'SELECT',
                referenceValue: 'duXuatId',
                name: 'Chọn phiếu dự xuất',
                defaultValue: '',
                options: [],
                required: '1',
                disabled: true,
                css: 'col-12 col-lg-3',
                appearance: 'legacy',
                selectionChange: ($event) => this.callFunctionShowData($event),
            },
        ];

        let fieldsCtrls = {};
        let fieldList = [];

        for (let f of this.fields) {
            fieldList = [...fieldList, f];

            if (f.type === 'NUMBER') {
                fieldsCtrls[f.referenceValue] = new FormControl(
                    { value: f.defaultValue, disabled: f.disabled },
                    f.required === '1' ? [Validators.required] : [],
                );
            } else if (f.type !== 'CHECKBOX') {
                let validators = [
                    Validators.minLength(f.minLength),
                    Validators.maxLength(f.maxLength),
                    Validators.pattern(f.pattern),
                ];

                if (f.type === 'EMAIL') {
                    validators = [...validators, Validators.email];
                }

                fieldsCtrls[f.referenceValue] = new FormControl(
                    {
                        value: f.defaultValue || '',
                        disabled: f.disabled,
                    },
                    f.required === '1'
                        ? [...validators, Validators.required]
                        : [...validators],
                );

                if (f.type === 'DATETIME') {
                    if (f.referenceValue === 'ngayKeHoach') {
                        fieldsCtrls[f.referenceValue].valueChanges.subscribe(
                            (value) => {
                                if (this.isEdit === false) {
                                    let date = new Date(value);
                                    const KEY = 'KHĐBTP';
                                    let firstPath = date
                                        .toLocaleDateString()
                                        .replace(/[^0-9]/g, '');
                                    let lastPath = new Date()
                                        .toLocaleTimeString()
                                        .split(/[^0-9]/g)
                                        .map((item, index) => {
                                            if (index < 2)
                                                return ('0' + item).substring(
                                                    0,
                                                    2,
                                                );
                                            return item;
                                        })
                                        .join('');
                                    let fullPath = KEY + firstPath + lastPath;
                                    const dependField = fieldList.find(
                                        (field) =>
                                            field.dependenField ===
                                            f.referenceValue,
                                    );

                                    if (!dependField) return;

                                    fieldsCtrls[
                                        dependField.referenceValue
                                    ].patchValue(fullPath);
                                }

                                fieldsCtrls['duXuatId'].enable();

                                let bodySubmit = {
                                    maxResultCount: 2147483647,
                                    criterias: [
                                        {
                                            propertyName: 'ngayDuXuat',
                                            operation: 0,
                                            value: value,
                                        },
                                        {
                                            propertyName: 'dauMoiBepId',
                                            operation: 0,
                                            value: this._dataService.getDauMoiBepId(),
                                        },
                                    ],
                                };

                                this._commonService
                                    .callDataAPIShort(
                                        '/api/services/read/DuXuat/GetAll',
                                        bodySubmit,
                                    )
                                    .subscribe((response) => {
                                        this.listProposed =
                                            response.result.items.map(
                                                (item) => ({
                                                    key: item.id,
                                                    name: item.tenDuXuat,
                                                }),
                                            );

                                        this.fields[3].options =
                                            this.listProposed;
                                    });
                            },
                        );
                    }
                }
            } else {
                //if checkbox, it need multiple
                let opts = {};
                for (let opt of f.options) {
                    opts[opt.key] = new FormControl({
                        value: opt.value,
                        disabled: f.disabled,
                    });
                }
            }
        }

        this.form = new FormGroup(fieldsCtrls);

        if (this.isEdit) {
            this.getDataEdit();
        }
    }

    renderToText(column) {
        let result = '';
        switch (column) {
            case 'tenLttpChatDot':
                result = 'Tên thực phẩm';
                break;
            case 'donViTinh':
                result = 'ĐVT';
                break;
            case 'donGia':
                result = 'Đơn giá';
                break;
            case 'hienCo':
                result = 'Hiện có';
                break;
            case 'actions':
                result = 'Thao tác';
                break;
        }
        return result;
    }

    callFunctionShowData(event: any) {
        this.isFinishedGetData = false;
        this.isLoading = true;
        this.danhSachMonAn = [];
        this._commonService
            .callDataAPIShort('/api/services/read/DuXuat/Get', {
                id: event.value,
            })
            .subscribe((response) => {
                if (response.success) {
                    this.danhSachMonAn =
                        response.result.chiTietDanhSachLttp.map((data) => ({
                            buoiAn: data.buoi,
                            danhSach: new MatTableDataSource<any>(
                                data.danhSach,
                            ),
                        }));

                    let arrayTongThu = [0, 0, 0, 0, 0];

                    response.result.chiTietDuXuat.forEach((item) => {
                        arrayTongThu[0] += this.sum(
                            item.chiTietGaoTienAn,
                            'tongTienAn',
                        );

                        item.chiTietGaoTienAn.forEach((chiTiet) => {
                            if (chiTiet.buoi === 0) {
                                arrayTongThu[1] += chiTiet.tongTienAn;
                            } else if (chiTiet.buoi === 1) {
                                arrayTongThu[2] += chiTiet.tongTienAn;
                            } else if (chiTiet.buoi === 2) {
                                arrayTongThu[3] += chiTiet.tongTienAn;
                                arrayTongThu[4] += chiTiet.tongTienAn;
                            }
                        });
                    });

                    arrayTongThu[0] = arrayTongThu[0] * 0.03;

                    for (let i = 0; i < arrayTongThu.length; i++) {
                        for (let j = 0; j < this.danhSachMonAn.length; j++) {
                            if (i === j) {
                                this.danhSachMonAn[j].tongThu = arrayTongThu[i];
                            }
                        }
                    }

                    this.calculateData();

                    setTimeout(() => {
                        for (let i = 0; i < this.danhSachMonAn.length; i++) {
                            this.danhSachMonAn[i].danhSach.paginator =
                                this.paginator.toArray()[i];
                        }
                    });
                    setTimeout(() => {
                        this.isFinishedGetData = true;
                        this.isLoading = false;
                    }, 200);
                }
            });
    }

    calculateSelectedAndInput(event, element, column) {
        switch (column) {
            case 'soLuongDonViBdMua':
                element['soLuongDonViBdTg'] = '';
                element['soLuongTrenBdMua'] = '';
                element['soLuongTrenBdTg'] = '';
                element['thanhTien'] =
                    element['soLuongDonViBdMua'] * element['donGia'];
                break;
            case 'soLuongDonViBdTg':
                element['soLuongDonViBdMua'] = '';
                element['soLuongTrenBdMua'] = '';
                element['soLuongTrenBdTg'] = '';
                element['thanhTien'] =
                    element['soLuongDonViBdTg'] * element['donGia'];
                break;
            case 'soLuongTrenBdMua':
                element['soLuongDonViBdTg'] = '';
                element['soLuongDonViBdMua'] = '';
                element['soLuongTrenBdTg'] = '';
                element['thanhTien'] =
                    element['soLuongTrenBdMua'] * element['donGia'];
                break;
            case 'soLuongTrenBdTg':
                element['soLuongTrenBdMua'] = '';
                element['soLuongDonViBdTg'] = '';
                element['soLuongDonViBdMua'] = '';
                element['thanhTien'] =
                    element['soLuongTrenBdTg'] * element['donGia'];
                break;
        }
        this.totalSum = 0;
        this.calculateData();
    }

    calculateData() {
        this.danhSachMonAn.forEach((detail) => {
            this.totalSum += this.sum(detail.danhSach.data, 'thanhTien');
        });
    }

    sum(array, key) {
        const initialValue = 0;
        const sumWithInitial = array.reduce(
            (accumulator, currentValue) => accumulator + currentValue[key],
            initialValue,
        );

        return sumWithInitial;
    }

    convertNameById(id: any, type: any) {
        let data;
        let dataList = [];

        if (this.listInventory.length > 0) {
            switch (type) {
                case 'Inventory':
                    dataList = this.listInventory;
                    break;
            }
            let item = dataList.find((item) => item.key === id);
            data = item && item.name ? item.name : '';
        }

        return data;
    }

    addRow(indexTab: any): void {
        const dialogRef = this.dialog.open(MaterialDialogComponent, {
            // disableClose: true,

            width: '70%',
            data: {
                type: 'ADJUSTMENT_PURCHASE_UPGRADE',
                isShow: false,
                currentTable: [],
            },
            panelClass: 'add-field-dialog-class',
        });

        dialogRef.componentInstance.onSave.subscribe((data) => {
            const isSubmitted = !!data;

            let dataTable = data.map((item) => ({
                lttpChatDotId: item.lttpChatDotId,
                tenLttpChatDot: this.convertNameById(
                    item.lttpChatDotId,
                    'FoodFuel',
                ),
                donViTinh: item.donViTinh,
                khoId: '',
                donGia: item.donGia,
                hienCo: 0,
                thanhTien: item.soLuong * item.donGia,
                soLuongTrenBdMua: item.nguonCungCap === 1 ? item.soLuong : '',
                soLuongTrenBdTg: item.nguonCungCap === 2 ? item.soLuong : '',
                soLuongDonViBdMua: item.nguonCungCap === 3 ? item.soLuong : '',
                soLuongDonViBdTg: item.nguonCungCap === 4 ? item.soLuong : '',
                typePriceImport: 0,
                type: 'other',
            }));

            this.danhSachMonAn[indexTab].danhSach.data = [
                ...dataTable,
                ...this.danhSachMonAn[indexTab].danhSach.data,
            ];

            this.danhSachMonAn[indexTab].danhSach.paginator =
                this.paginator.toArray()[indexTab];

            dialogRef.close(isSubmitted);
        });

        dialogRef.afterClosed().subscribe((result) => {
            if (!result) return;
            this._toastrService.success('', 'Thêm thành công');
        });
    }

    // Delete row
    deleteAction(element: any, indexTab: any, indexElement: any): void {
        const dialogRef = this.dialog.open(DeleteConfirmDialogComponent, {
            width: '500px',
            data: element,
        });

        dialogRef.componentInstance.handleDelete.subscribe((dataDelete) => {
            const isSubmitted = !!dataDelete;
            dialogRef.close(isSubmitted);
        });

        dialogRef.afterClosed().subscribe((result) => {
            if (!result) return;

            const index: number =
                this.danhSachMonAn[indexTab].danhSach.data.indexOf(element);

            if (index !== -1) {
                this.danhSachMonAn[indexTab].danhSach.data.splice(index, 1);
            }

            this.danhSachMonAn[indexTab].danhSach.paginator =
                this.paginator.toArray()[indexTab];

            this._toastrService.success('', 'Xóa thành công');
        });
    }

    getDataEdit() {
        this._commonService
            .callDataAPIShort('/api/services/read/KeHoachLttp/Get', {
                id: this.idPlan,
            })
            .subscribe((response) => {
                this.titleOnEdit = `Chỉnh sửa kế hoạch đảm bảo ${response.result.maKeHoach}`;
                this.dataEdit = response.result;
                this.totalSum = response.result.tongTien;

                response.result.chiTiet.forEach((data) => {
                    this.danhSachMonAn.map((item) => {
                        if (item.buoiAn === data.buoiAn) {
                            item.danhSach.data.push(data);
                        }
                    });
                });

                this.form.patchValue({
                    ...response.result,
                    duXuatId: response.result.duXuatId,
                });

                setTimeout(() => {
                    for (let i = 0; i < this.danhSachMonAn.length; i++) {
                        this.danhSachMonAn[i].danhSach.paginator =
                            this.paginator.toArray()[i];
                    }
                });

                this.isFinishedGetData = true;
            });
    }

    handleOpened(index): void {
        this.currentOpenedItemId = index;
    }

    // Back to list
    onBack(): void {
        this._router.navigate(['/app/quan-li-chi-an/ke-hoach-LTTP-chat-dot']);
    }

    onSubmit(isPublished: boolean): void {
        let dataDetail = [];
        this.danhSachMonAn.forEach((data) => {
            data.danhSach.data.forEach((item) => {
                dataDetail.push({
                    buoiAn: data.buoiAn,
                    ...item,
                    soLuongDonViBdMua: item.soLuongDonViBdMua
                        ? item.soLuongDonViBdMua
                        : 0,
                    soLuongDonViBdTg: item.soLuongDonViBdTg
                        ? item.soLuongDonViBdTg
                        : 0,
                    soLuongTrenBdMua: item.soLuongTrenBdMua
                        ? item.soLuongTrenBdMua
                        : 0,
                    soLuongTrenBdTg: item.soLuongTrenBdTg
                        ? item.soLuongTrenBdTg
                        : 0,
                });
            });
        });

        let bodySubmit = {
            ...this.form.value,
            ...this.dataEdit,
            tongTien: this.totalSum,
            dauMoiBepId: this._dataService.getDauMoiBepId(),
            chiTiet: dataDetail,
            trangThai: isPublished,
        };

        if (this.isEdit) {
            this._commonService
                .callDataAPIShort(
                    '/api/services/write/KeHoachLttp/Update',
                    bodySubmit,
                )
                .subscribe(
                    (response) => {
                        if (response.success) {
                            this._router.navigate([
                                'app/quan-li-chi-an/ke-hoach-LTTP-chat-dot',
                            ]);
                            this._toastrService.success(
                                '',
                                'Cập nhật thành công',
                            );
                        }
                    },
                    (err) => this._toastrService.errorServer(err),
                );
        } else {
            this._commonService
                .callDataAPIShort(
                    '/api/services/write/KeHoachLttp/Create',
                    bodySubmit,
                )
                .subscribe(
                    (response) => {
                        if (response.success) {
                            this._router.navigate([
                                'app/quan-li-chi-an/ke-hoach-LTTP-chat-dot',
                            ]);
                            this._toastrService.success(
                                '',
                                'Tạo kế hoạch thành công',
                            );
                        }
                    },
                    (err) => this._toastrService.errorServer(err),
                );
        }
    }

    handleInputFilter(filterValue) {
        this.danhSachMonAn[this.currentOpenedItemId].danhSach.filter =
            filterValue.toLowerCase();
    }
}
