import { Component, ElementRef, Input, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Observable, Subject, Subscription } from 'rxjs';
import {
    debounceTime,
    distinctUntilChanged,
    map,
    startWith,
} from 'rxjs/operators';

@Component({
    selector: 'fb-multiselect',
    template: `
        <div [formGroup]="form">
            <ng-container
                *ngIf="!isFlatForm && tabCtrl"
                [formGroupName]="tabCtrl"
            >
                <ng-container [formGroupName]="groupCtrl">
                    <mat-form-field [appearance]="field.appearance">
                        <mat-label>{{ field.name }}</mat-label>
                        <mat-select
                            multiple
                            [id]="field.referenceValue"
                            [formControlName]="getFormControlName()"
                            [required]="field.required === '1' ? true : false"
                        >
                            <mat-option
                                *ngFor="let opt of field.options"
                                [value]="opt.key"
                            >
                                {{ opt.name }}
                            </mat-option>
                        </mat-select>
                        <!-- VALIDATORS -->
                        <mat-error>{{ getErrorMessage() }}</mat-error>
                    </mat-form-field>
                </ng-container>
            </ng-container>

            <!-- <mat-form-field
                *ngIf="isFlatForm || !tabCtrl"
                [appearance]="field.appearance || 'outline'"
            >
                <mat-label>{{ field.name }}</mat-label>
                <mat-select
                    multiple
                    [id]="field.referenceValue"
                    [formControlName]="getFormControlName()"
                    [required]="field.required === '1' ? true : false"
                    (selectionChange)="handleSelectionChange($event)"
                >
                    <mat-option
                        *ngFor="let opt of field.options"
                        [value]="opt.key"
                    >
                        {{ opt.name }}
                    </mat-option>
                </mat-select>
                <mat-error>{{ getErrorMessage() }}</mat-error>
            </mat-form-field> -->

            <mat-form-field
                *ngIf="isFlatForm || !tabCtrl"
                [appearance]="field.appearance || 'outline'"
            >
                <mat-label>{{ field.name }}</mat-label>
                <mat-select
                    multiple
                    [id]="field.referenceValue"
                    [formControlName]="getFormControlName()"
                    [required]="field.required === '1' ? true : false"
                    (openedChange)="openedChange($event)"
                    (selectionChange)="handleSelectionChange($event)"
                >
                    <div class="select-container">
                        <mat-optgroup>
                            <mat-form-field style="width:100%;">
                                <input
                                    #search
                                    matInput
                                    placeholder="Tìm kiếm"
                                    autocomplete="off"
                                    aria-label="Search"
                                    (keydown)="handleKeyDown($event)"
                                    [formControl]="searchTextboxControl"
                                />

                                <button
                                    *ngIf="!search.value"
                                    mat-icon-button
                                    matSuffix
                                >
                                    <mat-icon>search</mat-icon>
                                </button>

                                <button
                                    [disableRipple]="true"
                                    *ngIf="search.value"
                                    matSuffix
                                    mat-icon-button
                                    aria-label="Clear"
                                    (click)="clearSearch($event)"
                                >
                                    <mat-icon>close</mat-icon>
                                </button>
                            </mat-form-field>
                        </mat-optgroup>

                        <mat-optgroup
                            *ngIf="(filteredOptions | async).length == 0"
                        >
                            <div>No results found!</div>
                        </mat-optgroup>

                        <mat-option
                            *ngFor="let option of filteredOptions | async"
                            [value]="option.key"
                        >
                            {{ option.name }}
                        </mat-option>
                    </div>
                </mat-select>
                <!-- VALIDATORS -->
                <mat-error>{{ getErrorMessage() }}</mat-error>
            </mat-form-field>
        </div>
    `,
    styles: [
        `
            mat-form-field {
                width: 100%;
            }

            .select-container {
                margin-top: 10px;
            }
        `,
    ],
})
export class MultiselectComponent {
    @Input() field: any = {};
    @Input() form: FormGroup;
    @Input() tabCtrl: any;
    @Input() groupCtrl: any;
    @Input() isFlatForm: boolean;

    @ViewChild('search') searchTextBox: ElementRef;

    // selectFormControl = new FormControl();
    searchTextboxControl = new FormControl();
    selectedValues = [];
    data: string[] = ['A1', 'A2', 'A3', 'B1', 'B2', 'B3', 'C1', 'C2', 'C3'];

    filteredOptions: Observable<any[]>;

    ngOnInit() {}

    constructor() {
        this.filteredOptions = this.searchTextboxControl.valueChanges.pipe(
            debounceTime(50),
            distinctUntilChanged(),
            startWith<string>(''),
            map((name) => this._filter(name)),
        );
    }

    get fieldName() {
        if (!this.isFlatForm && this.tabCtrl) {
            return this.form
                .get(this.tabCtrl)
                .get(this.groupCtrl)
                .get(this.field.referenceValue);
        } else if (this.isFlatForm && this.tabCtrl) {
            return this.form.controls[
                `${this.tabCtrl}.${this.groupCtrl}.${this.field.referenceValue}`
            ];
        } else {
            return this.form.controls[this.field.referenceValue];
        }
    }

    getFormControlName() {
        if (this.isFlatForm) {
            return `${this.tabCtrl}.${this.groupCtrl}.${this.field.referenceValue}`;
        } else {
            return this.field.referenceValue;
        }
    }

    getErrorMessage() {
        let errorMessage = '';

        if (this.fieldName.hasError('required')) {
            errorMessage = `${this.field.name} không được để trống.`;
        }

        return errorMessage;
    }

    handleKeyDown(event: KeyboardEvent) {
        if (event.which === 32) {
            event.stopPropagation();
        }
    }

    /**
     * Used to filter data based on search input
     */
    private _filter(name: string): String[] {
        const filterValue = name.toLowerCase();
        // Set selected values to retain the selected checkbox state
        this.setSelectedValues();
        this.fieldName.patchValue(this.selectedValues);
        let filteredList = this.field.options.filter((option) =>
            option['name'].toLowerCase().includes(filterValue),
        );
        return filteredList;
    }

    /**
     * Remove from selected values based on uncheck
     */
    selectionChange(event) {
        if (event.isUserInput && event.source.selected === false) {
            let index = this.selectedValues.indexOf(event.source.value);
            this.selectedValues.splice(index, 1);
        }
    }

    openedChange(e) {
        // Set search textbox value as empty while opening selectbox
        this.searchTextboxControl.patchValue('');
        // Focus to search textbox while clicking on selectbox
        if (e === true) {
            this.searchTextBox.nativeElement.focus();
        }
    }

    /**
     * Clearing search textbox value
     */
    clearSearch(event) {
        event.stopPropagation();
        this.searchTextboxControl.patchValue('');
    }

    /**
     * Set selected values to retain the state
     */
    setSelectedValues() {
        if (this.fieldName.value && this.fieldName.value.length > 0) {
            this.fieldName.value.forEach((e) => {
                if (this.selectedValues.indexOf(e) === -1) {
                    this.selectedValues.push(e);
                }
            });
        }
    }

    handleSelectionChange(event: any) {
        if (!this.field.selectionChange) return;

        this.field.selectionChange(event);
    }

    // Use to show data as a object in mat-select
    compareObjects(o1: any, o2: any): boolean {
        return o1.key === o2.key;
    }
}
