import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanSpendAddEditComponent } from './plan-spend-add-edit.component';

describe('PlanSpendAddEditComponent', () => {
  let component: PlanSpendAddEditComponent;
  let fixture: ComponentFixture<PlanSpendAddEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlanSpendAddEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanSpendAddEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
