import { query } from '@angular/animations';
import { SelectionModel } from '@angular/cdk/collections';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog, MatPaginator, MatTableDataSource } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { Food } from '@app/_shared/dialogs/dialog-cook-manager/dialog-cook-manager.component';
import { CategoryService } from '@core/services/category.service';
import { FoodFuelService } from '@core/services/food-fuel.service';
import { InventoryService } from '@core/services/inventory.service';
import { DialogInventoryDetailComponent } from '../inventory-management/dialog-inventory-detail/dialog-inventory-detail.component';

export const DATA_EXAMPLE = [
    {
        name: 'Gạo tẻ',
        nameMaterial: 'K001',
        unit: 'KG',
        amount: '10',
        amountIn: '100',
        amountOut: '90',
    },
    {
        name: 'Gạo tẻ',
        nameMaterial: 'THỰC PHẨM',
        unit: 'KG',
        amount: '10',
        amountIn: '100',
        amountOut: '90',
    },
    {
        name: 'Gạo tẻ',
        nameMaterial: 'THỰC PHẨM',
        unit: 'KG',
        amount: '10',
        amountIn: '100',
        amountOut: '90',
    },
    {
        name: 'Gạo tẻ',
        nameMaterial: 'THỰC PHẨM',
        unit: 'KG',
        amount: '10',
        amountIn: '100',
        amountOut: '90',
    },
    {
        name: 'Gạo tẻ',
        nameMaterial: 'THỰC PHẨM',
        unit: 'KG',
        amount: '10',
        amountIn: '100',
        amountOut: '90',
    },
];

@Component({
    selector: 'app-warehouse-report',
    templateUrl: './warehouse-report.component.html',
    styleUrls: ['./warehouse-report.component.scss'],
})
export class WarehouseReportComponent implements OnInit {
    displayedColumnsOfField: string[] = [
        'store',
        'name',
        'nameMaterial',
        'unit',
        'price',
        'amountImport',
        'priceImport',
        'priceExport',
        'amount',
        'actions',
    ];

    fieldSearch: any[] = [
        {
            type: 'TEXT',
            referenceValue: 'tenLttpChatDot',
            name: 'Tìm kiếm theo tên vật tư, hàng hoá',
            defaultValue: '',
            required: '0',
            icon: 'search',
            css: 'col-12 col-lg-6',
            appearance: 'legacy',
        },
        {
            type: 'SELECT',
            referenceValue: 'nhomLttpChatDotId',
            name: 'Nhóm nguyên vật liệu',
            defaultValue: '',
            options: [
                {
                    key: '',
                    name: '',
                },
            ],
            required: '0',
            css: 'col-12 col-lg-6',
            appearance: 'legacy',
        },
    ];
    rfSearch: FormGroup;

    nameWarehouse;
    idInventory;

    foodFuelList;
    typeOptions: any[] = [];
    foods: Food[] = [
        { value: 'steak-0', viewValue: 'Steak' },
        { value: 'pizza-1', viewValue: 'Pizza' },
        { value: 'tacos-2', viewValue: 'Tacos' },
    ];

    dataSource = new MatTableDataSource<any>([]);
    @ViewChild(MatPaginator) paginator!: MatPaginator;
    constructor(
        public dialog: MatDialog,
        private _activatedroute: ActivatedRoute,
        private _inventoryService: InventoryService,
        private _foodFuelService: FoodFuelService,
        private _categoryService: CategoryService,
        private _router: Router,
        private fb: FormBuilder,
    ) {}

    ngOnInit() {
        this._activatedroute.queryParams.subscribe((params) => {
            this.nameWarehouse = params.name;
            this.idInventory = params.id;
            this.getTypeOptions();
        });
        this.getInitForm();
        this.formOnChanges();

        this.getDataOnInit();
    }

    getDataOnInit() {
        this._foodFuelService
            .getAllFoodFuel({ maxResultCount: 9999999 })
            .subscribe((res) => {
                this.foodFuelList = res.result.items;
                let request = {
                    maxResultCount: 9999999,
                };

                this._inventoryService
                    .getAllInventory(request)
                    .subscribe((response) => {
                        this.dataSource = new MatTableDataSource<any>(
                            response.result.items,
                        );
                        this.dataSource.paginator = this.paginator;

                        this.fieldSearch = [
                            {
                                type: 'TEXT',
                                referenceValue: 'tenLttpChatDot',
                                name: 'Tìm kiếm theo tên vật tư, hàng hoá',
                                defaultValue: '',
                                required: '0',
                                icon: 'search',
                                css: 'col-12 col-lg-6',
                                appearance: 'legacy',
                            },
                            {
                                type: 'SELECT',
                                referenceValue: 'nhomLttpChatDotId',
                                name: 'Nhóm nguyên vật liệu',
                                defaultValue: this.typeOptions[0],
                                options: [...this.typeOptions],
                                required: '0',
                                css: 'col-12 col-lg-6',
                                appearance: 'legacy',
                            },
                        ];
                    });
            });
    }

    getTypeOptions() {
        let request = {
            maxResultCount: 999999,
            skipCount: 0,
            sorting: 'Code',
            criterias: [
                {
                    propertyName: 'GroupCode',
                    operation: 6,
                    value: 'phan-loai-lttp-chat-dot',
                },
            ],
        };
        this._categoryService.getAllCategory(request).subscribe((res) => {
            this.typeOptions = res.result.map((item) => ({
                key: item.codeData,
                name: item.name,
            }));
        });
    }

    getDataFromId(id, objKey) {
        let dataInit = '';

        if (objKey === 'phanLoai') {
            if (this.foodFuelList.length > 0) {
                const dataFound = this.foodFuelList.find(
                    (item) => item.id === id,
                );

                let codeData = dataFound[objKey];

                const dataFound2 = this.typeOptions.find(
                    (item) => item.key === codeData,
                );
                dataInit = dataFound2.name;
            }

            return dataInit;
        } else {
            if (this.foodFuelList.length > 0) {
                const dataFound = this.foodFuelList.find(
                    (item) => item.id === id,
                );

                dataInit = dataFound[objKey];
            }

            return dataInit;
        }
    }

    getInitForm() {
        this.rfSearch = this.fb.group({
            unitCook: '',
            unitStore: '',
        });
    }

    formOnChanges(): void {
        this.rfSearch.valueChanges.subscribe((val) => {
            console.log('val::', val);
        });
    }

    showDetail(element) {
        let name = this.getDataFromId(element.lttpChatDotId, 'tenLttpChatDot');
        let dialogRef = this.dialog.open(DialogInventoryDetailComponent, {
            width: '80%',
            data: {
                name: name,
                id: element.id,
                khoId: element.khoId,
                donGia: element.donGia,
                donViTinh: this.getDataFromId(
                    element.lttpChatDotId,
                    'donViTinh',
                ),
            },
            panelClass: 'my-custom-dialog-class',
        });
    }

    handleSearch(dataSearch: any) {
        if (dataSearch.key === 'reset') {
            this.getDataOnInit();
        } else {
            let bodySearch = {
                criterias: [
                    {
                        propertyName: 'khoId',
                        operation: 0,
                        value: this.idInventory,
                    },
                    {
                        propertyName: 'soluongTonKho',
                        operation: 2,
                        value: 0,
                    },
                ],
                tenVatTu: dataSearch.tenLttpChatDot
                    ? dataSearch.tenLttpChatDot
                    : '',
                maPhanLoai: dataSearch.nhomLttpChatDotId
                    ? dataSearch.nhomLttpChatDotId
                    : '',
            };
            this._inventoryService
                .getAllInventory(bodySearch)
                .subscribe((response) => {
                    this.dataSource = new MatTableDataSource<any>(
                        response.result.items,
                    );
                    this.dataSource.paginator = this.paginator;
                });
        }
    }

    onSubmit() {
        console.log('Submit');
    }
}
