import { SelectionModel } from '@angular/cdk/collections';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MatDialog, MatPaginator, MatTableDataSource } from '@angular/material';
import { QldkDialogComponent } from '@app/_shared/dialogs/qldk-dialog/qldk-dialog.component';
import { SearchComponent } from '@app/_shared/common/search/search.component';
import { QLDKService } from '@core/services/qldk.service';
import { ToastrService } from '@core/services/toastr.service';
import { DynamicDialogComponent } from '../dynamic-dialog-component/dynamic-dialog-component.component';
export interface PeriodicElement {
    index: string;
    codeData: string;
    name: string;
    des: string;
    fromDate: string;
    endDate: string;
    status: string;
    object: string;
    address: string;
}

const ELEMENT_DATA: any[] = [
    {
        index: '1',
        codeData: 'CT1',
        name: 'QLĐSK 01',
        des: '',
        fromDate: '2022-04-01T17:00:00.000Z',
        endDate: '2022-04-29T17:00:00.000Z',
        status: 'Hoàn thành',
        object: '',
        address: '',
    },
    {
        index: '2',
        codeData: 'CT2',
        name: 'QLĐSK 02',
        des: '',
        fromDate: '2022-07-06T17:00:00.000Z',
        endDate: '2022-07-27T17:00:00.000Z',
        status: 'Triển khai',
        object: '',
        address: '',
    },
    {
        index: '10',
        codeData: 'CT10',
        name: 'QLĐSK 10',
        des: '',
        fromDate: '2023-01-06T17:00:00.000Z',
        endDate: '2023-01-29T17:00:00.000Z',
        status: 'Chuẩn bị',
        object: '',
        address: '',
    },
    {
        index: '11',
        codeData: 'CT11',
        name: 'QLĐSK 11',
        des: '',
        fromDate: '2023-01-06T17:00:00.000Z',
        endDate: '2023-01-29T17:00:00.000Z',
        status: 'Chuẩn bị',
        object: '',
        address: '',
    },
];

@Component({
    selector: 'app-qldk',
    templateUrl: './qldk.component.html',
    styleUrls: ['./qldk.component.scss'],
})
export class QldkComponent implements OnInit {
    @ViewChild(MatPaginator) paginator!: MatPaginator;
    displayedColumns: string[] = [
        'index',
        'codeData',
        'name',
        'des',
        'fromDate',
        'endDate',
        'status',
        'actions',
    ];

    typeDialog: string;
    fieldSearch: any[] = [
        {
            type: 'TEXT',
            referenceValue: 'codeData',
            name: 'Mã món',
            defaultValue: '',
            required: '0',
            icon: 'format_list_numbered',
            css: 'col-12 col-lg-3',
        },
        {
            type: 'TEXT',
            referenceValue: 'name',
            name: 'Tên món',
            defaultValue: '',
            required: '0',
            icon: 'code',
            css: 'col-12 col-lg-3',
        },
        {
            type: 'DATETIME',
            referenceValue: 'startDate',
            name: 'Áp dụng từ',
            defaultValue: null,
            required: '0',
            icon: 'filter_vintage',
            css: 'col-12 col-lg-3',
        },
        {
            type: 'DATETIME',
            referenceValue: 'endDate',
            name: 'Đến ngày',
            defaultValue: null,
            required: '0',
            icon: 'filter_vintage',
            css: 'col-12 col-lg-3',
        },
        {
            type: 'SELECT',
            referenceValue: 'status',
            name: 'Trạng thái',
            defaultValue: '',
            options: [
                {
                    key: 'Hoàn thành',
                    name: 'Hoàn thành',
                },
                {
                    key: 'Chuẩn bị',
                    name: 'Chuẩn bị',
                },
                {
                    key: 'Triển khai',
                    name: 'Triển khai',
                },
            ],
            required: '0',
            icon: '',
            css: 'col-12 col-lg-4',
        },
        {
            type: 'TEXT',
            referenceValue: 'value4',
            name: 'Đối tượng',
            defaultValue: '',
            required: '0',
            icon: 'person',
            css: 'col-12 col-lg-4',
        },
        {
            type: 'TEXT',
            referenceValue: 'value5',
            name: 'Nhà ăn',
            defaultValue: '',
            required: '0',
            icon: 'place',
            css: 'col-12 col-lg-4',
        },
    ];
    result;
    form: FormGroup;
    selection = new SelectionModel<any>(true, []);
    // dataSource = ELEMENT_DATA;
    dataSource = new MatTableDataSource<any>([]);

    constructor(
        public dialog: MatDialog,
        private qldkService: QLDKService,
        private _toastrService: ToastrService,
    ) {}

    ngOnInit() {
        this.selection.clear();
        this.getInitData();
    }
    getInitData() {
        let data = {
            maxResultCount: 10,
            skipCount: 0,
            sorting: 'Code',
            criterias: [
                {
                    propertyName: 'Code',
                    operation: 6,
                    value: '',
                },
            ],
        };

        this.qldkService.getAllQLDK(data).subscribe((data) => {
            console.log(data.result.items);
            this.dataSource = new MatTableDataSource<any>(data.result.items);
            this.dataSource.paginator = this.paginator;
        });
    }
    /** Whether the number of selected elements matches the total number of rows. */
    // isAllSelected() {
    //     const numSelected = this.selection.selected.length;
    //     const numRows = this.dataSource.length;
    //     return numSelected === numRows;
    // }

    // /** Selects all rows if they are not all selected; otherwise clear selection. */
    // toggleAllRows() {
    //     if (this.isAllSelected()) {
    //         this.selection.clear();
    //         return;
    //     }

    //     this.selection.select(...this.dataSource);
    // }

    // /** The label for the checkbox on the passed row */
    // checkboxLabel(row?: any): string {
    //     if (!row) {
    //         return `${this.isAllSelected() ? 'deselect' : 'select'} all`;
    //     }
    //     return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${
    //         row.index + 1
    //     }`;
    // }

    // applyFilter(event: Event) {
    //     const filterValue = (event.target as HTMLInputElement).value;
    //     this.dataSource.filter = filterValue.trim().toLowerCase();
    // }

    openDialog(element: any): void {
        this.typeDialog = 'qldk';

        const fields = [
            {
                type: 'TEXT',
                referenceValue: 'codeData',
                name: 'Mã món',
                defaultValue: '',
                required: '1',
                icon: 'format_list_numbered',
                css: 'col-12 col-lg-6',
            },
            {
                type: 'TEXT',
                referenceValue: 'name',
                name: 'Tên món',
                defaultValue: null,
                required: '1',
                icon: 'code',
                css: 'col-12 col-lg-6',
            },
            {
                type: 'TEXTAREA',
                referenceValue: 'value1',
                name: 'Mô tả',
                defaultValue: '',
                required: '0',
                icon: 'title',
                css: 'col-12 col-lg-12',
            },
            {
                type: 'DATETIME',
                referenceValue: 'startDate',
                name: 'Áp dụng từ',
                defaultValue: '',
                required: '0',
                icon: 'filter_vintage',
                css: 'col-12 col-lg-4',
            },
            {
                type: 'DATETIME',
                referenceValue: 'endDate',
                name: 'Đến ngày',
                defaultValue: '',
                required: '0',
                icon: 'filter_vintage',
                css: 'col-12 col-lg-4',
            },
            {
                type: 'SELECT',
                referenceValue: 'status',
                name: 'Trạng thái',
                defaultValue: '',
                options: [
                    {
                        key: 'Hoàn thành',
                        name: 'Hoàn thành',
                    },
                    {
                        key: 'Chuẩn bị',
                        name: 'Chuẩn bị',
                    },
                    {
                        key: 'Triển khai',
                        name: 'Triển khai',
                    },
                ],
                required: '0',
                icon: '',
                css: 'col-12 col-lg-4',
            },
            {
                type: 'TEXT',
                referenceValue: 'value4',
                name: 'Đối tượng',
                defaultValue: '',
                required: '1',
                icon: 'person',
                css: 'col-12 col-lg-6',
            },
            {
                type: 'TEXT',
                referenceValue: 'value5',
                name: 'Nhà ăn',
                defaultValue: '',
                required: '0',
                icon: 'place',
                css: 'col-12 col-lg-6',
            },
        ];

        const dialogRef = this.dialog.open(DynamicDialogComponent, {
            // disableClose: true,
            width: '70%',
            data: {
                typeDialog: this.typeDialog,
                fields: fields,
                datas: element,
            },
            panelClass: 'my-custom-dialog-class',
        });

        dialogRef.componentInstance.onSave.subscribe((formValues) => {
            const isSubmitted = !!formValues;

            let dataSubmit = {
                ...element,
                ...formValues,
            };
            console.log('Form Submit QLDK: ', dataSubmit);

            if (!element) {
                this.qldkService.createQLDK(dataSubmit).subscribe(
                    (res) => dialogRef.close(isSubmitted),
                    (err) => this._toastrService.errorServer(err),
                );
            } else {
                this.qldkService.updateQLDK(dataSubmit).subscribe(
                    (res) => dialogRef.close(isSubmitted),
                    (err) => this._toastrService.errorServer(err),
                );
            }
        });

        dialogRef.componentInstance.handleDelete.subscribe((dataDelete) => {
            const isDeleted = !!dataDelete;

            this.qldkService.deleteQLDK(dataDelete).subscribe(
                (res) => dialogRef.close(isDeleted),
                (err) => this._toastrService.errorServer(err),
            );
        });

        dialogRef.afterClosed().subscribe((result) => {
            if (!result) return;

            this.getInitData();

            if (!element) {
                this._toastrService.success('', 'Thêm thành công');
            } else {
                this._toastrService.success('', 'Cập nhật thành công');
            }
        });
    }

    createAction(): void {
        this.openDialog(null);
    }

    editAction(element: any): void {
        let data = {
            id: element.id,
        };
        this.qldkService.getItemQLDK(data).subscribe((data) => {
            this.openDialog(data.result);
        });
    }

    handleSearch(data: any): void {
        console.log('Form Submit On Parent: ', data);
    }
}
