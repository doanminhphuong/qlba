import { ThrowStmt } from '@angular/compiler';
import {
    AfterViewChecked,
    AfterViewInit,
    ChangeDetectorRef,
    Component,
    EventEmitter,
    Inject,
    OnChanges,
    OnInit,
    SimpleChanges,
} from '@angular/core';
import {
    FormArray,
    FormBuilder,
    FormControl,
    FormGroup,
    Validators,
} from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Cook } from '@app/pages/cook-manager/cook-manager.component';
import { CommonService } from '@core/services/common.service';
import { DataService } from '@core/services/data-service';
import { ToastrService } from '@core/services/toastr.service';

export interface CookType extends Cook {
    manager: string;
    stocker: string;
    unitHead: string;
    assistant: string;
}

export interface Food {
    value: string | number;
    viewValue: string;
}

interface dataInputDialog {
    title: string;
    dataDialogValue: any;
    type: string;
    currentUnitUser: any;
    listChildOrganization: any;
    listUserOnUnit: any[];
}

@Component({
    selector: 'app-dialog-cook-manager',
    templateUrl: './dialog-cook-manager.component.html',
    styleUrls: ['./dialog-cook-manager.component.scss'],
})
export class DialogCookManagerComponent
    implements OnInit, AfterViewInit, AfterViewChecked
{
    rfDialog: FormGroup;
    onSave = new EventEmitter();
    listFormField;
    listOthersField;
    listUsers: any[] = [];
    formOthers: FormGroup;

    constructor(
        private readonly changeDetectorRef: ChangeDetectorRef,
        public dialogRef: MatDialogRef<DialogCookManagerComponent>,
        private fb: FormBuilder,
        @Inject(MAT_DIALOG_DATA) public data: dataInputDialog,
        private commonService: CommonService,
        private _toastrService: ToastrService,
        private _dataService: DataService,
    ) {}

    ngOnInit() {
        // this.getInitForm();
        this.getItemCurrentUnit();
        this.initialListFormField();
        console.log('data bếp', this.data.dataDialogValue);
    }

    getItemCurrentUnit() {
        let result = this.data.listChildOrganization.map((organization) => ({
            key: organization.id,
            name: organization.name,
        }));
        return result;
    }

    initialListFormField() {
        this.listFormField = [
            {
                type: 'TEXT',
                referenceValue: 'tenBep',
                name: 'Tên bếp',
                defaultValue: '',
                required: '1',
                css: 'col-12 col-lg-6',
                appearance: 'legacy',
            },
            {
                type: 'SELECT',
                referenceValue: 'donViQuanLyId',
                name: 'Đơn vị quản lý',
                defaultValue: '',
                options: [...this.getItemCurrentUnit()],
                required: '1',
                disabled: true,
                css: 'col-12 col-lg-6',
                appearance: 'legacy',
                search: '1',
                searchCtrl: 'searchCtrl',
            },
            {
                type: 'SELECT',
                referenceValue: 'quanLyBepId',
                name: 'Quản lý',
                defaultValue: '',
                options: [...this.listUsers],
                required: '1',
                disabled: true,
                css: 'col-12 col-lg-6',
                appearance: 'legacy',
                // search: '1',
                // searchCtrl: 'searchCtrl',
            },
            {
                type: 'SELECT',
                referenceValue: 'thuTruongId',
                name: 'Thủ trưởng đơn vị',
                defaultValue: '',
                options: [...this.listUsers],
                required: '1',
                disabled: true,
                css: 'col-12 col-lg-6',
                appearance: 'legacy',
                // search: '1',
                // searchCtrl: 'searchCtrl',
            },
            {
                type: 'RADIO',
                referenceValue: 'trangThai',
                name: 'Trạng thái',
                defaultValue: true,
                required: '0',
                options: [
                    { key: true, name: 'Hoạt động' },
                    { key: false, name: 'Tạm ngưng' },
                ],
                css: 'col-12 col-lg-6',
            },
        ];

        let fieldsCtrls = {};
        let fieldList = [];

        for (let f of this.listFormField) {
            fieldList = [...fieldList, f];

            if (f.type === 'NUMBER') {
                fieldsCtrls[f.referenceValue] = new FormControl(
                    f.defaultValue,
                    f.required === '1' ? [Validators.required] : [],
                );
            } else if (f.type !== 'CHECKBOX') {
                let validators = [
                    Validators.minLength(f.minLength),
                    Validators.maxLength(f.maxLength),
                ];
                if (f.type === 'EMAIL') {
                    validators = [...validators, Validators.email];
                }
                fieldsCtrls[f.referenceValue] = new FormControl(
                    f.defaultValue || '',
                    f.required === '1'
                        ? [...validators, Validators.required]
                        : [...validators],
                );

                if (f.type === 'SELECT') {
                    if (f.referenceValue === 'donViQuanLyId') {
                        fieldsCtrls[f.referenceValue].valueChanges.subscribe(
                            (value) => {
                                if (value) {
                                    let bodySubmit = {
                                        code: 'users',
                                        valueData: JSON.stringify({
                                            donViId: value,
                                        }),
                                    };

                                    this.commonService
                                        .callDataAPIShort(
                                            '/api/services/read/Data/GetData',
                                            bodySubmit,
                                        )
                                        .subscribe((response) => {
                                            this.listUsers = JSON.parse(
                                                response.result.valueData,
                                            ).map((item) => ({
                                                key: item.userId,
                                                name: this._dataService.compactWhitespace(
                                                    item.ho + ' ' + item.ten,
                                                ),
                                            }));

                                            fieldList[2].options = [
                                                ...this.listUsers,
                                            ];

                                            fieldList[3].options = [
                                                ...this.listUsers,
                                            ];
                                        });
                                }
                            },
                        );
                    }

                    if (f.referenceValue === 'quanLyBepId') {
                        fieldsCtrls[f.referenceValue].valueChanges.subscribe(
                            (value) => {
                                let result = this.listUsers.filter(
                                    (user) => user.key === value,
                                );
                                if (result.length > 0) {
                                    this.formOthers.patchValue({
                                        quanLy: result[0].name,
                                    });
                                }
                            },
                        );
                    }

                    if (f.referenceValue === 'thuTruongId') {
                        fieldsCtrls[f.referenceValue].valueChanges.subscribe(
                            (value) => {
                                let result = this.listUsers.filter(
                                    (user) => user.key === value,
                                );

                                if (result.length > 0) {
                                    this.formOthers.patchValue({
                                        thuTruongDonVi: result[0].name,
                                    });
                                }
                            },
                        );
                    }
                }
            } else {
                //if checkbox, it need multiple
                let opts = [];
                for (let index in f.options) {
                    opts[index] = new FormControl(false);
                }
                fieldsCtrls[f.referenceValue] = new FormArray(
                    opts,
                    f.required === '1' ? [Validators.required] : [],
                );
            }
        }

        this.rfDialog = new FormGroup(fieldsCtrls);

        // Form chi tiết khác
        this.listOthersField = [
            {
                type: 'TEXT',
                referenceValue: 'nguoiVietPhieu',
                name: 'Người viết phiếu',
                defaultValue: '',
                required: '0',
                disabled: false,
                css: 'col-12 col-lg-6',
                appearance: 'legacy',
            },
            {
                type: 'TEXT',
                referenceValue: 'nguoiNhan',
                name: 'Người nhận',
                defaultValue: '',
                required: '0',
                disabled: false,
                css: 'col-12 col-lg-6',
                appearance: 'legacy',
            },
            {
                type: 'TEXT',
                referenceValue: 'nguoiDuyet',
                name: 'Người duyệt',
                defaultValue: '',
                required: '0',
                disabled: false,
                css: 'col-12 col-lg-6',
                appearance: 'legacy',
            },
            {
                type: 'TEXT',
                referenceValue: 'quanLy',
                name: 'Quản lý',
                defaultValue: '',
                required: '0',
                disabled: false,
                css: 'col-12 col-lg-6',
                appearance: 'legacy',
            },
            {
                type: 'TEXT',
                referenceValue: 'troLyHauCan',
                name: 'Trợ lý hậu cần',
                defaultValue: '',
                required: '0',
                disabled: false,
                css: 'col-12 col-lg-6',
                appearance: 'legacy',
            },
            {
                type: 'TEXT',
                referenceValue: 'thuTruongDonVi',
                name: 'Thủ trưởng đơn vị',
                defaultValue: '',
                required: '0',
                disabled: false,
                css: 'col-12 col-lg-6',
                appearance: 'legacy',
            },
            {
                type: 'TEXT',
                referenceValue: 'thuKho',
                name: 'Thủ kho',
                defaultValue: '',
                required: '0',
                disabled: false,
                css: 'col-12 col-lg-6',
                appearance: 'legacy',
            },
            {
                type: 'TEXT',
                referenceValue: 'nguoiLapBaoCao',
                name: 'Người lập báo cáo',
                defaultValue: '',
                required: '0',
                disabled: false,
                css: 'col-12 col-lg-6',
                appearance: 'legacy',
            },
            {
                type: 'TEXT',
                referenceValue: 'trucBan',
                name: 'Trực ban',
                defaultValue: '',
                required: '0',
                disabled: false,
                css: 'col-12 col-lg-6',
                appearance: 'legacy',
            },
            {
                type: 'TEXT',
                referenceValue: 'bepTruong',
                name: 'Bếp trưởng',
                defaultValue: '',
                required: '0',
                disabled: false,
                css: 'col-12 col-lg-6',
                appearance: 'legacy',
            },
            {
                type: 'TEXT',
                referenceValue: 'truongBanTaiChinh',
                name: 'Trưởng ban tài chính',
                defaultValue: '',
                required: '0',
                disabled: false,
                css: 'col-12 col-lg-6',
                appearance: 'legacy',
            },
        ];

        let fieldsOthersCtrls = {};
        let fieldOthersList = [];

        for (let f of this.listOthersField) {
            fieldOthersList = [...fieldOthersList, f];

            if (f.type === 'NUMBER') {
                fieldsOthersCtrls[f.referenceValue] = new FormControl(
                    f.defaultValue,
                    f.required === '1' ? [Validators.required] : [],
                );
            } else if (f.type !== 'CHECKBOX') {
                let validators = [
                    Validators.minLength(f.minLength),
                    Validators.maxLength(f.maxLength),
                ];

                if (f.type === 'EMAIL') {
                    validators = [...validators, Validators.email];
                }
                fieldsOthersCtrls[f.referenceValue] = new FormControl(
                    f.defaultValue || '',
                    f.required === '1'
                        ? [...validators, Validators.required]
                        : [...validators],
                );
            } else {
                //if checkbox, it need multiple
                let opts = [];
                for (let index in f.options) {
                    opts[index] = new FormControl(false);
                }
                fieldsOthersCtrls[f.referenceValue] = new FormArray(
                    opts,
                    f.required === '1' ? [Validators.required] : [],
                );
            }
        }

        this.formOthers = new FormGroup(fieldsOthersCtrls);
    }

    ngAfterViewInit(): void {
        if (this.data.type === 'EDIT') {
            this.rfDialog.patchValue({
                ...this.data.dataDialogValue,
                tenBep: this.data.dataDialogValue.tenBep,
            });

            this.formOthers.patchValue({
                ...this.data.dataDialogValue,
            });
        }
    }

    ngAfterViewChecked(): void {
        this.changeDetectorRef.detectChanges();
    }

    onNoClick(): void {
        this.dialogRef.close();
    }

    getFields() {
        return this.listFormField;
    }

    getOthersFields() {
        return this.listOthersField;
    }

    onSubmit() {
        if (
            this.rfDialog.value.thuTruongId === this.rfDialog.value.quanLyBepId
        ) {
            this._toastrService.error(
                '',
                'Quản lý bếp và thủ trưởng không được cùng 1 người phụ trách',
            );

            return;
        } else {
            let dataForm = {
                ...this.rfDialog.value,
                donViCapTrenTrucTiepId: this.data.currentUnitUser.id,
                tenDonViCapTrenTrucTiep: 'string',
                hideValue: `${JSON.stringify(
                    this.rfDialog.value.quanLyBepId,
                )},${JSON.stringify(this.rfDialog.value.thuTruongId)}`,
                ...this.formOthers.value,
            };

            let bodyData =
                this.data.type === 'EDIT'
                    ? {
                          dataDialog: {
                              ...this.data.dataDialogValue,
                              ...dataForm,
                          },
                          type: this.data.type,
                      }
                    : {
                          dataDialog: dataForm,
                          type: this.data.type,
                      };

            this.onSave.emit(bodyData);
            this.dialogRef.close();
        }
    }
}
