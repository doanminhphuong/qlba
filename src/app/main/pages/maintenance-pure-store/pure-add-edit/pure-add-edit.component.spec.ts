import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PureAddEditComponent } from './pure-add-edit.component';

describe('PureAddEditComponent', () => {
  let component: PureAddEditComponent;
  let fixture: ComponentFixture<PureAddEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PureAddEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PureAddEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
