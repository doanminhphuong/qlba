import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatPaginator, MatTableDataSource } from '@angular/material';
import { Router } from '@angular/router';
import { DeleteConfirmDialogComponent } from '@app/_shared/dialogs/delete-confirm-dialog/delete-confirm-dialog.component';
import { CommonService } from '@core/services/common.service';
import { DataService } from '@core/services/data-service';
import { ToastrService } from '@core/services/toastr.service';
import { forkJoin, Subscription } from 'rxjs';

@Component({
    selector: 'app-list-total-export',
    templateUrl: './list-total-export.component.html',
    styleUrls: ['./list-total-export.component.scss'],
})
export class ListTotalExportComponent implements OnInit {
    displayedColumnsOfField = [
        'ngayTao',
        'maPhieu',
        'soLuongMatHang',
        'thanhTien',
        'khoXuat',
        'actions',
    ];

    dataSource = new MatTableDataSource<any>([]);
    @ViewChild(MatPaginator) paginator!: MatPaginator;

    listInventory: any[] = [];
    allowExec: boolean = false;

    bodySubmit;
    subscription: Subscription;
    isLoading: boolean = false;

    constructor(
        private _router: Router,
        private _commonService: CommonService,
        private _toastrService: ToastrService,
        private _dataService: DataService,
        private dialog: MatDialog,
    ) {
        this.subscription = this._dataService.getData().subscribe((message) => {
            this.isLoading = true;
            this.bodySubmit = { ...this.bodySubmit, dauMoiBepId: message.id };
            this.getInitData();
        });
    }

    ngOnInit() {
        let request = {
            id: localStorage.getItem('idMenu'),
            language: 'vi',
        };

        this._commonService.checkPermission(request).subscribe((response) => {
            if (JSON.parse(response.result).some((obj) => obj.key === 'EXEC')) {
                this.allowExec = true;

                this.bodySubmit = {
                    maxResultCount: 2147483647,
                    dauMoiBepId: this._dataService.getDauMoiBepId(),
                };
            } else {
                this.bodySubmit = {
                    maxResultCount: 2147483647,
                    criterias: [
                        {
                            propertyName: 'trangThai',
                            operation: 0,
                            value: 3,
                        },
                    ],
                    dauMoiBepId: this._dataService.getDauMoiBepId(),
                };
            }
            setTimeout(() => {
                this.getInitData();
            }, 1000);
        });
    }

    getInitData() {
        const dataInit = this._commonService.callDataAPIShort(
            '/api/services/read/PhieuTongXuat/GetAll',
            this.bodySubmit,
        );

        const dataInventory = this._commonService.callDataAPIShort(
            '/api/services/read/Kho/GetAll',
            {
                maxResultCount: 2147483647,
                criterias: [
                    {
                        propertyName: 'dauMoiBepId',
                        operation: 0,
                        value: this._dataService.getDauMoiBepId(),
                    },
                ],
            },
        );

        forkJoin([dataInit, dataInventory]).subscribe((response) => {
            this.dataSource = new MatTableDataSource<any>(
                response[0].result.items,
            );
            this.dataSource.paginator = this.paginator;

            this.listInventory = response[1].result.items.map((item) => ({
                key: item.id,
                name: item.tenKho,
            }));

            setTimeout(() => {
                this.isLoading = false;
            }, 200);
        });
    }

    convertIdToName(id: number) {
        let result;
        if (this.listInventory.length > 0) {
            const dataFound = this.listInventory.find(
                (item) => item.key === id,
            );

            result = dataFound ? dataFound.name : '';
        }
        return result;
    }

    detail(element: any, typeAction) {
        this._router.navigate([
            `manage/app/category/tong-xuat-kho/detail/${element.id}`,
            { typeAction },
        ]);
    }

    delete(element: any) {
        const dialogRef = this.dialog.open(DeleteConfirmDialogComponent, {
            width: '500px',
            data: element,
        });

        dialogRef.componentInstance.handleDelete.subscribe((dataDelete) => {
            const isSubmitted = !!dataDelete;

            this._commonService
                .callDataAPIShort('/api/services/write/PhieuTongXuat/Delete', {
                    id: element.id,
                })
                .subscribe(
                    (response) => dialogRef.close(isSubmitted),
                    (err) => this._toastrService.errorServer(err),
                );
        });

        dialogRef.afterClosed().subscribe((result) => {
            if (!result) return;

            this.getInitData();
            this._toastrService.success('', 'Xóa thành công');
        });
    }
}
