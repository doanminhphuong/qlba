import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SummaryExportComponent } from './summary-export.component';

describe('SummaryExportComponent', () => {
  let component: SummaryExportComponent;
  let fixture: ComponentFixture<SummaryExportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SummaryExportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SummaryExportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
