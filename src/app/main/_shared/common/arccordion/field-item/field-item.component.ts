import {
    Component,
    EventEmitter,
    Input,
    OnChanges,
    OnInit,
    Output,
    SimpleChanges,
    ViewChild,
} from '@angular/core';
import { MatDialog, MatTable, MatTableDataSource } from '@angular/material';
import { DynamicDialogComponent } from '@app/components/dynamic-dialog-component/dynamic-dialog-component.component';
import { CategoryService } from '@core/services/category.service';
import { ToastrService } from '@core/services/toastr.service';

export interface PeriodicElement {
    name: string;
    position: number;
    weight: number;
    symbol: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
    { position: 1, name: 'Hydrogen', weight: 1.0079, symbol: 'H' },
    { position: 2, name: 'Helium', weight: 4.0026, symbol: 'He' },
    { position: 3, name: 'Lithium', weight: 6.941, symbol: 'Li' },
    { position: 4, name: 'Beryllium', weight: 9.0122, symbol: 'Be' },
    { position: 5, name: 'Boron', weight: 10.811, symbol: 'B' },
    { position: 6, name: 'Carbon', weight: 12.0107, symbol: 'C' },
    { position: 7, name: 'Nitrogen', weight: 14.0067, symbol: 'N' },
    { position: 8, name: 'Oxygen', weight: 15.9994, symbol: 'O' },
    { position: 9, name: 'Fluorine', weight: 18.9984, symbol: 'F' },
    { position: 10, name: 'Neon', weight: 20.1797, symbol: 'Ne' },
];

@Component({
    selector: 'app-field-item',
    templateUrl: './field-item.component.html',
    styleUrls: ['./field-item.component.scss'],
})
export class FieldItemComponent implements OnInit, OnChanges {
    displayedColumns: string[] = [
        'name',
        'type',
        'required',
        'defaultValue',
        'referenceValue',
        'dependenCodes',
        'css',
        'index',
        'action',
    ];
    dataSource;
    @Input('filed') filed: any;
    @Input() groupCode: any;
    @Output() itemField: any = new EventEmitter();
    @ViewChild(MatTable) table: MatTable<any>;
    fieldSize: number;
    tempData: any;
    isTempData: boolean = false;

    constructor(
        public dialog: MatDialog,
        private _toastrService: ToastrService,
        private _categoryService: CategoryService,
    ) {}

    ngOnInit() {
        this.dataSource = this.filed;
        this.dataSource.sort((a, b) => a.number10 - b.number10); // DESC List Tab
    }

    // Check current data is changed to update
    ngOnChanges(changes: SimpleChanges): void {
        this.dataSource = changes.filed.currentValue;
    }

    handlesizeCol(element: any) {
        return JSON.parse(element).size;
    }

    deleteField(element: any) {
        console.log(element);
        this.dataSource = this.filed.filter((item) => item.id !== element.id);
        this.itemField.emit({ data: element, type: 'DELETE_FIELD' });
    }

    updateField(element: any) {
        // console.log('element', element);
        // console.log('this.filed', this.filed);

        // console.log('element::', element);
        // console.log('tempData::', this.tempData);

        // element.type = 'TEXT';
        // console.log('dataSource::', this.dataSource);
        // console.log('tempData::', this.tempData);

        const fields = [
            {
                type: 'TEXT',
                referenceValue: 'name',
                name: 'Tên hiển thị',
                defaultValue: '',
                required: false,
                icon: 'title',
                css: 'col-12 col-lg-12',
            },
            {
                type: 'TEXT',
                referenceValue: 'format',
                name: 'Định dạng',
                defaultValue: '',
                required: false,
                icon: 'link',
                css: 'col-12 col-lg-6',
            },
            {
                type: 'TEXT',
                referenceValue: 'validate',
                name: 'Tính toàn vẹn',
                defaultValue: '',
                required: false,
                icon: 'texture',
                css: 'col-12 col-lg-6',
            },
            {
                type: 'TEXT',
                referenceValue: 'dependenCodes',
                name: 'Field phụ thuộc',
                defaultValue: '',
                required: false,
                icon: 'texture',
                css: 'col-12 col-lg-4',
            },
            {
                type: 'TEXT',
                referenceValue: 'category',
                name: 'Danh mục',
                defaultValue: '',
                required: false,
                icon: 'texture',
                css: 'col-12 col-lg-4',
            },
            {
                type: 'TEXT',
                referenceValue: 'icon',
                name: 'Icon',
                defaultValue: '',
                required: false,
                icon: 'texture',
                css: 'col-12 col-lg-4',
            },
            {
                type: 'TEXT',
                referenceValue: 'formula',
                name: 'Công thức',
                defaultValue: '',
                required: false,
                icon: 'texture',
                css: 'col-12 col-lg-12',
            },
            {
                type: 'TEXTAREA',
                referenceValue: 'formulaView',
                name: 'Xem công thức',
                defaultValue: '',
                required: false,
                icon: 'texture',
                css: 'col-12 col-lg-12',
            },
        ];

        let dataField = {
            // data: this.isTempData ? this.tempData : element,
            datas: element,
            typeDialog: 'UPDATE_FIELD',
            fields: fields,
        };

        let dialogUpdateField = this.dialog.open(DynamicDialogComponent, {
            width: '70%',
            data: dataField,
            panelClass: 'my-custom-dialog-class',
        });

        dialogUpdateField.componentInstance.onSave.subscribe(
            (dataCreate: any) => {
                const isSubmitted = !!dataCreate;

                let dataSubmit = {
                    ...dataField.datas,
                    ...dataCreate,
                };

                const types = ['SELECT', 'CHECKBOX', 'RADIO', 'MULTISELECT'];

                if (types.includes(dataSubmit.type)) {
                    let data = {
                        maxResultCount: 10,
                        skipCount: 0,
                        sorting: 'Code',
                        criterias: [
                            {
                                propertyName: 'GroupCode',
                                operation: 6,
                                value: dataSubmit.category,
                            },
                        ],
                    };

                    this._categoryService
                        .getAllCategory(data)
                        .subscribe((data) => {
                            let options = data.result.map((x) => ({
                                key: x.codeData,
                                name: x.name,
                            }));

                            dataSubmit.options = options;
                        });
                }

                console.log('Data Submit: ', dataSubmit);

                this.isTempData = true;

                let indexField = this.filed.findIndex(
                    (item) => item.id === element.id,
                );
                this.filed.splice(indexField, 1, dataSubmit);
                this.dataSource = new MatTableDataSource(this.filed);

                this.itemField.emit({ data: dataSubmit, type: 'UPDATE_FIELD' });
                this.isTempData = false;

                dialogUpdateField.close(isSubmitted);
            },
        );

        dialogUpdateField.componentInstance.handleDelete.subscribe(
            (dataDelete) => {
                const isDeleted = !!dataDelete;
                console.log('Data Delete: ', dataDelete);

                this.dataSource = this.filed.filter(
                    (item) => item.id !== dataDelete.id,
                );
                this.itemField.emit({ data: dataDelete, type: 'DELETE_FIELD' });

                dialogUpdateField.close(isDeleted);
            },
        );

        dialogUpdateField.afterClosed().subscribe((result) => {
            if (!result) return;

            this._toastrService.success('', 'Cật nhật thành công');
        });
        // return;
        // this.itemField.emit({ data: element, type: 'UPDATE_FIELD' });
    }

    currentField(element: any) {
        console.log('element::', element);
    }
}
