import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustommerAddEditComponent } from './custommer-add-edit.component';

describe('CustommerAddEditComponent', () => {
  let component: CustommerAddEditComponent;
  let fixture: ComponentFixture<CustommerAddEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustommerAddEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustommerAddEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
