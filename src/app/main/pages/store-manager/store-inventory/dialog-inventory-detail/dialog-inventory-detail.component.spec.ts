import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogInventoryDetailComponent } from './dialog-inventory-detail.component';

describe('DialogInventoryDetailComponent', () => {
  let component: DialogInventoryDetailComponent;
  let fixture: ComponentFixture<DialogInventoryDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogInventoryDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogInventoryDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
