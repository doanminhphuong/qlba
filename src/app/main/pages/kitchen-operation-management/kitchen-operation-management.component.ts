import { SelectionModel } from '@angular/cdk/collections';
import {
    AfterViewInit,
    Component,
    OnDestroy,
    OnInit,
    ViewChild,
} from '@angular/core';
import {
    MatDialog,
    MatPaginator,
    MatTableDataSource,
    PageEvent,
} from '@angular/material';
import { Router } from '@angular/router';
import { DynamicDialogComponent } from '@app/components/dynamic-dialog-component/dynamic-dialog-component.component';
import { DeleteConfirmDialogComponent } from '@app/_shared/dialogs/delete-confirm-dialog/delete-confirm-dialog.component';
import { CommonService } from '@core/services/common.service';
import { DataService } from '@core/services/data-service';
import { ToastrService } from '@core/services/toastr.service';
import { Subscription } from 'rxjs';

@Component({
    selector: 'app-kitchen-operation-management',
    templateUrl: './kitchen-operation-management.component.html',
    styleUrls: ['./kitchen-operation-management.component.scss'],
})
export class KitchenOperationManagementComponent
    implements OnInit, AfterViewInit, OnDestroy
{
    @ViewChild(MatPaginator) paginator!: MatPaginator;
    subscription: Subscription;

    // Table
    displayedColumnsOfField: string[] = [
        'chungLoaiBep',
        'loaiBep',
        'soCheTao',
        'ngayBatDauSuDung',
        'status',
        'actions',
    ];
    dataSource = new MatTableDataSource<any>([]);
    selection = new SelectionModel<any>(true, []);

    // Pagination
    totalRows = 0;
    pageSize = 10;
    currentPage = 0;
    pageSizeOptions: number[] = [5, 10, 25, 50];
    dataSkipped: number = 0;

    // Search Advanced
    fieldSearch: any[] = [
        {
            type: 'SELECT',
            referenceValue: 'trangThai',
            name: 'Chọn trạng thái',
            defaultValue: 'all',
            required: '0',
            options: [
                { key: 'all', name: 'Tất cả' },
                { key: true, name: 'Hoạt động' },
                { key: false, name: 'Tạm ngưng' },
            ],
            css: 'col-12 col-lg-4',
            appearance: 'legacy',
        },
        {
            type: 'DATETIME',
            referenceValue: 'startDate',
            name: 'Từ ngày',
            defaultValue: '',
            required: '0',
            css: 'col-12 col-lg-4',
            appearance: 'legacy',
        },
        {
            type: 'DATETIME',
            referenceValue: 'endDate',
            name: 'Đến ngày',
            defaultValue: '',
            required: '0',
            dependenField: 'startDate',
            css: 'col-12 col-lg-4',
            appearance: 'legacy',
        },
    ];

    // Init
    dauMoiBepId: number;
    chungLoaiBep: string;
    loaiBepOptions = [
        { key: 0, name: 'Bếp lò hơi cơ khí' },
        { key: 1, name: 'Bếp dầu' },
        { key: 2, name: 'Bếp điện' },
    ];

    trangThaiFilter: boolean | string;
    tuNgayFilter: Date;
    denNgayFilter: Date;

    // Boolean
    isLoading: boolean = false;
    isFiltering: boolean = false;
    isFilteringAdvance: boolean = false;
    allowExec: boolean = false;

    constructor(
        private _router: Router,
        public dialog: MatDialog,
        private _toastrService: ToastrService,
        private _commonService: CommonService,
        private _dataService: DataService,
    ) {
        this._commonService.checkPermission2().subscribe((response) => {
            if (!response.result) return;

            const permissions = JSON.parse(response.result);
            this.allowExec = permissions.some((obj) => obj.key === 'EXEC');
        });

        this.subscription = this._dataService.getData().subscribe((message) => {
            this.isLoading = true;
            this.getInitData(message.id);
        });
    }

    ngOnInit() {
        this.dauMoiBepId = this._dataService.getDauMoiBepId();
        this.getInitData(this.dauMoiBepId);
    }

    ngAfterViewInit() {
        this.dataSource.paginator = this.paginator;
    }

    ngOnDestroy(): void {
        this.subscription.unsubscribe();
    }

    getInitData(dauMoiBepId: number): void {
        this.dauMoiBepId = dauMoiBepId;

        const body = {
            maxResultCount: this.pageSize,
            skipCount: this.dataSkipped,
            sorting: 'ngayBatDauSuDung DESC',
            criterias: [
                {
                    propertyName: 'dauMoiBepId',
                    operation: 0,
                    value: dauMoiBepId,
                },
            ],
        };

        this._commonService
            .callDataAPIShort('/api/services/read/Bep/GetAll', body)
            .subscribe((res) => {
                this.dataSource.data = res.result.items;
                setTimeout(() => {
                    this.paginator.pageIndex = this.currentPage;
                    this.paginator.length = res.result.totalCount;
                    this.isLoading = false;
                }, 200);
            });
    }

    pageChanged(event: PageEvent) {
        if (this.isFiltering) return;

        this.currentPage = event.pageIndex;
        this.dataSkipped = event.pageIndex * 10;

        this.getInitData(this.dauMoiBepId);
    }

    openDialog(dataDialog: any): void {
        const fields = [
            {
                type: 'TEXT',
                referenceValue: 'chungLoaiBep',
                name: 'Chủng loại bếp',
                defaultValue: '',
                required: '1',
                // icon: 'code',
                css: 'col-12 col-lg-6',
            },
            {
                type: 'SELECT',
                referenceValue: 'loaiBep',
                name: 'Chọn loại bếp',
                defaultValue: '',
                required: '1',
                options: [...this.loaiBepOptions],
                disabled: !!dataDialog,
                css: 'col-12 col-lg-6',
            },
            {
                type: 'TEXT',
                referenceValue: 'soCheTao',
                name: 'Số chế tạo',
                defaultValue: '',
                required: '1',
                // icon: 'code',
                css: 'col-12 col-lg-6',
            },
            {
                type: 'DATETIME',
                referenceValue: 'ngaySanXuat',
                name: 'Ngày sản xuất',
                defaultValue: '',
                required: '1',
                css: 'col-12 col-lg-6',
            },
            {
                type: 'TEXT',
                referenceValue: 'noiSanXuat',
                name: 'Nơi sản xuất',
                defaultValue: '',
                required: '1',
                css: 'col-12 col-lg-6',
            },
            {
                type: 'TEXT',
                referenceValue: 'donViKiemDinh',
                name: 'Đơn vị kiểm định',
                defaultValue: '',
                required: '1',
                css: 'col-12 col-lg-6',
            },
            {
                type: 'TEXT',
                referenceValue: 'soTemKiemDinh',
                name: 'Số tem kiểm định',
                defaultValue: '',
                required: '1',
                css: 'col-12 col-lg-6',
            },
            {
                type: 'DATETIME',
                referenceValue: 'ngayKiemDinh',
                name: 'Thời gian kiểm định',
                defaultValue: '',
                required: '1',
                css: 'col-12 col-lg-6',
            },
            {
                type: 'DATETIME',
                referenceValue: 'ngayBatDauSuDung',
                name: 'Thời gian bắt đầu sử dụng',
                defaultValue: '',
                required: '1',
                css: 'col-12 col-lg-6',
            },
            {
                type: 'RADIO',
                referenceValue: 'trangThai',
                name: 'Trạng thái',
                defaultValue: true,
                required: '0',
                options: [
                    { key: true, name: 'Hoạt động' },
                    { key: false, name: 'Tạm ngưng' },
                ],
                css: 'col-12 col-lg-6',
            },

            //donViKiemDinh
        ];

        let dialogRef = this.dialog.open(DynamicDialogComponent, {
            // disableClose: true,
            width: '70%',
            data: {
                datas: dataDialog,
                fields: fields,
                typeDialog: 'lttp',
            },
            panelClass: 'my-custom-dialog-class',
        });

        dialogRef.componentInstance.onSave.subscribe((formValues) => {
            const isSubmitted = !!formValues;

            let dataSubmit = {
                ...dataDialog,
                ...formValues,
                dauMoiBepId: this.dauMoiBepId,
            };

            if (dataSubmit.chungLoaiBep === dataSubmit.soCheTao) {
                this._toastrService.error(
                    '',
                    'Chủng loại bếp và Số chế tạo đang bị trùng',
                );
                return;
            } else {
                if (!dataDialog) {
                    this._commonService
                        .callDataAPIShort(
                            '/api/services/write/Bep/Create',
                            dataSubmit,
                        )
                        .subscribe(
                            (res) => dialogRef.close(isSubmitted),
                            (err) => this._toastrService.errorServer(err),
                        );
                } else {
                    this._commonService
                        .callDataAPIShort(
                            '/api/services/write/Bep/Update',
                            dataSubmit,
                        )
                        .subscribe(
                            (res) => dialogRef.close(isSubmitted),
                            (err) => this._toastrService.errorServer(err),
                        );
                }
            }
        });

        dialogRef.componentInstance.handleDelete.subscribe((dataDelete) => {
            const isDeleted = !!dataDelete;
        });

        dialogRef.afterClosed().subscribe((result) => {
            if (!result) return;

            if (this.isFiltering) {
                const propertyName = 'chungLoaiBep';
                const operation = 6;
                const value = this.chungLoaiBep;

                this.getAllDataFilter(propertyName, operation, value);
            } else if (this.isFilteringAdvance) {
                let bodyRequest = {
                    maxResultCount: 9999999,
                    sorting: 'ngayBatDauSuDung DESC',
                    criterias: [
                        // trangThai
                        {
                            propertyName:
                                this.trangThaiFilter !== null &&
                                this.trangThaiFilter !== 'all'
                                    ? 'trangThai'
                                    : '',
                            operation:
                                this.trangThaiFilter !== null &&
                                this.trangThaiFilter !== 'all'
                                    ? 'Equals'
                                    : 'Contains',
                            value:
                                this.trangThaiFilter !== null &&
                                this.trangThaiFilter !== 'all'
                                    ? this.trangThaiFilter
                                    : '',
                        },

                        // tuNgay
                        {
                            propertyName: this.tuNgayFilter
                                ? 'ngayBatDauSuDung'
                                : '',
                            operation: this.tuNgayFilter
                                ? 'GreaterThanOrEqual'
                                : 'Contains',
                            value: this.tuNgayFilter
                                ? this.tuNgayFilter
                                      .toISOString()
                                      .replace('T17', 'T00')
                                : '',
                        },
                        // denNgay
                        {
                            propertyName: this.denNgayFilter
                                ? 'ngayBatDauSuDung'
                                : '',
                            operation: this.denNgayFilter
                                ? 'LessThanOrEqual'
                                : 'Contains',
                            value: this.denNgayFilter
                                ? this.denNgayFilter
                                      .toISOString()
                                      .replace('T17', 'T00')
                                : '',
                        },
                    ],
                };

                this._commonService
                    .callDataAPIShort(
                        '/api/services/read/Bep/GetAll',
                        bodyRequest,
                    )
                    .subscribe((response) => {
                        this.dataSource.data = response.result.items;
                        this.dataSource.paginator = this.paginator;
                    });
            } else {
                this.getInitData(this.dauMoiBepId);
            }

            if (!dataDialog) {
                this._toastrService.success('', 'Thêm thành công');
            } else {
                this._toastrService.success('', 'Cập nhật thành công');
            }
        });
    }

    createAction(): void {
        this.openDialog(null);
    }

    editAction(element: any): void {
        this.openDialog(element);
    }

    deleteAction(element: any): void {
        const dialogRef = this.dialog.open(DeleteConfirmDialogComponent, {
            width: '500px',
            data: element,
        });

        dialogRef.componentInstance.handleDelete.subscribe((dataDelete) => {
            const isSubmitted = !!dataDelete;

            this._commonService
                .callDataAPIShort('/api/services/write/Bep/Delete', dataDelete)
                .subscribe(
                    (res) => dialogRef.close(isSubmitted),
                    (err) => this._toastrService.errorServer(err),
                );
        });

        dialogRef.afterClosed().subscribe((result) => {
            if (!result) return;

            this.getInitData(this.dauMoiBepId);
            this._toastrService.success('', 'Xóa thành công');
        });
    }

    getLoaiBep(key: number) {
        let result = '';

        let dataFound = this.loaiBepOptions.find((item) => item.key === key);
        result = dataFound ? dataFound['name'] : '';

        return result;
    }

    getAllDataFilter(
        propertyName: string,
        operation: number | string,
        value: any,
    ) {
        const bodyFilter = {
            maxResultCount: 9999,
            skipCount: 0,
            sorting: 'Id',
            criterias: [
                {
                    propertyName: 'dauMoiBepId',
                    operation: 0,
                    value: this.dauMoiBepId,
                },
                {
                    propertyName,
                    operation,
                    value,
                },
            ],
        };

        this._commonService
            .callDataAPIShort('/api/services/read/Bep/GetAll', bodyFilter)
            .subscribe((res) => {
                this.dataSource.data = res.result.items;
                this.dataSource.paginator = this.paginator;
            });
    }

    handleInputFilter(filterValue: string) {
        this.chungLoaiBep = filterValue.trim().toLowerCase();

        if (!this.chungLoaiBep) {
            this.isFiltering = false;
            this.getInitData(this.dauMoiBepId);
        } else {
            const propertyName = 'chungLoaiBep';
            const operation = 6;
            const value = this.chungLoaiBep;

            this.getAllDataFilter(propertyName, operation, value);
            this.isFiltering = true;
        }
    }

    handleSearchAdvanced(data: any): void {
        const { startDate, endDate, trangThai } = data;

        if (data.key === 'reset') {
            this.isFilteringAdvance = false;
            this.getInitData(this.dauMoiBepId);
        } else {
            const tuNgay = !startDate ? '' : startDate._d;
            const denNgay = !endDate ? '' : endDate._d;

            this.trangThaiFilter = trangThai;
            this.tuNgayFilter = tuNgay;
            this.denNgayFilter = denNgay;

            let bodyRequest = {
                maxResultCount: 9999999,
                sorting: 'ngayBatDauSuDung DESC',
                criterias: [
                    // trangThai
                    {
                        propertyName:
                            trangThai !== null && trangThai !== 'all'
                                ? 'trangThai'
                                : '',
                        operation:
                            trangThai !== null && trangThai !== 'all'
                                ? 'Equals'
                                : 'Contains',
                        value:
                            trangThai !== null && trangThai !== 'all'
                                ? trangThai
                                : '',
                    },

                    // tuNgay
                    {
                        propertyName: tuNgay ? 'ngayBatDauSuDung' : '',
                        operation: tuNgay ? 'GreaterThanOrEqual' : 'Contains',
                        value: tuNgay
                            ? tuNgay.toISOString().replace('T17', 'T00')
                            : '',
                    },
                    // denNgay
                    {
                        propertyName: denNgay ? 'ngayBatDauSuDung' : '',
                        operation: denNgay ? 'LessThanOrEqual' : 'Contains',
                        value: denNgay
                            ? denNgay.toISOString().replace('T17', 'T00')
                            : '',
                    },
                ],
            };

            this._commonService
                .callDataAPIShort('/api/services/read/Bep/GetAll', bodyRequest)
                .subscribe((response) => {
                    this.dataSource.data = response.result.items;
                    this.dataSource.paginator = this.paginator;
                    this.isFilteringAdvance = true;
                });
        }
    }

    navigate(category: string): void {
        this._router.navigate([`manage/app/category/${category}`]);
    }

    showDetail(element: any): void {
        const chungLoaiBep = element['chungLoaiBep'];

        const loaiBep =
            element['loaiBep'] === 0
                ? 'Bếp lò hơi cơ khí'
                : element['loaiBep'] === 1
                ? 'Bếp dầu'
                : 'Bếp điện';

        this._router.navigate([
            `manage/app/category/nhat-ky-van-hanh-bep/` + element.id,
            { chungLoaiBep, loaiBep },
        ]);
    }
}
