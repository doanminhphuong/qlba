import { Injectable } from '@angular/core';
import {
    CanActivate,
    ActivatedRouteSnapshot,
    RouterStateSnapshot,
    UrlTree,
    Router,
} from '@angular/router';
import { TokenStorageService } from '@core/services/token.service';
import { Observable } from 'rxjs';

const TOKEN_KEY = 'auth-token';

@Injectable({
    providedIn: 'root', // you can change to any level if needed
})
export class CanHomepageGuard implements CanActivate {
    constructor(
        private _tokenStorageService: TokenStorageService,
        private _router: Router,
    ) {}
    canActivate(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot,
    ):
        | Observable<boolean | UrlTree>
        | Promise<boolean | UrlTree>
        | boolean
        | UrlTree {
        let tokenService = this._tokenStorageService.getToken();
        let tokenLocale = localStorage.getItem(TOKEN_KEY);

        if (tokenService !== tokenLocale || !tokenService) {
            this._router.navigate(['login']);
            return false;
        } else {
            return true; // replace with actual logic
        }
    }
}
