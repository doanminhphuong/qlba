// "Hot Module Replacement" enabled environment

export const environment = {
    production: false,
    hmr: true,
    appConfig: 'appconfig.json',
    baseUrl: 'http://localhost:4200',
    SWAGGER_URL: 'http://bepan.tasp.vn',
    SSO_URL: 'http://sso.tasp.vn',
    SYS_URL: 'http://sys.tasp.vn',
    DATA_URL: 'http://data.tasp.vn',
};
