import {
    animate,
    state,
    style,
    transition,
    trigger,
} from '@angular/animations';
import { SelectionModel } from '@angular/cdk/collections';
import { DatePipe } from '@angular/common';
import {
    ChangeDetectorRef,
    Component,
    ElementRef,
    OnInit,
    QueryList,
    SimpleChanges,
    ViewChild,
    ViewChildren,
} from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatPaginator, MatTableDataSource } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { DeleteConfirmDialogComponent } from '@app/_shared/dialogs/delete-confirm-dialog/delete-confirm-dialog.component';
import { MaterialDialogComponent } from '@app/_shared/dialogs/material-dialog/material-dialog.component';
import { PriceConvertPipe } from '@core/pipes/price-convert.pipe';
import { CategoryService } from '@core/services/category.service';
import { CommonService } from '@core/services/common.service';
import { DataService } from '@core/services/data-service';
import { ExportExcelService } from '@core/services/export-excel.service';
import { FoodFuelService } from '@core/services/food-fuel.service';
import { InventoryService } from '@core/services/inventory.service';
import { PhieuXuatKhoService } from '@core/services/phieu-xuat-kho.service';
import { PriceService } from '@core/services/price.service';
import { ToastrService } from '@core/services/toastr.service';
import { UserService } from '@core/services/user.service';
import * as moment from 'moment';
import { forkJoin, Subject, Subscription } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';
import { GROUP_FOOD, GROUP_FOOD_UPGRADE } from './dataJson';

export interface PeriodicElement {
    name: string;
    position: number;
    weight: number;
}

export interface EstimateHuman {
    header: null;
    total: number;
    entityId: null;
    code: string;
    valueData: string;
    option: null;
    actions: null;
}

export interface duKienLttps {
    lttpChatDot: string;
    lttpChatDotId: number;
    donViTinhId: string;
    donViTinh: string;
    soLuong: number;
}

export interface duKienQuanSos {
    doiTuongId: string;
    tienAn: number;
    quanSo: number;
}

@Component({
    selector: 'app-plan-spend-add-edit',
    templateUrl: './plan-spend-add-edit.component.html',
    styleUrls: ['./plan-spend-add-edit.component.scss'],
    animations: [
        trigger('detailExpand', [
            state('collapsed', style({ height: '0px', minHeight: '0' })),
            state('expanded', style({ height: '*' })),
            transition(
                'expanded <=> collapsed',
                animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)'),
            ),
        ]),
    ],
    providers: [DatePipe, PriceConvertPipe],
})
export class PlanSpendAddEditComponent implements OnInit {
    public applyFilter = new Subject<KeyboardEvent>();
    private subscription: Subscription;
    @ViewChildren('paginatorList') paginator = new QueryList<MatPaginator>();
    @ViewChild('paginatorLTTP') paginatorLTTP!: MatPaginator;

    titleOnEdit: string;
    tabSelected = new FormControl(0);

    form: FormGroup;
    formLTTP: FormGroup;
    fields: any[] = [];
    fieldsPrice: any[] = [];
    fieldsLTTP: any[] = [];

    unitAfterSwap: number = 1;
    displayedColumns: string[] = ['name', 'weight', 'symbol', 'position'];

    displayedColumnsGroupFoodUpgrade: string[] = Object.keys(
        GROUP_FOOD_UPGRADE[0],
    ).filter((item) => item !== 'createdAt' && item !== 'id');
    listDeleteKey = ['weight', 'foodLevel', 'position'];
    displayedColumnsGroupFoodUpgradeMore = [
        'select',
        ...this.displayedColumnsGroupFoodUpgrade,
        'actions',
    ];

    statisticals = [];

    displayedColumnsGroopFood: string[] = Object.keys(GROUP_FOOD[0]).filter(
        (item) =>
            item !== 'weight' && item !== 'foodLevel' && item !== 'position',
    );
    listKeysFoodLevel = GROUP_FOOD[0].foodLevel.map((item) => item.title);

    dataSource = new MatTableDataSource<any>([]);
    dataSourceGroupFood = new MatTableDataSource<any>();
    dataSourceGroupFoodUpgrade = new MatTableDataSource<any>();
    dataSourceGroupFoodTotalPrice = new MatTableDataSource<any>();
    selection = new SelectionModel<any>(true, []);

    currentTable: any[] = [];
    proposedId: any;
    typeAction: number | string;
    dataEdit: any;
    priceAdjustmentList: any[] = [];
    allWarehouse: any[] = [];
    allUsers: any[] = [];
    dataEstimateHuman: EstimateHuman;

    // Boolean
    isEdit: boolean = false;
    isPublished: boolean = false;

    totalPrice: number = 0;
    typeOfPrice: string;
    adjustmentInfo: any[] = [];
    priceList: any[] = [];
    foodFuelList: any[] = [];

    // Categories
    userList: any[] = [];
    organizationOptions: any[] = [];
    thucDonOptions: any[] = [];
    userOptions: any[] = [];
    supplierOptions: any[] = [];
    chiTietList: any[] = [];
    duKienLttps: duKienLttps[] = [];
    duKienQuanSos: duKienQuanSos[] = [];

    isActive: boolean = false;
    warehouseExportId: number;

    cacheLttpChatDotId: number;
    cacheListFood: any = {};
    ngayDuXuat: string;

    // tab LTTP, CĐ
    thucDonId: number = 0;
    danhSachMonAn = [
        {
            buoiAn: 'Gạo + than + gia vị',
            danhSach: new MatTableDataSource<any>([]),
            tongTienDuXuat: 0,
            tongThu: 0,
        },
        {
            buoiAn: 'Sáng',
            danhSach: new MatTableDataSource<any>([]),
            tongTienDuXuat: 0,
            tongThu: 0,
        },
        {
            buoiAn: 'Trưa',
            danhSach: new MatTableDataSource<any>([]),
            tongTienDuXuat: 0,
            tongThu: 0,
        },
        {
            buoiAn: 'Chiều',
            danhSach: new MatTableDataSource<any>([]),
            tongTienDuXuat: 0,
            tongThu: 0,
        },
        {
            buoiAn: 'Sáng hôm sau',
            danhSach: new MatTableDataSource<any>([]),
            tongTienDuXuat: 0,
            tongThu: 0,
        },
    ];

    currentPriceAdjustmentId: number;
    listPriceAdjustmentId: any[] = [];
    listInventory: any[] = [];
    listQuanSoAn: any[] = [];
    DotDieuChinhGiaId: number = 0;
    DotDieuChinhGiaOptions: any[] = [];
    chiTietLttpChatDot: any[] = [];
    dataSourceLTTP = new MatTableDataSource<any>([]);
    displayedColumnsGroopFoodExpland: string[] = [];
    displayedColumnsOfLTTP: string[] = ['nhomLTTP', 'donViTinh'];
    displayedColumnsOfNVL: string[] = [
        'tenLttpChatDot',
        'donViTinh',
        'donGia',
        'hienCo',
        'soLuongTrenBdMua',
        'soLuongTrenBdTg',
        'soLuongDonViBdMua',
        'soLuongDonViBdTg',
        'thanhTien',
        'duXuat',
        'thanhTienDuXuat',
        'actions',
    ];
    @ViewChildren('handlePriceElement')
    handlePriceElement: QueryList<ElementRef>;
    userOrganization;

    selectedRadio: any;

    // tab Phần tiền
    displayedBoxPriceColumns: string[] = [
        'buoi',
        'quanSo',
        'mucTienAn',
        'tongTienAn',
    ];
    listReasonEat: any[] = [];

    // tab Phần gạo, chất đốt
    displayedFuelColumns: string[] = ['buoi', 'quanSo', 'dinhLuong', 'tong'];
    displayedFoodColumns: string[] = [
        'buoi',
        'quanSo',
        'dinhLuongGao',
        'tongGao',
    ];
    dataSourceFuel = new MatTableDataSource<any>([]);
    dataSourceFood = new MatTableDataSource<any>([]);
    listFood: any[] = [];
    listFuel: any[] = [];
    idFood: number;
    idFuel: number;
    defaultFood: number;
    defaultFuel: number;
    listFoodFuel: any[] = [];
    listNVLOfCurrentPriceAdjustment: any[] = [];

    // tab Phần ăn thêm
    displayedBoxExtraPriceColumns: string[] = [
        'loai',
        'quanSo',
        'mucTienAnThem',
        'tongTienAnThem',
    ];

    allowExec: boolean = false;
    isFinishedGetDataMoney: boolean = false;
    isFinishedGetDataMenu: boolean = false;
    isFinishedGetData: boolean = false;
    isLoading: boolean;
    thuaThieuHomTruoc: number;
    hasChange: boolean = false;
    currentOpenedItemId: number;

    constructor(
        private readonly changeDetectorRef: ChangeDetectorRef,
        private _activatedRoute: ActivatedRoute,
        private _toastrService: ToastrService,
        private _router: Router,
        public dialog: MatDialog,
        private _priceService: PriceService,
        private _categoryService: CategoryService,
        private _userService: UserService,
        private _foodFuelService: FoodFuelService,
        private _phieuXuatKhoService: PhieuXuatKhoService,
        private _inventoryService: InventoryService,
        private _commonService: CommonService,
        private datePipe: DatePipe,
        private _exportService: ExportExcelService,
        private _dataService: DataService,
    ) {
        this.subscription = this.applyFilter
            .pipe(
                map((event) => (event.target as HTMLInputElement).value),
                debounceTime(500),
                distinctUntilChanged(),
            )
            .subscribe((filterValue) => this.handleInputFilter(filterValue));
    }

    ngOnChanges(changes: SimpleChanges): void {
        console.log('changes', changes);
    }

    ngOnInit() {
        // Check if this is Create or Edit page
        this._activatedRoute.paramMap.subscribe((param) => {
            this.proposedId = +param.get('id');
            if (!this.proposedId) return;
            this.isEdit = true;
        });

        let request = {
            id: localStorage.getItem('idMenu'),
            language: 'vi',
        };

        this._commonService.checkPermission(request).subscribe((response) => {
            if (JSON.parse(response.result).some((obj) => obj.key === 'EXEC')) {
                this.allowExec = true;
            } else {
                this.displayedColumnsOfNVL.splice(8, 1);
                this.displayedColumnsOfNVL.splice(0, 1);
            }

            this.getInitData();

            this.getDataOfCurrentPriceAdjustment();
            this.getData();
        });
    }

    // Tab LTTP, CĐ
    getDataThucDon() {
        // Thực đơn của 3 buổi sáng, trưa, chiều
        this._commonService
            .callDataAPIShort(`/api/services/read/ChiTietThucDon/GetAll`, {
                maxResultCount: 2147483647,
                criterias: [
                    {
                        propertyName: 'ngayAn',
                        operation: 0,
                        value: new Date(this.form.value.ngayDuXuat)
                            .toISOString()
                            .replace('T23:59:59.999Z', 'T00:00:00Z'),
                    },
                ],
            })
            .subscribe((res) => {
                if (res.result.items.length > 0) {
                    this.thucDonId = res.result.items[0].thucDonId;
                    res.result.items.forEach((item) => {
                        this.danhSachMonAn.forEach((danhSach) => {
                            if (item.buoiAn === danhSach.buoiAn) {
                                this._commonService
                                    .callDataAPIShort(
                                        '/api/services/read/MonAn/Get',
                                        { id: item.monAnId },
                                    )
                                    .subscribe((monAn) => {
                                        // Kiểm tra LTTP trong món ăn có trong kho nào
                                        monAn.result.chiTiet.forEach(
                                            (chiTiet) => {
                                                let body = {
                                                    maxResultCount: 2147483647,
                                                    criterias: [
                                                        {
                                                            propertyName:
                                                                'lttpChatDotId',
                                                            operation: 0,
                                                            value: chiTiet.lttpChatDotId,
                                                        },
                                                        {
                                                            propertyName: 'ton',
                                                            operation: 2,
                                                            value: 0,
                                                        },
                                                    ],
                                                };

                                                this._commonService
                                                    .callDataAPIShort(
                                                        '/api/services/read/TonKho/GetAll',
                                                        body,
                                                    )
                                                    .subscribe((responses) => {
                                                        // Nếu có thì push vào data table với type === kho
                                                        if (
                                                            responses.result
                                                                .items.length >
                                                            0
                                                        ) {
                                                            responses.result.items.forEach(
                                                                (kho) => {
                                                                    danhSach.danhSach.data.push(
                                                                        {
                                                                            lttpChatDotId:
                                                                                kho.lttpChatDotId,
                                                                            tenLttpChatDot:
                                                                                this.convertNameById(
                                                                                    kho.lttpChatDotId,
                                                                                    'FoodFuel',
                                                                                ),
                                                                            donViTinh:
                                                                                kho.donViTinh,
                                                                            khoId: kho.khoId,
                                                                            donGia: kho.donGia,
                                                                            hienCo: kho.ton,
                                                                            soLuongTrenBdMua:
                                                                                '',
                                                                            soLuongTrenBdTg:
                                                                                '',
                                                                            soLuongDonViBdMua:
                                                                                '',
                                                                            soLuongDonViBdTg:
                                                                                '',
                                                                            thanhTien: 0,
                                                                            duXuat: '',
                                                                            thanhTienDuXuat: 0,
                                                                            type: 'kho',
                                                                            typePriceImport: 0,
                                                                        },
                                                                    );

                                                                    danhSach.danhSach.data =
                                                                        danhSach.danhSach.data.filter(
                                                                            (
                                                                                value,
                                                                                index,
                                                                                self,
                                                                            ) =>
                                                                                index ===
                                                                                self.findIndex(
                                                                                    (
                                                                                        t,
                                                                                    ) =>
                                                                                        t.lttpChatDotId ===
                                                                                            value.lttpChatDotId &&
                                                                                        t.type ===
                                                                                            value.type,
                                                                                ),
                                                                        );
                                                                },
                                                            );

                                                            for (
                                                                let i = 0;
                                                                i <
                                                                this
                                                                    .danhSachMonAn
                                                                    .length;
                                                                i++
                                                            ) {
                                                                this.danhSachMonAn[
                                                                    i
                                                                ].danhSach.paginator =
                                                                    this.paginator.toArray()[
                                                                        i
                                                                    ];
                                                            }
                                                        }
                                                        // Nếu không thì kiểm tra LTTP có trong hội đồng giá đang được sử dụng hiện tại
                                                        else if (
                                                            responses.result
                                                                .items
                                                                .length === 0
                                                        ) {
                                                            let bodySubmit = {
                                                                maxResultCount: 2147483647,
                                                                criterias: [
                                                                    {
                                                                        propertyName:
                                                                            'dotDieuChinhGiaId',
                                                                        operation: 0,
                                                                        value: this
                                                                            .currentPriceAdjustmentId,
                                                                    },
                                                                    {
                                                                        propertyName:
                                                                            'lttpChatDotId',
                                                                        operation: 0,
                                                                        value: chiTiet.lttpChatDotId,
                                                                    },
                                                                ],
                                                            };

                                                            this._commonService
                                                                .callDataAPIShort(
                                                                    '/api/services/read/ChiTietDotDieuChinhGia/GetAll',
                                                                    bodySubmit,
                                                                )
                                                                .subscribe(
                                                                    (
                                                                        detail,
                                                                    ) => {
                                                                        if (
                                                                            detail
                                                                                .result
                                                                                .items
                                                                                .length >
                                                                            0
                                                                        ) {
                                                                            detail.result.items.forEach(
                                                                                (
                                                                                    dieuChinh,
                                                                                ) => {
                                                                                    danhSach.danhSach.data.push(
                                                                                        {
                                                                                            lttpChatDotId:
                                                                                                dieuChinh.lttpChatDotId,
                                                                                            tenLttpChatDot:
                                                                                                this.convertNameById(
                                                                                                    dieuChinh.lttpChatDotId,
                                                                                                    'FoodFuel',
                                                                                                ),
                                                                                            donViTinh:
                                                                                                this.getDonViTinh(
                                                                                                    dieuChinh.lttpChatDotId,
                                                                                                ),
                                                                                            khoId: '',
                                                                                            loaiGia:
                                                                                                [
                                                                                                    {
                                                                                                        key: dieuChinh.giaThiTruong,
                                                                                                        name:
                                                                                                            'Giá thị trường ' +
                                                                                                            dieuChinh.giaThiTruong,
                                                                                                    },
                                                                                                    {
                                                                                                        key: dieuChinh.giaTangGiaSanXuat,
                                                                                                        name:
                                                                                                            'Giá tăng gia ' +
                                                                                                            dieuChinh.giaTangGiaSanXuat,
                                                                                                    },
                                                                                                ],
                                                                                            donGia: dieuChinh.giaThiTruong,
                                                                                            hienCo: 0,
                                                                                            soLuongTrenBdMua:
                                                                                                '',
                                                                                            soLuongTrenBdTg:
                                                                                                '',
                                                                                            soLuongDonViBdMua:
                                                                                                '',
                                                                                            soLuongDonViBdTg:
                                                                                                '',
                                                                                            thanhTien: 0,
                                                                                            duXuat: '',
                                                                                            thanhTienDuXuat: 0,
                                                                                            type: 'dcg',
                                                                                            typePriceImport: 0,
                                                                                        },
                                                                                    );

                                                                                    danhSach.danhSach.data =
                                                                                        danhSach.danhSach.data.filter(
                                                                                            (
                                                                                                value,
                                                                                                index,
                                                                                                self,
                                                                                            ) =>
                                                                                                index ===
                                                                                                self.findIndex(
                                                                                                    (
                                                                                                        t,
                                                                                                    ) =>
                                                                                                        t.lttpChatDotId ===
                                                                                                            value.lttpChatDotId &&
                                                                                                        t.type ===
                                                                                                            value.type,
                                                                                                ),
                                                                                        );
                                                                                },
                                                                            );

                                                                            for (
                                                                                let i = 0;
                                                                                i <
                                                                                this
                                                                                    .danhSachMonAn
                                                                                    .length;
                                                                                i++
                                                                            ) {
                                                                                this.danhSachMonAn[
                                                                                    i
                                                                                ].danhSach.paginator =
                                                                                    this.paginator.toArray()[
                                                                                        i
                                                                                    ];
                                                                            }
                                                                        } else if (
                                                                            detail
                                                                                .result
                                                                                .items
                                                                                .length ===
                                                                            0
                                                                        ) {
                                                                            // Nếu không có cả ở kho và hội đồng giá thì push vào data table với type === other
                                                                            this.listFoodFuel.forEach(
                                                                                (
                                                                                    foodFuel,
                                                                                ) => {
                                                                                    if (
                                                                                        foodFuel.key ===
                                                                                        chiTiet.lttpChatDotId
                                                                                    ) {
                                                                                        danhSach.danhSach.data.push(
                                                                                            {
                                                                                                lttpChatDotId:
                                                                                                    foodFuel.key,
                                                                                                tenLttpChatDot:
                                                                                                    this.convertNameById(
                                                                                                        foodFuel.key,
                                                                                                        'FoodFuel',
                                                                                                    ),
                                                                                                donViTinh:
                                                                                                    foodFuel.donViTinh,
                                                                                                khoId: '',
                                                                                                donGia: 0,
                                                                                                hienCo: 0,
                                                                                                soLuongTrenBdMua:
                                                                                                    '',
                                                                                                soLuongTrenBdTg:
                                                                                                    '',
                                                                                                soLuongDonViBdMua:
                                                                                                    '',
                                                                                                soLuongDonViBdTg:
                                                                                                    '',
                                                                                                thanhTien: 0,
                                                                                                duXuat: '',
                                                                                                thanhTienDuXuat: 0,
                                                                                                type: 'other',
                                                                                                typePriceImport: 0,
                                                                                            },
                                                                                        );

                                                                                        danhSach.danhSach.data =
                                                                                            danhSach.danhSach.data.filter(
                                                                                                (
                                                                                                    value,
                                                                                                    index,
                                                                                                    self,
                                                                                                ) =>
                                                                                                    index ===
                                                                                                    self.findIndex(
                                                                                                        (
                                                                                                            t,
                                                                                                        ) =>
                                                                                                            t.lttpChatDotId ===
                                                                                                                value.lttpChatDotId &&
                                                                                                            t.type ===
                                                                                                                value.type,
                                                                                                    ),
                                                                                            );
                                                                                    }
                                                                                },
                                                                            );

                                                                            for (
                                                                                let i = 0;
                                                                                i <
                                                                                this
                                                                                    .danhSachMonAn
                                                                                    .length;
                                                                                i++
                                                                            ) {
                                                                                this.danhSachMonAn[
                                                                                    i
                                                                                ].danhSach.paginator =
                                                                                    this.paginator.toArray()[
                                                                                        i
                                                                                    ];
                                                                            }
                                                                        }
                                                                    },
                                                                );
                                                        }
                                                    });
                                            },
                                        );
                                    });
                            }
                        });
                    });
                }
            });

        //  Thực đơn sáng hôm sau
        this._commonService
            .callDataAPIShort(`/api/services/read/ChiTietThucDon/GetAll`, {
                maxResultCount: 2147483647,
                criterias: [
                    {
                        propertyName: 'ngayAn',
                        operation: 0,
                        value: moment(this.form.value.ngayDuXuat)
                            .add(1, 'days')
                            .toISOString(),
                    },
                ],
            })
            .subscribe((res) => {
                if (res.result.items.length > 0) {
                    res.result.items.forEach((item) => {
                        this.danhSachMonAn.forEach((danhSach) => {
                            if (
                                item.buoiAn === 'Sáng' &&
                                danhSach.buoiAn === 'Sáng hôm sau'
                            ) {
                                this._commonService
                                    .callDataAPIShort(
                                        '/api/services/read/MonAn/Get',
                                        { id: item.monAnId },
                                    )
                                    .subscribe((monAn) => {
                                        // Kiểm tra LTTP trong món ăn có trong kho nào
                                        monAn.result.chiTiet.forEach(
                                            (chiTiet) => {
                                                let body = {
                                                    maxResultCount: 2147483647,
                                                    criterias: [
                                                        {
                                                            propertyName:
                                                                'lttpChatDotId',
                                                            operation: 0,
                                                            value: chiTiet.lttpChatDotId,
                                                        },
                                                        {
                                                            propertyName: 'ton',
                                                            operation: 2,
                                                            value: 0,
                                                        },
                                                    ],
                                                };

                                                this._commonService
                                                    .callDataAPIShort(
                                                        '/api/services/read/TonKho/GetAll',
                                                        body,
                                                    )
                                                    .subscribe((responses) => {
                                                        // Nếu có thì push vào data table với type === kho
                                                        if (
                                                            responses.result
                                                                .items.length >
                                                            0
                                                        ) {
                                                            responses.result.items.forEach(
                                                                (kho) => {
                                                                    danhSach.danhSach.data.push(
                                                                        {
                                                                            lttpChatDotId:
                                                                                kho.lttpChatDotId,
                                                                            tenLttpChatDot:
                                                                                this.convertNameById(
                                                                                    kho.lttpChatDotId,
                                                                                    'FoodFuel',
                                                                                ),
                                                                            donViTinh:
                                                                                kho.donViTinh,
                                                                            khoId: kho.khoId,
                                                                            donGia: kho.donGia,
                                                                            hienCo: kho.ton,
                                                                            soLuongTrenBdMua:
                                                                                '',
                                                                            soLuongTrenBdTg:
                                                                                '',
                                                                            soLuongDonViBdMua:
                                                                                '',
                                                                            soLuongDonViBdTg:
                                                                                '',
                                                                            thanhTien: 0,
                                                                            duXuat: '',
                                                                            thanhTienDuXuat: 0,
                                                                            type: 'kho',
                                                                            typePriceImport: 0,
                                                                        },
                                                                    );

                                                                    danhSach.danhSach.data =
                                                                        danhSach.danhSach.data.filter(
                                                                            (
                                                                                value,
                                                                                index,
                                                                                self,
                                                                            ) =>
                                                                                index ===
                                                                                self.findIndex(
                                                                                    (
                                                                                        t,
                                                                                    ) =>
                                                                                        t.lttpChatDotId ===
                                                                                            value.lttpChatDotId &&
                                                                                        t.type ===
                                                                                            value.type,
                                                                                ),
                                                                        );
                                                                },
                                                            );

                                                            this.danhSachMonAn[3].danhSach.paginator =
                                                                this.paginator.toArray()[3];
                                                        }
                                                        // Nếu không thì kiểm tra LTTP có trong hội đồng giá đang được sử dụng hiện tại
                                                        else if (
                                                            responses.result
                                                                .items
                                                                .length === 0
                                                        ) {
                                                            let bodySubmit = {
                                                                maxResultCount: 2147483647,
                                                                criterias: [
                                                                    {
                                                                        propertyName:
                                                                            'dotDieuChinhGiaId',
                                                                        operation: 0,
                                                                        value: this
                                                                            .currentPriceAdjustmentId,
                                                                    },
                                                                    {
                                                                        propertyName:
                                                                            'lttpChatDotId',
                                                                        operation: 0,
                                                                        value: chiTiet.lttpChatDotId,
                                                                    },
                                                                ],
                                                            };

                                                            this._commonService
                                                                .callDataAPIShort(
                                                                    '/api/services/read/ChiTietDotDieuChinhGia/GetAll',
                                                                    bodySubmit,
                                                                )
                                                                .subscribe(
                                                                    (
                                                                        detail,
                                                                    ) => {
                                                                        if (
                                                                            detail
                                                                                .result
                                                                                .items
                                                                                .length >
                                                                            0
                                                                        ) {
                                                                            detail.result.items.forEach(
                                                                                (
                                                                                    dieuChinh,
                                                                                ) => {
                                                                                    danhSach.danhSach.data.push(
                                                                                        {
                                                                                            lttpChatDotId:
                                                                                                dieuChinh.lttpChatDotId,
                                                                                            tenLttpChatDot:
                                                                                                this.convertNameById(
                                                                                                    dieuChinh.lttpChatDotId,
                                                                                                    'FoodFuel',
                                                                                                ),
                                                                                            donViTinh:
                                                                                                this.getDonViTinh(
                                                                                                    dieuChinh.lttpChatDotId,
                                                                                                ),
                                                                                            khoId: '',
                                                                                            loaiGia:
                                                                                                [
                                                                                                    {
                                                                                                        key: dieuChinh.giaThiTruong,
                                                                                                        name:
                                                                                                            'Giá thị trường ' +
                                                                                                            dieuChinh.giaThiTruong,
                                                                                                    },
                                                                                                    {
                                                                                                        key: dieuChinh.giaTangGiaSanXuat,
                                                                                                        name:
                                                                                                            'Giá tăng gia ' +
                                                                                                            dieuChinh.giaTangGiaSanXuat,
                                                                                                    },
                                                                                                ],
                                                                                            donGia: dieuChinh.giaThiTruong,
                                                                                            hienCo: 0,
                                                                                            soLuongTrenBdMua:
                                                                                                '',
                                                                                            soLuongTrenBdTg:
                                                                                                '',
                                                                                            soLuongDonViBdMua:
                                                                                                '',
                                                                                            soLuongDonViBdTg:
                                                                                                '',
                                                                                            thanhTien: 0,
                                                                                            duXuat: '',
                                                                                            thanhTienDuXuat: 0,
                                                                                            type: 'dcg',
                                                                                            typePriceImport: 0,
                                                                                        },
                                                                                    );

                                                                                    danhSach.danhSach.data =
                                                                                        danhSach.danhSach.data.filter(
                                                                                            (
                                                                                                value,
                                                                                                index,
                                                                                                self,
                                                                                            ) =>
                                                                                                index ===
                                                                                                self.findIndex(
                                                                                                    (
                                                                                                        t,
                                                                                                    ) =>
                                                                                                        t.lttpChatDotId ===
                                                                                                            value.lttpChatDotId &&
                                                                                                        t.type ===
                                                                                                            value.type,
                                                                                                ),
                                                                                        );
                                                                                },
                                                                            );

                                                                            this.danhSachMonAn[3].danhSach.paginator =
                                                                                this.paginator.toArray()[3];
                                                                        } else if (
                                                                            detail
                                                                                .result
                                                                                .items
                                                                                .length ===
                                                                            0
                                                                        ) {
                                                                            // Nếu không có cả ở kho và hội đồng giá thì push vào data table với type === other
                                                                            this.listFoodFuel.forEach(
                                                                                (
                                                                                    foodFuel,
                                                                                ) => {
                                                                                    if (
                                                                                        foodFuel.key ===
                                                                                        chiTiet.lttpChatDotId
                                                                                    ) {
                                                                                        danhSach.danhSach.data.push(
                                                                                            {
                                                                                                lttpChatDotId:
                                                                                                    foodFuel.key,
                                                                                                tenLttpChatDot:
                                                                                                    this.convertNameById(
                                                                                                        foodFuel.key,
                                                                                                        'FoodFuel',
                                                                                                    ),
                                                                                                donViTinh:
                                                                                                    foodFuel.donViTinh,
                                                                                                khoId: '',
                                                                                                donGia: 0,
                                                                                                hienCo: 0,
                                                                                                soLuongTrenBdMua:
                                                                                                    '',
                                                                                                soLuongTrenBdTg:
                                                                                                    '',
                                                                                                soLuongDonViBdMua:
                                                                                                    '',
                                                                                                soLuongDonViBdTg:
                                                                                                    '',
                                                                                                thanhTien: 0,
                                                                                                duXuat: '',
                                                                                                thanhTienDuXuat: 0,
                                                                                                type: 'other',
                                                                                                typePriceImport: 0,
                                                                                            },
                                                                                        );

                                                                                        danhSach.danhSach.data =
                                                                                            danhSach.danhSach.data.filter(
                                                                                                (
                                                                                                    value,
                                                                                                    index,
                                                                                                    self,
                                                                                                ) =>
                                                                                                    index ===
                                                                                                    self.findIndex(
                                                                                                        (
                                                                                                            t,
                                                                                                        ) =>
                                                                                                            t.lttpChatDotId ===
                                                                                                                value.lttpChatDotId &&
                                                                                                            t.type ===
                                                                                                                value.type,
                                                                                                    ),
                                                                                            );
                                                                                    }
                                                                                },
                                                                            );

                                                                            this.danhSachMonAn[3].danhSach.paginator =
                                                                                this.paginator.toArray()[3];
                                                                        }
                                                                    },
                                                                );
                                                        }
                                                    });
                                            },
                                        );
                                    });
                            }
                        });
                    });
                }

                this.isFinishedGetDataMenu = true;
                this.showData(
                    this.form.value.nhomGaoId,
                    this.form.value.nhomChatDotId,
                );
            });
    }

    calculateInputAndSelected(event, element, ids) {
        let field = element['soLuongDonViBdMua']
            ? element['soLuongDonViBdMua']
            : element['soLuongDonViBdTg']
            ? element['soLuongDonViBdTg']
            : element['soLuongTrenBdMua']
            ? element['soLuongTrenBdMua']
            : element['soLuongTrenBdTg']
            ? element['soLuongTrenBdTg']
            : 0;
        element['donGia'] = event.value;
        element['thanhTien'] = field * element['donGia'];
        element['thanhTienDuXuat'] = element['duXuat'] * element['donGia'];
        this.calculateData();
    }

    calculateSelectedAndInput(event, element, column) {
        switch (column) {
            case 'soLuongDonViBdMua':
                element['soLuongDonViBdTg'] = '';
                element['soLuongTrenBdMua'] = '';
                element['soLuongTrenBdTg'] = '';
                element['thanhTien'] =
                    element['soLuongDonViBdMua'] * element['donGia'];
                break;
            case 'soLuongDonViBdTg':
                element['soLuongDonViBdMua'] = '';
                element['soLuongTrenBdMua'] = '';
                element['soLuongTrenBdTg'] = '';
                element['thanhTien'] =
                    element['soLuongDonViBdTg'] * element['donGia'];
                break;
            case 'soLuongTrenBdMua':
                element['soLuongDonViBdTg'] = '';
                element['soLuongDonViBdMua'] = '';
                element['soLuongTrenBdTg'] = '';
                element['thanhTien'] =
                    element['soLuongTrenBdMua'] * element['donGia'];
                break;
            case 'soLuongTrenBdTg':
                element['soLuongTrenBdMua'] = '';
                element['soLuongDonViBdTg'] = '';
                element['soLuongDonViBdMua'] = '';
                element['thanhTien'] =
                    element['soLuongTrenBdTg'] * element['donGia'];
                break;
            case 'duXuat':
                element['thanhTienDuXuat'] =
                    element['duXuat'] * element['donGia'];
                this.calculateData();
                break;
        }
    }

    addData(event, element, column) {
        let field = element['soLuongDonViBdMua']
            ? element['soLuongDonViBdMua']
            : element['soLuongDonViBdTg']
            ? element['soLuongDonViBdTg']
            : element['soLuongTrenBdMua']
            ? element['soLuongTrenBdMua']
            : element['soLuongTrenBdTg']
            ? element['soLuongTrenBdTg']
            : 0;
        element['donGia'] = event;
        element['thanhTien'] = field * element['donGia'];
        element['thanhTienDuXuat'] = element['duXuat'] * element['donGia'];
        this.calculateData();
    }

    calculateData() {
        this.danhSachMonAn.map((detail) => {
            let tongTienDuXuat = this.sum(
                detail.danhSach.data,
                'thanhTienDuXuat',
            );
            detail.tongTienDuXuat = tongTienDuXuat;
        });

        this.statisticals[2].value = this.sum(
            this.danhSachMonAn,
            'tongTienDuXuat',
        );

        this.statisticals[4].value =
            this.sum(this.danhSachMonAn, 'tongTienDuXuat') -
            this.thuaThieuHomTruoc;

        this.statisticals[5].value =
            this.statisticals[1].value - this.statisticals[4].value;
    }

    sum(array, key) {
        const initialValue = 0;
        const sumWithInitial = array.reduce(
            (accumulator, currentValue) => accumulator + currentValue[key],
            initialValue,
        );

        return sumWithInitial;
    }

    getDonViTinh(id: any) {
        let data;
        let dataList = [];

        if (this.listPriceAdjustmentId.length > 0) {
            dataList = this.listPriceAdjustmentId;

            let item = dataList.find((item) => item.lttpChatDotId === id);
            data = item ? item.lttpChatDot.donViTinh : '';
        }

        return data;
    }
    //

    // Tab tiền ăn
    getMoney(date: any) {
        let url = '/api/services/read/DuXuat/DuXuatTienAn';
        let bodySubmit = {
            ngayDuXuat: date,
            danhSachDotQuanSoAn: this.form.value.danhSachDotQuanSoAn,
        };

        this._commonService
            .callDataAPIShort(url, bodySubmit)
            .subscribe((response) => {
                this.dataSourceGroupFoodTotalPrice.data =
                    response.result.chiTietDuXuat;
                this.thuaThieuHomTruoc = response.result.thuaThieuHomTruoc;
                this.statisticals = [
                    {
                        value: 0,
                        title: 'Quân số bình quân',
                    },
                    {
                        value: response.result.tongTien,
                        title: 'Cộng thu',
                    },
                    {
                        value: this.sum(this.danhSachMonAn, 'tongTienDuXuat'),
                        title: 'Tiền đã sử dụng',
                    },
                    {
                        value: this.thuaThieuHomTruoc,
                        title:
                            this.thuaThieuHomTruoc > 0
                                ? 'Thừa hôm trước'
                                : 'Thiếu hôm trước',
                    },
                    {
                        value: response.result.congChi,
                        title: 'Cộng chi',
                    },
                    {
                        value:
                            response.result.tongTien - response.result.congChi,
                        title: 'Thừa trong ngày',
                    },
                ];

                let arrayTongThu = [0, 0, 0, 0, 0];

                let result = 0;
                this.dataSourceGroupFoodTotalPrice.data.forEach((item) => {
                    result +=
                        this.totalCostOfThreeMeal(
                            item.chiTietGaoTienAn,
                            'total',
                        ) +
                        this.totalCostOfThreeMeal(item.chiTietAnThem, 'extra');

                    arrayTongThu[0] += this.sum(
                        item.chiTietGaoTienAn,
                        'tongTienAn',
                    );

                    item.chiTietGaoTienAn.forEach((chiTiet) => {
                        if (chiTiet.buoi === 0) {
                            arrayTongThu[1] += chiTiet.tongTienAn;
                        } else if (chiTiet.buoi === 1) {
                            arrayTongThu[2] += chiTiet.tongTienAn;
                        } else if (chiTiet.buoi === 2) {
                            arrayTongThu[3] += chiTiet.tongTienAn;
                            arrayTongThu[4] += chiTiet.tongTienAn;
                        }
                    });
                });

                arrayTongThu[0] = arrayTongThu[0] * 0.03;

                for (let i = 0; i < arrayTongThu.length; i++) {
                    for (let j = 0; j < this.danhSachMonAn.length; j++) {
                        if (i === j) {
                            this.danhSachMonAn[j].tongThu = arrayTongThu[i];
                        }
                    }
                }

                this.isFinishedGetDataMoney = true;
                this.getDataThucDon();
            });
    }
    //

    // Tab Phần gạo, LTTP
    handleSelect(data: any, type: string) {
        switch (type) {
            case 'food':
                this.idFood = data.value;
                break;
            case 'fuel':
                this.idFuel = data.value;
                break;
        }
    }

    showData(idFood: any, idFuel: any) {
        let date = this.transformDate(new Date(this.form.value.ngayDuXuat));

        let url =
            '/api/services/read/DuXuat/DuXuatGaoChatDot?date=' +
            date +
            `&NhomGaoId=${idFood}&NhomChatDotId=${idFuel}`;

        this._commonService.callDataAPIShort(url, {}).subscribe((response) => {
            this.dataSourceFuel.data = response.result.chiTietChatDot;

            this.dataSourceFood.data = response.result.chiTietDuXuat;

            let tongGao = response.result.tongGao;
            let result = 0;
            this.dataSourceFood.data.forEach((item) => {
                result += this.totalCostOfThreeMeal(
                    item.chiTietGaoTienAn,
                    'food',
                );
            });

            this.formLTTP.patchValue({
                tongGao: Math.round(tongGao * 1e3) / 1e3,
                gaoDaSuDung: Math.round(result * 1e3) / 1e3,
                gaoThuaTrongNgay:
                    tongGao - result >= 0
                        ? Math.round((response.result.tongGao - result) * 1e3) /
                          1e3
                        : 0,
            });

            let averageNumber = 0;
            this.dataSourceGroupFoodTotalPrice.data.forEach((item) => {
                averageNumber +=
                    this.sum(item.chiTietGaoTienAn, 'tongTienAn') /
                    item.mucTien;
            });

            this.statisticals[0].value = Math.round(averageNumber);

            this.isFinishedGetData = true;

            if (
                this.isFinishedGetData &&
                this.isFinishedGetDataMenu &&
                this.isFinishedGetDataMoney
            ) {
                this.isLoading = false;
            }
        });
    }
    //

    resetData() {
        this.dataSourceGroupFoodTotalPrice.data = [];
        this.dataSourceLTTP.data = [];
        this.danhSachMonAn.forEach((danhSach) => {
            danhSach.danhSach.data = [];
        });
    }

    ngAfterViewChecked(): void {
        this.changeDetectorRef.detectChanges();
    }

    ngAfterViewInit(): void {
        this.isPublished = this.dataEdit ? this.dataEdit.isPublished : false;
        for (let i = 0; i < this.danhSachMonAn.length; i++) {
            this.danhSachMonAn[i].danhSach.paginator =
                this.paginator.toArray()[i];
        }
    }

    getData(): void {
        // let getListFood =
        this._commonService
            .callDataAPIShort('/api/services/read/LttpChatDot/GetAll', {
                maxResultCount: 9999999,
            })
            .subscribe((res) => {
                this.listFoodFuel = res.result.items.map((item) => ({
                    key: item.id,
                    name: item.tenLttpChatDot,
                    donViTinh: item.donViTinh,
                }));
            });

        this._commonService
            .callDataAPIShort('/api/services/read/Kho/GetAll', {
                maxResultCount: 9999999,
            })
            .subscribe((res) => {
                this.listInventory = res.result.items.map((item) => ({
                    key: item.id,
                    name: item.tenKho,
                }));
            });

        this._userService.getUserOrganization().subscribe((response) => {
            let organization = response.result.filter(
                (item) =>
                    item.groupCode === 'DonVi' || item.groupCode === '1-donvi',
            );

            if (organization.length > 0) {
                this.userOrganization = organization[0].id;
            }
        });
    }

    getBodyCategory(groupCode: string) {
        return {
            maxResultCount: 999999,
            skipCount: 0,
            sorting: 'Code',
            criterias: [
                {
                    propertyName: 'GroupCode',
                    operation: 6,
                    value: groupCode,
                },
            ],
        };
    }

    getInitData(): void {
        let getAllReasonEat = this._commonService.callDataAPIShort(
            '/api/services/read/Category/GetList',
            this.getBodyCategory('ly-do-xuat-hang'),
        );

        let getDotQuanSoAn = this._commonService.callDataAPIShort(
            '/api/services/read/DotQuanSoAn/GetAll',
            { maxResultCount: 2147483647 },
        );

        let dataGroupFoodFuel = this._commonService.callDataAPIShort(
            '/api/services/read/NhomLttpChatDot/GetAll',
            {
                maxResultCount: 9999999,
            },
        );

        let dataFoodFuel = this._commonService.callDataAPIShort(
            '/api/services/read/LttpChatDot/GetAll',
            {
                maxResultCount: 9999999,
            },
        );

        let dataInventories = this._commonService.callDataAPIShort(
            '/api/services/read/Kho/GetAll',
            {
                maxResultCount: 9999999,
            },
        );

        forkJoin([
            getAllReasonEat,
            getDotQuanSoAn,
            dataGroupFoodFuel,
            dataFoodFuel,
            dataInventories,
        ]).subscribe((results) => {
            this.listReasonEat = results[0].result.map((item) => ({
                key: item.codeData,
                name: item.name,
            }));

            this.listQuanSoAn = results[1].result.items.map((item) => ({
                key: item.id,
                name: item.tenDot,
            }));

            results[2].result.items.map((item) => {
                switch (item.codeData) {
                    case 'LT':
                        this.listFood.push({
                            key: item.id,
                            name: item.tenNhomLttpChatDot,
                        });
                        break;
                    case 'CD':
                        this.listFuel.push({
                            key: item.id,
                            name: item.tenNhomLttpChatDot,
                        });
                        break;
                }
            });

            this.listFoodFuel = results[3].result.items.map((item) => ({
                key: item.id,
                name: item.tenLttpChatDot,
                donViTinh: item.donViTinh,
            }));

            this.listInventory = results[4].result.items.map((item) => ({
                key: item.id,
                name: item.tenKho,
            }));

            this.getInitForm();
        });
    }

    getDataOfCurrentPriceAdjustment() {
        this._commonService
            .callDataAPIShort(
                '/api/services/read/DotDieuChinhGia/GetCurrent',
                null,
            )
            .subscribe((res) => {
                this.currentPriceAdjustmentId = res.result.id;

                this._commonService
                    .callDataAPIShort(
                        '/api/services/read/DotDieuChinhGia/Get',
                        { id: this.currentPriceAdjustmentId },
                    )
                    .subscribe((response) => {
                        this.listPriceAdjustmentId = response.result.chiTiet;
                    });
            });
    }

    getInitForm(): void {
        var date = new Date();

        // add a day
        date.setDate(date.getDate() + 1);

        let validateDate = new Date().getHours() >= 17 ? date : new Date();
        this.fields = [
            {
                type: 'DATETIME',
                referenceValue: 'ngayDuXuat',
                name: 'Chọn ngày dự chi',
                defaultValue: '',
                required: '1',
                css: 'col-12 col-lg-4',
                appearance: 'legacy',
                disabled:
                    this.allowExec === false || this.isPublished === true
                        ? true
                        : false,
                min: validateDate,
            },
            {
                type: 'TEXT',
                referenceValue: 'maDuXuat',
                name: 'Nhập mã dự chi',
                defaultValue: '',
                required: '1',
                dependenField: 'ngayDuXuat',
                css: 'col-12 col-lg-4',
                appearance: 'legacy',
                disabled:
                    this.allowExec === false || this.isPublished === true
                        ? true
                        : false,
            },
            {
                type: 'TEXT',
                referenceValue: 'tenDuXuat',
                name: 'Nhập tên dự chi',
                defaultValue: '',
                required: '1',
                css: 'col-12 col-lg-4',
                appearance: 'legacy',
                disabled:
                    this.allowExec === false || this.isPublished === true
                        ? true
                        : false,
            },
            {
                type: 'SELECT',
                referenceValue: 'nhomGaoId',
                name: 'Chọn loại gạo',
                defaultValue: '',
                options: [...this.listFood],
                required: '1',
                disabled: this.isPublished === true ? true : false,
                css: 'col-12 col-lg-4',
                appearance: 'legacy',
            },
            {
                type: 'SELECT',
                referenceValue: 'nhomChatDotId',
                name: 'Chọn chất đốt',
                defaultValue: '',
                options: [...this.listFuel],
                required: '1',
                disabled: this.isPublished === true ? true : false,
                css: 'col-12 col-lg-4',
                appearance: 'legacy',
            },
            {
                type: 'SELECT',
                referenceValue: 'lyDoSuDung',
                name: 'Chọn lý do xuất',
                defaultValue: '',
                options: [...this.listReasonEat],
                required: '1',
                disabled: this.isPublished === true ? true : false,
                css: 'col-12 col-lg-4',
                appearance: 'legacy',
            },
            {
                type: 'MULTISELECT',
                referenceValue: 'danhSachDotQuanSoAn',
                name: 'Chọn đợt quân số ăn',
                defaultValue: '',
                options: [...this.listQuanSoAn],
                required: '1',
                disabled: this.isPublished === true ? true : false,
                css: 'col-12 col-lg-4',
                appearance: 'legacy',
                selectionChange: ($event) => this.callFunctionShowData($event),
            },
        ];

        this.fieldsLTTP = [
            {
                type: 'NUMBER',
                referenceValue: 'tongGao',
                name: 'Tổng gạo',
                defaultValue: 0,
                required: '0',
                css: 'col-12 col-lg-4',
                appearance: 'legacy',
                disabled: this.allowExec === false ? true : false,
            },
            {
                type: 'NUMBER',
                referenceValue: 'gaoDaSuDung',
                name: 'Gạo đã sử dụng',
                defaultValue: 0,
                required: '0',
                css: 'col-12 col-lg-4',
                appearance: 'legacy',
                disabled: this.allowExec === false ? true : false,
            },
            {
                type: 'NUMBER',
                referenceValue: 'gaoThuaTrongNgay',
                name: 'Gạo thừa trong ngày',
                defaultValue: 0,
                required: '0',
                css: 'col-12 col-lg-4',
                appearance: 'legacy',
                disabled: this.allowExec === false ? true : false,
            },
        ];

        let fieldsCtrls = {};
        let fieldList = [];

        for (let f of this.fields) {
            fieldList = [...fieldList, f];

            if (f.type === 'NUMBER') {
                fieldsCtrls[f.referenceValue] = new FormControl(
                    { value: f.defaultValue, disabled: f.disabled },
                    f.required === '1' ? [Validators.required] : [],
                );
            } else if (f.type !== 'CHECKBOX') {
                let validators = [
                    Validators.minLength(f.minLength),
                    Validators.maxLength(f.maxLength),
                    Validators.pattern(f.pattern),
                ];

                if (f.type === 'EMAIL') {
                    validators = [...validators, Validators.email];
                }

                fieldsCtrls[f.referenceValue] = new FormControl(
                    {
                        value: f.defaultValue || '',
                        disabled: f.disabled,
                    },
                    f.required === '1'
                        ? [...validators, Validators.required]
                        : [...validators],
                );

                if (f.type === 'DATETIME') {
                    if (f.referenceValue === 'ngayDuXuat') {
                        fieldsCtrls[f.referenceValue].valueChanges.subscribe(
                            (value) => {
                                if (this.isEdit === false) {
                                    const initialValue =
                                        this.form.value.ngayDuXuat;

                                    this.hasChange = Object.keys(
                                        initialValue,
                                    ).some(
                                        (key) =>
                                            this.form.value[key] !=
                                            initialValue[key],
                                    );

                                    if (this.hasChange) {
                                        this.callFunctionShowData(null);
                                    }

                                    this.resetData();
                                    let date = new Date(value);
                                    const KEY = 'KHLTTP';
                                    let firstPath = date
                                        .toLocaleDateString()
                                        .replace(/[^0-9]/g, '');
                                    let lastPath = new Date()
                                        .toLocaleTimeString()
                                        .split(/[^0-9]/g)
                                        .map((item, index) => {
                                            if (index < 2)
                                                return ('0' + item).substring(
                                                    0,
                                                    2,
                                                );
                                            return item;
                                        })
                                        .join('');
                                    let fullPath = KEY + firstPath + lastPath;
                                    const dependField = fieldList.find(
                                        (field) =>
                                            field.dependenField ===
                                            f.referenceValue,
                                    );

                                    if (!dependField) return;

                                    fieldsCtrls[
                                        dependField.referenceValue
                                    ].patchValue(fullPath);
                                }
                            },
                        );
                    }
                }
            } else {
                //if checkbox, it need multiple
                let opts = {};
                for (let opt of f.options) {
                    opts[opt.key] = new FormControl({
                        value: opt.value,
                        disabled: f.disabled,
                    });
                }
            }
        }

        let fieldsLTTPCtrls = {};
        let fieldLTTPList = [];

        for (let f of this.fieldsLTTP) {
            fieldLTTPList = [...fieldLTTPList, f];

            if (f.type === 'NUMBER') {
                fieldsLTTPCtrls[f.referenceValue] = new FormControl(
                    { value: f.defaultValue, disabled: f.disabled },
                    f.required === '1' ? [Validators.required] : [],
                );
            } else if (f.type !== 'CHECKBOX') {
                let validators = [
                    Validators.minLength(f.minLength),
                    Validators.maxLength(f.maxLength),
                    Validators.pattern(f.pattern),
                ];

                if (f.type === 'EMAIL') {
                    validators = [...validators, Validators.email];
                }

                fieldsLTTPCtrls[f.referenceValue] = new FormControl(
                    {
                        value: f.defaultValue || '',
                        disabled: f.disabled,
                    },
                    f.required === '1'
                        ? [...validators, Validators.required]
                        : [...validators],
                );
            } else {
                //if checkbox, it need multiple
                let opts = {};
                for (let opt of f.options) {
                    opts[opt.key] = new FormControl({
                        value: opt.value,
                        disabled: f.disabled,
                    });
                }
            }
        }

        this.form = new FormGroup(fieldsCtrls);
        this.formLTTP = new FormGroup(fieldsLTTPCtrls);

        if (this.isEdit) {
            this.getDataEdit();
        }
    }

    callFunctionShowData(event: any) {
        this.isLoading = true;
        this.isFinishedGetData = false;
        this.isFinishedGetDataMenu = false;
        this.isFinishedGetDataMoney = false;
        this.getMoney(this.transformDate(this.form.value.ngayDuXuat));
    }

    convertNameById(id: any, type: any) {
        let data;
        let dataList = [];

        if (this.listFoodFuel.length > 0) {
            switch (type) {
                case 'FoodFuel':
                    dataList = this.listFoodFuel;
                    break;
                case 'Inventory':
                    dataList = this.listInventory;
                    break;
            }
            let item = dataList.find((item) => item.key === id);
            data = item && item.name ? item.name : '';
        }

        return data;
    }

    totalCostOfThreeMeal(listMeal, type) {
        let total = 0;
        if (listMeal) {
            listMeal.forEach((item) => {
                switch (type) {
                    case 'rate':
                        total += item.mucTienAn;
                        break;
                    case 'total':
                        total += item.tongTienAn;
                        break;
                    case 'extra':
                        total += item.tongTienAnThem;
                        break;
                    case 'fuel':
                        total += item.tong;
                        break;
                    case 'food':
                        total += item.tongGao;
                        break;
                    case 'foodRate':
                        total += item.dinhLuongGao;
                        break;
                }
            });
        }

        return total;
    }

    convertToMeal(key: any) {
        if (key === 0 || key === '0') {
            return 'Sáng';
        } else if (key === 1 || key === '1') {
            return 'Trưa';
        } else if (key === 2 || key === '2') {
            return 'Chiều';
        }
    }

    convertToExtra(key: any) {
        if (key === 0 || key === '0') {
            return 'Ăn ốm';
        } else if (key === 1 || key === '1') {
            return 'Ăn lễ';
        } else if (key === 2 || key === '2') {
            return 'Ăn làm nhiệm vụ';
        }
    }

    get maDuXuat() {
        return this.form.get('maDuXuat');
    }
    get menuId() {
        return this.form.get('menuId');
    }

    transformDate(date) {
        return this.datePipe.transform(date, 'yyyy-MM-dd');
    }

    getDataEdit(): void {
        const dataEditId = {
            id: this.proposedId,
        };
        this._commonService
            .callDataAPIShort('/api/services/read/DuXuat/Get', dataEditId)
            .subscribe((res) => {
                this.dataEdit = res.result;
                this.isPublished = this.dataEdit.isPublished;
                this.thuaThieuHomTruoc = this.dataEdit.thuaThieuHomTruoc;
                // Get Title
                this.allowExec === false || this.isPublished === true
                    ? (this.titleOnEdit = `Chi tiết dự chi ngày kế tiếp '${this.dataEdit.maDuXuat}'`)
                    : (this.titleOnEdit = `Chỉnh sửa dự chi ngày kế tiếp '${this.dataEdit.maDuXuat}'`);

                this.ngayDuXuat = this.dataEdit.ngayDuXuat;

                // Patch data to Form
                this.form.patchValue(this.dataEdit);
                this.formLTTP.patchValue(this.dataEdit);

                if (this.isPublished) {
                    this.displayedColumnsOfNVL.splice(0, 1);
                    this.displayedColumnsOfNVL.splice(7, 1);
                    this.form.controls['ngayDuXuat'].disable();
                    this.form.controls['maDuXuat'].disable();
                    this.form.controls['maDuXuat'].disable();
                    this.form.controls['tenDuXuat'].disable();
                    this.form.controls['danhSachDotQuanSoAn'].disable();
                    this.formLTTP.controls['tongGao'].disable();
                    this.formLTTP.controls['gaoDaSuDung'].disable();
                    this.formLTTP.controls['gaoThuaTrongNgay'].disable();
                }

                this.statisticals = [
                    {
                        value: this.dataEdit.quanSoBinhQuan,
                        title: 'Quân số bình quân',
                    },
                    {
                        value: this.dataEdit.tongTien,
                        title: 'Cộng thu',
                    },
                    {
                        value: this.dataEdit.daSuDung,
                        title: 'Tiền đã sử dụng',
                    },
                    {
                        value: this.thuaThieuHomTruoc,
                        title: 'Thiếu hôm trước',
                    },
                    {
                        value: this.dataEdit.congChi,
                        title: 'Cộng chi',
                    },
                    {
                        value: this.dataEdit.tongTien - this.dataEdit.congChi,
                        title: 'Thừa trong ngày',
                    },
                ];

                // // Patch data to Table (Details)
                this.dataSourceGroupFoodTotalPrice.data =
                    this.dataEdit.chiTietDuXuat;
                this.dataSourceFood.data = this.dataEdit.chiTietDuXuat;
                this.dataSourceFuel.data = this.dataEdit.chiTietChatDot;

                this.dataSourceLTTP = new MatTableDataSource<any>(
                    this.dataEdit.chiTietNhomLttp,
                );
                this.dataSourceLTTP.paginator = this.paginatorLTTP;

                this.displayedColumnsGroopFoodExpland = [
                    ...this.displayedColumnsOfLTTP,
                    ...this.listKeysFoodLevel,
                ];

                this.danhSachMonAn = this.dataEdit.chiTietDanhSachLttp.map(
                    (item) => ({
                        buoiAn: item.buoi,
                        danhSach: new MatTableDataSource<any>(item.danhSach),
                        tongTienDuXuat: 0,
                        tongThu: 0,
                    }),
                );

                let arrayTongThu = [0, 0, 0, 0, 0];

                let result = 0;
                this.dataSourceGroupFoodTotalPrice.data.forEach((item) => {
                    result +=
                        this.totalCostOfThreeMeal(
                            item.chiTietGaoTienAn,
                            'total',
                        ) +
                        this.totalCostOfThreeMeal(item.chiTietAnThem, 'extra');

                    arrayTongThu[0] += this.sum(
                        item.chiTietGaoTienAn,
                        'tongTienAn',
                    );

                    item.chiTietGaoTienAn.forEach((chiTiet) => {
                        if (chiTiet.buoi === 0) {
                            arrayTongThu[1] += chiTiet.tongTienAn;
                        } else if (chiTiet.buoi === 1) {
                            arrayTongThu[2] += chiTiet.tongTienAn;
                        } else if (chiTiet.buoi === 2) {
                            arrayTongThu[3] += chiTiet.tongTienAn;
                            arrayTongThu[4] += chiTiet.tongTienAn;
                        }
                    });
                });

                arrayTongThu[0] = arrayTongThu[0] * 0.03;

                for (let i = 0; i < arrayTongThu.length; i++) {
                    for (let j = 0; j < this.danhSachMonAn.length; j++) {
                        if (i === j) {
                            this.danhSachMonAn[j].tongThu = arrayTongThu[i];
                        }
                    }
                }

                setTimeout(() => {
                    for (let i = 0; i < this.danhSachMonAn.length; i++) {
                        this.danhSachMonAn[i].danhSach.paginator =
                            this.paginator.toArray()[i];
                    }
                });

                this.isFinishedGetData = true;
                this.isFinishedGetDataMenu = true;
                this.isFinishedGetDataMoney = true;

                this.calculateData();
            });
    }

    // Handle Input Filter
    handleInputFilter(filterValue) {
        this.danhSachMonAn[this.currentOpenedItemId].danhSach.filter =
            filterValue.toLowerCase();
    }

    /** Whether the number of selected elements matches the total number of rows. */
    isAllSelected() {
        const numSelected = this.selection.selected.length;
        const numRows =
            this.danhSachMonAn[this.tabSelected.value].danhSach.data.length;
        return numSelected === numRows;
    }

    /** Selects all rows if they are not all selected; otherwise clear selection. */
    toggleAllRows() {
        if (this.isAllSelected()) {
            this.selection.clear();
            return;
        }

        this.selection.select(
            ...this.danhSachMonAn[this.tabSelected.value].danhSach.data,
        );
    }

    /** The label for the checkbox on the passed row */
    checkboxLabel(row?: any): string {
        if (!row) {
            return `${this.isAllSelected() ? 'deselect' : 'select'} all`;
        }
        return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${
            row.index + 1
        }`;
    }

    handleSelectionTable() {
        let tempSource = [...this.selection.selected];
        let mainSource =
            this.danhSachMonAn[this.tabSelected.value].danhSach.data;

        const dialogRef = this.dialog.open(DeleteConfirmDialogComponent, {
            width: '500px',
            data: tempSource,
        });

        dialogRef.componentInstance.handleDelete.subscribe((dataDelete) => {
            const isSubmitted = !!dataDelete;
            dialogRef.close(isSubmitted);
        });

        dialogRef.afterClosed().subscribe((result) => {
            if (!result) return;

            for (let index = 0; index < tempSource.length; index++) {
                const element = tempSource[index];
                let indexFromTemp = mainSource.findIndex(
                    (item) => item.lttpChatDotId === element.lttpChatDotId,
                );

                mainSource.splice(indexFromTemp, 1);
                this.danhSachMonAn[this.tabSelected.value].danhSach.data =
                    mainSource;
            }
            this.selection.clear();
            this._toastrService.success('', 'Xóa thành công');
        });
    }

    getUserNameColumnFoodUpgrade(column) {
        let nameColumn = {
            name: 'Tên LTTP, chất đốt',
            unit: 'ĐVT',
            unitPrice: 'Đơn giá',
            store: 'Kho',
            currentTotal: 'Hiện có',
            expTotal: 'Dự xuất',
            totalMoney: 'Thành tiền',
            actions: 'Thao tác',
        };
        if (!(column in nameColumn)) return column;
        return nameColumn[column];
    }

    getUserNameColumnBoxPrice(column, type) {
        let nameColumn;
        if (type === 'price') {
            nameColumn = {
                buoi: 'Buổi ăn',
                quanSo: 'Số lượng ăn',
                mucTienAn: 'Đơn giá',
                tongTienAn: 'Thành tiền',
            };
        } else if (type === 'extra') {
            nameColumn = {
                loai: 'Loại ăn thêm',
                quanSo: 'Số lượng ăn',
                mucTienAnThem: 'Đơn giá',
                tongTienAnThem: 'Thành tiền',
            };
        } else if (type === 'fuel') {
            nameColumn = {
                buoi: 'Buổi ăn',
                quanSo: 'Số lượng ăn',
                dinhLuong: 'Định lượng',
                tong: 'Tổng',
            };
        } else if (type === 'food') {
            nameColumn = {
                buoi: 'Buổi ăn',
                quanSo: 'Số lượng ăn',
                dinhLuongGao: 'Định lượng',
                tongGao: 'Tổng',
            };
        }

        if (!(column in nameColumn)) return column;
        return nameColumn[column];
    }

    // Add row
    addRow(indexTab: any): void {
        const dialogRef = this.dialog.open(MaterialDialogComponent, {
            // disableClose: true,

            width: '70%',
            data: {
                type: 'ADJUSTMENT_PURCHASE_UPGRADE',
                khoXuatId: this.warehouseExportId,
                currentTable: this.currentTable,
                isShow: false,
            },
            panelClass: 'add-field-dialog-class',
        });

        dialogRef.componentInstance.onSave.subscribe((data) => {
            const isSubmitted = !!data;

            let dataTable = data.map((item) => ({
                lttpChatDotId: item.lttpChatDotId,
                tenLttpChatDot: this.convertNameById(
                    item.lttpChatDotId,
                    'FoodFuel',
                ),
                donViTinh: item.donViTinh,
                khoId: '',
                donGia: item.donGia,
                hienCo: 0,
                duXuat: item.soLuong,
                thanhTien: item.soLuong * item.donGia,
                soLuongTrenBdMua: item.nguonCungCap === 1 ? item.soLuong : '',
                soLuongTrenBdTg: item.nguonCungCap === 2 ? item.soLuong : '',
                soLuongDonViBdMua: item.nguonCungCap === 3 ? item.soLuong : '',
                soLuongDonViBdTg: item.nguonCungCap === 4 ? item.soLuong : '',
                thanhTienDuXuat: item.soLuong * item.donGia,
                typePriceImport: 0,
                type: 'other',
            }));

            this.danhSachMonAn[indexTab].danhSach.data = [
                ...dataTable,
                ...this.danhSachMonAn[indexTab].danhSach.data,
            ];

            this.danhSachMonAn[indexTab].danhSach.paginator =
                this.paginator.toArray()[indexTab];
            this.calculateData();

            dialogRef.close(isSubmitted);
        });

        dialogRef.afterClosed().subscribe((result) => {
            if (!result) return;
            this._toastrService.success('', 'Thêm thành công');
        });
    }

    // Delete row
    deleteAction(element: any, indexTab: any, indexElement: any): void {
        const dialogRef = this.dialog.open(DeleteConfirmDialogComponent, {
            width: '500px',
            data: element,
        });

        dialogRef.componentInstance.handleDelete.subscribe((dataDelete) => {
            const isSubmitted = !!dataDelete;
            dialogRef.close(isSubmitted);
        });

        dialogRef.afterClosed().subscribe((result) => {
            if (!result) return;

            const index: number =
                this.danhSachMonAn[indexTab].danhSach.data.indexOf(element);

            if (index !== -1) {
                this.danhSachMonAn[indexTab].danhSach.data.splice(index, 1);
            }

            this.danhSachMonAn[indexTab].danhSach.paginator =
                this.paginator.toArray()[indexTab];

            this.calculateData();

            this._toastrService.success('', 'Xóa thành công');
        });
    }

    // Back to list
    onBack(): void {
        this._router.navigate(['app/quan-li-chi-an/du-chi-ngay-ke-tiep']);
    }

    // Submit data
    onSubmit(isPublished: boolean): void {
        let mealNotFood = [];
        let chiTietDuXuat = [];

        let dataSubmit = this.danhSachMonAn.map((item) => ({
            buoi: item.buoiAn,
            danhSach: item.danhSach.data.map((data) => ({
                ...data,
                soLuongDonViBdMua: data.soLuongDonViBdMua
                    ? data.soLuongDonViBdMua
                    : 0,
                soLuongDonViBdTg: data.soLuongDonViBdTg
                    ? data.soLuongDonViBdTg
                    : 0,
                soLuongTrenBdMua: data.soLuongTrenBdMua
                    ? data.soLuongTrenBdMua
                    : 0,
                soLuongTrenBdTg: data.soLuongTrenBdTg
                    ? data.soLuongTrenBdTg
                    : 0,
            })),
        }));

        if (this.isEdit === false) {
            this.dataSourceFood.data.forEach((food) => {
                this.dataSourceGroupFoodTotalPrice.data.forEach((price) => {
                    if (price.mucTien === food.mucTien) {
                        chiTietDuXuat.push({
                            mucTien: price.mucTien,
                            chiTietAnThem: price.chiTietAnThem,
                            chiTietGaoTienAn: food.chiTietGaoTienAn.map(
                                (item, i) =>
                                    Object.assign({
                                        buoi: item.buoi,
                                        dinhLuongGao: item.dinhLuongGao,
                                        tongGao: item.tongGao,
                                        quanSo: price.chiTietGaoTienAn[i]
                                            .quanSo,
                                        mucTienAn:
                                            price.chiTietGaoTienAn[i].mucTienAn,
                                        tongTienAn:
                                            price.chiTietGaoTienAn[i]
                                                .tongTienAn,
                                    }),
                            ),
                        });
                    }
                });
            });
        }

        for (const obj of dataSubmit) {
            for (const item of obj.danhSach) {
                if (item.duXuat === 0) {
                    mealNotFood.push(obj.buoi);
                }
            }
        }

        let result = mealNotFood.filter(function (elem, index, self) {
            return index === self.indexOf(elem);
        });

        let nameOfMeal = this.checkMealNotInput(result);

        if (nameOfMeal.length > 0) {
            this._toastrService.error(
                '',
                'Chưa nhập dự xuất vật tư buổi ' + nameOfMeal,
            );
        } else {
            let bodySubmit = {
                ...this.dataEdit,
                ...this.form.value,
                ...this.formLTTP.value,
                isPublished: isPublished,
                chiTietDanhSachLttp: dataSubmit,
                chiTietDuXuat: this.isEdit
                    ? this.dataEdit.chiTietDuXuat
                    : chiTietDuXuat,
                chiTietChatDot: this.dataSourceFuel.data,
                dauMoiBepId: this._dataService.getDauMoiBepId(),
                truyLinh: 0,
                tongTien: this.statisticals[1].value,
                congChi: this.statisticals[4].value,
                thuaThieuHomTruoc: this.statisticals[3].value,
                thuaThieuTrongNgay: this.statisticals[5].value,
                daSuDung: this.statisticals[2].value,
                danhSachDotQuanSoAn: this.form.value.danhSachDotQuanSoAn,
                quanSoBinhQuan: this.statisticals[0].value,
            };

            if (this.isEdit) {
                this._commonService
                    .callDataAPIShort(
                        '/api/services/write/DuXuat/Update',
                        bodySubmit,
                    )
                    .subscribe(
                        (response) => {
                            this._router.navigate([
                                'app/quan-li-chi-an/du-chi-ngay-ke-tiep',
                            ]);
                            this._toastrService.success(
                                '',
                                'Cập nhật thành công',
                            );
                        },
                        (err) => this._toastrService.errorServer(err),
                    );
            } else {
                this._commonService
                    .callDataAPIShort(
                        '/api/services/write/DuXuat/Create',
                        bodySubmit,
                    )
                    .subscribe(
                        (response) => {
                            this._router.navigate([
                                'app/quan-li-chi-an/du-chi-ngay-ke-tiep',
                            ]);
                            this._toastrService.success('', 'Tạo thành công');
                        },
                        (err) => this._toastrService.errorServer(err),
                    );
            }
        }
    }

    checkMealNotInput(input: unknown[] | string) {
        const inputArray = Array.isArray(input) ? input : null;

        if (inputArray) {
            return inputArray.map((str) => ' ' + str);
        }

        return input;
    }

    exportExcel() {
        let chiTietDuXuat = [];
        let ngayDuXuat = this.isEdit
            ? this.datePipe.transform(this.dataEdit.ngayDuXuat, 'dd-MM-yyyy')
            : this.datePipe.transform(this.form.value.ngayDuXuat, 'dd-MM-yyyy');
        let dataSubmit = this.danhSachMonAn.map((item) => ({
            buoi: item.buoiAn,
            danhSach: item.danhSach.data,
        }));

        this.dataSourceFood.data.forEach((food) => {
            this.dataSourceGroupFoodTotalPrice.data.forEach((price) => {
                if (price.mucTien === food.mucTien) {
                    chiTietDuXuat.push({
                        mucTien: price.mucTien,
                        chiTietAnThem: price.chiTietAnThem,
                        chiTietGaoTienAn: food.chiTietGaoTienAn.map((item, i) =>
                            Object.assign({
                                buoi: item.buoi,
                                dinhLuongGao: item.dinhLuongGao,
                                tongGao: item.tongGao,
                                quanSo: price.chiTietGaoTienAn[i].quanSo,
                                mucTienAn: price.chiTietGaoTienAn[i].mucTienAn,
                                tongTienAn:
                                    price.chiTietGaoTienAn[i].tongTienAn,
                            }),
                        ),
                    });
                }
            });
        });

        let bodyDataExcel =
            this.isEdit === true
                ? {
                      ...this.dataEdit,
                      ngayDuXuat: this.dataEdit.ngayDuXuat,
                      chiTietDuXuat: this.dataEdit.chiTietDuXuat,
                      chiTietDanhSachLttp: this.dataEdit.chiTietDanhSachLttp,
                  }
                : {
                      ngayDuXuat: this.form.value.ngayDuXuat,
                      chiTietDuXuat: chiTietDuXuat,
                      chiTietDanhSachLttp: dataSubmit,
                      tongTien: this.statisticals[1].value,
                      congChi: this.statisticals[4].value,
                      thuaThieuHomTruoc: this.statisticals[3].value,
                      thuaThieuTrongNgay: this.statisticals[5].value,
                      daSuDung: this.statisticals[2].value,
                  };

        this._exportService.generateDuXuatExcel(
            bodyDataExcel,
            `DuXuat ${ngayDuXuat}`,
        );
    }

    renderColumnelement(element) {
        if (!element) return element;
        let listColumn = {
            lttpChatDot: 'Tên LTTP',
            donViTinh: 'Đơn vị tính',
            actions: 'Thao tác',
        };

        return listColumn[element];
    }

    renderColumnelementChild(element) {
        let listColumn = {
            tenLttpChatDot: 'Thực phẩm',
            donViTinh: 'ĐVT',
            donGia: 'Giá',
            nhuCau: 'Nhu cầu  ',
            nhuCauTT: 'Nhu cầu TT ',
            hienCo: 'Hiện có',
            hienCoTT: 'Hiện có - TT',
            soluongTonKho: 'SL',
            thanhTien: 'Thành tiền',
        };

        if (!element || !listColumn[element]) return element;

        return listColumn[element];
    }

    renderToText(column) {
        let result = '';
        switch (column) {
            case 'tenLttpChatDot':
                result = 'Tên thực phẩm';
                break;
            case 'donViTinh':
                result = 'ĐVT';
                break;
            case 'donGia':
                result = 'Đơn giá';
                break;
            case 'hienCo':
                result = 'Hiện có';
                break;
            case 'actions':
                result = 'Thao tác';
                break;
        }
        return result;
    }

    handleOpened(index): void {
        this.currentOpenedItemId = index;
    }
}
