import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuListAddEditComponent } from './menu-list-add-edit.component';

describe('MenuListAddEditComponent', () => {
  let component: MenuListAddEditComponent;
  let fixture: ComponentFixture<MenuListAddEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenuListAddEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuListAddEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
