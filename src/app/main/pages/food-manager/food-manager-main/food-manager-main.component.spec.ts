import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FoodManagerMainComponent } from './food-manager-main.component';

describe('FoodManagerMainComponent', () => {
  let component: FoodManagerMainComponent;
  let fixture: ComponentFixture<FoodManagerMainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FoodManagerMainComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FoodManagerMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
