import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MaintenancePureStoreComponent } from './maintenance-pure-store.component';

describe('MaintenancePureStoreComponent', () => {
  let component: MaintenancePureStoreComponent;
  let fixture: ComponentFixture<MaintenancePureStoreComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MaintenancePureStoreComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MaintenancePureStoreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
