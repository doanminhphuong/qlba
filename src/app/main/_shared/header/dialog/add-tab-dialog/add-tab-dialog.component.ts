import { Component, Inject, OnInit } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";

export interface DialogData {
    name: string;
}

@Component({
    selector: "app-add-tab-dialog",
    templateUrl: "./add-tab-dialog.component.html",
    styleUrls: ["./add-tab-dialog.component.scss"]
})
export class AddTabDialogComponent implements OnInit {
    constructor(
        public dialogRef: MatDialogRef<AddTabDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: DialogData
    ) { }

    ngOnInit() { }

    onNoClick(): void {
        this.dialogRef.close("");
    }
}
