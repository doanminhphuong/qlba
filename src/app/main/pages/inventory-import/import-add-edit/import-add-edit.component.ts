import { SelectionModel } from '@angular/cdk/collections';
import {
    AfterViewInit,
    ChangeDetectorRef,
    Component,
    OnInit,
    ViewChild,
} from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatPaginator, MatTableDataSource } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { DeleteConfirmDialogComponent } from '@app/_shared/dialogs/delete-confirm-dialog/delete-confirm-dialog.component';
import { MaterialDialogComponent } from '@app/_shared/dialogs/material-dialog/material-dialog.component';
import { FoodFuelService } from '@core/services/food-fuel.service';
import { PriceService } from '@core/services/price.service';
import { ToastrService } from '@core/services/toastr.service';
import { Subject, Subscription } from 'rxjs';
import {
    debounceTime,
    distinctUntilChanged,
    map,
    pairwise,
} from 'rxjs/operators';
import { forkJoin, Observable } from 'rxjs';
import { InventoryService } from '@core/services/inventory.service';
import { CategoryService } from '@core/services/category.service';
import { UserService } from '@core/services/user.service';
import {
    MomentDateAdapter,
    MAT_MOMENT_DATE_ADAPTER_OPTIONS,
} from '@angular/material-moment-adapter';
import {
    DateAdapter,
    MAT_DATE_FORMATS,
    MAT_DATE_LOCALE,
} from '@angular/material/core';
import { DialogRejectStatusComponent } from '@app/_shared/dialogs/dialog-reject-status/dialog-reject-status.component';
import { CommonService } from '@core/services/common.service';
import { ExportExcelService } from '@core/services/export-excel.service';
import * as moment from 'moment';
import { DatePipe } from '@angular/common';
import * as _ from 'lodash';
import { DataService } from '@core/services/data-service';

export const MY_FORMATS = {
    parse: {
        dateInput: 'DD/MM/YYYY', // this is how your date will be parsed from Input
    },
    display: {
        dateInput: 'DD/MM/YYYY', // this is how your date will get displayed on the Input
        monthYearLabel: 'MMMM YYYY',
        dateA11yLabel: 'LL',
        monthYearA11yLabel: 'MMMM YYYY',
    },
};

@Component({
    selector: 'app-import-add-edit',
    templateUrl: './import-add-edit-v2.component.html',
    styleUrls: ['./import-add-edit.component.scss'],
    providers: [
        {
            provide: DateAdapter,
            useClass: MomentDateAdapter,
            deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS],
        },

        { provide: MAT_DATE_LOCALE, useValue: 'vi-VI' },
        { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
        {
            provide: MAT_MOMENT_DATE_ADAPTER_OPTIONS,
            useValue: { useUtc: true },
        },
        DatePipe,
    ],
})
export class ImportAddEditComponent implements OnInit {
    public applyFilter = new Subject<KeyboardEvent>();
    private subscription: Subscription;
    @ViewChild(MatPaginator) paginator!: MatPaginator;
    currentDate: Date = new Date();

    titleOnEdit: string;

    form: FormGroup;
    fields: any[] = [];
    fieldsClone: any[] = [];

    displayedColumns: string[] = [
        'maLttpChatDot',
        'tenLttpChatDot',
        'soLuongPhaiNhap',
        'soLuongThucNhap',
        'donViTinh',
        'donGia',
        'thanhTien',
        'actions',
    ];

    dataSource = new MatTableDataSource<any>([]);
    selection = new SelectionModel<any>(true, []);

    foodFuelList: any[] = [];

    recentlyTable: any;
    currentTable: any[] = [];
    inventoryImportId: any;
    dataEdit: any;
    isEdit: boolean = false;
    typeAction: number | string;
    isHasAdjustmentBefore: boolean = false;
    isExportData: boolean = false;
    isStatusImportSotre: boolean = false;

    listPhieuXuatKhoHoacMuaId: any[] = [];

    warehouseList: any[] = [];
    purchaseList: any[] = [];

    tempkhoNhanId: number = 0;
    tempPhieuXuatKhoHoacMuaId;
    importExportData: any[] = [];
    exportStorage: any[] = [];
    userList: any[] = [];
    shipperList: any[] = [];
    priceAdjustmentList: any[] = [];
    supplierOptions: any[] = [];
    totalPrice: number = 0;
    toggleLayoutHidden: boolean = false;
    isActiveExportExcel: boolean = false;
    priceAdjustmentId: number = 0;

    cacheId: any[] = [];

    constructor(
        private readonly changeDetectorRef: ChangeDetectorRef,
        private _activatedRoute: ActivatedRoute,
        private _toastrService: ToastrService,
        private _router: Router,
        public dialog: MatDialog,
        private _priceService: PriceService,
        private _foodFuelService: FoodFuelService,
        private _inventoryService: InventoryService,
        private _categoryService: CategoryService,
        private _userService: UserService,
        private _commonService: CommonService,
        private _exportService: ExportExcelService,
        private datePipe: DatePipe,
        private _dataService: DataService,
    ) {
        this.subscription = this.applyFilter
            .pipe(
                map((event) => (event.target as HTMLInputElement).value),
                debounceTime(500),
                distinctUntilChanged(),
            )
            .subscribe((filterValue) => this.handleInputFilter(filterValue));

        this.subscription = this._dataService.getData().subscribe((message) => {
            if (this.isEdit === false) {
                this._dataService.changeDauMoiBep(message.id);
                this.getInitData(message.id);
            }
        });
    }

    ngOnInit() {
        // Check if this is Create or Edit page
        this._activatedRoute.paramMap.subscribe((param) => {
            this.inventoryImportId = +param.get('id');
            this.typeAction = +param.get('typeAction');

            if (this.typeAction === 1 || this.typeAction === 3) {
                this.displayedColumns.splice(7, 1);
            }
            if (!this.inventoryImportId) return;
            this.isEdit = true;
        });

        this.getShipperOptions();
        this.getAllFoodFuel();
        setTimeout(() => {
            this.getInitData(this._dataService.getDauMoiBepId());
        }, 1000);
    }

    ngAfterViewChecked(): void {
        this.changeDetectorRef.detectChanges();
    }

    ngAfterViewInit(): void {}

    getBodyCategory(groupCode: string) {
        return {
            maxResultCount: 10,
            skipCount: 0,
            sorting: 'Code',
            criterias: [
                {
                    propertyName: 'GroupCode',
                    operation: 6,
                    value: groupCode,
                },
            ],
        };
    }

    getShipperOptions() {
        this._categoryService
            .getAllCategory(this.getBodyCategory('nha-cung-cap'))
            .subscribe((res) => {
                this.shipperList = res.result.map((item) => ({
                    key: item.codeData,
                    name: item.name,
                    type: item.number1,
                }));
            });
    }

    getInitData(id: number): void {
        const valueData = {
            size: '9999',
            page: '1',
            donViId: '266a601d-c450-4caf-8c0f-ff9863fbcf36',
        };

        const bodySubmit = {
            code: 'Users',
            valueData: JSON.stringify(valueData),
        };

        const dataWarehouse = this._inventoryService.getAllWarehouse({
            maxResultCount: 999999999,
            criterias: [
                {
                    propertyName: 'dauMoiBepId',
                    operation: 0,
                    value: id,
                },
            ],
        });
        const dataPurchase = this._priceService.getAllPurchase({
            maxResultCount: 2147483647,
        });
        const dataUser = this._userService.getDataSearch(bodySubmit);
        const exportStorageData = this._commonService.callDataAPIShort(
            `/api/services/read/PhieuMuaHang/GetListNotUsed?dauMoiBepId=${this._dataService.getDauMoiBepId()}`,
            null,
        );
        const dataPriceAdjust = this._priceService.getAllPriceAdjustment({});
        const dataPriceAdjustCurrent = this._commonService.callDataAPIShort(
            '/api/services/read/DotDieuChinhGia/GetCurrent',
            {},
        );
        forkJoin([
            dataWarehouse,
            dataPurchase,
            dataUser,
            exportStorageData,
            dataPriceAdjust,
            dataPriceAdjustCurrent,
        ]).subscribe((results) => {
            this.warehouseList = results[0].result.items
                .filter((item) => item.status === 'ENABLE')
                .map((item) => ({
                    key: item.id,
                    name: item.tenKho,
                }));

            if (this.isEdit === true) {
                if (this.typeAction === 3) {
                    this.purchaseList = results[1].result.items
                        .filter((item) => item.trangThai === 3)
                        .map((item) => ({
                            key: item.id,
                            name:
                                item.maPhieuMuaHang +
                                ' - ' +
                                this.transformDate(item.ngayMua),
                        }));
                } else {
                    this.purchaseList = results[1].result.items
                        .filter((item) => item.trangThai === 0)
                        .map((item) => ({
                            key: item.id,
                            name:
                                item.maPhieuMuaHang +
                                ' - ' +
                                this.transformDate(item.ngayMua),
                        }));
                }
            } else {
                this.purchaseList = results[1].result.items
                    .filter((item) => item.trangThai === 0)
                    .map((item) => ({
                        key: item.id,
                        name:
                            item.maPhieuMuaHang +
                            ' - ' +
                            this.transformDate(item.ngayMua),
                    }));
            }

            this.priceAdjustmentList = results[4].result.items
                .filter((item) => item.status === 'ENABLE')
                .map((item) => ({
                    key: item.id,
                    name: item.tenDotDieuChinhGia,
                }));

            this.priceAdjustmentId = results[5].result.id;

            this.fields = [
                {
                    type: 'DATETIME',
                    referenceValue: 'ngayNhapKho',
                    name: 'Ngày nhận hàng',
                    defaultValue: '',
                    required: '1',
                    disabled: this.typeAction === 1 || this.typeAction === 3,
                    css: 'col-12 col-lg-4',
                    appearance: 'legacy',
                    min: new Date(),
                },

                {
                    type: 'DATETIME',
                    referenceValue: 'ngayHetHan',
                    name: 'Ngày hết hạn',
                    defaultValue: '',
                    required: '1',
                    css: 'col-12 col-lg-4',
                    appearance: 'legacy',
                    dependenField: 'ngayNhapKho',
                    disabled: true,
                },
                {
                    type: 'TEXT',
                    referenceValue: 'maPhieuNhapKho',
                    name: 'Mã phiếu nhập',
                    defaultValue: '',
                    required: '1',
                    disabled: this.typeAction === 1 || this.typeAction === 3,
                    css: 'col-12 col-lg-4',
                    appearance: 'legacy',
                },
                {
                    type: 'SELECT',
                    referenceValue: 'khoNhanId',
                    name: 'Chọn kho nhận hàng',
                    defaultValue: '',
                    options: [...this.warehouseList],
                    required: '1',
                    disabled: this.typeAction === 1 || this.typeAction === 3,
                    css: 'col-12 col-lg-4',
                    appearance: 'legacy',
                },
                {
                    type: 'SELECT',
                    referenceValue: 'dotDieuChinhGiaId',
                    name: 'Đợt điều chỉnh giá',
                    defaultValue: this.priceAdjustmentId,
                    options: [...this.priceAdjustmentList],
                    required: '1',
                    disabled: this.typeAction === 1 || this.typeAction === 3,
                    css: 'col-12 col-lg-4',
                    appearance: 'legacy',
                },
                {
                    type: 'SELECT',
                    referenceValue: 'maNguonCungCap',
                    name: 'Nguồn cung cấp',
                    defaultValue: '',
                    options: [...this.shipperList],
                    required: '1',
                    disabled: true,
                    css: 'col-12 col-lg-4',
                    appearance: 'legacy',
                    dependenField: 'khoNhanId',
                    selectionChange: (e) => this.handleTypeSelectPurchase(e),
                },
                {
                    type: 'TEXT',
                    referenceValue: 'nguoiNhap',
                    name: 'Người viết phiếu',
                    defaultValue: this._dataService.getNguoiVietPhieu(),
                    // options: [...this.userList],
                    required: '0',
                    search: '1',
                    searchCtrl: 'searchCtrl',
                    disabled: this.typeAction === 1 || this.typeAction === 3,
                    css: 'col-12 col-lg-4',
                    appearance: 'legacy',
                },
                {
                    type: 'TEXT',
                    referenceValue: 'nguoiNhan',
                    name: 'Người nhận',
                    defaultValue: this._dataService.getNguoiNhan(),
                    // options: [...this.userList],
                    required: '0',
                    search: '1',
                    searchCtrl: 'searchCtrl',
                    disabled: this.typeAction === 1 || this.typeAction === 3,
                    css: 'col-12 col-lg-4',
                    appearance: 'legacy',
                },
                {
                    type: 'MULTISELECT',
                    referenceValue: 'phieuXuatNhapHangId',
                    name: 'Phiếu nhập xuất hàng',
                    defaultValue: '',
                    options: [...this.purchaseList],
                    required: '0',
                    disabled: true,
                    css: 'col-12 col-lg-4',
                    appearance: 'legacy',
                    dependenField: 'maNguonCungCap',
                    selectionChange: (e) => this.handleSelectPurchase(e),
                },
                {
                    type: 'TEXTAREA',
                    referenceValue: 'ghiChu',
                    name: 'Ghi chú',
                    defaultValue: '',
                    required: '0',
                    disabled: this.typeAction === 1 || this.typeAction === 3,
                    css: 'col-12 col-lg-12',
                    appearance: 'legacy',
                },
            ];

            this.getInitForm();
        });

        this.dataSource.paginator = this.paginator;
    }

    getInitForm(): void {
        let fieldsCtrls = {};
        let fieldList = [];

        let mainFields = !this.toggleLayoutHidden
            ? this.fields
            : this.fieldsClone;
        for (let f of mainFields) {
            fieldList = [...fieldList, f];

            if (f.type === 'NUMBER') {
                fieldsCtrls[f.referenceValue] = new FormControl(
                    { value: f.defaultValue, disabled: f.disabled },
                    f.required === '1' ? [Validators.required] : [],
                );
            } else if (f.type !== 'CHECKBOX') {
                let validators = [
                    Validators.minLength(f.minLength),
                    Validators.maxLength(f.maxLength),
                    Validators.pattern(f.pattern),
                ];

                if (f.type === 'EMAIL') {
                    validators = [...validators, Validators.email];
                }

                fieldsCtrls[f.referenceValue] = new FormControl(
                    {
                        value: f.defaultValue || '',
                        disabled: f.disabled,
                    },

                    f.required === '1'
                        ? [...validators, Validators.required]
                        : [...validators],
                );

                if (f.type === 'DATETIME') {
                    if (f.referenceValue === 'ngayNhapKho') {
                        fieldsCtrls[f.referenceValue].valueChanges.subscribe(
                            (value) => {
                                const KEY = 'PNK';
                                if (this.isEdit && !this.isStatusImportSotre) {
                                    this.isStatusImportSotre = true;
                                    return;
                                }

                                let date = new Date(value);
                                let part3 = date
                                    .toLocaleDateString('en-GB')
                                    .replace(/[^0-9]/g, '');
                                let part4;

                                if (localStorage.getItem('maNK') === null) {
                                    localStorage.setItem(
                                        'maNK',
                                        JSON.stringify({
                                            currentDate: new Date()
                                                .toISOString()
                                                .slice(0, 10),
                                            index: 1,
                                        }),
                                    );

                                    let dataLocal = JSON.parse(
                                        localStorage.getItem('maNK'),
                                    );

                                    part4 = this.pad(dataLocal.index);
                                } else {
                                    let currentDate = new Date()
                                        .toISOString()
                                        .slice(0, 10);

                                    let dataLocal = JSON.parse(
                                        localStorage.getItem('maNK'),
                                    );

                                    if (currentDate === dataLocal.currentDate) {
                                        part4 = this.pad(dataLocal.index);
                                    } else {
                                        localStorage.setItem(
                                            'maNK',
                                            JSON.stringify({
                                                currentDate: new Date()
                                                    .toISOString()
                                                    .slice(0, 10),
                                                index: 1,
                                            }),
                                        );

                                        let dataLocal = JSON.parse(
                                            localStorage.getItem('maNK'),
                                        );

                                        part4 = this.pad(dataLocal.index);
                                    }
                                }

                                let fullPath = KEY + '.' + part3 + '.' + part4;
                                this.form.patchValue({
                                    maPhieuNhapKho: fullPath,
                                });

                                const dependField = fieldList.find(
                                    (x) => x.dependenField === f.referenceValue,
                                );

                                if (!dependField) return;

                                dependField.min = value;

                                if (this.typeAction === 3) {
                                    fieldsCtrls[
                                        dependField.referenceValue
                                    ].disable();
                                } else {
                                    fieldsCtrls[
                                        dependField.referenceValue
                                    ].enable();
                                }
                            },
                        );
                    }

                    fieldsCtrls[f.referenceValue].valueChanges.subscribe(
                        (val) => {
                            const dependField = fieldList.find(
                                (x) => x.dependenField === f.referenceValue,
                            );

                            if (!dependField) return;

                            dependField.min = val;
                            if (
                                this.typeAction === 1 ||
                                this.typeAction === 3
                            ) {
                                fieldsCtrls[
                                    dependField.referenceValue
                                ].disable();
                            } else {
                                fieldsCtrls[
                                    dependField.referenceValue
                                ].enable();
                            }
                        },
                    );
                }
                if (f.type === 'SELECT') {
                    if (f.referenceValue === 'khoNhanId') {
                        fieldsCtrls[f.referenceValue].valueChanges.subscribe(
                            (value) => {
                                this.tempkhoNhanId = value;

                                const dependField = fieldList.find(
                                    (field) =>
                                        field.dependenField ===
                                        f.referenceValue,
                                );

                                if (!dependField) return;

                                if (
                                    this.typeAction !== 1 &&
                                    this.typeAction !== 3
                                ) {
                                    fieldsCtrls[
                                        dependField.referenceValue
                                    ].enable();
                                }
                            },
                        );
                    }

                    if (f.referenceValue === 'maNguonCungCap') {
                        fieldsCtrls[f.referenceValue].valueChanges.subscribe(
                            (value) => {
                                if (value !== '') {
                                    const dependField = fieldList.find(
                                        (x) =>
                                            x.dependenField ===
                                            f.referenceValue,
                                    );

                                    if (!dependField) return;

                                    let nhaCungCap = this.shipperList.find(
                                        (shipper) => shipper.key === value,
                                    );

                                    if (nhaCungCap && nhaCungCap.type === 1) {
                                        dependField.name =
                                            'Phiếu xuất hàng từ ' +
                                            nhaCungCap.name;
                                        this.isExportData = true;

                                        fieldsCtrls[
                                            dependField.referenceValue
                                        ].disable();
                                    } else if (
                                        nhaCungCap &&
                                        nhaCungCap.type === 0
                                    ) {
                                        this.isExportData = false;
                                        dependField.name =
                                            'Phiếu mua hàng từ ' +
                                            nhaCungCap.name;

                                        if (this.typeAction === 3) {
                                            fieldsCtrls[
                                                dependField.referenceValue
                                            ].disable();
                                        } else {
                                            fieldsCtrls[
                                                dependField.referenceValue
                                            ].enable();
                                        }
                                    }
                                }
                            },
                        );
                    }
                }
            } else {
                //if checkbox, it need multiple
                let opts = {};
                for (let opt of f.options) {
                    opts[opt.key] = new FormControl({
                        value: opt.value,
                        disabled: f.disabled,
                    });
                }
                fieldsCtrls[f.referenceValue] = new FormGroup(opts);
            }
        }

        this.form = new FormGroup(fieldsCtrls);

        if (this.isEdit) {
            this.getDataEdit();
        }
    }

    pad(d) {
        return d < 10 ? '0' + d.toString() : d.toString();
    }

    transformDate(date) {
        return this.datePipe.transform(date, 'dd-MM-yyyy');
    }

    addData(event, element) {
        element.thanhTien = +event * element.donGia;

        this.totalPrice = this.dataSource.data.reduce(
            (acc, item) => acc + item.thanhTien,
            0,
        );
    }

    get phieuXuatNhapHangId() {
        if (this.form && this.form.get('phieuXuatNhapHangId')) {
            return this.form.get('phieuXuatNhapHangId');
        } else {
            return null;
        }
    }

    getDataEdit(): void {
        const dataEditId = {
            id: this.inventoryImportId,
        };

        this._inventoryService
            .getInventoryImportById(dataEditId)
            .subscribe((res) => {
                this.dataEdit = res.result;
                this.tempPhieuXuatKhoHoacMuaId =
                    this.dataEdit && this.dataEdit.phieuXuatKhoId
                        ? this.dataEdit.phieuXuatKhoId
                        : this.dataEdit.phieuMuaHangId;

                switch (this.dataEdit.trangThai) {
                    case 0:
                        this.titleOnEdit = `Chỉnh sửa phiếu nhập kho '${this.dataEdit.maPhieuNhapKho}'`;
                        break;
                    case 3:
                        this.titleOnEdit = `Chi tiết phiếu nhập kho '${this.dataEdit.maPhieuNhapKho}'`;
                        break;
                }

                // Patch data to Form
                this.form.patchValue({
                    ...this.dataEdit,
                    phieuXuatNhapHangId:
                        this.tempPhieuXuatKhoHoacMuaId !== ''
                            ? JSON.parse(this.tempPhieuXuatKhoHoacMuaId)
                            : [],
                });

                // Patch data to Table (Details)
                this.currentTable = this.dataEdit.chiTiet.map((item) => ({
                    ...item,
                    ...item.lttpChatDot,
                }));

                // Remove id on each item
                this.currentTable.forEach((item) => {
                    delete item.id;
                });

                this.dataSource.data = this.currentTable;

                this.totalPrice = this.dataEdit.tongTien;
            });
    }

    getAllFoodFuel(): void {
        this._foodFuelService
            .getAllFoodFuel({})
            .subscribe((res) => (this.foodFuelList = res.result.items));
    }

    // Handle Input Filter
    handleInputFilter(filterValue) {
        this.dataSource.data = this.currentTable.filter((item) =>
            item.tenLttpChatDot
                .toLowerCase()
                .includes(filterValue.trim().toLowerCase()),
        );
    }

    handleTypeSelectPurchase(event) {
        this.form.controls['phieuXuatNhapHangId'].reset(null);
        this.dataSource.data = [];
        this.currentTable = [];
    }

    // Handle Select PMH
    handleSelectPurchase(event: any) {
        let purchaseId = event.value;
        this.dataSource.data = [];
        this.currentTable = [];
        purchaseId.forEach((item) => {
            this._priceService
                .getPurchaseById({ id: item })
                .subscribe((res) => {
                    this.currentTable = [
                        ...this.currentTable,
                        ...res.result.chiTiet.map((item) => ({
                            ...item,
                            ...item.lttpChatDot,
                            soLuongThucNhap: item.soLuong,
                            soLuongPhaiNhap: item.soLuong,
                        })),
                    ];

                    this.currentTable.forEach((item) => {
                        delete item.id;
                        delete item.lttpChatDot;
                    });

                    this.dataSource.data = this.currentTable;

                    this.getTotalPrice();

                    console.log('this.currentTable', this.currentTable);
                });
        });

        // if (this.isExportData) {
        //     purchaseId.forEach((item) => {
        //         this._commonService
        //             .callDataAPIShort(
        //                 '/api/services/read/PhieuXuatKho/Get',
        //                 {
        //                     id: item,
        //                 },
        //             )
        //             .subscribe((res) => {
        //                 this.currentTable = [
        //                     ...this.currentTable,
        //                     ...res.result.chiTiet.map((item) => ({
        //                         ...item,
        //                         ...item.lttpChatDot,
        //                         soLuongThucNhap: item.soLuongThucXuat,
        //                         soLuongPhaiNhap: item.soLuongPhaiXuat,
        //                     })),
        //                 ];

        //                 this.currentTable.forEach((item) => {
        //                     delete item.id;
        //                     delete item.lttpChatDot;
        //                 });

        //                 this.dataSource.data = this.currentTable;

        //                 this.getTotalPrice();
        //             });
        //     });
        // } else {

        // }
    }

    /** Whether the number of selected elements matches the total number of rows. */
    isAllSelected() {
        const numSelected = this.selection.selected.length;
        const numRows = this.dataSource.data.length;
        return numSelected === numRows;
    }

    /** Selects all rows if they are not all selected; otherwise clear selection. */
    toggleAllRows() {
        if (this.isAllSelected()) {
            this.selection.clear();
            return;
        }

        this.selection.select(...this.dataSource.data);
    }

    /** The label for the checkbox on the passed row */
    checkboxLabel(row?: any): string {
        if (!row) {
            return `${this.isAllSelected() ? 'deselect' : 'select'} all`;
        }
        return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${
            row.index + 1
        }`;
    }

    // GUID
    guid = () => {
        let s4 = () => {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        };
        //return id of format 'aaaaaaaa'-'aaaa'-'aaaa'-'aaaa'-'aaaaaaaaaaaa'
        return (
            s4() +
            s4() +
            '-' +
            s4() +
            '-' +
            s4() +
            '-' +
            s4() +
            '-' +
            s4() +
            s4() +
            s4()
        );
    };

    // Add row
    addRow(): void {
        const dialogRef = this.dialog.open(MaterialDialogComponent, {
            // disableClose: true,
            width: '70%',
            data: {
                type: 'INVENTORY_IMPORT',
                currentTable: this.currentTable,
                isShow: false,
            },
            panelClass: 'add-field-dialog-class',
        });

        dialogRef.componentInstance.onSave.subscribe((data) => {
            const isSubmitted = !!data;
            this.currentTable = [...data, ...this.dataSource.data];
            this.dataSource.data = this.currentTable;
            this.getTotalPrice();

            dialogRef.close(isSubmitted);
        });

        dialogRef.afterClosed().subscribe((result) => {
            if (!result) return;
            this._toastrService.success('', 'Thêm thành công');
        });
    }

    getTotalPrice() {
        this.dataSource.data.forEach((item) => {
            item.thanhTien = item.donGia * item.soLuongThucNhap || 0;
        });

        this.totalPrice = this.dataSource.data.reduce(
            (acc, item) => acc + item.thanhTien,
            0,
        );
    }

    // Delete row
    deleteAction(element: any): void {
        const dialogRef = this.dialog.open(DeleteConfirmDialogComponent, {
            width: '500px',
            data: element,
        });

        dialogRef.componentInstance.handleDelete.subscribe((dataDelete) => {
            const isSubmitted = !!dataDelete;
            dialogRef.close(isSubmitted);
        });

        dialogRef.afterClosed().subscribe((result) => {
            if (!result) return;

            const index: number = this.currentTable.indexOf(element);
            if (index !== -1) {
                this.currentTable.splice(index, 1);
            }

            this.dataSource.data = this.currentTable;
            this.getTotalPrice();
            this._toastrService.success('', 'Xóa thành công');
        });
    }

    // Back to list
    onBack(): void {
        this._router.navigate(['manage/app/category/nhap-kho']);
    }

    exportExcel(): void {
        console.log('exportExcel here');

        // this._exportService.exportExcel(this.currentTable, 'customers');
        let khoNhanName = this.warehouseList.find((item) =>
            item.key === this.isEdit
                ? this.dataEdit.khoNhanId
                : this.form.get('khoNhanId').value,
        );
        let nhaCungCapName = this.shipperList.find((item) =>
            item.key === this.isEdit
                ? this.dataEdit.nhaCungCapId
                : this.form.get('maNguonCungCap').value,
        );

        let maPhieuNhapKhoRaw = this.isEdit
            ? this.dataEdit.maPhieuNhapKho
            : this.form.get('maPhieuNhapKho').value;
        let bodyDataEdit = {
            ...this.dataEdit,
            khoNhanName:
                khoNhanName && khoNhanName.name ? khoNhanName.name : '...',
            nhaCungCapName:
                nhaCungCapName && nhaCungCapName.name
                    ? nhaCungCapName.name
                    : '...',
        };
        let bodyDataExcel = this.isEdit
            ? bodyDataEdit
            : {
                  ...this.dataEdit,
                  ...this.form.value,
                  khoNhanName:
                      khoNhanName && khoNhanName.name
                          ? khoNhanName.name
                          : '...',
                  nhaCungCapName:
                      nhaCungCapName && nhaCungCapName.name
                          ? nhaCungCapName.name
                          : '...',
                  tongTien: this.totalPrice,
                  chiTiet: [...this.currentTable],
                  phieuXuatKhoId: this.isExportData
                      ? this.form.value.phieuXuatNhapHangId
                      : 0,
                  phieuMuaHangId: this.isExportData
                      ? 0
                      : this.form.value.phieuXuatNhapHangId,
              };

        this._exportService.generateExcel(
            bodyDataExcel,
            `NhapKho ${maPhieuNhapKhoRaw}.xlsx`,
        );
    }

    exportExcelAndSave() {
        // this.isActiveExportExcel = true;
        // this.onSave(0);
        this.exportExcel();
    }

    // Submit data
    onSubmit(): void {
        const dataSubmit = {
            ...this.dataEdit,
            ...this.form.value,
            tongTien: this.totalPrice,
            chiTiet: [...this.currentTable],
            phieuXuatKhoId: '',
            phieuMuaHangId: this.isExportData
                ? ''
                : this.form.value.phieuXuatNhapHangId
                ? JSON.stringify(this.form.value.phieuXuatNhapHangId)
                : '',
            trangThai: 3,
        };

        if (!this.isEdit) {
            this._inventoryService.createInventoryImport(dataSubmit).subscribe(
                (res) => {
                    if (res.success) {
                        let dataLocal = JSON.parse(
                            localStorage.getItem('maNK'),
                        );

                        localStorage.setItem(
                            'maNK',
                            JSON.stringify({
                                currentDate: new Date()
                                    .toISOString()
                                    .slice(0, 10),
                                index: dataLocal.index + 1,
                            }),
                        );

                        this._router.navigate(['manage/app/category/nhap-kho']);
                        this._toastrService.success(
                            '',
                            'Tạo phiếu nhập kho thành công',
                        );
                    }
                },
                (err) => this._toastrService.errorServer(err),
            );
        } else {
            this._inventoryService.updateInventoryImport(dataSubmit).subscribe(
                (res) => {
                    this._router.navigate(['manage/app/category/nhap-kho']);
                    this._toastrService.success('', 'Cập nhật thành công');
                },
                (err) => this._toastrService.errorServer(err),
            );
        }
    }

    onSave(status = 0): void {
        const dataSubmit = {
            ...this.dataEdit,
            ...this.form.value,
            tongTien: this.totalPrice,
            chiTiet: [...this.currentTable],
            phieuXuatKhoId: '',
            phieuMuaHangId: this.isExportData
                ? ''
                : JSON.stringify(this.form.value.phieuXuatNhapHangId),
            trangThai: status,
        };

        if (!this.isEdit) {
            this._inventoryService.createInventoryImport(dataSubmit).subscribe(
                (res) => {
                    if (res.success) {
                        let dataLocal = JSON.parse(
                            localStorage.getItem('maNK'),
                        );

                        localStorage.setItem(
                            'maNK',
                            JSON.stringify({
                                currentDate: new Date()
                                    .toISOString()
                                    .slice(0, 10),
                                index: dataLocal.index + 1,
                            }),
                        );

                        if (this.isActiveExportExcel === false) {
                            this._router.navigate([
                                'manage/app/category/nhap-kho',
                            ]);
                        }
                        this._toastrService.success(
                            '',
                            'Lưu nháp phiếu nhập kho thành công',
                        );
                    }
                },
                (err) => this._toastrService.errorServer(err),
            );
        } else {
            this._inventoryService.updateInventoryImport(dataSubmit).subscribe(
                (res) => {
                    this._router.navigate(['manage/app/category/nhap-kho']);
                    this._toastrService.success('', 'Cập nhật thành công');
                },
                (err) => this._toastrService.errorServer(err),
            );
        }
    }

    printContent() {
        window.print();
    }

    rejectAction() {
        const dialogRef = this.dialog.open(DialogRejectStatusComponent, {
            width: '500px',
            data:
                this.dataEdit && this.dataEdit.lyDoTuChoi
                    ? this.dataEdit.lyDoTuChoi
                    : '',
        });

        dialogRef.afterClosed().subscribe((result) => {
            if (result && result.length > 0) {
                //Reject change status = 2
                // let optionsData = {
                //     lyDoTuChoi: result,
                // };
                const dataSubmit = {
                    ...this.dataEdit,
                    ...this.form.value,
                    tongTien: this.totalPrice,
                    chiTiet: [...this.currentTable],
                    lyDoTuChoi: result,
                    trangThai: 2,
                };
                this._inventoryService
                    .updateInventoryImport(dataSubmit)
                    .subscribe(
                        (res) => {
                            this._router.navigate([
                                'manage/app/category/nhap-kho',
                            ]);
                            this._toastrService.success(
                                '',
                                'Cập nhật thành công',
                            );
                        },
                        (err) => this._toastrService.errorServer(err),
                    );
                // this.onSave(2, optionsData);

                // this._router.navigate(['manage/app/category/xuat-kho']);
            }
        });
    }

    reviewAction() {
        console.log('reviewAction');
    }

    handleToggleLayout() {
        this.toggleLayoutHidden = !this.toggleLayoutHidden;
    }
}
