import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CircularsManagementComponent } from './circulars-management.component';

describe('CircularsManagementComponent', () => {
  let component: CircularsManagementComponent;
  let fixture: ComponentFixture<CircularsManagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CircularsManagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CircularsManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
