export const field_ITEM = {
  name: "Nội khoa",
  code: "NK",
  type: "Group",
  css: "",
  index: 1,
  items: [
    {
      name: "Name",
      code: "Fi-Name",
      type: "Field",
      css: '{"size":"8"}',
      index: 1,
      referenceValue: "name",
      required: "0",
      format: null,
      validate: "",
      dependField: "",
      formula: "",
      formulaView:
        '{"title": "Tuần hoàn máu", "placeholder": "", "showInList": "true", "icon": ""}',
      category: "tuan-hoan"
    },
    {
      name: "Field 16",
      code: "Fi-16",
      type: "Field",
      css: '{"size":"4"}',
      index: 2,
      referenceValue: "",
      required: "0",
      format: null,
      validate: null,
      dependField: null,
      formula: null,
      formulaView: '{"title": "Phân loại", "showInList": "true", "icon": ""}',
      category: "tuan-hoan"
    },
    {
      name: "Name",
      code: "Fi-Name",
      type: "Field",
      css: '{"size":"8"}',
      index: 3,
      referenceValue: "name",
      required: "0",
      format: null,
      validate: "",
      dependField: "",
      formula: "",
      formulaView:
        '{"title": "Hô hấp", "placeholder": "", "showInList": "true", "icon": ""}',
      category: "ho-hap"
    },
    {
      name: "Field 16",
      code: "Fi-16",
      type: "Field",
      css: '{"size":"4"}',
      index: 4,
      referenceValue: "",
      required: "0",
      format: null,
      validate: null,
      dependField: null,
      formula: null,
      formulaView: '{"title": "Phân loại", "showInList": "true", "icon": ""}',
      category: "ho-hap"
    }
  ]
};
