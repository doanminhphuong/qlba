import {
    ChangeDetectorRef,
    Component,
    EventEmitter,
    Input,
    OnInit,
    Output,
} from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
    selector: 'app-search',
    templateUrl: './search.component.html',
    styleUrls: ['./search.component.scss'],
})
export class SearchComponent implements OnInit {
    @Input() function: Function;
    @Input() fieldSearch;
    @Input() type: string;
    @Output() getSearchInfo: EventEmitter<any> = new EventEmitter<any>();
    formSearch: FormGroup;

    panelOpenState: boolean = false;

    constructor(private readonly changeDetectorRef: ChangeDetectorRef) { }

    ngOnInit() {
        let fieldsCtrls = {};
        let fieldList = [];

        for (let f of this.fieldSearch) {
            fieldList = [...fieldList, f];

            if (f.type === 'NUMBER') {
                fieldsCtrls[f.referenceValue] = new FormControl(
                    { value: f.defaultValue, disabled: f.disabled },
                    f.required === '1' ? [Validators.required] : [],
                );
            } else if (f.type !== 'CHECKBOX') {
                let validators = [
                    Validators.minLength(f.minLength),
                    Validators.maxLength(f.maxLength),
                    Validators.pattern(f.pattern),
                ];

                if (f.type === 'EMAIL') {
                    validators = [...validators, Validators.email];
                }

                fieldsCtrls[f.referenceValue] = new FormControl(
                    {
                        value: f.defaultValue || '',
                        disabled: f.disabled,
                    },

                    f.required === '1'
                        ? [...validators, Validators.required]
                        : [...validators],
                );

                if (f.type === 'DATETIME') {
                    fieldsCtrls[f.referenceValue].valueChanges.subscribe(
                        (val) => {
                            const dependField = fieldList.find(
                                (x) => x.dependenField === f.referenceValue,
                            );

                            if (!dependField) return;

                            dependField.min = val;
                            fieldsCtrls[dependField.referenceValue].enable();
                        },
                    );
                }
            } else {
                //if checkbox, it need multiple
                let opts = {};
                for (let opt of f.options) {
                    opts[opt.key] = new FormControl({
                        value: opt.value,
                        disabled: f.disabled,
                    });
                }
                fieldsCtrls[f.referenceValue] = new FormGroup(opts);
            }
        }

        this.formSearch = new FormGroup(fieldsCtrls);
    }

    getFields() {
        return this.fieldSearch;
    }

    ngAfterViewChecked(): void {
        this.changeDetectorRef.detectChanges();
    }

    onSubmit(): void {
        const dataSubmit = this.formSearch.value;

        this.getSearchInfo.emit(dataSubmit);
    }

    onDeleteSearchInfo(): void {
        let valueForm = this.formSearch.value;
        let keys = Object.keys(valueForm); // Get array of keys
        let result = {
            key: 'reset',
        };
        // Iterate all keys
        keys.forEach((key) => {
            result[key] = valueForm[key] = null;
        });

        this.formSearch.reset();
        this.formSearch.get('endDate').disable();

        this.getSearchInfo.emit(result);
    }
}
