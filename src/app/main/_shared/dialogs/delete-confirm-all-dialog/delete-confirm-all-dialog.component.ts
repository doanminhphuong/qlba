import { Component, EventEmitter, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
    selector: 'app-delete-confirm-all-dialog',
    templateUrl: './delete-confirm-all-dialog.component.html',
    styleUrls: ['./delete-confirm-all-dialog.component.scss'],
})
export class DeleteConfirmAllDialogComponent implements OnInit {
    handleDelete = new EventEmitter();

    constructor(
        public dialogRef: MatDialogRef<DeleteConfirmAllDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any,
    ) {}

    ngOnInit() {}

    onCloseDialog(data?: any): void {
        this.dialogRef.close(data);
    }

    onConfirmDelete(): void {
        this.handleDelete.emit(this.data);
    }
}
