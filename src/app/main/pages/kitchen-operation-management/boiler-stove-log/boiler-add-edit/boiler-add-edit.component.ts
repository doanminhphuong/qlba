import { NgxMatDateFormats, NGX_MAT_DATE_FORMATS } from '@angular-material-components/datetime-picker';
import { DatePipe } from '@angular/common';
import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import {
    FormArray,
    FormBuilder,
    FormControl,
    FormGroup,
    Validators
} from '@angular/forms';
import { MatDialog, MatPaginator } from '@angular/material';
import {
    MAT_MOMENT_DATE_ADAPTER_OPTIONS,
    MomentDateAdapter
} from '@angular/material-moment-adapter';
import {
    DateAdapter, MAT_DATE_LOCALE
} from '@angular/material/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CommonService } from '@core/services/common.service';
import { ToastrService } from '@core/services/toastr.service';
import { Subject, Subscription } from 'rxjs';
// tslint:disable-next-line:no-duplicate-imports
import { NGX_MAT_MOMENT_DATE_ADAPTER_OPTIONS } from '@angular-material-components/moment-adapter';
import { forkJoin } from 'rxjs';
import { DataService } from '@core/services/data-service';

export const MY_FORMATS: NgxMatDateFormats = {
    parse: {
        dateInput: 'DD/MM/YYYY', // this is how your date will be parsed from Input
    },
    display: {
        dateInput: 'DD/MM/YYYY HH:mm:ss', // this is how your date will get displayed on the Input
        monthYearLabel: 'MMMM YYYY',
        dateA11yLabel: 'LL',
        monthYearA11yLabel: 'MMMM YYYY',
    },
};

export interface IBep {
    id: number;
    loaiBep: number;
    chungLoaiBep: string;
    soCheTao: string;
    ngaySanXuat: string;
    noiSanXuat: string;
    ngayBatDauSuDung: string;
    ngayKiemDinh: string;
    donViKiemDinh: string;
    soTemKiemDinh: string;
    trangThai: boolean;
    dauMoiBepId: number;
}

export interface IOption {
    key: any;
    name: string;
}

@Component({
    selector: 'app-boiler-add-edit',
    templateUrl: './boiler-add-edit.component.html',
    styleUrls: ['./boiler-add-edit.component.scss'],
    providers: [
        {
            provide: DateAdapter,
            useClass: MomentDateAdapter,
            deps: [MAT_DATE_LOCALE, NGX_MAT_MOMENT_DATE_ADAPTER_OPTIONS],
        },

        { provide: MAT_DATE_LOCALE, useValue: 'vi-VI' },
        { provide: NGX_MAT_DATE_FORMATS, useValue: MY_FORMATS },
        {
            provide: MAT_MOMENT_DATE_ADAPTER_OPTIONS,
            useValue: { useUtc: true },
        },
        DatePipe,
    ],
})
export class BoilerAddEditComponent implements OnInit {
    public applyFilter = new Subject<KeyboardEvent>();
    private subscription: Subscription;
    @ViewChild(MatPaginator) paginator!: MatPaginator;
    currentDate: Date = new Date();

    titleOnEdit: string;

    // Form
    form: FormGroup;
    chiTietForm: FormGroup;
    fields: any[] = [];
    dataEdit: any;

    // Init Variable
    bepId: number;
    nhatKyId: string;
    pageTitle: string;
    dauMoiBepId: number;

    // Params
    loaiBepParam: string;
    chungLoaiBepParam: string;

    // Options
    trangThaiOptions: IOption[] = [
        { key: true, name: 'Bình thường' },
        { key: false, name: 'Sự cố' },
    ];
    dotQuanSoAnOptions: IOption[] = [];

    // Boolean
    isEdit: boolean = false;

    // ngx-mat-datetime-picker
    public disabled = false;
    public showSpinners = true;
    public showSeconds = true;
    public touchUi = false;
    public enableMeridian = false;
    public stepHour = 1;
    public stepMinute = 1;
    public stepSecond = 1;

    constructor(
        private fb: FormBuilder,
        private readonly changeDetectorRef: ChangeDetectorRef,
        private _activatedRoute: ActivatedRoute,
        private _toastrService: ToastrService,
        private _router: Router,
        public dialog: MatDialog,
        private _commonService: CommonService,
        private _dataService: DataService,
    ) { }

    ngOnInit() {
        this.dauMoiBepId = this._dataService.getDauMoiBepId();

        // Check if this is Create or Edit page
        this._activatedRoute.paramMap.subscribe((param) => {
            this.bepId = +param.get('bepId');
            this.nhatKyId = param.get('nhatKyId');
            this.loaiBepParam = param.get('loaiBep');
            this.chungLoaiBepParam = param.get('chungLoaiBep');

            if (!this.nhatKyId) return;
            this.isEdit = true;
        });

        this.getInitData();
    }

    ngAfterViewChecked(): void {
        this.changeDetectorRef.detectChanges();
    }

    ngAfterViewInit(): void { }

    getInitData(): void {
        this.pageTitle = `${this.loaiBepParam} "${this.chungLoaiBepParam}"`;
        this.getChiTietForm();

        const QuanSoAn = this._commonService
            .callDataAPIShort(
                '/api/services/read/DotQuanSoAn/GetAll',
                {
                    criterias: [
                        {
                            propertyName: 'dauMoiBepId',
                            operation: 0,
                            value: this.dauMoiBepId,
                        },
                    ],
                }
            )

        forkJoin([QuanSoAn]).subscribe((responses) => {
            this.dotQuanSoAnOptions = responses[0].result.items
                .map((item) => ({
                    key: item.id,
                    name: item.tenDot,
                }))

            this.getInitForm();
        })
    }

    getInitForm(): void {
        this.fields = this.loaiBepParam === 'Bếp lò hơi cơ khí'
            ? [
                // {
                //     type: 'SELECT',
                //     referenceValue: 'dotQuanSoAnId',
                //     name: 'Đợt quân số ăn',
                //     defaultValue: '',
                //     required: '1',
                //     options: [...this.dotQuanSoAnOptions],
                //     css: 'col-12 col-lg-3',
                //     appearance: 'legacy',
                //     selectionChange: (event) => this.handleDotQuanSoAnChange(event),
                // },
                {
                    type: 'NUMBER',
                    referenceValue: 'quanSoAn',
                    name: 'Quân số ăn',
                    defaultValue: '',
                    required: '1',
                    readonly: false,
                    css: 'col-12 col-lg-4',
                    appearance: 'legacy',
                },
                {
                    type: 'NUMBER',
                    referenceValue: 'nhienLieuDot',
                    name: 'Nhiên liệu đốt',
                    defaultValue: '',
                    required: '1',
                    css: 'col-12 col-lg-4',
                    appearance: 'legacy',
                },
                {
                    type: 'DATETIME',
                    referenceValue: 'ngayGhiChep',
                    name: 'Ngày ghi chép',
                    defaultValue: this.currentDate,
                    required: '1',
                    css: 'col-12 col-lg-4',
                    appearance: 'legacy',
                },
            ] : [
                {
                    type: 'DATETIME',
                    referenceValue: 'ngayGhiChep',
                    name: 'Ngày ghi chép',
                    defaultValue: this.currentDate,
                    required: '1',
                    css: 'col-12 col-lg-4',
                    appearance: 'legacy',
                },
            ];

        let fieldsCtrls = {};
        let fieldList = [];

        for (let f of this.fields) {
            fieldList = [...fieldList, f];

            if (f.type === 'NUMBER') {
                fieldsCtrls[f.referenceValue] = new FormControl(
                    { value: f.defaultValue, disabled: f.disabled },
                    f.required === '1' ? [Validators.required] : [],
                );
            } else if (f.type !== 'CHECKBOX') {
                let validators = [
                    Validators.minLength(f.minLength),
                    Validators.maxLength(f.maxLength),
                    Validators.pattern(f.pattern),
                ];

                if (f.type === 'EMAIL') {
                    validators = [...validators, Validators.email];
                }

                fieldsCtrls[f.referenceValue] = new FormControl(
                    {
                        value: f.defaultValue || '',
                        disabled: f.disabled,
                    },

                    f.required === '1'
                        ? [...validators, Validators.required]
                        : [...validators],
                );
            } else {
                //if checkbox, it need multiple
                let opts = {};
                for (let opt of f.options) {
                    opts[opt.key] = new FormControl({
                        value: opt.value,
                        disabled: f.disabled,
                    });
                }
                fieldsCtrls[f.referenceValue] = new FormGroup(opts);
            }
        }

        this.form = new FormGroup(fieldsCtrls);

        if (this.isEdit) {
            this.getDataEdit();
        }
    }

    getDataEdit(): void {
        this._commonService
            .callDataAPIShort('/api/services/read/NhatKyVanHanh/Get', {
                id: this.nhatKyId,
            })
            .subscribe((res) => {
                this.dataEdit = res.result;

                this.titleOnEdit = 'Chỉnh sửa nhật ký vận hành';

                // Patch data to Form
                this.form.patchValue(this.dataEdit);

                const dataChiTietForm = {
                    datas: JSON.parse(this.dataEdit['chiTiet']),
                };
                this.chiTietForm.patchValue(dataChiTietForm);
            });
    }

    getChiTietForm(): void {
        this.chiTietForm = this.fb.group({
            datas: this.fb.array([]),
        });

        this.datas().push(this.newData('Sáng'));
        this.datas().push(this.newData('Trưa'));
        this.datas().push(this.newData('Chiều'));
    }

    // Mảng lớn
    datas(): FormArray {
        return this.chiTietForm.get('datas') as FormArray;
    }

    newData(caNau: string): FormGroup {
        return this.loaiBepParam === 'Bếp lò hơi cơ khí'
            ? this.fb.group({
                CaNau: [caNau],
                ThoiDiemBatDau: ['', Validators.required],
                ThoiDiemKetThuc: ['', Validators.required],
                TrangThaiHoatDong: [true],
                MoTa: ['', Validators.required],
                MucNuoc: ['', Validators.required],
            }) : this.fb.group({
                CaNau: [caNau],
                ThoiDiemBatDau: ['', Validators.required],
                ThoiDiemKetThuc: ['', Validators.required],
                TrangThaiHoatDong: [true],
                MoTa: ['', Validators.required],
                QuanSo: ['', Validators.required],
                KetQua: ['', Validators.required],
                TongThoiGian: ['', Validators.required],
                LuongTieuHao: ['', Validators.required],
            });
    }

    handleDotQuanSoAnChange<T>(event: T): void {
        this._commonService
            .callDataAPIShort(
                '/api/services/read/ChiTietDotQuanSoAn/GetAll',
                {
                    criterias: [
                        {
                            propertyName: 'dotQuanSoAnId',
                            operation: 'Equals',
                            value: event['value'],
                        },
                    ],
                }
            )
            .subscribe((res) => {
                const quanSoAn = res.result.totalCount;
                this.form.patchValue({ quanSoAn });
            })
    }

    // Back to list
    onBack(): void {
        this._router.navigate([
            `manage/app/category/nhat-ky-van-hanh-bep/${this.bepId}`,
            {
                chungLoaiBep: this.chungLoaiBepParam,
                loaiBep: this.loaiBepParam
            },
        ]);
    }

    onSubmit(): void {
        const chiTiet = JSON.stringify(this.chiTietForm.value['datas']);

        const dataSubmit = {
            ...this.dataEdit,
            ...this.form.value,
            chiTiet,
            bepId: this.bepId,
        };

        if (!this.isEdit) {
            this._commonService
                .callDataAPIShort(
                    '/api/services/write/NhatKyVanHanh/Create',
                    dataSubmit,
                )
                .subscribe(
                    (res) => {
                        this._router.navigate([
                            `manage/app/category/nhat-ky-van-hanh-bep/${this.bepId}`,
                            {
                                chungLoaiBep: this.chungLoaiBepParam,
                                loaiBep: this.loaiBepParam
                            },
                        ]);
                        this._toastrService.success(
                            '',
                            'Tạo mới nhật ký vận hành bếp thành công',
                        );
                    },
                    (err) => this._toastrService.errorServer(err),
                );
        } else {
            this._commonService
                .callDataAPIShort(
                    '/api/services/write/NhatKyVanHanh/Update',
                    dataSubmit,
                )
                .subscribe(
                    (res) => {
                        this._router.navigate([
                            `manage/app/category/nhat-ky-van-hanh-bep/${this.bepId}`,
                            {
                                chungLoaiBep: this.chungLoaiBepParam,
                                loaiBep: this.loaiBepParam
                            },
                        ]);
                        this._toastrService.success('', 'Cập nhật thành công');
                    },
                    (err) => this._toastrService.errorServer(err),
                );
        }
    }
}
