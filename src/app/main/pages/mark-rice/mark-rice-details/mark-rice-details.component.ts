import { R } from '@angular/cdk/keycodes';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatPaginator } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { DynamicDialogComponent } from '@app/components/dynamic-dialog-component/dynamic-dialog-component.component';
import { CategoryService } from '@core/services/category.service';
import { CommonService } from '@core/services/common.service';
import { ExportExcelService } from '@core/services/export-excel.service';
import { FoodyService } from '@core/services/foody.service';
import { HoSoSucKhoeService } from '@core/services/ho-so-suc-khoe.service';
import { ToastrService } from '@core/services/toastr.service';
import { UserService } from '@core/services/user.service';
import { forkJoin } from 'rxjs';

@Component({
    selector: 'app-mark-rice-details',
    templateUrl: './mark-rice-details.component.html',
    styleUrls: ['./mark-rice-details.component.scss'],
})
export class MarkRiceDetailsComponent implements OnInit {
    @ViewChild(MatPaginator) paginator!: MatPaginator;
    isChamKhongAn: FormControl = new FormControl(true);
    currentDate: Date;

    type: string = 'CHAMCOM';
    fieldSearch: any[] = [];

    dotQuanSoAnOptions: any[] = [];
    lyDoOptions: any[] = [];
    userOptions: any[] = [];
    userOptionsClone: any[] = [];

    daysOfMonth: any[] = [];
    dataRenderByDay: any[] = [];
    dataRenderByDayClone: any[] = [];
    dataRenderByUser: any[] = [];
    chamComId: number;
    dataEdit: any;
    chiTietList: any[] = [];
    chiTietAnThemList: any[] = [];
    dataChamCom: any[] = [];
    dateStart: Date;
    dateEnd: Date;
    dateMin: Date;
    dateMax: Date;
    tenDonVi: string = '';

    p: number = 1;
    pageSize: number = 31;
    totalItems: number;
    skipCount: number = 0;
    skipCountOfChamCom: number = 0;

    // Data Submit
    dotQuanSoAnId: number;
    chamComKhongAn: boolean;
    tenPhieuChamCom: string;

    fieldSearchAdvance: any[] = [];

    form: FormGroup;
    fields: any[] = [];
    totalDate: number;
    isLoading: boolean = false;

    quanNhanList: any[] = [];
    daysEditing: number[] = [];
    daysFilter: number[] = [];
    daysMarked: number[] = [];
    lyDoAnThem: number;
    excelName: string;

    listObjectEat: any[] = [];
    listOrganization: any[] = [];

    allowExec: boolean = false;
    isChamNhieuNgay: boolean = false;
    isShow: boolean = false;

    constructor(
        private _router: Router,
        private _activatedRoute: ActivatedRoute,
        public dialog: MatDialog,
        private _toastrService: ToastrService,
        private _foodyService: FoodyService,
        private _userService: UserService,
        private _categoryService: CategoryService,
        private _exportService: ExportExcelService,
        private _commonService: CommonService,
        private hsskService: HoSoSucKhoeService,
    ) {
        let request = {
            id: localStorage.getItem('idMenu'),
            language: 'vi',
        };

        this._commonService.checkPermission(request).subscribe((response) => {
            if (!response.result) return;

            if (JSON.parse(response.result).some((obj) => obj.key === 'EXEC')) {
                this.allowExec = true;
            }
        });
    }

    ngOnInit() {
        const dateNow = new Date()
            .toISOString()
            .slice(0, 11)
            .replace('T', 'T00:00:00Z');

        this.currentDate = new Date(dateNow);

        this._activatedRoute.paramMap.subscribe((param) => {
            this.chamComId = +param.get('id');
        });

        this.getChamComById();
        this.getInitData();
        // Check if this is Create or Edit page
    }

    getChamComById() {
        this._foodyService
            .getChamComById({ id: this.chamComId })
            .subscribe((res) => {
                this.dataEdit = res.result;
                this.chamComKhongAn = this.dataEdit.chamKhongAn;
                this.tenPhieuChamCom = this.dataEdit.tenChamCom;

                this.dateMin = new Date(this.dataEdit['ngayBatDau']);
                this.dateMax = new Date(this.dataEdit['ngayKetThuc']);

                this.getDaysOfMonth();
            });
    }

    getInitData(): void {
        const dataDotQuanSoAn = this._foodyService.getAllDotQuanSoAn({});

        const dataDoiTuongAn = this._commonService.callDataAPIShort(
            '/api/services/read/DoiTuongAn/GetAll',
            { maxResultCount: 2147483647 },
        );

        const bodyLydo = {
            sorting: 'Code',
            criterias: [
                {
                    propertyName: 'GroupCode',
                    operation: 6,
                    value: 'quan-so-khong-an',
                },
            ],
        };

        const dataLyDoKhongAn = this._categoryService.getAllCategory(bodyLydo);

        const dataDonVi = this._userService.getOrganization({});

        forkJoin([
            dataDotQuanSoAn,
            dataLyDoKhongAn,
            dataDoiTuongAn,
            dataDonVi,
        ]).subscribe((results) => {
            this.dotQuanSoAnOptions = results[0].result.items
                .filter((item) => {
                    // Tháng chấm cơm >= Tháng bắt đầu của Quân số ăn
                    // Năm chấm cơm >= Năm bắt đầu của Quân số ăn
                    // Tháng chấm cơm <= Tháng kết thúc của Quân số ăn
                    // Năm chấm cơm <= Năm kết thúc của Quân số ăn
                    const thangBatDau = new Date(item.tuNgay).getMonth();
                    const namBatDau = new Date(item.tuNgay).getFullYear();
                    const thangKetThuc = new Date(item.denNgay).getMonth();
                    const namKetThuc = new Date(item.denNgay).getFullYear();
                    const thangChamCom = new Date(this.dataEdit['ngayBatDau']).getMonth();
                    const namChamCom = new Date(this.dataEdit['ngayBatDau']).getFullYear();

                    return thangBatDau <= thangChamCom
                        && namBatDau <= namChamCom
                        && thangChamCom <= thangKetThuc
                        && namChamCom <= namKetThuc;
                })
                .map((item) => ({
                    key: item.id,
                    name: item.tenDot,
                    tuNgay: item.tuNgay,
                    denNgay: item.denNgay,
                    donViId: item.value1,
                }));

            this.lyDoOptions = results[1].result.map((item) => ({
                key: item.id,
                name: item.name,
            }));

            this.listObjectEat = results[2].result.items.map((item) => ({
                key: item.codeData,
                name: item.name,
            }));

            this.listOrganization = results[3].result.map((item) => ({
                key: item.id,
                code: item.codeData,
                name: item.name,
            }));

            if (this.dotQuanSoAnOptions.length > 0) {
                this.dotQuanSoAnId = this.dotQuanSoAnOptions[0].key;
            }

            this.getInitForm();
        });
    }

    getInitForm(): void {
        this.fields = [
            {
                type: 'SELECT',
                referenceValue: 'dotQuanSoAnId',
                name: 'Đợt quân số ăn',
                defaultValue: '',
                required: '1',
                options: [...this.dotQuanSoAnOptions],
                css: 'col-12 col-lg-4',
                appearance: 'legacy',
                selectionChange: (event) => this.handleSelectionChange(event),
            },
        ];

        let fieldsCtrls = {};
        let fieldList = [];

        for (let f of this.fields) {
            fieldList = [...fieldList, f];

            if (f.type === 'NUMBER') {
                fieldsCtrls[f.referenceValue] = new FormControl(
                    { value: f.defaultValue, disabled: f.disabled },
                    f.required === '1' ? [Validators.required] : [],
                );
            } else if (f.type !== 'CHECKBOX') {
                let validators = [
                    Validators.minLength(f.minLength),
                    Validators.maxLength(f.maxLength),
                    Validators.pattern(f.pattern),
                ];

                if (f.type === 'EMAIL') {
                    validators = [...validators, Validators.email];
                }

                fieldsCtrls[f.referenceValue] = new FormControl(
                    {
                        value: f.defaultValue || '',
                        disabled: f.disabled,
                    },
                    f.required === '1'
                        ? [...validators, Validators.required]
                        : [...validators],
                );
            } else {
                //if checkbox, it need multiple
                let opts = {};
                for (let opt of f.options) {
                    opts[opt.key] = new FormControl({
                        value: opt.value,
                        disabled: f.disabled,
                    });
                }
            }
        }

        this.form = new FormGroup(fieldsCtrls);

        if (this.dotQuanSoAnOptions.length > 0) {
            this.form.patchValue({ dotQuanSoAnId: this.dotQuanSoAnId });
            this.showDetails(this.dotQuanSoAnId);
        }

        this.isShow = true;
    }

    // CHAM COM
    handleSelectionChange(event: any): void {
        this.dotQuanSoAnId = event.value;
        this.showDetails(this.dotQuanSoAnId);
    }

    showDetails(dotQuanSoAnId: number): void {
        this.isLoading = true;
        this.getDaysOfMonth();
        this.dateStart = this.getTuNgay(dotQuanSoAnId, 'tuNgay');
        this.dateEnd = this.getTuNgay(dotQuanSoAnId, 'denNgay');
        this.getDonViName(dotQuanSoAnId);
        this.getChiTietDotQuanSoAn(dotQuanSoAnId);
    }

    getTuNgay(dotQuanSoAnId: number, key: string) {
        const found = this.dotQuanSoAnOptions.find(
            (item) => item.key === dotQuanSoAnId
        );

        return new Date(found[key]);
    }

    getDonViName(dotQuanSoAnId: number) {
        const found = this.dotQuanSoAnOptions.find(
            (item) => item.key === dotQuanSoAnId
        );

        const donViId = found['donViId'];

        this.hsskService.getOrganization({ id: donViId })
            .subscribe((res) => {
                this.tenDonVi = res.result.name;
            })
    }

    getChiTietDotQuanSoAn(dotQuanSoAnId: number): void {
        const bodyDotQSA = {
            maxResultCount: 9999,
            skipCount: 0,
            sorting: 'Id',
            criterias: [
                {
                    propertyName: 'DotQuanSoAnId',
                    operation: 0,
                    value: dotQuanSoAnId,
                },
            ],
        };

        this._foodyService
            .getChiTietDotQuanSoAn(bodyDotQSA)
            .subscribe((res) => {
                const { items, totalCount } = res.result;
                this.totalItems = totalCount;

                if (items.length === 0) {
                    alert('Chưa có dữ liệu quân nhân trong đợt quân số ăn!');
                    return;
                }

                this.userOptions = items
                    .filter((item) => item.trangThai)
                    .map((item) => ({
                        key: item.id,
                        name: item.tenQuanNhan,
                        tenDoiTuongAn: item.tenDoiTuongAn,
                    }));

                this.userOptionsClone = this.userOptions;

                this.fieldSearch = [
                    {
                        type: 'MULTISELECT',
                        referenceValue: 'userIds',
                        name: 'Chọn quân nhân',
                        defaultValue: '',
                        required: '1',
                        options: [...this.userOptions],
                        search: '1',
                        searchCtrl: 'searchCtrl',
                        css: 'col-12 col-lg-4',
                        appearance: 'legacy',
                    },

                    {
                        type: 'DATETIME',
                        referenceValue: 'startDate',
                        name: 'Từ ngày',
                        defaultValue: '',
                        min: (this.dateStart.getMonth()
                            === new Date(this.dataEdit['ngayBatDau']).getMonth())
                            && (this.dateStart.getFullYear()
                                === new Date(this.dataEdit['ngayBatDau']).getFullYear())
                            ? this.dateStart
                            : new Date(this.dataEdit['ngayBatDau']),
                        max: (this.dateEnd.getMonth()
                            === new Date(this.dataEdit['ngayKetThuc']).getMonth())
                            && (this.dateEnd.getFullYear()
                                === new Date(this.dataEdit['ngayKetThuc']).getFullYear())
                            ? this.dateEnd
                            : new Date(this.dataEdit['ngayKetThuc']),
                        required: '1',
                        css: 'col-12 col-lg-4',
                        appearance: 'legacy',
                    },
                    {
                        type: 'DATETIME',
                        referenceValue: 'endDate',
                        name: 'Đến ngày',
                        defaultValue: '',
                        max: (this.dateEnd.getMonth()
                            === new Date(this.dataEdit['ngayKetThuc']).getMonth())
                            && (this.dateEnd.getFullYear()
                                === new Date(this.dataEdit['ngayKetThuc']).getFullYear())
                            ? this.dateEnd
                            : new Date(this.dataEdit['ngayKetThuc']),
                        required: '1',
                        dependenField: 'startDate',
                        disabled: true,
                        css: 'col-12 col-lg-4',
                        appearance: 'legacy',
                    },
                ];

                this.getAllChiTietChamCom2();
            });
    }

    getAllChiTietChamCom2(): void {
        const bodyChiTietChamCom = {
            maxResultCount: 2147483647,
            skipCount: 0,
            sorting: 'ChiTietDotQuanSoAnId ASC, NgayAn ASC',
            criterias: [
                {
                    propertyName: 'ChamComId',
                    operation: 0,
                    value: this.chamComId,
                },
            ],
            dotQuanSoAnId: this.dotQuanSoAnId,
        };

        this._foodyService.getAllChiTietChamCom(bodyChiTietChamCom).subscribe(
            (res) => {
                this.dataChamCom = res.result.items;

                const ngayAns = res.result.items
                    .map((data) => data.ngayAn)
                    .filter((item, index, arr) => arr.indexOf(item) === index);
                this.daysMarked = ngayAns.map((ngay) => new Date(ngay).getTime());

                const dataConvertFollowDay = ngayAns.map((ngayAn) => {
                    const filtering = this.dataChamCom
                        .filter((data) => data.ngayAn === ngayAn)
                        .map((data) => ({
                            chiTietDotQuanSoAnId: data.chiTietDotQuanSoAnId,
                            chiTietBuoiAns: JSON.parse(data.buoiAn)
                                .filter((b) => !b.CoAn)
                                .map((b) => ({
                                    BuoiAn: b.BuoiAn,
                                    CoAn: b.CoAn,
                                    LyDoKhongAn: b.LyDoKhongAn,
                                })),
                        }))
                        .filter((data) => data.chiTietBuoiAns.length > 0);

                    const filteringAnThem = this.dataChamCom
                        .filter((data) => data.ngayAn === ngayAn && data.anThem)
                        .map((data) => ({
                            chiTietDotQuanSoAnId: data.chiTietDotQuanSoAnId,
                            anThem: JSON.parse(data.anThem),
                        }))
                        .filter((data) => data.anThem.length > 0);

                    return {
                        day: new Date(ngayAn).getDate(),
                        danhSach: [...filtering],
                        danhSachAnThem: [...filteringAnThem],
                    };
                });


                if (
                    (this.dateStart.getMonth()
                        === new Date(this.dataEdit['ngayBatDau']).getMonth()) &&
                    (this.dateStart.getFullYear()
                        === new Date(this.dataEdit['ngayBatDau']).getFullYear())
                ) {
                    this.dateMin = this.dateStart;
                    this.dataRenderByDay = this.dataRenderByDay.filter(
                        (data) => data.day >= this.dateStart.getDate()
                    );
                }

                if (
                    (this.dateEnd.getMonth()
                        === new Date(this.dataEdit['ngayKetThuc']).getMonth()) &&
                    (this.dateEnd.getFullYear()
                        === new Date(this.dataEdit['ngayKetThuc']).getFullYear())
                ) {
                    this.dateMax = this.dateEnd;
                    this.dataRenderByDay = this.dataRenderByDay.filter(
                        (data) => data.day <= this.dateEnd.getDate()
                    );
                }

                this.dataRenderByDay.forEach((data, index) => {
                    const dataFound = dataConvertFollowDay.find(
                        (v, i) => data.day === v.day,
                    );

                    if (dataFound) {
                        data.danhSach = dataFound.danhSach;
                        data.danhSachAnThem = dataFound.danhSachAnThem;
                    } else {
                        data.danhSach = [];
                        data.danhSachAnThem = [];
                    }
                });

                if (this.daysFilter.length > 0) {
                    this.dataRenderByDayClone = this.dataRenderByDay.filter(
                        (data) => this.daysFilter.includes(data.day),
                    );
                } else {
                    this.dataRenderByDayClone = this.dataRenderByDay;
                }

                this.isLoading = false;
            },
            (err) => {
                this.getDaysOfMonth();
                this.isLoading = false;
            },
        );
    }

    paginate(arr, size) {
        return arr.reduce((acc, val, i) => {
            let idx = Math.floor(i / size);
            let page = acc[idx] || (acc[idx] = []);
            page.push(val);

            return acc;
        }, []);
    }

    getDaysOfMonth() {
        let daysOfMonth = [];
        const startDate = new Date(this.dataEdit['ngayBatDau']);
        const endDate = new Date(this.dataEdit['ngayKetThuc']);

        for (let d = startDate.getDate(); d <= endDate.getDate(); d++) {
            const data = {
                day: d,
                danhSach: [],
                danhSachAnThem: [],
            };

            daysOfMonth.push(data);
        }

        this.dataRenderByDay = [...daysOfMonth];
        this.dataRenderByDayClone = this.dataRenderByDay;
    }

    // PAGINATION HANDLER
    handlePageChange(event: any) {
        // this.isLoading = true;
        // setTimeout(() => (this.isLoading = false), 1000);
    }

    guid = () => {
        let s4 = () => {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        };
        //return id of format 'aaaaaaaa'-'aaaa'-'aaaa'-'aaaa'-'aaaaaaaaaaaa'
        return (
            s4() +
            s4() +
            '-' +
            s4() +
            '-' +
            s4() +
            '-' +
            s4() +
            '-' +
            s4() +
            s4() +
            s4()
        );
    };

    handleMergeSameUser(userList) {
        let output = [];

        userList.forEach((user) => {
            const existingUser = output.filter((v, i) => {
                return v.chiTietDotQuanSoAnId === user.chiTietDotQuanSoAnId;
            });

            if (existingUser.length > 0) {
                const existingIndex = output.indexOf(existingUser[0]);
                output[existingIndex].chiTietBuoiAns = user.chiTietBuoiAns;
            } else {
                output.push(user);
            }
        });

        return output;
    }

    handleMergeSameDay(danhSach) {
        let output = [];

        danhSach.forEach((item) => {
            const existingDay = output.filter((v, i) => {
                return v.day === item.day;
            });

            if (existingDay.length > 0) {
                const existingIndex = output.indexOf(existingDay[0]);
                output[existingIndex].buoiAns = [...output[existingIndex].buoiAns, ...item.buoiAns];
            } else {
                output.push(item);
            }
        });

        return output;
    }

    getTotalFollowDay(day: number, session: number) {
        const dataFound = this.dataRenderByDay.find((data) => data.day === day);
        const { danhSach } = dataFound;
        const results = danhSach.reduce(
            (acc, cur) =>
                acc.concat(
                    cur.chiTietBuoiAns.filter(
                        (ct) => ct.BuoiAn === session && !ct.CoAn,
                    ),
                ),
            [],
        );
        return results.length;
    }

    getTotalFollowUser(userId: string, session: number) {
        const filter = this.dataRenderByDay
            .filter((data) => data.danhSach.length > 0)
            .map((data) => data.danhSach)
            .reduce(
                (acc, cur) =>
                    acc.concat(
                        cur.filter((c) => c.chiTietDotQuanSoAnId === userId),
                    ),
                [],
            );

        const results = filter.reduce(
            (acc, cur) =>
                acc.concat(
                    cur.chiTietBuoiAns.filter(
                        (ct) => ct.BuoiAn === session && !ct.CoAn,
                    ),
                ),
            [],
        );

        return results.length;
    }

    getTotalAnThem(userId: string, lyDo: number) {
        const results = this.dataRenderByDay
            .filter((data) => data.danhSachAnThem.length > 0)
            .reduce(
                (acc, cur) =>
                    acc.concat(
                        cur.danhSachAnThem.filter(
                            (ds) =>
                                ds.chiTietDotQuanSoAnId === userId &&
                                ds.anThem.includes(lyDo),
                        ),
                    ),
                [],
            );

        return results.length;
    }

    getTotalAll(session: number) {
        let sang = 0;
        let trua = 0;
        let chieu = 0;

        this.dataRenderByDay.forEach((data) => {
            if (this.getTotalFollowDay(data.day, 0) > 0) {
                sang += this.getTotalFollowDay(data.day, 0);
            }
            if (this.getTotalFollowDay(data.day, 1) > 0) {
                trua += this.getTotalFollowDay(data.day, 1);
            }
            if (this.getTotalFollowDay(data.day, 2) > 0) {
                chieu += this.getTotalFollowDay(data.day, 2);
            }
        });

        switch (session) {
            case 0:
                return sang;
            case 1:
                return trua;
            case 2:
                return chieu;
        }
    }

    formatDate(date: Date) {
        const dd = ('0' + date.getDate()).slice(-2);
        const mm = ('0' + (date.getMonth() + 1)).slice(-2);
        const yyyy = date.getFullYear();

        return `${yyyy}-${mm}-${dd}`;
    }

    getTomorrow(currentDate: Date) {
        currentDate.setDate(currentDate.getDate() + 1);
        return currentDate;
    }

    handleSearch(dataSearch: any) {
        const { userIds, startDate, endDate, key } = dataSearch;

        if (key && key === 'reset') {
            this.daysFilter = [];
            this.userOptionsClone = this.userOptions;
            this.dataRenderByDayClone = this.dataRenderByDay;
        } else {
            this.userOptionsClone = this.userOptions.filter((user) =>
                userIds.includes(user.key),
            );

            const tuNgay = startDate._d.getDate();
            const denNgay = endDate._d.getDate();
            let days = [];

            for (let d = tuNgay; d <= denNgay; d++) {
                days.push(d);
            }
            this.daysFilter = [...days];
            this.dataRenderByDayClone = this.dataRenderByDay.filter((data) =>
                days.includes(data.day),
            );
        }
    }

    onSubmit() {
        this.isLoading = true;

        let dataDays;

        if (this.isChamNhieuNgay) {
            dataDays = this.dataRenderByDay.filter(
                (data) => data.danhSach.length > 0
            );
        } else {
            dataDays = this.dataRenderByDay.filter(
                (data) => this.daysEditing.includes(data.day)
                    || data.day === this.currentDate.getDate()
            );
        }

        const danhSach = dataDays.map((data) => {
            const ngayAn = this.formatDate(
                new Date(
                    new Date(this.dataEdit['ngayBatDau']).getFullYear(),
                    new Date(this.dataEdit['ngayBatDau']).getMonth(),
                    data.day,
                ),
            );

            return {
                ngayAn,
                danhSach: data.danhSach,
                danhSachAnThem: data.danhSachAnThem,
            };
        });

        const dataSubmit = {
            chamComId: this.chamComId,
            dotQuanSoAnId: this.dotQuanSoAnId,
            chamKhongAn: this.isChamKhongAn.value,
            danhSach,
        };
        console.log('dataSubmit:: ', dataSubmit);

        this._foodyService.createChiTietChamCom(dataSubmit).subscribe(
            (res) => {
                this._toastrService.success('', 'Chấm cơm thành công');
                this.daysEditing = [];
                this.isChamNhieuNgay = false;
                this.getChamComById();
                this.getAllChiTietChamCom2();
            },
            (err) => {
                this._toastrService.errorServer(err);
                this.isLoading = false;
            },
        );
    }

    onBack(): void {
        this._router.navigate(['manage/app/category/quan-ly-cham-com']);
    }

    getDateClicked(day: number) {
        const dateClicked = new Date(
            new Date(this.dataEdit['ngayBatDau']).getFullYear(),
            new Date(this.dataEdit['ngayBatDau']).getMonth(),
            day,
            7, 0, 0
        );

        return dateClicked.getTime();
    }

    getLyDoKhongAn(day: number, userId: string, session: number) {
        let result = '';

        if (this.dataRenderByDay.length > 0) {
            const dayFound = this.dataRenderByDay.find(
                (data) => data.day === day
            );

            const userFound = dayFound.danhSach.find(
                (ds) => ds.chiTietDotQuanSoAnId === userId
            );

            if (userFound && userFound.chiTietBuoiAns.length > 0) {
                const buoiAnFound = userFound.chiTietBuoiAns.find(
                    (ct) => ct.BuoiAn === session
                );

                if (buoiAnFound) {
                    const lyDoKhongAn = this.lyDoOptions.find(
                        (item) => item.key === buoiAnFound['LyDoKhongAn']
                    );

                    result = lyDoKhongAn ? lyDoKhongAn['name'] : '';
                }
            }
        }

        return result;
    }

    handleChamNhieuNgay(user) {
        const chiTietDotQuanSoAnId = user.key;
        const name = user.name;

        let fields = [
            {
                type: 'DATETIME',
                referenceValue: 'startDate',
                name: 'Từ ngày',
                defaultValue: '',
                required: '1',
                min: this.dateMin > this.currentDate
                    ? this.dateMin
                    : this.currentDate,
                max: this.dateMax,
                css: 'col-12 col-lg-6',
                appearance: 'legacy',
            },
            {
                type: 'DATETIME',
                referenceValue: 'endDate',
                name: 'Đến ngày',
                defaultValue: '',
                required: '1',
                max: this.dateMax,
                dependenField: 'startDate',
                disabled: true,
                css: 'col-12 col-lg-6',
                appearance: 'legacy',
            },
            {
                type: 'RADIO',
                referenceValue: 'LyDoKhongAn',
                name: 'Lý do không ăn',
                defaultValue: '',
                required: '1',
                options: [...this.lyDoOptions],
                css: 'col-12 col-lg-12',
                appearance: 'legacy',
            },
        ];

        let dialogRef = this.dialog.open(DynamicDialogComponent, {
            // disableClose: true,
            width: '700px',
            data: {
                name,
                typeDialog: 'CHAMCOM_UPDATE',
                datas: null,
                fields: fields,
            },
            panelClass: 'my-custom-dialog-class',
        });

        dialogRef.componentInstance.onSave.subscribe((formValues) => {
            const isSubmitted = !!formValues;
            const { startDate, endDate, LyDoKhongAn } = formValues;

            const tuNgay = startDate._d.getDate();
            const denNgay = endDate._d.getDate();
            let days = [];

            for (let d = tuNgay; d <= denNgay; d++) {
                days.push(d);

                const dayFound = this.daysMarked.find(
                    (item) => item === this.getDateClicked(d)
                );
                if (!dayFound) {
                    this.daysMarked.push(this.getDateClicked(d));
                }
            }

            this.dataRenderByDay
                .filter((data) => days.includes(data.day))
                .forEach((data) => {
                    const dataImport = {
                        chiTietDotQuanSoAnId,
                        chiTietBuoiAns: [
                            { BuoiAn: 0, CoAn: false, LyDoKhongAn },
                            { BuoiAn: 1, CoAn: false, LyDoKhongAn },
                            { BuoiAn: 2, CoAn: false, LyDoKhongAn },
                        ],
                    };

                    data.danhSach = this.handleMergeSameUser([
                        ...data.danhSach,
                        dataImport,
                    ]);
                });

            dialogRef.close(isSubmitted);
        });

        dialogRef.afterClosed().subscribe((result) => {
            if (!result) return;

            this.isChamNhieuNgay = true;
            this._toastrService.success('', 'Chấm thành công');
        });
    }

    handleTickOnSession(
        data: any,
        chiTietDotQuanSoAnId: string,
        session: number,
    ) {
        const dateClicked = this.getDateClicked(data.day);
        if (
            !this.allowExec ||
            (dateClicked < this.currentDate.getTime())
        ) return;

        const fields = [
            {
                type: 'RADIO2',
                referenceValue: 'LyDoKhongAn',
                name: 'Lý do không ăn',
                defaultValue: '',
                required: '1',
                options: [...this.lyDoOptions],
                css: 'col-12 col-lg-12',
                appearance: 'legacy',
            },
        ];

        const buoiAn = session === 0
            ? 'sáng'
            : session === 1
                ? 'trưa'
                : 'chiều';
        const titleDialog = `Chấm không ăn - buổi ${buoiAn} - ngày ${data.day}`;

        const showDialog = (callback) => {
            let dialogRef = this.dialog.open(DynamicDialogComponent, {
                // disableClose: true,
                width: '600px',
                data: {
                    name: titleDialog,
                    fields: fields,
                    typeDialog: 'CHAMCOM',
                },
                panelClass: 'my-custom-dialog-class',
            });

            dialogRef.componentInstance.onSave.subscribe((formValues) => {
                const isSubmitted = !!formValues;

                const chiTietBuoiAn = {
                    BuoiAn: session,
                    CoAn: false,
                    LyDoKhongAn: formValues['LyDoKhongAn'],
                };

                callback(chiTietBuoiAn);

                const dayFound = this.daysMarked.find(
                    (item) => item === this.getDateClicked(data.day)
                );
                if (!dayFound) {
                    this.daysMarked.push(this.getDateClicked(data.day));
                }

                dialogRef.close(isSubmitted);
            });

            dialogRef.afterClosed().subscribe((result) => {
                if (!result) return;
            });
        }

        // Handle import data follow day
        const dataImportFollowDay = {
            chiTietDotQuanSoAnId,
            chiTietBuoiAns: [],
        };

        if (data.danhSach.length > 0) {
            const dataFound = data.danhSach.find(
                (ds) => ds.chiTietDotQuanSoAnId === chiTietDotQuanSoAnId,
            );

            if (dataFound) {
                const { chiTietBuoiAns } = dataFound;

                const isExistBuoiAn = chiTietBuoiAns.find(
                    (ct) => ct.BuoiAn === session,
                );

                if (isExistBuoiAn) {
                    const newChiTietBuoiAns = chiTietBuoiAns.filter(
                        (ct) => ct.BuoiAn !== session,
                    );
                    dataFound.chiTietBuoiAns = newChiTietBuoiAns;
                } else {
                    showDialog((chiTietBuoiAn) => {
                        dataFound.chiTietBuoiAns.push(chiTietBuoiAn);
                    });
                }
            } else {
                showDialog((chiTietBuoiAn) => {
                    data.danhSach.push(dataImportFollowDay);
                    dataImportFollowDay.chiTietBuoiAns.push(chiTietBuoiAn);
                });
            }
        } else {
            data.danhSach = this.userOptions.map((user) => ({
                chiTietDotQuanSoAnId: user.key,
                chiTietBuoiAns: [],
            }));

            const dataFound = data.danhSach.find(
                (data) => data.chiTietDotQuanSoAnId === chiTietDotQuanSoAnId,
            );

            if (dataFound) {
                showDialog((chiTietBuoiAn) => {
                    dataFound.chiTietBuoiAns.push(chiTietBuoiAn);
                });
            }
        }

        const dayExisting = this.daysEditing.find((item) => item === data.day);
        if (!dayExisting) {
            this.daysEditing.push(data.day);
        }
    }

    handleCheckAll(data, session: number) {
        const dateClicked = this.getDateClicked(data.day);
        if (
            !this.allowExec ||
            (dateClicked < this.currentDate.getTime())
        ) return;

        const fields = [
            {
                type: 'RADIO2',
                referenceValue: 'LyDoKhongAn',
                name: 'Lý do không ăn',
                defaultValue: '',
                required: '1',
                options: [...this.lyDoOptions],
                css: 'col-12 col-lg-12',
                appearance: 'legacy',
            },
        ];

        const buoiAn = session === 0
            ? 'sáng'
            : session === 1
                ? 'trưa'
                : 'chiều';
        const titleDialog = `Chấm không ăn cho tất cả - buổi ${buoiAn} - ngày ${data.day}`;

        const showDialog = () => {
            let dialogRef = this.dialog.open(DynamicDialogComponent, {
                // disableClose: true,
                width: '600px',
                data: {
                    name: titleDialog,
                    fields: fields,
                    typeDialog: 'CHAMCOM',
                },
                panelClass: 'my-custom-dialog-class',
            });

            dialogRef.componentInstance.onSave.subscribe((formValues) => {
                const isSubmitted = !!formValues;

                data.danhSach
                    .filter((ds) =>
                        chiTietDotQuanSoAnIds.includes(ds.chiTietDotQuanSoAnId),
                    )
                    .forEach((ds) => {
                        const chiTietBuoiAn = {
                            BuoiAn: session,
                            CoAn: false,
                            LyDoKhongAn: formValues['LyDoKhongAn'],
                        };

                        const isExisting = ds.chiTietBuoiAns.find(
                            (ct) => ct.BuoiAn === session,
                        );

                        if (!isExisting) {
                            ds.chiTietBuoiAns.push(chiTietBuoiAn);
                        }
                    });

                const dayFound = this.daysMarked.find(
                    (item) => item === this.getDateClicked(data.day)
                );
                if (!dayFound) {
                    this.daysMarked.push(this.getDateClicked(data.day));
                }

                dialogRef.close(isSubmitted);
            });

            dialogRef.afterClosed().subscribe((result) => {
                if (!result) return;
            });
        }

        // Start
        const chiTietDotQuanSoAnIds = this.userOptionsClone.map(
            (user) => user.key,
        );

        let currentDanhSach = data.danhSach.filter((ds) =>
            chiTietDotQuanSoAnIds.includes(ds.chiTietDotQuanSoAnId)
        );

        if (currentDanhSach.length < this.userOptionsClone.length) {
            currentDanhSach = this.userOptionsClone.map((user) => {
                const dataFound = currentDanhSach.find(
                    (ds) => ds.chiTietDotQuanSoAnId === user.key,
                );

                const chiTietBuoiAns = dataFound
                    ? dataFound.chiTietBuoiAns
                    : [];

                return {
                    chiTietDotQuanSoAnId: user.key,
                    chiTietBuoiAns,
                };
            });

            data.danhSach = this.handleMergeSameUser([
                ...data.danhSach,
                ...currentDanhSach
            ]);

            showDialog();
        } else {
            const chiTietBuoiAns = currentDanhSach
                .filter((ds) =>
                    chiTietDotQuanSoAnIds.includes(ds.chiTietDotQuanSoAnId),
                )
                .reduce(
                    (acc, cur) =>
                        acc.concat(
                            cur.chiTietBuoiAns.filter(
                                (ct) => ct.BuoiAn === session,
                            ),
                        ),
                    [],
                );

            if (chiTietBuoiAns.length === this.userOptionsClone.length) {
                currentDanhSach.forEach((ds) => {
                    ds.chiTietBuoiAns = ds.chiTietBuoiAns.filter(
                        (ct) => ct.BuoiAn !== session,
                    );
                })

                data.danhSach = this.handleMergeSameUser([
                    ...data.danhSach,
                    ...currentDanhSach
                ]);
            } else {
                if (chiTietBuoiAns.length > 0) {
                    showDialog();
                } else {
                    showDialog();
                }
            }
        }

        const dayExisting = this.daysEditing.find((item) => item === data.day);
        if (!dayExisting) {
            this.daysEditing.push(data.day);
        }
    }

    handleCheckAnThem(data, chiTietDotQuanSoAnId: string) {
        const dateClicked = this.getDateClicked(data.day);
        if (
            !this.allowExec ||
            (dateClicked < this.currentDate.getTime())
        ) return;

        const chiTietAnThem = {
            chiTietDotQuanSoAnId,
            anThem: [this.lyDoAnThem],
        };

        if (data.danhSachAnThem.length > 0) {
            const dataFound = data.danhSachAnThem.find(
                (ds) => ds.chiTietDotQuanSoAnId === chiTietDotQuanSoAnId,
            );
            if (dataFound) {
                const { anThem } = dataFound;

                const isExistingLyDo = anThem.find(
                    (a) => a === this.lyDoAnThem,
                );

                if (isExistingLyDo || isExistingLyDo === 0) {
                    dataFound.anThem = anThem.filter(
                        (a) => a !== this.lyDoAnThem,
                    );
                } else {
                    dataFound.anThem.push(this.lyDoAnThem);
                }
            } else {
                data.danhSachAnThem.push(chiTietAnThem);
            }
        } else {
            data.danhSachAnThem.push(chiTietAnThem);
        }

        const dayExisting = this.daysEditing.find((item) => item === data.day);
        if (!dayExisting) {
            this.daysEditing.push(data.day);
        }
    }

    handleCheckAllAnThem(data) {
        const dateClicked = this.getDateClicked(data.day);
        if (
            !this.allowExec ||
            (dateClicked < this.currentDate.getTime())
        ) return;

        const dayExisting = this.daysEditing.find((item) => item === data.day);
        if (!dayExisting) {
            this.daysEditing.push(data.day);
        }

        if (data.danhSachAnThem.length < this.userOptions.length) {
            const { danhSachAnThem } = data;

            data.danhSachAnThem = this.userOptions.map((user) => {
                const dataFound = danhSachAnThem.find(
                    (ds) => ds.chiTietDotQuanSoAnId === user.key,
                );

                const anThem = dataFound ? dataFound.anThem : [];

                return {
                    chiTietDotQuanSoAnId: user.key,
                    anThem,
                };
            });

            data.danhSachAnThem.forEach((ds) => {
                const isExisting = ds.anThem.find((a) => a === this.lyDoAnThem);
                if (!isExisting && isExisting !== 0) {
                    ds.anThem.push(this.lyDoAnThem);
                }
            });
        } else {
            const anThems = data.danhSachAnThem.filter((ds) =>
                ds.anThem.includes(this.lyDoAnThem),
            );

            data.danhSachAnThem.forEach((ds) => {
                if (anThems.length === this.userOptions.length) {
                    ds.anThem = ds.anThem.filter((a) => a !== this.lyDoAnThem);
                } else {
                    if (anThems.length > 0) {
                        const isExisting = ds.anThem.find(
                            (a) => a === this.lyDoAnThem,
                        );
                        if (!isExisting) {
                            ds.anThem.push(this.lyDoAnThem);
                        }
                    } else {
                        ds.anThem.push(this.lyDoAnThem);
                    }
                }
            });
        }

        setTimeout(() => (this.isLoading = false), 1000);
    }

    handleTab(event) {
        this.lyDoAnThem = event.index - 1;
        this.excelName = event.tab.textLabel;
    }

    exportExcelChamCom(isChamAn: boolean) {
        const startDate = this.dateMin;
        const endDate = this.dateMax;
        const dataNecessary = {
            tenDonVi: this.tenDonVi,
            startDate,
            endDate,
            isChamAn,
        };

        const SYMBOL = isChamAn ? 'x' : 'o';

        const dataChamCom = this.userOptions.map((user) => {
            const test = this.dataRenderByDay.map(
                (item) => ({
                    day: item.day,
                    buoiAns: [],
                }));

            const filterings = this.dataChamCom
                .filter((data) => {
                    return data.chiTietDotQuanSoAnId === user.key;
                })
                .map((data) => ({
                    day: new Date(data.ngayAn).getDate(),
                    buoiAns: JSON.parse(data.buoiAn)
                        .filter((b) => isChamAn ? b.CoAn : !b.CoAn)
                        .map((b) => b.BuoiAn),
                }));

            return {
                tenQuanNhan: user.name,
                danhSach: this.handleMergeSameDay([
                    ...test,
                    ...filterings,
                ]),
            };
        });

        const chiTietChamCom = dataChamCom.map((u, i) => {
            const anThem = u.danhSach
                // .filter((ds) => ds.buoiAns.length > 0)
                .map((ds) => {
                    let data = {};

                    if (
                        this.daysMarked.includes(
                            this.getDateClicked(ds.day)
                        )
                    ) {
                        data[ds.day + 'S'] = ds.buoiAns.includes(0)
                            ? SYMBOL
                            : '';
                        data[ds.day + 'T'] = ds.buoiAns.includes(1)
                            ? SYMBOL
                            : '';
                        data[ds.day + 'C'] = ds.buoiAns.includes(2)
                            ? SYMBOL
                            : '';
                    } else {
                        data[ds.day + 'S'] = '-';
                        data[ds.day + 'T'] = '-';
                        data[ds.day + 'C'] = '-';
                    }

                    return data;
                })
                .reduce((acc, cur) => Object.assign(acc, cur), {});

            return {
                ngayChamCom: u.tenQuanNhan,
                ...anThem,
            };
        });

        const _month = ('0' + (startDate.getMonth() + 1)).slice(-2);
        const _year = startDate.getFullYear();

        this._exportService.generateExportChamComExcel(
            dataNecessary,
            chiTietChamCom,
            `Chấm cơm ${_month}-${_year}`,
        );
    }

    exportExcelAnThem() {
        const startDate = this.dateMin;
        const endDate = this.dateMax;
        const dataNecessary = {
            tenDonVi: this.tenDonVi,
            startDate,
            endDate,
        };

        const dataAnThem = this.userOptions.map((user) => {
            const filterings = this.dataChamCom
                .filter(
                    (data) =>
                        data.chiTietDotQuanSoAnId === user.key && data.anThem,
                )
                .map((data) => ({
                    day: new Date(data.ngayAn).getDate(),
                    anThem: JSON.parse(data.anThem),
                }));

            return {
                tenQuanNhan: user.name,
                danhSachAnThem: [...filterings],
            };
        });

        const chiTietAnThem = dataAnThem
            .map((u, i) => {
                let anThem = u.danhSachAnThem
                    .filter((ds) => ds.anThem.includes(this.lyDoAnThem))
                    .map((ds) => ({ [ds.day]: 'x' }))
                    .reduce((acc, cur, i, arr) => {
                        var key = Object.keys(cur)[0]; //first property: a, b, c
                        acc[key] = cur[key];
                        acc['totalAnThem'] = ('0' + arr.length).slice(-2);
                        return acc;
                    }, {});

                // if (!anThem['totalAnThem']) {
                //     anThem['totalAnThem'] = '00';
                // }

                return {
                    tenQuanNhan: u.tenQuanNhan,
                    ...anThem,
                };
            })
            .filter((x) => Object.keys(x).length > 1);

        const _month = ('0' + (startDate.getMonth() + 1)).slice(-2);
        const _year = startDate.getFullYear();

        this._exportService.generateExportAnThemExcel(
            dataNecessary,
            chiTietAnThem,
            this.lyDoAnThem,
            `${this.excelName} ${_month}-${_year}`,
        );
    }

    exportExcelBaoCao() {
        const startDate = new Date(this.dataEdit['ngayBatDau']);
        const endDate = new Date(this.dataEdit['ngayKetThuc']);
        const dataNecessary = {
            tenDonVi: this.tenDonVi,
            startDate,
            endDate,
        };

        const thang = startDate.getMonth() + 1;
        const nam = startDate.getFullYear();

        const bodyMoneyReport = {
            dauMoiBepId: this.dataEdit.dauMoiBepId,
            thang,
            nam,
        };

        this._foodyService
            .getBaoCaoChiTieuTienAn(bodyMoneyReport)
            .subscribe((res) => {
                const dataChiTieuTienAn = res.result;

                this._exportService.generateExportChiTieuExcel(
                    dataNecessary,
                    dataChiTieuTienAn,
                    `Báo cáo chi tiêu ${this.dataEdit.tenChamCom}`,
                );
            });
    }

    filterDoiTuongAn(price: string) {
        const filterQuanNhan = this.userOptions.filter((item, index) =>
            item.tenDoiTuongAn.includes(price),
        );

        return filterQuanNhan.length;
    }

    checkDataKhongAn(dataOfDay, chiTietDotQuanSoAnId: string, session: number) {
        let result = false;

        const { danhSach } = dataOfDay;

        if (danhSach.length > 0) {
            const userFound = danhSach.find(
                (ds) => ds.chiTietDotQuanSoAnId === chiTietDotQuanSoAnId,
            );

            if (!userFound) {
                return false;
            }

            const isChecked = userFound.chiTietBuoiAns.find(
                (ct) => ct.BuoiAn === session,
            );

            result = isChecked ? true : false;
        }

        return result;
    }

    resetDataChamCom() {
        this.isLoading = true;
        this.daysEditing = [];
        this.getAllChiTietChamCom2();
        this._toastrService.success('', 'Khôi phục thành công');
    }

    convertToObjectEat(id: string, type: string) {
        let result;
        let dataList = [];

        if (this.listObjectEat.length > 0 && this.listOrganization.length > 0) {
            switch (type) {
                case 'eat':
                    dataList = this.listObjectEat;
                    break;
                case 'donVi':
                    dataList = this.listOrganization;
                    break;
            }

            let item = dataList.find((item) => item.key === id);
            result = item && item.name ? item.name : '';
        }

        return result;
    }

    exportExcelChiaSuatCom() {
        let fields = [
            {
                type: 'DATETIME',
                referenceValue: 'ngayXuaExcel',
                name: 'Chọn ngày xuất excel',
                defaultValue: this.currentDate,
                required: '1',
                css: 'col-12 col-lg-12',
                appearance: 'legacy',
                min: this.dateMin,
                max: this.dateMax,
            },
        ];

        let dialogRef = this.dialog.open(DynamicDialogComponent, {
            // disableClose: true,
            width: '400px',
            data: {
                fields: fields,
                typeDialog: 'excelChiaAn',
            },
            panelClass: 'my-custom-dialog-class',
        });

        dialogRef.componentInstance.onSave.subscribe((formValues) => {
            const dataNecessary = {
                tenDonVi: this.tenDonVi,
                ngayXuatBaoCao: !formValues['ngayXuaExcel']._d
                    ? this.currentDate
                    : formValues['ngayXuaExcel']._d,
            };

            let dateFilter = new Date(formValues.ngayXuaExcel)
                .toISOString()
                .replace('T00:00:00.000Z', 'T00:00:00Z');

            let bodyRequest = {
                chamComId: this.chamComId,
                ngayBaoCao: dateFilter,
            };

            this._commonService
                .callDataAPIShort(
                    '/api/services/read/BaoCao/GetBaoCaoChiaSuatCom',
                    bodyRequest,
                )
                .subscribe((response) => {
                    let body = response.result.map((result) => ({
                        ...result,
                        donViName: this.convertToObjectEat(
                            result.donViId,
                            'donVi',
                        ),
                        doiTuongName: this.convertToObjectEat(
                            result.doiTuong,
                            'eat',
                        ),
                    }));

                    let dateExport = new Date(formValues.ngayXuaExcel)
                        .toLocaleDateString('en-GB')
                        .replace(/\//g, '-');

                    this._exportService.generateExportChiaSuatCom(
                        dataNecessary,
                        body,
                        `Báo cáo chia suất cơm`,
                    );
                });
        });
    }
}
