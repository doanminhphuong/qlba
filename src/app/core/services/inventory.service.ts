import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { Observable } from 'rxjs';
import { TokenStorageService } from './token.service';
import { UtilsService } from './utils.service';

@Injectable({
    providedIn: 'root',
})
export class InventoryService {
    BASE_URL = `${environment.SWAGGER_URL}/api/services/`;

    constructor(
        private http: HttpClient,
        private utils: UtilsService,
        private _tokenStorageService: TokenStorageService,
    ) {}

    // KHO
    getAllWarehouse(data: any): Observable<any> {
        let token = this._tokenStorageService.getToken();
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
        });

        let options = { headers: headers };

        const END_POINT = 'read/Kho/GetAll';

        return this.http.post<any>(
            `${this.BASE_URL}${END_POINT}`,
            data,
            options,
        );
    }

    getWarehouseById(data: any): Observable<any> {
        let token = this._tokenStorageService.getToken();
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
        });

        let options = { headers: headers };

        const END_POINT = 'read/Kho/Get';

        return this.http.post<any>(
            `${this.BASE_URL}${END_POINT}`,
            data,
            options,
        );
    }

    createWarehouse(data: any): Observable<any> {
        let token = this._tokenStorageService.getToken();
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
        });

        let options = { headers: headers };

        const END_POINT = 'write/Kho/Create';

        return this.http.post<any>(
            `${this.BASE_URL}${END_POINT}`,
            data,
            options,
        );
    }

    updateWarehouse(data: any): Observable<any> {
        let token = this._tokenStorageService.getToken();
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
        });

        let options = { headers: headers };

        const END_POINT = 'write/Kho/Update';

        return this.http.post<any>(
            `${this.BASE_URL}${END_POINT}`,
            data,
            options,
        );
    }

    deleteWarehouse(data: any): Observable<any> {
        let token = this._tokenStorageService.getToken();
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
        });

        let options = { headers: headers };

        const END_POINT = 'write/Kho/Delete';

        return this.http.post<any>(
            `${this.BASE_URL}${END_POINT}`,
            data,
            options,
        );
    }

    // TỒN KHO ĐẦU KỲ
    getAllInventory(data: any): Observable<any> {
        let token = this._tokenStorageService.getToken();
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
        });

        let options = { headers: headers };

        const END_POINT = 'read/TonKho/GetAll';

        return this.http.post<any>(
            `${this.BASE_URL}${END_POINT}`,
            data,
            options,
        );
    }

    getInventoryById(data: any): Observable<any> {
        let token = this._tokenStorageService.getToken();
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
        });

        let options = { headers: headers };

        const END_POINT = 'read/TonKho/Get';

        return this.http.post<any>(
            `${this.BASE_URL}${END_POINT}`,
            data,
            options,
        );
    }

    createInventory(data: any): Observable<any> {
        let token = this._tokenStorageService.getToken();
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
        });

        let options = { headers: headers };

        const END_POINT = 'write/TonKho/Create';

        return this.http.post<any>(
            `${this.BASE_URL}${END_POINT}`,
            data,
            options,
        );
    }

    createListInventory(data: any): Observable<any> {
        let token = this._tokenStorageService.getToken();
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
        });

        let options = { headers: headers };

        const END_POINT = 'write/TonKho/CreateList';

        return this.http.post<any>(
            `${this.BASE_URL}${END_POINT}`,
            data,
            options,
        );
    }

    // LỊCH SỬ TỒN KHO
    getHistoryProductOfInventory(data: any): Observable<any> {
        let token = this._tokenStorageService.getToken();
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
        });

        let options = { headers: headers };

        const END_POINT = 'read/LichSuTonKho/GetAll';

        return this.http.post<any>(
            `${this.BASE_URL}${END_POINT}`,
            data,
            options,
        );
    }

    // PHIẾU NHẬP KHO
    getAllInventoryImport(data: any): Observable<any> {
        let token = this._tokenStorageService.getToken();
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
        });

        let options = { headers: headers };

        const END_POINT = 'read/PhieuNhapKho/GetAll';

        return this.http.post<any>(
            `${this.BASE_URL}${END_POINT}`,
            data,
            options,
        );
    }

    getInventoryImportById(data: any): Observable<any> {
        let token = this._tokenStorageService.getToken();
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
        });

        let options = { headers: headers };

        const END_POINT = 'read/PhieuNhapKho/Get';

        return this.http.post<any>(
            `${this.BASE_URL}${END_POINT}`,
            data,
            options,
        );
    }

    createInventoryImport(data: any): Observable<any> {
        let token = this._tokenStorageService.getToken();
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
        });

        let options = { headers: headers };

        const END_POINT = 'write/PhieuNhapKho/Create';

        return this.http.post<any>(
            `${this.BASE_URL}${END_POINT}`,
            data,
            options,
        );
    }

    createListInventoryImport(data: any): Observable<any> {
        let token = this._tokenStorageService.getToken();
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
        });

        let options = { headers: headers };

        const END_POINT = 'write/PhieuNhapKho/CreateList';

        return this.http.post<any>(
            `${this.BASE_URL}${END_POINT}`,
            data,
            options,
        );
    }

    updateInventoryImport(data: any): Observable<any> {
        let token = this._tokenStorageService.getToken();
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
        });

        let options = { headers: headers };

        const END_POINT = 'write/PhieuNhapKho/Update';

        return this.http.post<any>(
            `${this.BASE_URL}${END_POINT}`,
            data,
            options,
        );
    }

    deleteInventoryImport(data: any): Observable<any> {
        let token = this._tokenStorageService.getToken();
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
        });

        let options = { headers: headers };

        const END_POINT = 'write/PhieuNhapKho/Delete';

        return this.http.post<any>(
            `${this.BASE_URL}${END_POINT}`,
            data,
            options,
        );
    }

    // PHIẾU CHUYỂN KHO
    createInventoryTransfer(data: any): Observable<any> {
        let token = this._tokenStorageService.getToken();
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
        });

        let options = { headers: headers };

        const END_POINT = 'write/PhieuChuyenKho/Create';

        return this.http.post<any>(
            `${this.BASE_URL}${END_POINT}`,
            data,
            options,
        );
    }
}
