import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatPaginator, MatTableDataSource } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { CommonService } from '@core/services/common.service';
import { DataService } from '@core/services/data-service';
import { ExportExcelService } from '@core/services/export-excel.service';
import { InventoryService } from '@core/services/inventory.service';
import { ToastrService } from '@core/services/toastr.service';
import { UserService } from '@core/services/user.service';
import { forkJoin, Subject, Subscription } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';

@Component({
    selector: 'app-add-pure-store',
    templateUrl: './add-pure-store.component.html',
    styleUrls: ['./add-pure-store.component.scss'],
})
export class AddPureStoreComponent implements OnInit {
    @ViewChild(MatPaginator) paginator!: MatPaginator;
    form: FormGroup;
    field: any[] = [];
    warehouseList: any[] = [];
    public applyFilter = new Subject<KeyboardEvent>();
    private subscription: Subscription;
    dataSource = new MatTableDataSource<any>([]);
    idTinhKho: string;
    dataDetail: any;
    dataInfo: any;
    isPrinter: boolean = false;
    isEdit: boolean = false;
    isLoading: boolean = false;
    userId: any;

    displayedColumns: string[] = [
        'tenLttpChatDot',
        'donViTinh',
        'donGia',
        'soLuong',
        'thanhTien',
        'soLuongThucTe',
        'thanhTienThucTe',
        'soLuongThua',
        'soLuongThieu',
        'ghiChu',
    ];

    constructor(
        private _router: Router,
        private _inventoryService: InventoryService,
        private _userService: UserService,
        public dialog: MatDialog,
        private _toastrService: ToastrService,
        private commonService: CommonService,
        private _activatedroute: ActivatedRoute,
        private _exportService: ExportExcelService,
        private _dataService: DataService,
    ) {
        this.subscription = this.applyFilter
            .pipe(
                map((event) => (event.target as HTMLInputElement).value),
                debounceTime(500),
                distinctUntilChanged(),
            )
            .subscribe((filterValue) => this.handleInputFilter(filterValue));

        this.subscription = this._dataService.getData().subscribe((message) => {
            if (this.isEdit === false) {
                this.isLoading = true;
                this.getInitData(message.id);
            }
        });
    }

    ngOnInit() {
        this.getInitData(this._dataService.getDauMoiBepId());
        this.getIdUserLogin();
    }

    getIdUserLogin() {
        this._userService.getCurrentUsers().subscribe((response) => {
            this.userId = response.result.id;
        });
    }

    getInitData(id: number) {
        this._activatedroute.paramMap.subscribe((params) => {
            this.idTinhKho = params.get('id');
        });

        const dataWarehouse = this._inventoryService.getAllWarehouse({
            maxResultCount: 999999999,
            criterias: [
                {
                    propertyName: 'dauMoiBepId',
                    operation: 0,
                    value: id,
                },
            ],
        });

        forkJoin([dataWarehouse]).subscribe((results) => {
            this.warehouseList = results[0].result.items
                .filter((item) => item.status === 'ENABLE')
                .map((item) => ({
                    key: item.id,
                    name: item.tenKho,
                }));

            if (this.warehouseList.length > 0) {
                this.field = [
                    {
                        type: 'TEXT',
                        referenceValue: 'tenBienBan',
                        name: 'Nhập tên biên bản',
                        defaultValue: '',
                        required: '1',
                        css: 'col-12 col-lg-4',
                        appearance: 'legacy',
                        disabled: this.idTinhKho ? true : false,
                    },
                    {
                        type: 'SELECT',
                        referenceValue: 'khoId',
                        name: 'Chọn kho kiểm kê',
                        defaultValue: '',
                        options: [...this.warehouseList],
                        search: '1',
                        required: '1',
                        searchCtrl: 'searchCtrl',
                        css: 'col-12 col-lg-4',
                        appearance: 'legacy',
                        disabled: this.idTinhKho ? true : false,
                        selectionChange: ($event) =>
                            this.showListFoodFuelOfInventory($event.value),
                    },
                    {
                        type: 'DATETIME',
                        referenceValue: 'ngayTao',
                        name: 'Ngày tịnh kho',
                        defaultValue: '',
                        required: '1',
                        css: 'col-12 col-lg-4',
                        appearance: 'legacy',
                        disabled: this.idTinhKho ? true : false,
                    },
                ];
                this.initForm();

                setTimeout(() => {
                    this.isLoading = false;
                }, 200);
            }
        });
    }

    initForm() {
        let fieldsCtrls = {};
        let fieldList = [];

        for (let f of this.field) {
            fieldList = [...fieldList, f];

            if (f.type === 'NUMBER') {
                fieldsCtrls[f.referenceValue] = new FormControl(
                    { value: f.defaultValue, disabled: f.disabled },
                    f.required === '1' ? [Validators.required] : [],
                );
            } else if (f.type !== 'CHECKBOX') {
                let validators = [
                    Validators.minLength(f.minLength),
                    Validators.maxLength(f.maxLength),
                    Validators.pattern(f.pattern),
                ];

                if (f.type === 'EMAIL') {
                    validators = [...validators, Validators.email];
                }

                fieldsCtrls[f.referenceValue] = new FormControl(
                    {
                        value: f.defaultValue || '',
                        disabled: f.disabled,
                    },

                    f.required === '1'
                        ? [...validators, Validators.required]
                        : [...validators],
                );

                if (f.type === 'DATETIME') {
                    fieldsCtrls[f.referenceValue].valueChanges.subscribe(
                        (val) => {
                            const dependField = fieldList.find(
                                (x) => x.dependenField === f.referenceValue,
                            );

                            if (!dependField) return;

                            dependField.min = val;
                        },
                    );
                }
            } else {
                //if checkbox, it need multiple
                let opts = {};
                for (let opt of f.options) {
                    opts[opt.key] = new FormControl({
                        value: opt.value,
                        disabled: f.disabled,
                    });
                }
            }
        }

        this.form = new FormGroup(fieldsCtrls);

        if (this.idTinhKho) {
            this.getDataDetail();
        }
    }

    getDataDetail() {
        this.isEdit = true;
        this.commonService
            .callDataAPIShort('/api/services/read/BienBanTinhKho/Get', {
                id: this.idTinhKho,
            })
            .subscribe((response) => {
                this.dataInfo = response.result;
                this.dataDetail = response.result.chiTiet;
                this.form.patchValue(this.dataInfo);
                this.dataSource = new MatTableDataSource<any>(this.dataDetail);
                this.dataSource.paginator = this.paginator;
            });
    }

    showListFoodFuelOfInventory(idInventory: any) {
        let request = {
            maxResultCount: 9999999,
            criterias: [
                {
                    propertyName: 'khoId',
                    operation: 0,
                    value: idInventory,
                },
                {
                    propertyName: 'ton',
                    operation: 2,
                    value: 0,
                },
            ],
        };

        this.commonService
            .callDataAPIShort('/api/services/read/TonKho/GetAll', request)
            .subscribe((result) => {
                let dataDetail = result.result.items.map((item) => ({
                    tenLttpChatDot: item.tenLttpChatDot,
                    donViTinh: item.donViTinh,
                    donGia: item.donGia,
                    soLuong: item.ton,
                    thanhTien: item.donGia * item.ton,
                    thanhTienThucTe: 0,
                    soLuongThua: 0,
                    thanhTienThua: 0,
                    soLuongThieu: 0,
                    thanhTienThieu: 0,
                    ghiChu: '',
                    tonKhoId: item.id,
                }));

                this.dataSource = new MatTableDataSource<any>(dataDetail);
                this.dataSource.paginator = this.paginator;
            });
    }

    onBack() {
        this._router.navigateByUrl('manage/app/category/bien-ban-tinh-kho');
    }

    handleInputFilter(filterValue: string) {
        this.dataSource.filter = filterValue.trim().toLowerCase();
    }

    onSubmit(buttonType: string) {
        let isError = false;

        let chiTiet = this.dataSource.data.map((item) => ({
            ...item,
            thanhTienThucTe:
                item.soLuongThucTe > 0 ? item.soLuongThucTe * item.donGia : 0,
            soLuongThua:
                item.soLuongThucTe > 0 && item.soLuongThucTe > item.soLuong
                    ? item.soLuongThucTe - item.soLuong
                    : 0,

            soLuongThieu:
                item.soLuongThucTe > 0 && item.soLuongThucTe < item.soLuong
                    ? item.soLuong - item.soLuongThucTe
                    : 0,
        }));

        for (const obj of chiTiet) {
            if (
                (!obj.soLuongThucTe && obj.soLuongThucTe !== 0) ||
                !Number.isInteger(obj.soLuongThucTe)
            ) {
                isError = true;
                this._toastrService.error('', 'Số lượng thực tế chưa nhập đủ');
                break;
            }

            if (isError) {
                return false;
            }
        }

        if (isError === false) {
            let dataSubmit = {
                ...this.form.value,
                nguoiLapId: this.userId,
                chiTiet: chiTiet.map((item) => ({
                    ...item,
                    thanhTienThua:
                        item.soLuongThua > 0
                            ? item.soLuongThua * item.donGia
                            : 0,
                    thanhTienThieu:
                        item.soLuongThieu > 0
                            ? item.soLuongThieu * item.donGia
                            : 0,
                })),
            };
            this.dataDetail = dataSubmit.chiTiet;

            this.commonService
                .callDataAPIShort(
                    '/api/services/write/BienBanTinhKho/Create',
                    dataSubmit,
                )
                .subscribe(
                    (response) => {
                        if (response.success) {
                            this._router.navigate([
                                'manage/app/category/bien-ban-tinh-kho',
                            ]);
                            this._toastrService.success('', 'Tạo thành công');

                            if (buttonType === 'print') {
                                this.print('printer');
                            }
                        }
                    },
                    (err) => this._toastrService.errorServer(err),
                );
        }
    }

    print(tableId: string) {
        let printContents, popupWin;
        printContents = document.getElementById(tableId).innerHTML;
        popupWin = window.open(
            '',
            '_blank',
            'top=0,left=0,height=auto,width=auto',
        );
        popupWin.document.open();
        popupWin.document.write(
            `<html>
                <style type="text/css">
                    body {font-family: 'Times New Roman', Times, serif; font-size: 14px}

                    table th, table td {
                        border:1px solid #000;
                        padding: 5px;
                    }

                    table {
                        margin:0;
                        padding:0;
                        border-spacing: 0;
                        border-collapse: collapse;
                    }

                    .text-left {
                        text-align: left;
                    }

                    .mt-10 {
                        margin-top: 10px
                    }

                    .no-color {
                        color: transparent
                    }

                    .float-left {
                        floadt: left;
                    }

                    .header {
                        margin-bottom: 10px
                    }

                    .px-20 {
                        padding: 0 20px;
                    }

                    .light {
                        font-weight: normal;
                    }

                    .indent-10 {
                        text-indent: 10px
                    }

                    .mt-20 {
                        margin-top: 20px
                    }

                    .mb-20 {
                        margin-bottom: 20px
                    }

                    .p-20-5 {
                        padding: 20px 5px
                    }

                    .ml-5 {
                        margin-left: 5px
                    }

                    .col-9, .col-md-9 {
                        width: 75%
                    }

                    .col-3, .col-md-3 {
                        width: 25%
                    }

                    .underline {
                        border-bottom-color: black;
                        border-bottom-width: 1px;
                        border-bottom-style: groove
                    }

                    .my-10 {
                        margin-top: 10px;
                        margin-bottom: 10px;
                    }

                    .bold {
                        font-weight: bold
                    }

                    .text-center {
                        text-align: center
                    }

                    .d-flex {
                        display: flex;
                    }

                    .justify-space-between {
                        justify-content: space-between;
                    }

                    .m-0 {
                        margin: 0;
                    }

                    .italic {
                        font-style: italic;
                    }

                    .mb-10 {
                        margin-bottom: 10px
                    }

                    .align-end {
                        align-items: end;
                    }

                    .w-60 {
                        width: 60%
                    }

                    .max-w {
                        width: 100%;
                    }

                    .align-center {
                        align-items: center;
                    }

                    .box-sig {
                        height: 100px; width: 100%
                    }

                    @media print { @page { size: A4 landscape; font-size: 14px} }
                </style>
                <head>
                    <title></title>
                </head>
                <body onload="window.print();window.close()">${printContents}</body>
            </html>`,
        );
        popupWin.document.close();
    }

    exportExcel() {
        let bodyDataExcel = {
            ...this.dataInfo,
        };
        this._exportService.generatePureStoreExcel(
            bodyDataExcel,
            `Tinh Kho ${this.form.get('tenBienBan').value}.xlsx`,
        );
    }
}
