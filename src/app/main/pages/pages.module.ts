import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PagesRoutingModule } from './pages-routing.module';
import { PagesComponent } from './pages.component';
import { LayoutModule } from '@app/_shared/layout.module';
import { BlankComponent } from '@app/components/blank/blank.component';
import { DynamicRouterComponent } from '@app/components/dynamic-router-component/dynamic-router-component.component';
import {
    MatCheckboxModule,
    MatDividerModule,
    MatIconModule,
    MatPaginatorModule,
    MatProgressSpinnerModule,
    MatTableModule,
} from '@angular/material';

@NgModule({
    declarations: [PagesComponent, BlankComponent, DynamicRouterComponent],
    imports: [
        CommonModule,
        PagesRoutingModule,
        LayoutModule,

        /////////////Dang test import Module, sau nay tach ra thi phai xoa nhung phan nay
        MatIconModule,
        MatDividerModule,
        MatPaginatorModule,
        MatProgressSpinnerModule,
        MatTableModule,
        MatCheckboxModule,
        //Dang test import Module, sau nay tach ra thi phai xoa nhung phan nay
    ],
})
export class PagesModule {}
