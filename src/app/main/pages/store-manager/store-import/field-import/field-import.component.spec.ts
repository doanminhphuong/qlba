import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FieldImportComponent } from './field-import.component';

describe('FieldImportComponent', () => {
  let component: FieldImportComponent;
  let fixture: ComponentFixture<FieldImportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FieldImportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FieldImportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
