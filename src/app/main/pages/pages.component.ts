import { Component } from '@angular/core';

@Component({
    selector: 'ngx-pages',
    styleUrls: ['pages.component.scss'],
    template: `
        <div>
            <router-outlet></router-outlet>
        </div>
    `,
})
export class PagesComponent {
    //   menu = MENU_ITEMS;
}
