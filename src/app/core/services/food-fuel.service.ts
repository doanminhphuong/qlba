import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { Observable } from 'rxjs';
import { TokenStorageService } from './token.service';
import { UtilsService } from './utils.service';

@Injectable({
    providedIn: 'root',
})
export class FoodFuelService {
    BASE_URL = `${environment.SWAGGER_URL}/api/services/`;

    constructor(
        private http: HttpClient,
        private utils: UtilsService,
        private _tokenStorageService: TokenStorageService,
    ) {}

    // NHÓM LƯƠNG THỰC PHẨM, CHẤT ĐỐT
    getAllGroupOfFoodFuel(data: any): Observable<any> {
        let token = this._tokenStorageService.getToken();
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
        });

        let options = { headers: headers };

        const END_POINT = 'read/NhomLttpChatDot/GetAll';

        return this.http.post<any>(
            `${this.BASE_URL}${END_POINT}`,
            data,
            options,
        );
    }

    createGroupOfFoodFuel(data: any): Observable<any> {
        let token = this._tokenStorageService.getToken();
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
        });

        let options = { headers: headers };

        const END_POINT = 'write/NhomLttpChatDot/Create';

        return this.http.post<any>(
            `${this.BASE_URL}${END_POINT}`,
            data,
            options,
        );
    }

    updateGroupOfFoodFuel(data: any): Observable<any> {
        let token = this._tokenStorageService.getToken();
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
        });

        let options = { headers: headers };

        const END_POINT = 'write/NhomLttpChatDot/Update';

        return this.http.post<any>(
            `${this.BASE_URL}${END_POINT}`,
            data,
            options,
        );
    }

    deleteGroupOfFoodFuel(data: any): Observable<any> {
        let token = this._tokenStorageService.getToken();
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
        });

        let options = { headers: headers };

        const END_POINT = 'write/NhomLttpChatDot/Delete';

        return this.http.post<any>(
            `${this.BASE_URL}${END_POINT}`,
            data,
            options,
        );
    }

    // LƯƠNG THỰC PHẨM, CHẤT ĐỐT
    getAllFoodFuel(data: any): Observable<any> {
        let token = this._tokenStorageService.getToken();
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
        });

        let options = { headers: headers };

        const END_POINT = 'read/LttpChatDot/GetAll';

        return this.http.post<any>(
            `${this.BASE_URL}${END_POINT}`,
            data,
            options,
        );
    }

    createFoodFuel(data: any): Observable<any> {
        let token = this._tokenStorageService.getToken();
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
        });

        let options = { headers: headers };

        const END_POINT = 'write/LttpChatDot/Create';

        return this.http.post<any>(
            `${this.BASE_URL}${END_POINT}`,
            data,
            options,
        );
    }

    updateFoodFuel(data: any): Observable<any> {
        let token = this._tokenStorageService.getToken();
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
        });

        let options = { headers: headers };

        const END_POINT = 'write/LttpChatDot/Update';

        return this.http.post<any>(
            `${this.BASE_URL}${END_POINT}`,
            data,
            options,
        );
    }

    deleteFoodFuel(data: any): Observable<any> {
        let token = this._tokenStorageService.getToken();
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
        });

        let options = { headers: headers };

        const END_POINT = 'write/LttpChatDot/Delete';

        return this.http.post<any>(
            `${this.BASE_URL}${END_POINT}`,
            data,
            options,
        );
    }
}
