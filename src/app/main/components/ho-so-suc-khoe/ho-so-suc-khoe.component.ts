import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { Router } from '@angular/router';
import { HoSoSucKhoeService } from '@core/services/ho-so-suc-khoe.service';

@Component({
    selector: 'app-ho-so-suc-khoe',
    templateUrl: './ho-so-suc-khoe.component.html',
    styleUrls: ['./ho-so-suc-khoe.component.scss'],
})
export class HoSoSucKhoeComponent implements OnInit {

    @ViewChild(MatPaginator) paginator!: MatPaginator;

    displayedColumns: string[] = [
        'index',
        'maYTe',
        'fullName',
        'birthday',
        'gender',
        'unit',
        'rank',
        'position',
        'actions',
    ];

    fieldSearch: any[] = [
        {
            type: 'TEXT',
            referenceValue: 'maYTe',
            name: 'Mã y tế',
            defaultValue: '',
            required: '0',
            icon: 'format_list_numbered',
            css: 'col-12 col-lg-4',
        },
        {
            type: 'TEXT',
            referenceValue: 'name',
            name: 'Họ và tên',
            defaultValue: '',
            required: '0',
            icon: 'code',
            css: 'col-12 col-lg-4',
        },
        {
            type: 'TEXT',
            referenceValue: 'gender',
            name: 'Giới tính',
            defaultValue: '',
            required: '0',
            icon: 'code',
            css: 'col-12 col-lg-4',
        },
        {
            type: 'TEXT',
            referenceValue: 'unit',
            name: 'Đơn vị',
            defaultValue: null,
            required: '0',
            icon: 'filter_vintage',
            css: 'col-12 col-lg-4',
        },
        {
            type: 'TEXT',
            referenceValue: 'chucVu',
            name: 'Chức vụ',
            defaultValue: null,
            required: '0',
            icon: 'filter_vintage',
            css: 'col-12 col-lg-4',
        },
        {
            type: 'TEXT',
            referenceValue: 'capBac',
            name: 'Cấp bậc',
            defaultValue: null,
            required: '0',
            icon: 'filter_vintage',
            css: 'col-12 col-lg-4',
        },
    ];

    dataSource = new MatTableDataSource<any>([]);
    result;

    constructor(private hsskService: HoSoSucKhoeService, private _router: Router,) {}

    ngOnInit() {
        let body = {
            maxResultCount: 1000,
            skipCount: 0,
            sorting: 'Code',
            criterias: [
                {
                    propertyName: 'Code',
                    operation: 6,
                    value: '',
                },
            ],
        };

        this.hsskService.getUsers(body).subscribe((result: any) => {
            let data = result;

            this.dataSource = new MatTableDataSource<any>(data.result);
            this.dataSource.paginator = this.paginator;
        });
    }

    getValueDataTable(element, role: string = 'capBac') {
        const obj = JSON.parse(element.valueData);
        return obj[role] || ' ';
    }

    createAction(element: any) {
        this._router.navigateByUrl(
            'manage/app/category/lap-ho-so-suc-khoe/create/' + element.id,
        );
    }

    handleSearch(data: any) {}
}
