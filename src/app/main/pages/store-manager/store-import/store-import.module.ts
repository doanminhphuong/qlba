import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustomerImportComponent } from './customer-import/customer-import.component';
import { FieldImportComponent } from './field-import/field-import.component';
import { ListImportComponent } from './list-import/list-import.component';
import { SummaryImportComponent } from './summary-import/summary-import.component';

import { StoreImportRoutingModule } from './store-import-routing.module';
import { LayoutModule } from '@app/_shared/layout.module';
import {
    MatCheckboxModule,
    MatDividerModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatPaginatorModule,
    MatProgressSpinnerModule,
    MatTableModule,
} from '@angular/material';
import { ListImportAddEditComponent } from './list-import/list-import-add-edit/list-import-add-edit.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CustommerAddEditComponent } from './customer-import/custommer-add-edit/custommer-add-edit.component';
import { SummaryImportAddEditComponent } from './summary-import/summary-import-add-edit/summary-import-add-edit.component';
import { ConvertNumToStrPipe } from './customer-import/custommer-add-edit/convert-num-to-str.pipe';

@NgModule({
    declarations: [
        CustomerImportComponent,
        FieldImportComponent,
        ListImportComponent,
        SummaryImportComponent,
        ListImportAddEditComponent,
        CustommerAddEditComponent,
        SummaryImportAddEditComponent,
        ConvertNumToStrPipe,
    ],
    imports: [
        CommonModule,
        StoreImportRoutingModule,
        LayoutModule,
        MatIconModule,
        MatDividerModule,
        MatPaginatorModule,
        MatProgressSpinnerModule,
        MatTableModule,
        FormsModule,
        ReactiveFormsModule,
        MatFormFieldModule,
        MatInputModule,
        MatCheckboxModule,
    ],
    providers: [ConvertNumToStrPipe],
})
export class StoreImportModule {}
