import { Component, OnInit } from '@angular/core';
import {
  AbstractControl,
  FormArray,
  FormControl,
  FormGroup,
  ValidationErrors,
  ValidatorFn,
  Validators,
} from '@angular/forms';
import { CommonService } from '@core/services/common.service';
import { HoSoSucKhoeService } from '@core/services/ho-so-suc-khoe.service';
import { QLDKService } from '@core/services/qldk.service';
import { ToastrService } from '@core/services/toastr.service';
import { TokenStorageService } from '@core/services/token.service';
import { UserService } from '@core/services/user.service';
import { forkJoin } from 'rxjs';

@Component({
  selector: 'app-account-info',
  templateUrl: './account-info.component.html',
  styleUrls: ['./account-info.component.scss']
})
export class AccountInfoComponent implements OnInit {
  currentUser;
  currentOrg;
  form: FormGroup;
  formInfo: FormGroup;
  formPassword: FormGroup;
  fieldsInfo = [];
  fieldsPassword = [];

  constructor(
    private _tokenStorageService: TokenStorageService,
    public _hssk: HoSoSucKhoeService,
    public _userService: UserService,
    private _toastrService: ToastrService,
    private _commonService: CommonService,
  ) { }

  ngOnInit() {
    this.getCurrentUser();
  }

  getCurrentUser() {
    const dataUser = this._userService.getCurrentUsers();
    const dataOrg = this._userService.getUserOrganization();

    forkJoin([dataUser, dataOrg]).subscribe((responses) => {
      this.currentUser = responses[0].result;
      console.log('currentUser:: ', this.currentUser);

      const listOrg = responses[1].result;

      this.currentOrg = listOrg.find(
        (item) =>
          item.groupCode.toUpperCase().includes('DONVI'),
      );

      this.fieldsInfo = [
        {
          type: 'TEXT',
          referenceValue: 'userName',
          name: 'Tên tài khoản',
          defaultValue: this.currentUser['userName'],
          disabled: true,
          required: '0',
          // icon: 'assignment_ind',
          css: 'col-12 col-lg-12',
        },
        {
          type: 'TEXT',
          referenceValue: 'surname',
          name: 'Họ',
          defaultValue: this.currentUser['surname'],
          disabled: false,
          required: '1',
          // icon: 'event_seat',
          css: 'col-12 col-lg-12',
        },
        {
          type: 'TEXT',
          referenceValue: 'name',
          name: 'Tên',
          defaultValue: this.currentUser['name'],
          disabled: false,
          required: '1',
          // icon: 'layers',
          css: 'col-12 col-lg-12',
        },
        {
          type: 'TEXT',
          referenceValue: 'donVi',
          name: 'Đơn vị',
          defaultValue: this.currentOrg['name'],
          disabled: true,
          required: '0',
          // icon: 'account_circle',
          css: 'col-12 col-lg-12',
        },
        {
          type: 'TEXT',
          referenceValue: 'emailAddress',
          name: 'Chức danh',
          defaultValue: '',
          disabled: false,
          required: '0',
          // icon: 'email',
          css: 'col-12 col-lg-12',
        },
      ];

      this.fieldsPassword = [
        {
          type: 'PASSWORD',
          referenceValue: 'oldPass',
          name: 'Mật khẩu cũ',
          defaultValue: '',
          disabled: false,
          required: '0',
          css: 'col-12 col-lg-12',
        },
        {
          type: 'PASSWORD',
          referenceValue: 'newPass',
          name: 'Mật khẩu mới',
          defaultValue: '',
          disabled: false,
          required: '0',
          css: 'col-12 col-lg-12',
        },
        {
          type: 'PASSWORD',
          referenceValue: 'confirmNewPass',
          name: 'Nhập lại mật khẩu mới',
          defaultValue: '',
          disabled: false,
          required: '0',
          css: 'col-12 col-lg-12',
        },
      ];

      this.getInitFormInfo();
      this.getInitFormPassword();
    })
  }

  getInitFormInfo() {
    let fieldsCtrls = {};
    let fieldList = [];

    for (let f of this.fieldsInfo) {
      fieldList = [...fieldList, f];

      if (f.type === 'NUMBER' || f.type === 'PASSWORD') {
        fieldsCtrls[f.referenceValue] = new FormControl(
          { value: f.defaultValue, disabled: f.disabled },
          f.required === '1' ? [Validators.required] : [],
        );
      } else if (f.type !== 'CHECKBOX') {
        let validators = [
          Validators.minLength(f.minLength),
          Validators.maxLength(f.maxLength),
          Validators.pattern(f.pattern),
        ];

        if (f.type === 'EMAIL') {
          validators = [...validators, Validators.email];
        }

        fieldsCtrls[f.referenceValue] = new FormControl(
          {
            value: f.defaultValue || '',
            disabled: f.disabled,
          },
          f.required === '1'
            ? [...validators, Validators.required]
            : [...validators],
        );
      } else {
        //if checkbox, it need multiple
        let opts = {};
        for (let opt of f.options) {
          opts[opt.key] = new FormControl({
            value: opt.value,
            disabled: f.disabled,
          });
        }
        fieldsCtrls[f.referenceValue] = new FormGroup(opts);
      }
    }

    this.formInfo = new FormGroup(fieldsCtrls);
  }

  getInitFormPassword() {
    let fieldsCtrls = {};
    let fieldList = [];

    for (let f of this.fieldsPassword) {
      fieldList = [...fieldList, f];

      if (f.type === 'NUMBER') {
        fieldsCtrls[f.referenceValue] = new FormControl(
          { value: f.defaultValue, disabled: f.disabled },
          f.required === '1' ? [Validators.required] : [],
        );
      } else if (f.type === 'PASSWORD') {
        fieldsCtrls[f.referenceValue] = new FormControl(
          { value: f.defaultValue, disabled: f.disabled },
          f.required === '1' ? [Validators.required] : [],
        );
      } else if (f.type !== 'CHECKBOX') {
        let validators = [
          Validators.minLength(f.minLength),
          Validators.maxLength(f.maxLength),
          Validators.pattern(f.pattern),
        ];

        if (f.type === 'EMAIL') {
          validators = [...validators, Validators.email];
        }

        fieldsCtrls[f.referenceValue] = new FormControl(
          {
            value: f.defaultValue || '',
            disabled: f.disabled,
          },
          f.required === '1'
            ? [...validators, Validators.required]
            : [...validators],
        );
      } else {
        //if checkbox, it need multiple
        let opts = {};
        for (let opt of f.options) {
          opts[opt.key] = new FormControl({
            value: opt.value,
            disabled: f.disabled,
          });
        }
        fieldsCtrls[f.referenceValue] = new FormGroup(opts);
      }
    }

    this.formPassword = new FormGroup(fieldsCtrls, {
      validators: this.checkPasswords,
    });
  }

  checkPasswords: ValidatorFn = (
    group: AbstractControl,
  ): ValidationErrors | null => {
    let pass = group.get('newPass').value;
    let confirmPass = group.get('confirmNewPass').value;

    return pass === confirmPass ? { notSame: false } : { notSame: true };
  };

  getValueDataTable(element, role: string = 'capBac') {
    if (element.valueData) {
      const obj = JSON.parse(element.valueData.replace(/\NULL/g, '""'));
      return obj[role] || ' ';
    } else {
      return ' ';
    }
  }

  onSubmit() {
    // let dataInfo = {
    //   ...this.currentUser,
    //   ...this.formInfo.value,
    // };
    // console.log('dataInfo:: ', dataInfo);

    console.log('formInfo:: ', this.formInfo.value);
    console.log('formPassword:: ', this.formPassword.value);

    // this._commonService
    //     .callDataAPIShort('/api/services/read/Bep/GetAll', dataInfo)
    //     .subscribe((result) => {
    //       if (result.success == true) {
    //           this._toastrService.success('', 'Cập nhật thành công');

    //           this.getCurrentUser();
    //       }
    //   })

    // let dataPassword = {
    //     id: this.currentUser.id,
    //     oldPass: this.formPassword.value.oldPass,
    //     newPass: this.formPassword.value.newPass,
    // };

    // if (
    //     this.formPassword.value.confirmNewPass &&
    //     this.formPassword.value.newPass
    // ) {
    //     if (this.formPassword.errors.notSame === true) {
    //         this._toastrService.error('', 'Password không trùng');
    //     } else {
    //         this._userService.changePassword(dataPassword).subscribe((result) => {});

    //         this._userService.uppdateInfoUser(dataInfo).subscribe((result) => {
    //                 if (result.success == true) {
    //                     this._toastrService.success(
    //                         '',
    //                         'Cập nhật thành công',
    //                     );

    //                     this.getCurrentUser();
    //                 }
    //         });
    //     }
    // } else {
    //     this._userService.uppdateInfoUser(dataInfo).subscribe((result) => {
    //         if (result.success == true) {
    //             this._toastrService.success('', 'Cập nhật thành công');

    //             this.getCurrentUser();
    //         }
    //     });
    // }
  }
}
