import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AppAuthService } from '@core/services/auth.service';
import { TokenStorageService } from '@core/services/token.service';
import { environment } from 'environments/environment';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
    form: FormGroup;
    hide = true;
    constructor(
        private fb: FormBuilder,
        private authService: AppAuthService,
        private router: Router,
        private tokenStorageService: TokenStorageService,
    ) {
        this.form = this.fb.group({
            userNameOrEmailAddress: ['', Validators.required],
            password: ['', Validators.required],
        });
    }

    ngOnInit() {
        this.loginCustom();
    }

    login() {
        const val = this.form.value;
        console.log(val);

        if (val.userNameOrEmailAddress && val.password) {
            this.authService
                .login(val.userNameOrEmailAddress, val.password)
                .subscribe((res: any) => {
                    console.log('User is logged in');
                    this.router.navigate(['/']);
                    // this.router.navigate([
                    //     '/manage/app/category/luong-thuc-thuc-pham-chat-dot',
                    // ]);
                });
        }
    }

    loginCustom(): void {
        let { baseUrl } = environment;
        const Url = `${environment.SSO_URL}/v/login#${baseUrl}/set-token`;
        // window.open(Url);
        window.location.href = Url;
    }
}
