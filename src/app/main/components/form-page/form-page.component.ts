import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { convertToR3QueryMetadata } from '@angular/core/src/render3/jit/directive';
import {
    FormArray,
    FormBuilder,
    FormControl,
    FormGroup,
    Validators,
} from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { CategoryDialogComponent } from '@app/_shared/dialogs/category-dialog/category-dialog.component';
import { AddFieldDialogComponent } from '@app/_shared/header/dialog/add-field-dialog/add-field-dialog.component';
import { AddGroupDialogComponent } from '@app/_shared/header/dialog/add-group-dialog/add-group-dialog.component';
import { CategoryService } from '@core/services/category.service';
import { ToastrService } from '@core/services/toastr.service';
import { DynamicDialogComponent } from '../dynamic-dialog-component/dynamic-dialog-component.component';

@Component({
    selector: 'app-form-page',
    templateUrl: './form-page.component.html',
    styleUrls: ['./form-page.component.scss'],
})
export class FormPageComponent implements OnInit {
    isEditField: boolean = false;
    isAddGroupData: boolean = false;
    isAddGroupFieldData: boolean = false;
    isEnableDelete: boolean = false;
    isDeleteTab: boolean = false;
    isCheckedTab: boolean = true;
    typePassDataDialog: string;

    formId: string;
    data: any;
    dataGroup: any[] = [];
    errorAPI: boolean = false;
    selected = new FormControl(0);
    typeDialog: string;
    index: number;
    form: FormGroup;

    constructor(
        private _activatedroute: ActivatedRoute,
        private _categoryService: CategoryService,
        private _toastrService: ToastrService,
        public dialog: MatDialog,
        private formBuilder: FormBuilder,
        private _router: Router,
    ) {
        this.form = this.formBuilder.group({
            code: ['', Validators.required],
            name: ['', Validators.required],
            status: ['DEFAULT'],
            language: [''],
            value1: [''],
        });
    }

    get code() {
        return this.form.get('code');
    }
    get name() {
        return this.form.get('name');
    }

    ngOnInit() {
        this.initData();
    }

    initData() {
        this._activatedroute.paramMap.subscribe((params) => {
            this.formId = params.get('id');
            this.isEditField = !!this.formId;

            let dataForm = {
                id: this.formId,
            };

            this._categoryService.getFormByID(dataForm).subscribe(
                (res: any) => {
                    this.errorAPI = false;
                    this.data = res.result;
                    this.dataGroup = JSON.parse(this.data.valueData) || [];
                    this.dataGroup = this.dataGroup.sort(
                        (a, b) => a.index - b.index,
                    ); // DESC List Tab
                    console.log('dataFormById', res.result);
                    console.log('dataGroup', this.dataGroup);

                    this.form.patchValue(res.result);
                },
                (err) => {
                    this.errorAPI = true;
                    this._toastrService.error(
                        err.error.details,
                        err.error.message,
                    );
                },
            );
        });
    }

    guid = () => {
        let s4 = () => {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        };
        //return id of format 'aaaaaaaa'-'aaaa'-'aaaa'-'aaaa'-'aaaaaaaaaaaa'
        return (
            s4() +
            s4() +
            '-' +
            s4() +
            '-' +
            s4() +
            '-' +
            s4() +
            '-' +
            s4() +
            s4() +
            s4()
        );
    };

    handleGroupItem(item: any, listData: any) {
        // console.log('items::', item.item);
        // console.log('type::', item.type);
        console.log('listData::', listData);

        let indexItem = listData.findIndex(
            (itemDataLoop: any) => itemDataLoop.id === item.item.id,
        );
        // console.log('indexItem::', indexItem);

        switch (item.type.toUpperCase()) {
            case 'ADD':
                let dialogFieldRef = this.dialog.open(AddFieldDialogComponent, {
                    width: '70%',
                    data: {
                        type: item.type,
                        data: item.item,
                    },
                    panelClass: 'add-field-dialog-class',
                });

                dialogFieldRef.componentInstance.onSave.subscribe(
                    (dataCreate: any) => {
                        // console.log('dataCreate', dataCreate);

                        let dataSubmit = {
                            ...item.item,
                            items: [...item.item.items, ...dataCreate],
                        };

                        listData.splice(indexItem, 1, dataSubmit);
                    },
                );
                this.index = indexItem;
                break;
            case 'REFRESH':
                console.log('REFRESH');
                console.log(listData[indexItem]);
                let itemSort = [...item.item.items].sort(
                    (a, b) => a.number10 - b.number10,
                );
                let dataSubmit = {
                    ...item.item,
                    items: itemSort,
                };

                listData.splice(indexItem, 1, dataSubmit);

                break;
            case 'EDIT':
                const fields = [
                    {
                        type: 'TEXT',
                        referenceValue: 'code',
                        name: 'Mã',
                        defaultValue: '',
                        required: '1',
                        pattern: '^[a-zA-Z0-9-]*$',
                        icon: 'code',
                        css: 'col-12 col-lg-3',
                    },
                    {
                        type: 'TEXT',
                        referenceValue: 'name',
                        name: 'Tên',
                        defaultValue: '',
                        required: '1',
                        icon: 'code',
                        css: 'col-12 col-lg-3',
                    },
                    {
                        type: 'NUMBER',
                        referenceValue: 'index',
                        name: 'Vị trí',
                        defaultValue: 1,
                        required: '0',
                        icon: 'code',
                        css: 'col-12 col-lg-3',
                    },
                    {
                        type: 'SELECT',
                        referenceValue: 'type',
                        name: 'Loại',
                        defaultValue: 'Group',
                        required: '0',
                        options: [
                            { key: 'Group', name: 'Group' },
                            { key: 'Field', name: 'Field' },
                            { key: 'Table', name: 'Table' },
                            { key: 'Tab', name: 'Tab' },
                        ],
                        css: 'col-12 col-lg-3',
                    },
                    {
                        type: 'TEXTAREA',
                        referenceValue: 'header',
                        name: 'Header',
                        defaultValue: '',
                        required: '0',
                        css: 'col-12 col-lg-12',
                        hint: `VD: [{"label":"Mã bệnh","key":"code","type":"text"}]`,
                    },
                    {
                        type: 'CHECKBOX',
                        referenceValue: 'fixed',
                        name: '',
                        required: '0',
                        options: [
                            {
                                key: 'LOCK',
                                name: 'Cố định số lượng dòng',
                                checked: true,
                            },
                        ],
                        css: 'col-12 col-lg-12',
                    },
                    {
                        type: 'TEXTAREA',
                        referenceValue: 'css',
                        name: 'CSS',
                        defaultValue: '',
                        required: '0',
                        css: 'col-12 col-lg-12',
                    },
                ];

                let dialogRef = this.dialog.open(DynamicDialogComponent, {
                    width: '70%',
                    data: {
                        typeDialog: 'GROUP',
                        datas: item.item,
                        fields: fields,
                    },
                    panelClass: 'my-custom-dialog-class',
                });

                dialogRef.componentInstance.onSave.subscribe(
                    (dataCreate: any) => {
                        const isSubmitted = !!dataCreate;

                        const dataSubmit = {
                            ...item.item,
                            ...dataCreate,
                        };

                        listData.splice(indexItem, 1, dataSubmit);

                        dialogRef.close(isSubmitted);
                    },
                );

                // handle delete group when click button "Xoá" in Edit Group Dialog
                dialogRef.componentInstance.handleDelete.subscribe(
                    (dataDelete) => {
                        console.log('dataDelete', dataDelete);
                        const isDeleted = !!dataDelete;
                        listData.splice(indexItem, 1);
                        dialogRef.close(isDeleted);
                    },
                );

                dialogRef.afterClosed().subscribe((result) => {
                    if (!result) return;
                    listData = listData.sort((a, b) => a.index - b.index); // DESC List Group
                    this._toastrService.success('', 'Cập nhật thành công');
                });
                break;

            case 'DELETE':
                console.log(`indexItem::${indexItem}-${item.item.id}`);
                console.log(listData[indexItem]);
                // console.log('listData::', listData);
                listData.splice(indexItem, 1);
                this._toastrService.success('', 'Xóa thành công');
                break;

            case 'UPDATE_FIELD':
                // console.log('UPDATE_FIELD');
                listData.splice(indexItem, 1, item.item);
                break;
            default:
                break;
        }
    }

    addTab(selectAfterAdding: boolean, data: any, index?: number) {
        this.isAddGroupData = true;
        this.typeDialog = 'TAB';

        const fields = [
            {
                type: 'TEXT',
                referenceValue: 'code',
                name: 'Mã',
                defaultValue: '',
                required: '1',
                pattern: '^[a-zA-Z0-9-]*$',
                icon: 'code',
                css: 'col-12 col-lg-3',
            },
            {
                type: 'TEXT',
                referenceValue: 'name',
                name: 'Tên',
                defaultValue: '',
                required: '1',
                icon: 'code',
                css: 'col-12 col-lg-3',
            },
            {
                type: 'NUMBER',
                referenceValue: 'index',
                name: 'Vị trí',
                defaultValue: 1,
                required: '0',
                icon: 'code',
                css: 'col-12 col-lg-3',
            },
            {
                type: 'SELECT',
                referenceValue: 'type',
                name: 'Loại',
                defaultValue: 'Tab',
                required: '0',
                options: [
                    { key: 'Group', name: 'Group' },
                    { key: 'Field', name: 'Field' },
                    { key: 'Table', name: 'Table' },
                    { key: 'Tab', name: 'Tab' },
                ],
                css: 'col-12 col-lg-3',
            },
            {
                type: 'TEXTAREA',
                referenceValue: 'css',
                name: 'CSS',
                defaultValue: '',
                required: '0',
                css: 'col-12 col-lg-12',
            },
        ];

        let dialogRef = this.dialog.open(DynamicDialogComponent, {
            width: '70%',
            data: {
                typeDialog: this.typeDialog,
                datas: data,
                fields: fields,
            },
            panelClass: 'my-custom-dialog-class',
        });

        dialogRef.componentInstance.onSave.subscribe((dataCreate: any) => {
            const isSubmitted = !!dataCreate;

            let dataSubmit = {
                ...data,
                ...dataCreate,
            };

            if (!data) {
                dataSubmit = {
                    ...dataSubmit,
                    id: this.guid(),
                    items: [],
                };

                this.dataGroup.push(dataSubmit);
            } else {
                this.dataGroup[index] = dataSubmit;
                console.log('CHECK DATA GROUP: ', this.dataGroup);
            }

            if (selectAfterAdding) {
                this.selected.setValue(this.dataGroup.length - 1);
            }

            dialogRef.close(isSubmitted);
        });

        dialogRef.afterClosed().subscribe((result) => {
            if (!result) return;

            this.dataGroup = this.dataGroup.sort((a, b) => a.index - b.index); // DESC List Tab

            this._toastrService.success('', 'Lưu thành công');
        });
    }

    removeTab(index: number) {
        this.isDeleteTab = true;
    }

    editTab(index: any) {
        console.log('index::', index);
        console.log('item::', this.dataGroup[index]);
        // this.isDeleteTab = true;
    }

    cancelDelete() {
        this.isDeleteTab = false;
    }

    confirmRemoveTab(index: number) {
        this.dataGroup.splice(index, 1);
        this.isDeleteTab = false;
    }

    tabSelectionChanged(event) {
        // Get the selected tab
        this.isDeleteTab = false;
        // Call some method that you want
    }

    handleGroupField(item: any) {
        console.log('Item: ', item);
        this.isAddGroupFieldData = true;
        this.typeDialog = 'GROUP';

        const fields = [
            {
                type: 'TEXT',
                referenceValue: 'code',
                name: 'Mã',
                defaultValue: '',
                required: '1',
                pattern: '^[a-zA-Z0-9-]*$',
                icon: 'code',
                css: 'col-12 col-lg-3',
            },
            {
                type: 'TEXT',
                referenceValue: 'name',
                name: 'Tên',
                defaultValue: '',
                required: '1',
                icon: 'code',
                css: 'col-12 col-lg-3',
            },
            {
                type: 'NUMBER',
                referenceValue: 'index',
                name: 'Vị trí',
                defaultValue: 1,
                required: '0',
                icon: 'code',
                css: 'col-12 col-lg-3',
            },
            {
                type: 'SELECT',
                referenceValue: 'type',
                name: 'Loại',
                defaultValue: 'Group',
                required: '0',
                options: [
                    { key: 'Group', name: 'Group' },
                    { key: 'Field', name: 'Field' },
                    { key: 'Table', name: 'Table' },
                    { key: 'Tab', name: 'Tab' },
                ],
                css: 'col-12 col-lg-3',
            },
            {
                type: 'TEXTAREA',
                referenceValue: 'header',
                name: 'Header',
                defaultValue: '',
                required: '0',
                css: 'col-12 col-lg-12',
                hint: `VD: [{"label":"Mã bệnh","key":"code","type":"text"}]`,
            },
            {
                type: 'CHECKBOX',
                referenceValue: 'fixed',
                name: '',
                required: '0',
                options: [
                    {
                        key: 'LOCK',
                        name: 'Cố định số lượng dòng',
                        checked: true,
                    },
                ],
                css: 'col-12 col-lg-12',
            },
            {
                type: 'TEXTAREA',
                referenceValue: 'css',
                name: 'CSS',
                defaultValue: '',
                required: '0',
                css: 'col-12 col-lg-12',
            },
        ];

        let dialogRef = this.dialog.open(DynamicDialogComponent, {
            width: '70%',
            data: {
                typeDialog: this.typeDialog,
                datas: null,
                fields: fields,
            },
            panelClass: 'my-custom-dialog-class',
        });

        dialogRef.componentInstance.onSave.subscribe((dataCreate: any) => {
            const isSubmitted = !!dataCreate;

            const dataSubmit = {
                ...dataCreate,
                id: this.guid(),
                items: [],
            };

            item.items.push(dataSubmit);

            dialogRef.close(isSubmitted);
        });

        dialogRef.afterClosed().subscribe((result) => {
            if (!result) return;
            item.items = item.items.sort((a, b) => a.index - b.index);
            this._toastrService.success('', 'Lưu thành công');
        });
    }

    // Handle Button Delete
    handleDeleteForm(): void {
        console.log('DELETED!');
        let dataDelete = {
            id: this.data.id,
        };
        this._categoryService.deleteForm(dataDelete).subscribe(
            (data) => {
                this._toastrService.success('', 'Xoá thành công');
                this._router.navigate(['manager/', 'sys/manager/forms']);
            },
            (err: HttpErrorResponse) => {
                this._toastrService.error(err.error.details, err.error.message);
            },
        );
    }

    submitForm() {
        let dataValue = {
            ...this.data,
            ...this.form.value,
            valueData: JSON.stringify(this.dataGroup || []),
        };

        console.log('Check: ', dataValue);

        this._categoryService.updateForm(dataValue).subscribe(
            (res) => {
                console.log('Done update', res);
                this._toastrService.success('', 'Lưu thành công');
            },
            (err: HttpErrorResponse) => {
                this._toastrService.error(err.error.details, err.error.message);
            },
        );

        // let _dataValue =  _data.dataValue
    }
}
