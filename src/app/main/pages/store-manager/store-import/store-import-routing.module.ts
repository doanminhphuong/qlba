import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CustomerImportComponent } from './customer-import/customer-import.component';
import { CustommerAddEditComponent } from './customer-import/custommer-add-edit/custommer-add-edit.component';
import { ListImportAddEditComponent } from './list-import/list-import-add-edit/list-import-add-edit.component';
import { ListImportComponent } from './list-import/list-import.component';
import { SummaryImportAddEditComponent } from './summary-import/summary-import-add-edit/summary-import-add-edit.component';
import { SummaryImportComponent } from './summary-import/summary-import.component';

const routes: Routes = [
    {
        path: '',
        redirectTo: 'danh-sach-phieu-nhap',
        pathMatch: 'full',
    },
    {
        path: 'danh-sach-phieu-nhap',
        children: [
            {
                path: '',
                component: ListImportComponent,
            },
        ],
    },

    {
        path: 'tong-nhap-kho',
        children: [
            {
                path: '',
                component: SummaryImportComponent,
            },
            {
                path: 'create',
                component: SummaryImportAddEditComponent,
            },
            {
                path: 'detail/:id',
                component: SummaryImportAddEditComponent,
            },
        ],
    },
    {
        path: 'bang-ke-mua-hang',
        children: [
            {
                path: '',
                component: CustomerImportComponent,
            },
            {
                path: 'create',
                component: CustommerAddEditComponent,
            },
            {
                path: 'edit/:id',
                component: CustommerAddEditComponent,
            },
        ],
    },
    {
        path: 'tao-phieu-nhap-kho',
        component: ListImportAddEditComponent,
    },
    {
        path: 'create',
        component: ListImportAddEditComponent,
    },
    {
        path: 'edit/:id',
        component: ListImportAddEditComponent,
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class StoreImportRoutingModule {}
