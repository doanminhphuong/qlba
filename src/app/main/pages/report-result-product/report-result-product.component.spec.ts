import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportResultProductComponent } from './report-result-product.component';

describe('ReportResultProductComponent', () => {
  let component: ReportResultProductComponent;
  let fixture: ComponentFixture<ReportResultProductComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportResultProductComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportResultProductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
