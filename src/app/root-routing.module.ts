﻿import { NgModule } from '@angular/core';
import {
    Routes,
    RouterModule,
    PreloadAllModules,
    ExtraOptions,
} from '@angular/router';
import { HomeComponent } from '@app/home/home.component';
import { CustomPreloadingStrategy } from './custom-preloading';

const routes: Routes = [
    {
        path: '',
        loadChildren: 'app/main/app.module#AppModule', //Lazy load account module
        data: { preload: true },
    },
];

const config: ExtraOptions = {
    useHash: false,
    preloadingStrategy: CustomPreloadingStrategy,
};

@NgModule({
    imports: [RouterModule.forRoot(routes, config)],
    exports: [RouterModule],
    providers: [CustomPreloadingStrategy],
})
export class RootRoutingModule {}
