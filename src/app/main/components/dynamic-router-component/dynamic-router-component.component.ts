import { SelectionModel } from '@angular/cdk/collections';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatPaginator, MatTableDataSource } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { DeleteConfirmDialogComponent } from '@app/_shared/dialogs/delete-confirm-dialog/delete-confirm-dialog.component';
import { CategoryService } from '@core/services/category.service';
import { FieldService } from '@core/services/field.service';
import { ToastrService } from '@core/services/toastr.service';
import { TokenStorageService } from '@core/services/token.service';
import { DynamicDialogComponent } from '../dynamic-dialog-component/dynamic-dialog-component.component';

export interface PeriodicElement {
    referenceValue: string;
    position: number;
    weight: number;
    symbol: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
    { position: 1, referenceValue: 'Hydrogen', weight: 1.0079, symbol: 'H' },
    { position: 2, referenceValue: 'Helium', weight: 4.0026, symbol: 'He' },
    { position: 3, referenceValue: 'Lithium', weight: 6.941, symbol: 'Li' },
    { position: 4, referenceValue: 'Beryllium', weight: 9.0122, symbol: 'Be' },
    { position: 5, referenceValue: 'Boron', weight: 10.811, symbol: 'B' },
    { position: 6, referenceValue: 'Carbon', weight: 12.0107, symbol: 'C' },
    { position: 7, referenceValue: 'Nitrogen', weight: 14.0067, symbol: 'N' },
    { position: 8, referenceValue: 'Oxygen', weight: 15.9994, symbol: 'O' },
    { position: 9, referenceValue: 'Fluorine', weight: 18.9984, symbol: 'F' },
    { position: 10, referenceValue: 'Neon', weight: 20.1797, symbol: 'Ne' },
];

/**
 * @title Table with selection
 */
@Component({
    selector: 'app-dynamic-router-component',
    templateUrl: './dynamic-router-component.component.html',
    styleUrls: ['./dynamic-router-component.component.scss'],
})
export class DynamicRouterComponent implements OnInit {
    @ViewChild(MatPaginator) paginator!: MatPaginator;

    displayedColumns: string[] = [
        'select',
        'index',
        'codeData',
        'name',
        'number1',
        'number2',
        'actions',
    ];

    displayedColumnsOfField: string[] = [
        'select',
        'code',
        'codeData',
        'name',
        'type',
        'defaultValue',
        'referenceValue',
        'dependenCodes',
        'actions',
    ];

    displayedColumnsOfForm: string[] = [
        'select',
        'index',
        'code',
        'name',
        'actions',
    ];

    dataSource = new MatTableDataSource<any>([]);
    selection = new SelectionModel<any>(true, []);

    valueData: string;
    data: any;
    dataList: any;

    title: string;

    isFormPage: boolean = false;
    isFieldPage: boolean = false;
    isEditField: boolean;
    typeDialog: string;
    fieldData: any[] = [];

    isCategoryPage: boolean = false;
    indexCategory: number;

    constructor(
        private _activatedroute: ActivatedRoute,
        private _router: Router,
        public dialog: MatDialog,
        private _categoryService: CategoryService,
        private _fieldService: FieldService,
        private _toastrService: ToastrService,
        private _tokenStorageService: TokenStorageService,
    ) {}

    ngOnInit() {
        this.getInitData();

        // console.log("this.data", this.data);
        // console.log("environment url", environment.baseUrl);
    }
    resetData() {
        // Reset checkbox when redirect router
        this.selection.clear();
        this.isFormPage = false; //reset page type
        this.isFieldPage = false;
    }

    getInitData(): void {
        // console.log(this._tokenStorageService.getToken());
        this.data = this._activatedroute.paramMap.subscribe((params) => {
            this.resetData();
            this.valueData = params.get('valueData');
            this.title = params.get('name');
            console.log(this.valueData);
            this.isCategoryPage = true;
            // valueData trả về có dạng: /app/category/gioi-tinh.
            // Cần lấy "gioi-tinh" để get data
            // CODE xử lý: split tách thành mảng r lấy phần tử cuối vs pop
            let keyValue = this.valueData.split('/').pop();
            // console.log('Key value: ', keyValue);

            let data = {
                maxResultCount: 10,
                skipCount: 0,
                sorting: 'Code',
                criterias: [
                    {
                        propertyName: 'GroupCode',
                        operation: 6,
                        value: keyValue,
                    },
                ],
            };

            // console.log("data::", data);

            this._categoryService.getAllCategory(data).subscribe((data) => {
                this.dataList = data.result;

                if (this.dataList.length > 0) {
                    this.indexCategory =
                        this.dataList[this.dataList.length - 1].index;
                } else {
                    this.indexCategory = 0;
                }

                this.dataSource = new MatTableDataSource<any>(data.result);
                this.dataSource.paginator = this.paginator;
            });
        });
    }

    /** Whether the number of selected elements matches the total number of rows. */
    isAllSelected() {
        const numSelected = this.selection.selected.length;
        const numRows = this.dataSource.data.length;
        return numSelected === numRows;
    }

    /** Selects all rows if they are not all selected; otherwise clear selection. */
    toggleAllRows() {
        if (this.isAllSelected()) {
            this.selection.clear();
            return;
        }

        this.selection.select(...this.dataSource.data);
    }

    /** The label for the checkbox on the passed row */
    checkboxLabel(row?: any): string {
        if (!row) {
            return `${this.isAllSelected() ? 'deselect' : 'select'} all`;
        }
        return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${
            row.index + 1
        }`;
    }

    openDialog_2(dataDialog: any): void {
        console.log(dataDialog);
        let fields = [];
        // Check đang ở page nào để hiện thị dialog
        if (this.isFormPage) {
            this.typeDialog = 'form';
            fields = [
                {
                    type: 'TEXT',
                    referenceValue: 'code',
                    name: 'Mã',
                    defaultValue: '',
                    required: '1',
                    pattern: '^[a-zA-Z0-9-]*$',
                    icon: 'code',
                    css: 'col-12 col-lg-3',
                },
                {
                    type: 'TEXT',
                    referenceValue: 'name',
                    name: 'Tên',
                    defaultValue: '',
                    required: '1',
                    icon: 'code',
                    css: 'col-12 col-lg-6',
                },
                {
                    type: 'SELECT',
                    referenceValue: 'type',
                    name: 'Trạng thái',
                    defaultValue: 'DEFAULT',
                    required: '0',
                    options: [
                        { key: 'DEFAULT', name: 'Mặc định' },
                        { key: 'LOCK', name: 'Khoá' },
                    ],
                    css: 'col-12 col-lg-3',
                },
                {
                    type: 'TEXTAREA',
                    referenceValue: 'language',
                    name: 'Ngôn ngữ',
                    defaultValue: '',
                    required: '0',
                    css: 'col-12 col-lg-12',
                },
                {
                    type: 'TEXTAREA',
                    referenceValue: 'description',
                    name: 'Ghi chú',
                    defaultValue: '',
                    required: '0',
                    icon: 'title',
                    css: 'col-12 col-lg-12',
                },
            ];
        } else if (this.isFieldPage) {
            this.typeDialog = 'field';
            fields = [
                {
                    type: 'TEXT',
                    referenceValue: 'code',
                    name: 'Mã',
                    defaultValue: '',
                    required: '1',
                    pattern: '^[a-zA-Z0-9-]*$',
                    icon: 'code',
                    css: 'col-12 col-lg-4',
                    groupCode: 'THONGTIN',
                },
                {
                    type: 'TEXT',
                    referenceValue: 'codeData',
                    name: 'Mã ghi nhớ',
                    defaultValue: '',
                    required: '0',
                    icon: 'code',
                    css: 'col-12 col-lg-4',
                    groupCode: 'THONGTIN',
                },
                {
                    type: 'SELECT',
                    referenceValue: 'type',
                    name: 'Loại',
                    defaultValue: null,
                    required: '1',
                    options: [
                        { key: 'TEXT', name: 'Text' },
                        { key: 'TEXTAREA', name: 'Textarea' },
                        { key: 'NUMBER', name: 'Number' },
                        { key: 'DATETIME', name: 'Datetime' },
                        { key: 'CHECKBOX', name: 'Checkbox' },
                        { key: 'RADIO', name: 'Radio' },
                        { key: 'SELECT', name: 'Select' },
                        { key: 'MULTISELECT', name: 'Multiselect' },
                    ],
                    css: 'col-12 col-lg-4',
                    groupCode: 'THONGTIN',
                },
                {
                    type: 'TEXT',
                    referenceValue: 'name',
                    name: 'Tên',
                    defaultValue: '',
                    required: '1',
                    icon: 'title',
                    css: 'col-12 col-lg-6',
                    groupCode: 'THONGTIN',
                },
                {
                    type: 'NUMBER',
                    referenceValue: 'number1',
                    name: 'Vị trí',
                    defaultValue: 1,
                    required: '0',
                    icon: 'format_list_numbered',
                    css: 'col-12 col-lg-3',
                    groupCode: 'THONGTIN',
                },
                {
                    type: 'SELECT',
                    referenceValue: 'status',
                    name: 'Trạng thái',
                    defaultValue: 'DEFAULT',
                    required: '0',
                    options: [
                        { key: 'DEFAULT', name: 'Mặc định' },
                        { key: 'LOCK', name: 'Khóa' },
                    ],
                    css: 'col-12 col-lg-3',
                    groupCode: 'THONGTIN',
                },
                {
                    type: 'TEXT',
                    referenceValue: 'hideValue',
                    name: 'Từ khóa',
                    defaultValue: '',
                    required: '0',
                    icon: 'code',
                    css: 'col-12 col-lg-12',
                    groupCode: 'THONGTIN',
                },
                {
                    type: 'TEXTAREA',
                    referenceValue: 'language',
                    name: 'Ngôn ngữ',
                    defaultValue: '',
                    required: '0',
                    css: 'col-12 col-lg-12',
                    groupCode: 'THONGTIN',
                },

                {
                    type: 'TEXT',
                    referenceValue: 'defaultValue',
                    name: 'Giá trị mặc định',
                    defaultValue: '',
                    required: '0',
                    icon: 'texture',
                    css: 'col-12 col-lg-6',
                    groupCode: 'CAIDAT',
                },
                {
                    type: 'TEXT',
                    referenceValue: 'referenceValue',
                    name: 'Giá trị liên kết',
                    defaultValue: '',
                    required: '0',
                    icon: 'link',
                    css: 'col-12 col-lg-6',
                    groupCode: 'CAIDAT',
                },
                {
                    type: 'SELECT',
                    referenceValue: 'required',
                    name: 'Bắt buộc',
                    defaultValue: '0',
                    required: '0',
                    options: [
                        { key: '0', name: 'Không' },
                        { key: '1', name: 'Có' },
                    ],
                    css: 'col-12 col-lg-6',
                    groupCode: 'CAIDAT',
                },
                {
                    type: 'TEXT',
                    referenceValue: 'format',
                    name: 'Định dạng',
                    defaultValue: '',
                    required: '0',
                    icon: 'link',
                    css: 'col-12 col-lg-6',
                    groupCode: 'CAIDAT',
                },
                {
                    type: 'TEXT',
                    referenceValue: 'validate',
                    name: 'Tính toàn vẹn',
                    defaultValue: '',
                    required: '0',
                    icon: 'texture',
                    css: 'col-12 col-lg-4',
                    groupCode: 'CAIDAT',
                },
                {
                    type: 'TEXT',
                    referenceValue: 'dependenCodes',
                    name: 'Field phụ thuộc',
                    defaultValue: '',
                    required: '0',
                    icon: 'texture',
                    css: 'col-12 col-lg-4',
                    groupCode: 'CAIDAT',
                },
                {
                    type: 'TEXT',
                    referenceValue: 'category',
                    name: 'Danh mục',
                    defaultValue: '',
                    required: '0',
                    icon: 'texture',
                    css: 'col-12 col-lg-4',
                    groupCode: 'CAIDAT',
                },
                {
                    type: 'TEXT',
                    referenceValue: 'formula',
                    name: 'Công thức',
                    defaultValue: '',
                    required: '0',
                    icon: 'texture',
                    css: 'col-12 col-lg-12',
                    groupCode: 'CAIDAT',
                },
                {
                    type: 'TEXTAREA',
                    referenceValue: 'css',
                    name: 'CSS',
                    defaultValue: 'col-lg-12',
                    required: '0',
                    icon: 'texture',
                    css: 'col-12 col-lg-12',
                    groupCode: 'CAIDAT',
                },
            ];
        } else {
            this.typeDialog = 'category';
            fields = [
                {
                    type: 'NUMBER',
                    referenceValue: 'index',
                    name: 'Vị trí',
                    defaultValue: this.indexCategory + 1,
                    required: '1',
                    icon: 'format_list_numbered',
                    css: 'col-12 col-lg-6',
                },
                {
                    type: 'TEXT',
                    referenceValue: 'codeData',
                    name: 'Mã',
                    defaultValue: '',
                    required: '1',
                    icon: 'code',
                    css: 'col-12 col-lg-6',
                },
                {
                    type: 'TEXT',
                    referenceValue: 'name',
                    name: 'Tên',
                    defaultValue: '',
                    required: '1',
                    icon: 'title',
                    css: 'col-12 col-lg-12',
                },
                {
                    type: 'NUMBER',
                    referenceValue: 'number1',
                    name: 'Giá trị 1',
                    defaultValue: 0,
                    required: '0',
                    icon: 'filter_vintage',
                    css: 'col-12 col-lg-6',
                },
                {
                    type: 'NUMBER',
                    referenceValue: 'number2',
                    name: 'Giá trị 2',
                    defaultValue: 0,
                    required: '0',
                    icon: 'filter_vintage',
                    css: 'col-12 col-lg-6',
                },
            ];
        }

        let dialogRef = this.dialog.open(DynamicDialogComponent, {
            // disableClose: true,
            width: '70%',
            data: {
                datas: dataDialog,
                fields: fields,
                typeDialog: this.typeDialog,
            },
            panelClass: 'my-custom-dialog-class',
        });

        dialogRef.componentInstance.onSave.subscribe((formValues) => {
            const isSubmitted = !!formValues;

            // Handle dialog for Category
            if (this.typeDialog === 'category') {
                let dataSubmit = {
                    ...dataDialog,
                    ...formValues,
                    groupCode: this.valueData.split('/').pop(),
                };

                if (!dataDialog) {
                    this._categoryService.createCategory(dataSubmit).subscribe(
                        (res) => dialogRef.close(isSubmitted),
                        (err) => this._toastrService.errorServer(err),
                    );
                } else {
                    this._categoryService.updateCategory(dataSubmit).subscribe(
                        (res) => dialogRef.close(isSubmitted),
                        (err) => this._toastrService.errorServer(err),
                    );
                }
            }

            //Handle dialog for Field
            if (this.typeDialog === 'field') {
                let dataSubmit = {
                    ...dataDialog,
                    ...formValues,
                };

                if (!dataDialog) {
                    this._fieldService.createField(dataSubmit).subscribe(
                        (res) => dialogRef.close(isSubmitted),
                        (err) => this._toastrService.errorServer(err),
                    );
                } else {
                    this._fieldService.updateField(dataSubmit).subscribe(
                        (res) => dialogRef.close(isSubmitted),
                        (err) => this._toastrService.errorServer(err),
                    );
                }
            }

            //Handle dialog for Form
            if (this.typeDialog === 'form') {
                let dataSubmit = formValues;

                this._categoryService.createForm(dataSubmit).subscribe(
                    (res) => dialogRef.close(isSubmitted),
                    (err) => this._toastrService.errorServer(err),
                );
            }
        });

        dialogRef.componentInstance.handleDelete.subscribe((dataDelete) => {
            const isDeleted = !!dataDelete;

            // Delete Category
            if (this.typeDialog === 'category') {
                this._categoryService.deleteCategory(dataDelete).subscribe(
                    (res) => dialogRef.close(isDeleted),
                    (err) => this._toastrService.errorServer(err),
                );
            }

            // Delete Field
            if (this.typeDialog === 'field') {
                this._fieldService.deleteField(dataDelete).subscribe(
                    (res) => dialogRef.close(isDeleted),
                    (err) => this._toastrService.errorServer(err),
                );
            }
        });

        dialogRef.afterClosed().subscribe((result) => {
            if (!result) return;

            this.getInitData();

            if (!dataDialog) {
                this._toastrService.success('', 'Thêm thành công');
            } else {
                this._toastrService.success('', 'Cập nhật thành công');
            }
        });
    }

    createAction(): void {
        this.openDialog_2(null);
    }

    editAction(element: any): void {
        if (this.isFormPage) {
            this._router.navigateByUrl(
                '/manager/sys-manager-forms/details/' + element.id,
            );
        } else {
            this.openDialog_2(element);
        }
    }

    deleteListItem() {
        const dialogRef = this.dialog.open(DeleteConfirmDialogComponent, {
            width: '500px',
        });

        dialogRef.componentInstance.handleDelete.subscribe((dataDelete) => {
            const isSubmitted = !dataDelete;

            this.selection.selected.forEach((item) => {
                const dataDelete = {
                    id: item.id,
                };

                this._categoryService.deleteCategory(dataDelete).subscribe(
                    (res) => dialogRef.close(isSubmitted),
                    (err) => this._toastrService.errorServer(err),
                );
            });
        });

        dialogRef.afterClosed().subscribe((result) => {
            if (!result) return;

            this.getInitData();
            this._toastrService.success('', 'Xóa thành công');
        });
    }
}
