import { DatePipe } from '@angular/common';
import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import {
    DateAdapter,
    MatDialogRef,
    MatPaginator,
    MatTableDataSource,
    MAT_DATE_FORMATS,
    MAT_DATE_LOCALE,
    MAT_DIALOG_DATA,
} from '@angular/material';
import {
    MomentDateAdapter,
    MAT_MOMENT_DATE_ADAPTER_OPTIONS,
} from '@angular/material-moment-adapter';
import { Router } from '@angular/router';
import {
    MaterialDialogComponent,
    MY_FORMATS,
} from '@app/_shared/dialogs/material-dialog/material-dialog.component';
import { CommonService } from '@core/services/common.service';
import { InventoryService } from '@core/services/inventory.service';

@Component({
    selector: 'app-inventory-details-dialog',
    templateUrl: './inventory-details-dialog.component.html',
    styleUrls: ['./inventory-details-dialog.component.scss'],
    providers: [
        {
            provide: DateAdapter,
            useClass: MomentDateAdapter,
            deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS],
        },
        { provide: MAT_DATE_LOCALE, useValue: 'vi-VI' },
        { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
        {
            provide: MAT_MOMENT_DATE_ADAPTER_OPTIONS,
            useValue: { useUtc: true },
        },
        DatePipe,
    ],
})
export class InventoryDetailsDialogComponent implements OnInit {
    displayedColumnsOfField: string[] = [
        'licenseType',
        'licenseDate',
        'licenseNumber',
        'unit',
        'yesterday',
        'deviant',
        'amount',
        'note',
        'action',
    ];

    dataSource = new MatTableDataSource<any>([]);
    @ViewChild(MatPaginator) paginator!: MatPaginator;

    isPicker: boolean = false;
    dateStart = new FormControl('');
    dateEnd = new FormControl('');
    codeName = new FormControl('');
    defaultSelected = 'all';
    minDate;

    constructor(
        @Inject(MAT_DIALOG_DATA) public data: any,
        public dialogRef: MatDialogRef<MaterialDialogComponent>,
        private inventoryService: InventoryService,
        private _router: Router,
        private _commonService: CommonService,
    ) {
        if (this.data.allowExec === false) {
            this.displayedColumnsOfField.splice(8, 1);
        }
    }

    ngOnInit() {
        this.getInitData();
    }

    getInitData() {
        let bodyRequest = {
            maxResultCount: 9999999,
            sorting: 'creationTime ASC',
            criterias: [
                {
                    propertyName: 'tonKhoDccdId',
                    operation: 'Equals',
                    value: this.data.id,
                },
            ],
        };

        this._commonService
            .callDataAPIShort(
                '/api/services/read/LichSuTonKhoDccd/GetAll',
                bodyRequest,
            )
            .subscribe((response) => {
                this.dataSource = new MatTableDataSource<any>(
                    response.result.items,
                );
                this.dataSource.paginator = this.paginator;
            });
    }

    enablePicker(event) {
        if (event.value) {
            this.isPicker = true;
            this.minDate = event.value;
        }
    }

    checkPositiveNumbers(number) {
        if (number > 0) {
            return '+' + number;
        } else if (number < 0) {
            return number;
        } else return '';
    }

    onCloseDialog(data?: any): void {
        this.dialogRef.close(data);
    }

    navigateDetail(element, type) {
        let typeAction = 3;
        if (type === 'tinhKho') {
            this._router.navigate([
                'app/dung-cu-cap-duong/tinh-kho-dccd/detail/' + element.phieuId,
            ]);
        } else if (type === 'nhapKho') {
            this._router.navigate([
                'app/dung-cu-cap-duong/ds-pnk-dccd/edit/' + element.phieuId,
                { typeAction },
            ]);
        }
        this.onCloseDialog();
    }

    filter() {
        let bodyRequest = {
            maxResultCount: 9999999,
            sorting: 'creationTime ASC',
            criterias: [
                {
                    propertyName: 'tonKhoDccdId',
                    operation: 'Equals',
                    value: this.data.id,
                },
                // Search maPhieu
                {
                    propertyName: this.codeName.value ? 'maPhieu' : '',
                    operation: 'Contains',
                    value: this.codeName.value ? this.codeName.value : '',
                },
                // Search loaiPhieu
                {
                    propertyName:
                        this.defaultSelected !== 'all' ? 'loaiPhieu' : '',
                    operation:
                        this.defaultSelected !== 'all' ? 'Equals' : 'Contains',
                    value:
                        this.defaultSelected !== 'all'
                            ? this.defaultSelected
                            : '',
                },
                // Search tuNgay
                {
                    propertyName: this.dateStart.value ? 'ngay' : '',
                    operation: this.dateStart.value
                        ? 'GreaterThanOrEqual'
                        : 'Contains',
                    value: this.dateStart.value
                        ? new Date(this.dateStart.value)
                              .toISOString()
                              .replace('T17', 'T00')
                        : '',
                },
                // Search denNgay
                {
                    propertyName: this.dateEnd.value ? 'ngay' : '',
                    operation: this.dateEnd.value
                        ? 'LessThanOrEqual'
                        : 'Contains',
                    value: this.dateEnd.value
                        ? new Date(this.dateEnd.value)
                              .toISOString()
                              .replace('T17', 'T00')
                        : '',
                },
            ],
        };

        this._commonService
            .callDataAPIShort(
                '/api/services/read/LichSuTonKhoDccd/GetAll',
                bodyRequest,
            )
            .subscribe((response) => {
                this.dataSource = new MatTableDataSource<any>(
                    response.result.items,
                );
                this.dataSource.paginator = this.paginator;
            });
    }

    reset() {
        this.defaultSelected = 'all';
        this.codeName.reset();
        this.dateStart.reset();
        this.dateEnd.reset();
        this.isPicker = false;
        this.getInitData();
    }

    handleSelectFilter(dataSelected: any) {
        this.defaultSelected = dataSelected.value;
    }
}
