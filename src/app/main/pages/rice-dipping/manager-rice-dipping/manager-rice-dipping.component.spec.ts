import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagerRiceDippingComponent } from './manager-rice-dipping.component';

describe('ManagerRiceDippingComponent', () => {
  let component: ManagerRiceDippingComponent;
  let fixture: ComponentFixture<ManagerRiceDippingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManagerRiceDippingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagerRiceDippingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
