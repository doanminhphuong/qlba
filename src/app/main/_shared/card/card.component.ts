import { Component, Input, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { CategoryService } from '@core/services/category.service';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {
  @Input() tabs: any[] = [];
  @Input() data: any;

  form: FormGroup;
  fields: any[] = [];

  constructor(private categoryService: CategoryService) { }

  ngOnInit() { }

  onSubmit(): void {

  }

}
