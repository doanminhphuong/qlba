import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BoilerAddEditComponent } from './boiler-add-edit.component';

describe('BoilerAddEditComponent', () => {
  let component: BoilerAddEditComponent;
  let fixture: ComponentFixture<BoilerAddEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BoilerAddEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BoilerAddEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
