import { Component, EventEmitter, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
    selector: 'app-dialog-reject-status',
    templateUrl: './dialog-reject-status.component.html',
    styleUrls: ['./dialog-reject-status.component.scss'],
})
export class DialogRejectStatusComponent implements OnInit {
    onSave = new EventEmitter();
    constructor(
        public dialogRef: MatDialogRef<DialogRejectStatusComponent>,
        @Inject(MAT_DIALOG_DATA) public data: string,
    ) {}

    ngOnInit() {}

    onSubmit() {
        console.log('SUbmit');
        this.onSave.emit();
    }
}
