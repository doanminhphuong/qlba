import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BASE_URL } from './data-service';

@Injectable({
  providedIn: 'root'
})
export class FoodyService {
  constructor(
    private http: HttpClient,
  ) { }

  // MÓN ĂN
  getAllMonAn(data: any): Observable<any> {
    const END_POINT = '/read/MonAn/GetAll';

    return this.http.post<any>(`${BASE_URL}${END_POINT}`, data);
  }

  getMonAnById(data: any): Observable<any> {
    const END_POINT = '/read/MonAn/Get';

    return this.http.post<any>(`${BASE_URL}${END_POINT}`, data);
  }

  createMonAn(data: any): Observable<any> {
    const END_POINT = '/write/MonAn/Create';

    return this.http.post<any>(`${BASE_URL}${END_POINT}`, data);
  }

  updateMonAn(data: any): Observable<any> {
    const END_POINT = '/write/MonAn/Update';

    return this.http.post<any>(`${BASE_URL}${END_POINT}`, data);
  }

  deleteMonAn(data: any): Observable<any> {
    const END_POINT = '/write/MonAn/Delete';

    return this.http.post<any>(`${BASE_URL}${END_POINT}`, data);
  }

  // CHẤM CƠM
  getAllChamCom(data: any): Observable<any> {
    const END_POINT = '/read/ChamCom/GetAll';

    return this.http.post<any>(`${BASE_URL}${END_POINT}`, data);
  }

  getChamComById(data: any): Observable<any> {
    const END_POINT = '/read/ChamCom/Get';

    return this.http.post<any>(`${BASE_URL}${END_POINT}`, data);
  }

  createChamCom(data: any): Observable<any> {
    const END_POINT = '/write/ChamCom/Create';

    return this.http.post<any>(`${BASE_URL}${END_POINT}`, data);
  }

  updateChamCom(data: any): Observable<any> {
    const END_POINT = '/write/ChamCom/Update';

    return this.http.post<any>(`${BASE_URL}${END_POINT}`, data);
  }

  deleteChamCom(data: any): Observable<any> {
    const END_POINT = '/write/ChamCom/Delete';

    return this.http.post<any>(`${BASE_URL}${END_POINT}`, data);
  }

  // CHI TIẾT CHẤM CƠM
  getAllChiTietChamCom(data: any): Observable<any> {
    const END_POINT = '/read/ChiTietChamCom/GetAll';

    return this.http.post<any>(`${BASE_URL}${END_POINT}`, data);
  }

  getChiTietChamComById(data: any): Observable<any> {
    const END_POINT = '/read/ChiTietChamCom/Get';

    return this.http.post<any>(`${BASE_URL}${END_POINT}`, data);
  }

  createChiTietChamCom(data: any): Observable<any> {
    const END_POINT = '/write/ChiTietChamCom/ChamCom';

    return this.http.post<any>(`${BASE_URL}${END_POINT}`, data);
  }

  createChamComAn(data: any): Observable<any> {
    const END_POINT = '/write/ChiTietChamCom/ChamComAn';

    return this.http.post<any>(`${BASE_URL}${END_POINT}`, data);
  }

  updateChiTietChamCom(data: any): Observable<any> {
    const END_POINT = '/write/ChiTietChamCom/Update';

    return this.http.post<any>(`${BASE_URL}${END_POINT}`, data);
  }

  // ĐỢT QUÂN SỐ ĂN
  getAllDotQuanSoAn(data: any): Observable<any> {
    const END_POINT = '/read/DotQuanSoAn/GetAll';

    return this.http.post<any>(`${BASE_URL}${END_POINT}`, data);
  }

  getChiTietDotQuanSoAn(data: any): Observable<any> {
    const END_POINT = '/read/ChiTietDotQuanSoAn/GetAll';

    return this.http.post<any>(`${BASE_URL}${END_POINT}`, data);
  }

  // BẾP
  getAllDauMoiBep(data: any): Observable<any> {
    const END_POINT = '/read/DauMoiBep/GetAll';

    return this.http.post<any>(`${BASE_URL}${END_POINT}`, data);
  }

  // BÁO CÁO CHI TIÊU TIỀN ĂN
  getBaoCaoChiTieuTienAn(data: any): Observable<any> {
    const END_POINT = '/read/BaoCao/GetBaoCaoChiTieuTienAn';

    return this.http.post<any>(`${BASE_URL}${END_POINT}`, data);
  }
}
