import { SelectionModel } from '@angular/cdk/collections';
import {
    AfterViewChecked,
    AfterViewInit,
    ChangeDetectorRef,
    Component,
    ElementRef,
    EventEmitter,
    Inject,
    OnInit,
    ViewChild,
    ViewChildren,
    QueryList,
} from '@angular/core';
import {
    MatDialogRef,
    MatPaginator,
    MatSort,
    MatTableDataSource,
    MAT_DIALOG_DATA,
} from '@angular/material';
import { CategoryService } from '@core/services/category.service';
import { FoodFuelService } from '@core/services/food-fuel.service';
import { PriceService } from '@core/services/price.service';
import { Subject, Subscription } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';
import { forkJoin, Observable } from 'rxjs';

import {
    MomentDateAdapter,
    MAT_MOMENT_DATE_ADAPTER_OPTIONS,
} from '@angular/material-moment-adapter';
import {
    DateAdapter,
    MAT_DATE_FORMATS,
    MAT_DATE_LOCALE,
} from '@angular/material/core';
import { InventoryService } from '@core/services/inventory.service';
import { ToastrService } from '@core/services/toastr.service';
import { CommonService } from '@core/services/common.service';
import { DataService } from '@core/services/data-service';

export const MY_FORMATS = {
    parse: {
        dateInput: 'DD/MM/YYYY', // this is how your date will be parsed from Input
    },
    display: {
        dateInput: 'DD/MM/YYYY', // this is how your date will get displayed on the Input
        monthYearLabel: 'MMMM YYYY',
        dateA11yLabel: 'LL',
        monthYearA11yLabel: 'MMMM YYYY',
    },
};

export interface MaterialElement {
    select?: any;
    id: string;
    code: string;
    name: string;
    group: string;
    unit: string;
    type: string;
    status: boolean;
    giaMuaNgoai?: number;
    giaTGSX?: number;
    ghiChu?: number;
}

@Component({
    selector: 'app-maintenance-dialog',
    templateUrl: './maintenance-dialog.component.html',
    styleUrls: ['./maintenance-dialog.component.scss'],
    providers: [
        // `MomentDateAdapter` can be automatically provided by importing `MomentDateModule` in your
        // application's root module. We provide it at the component level here, due to limitations of
        // our example generation script.
        {
            provide: DateAdapter,
            useClass: MomentDateAdapter,
            deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS],
        },

        { provide: MAT_DATE_LOCALE, useValue: 'vi-VI' },
        { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
        {
            provide: MAT_MOMENT_DATE_ADAPTER_OPTIONS,
            useValue: { useUtc: true },
        },
    ],
})
export class MaintenanceDialogComponent
    implements OnInit, AfterViewInit, AfterViewChecked {
    displayedColumnsAdjustment: string[] = [
        'select',
        'maLttpChatDot',
        'tenLttpChatDot',
        'donViTinh',
        'giaThiTruong',
        'giaTangGiaSanXuat',
        'value10',
    ];

    displayedColumnsAdjustmentPurchase: string[] = [
        'select',
        'maLttpChatDot',
        'tenLttpChatDot',
        'donViTinh',
        'giaThiTruong',
        'giaTangGiaSanXuat',
        'soLuong',
    ];

    displayedColumnsAdjustmentPurchaseUpgrade: string[] = [
        'select',
        'maLttpChatDot',
        'tenLttpChatDot',
        'donViTinh',
        'address',
        'typeOfPrice',
        'valueOfPrice',
        'soLuongThucNhap',
    ];

    displayedColumnsInventory: string[] = [
        'select',
        'maLttpChatDot',
        'tenLttpChatDot',
        'phanLoai',
        'donViTinh',
        'soluongTonKho',
        'donGia',
        'ngayHetHan',
    ];

    displayedColumnsInventoryImport: string[] = [
        'select',
        'maDungCuCapDuong',
        'tenDungCuCapDuong',
        'donViTinh',
        'phanHang',
        'soLuong',
    ];

    displayedColumnsInventoryTransfer: string[] = [
        'select',
        'tenLttpChatDot',
        'phanLoai',
        'donViTinh',
        'soLuong',
        'donGia',
        'soluongTonKho',
    ];

    displayedColumnsInventoryExport: string[] = [
        'select',
        'tenLttpChatDot',
        'phanLoai',
        'donViTinh',
        'soLuongPhaiXuat',
        'soLuongThucXuat',
        'donGia',
        'soluongTonKho',
    ];

    displayedColumnsPurchase: string[] = [
        'select',
        'maLttpChatDot',
        'tenLttpChatDot',
        'phanLoai',
        'donViTinh',
        'soLuong',
    ];

    displayedColumnsFoody: string[] = [
        'select',
        'maLttpChatDot',
        'tenLttpChatDot',
        'tenNhomLttpChatDot',
        'donViTinh',
        'phanLoai',
    ];

    currentDate: Date = new Date();

    dataSource = new MatTableDataSource<any>([]);
    selection = new SelectionModel<any>(true, []);
    onSave = new EventEmitter();

    @ViewChild(MatPaginator) paginator!: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;
    @ViewChildren('handlePriceElement')
    handlePriceElement: QueryList<ElementRef>;

    public applyFilter = new Subject<KeyboardEvent>();
    private subscription: Subscription;

    priceAdjustmentList: any[] = [];
    priceAdjustmentId: number;
    maintenanceList: any[] = [];
    unitOptions: any[] = [];
    typeOptions: any[] = [];
    isPurchasePrice: boolean = false;
    availableList: any[] = [];

    isShow: boolean = false;
    warehouseList: any[] = [];
    inventoryList: any[] = [];
    listRank = [
        {
            value: 1,
            text: 1,
        },
        {
            value: 2,
            text: 2,
        },
        {
            value: 3,
            text: 3,
        },
    ];

    dauMoiBepId: number;

    constructor(
        private readonly changeDetectorRef: ChangeDetectorRef,
        public dialogRef: MatDialogRef<MaintenanceDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any,
        private _categoryService: CategoryService,
        private _foodFuelService: FoodFuelService,
        private _priceService: PriceService,
        private _inventoryService: InventoryService,
        private elRef: ElementRef<HTMLInputElement>,
        private _toastrService: ToastrService,
        private _commonService: CommonService,
        private _dataService: DataService,
    ) {
        this.subscription = this.applyFilter
            .pipe(
                map((event) => (event.target as HTMLInputElement).value),
                debounceTime(500),
                distinctUntilChanged(),
            )
            .subscribe((filterValue) => this.handleInputFilter(filterValue));
    }

    ngOnInit() {
        this.dauMoiBepId = this._dataService.getDauMoiBepId();
        this.getAllMaintenanceTool();
        this.getMultiCategories();
    }

    ngAfterViewInit(): void { }

    ngAfterViewChecked(): void {
        this.changeDetectorRef.detectChanges();
    }

    ngOnDestroy(): void {
        this.subscription.unsubscribe();
    }

    getAllMaintenanceTool(): void {
        const bodyMaintenance = {
            maxResultCount: 9999,
            skipCount: 0,
            sorting: 'Id',
            criterias: [
                {
                    propertyName: 'dauMoiBepId',
                    operation: 0,
                    value: this.dauMoiBepId,
                },
            ],
        };

        this._commonService
            .callDataAPIShort(
                '/api/services/read/DungCuCapDuong/GetAll',
                bodyMaintenance,
            )
            .subscribe((res) => {
                this.maintenanceList = res.result.items;
                this.getInitData();
            });

        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
    }

    getInitData(): void {
        if (this.data.type === 'ADJUSTMENT') {
            this.availableList = this.maintenanceList;
            this.dataSource.data = this.maintenanceList;

            this.getAvailableList();

            this._priceService.getAllPriceAdjustment({}).subscribe((res) => {
                this.priceAdjustmentList = res.result.items;

                if (this.priceAdjustmentList.length > 0) {
                    const priceAdjustId = {
                        id: this.priceAdjustmentList[0].id,
                    };

                    this._priceService
                        .getPriceAdjustmentById(priceAdjustId)
                        .subscribe((res) => {
                            const { chiTiet } = res.result;

                            // Get data of recently adjustment
                            const getDataById = (id, objKey) => {
                                const dataFound = chiTiet.find(
                                    (item) => item.lttpChatDotId === id,
                                );

                                return dataFound ? dataFound[objKey] : null;
                            };

                            // Patch data from recently adjustment
                            this.dataSource.data.forEach((item) => {
                                item.giaThiTruong = getDataById(
                                    item.id,
                                    'giaThiTruong',
                                );
                                item.giaTangGiaSanXuat = getDataById(
                                    item.id,
                                    'giaTangGiaSanXuat',
                                );
                                item.value10 = getDataById(item.id, 'value10');

                                if (item.giaThiTruong) {
                                    item.isEnable = true;
                                }
                            });

                            // const oldSelection = this.dataSource.data.filter(
                            //     (item) => item.giaThiTruong,
                            // );

                            // this.selection.select(...oldSelection);

                            if (this.data.currentTable.length > 0) {
                                this.selection.clear();
                            }
                        });
                }
            });
        }
        if (this.data.type === 'ADJUSTMENT_PURCHASE') {
            this.availableList = this.maintenanceList;
            this.dataSource.data = this.maintenanceList;

            this.getAvailableList();

            this._priceService.getAllPriceAdjustment({}).subscribe((res) => {
                this.priceAdjustmentList = res.result.items;

                if (this.priceAdjustmentList.length > 0) {
                    const priceAdjustId = {
                        id: this.priceAdjustmentList[0].id,
                    };

                    this._priceService
                        .getPriceAdjustmentById(priceAdjustId)
                        .subscribe((res) => {
                            const { chiTiet } = res.result;

                            // Get data of recently adjustment
                            const getDataById = (id, objKey) => {
                                const dataFound = chiTiet.find(
                                    (item) => item.lttpChatDotId === id,
                                );

                                return dataFound ? dataFound[objKey] : null;
                            };

                            // Patch data from recently adjustment
                            this.dataSource.data.forEach((item) => {
                                item.giaThiTruong = getDataById(
                                    item.id,
                                    'giaThiTruong',
                                );
                                item.giaTangGiaSanXuat = getDataById(
                                    item.id,
                                    'giaTangGiaSanXuat',
                                );
                                item.value10 = getDataById(item.id, 'value10');

                                if (item.giaThiTruong) {
                                    item.isEnable = true;
                                }
                            });

                            // const oldSelection = this.dataSource.data.filter(
                            //     (item) => item.giaThiTruong,
                            // );

                            // this.selection.select(...oldSelection);

                            if (this.data.currentTable.length > 0) {
                                this.selection.clear();
                            }
                        });
                }
            });
        }
        if (
            this.data.type === 'ADJUSTMENT_PURCHASE_UPGRADE' ||
            this.data.type === 'INVENTORY_IMPORT'
        ) {
            this.availableList = this.maintenanceList;
            this.dataSource.data = this.maintenanceList;

            // this.getAvailableList();
        }

        if (this.data.type === 'PURCHASE') {
            this.availableList = this.maintenanceList.filter((item) =>
                this.data.chiTietIds.includes(item.id),
            );
            this.dataSource.data = this.availableList;

            this.getAvailableList();
        }

        if (this.data.type === 'INVENTORY' || this.data.type === 'FOODY') {
            this.availableList = this.maintenanceList;
            this.dataSource.data = this.maintenanceList;

            if (this.data.type === 'FOODY') {
                this.getAvailableList();
            }
        }

        if (
            this.data.type === 'INVENTORY_TRANSFER' ||
            this.data.type === 'INVENTORY_EXPORT'
        ) {
            const bodyInventory = {
                criterias: [
                    {
                        propertyName: 'khoId',
                        operation: 0,
                        value: this.data.khoXuatId,
                    },
                    {
                        propertyName: 'ton',
                        operation: 'GreaterThan',
                        value: 0,
                    },
                ],
            };
            const dataInventory =
                this._inventoryService.getAllInventory(bodyInventory);

            forkJoin([dataInventory]).subscribe((results) => {
                this.inventoryList = results[0].result.items;
                this.inventoryList.forEach((item) => {
                    item.phanLoai = this.getInfoFoodFuel(
                        item.lttpChatDotId,
                        'phanLoai',
                    );
                    item.donViTinh = this.getInfoFoodFuel(
                        item.lttpChatDotId,
                        'donViTinh',
                    );
                    item.tonKhoId = item.id;
                    delete item.id;
                });

                console.log('this.inventoryList', this.inventoryList);

                this.availableList = this.inventoryList;
                this.dataSource.data = this.availableList;

                // this.availableList = this.maintenanceList.filter((item) =>
                //     materialIds.includes(item.id),
                // );

                // this.dataSource.data = this.availableList;

                // this.dataSource.data.forEach((item) => {
                //     item.tonKhoId = this.getDataOfItem(item.id, 'id');
                //     item.donGia = this.getDataOfItem(item.id, 'donGia');
                // });
            });
        }
    }

    getAvailableList(): void {
        if (this.data.currentTable.length === 0) return;
        const materialIdList = this.data.currentTable.map(
            (item) => item.dungCuCapDuongId,
        );

        this.availableList = this.dataSource.data.filter(
            (item) => !materialIdList.includes(item.id),
        );
        this.dataSource.data = this.availableList;
    }

    getMultiCategories() {
        this.getUnitOptions();
        this.getTypeOptions();
    }

    getBodyCategory(groupCode: string) {
        return {
            maxResultCount: 10,
            skipCount: 0,
            sorting: 'Code',
            criterias: [
                {
                    propertyName: 'GroupCode',
                    operation: 6,
                    value: groupCode,
                },
            ],
        };
    }

    getUnitOptions() {
        this._categoryService
            .getAllCategory(this.getBodyCategory('don-vi-tinh'))
            .subscribe((res) => {
                this.unitOptions = res.result.map((item) => ({
                    key: item.id,
                    name: item.name,
                }));
            });
    }

    getTypeOptions() {
        this._categoryService
            .getAllCategory(this.getBodyCategory('phan-loai-lttp-chat-dot'))
            .subscribe((res) => {
                this.typeOptions = res.result.map((item) => ({
                    key: item.codeData,
                    name: item.name,
                }));
            });
    }

    getObjectElementUser(key, type) {
        let data;
        let dataList = [];

        if (this.unitOptions.length > 0 && this.typeOptions.length > 0) {
            switch (type) {
                case 'UNIT':
                    dataList = this.unitOptions;
                    break;
                case 'TYPE':
                    dataList = this.typeOptions;
                    break;
            }
            let item = dataList.find((item) => item.key === key);
            data = item && item.name ? item.name : '';
        }

        return data;
    }

    getDataOfItem(id, objKey) {
        let result = '';

        if (this.inventoryList.length > 0) {
            const dataFound = this.inventoryList.find(
                (item) => item.lttpChatDotId === id,
            );

            result = dataFound ? dataFound[objKey] : '';
        }

        return result;
    }

    getInventoryOfItem(id) {
        let result = '';

        if (this.inventoryList.length > 0) {
            const dataFounds = this.inventoryList.filter(
                (item) => item.lttpChatDotId === id,
            );

            result = dataFounds.reduce(
                (acc, item) => acc + item.soluongTonKho,
                0,
            );
        }

        return result;
    }

    getInfoFoodFuel(id, objKey) {
        let result = '';

        if (this.maintenanceList.length > 0) {
            const dataFound = this.maintenanceList.find(
                (item) => item.id === id,
            );

            result = dataFound ? dataFound[objKey] : '';
        }

        return result;
    }

    typeOfPriceChanged(event, element, ids) {
        if (event.value === 'Khac') {
            element.handlePrice = element.handleRawPrice || 0;
            element.donGia = element.handleRawPrice || 0;
        } else {
            element.handlePrice = element[event.value];
            element.donGia = element[event.value];
        }

        if (event.value === 'Khac' || event.value === 'giaThiTruong') {
            element.loaiGia = 0;
        } else {
            element.loaiGia = 1;
        }

        element.handleType = event.value;
        this.handlePriceElement.toArray()[ids].nativeElement.focus(),
            setTimeout(() =>
                this.handlePriceElement.toArray()[ids].nativeElement.focus(),
            );
    }

    typeOfAddressChanged(event, element, ids) {
        element.diaChiMuaHang = event.value;
    }

    getRawDataPrice(event, element) {
        element.handlePrice = +event.target.value;
        element.donGia = +event.target.value;
    }

    /** Whether the number of selected elements matches the total number of rows. */
    isAllSelected() {
        const numSelected = this.selection.selected.length;
        const numRows = this.dataSource.data.length;
        return numSelected === numRows;
    }

    /** Selects all rows if they are not all selected; otherwise clear selection. */
    toggleAllRows() {
        if (this.isAllSelected()) {
            this.selection.clear();
            this.dataSource.data.forEach((item) => (item.isChecked = false));
        } else {
            this.selection.select(...this.dataSource.data);
            this.dataSource.data.forEach((item) => (item.isChecked = true));
        }
    }

    /** The label for the checkbox on the passed row */
    checkboxLabel(row?: MaterialElement): string {
        if (!row) {
            return `${this.isAllSelected() ? 'deselect' : 'select'} all`;
        }
        // return `${this.selection.isSelected(row) ? "deselect" : "select"} row ${row.position + 1}`;
        return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.select + 1
            }`;
    }

    // Input Filter
    handleInputFilter(filterValue) {
        this.dataSource.data = this.availableList.filter((item) =>
            item.tenDungCuCapDuong
                .toLowerCase()
                .includes(filterValue.trim().toLowerCase()),
        );
    }

    // Select Filter
    handleSelectFilter(event: any) {
        if (!event.value) {
            this.dataSource.data = this.availableList;
        } else {
            this.dataSource.data = this.availableList.filter(
                (item) => item.phanLoai === event.value,
            );
        }
    }

    handleSelectWarehouse(event: any) {
        const body = {
            criterias: [
                {
                    propertyName: 'khoId',
                    operation: 0,
                    value: event.value,
                },
            ],
        };

        this._inventoryService.getAllInventory(body).subscribe((res) => {
            const materialIds = res.result.items.map(
                (item) => item.lttpChatDotId,
            );

            this.availableList = this.maintenanceList.filter((item) =>
                materialIds.includes(item.id),
            );

            this.dataSource.data = this.availableList;

            this.dataSource.data.forEach((item) => {
                item.tonKhoId = this.getDataOfItem(item.id, 'id');
                item.donGia = this.getDataOfItem(item.id, 'donGia');
            });
        });
    }

    getDataFilter(filterValue) {
        this.dataSource.filter = filterValue.trim().toLowerCase();
        let dataSearch = {
            maxResultCount: 100,
            criterias: [
                {
                    propertyName: 'Search',
                    operation: 'OrEquals',
                    value: `[{"propertyName":"Name","operation":"Contains","value":"${filterValue}"}]`,
                },
            ],
            sorting: 'Number1',
            tenantId: 1,
            language: 'vi',
        };
        this._categoryService
            .searchFieldFormName(dataSearch)
            .subscribe((res: any) => {
                this.dataSource = new MatTableDataSource<MaterialElement>(
                    res.result,
                );
                this.dataSource.paginator = this.paginator;
            });

        if (this.dataSource.paginator) {
            this.dataSource.paginator.firstPage();
        }
    }

    onCloseDialog(data?: any): void {
        this.dialogRef.close(data);
    }

    selectCurrent(isChecked: boolean, dataRow: any): void {
        this.selection.toggle(dataRow);

        if (isChecked) {
            dataRow.isChecked = true;
        } else {
            dataRow.isChecked = false;
        }
    }

    handleValidatePurchase(event: any, dataRow: any) {
        const valuePurchase = event.target.value;

        if (valuePurchase !== '' && valuePurchase > 0) {
            dataRow.isEnable = true;
        } else {
            dataRow.isEnable = false;

            if (dataRow.isChecked) {
                dataRow.isChecked = false;
                this.selection.toggle(dataRow);
            }
        }
    }

    handleValidateAmount(event: any, dataRow: any) {
        const valueAmount = event.target.value;

        if (valueAmount !== '' && valueAmount > 0) {
            dataRow.isHaveAmount = true;
        } else {
            dataRow.isHaveAmount = false;

            if (dataRow.isChecked) {
                dataRow.isChecked = false;
                this.selection.toggle(dataRow);
            }
        }
    }

    enableToggleAllCheckbox(): boolean {
        return this.dataSource.data.every((item) => item.isEnable);
    }

    guid = () => {
        let s4 = () => {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        };
        //return id of format 'aaaaaaaa'-'aaaa'-'aaaa'-'aaaa'-'aaaaaaaaaaaa'
        return (
            s4() +
            s4() +
            '-' +
            s4() +
            '-' +
            s4() +
            '-' +
            s4() +
            '-' +
            s4() +
            s4() +
            s4()
        );
    };

    onSubmit() {
        this.selection.selected.forEach((item) => {
            if (!item.dungCuCapDuongId) {
                item.dungCuCapDuongId = item.id;
                delete item.id;
            }
        });

        let isError = false;

        if (this.data.type === 'INVENTORY_EXPORT') {
            for (const obj of this.selection.selected) {
                if (
                    ((!obj.soLuongPhaiXuat ||
                        !Number.isInteger(obj.soLuongPhaiXuat)) &&
                        !obj.soLuongThucXuat) ||
                    !Number.isInteger(obj.soLuongThucXuat)
                ) {
                    isError = true;
                    this._toastrService.error(
                        '',
                        'Chưa nhập thực xuất và phải xuất vật tư hàng hoá đã chọn',
                    );
                    break;
                }

                if (isError) {
                    return false;
                }
            }

            if (isError === false) {
                this.onSave.emit(this.selection.selected);
            }
        } else if (this.data.type === 'ADJUSTMENT_PURCHASE_UPGRADE') {
            for (const obj of this.selection.selected) {
                if (
                    ((!obj.soLuong || !Number.isInteger(obj.soLuong)) &&
                        !obj.soLuong) ||
                    !Number.isInteger(obj.soLuong)
                ) {
                    isError = true;
                    this._toastrService.error(
                        '',
                        'Chưa nhập số lượng vật tư hàng hoá đã chọn',
                    );
                    break;
                }

                if (isError) {
                    return false;
                }
            }

            if (isError === false) {
                this.onSave.emit(this.selection.selected);
            }
        } else if (this.data.type === 'INVENTORY_IMPORT') {
            for (const obj of this.selection.selected) {
                if (!obj.soLuong || !Number.isInteger(obj.soLuong)) {
                    isError = true;
                    this._toastrService.error(
                        '',
                        'Chưa nhập số lượng cho dụng cụ cấp dưỡng đã chọn',
                    );
                    break;
                }

                if (isError) {
                    return false;
                }
            }

            if (isError === false) {
                this.onSave.emit(this.selection.selected);
            }
        } else {
            this.onSave.emit(this.selection.selected);
        }
    }
}
