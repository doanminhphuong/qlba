import { importType } from '@angular/compiler/src/output/output_ast';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FoodAndFuelComponent } from './food-and-fuel/food-and-fuel.component';
import { StoreInventoryComponent } from './store-inventory/store-inventory.component';
import { StoreListComponent } from './store-list/store-list.component';

const routes: Routes = [
    {
        path: 'danh-sach-kho',
        component: StoreListComponent,
    },
    {
        path: 'luong-thuc-thuc-pham-chat-dot',
        component: FoodAndFuelComponent,
    },
    {
        path: 'nhap-kho',
        loadChildren: () =>
            import('./store-import/store-import.module').then(
                (m) => m.StoreImportModule,
            ),
    },
    {
        path: 'xuat-kho',
        loadChildren: () =>
            import('./store-export/store-export.module').then(
                (m) => m.StoreExportModule,
            ),
    },
    {
        path: 'ton-kho',
        component: StoreInventoryComponent,
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class StoreManagerRoutingModule {}
