import {
    ChangeDetectorRef,
    Component,
    EventEmitter,
    Inject,
    OnInit,
} from '@angular/core';
import {
    FormArray,
    FormBuilder,
    FormControl,
    FormGroup,
    Validators,
} from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { CategoryService } from '@core/services/category.service';
import { CircularService } from '@core/services/circular-service';
import { CommonService } from '@core/services/common.service';
import { FoodFuelService } from '@core/services/food-fuel.service';
import { InventoryService } from '@core/services/inventory.service';
import { PriceService } from '@core/services/price.service';
import { QuantityService } from '@core/services/quantity.service';
import { ToastrService } from '@core/services/toastr.service';
import { UserService } from '@core/services/user.service';
import { forkJoin } from 'rxjs';
import { DialogData } from '../category-dialog/category-dialog.component';

export interface ElementSelect {
    key: string;
    value: string;
}

export interface QuantityDetails {
    nhomLttpChatDotId: string;
    donViTinhId: string;
    dinhLuong: string;
    id?: number | string;
}

export class QuantiveItem {
    nhomLttpChatDotId = '';
    donViTinhId = '';
    dinhLuong = '';
    id?: number | string;
}

@Component({
    selector: 'app-dialog-quantitative',
    templateUrl: './dialog-quantitative.component.html',
    styleUrls: ['./dialog-quantitative.component.scss'],
})
export class DialogQuantitativeComponent implements OnInit {
    nameLTTP: string;
    donViTinhId: string;
    dinhLuong: string;
    listFoodFuel: any[] = [];
    listUnitCtegory: any[] = [];
    quantiveItemCurrent;
    listQuantityItem = [];
    quantiveId: any;
    onSave = new EventEmitter();
    dataEdit: any;
    nameCirculars: string | number;
    circular: Object;
    listCirculars: ElementSelect[] = [];
    isEdit: boolean = false;
    thongTuId: number = 0;
    form = this.fb.group({
        quantity: this.fb.array([]),
    });
    formQuantive = this._fb.group({
        quantity: this._fb.array([]),
    });
    // this.fb.group(new QuantiveItem())

    initDataTest = [
        {
            nhomLttpChatDotId: 1,
            donViTinhId: '347d86a7-a8d2-415d-edcc-08da8ee24d0e',
            dinhLuong: 3,
            tenantId: 1,
            id: 0,
        },
    ];

    constructor(
        public dialogRef: MatDialogRef<DialogQuantitativeComponent>,
        @Inject(MAT_DIALOG_DATA) public data: DialogData,
        private fb: FormBuilder,
        private _fb: FormBuilder,
        private _foodFuelService: FoodFuelService,
        private _categoryService: CategoryService,
        private commonService: CommonService,
        private _commonService: CommonService,
        private readonly changeDetectorRef: ChangeDetectorRef,
        private _activatedRoute: ActivatedRoute,
        private _toastrService: ToastrService,
        private _router: Router,
        public dialog: MatDialog,
        private _priceService: PriceService,
        private _userService: UserService,
        private _dinhLuongAnService: QuantityService,
        private _inventoryService: InventoryService,
        private _circularService: CircularService,
    ) {}

    get quantity() {
        return this.form.get('quantity') as FormArray;
    }

    ngOnInit() {
        this._activatedRoute.paramMap.subscribe((param) => {
            this.thongTuId = +param.get('thongTuId');
            this.quantiveId = +param.get('id');
            if (!this.quantiveId) return;
            this.isEdit = true;
        });
        this.getInitData();
    }

    createQuantitive(quantitive): FormGroup {
        return this.fb.group({
            ...quantitive,
            tenNhomLttpChatDot: new FormControl({
                value: quantitive.tenNhomLttpChatDot,
                disabled: true,
            }),
            donViTinh: new FormControl({
                value: quantitive.donViTinh,
                disabled: true,
            }),
            dinhLuong: new FormControl({
                value: quantitive.dinhLuong,
                disabled: true,
            }),
            id: [quantitive.id] || 0,
            tenantId: 1,
        });
    }

    getInitData(): void {
        let dataEdit = this.data.datas;
        if (!dataEdit) return;

        let bodydonViTinhId = {
            maxResultCount: 10,
            skipCount: 0,
            sorting: 'Code',
            criterias: [
                {
                    propertyName: 'GroupCode',
                    operation: 6,
                    value: 'don-vi-tinh',
                },
            ],
        };

        const dataAllFoodFuel = this._foodFuelService.getAllGroupOfFoodFuel({
            maxResultCount: 2147483647,
        });
        const dataUnitCtegory =
            this._categoryService.getAllCategory(bodydonViTinhId);

        const dataDinhLuongAn = this.commonService.callDataAPIShort(
            '/api/services/read/DinhLuongAn/Get',
            {
                id: dataEdit,
            },
        );

        forkJoin([dataAllFoodFuel, dataUnitCtegory, dataDinhLuongAn]).subscribe(
            (res) => {
                this._commonService.covertDataToKeyValueInSelect(
                    res[0].result.items,
                    this.listFoodFuel,
                    'id',
                    'tenNhomLttpChatDot',
                );
                this._commonService.covertDataToKeyValueInSelect(
                    res[1].result,
                    this.listUnitCtegory,
                    'id',
                    'name',
                );

                this.dataEdit = res[2].result;
                this.form = this._fb.group({
                    quantity: this._fb.array(
                        this.dataEdit.chiTiet.map((quantive) =>
                            this.createQuantitive(quantive),
                        ),
                    ),
                });
                this.formQuantive.patchValue({
                    quantity: this.listQuantityItem,
                });
            },
        );
    }

    addQuantitative() {
        const quantitativeForm = this.fb.group({
            nhomLttpChatDotId: [this.nameLTTP, Validators.required],
            donViTinhId: [this.donViTinhId, Validators.required],
            dinhLuong: [
                this.dinhLuong,
                [
                    Validators.required,
                    Validators.pattern('^[0-9]+(.[0-9]{0,2})?$'),
                ],
            ],
            tenantId: 1,
            id: 0,
            // dinhLuongAnId: this.data, //
        });

        // this.quantity.push(quantitativeForm);
        this.quantity.insert(0, quantitativeForm);
        this.resetValueData();
        // console.log(this.quantity.value);
    }

    deleteQuantitative(lessonIndex: number) {
        this.quantity.removeAt(lessonIndex);
    }
    clearAll() {
        this.quantity.removeAt(0);
    }
    updateQuantitative(lessonIndex: number) {
        // console.log(this.quantity.get(lessonIndex.toString()).value);
        let value = this.quantity.get(lessonIndex.toString()).value;
        this.nameLTTP = value.nhomLttpChatDotId;
        this.donViTinhId = value.donViTinhId;
        this.dinhLuong = value.dinhLuong;
    }

    resetValueData() {
        this.nameLTTP = '';
        this.donViTinhId = '';
        this.dinhLuong = '';
    }

    onCloseDialog(data?: any): void {
        this.dialogRef.close(data);
    }

    checkDisableMatOption(unit) {
        return this.quantity.value.some(
            (item) => item.nhomLttpChatDotId === unit,
        );
    }

    onSubmit() {
        this.onSave.emit(this.quantity.value);
        this.onCloseDialog();
    }
}
