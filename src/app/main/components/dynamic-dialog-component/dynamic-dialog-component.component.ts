import {
    AfterViewChecked,
    AfterViewInit,
    ChangeDetectorRef,
    Component,
    EventEmitter,
    Inject,
    OnInit,
} from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import {
    MatChipInputEvent,
    MatDialog,
    MatDialogRef,
    MAT_DIALOG_DATA,
} from '@angular/material';
import { CategoryService } from '@core/services/category.service';
import { FieldService } from '@core/services/field.service';
import { FoodFuelService } from '@core/services/food-fuel.service';
import { ToastrService } from '@core/services/toastr.service';

export interface DialogData {
    name: string;
    fields: any[];
    datas: any;
    typeDialog: string;
    action: string;
    groupCode: string;
}

@Component({
    selector: 'app-dynamic-dialog-component',
    templateUrl: './dynamic-dialog-component.component.html',
    styleUrls: ['./dynamic-dialog-component.component.scss'],
})
export class DynamicDialogComponent
    implements OnInit, AfterViewInit, AfterViewChecked
{
    onSave = new EventEmitter();
    handleDelete = new EventEmitter();

    form: FormGroup;
    fileName: any;
    fieldList: any[] = [];

    isSubmitted: boolean = false;
    isEnableDelete: boolean = false;

    // Thông tư
    base64Output;
    listFile = {
        attachments: [],
        tags: [],
    };

    objectFile;
    selectedFiles: FileList;
    listSelectedFiles;
    fileCtrl = new FormControl('');

    tabs = [
        {
            label: 'Thông tin',
            icon: 'fa fa-pencil-square-o',
            groupCode: 'THONGTIN',
        },
        {
            label: 'Cài đặt',
            icon: 'fa fa-sliders',
            groupCode: 'CAIDAT',
        },
    ];

    constructor(
        private readonly changeDetectorRef: ChangeDetectorRef,
        private _toastrService: ToastrService,
        private _categoryService: CategoryService,
        private _foodFuelService: FoodFuelService,
        public dialog: MatDialog,
        public dialogRef: MatDialogRef<DynamicDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: DialogData,
    ) {}

    ngOnInit() {
        this.getInitForm();
        this.getInitData();
        this.listSelectedFiles = [];
    }

    ngAfterViewInit(): void {
        this.getInitData();
    }

    ngAfterViewChecked(): void {
        this.changeDetectorRef.detectChanges();
    }

    getInitForm() {
        let fieldsCtrls = {};
        let fieldList = [];

        for (let f of this.data.fields) {
            fieldList = [...fieldList, f];
            if (f.type === 'NUMBER') {
                fieldsCtrls[f.referenceValue] = new FormControl(
                    { value: f.defaultValue, disabled: f.disabled },
                    f.required === '1' ? [Validators.required] : [],
                );
            } else if (f.type !== 'CHECKBOX') {
                let validators = [
                    Validators.minLength(f.minLength),
                    Validators.maxLength(f.maxLength),
                    Validators.pattern(f.pattern),
                ];

                if (f.type === 'EMAIL') {
                    validators = [...validators, Validators.email];
                }

                fieldsCtrls[f.referenceValue] = new FormControl(
                    { value: f.defaultValue, disabled: f.disabled },
                    f.required === '1'
                        ? [...validators, Validators.required]
                        : [...validators],
                );

                if (f.type === 'DATETIME') {
                    fieldsCtrls[f.referenceValue].valueChanges.subscribe(
                        (val) => {
                            const dependField = fieldList.find(
                                (x) => x.dependenField === f.referenceValue,
                            );

                            if (!dependField) return;

                            dependField.min = val;
                            fieldsCtrls[dependField.referenceValue].enable();
                        },
                    );
                }

                if (this.data.typeDialog === 'lttp') {
                    if (f.type === 'SELECT') {
                        fieldsCtrls[f.referenceValue].valueChanges.subscribe(
                            (val) => {
                                if (val !== '') {
                                    const dependedField = fieldList.find(
                                        (x) =>
                                            x.dependenField ===
                                            f.referenceValue,
                                    );
                                    if (!dependedField) return;

                                    let body = {
                                        maxResultCount: 2147483647,
                                        criterias: [
                                            {
                                                propertyName: 'codeData',
                                                operation: 0,
                                                value: val,
                                            },
                                        ],
                                    };
                                    this._foodFuelService
                                        .getAllGroupOfFoodFuel(body)
                                        .subscribe((data) => {
                                            dependedField.options =
                                                data.result.items
                                                    .filter(
                                                        (data) =>
                                                            data.status ===
                                                            'ENABLE',
                                                    )
                                                    .map((item) => ({
                                                        key: item.id,
                                                        name: item.tenNhomLttpChatDot,
                                                    }));
                                            fieldsCtrls[
                                                dependedField.referenceValue
                                            ].enable();
                                        });
                                }
                            },
                        );
                    } else if (f.type === 'TEXT') {
                        fieldsCtrls[f.referenceValue].valueChanges.subscribe(
                            (val) => {
                                if (val !== '') {
                                    const dependedField = fieldList.find(
                                        (x) =>
                                            x.dependenField ===
                                            f.referenceValue,
                                    );
                                    if (!dependedField) return;

                                    if (val.length > 10) {
                                        fieldsCtrls[
                                            dependedField.referenceValue
                                        ].patchValue(
                                            val
                                                .normalize('NFD')
                                                .replace(/[\u0300-\u036f]/g, '')
                                                .match(/(^|\s)\w[0-9]{0,3}/g)
                                                .join('')
                                                .replace(/\s/g, '')
                                                .toUpperCase(),
                                        );
                                    } else {
                                        fieldsCtrls[
                                            dependedField.referenceValue
                                        ].patchValue(
                                            val
                                                .normalize('NFD')
                                                .replace(/[\u0300-\u036f]/g, '')
                                                .replace(/\s/g, '')
                                                .toUpperCase(),
                                        );
                                    }
                                }
                            },
                        );
                    }
                }
            } else {
                //if checkbox, it need multiple
                let opts = {};
                for (let opt of f.options) {
                    opts[opt.key] = new FormControl({
                        value: opt.value,
                        disabled: f.disabled,
                    });
                }
                fieldsCtrls[f.referenceValue] = new FormGroup(opts);
            }
        }

        this.form = new FormGroup(fieldsCtrls);
    }

    getInitData(): void {
        let dataEdit = this.data.datas;

        if (!dataEdit) return;

        if (this.data.typeDialog === 'editTT') {
            let dataFile = JSON.parse(dataEdit.hideValue);

            this.listSelectedFiles = dataFile;
        }

        this.form.patchValue(dataEdit);
    }

    getFields() {
        if (
            this.data.typeDialog &&
            this.data.typeDialog.toUpperCase() === 'GROUP'
        ) {
            if (this.form.value.type.toUpperCase() !== 'TABLE') {
                return this.data.fields.filter(
                    (item) =>
                        item.referenceValue !== 'fixed' &&
                        item.referenceValue !== 'header',
                );
            }

            return this.data.fields;
        }

        return this.data.fields;
    }

    onCloseDialog(data?: any): void {
        this.dialogRef.close(data);
    }

    onDelete(): void {
        this.isEnableDelete = true;
    }

    onCancelDelete(): void {
        this.isEnableDelete = false;
    }

    // Get file
    onFileSelected(event: any) {
        if (!this.data.datas) {
            if (event.target.files.length > 0) {
                for (let item of event.target.files) {
                    this.listSelectedFiles.push(item);

                    const reader: FileReader = new FileReader();
                    reader.readAsDataURL(item);
                    reader.onloadend = () => {
                        this.base64Output = reader.result;

                        let final64 = this.base64Output.split(',');

                        this.objectFile = {
                            fileName: item.name,
                            fileType: item.type,
                            file64: final64[1],
                        };

                        this.listFile.attachments.push(this.objectFile);
                    };
                }
            }
        } else {
            if (event.target.files.length > 0) {
                for (let item of event.target.files) {
                    const reader: FileReader = new FileReader();
                    reader.readAsDataURL(item);
                    reader.onloadend = () => {
                        this.base64Output = reader.result;

                        let final64 = this.base64Output.split(',');

                        this.objectFile = {
                            fileName: item.name,
                            fileType: item.type,
                            file64: final64[1],
                        };

                        this.listSelectedFiles.attachments.push(
                            this.objectFile,
                        );
                    };
                }
            }
        }
    }

    // Thông tư

    remove(file: string): void {
        if (this.data.datas) {
            const index = this.listSelectedFiles.attachments.indexOf(file);

            if (index >= 0) {
                this.listSelectedFiles.attachments.splice(index, 1);
            }
        } else {
            const index = this.listSelectedFiles.indexOf(file);

            if (index >= 0) {
                this.listSelectedFiles.splice(index, 1);
            }
        }
    }
    //

    replaceWhiteSpace(): void {
        for (let f of this.data.fields) {
            if (f.type === 'TEXT' || f.type === 'TEXTAREA') {
                const initField = this.form.get(f.referenceValue);

                if (initField.value === null) {
                    initField.setValue('');
                } else {
                    const valueAfterReplace = initField.value
                        .replace(/\s+/g, ' ')
                        .trim();
                    initField.setValue(valueAfterReplace);
                }
            }
        }
    }

    onSubmit(): void {
        if (this.data.typeDialog !== 'lttp') {
            this.replaceWhiteSpace();
        }

        let formValues = this.form.value;
        const date = new Date();
        if (
            this.data.typeDialog &&
            this.data.typeDialog.toUpperCase() === 'GROUP'
        ) {
            if (formValues.type.toUpperCase() !== 'TABLE') {
                delete formValues['header'];
                delete formValues['fixed'];
            }
        }

        if (this.data.typeDialog === 'createTT') {
            formValues = {
                ...formValues,
                hideValue: JSON.stringify(this.listFile),
                ngayTao: date.toISOString(),
            };
        } else if (this.data.typeDialog === 'editTT') {
            formValues = {
                ...formValues,
                hideValue: JSON.stringify(this.listSelectedFiles),
            };
        }

        this.onSave.emit(formValues);
    }

    confirmDelete(): void {
        let dataDelete;
        if (this.data.datas.type === 'Group') {
            dataDelete = {
                item: this.data.datas,
            };
        } else {
            dataDelete = {
                id: this.data.datas.id,
            };
        }

        this.handleDelete.emit(dataDelete);
    }
}
