import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';

import {
    MomentDateAdapter,
    MAT_MOMENT_DATE_ADAPTER_OPTIONS,
} from '@angular/material-moment-adapter';
import {
    DateAdapter,
    MAT_DATE_FORMATS,
    MAT_DATE_LOCALE,
} from '@angular/material/core';

export const MY_FORMATS = {
    parse: {
        dateInput: 'DD/MM/YYYY', // this is how your date will be parsed from Input
    },
    display: {
        dateInput: 'DD/MM/YYYY', // this is how your date will get displayed on the Input
        monthYearLabel: 'MMMM YYYY',
        dateA11yLabel: 'LL',
        monthYearA11yLabel: 'MMMM YYYY',
    },
};

@Component({
    selector: 'fb-datetime',
    template: `
        <div [formGroup]="form">
            <ng-container
                *ngIf="!isFlatForm && tabCtrl"
                [formGroupName]="tabCtrl"
            >
                <ng-container [formGroupName]="groupCtrl">
                    <mat-form-field
                        [appearance]="field.appearance || 'outline'"
                        (mouseup)="field.diabled ? '' : picker.open()"
                    >
                        <mat-label>{{ field.name }}</mat-label>
                        <input
                            matInput
                            [matDatepicker]="picker"
                            [placeholder]="field.placeholder"
                            [value]="field.defaultValue"
                            [id]="field.referenceValue"
                            [name]="field.referenceValue"
                            [formControlName]="getFormControlName()"
                            [required]="field.required === '1' ? true : false"
                            spellcheck="false"
                            [readonly]="true"
                            [min]="field.min"
                            [max]="field.max"
                        />
                        <mat-datepicker-toggle
                            matSuffix
                            [for]="picker"
                            [disabled]="field.disabled"
                        ></mat-datepicker-toggle>
                        <mat-datepicker #picker></mat-datepicker>
                        <!-- VALIDATORS -->
                        <mat-error *ngIf="isTouched">{{
                            getErrorMessage()
                        }}</mat-error>
                    </mat-form-field>
                </ng-container>
            </ng-container>

            <mat-form-field
                *ngIf="isFlatForm || !tabCtrl"
                [appearance]="field.appearance || 'outline'"
                (mouseup)="picker.open()"
            >
                <mat-label>{{ field.name }}</mat-label>
                <input
                    matInput
                    [matDatepicker]="picker"
                    [placeholder]="field.placeholder"
                    [value]="field.defaultValue"
                    [id]="field.referenceValue"
                    [name]="field.referenceValue"
                    [matDatepickerFilter]="field.matDatepickerFilter"
                    [formControlName]="getFormControlName()"
                    [required]="field.required === '1' ? true : false"
                    spellcheck="false"
                    [readonly]="true"
                    [min]="field.min"
                    [max]="field.max"
                />
                <mat-datepicker-toggle
                    matSuffix
                    [for]="picker"
                ></mat-datepicker-toggle>
                <mat-datepicker #picker></mat-datepicker>
                <!-- VALIDATORS -->
                <mat-error *ngIf="isTouched">{{ getErrorMessage() }}</mat-error>
            </mat-form-field>
        </div>
    `,
    styles: [
        `
            mat-form-field {
                width: 100%;
            }
        `,
    ],
    providers: [
        // `MomentDateAdapter` can be automatically provided by importing `MomentDateModule` in your
        // application's root module. We provide it at the component level here, due to limitations of
        // our example generation script.
        {
            provide: DateAdapter,
            useClass: MomentDateAdapter,
            deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS],
        },

        { provide: MAT_DATE_LOCALE, useValue: 'vi-VI' },
        { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
        {
            provide: MAT_MOMENT_DATE_ADAPTER_OPTIONS,
            useValue: { useUtc: true },
        },
    ],
})
export class DateTimeComponent implements OnInit {
    @Input() field: any = {};
    @Input() form: FormGroup;
    @Input() tabCtrl: any;
    @Input() groupCtrl: any;
    @Input() isFlatForm: boolean;

    currentDate: Date = new Date();

    get fieldName() {
        if (!this.isFlatForm && this.tabCtrl) {
            return this.form
                .get(this.tabCtrl)
                .get(this.groupCtrl)
                .get(this.field.referenceValue);
        } else if (this.isFlatForm && this.tabCtrl) {
            return this.form.controls[
                `${this.tabCtrl}.${this.groupCtrl}.${this.field.referenceValue}`
            ];
        } else {
            return this.form.controls[this.field.referenceValue];
        }
    }

    get isValid() {
        return this.fieldName.valid;
    }
    get isDirty() {
        return this.fieldName.dirty;
    }
    get isTouched() {
        return this.fieldName.touched;
    }

    constructor() { }

    ngOnInit() { }

    getFormControlName() {
        if (this.isFlatForm) {
            return `${this.tabCtrl}.${this.groupCtrl}.${this.field.referenceValue}`;
        } else {
            return this.field.referenceValue;
        }
    }

    getErrorMessage() {
        let errorMessage = '';

        if (this.fieldName.hasError('required')) {
            errorMessage = `${this.field.name} không được để trống.`;
        }

        return errorMessage;
    }
}
