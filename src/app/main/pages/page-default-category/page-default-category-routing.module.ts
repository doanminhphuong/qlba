import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// import { FinancialDashboardComponent } from '../financial-report/financial-dashboard/financial-dashboard.component';
import { PageBlankComponent } from './page-dynamic-category/page-blank/page-blank.component';
import { PageDynamicComponent } from './page-dynamic-category/page-dynamic/page-dynamic.component';

const routes: Routes = [
    {
        path: '',
        children: [
            {
                path: '',
                redirectTo: 'home',
                pathMatch: 'full',
            },
            {
                path: 'home',
                loadChildren: () =>
                    import(
                        '../financial-dashboard-page/finance-dashboard.module'
                    ).then((m) => m.FinanceDashboardModule),
            },
            {
                path: 'du-chi',
                component: PageDynamicComponent,
            },
            {
                path: 'category',
                children: [
                    {
                        path: '',
                        redirectTo: 'inbox',
                        pathMatch: 'full',
                    },
                    {
                        path: 'inbox',
                        component: PageBlankComponent,
                    },
                    {
                        path: ':valueData',
                        component: PageBlankComponent,
                    },
                ],
            },
        ],
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class PageDefaultCategoryRoutingModule {}
