import { Component, Inject, OnInit } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { UserService } from "@core/services/user.service";

@Component({
  selector: "app-render-form-dialog",
  templateUrl: "./render-form-dialog.component.html",
  styleUrls: ["./render-form-dialog.component.scss"]
})
export class RenderFormDialogComponent implements OnInit {
  filedList: any[] = [];
  constructor(
    public dialogRef: MatDialogRef<RenderFormDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _userService: UserService
  ) {}

  ngOnInit() {
    console.log("data RenderFormDialogComponent", this.data);
    this.initData();
  }

  initData() {
    const ID = this.data.id;
    this._userService.getDataGroupById(ID).subscribe((res) => {
      this.filedList = res;

      console.log(this.filedList);
      // console.log(res);
    });
  }
}
