import {
    Component,
    EventEmitter,
    Input,
    OnInit,
    Output,
    QueryList,
    Renderer2,
    ViewChild,
} from '@angular/core';
import { MatMenuTrigger } from '@angular/material';
import { Router } from '@angular/router';

@Component({
    selector: 'app-menu-item',
    templateUrl: './menu-item.component.html',
    styleUrls: ['./menu-item.component.scss'],
})
export class MenuItemComponent implements OnInit {
    @Input() items;
    @ViewChild('childMenu') public childMenu;

    constructor(private router: Router) {}

    ngOnInit(): void {}

    saveIdMenu(id: string): void {
        localStorage.setItem('idMenu', id);
    }
}
