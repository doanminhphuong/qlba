import { SelectionModel } from '@angular/cdk/collections';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatPaginator, MatTableDataSource } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { DynamicDialogComponent } from '@app/components/dynamic-dialog-component/dynamic-dialog-component.component';
import { DeleteConfirmDialogComponent } from '@app/_shared/dialogs/delete-confirm-dialog/delete-confirm-dialog.component';
import { CommonService } from '@core/services/common.service';
import { DataService } from '@core/services/data-service';
import { InventoryService } from '@core/services/inventory.service';
import { ToastrService } from '@core/services/toastr.service';
import { UserService } from '@core/services/user.service';
import { Subscription } from 'rxjs';
@Component({
    selector: 'app-store-list',
    templateUrl: './store-list.component.html',
    styleUrls: ['./store-list.component.scss'],
})
export class StoreListComponent implements OnInit {
    @ViewChild(MatPaginator) paginator!: MatPaginator;
    currentDate: Date = new Date();

    displayedColumnsOfField: string[] = [
        'select',
        'maKho',
        'tenKho',
        'creationTime',
        'status',
        'actions',
    ];

    dataSource = new MatTableDataSource<any>([]);
    selection = new SelectionModel<any>(true, []);

    adjustmentList: any[] = [];
    listDauMoiBep: any[] = [];

    allowExec: boolean = false;
    subscription: Subscription;

    isLoading: boolean = true;

    constructor(
        private _router: Router,
        public dialog: MatDialog,
        private _toastrService: ToastrService,
        private _inventoryService: InventoryService,
        private userService: UserService,
        private _commonService: CommonService,
        private _dataService: DataService,
        private activatedRoute: ActivatedRoute,
    ) {
        this._commonService.checkPermission2().subscribe((response) => {
            if (!response.result) return;

            if (JSON.parse(response.result).some((obj) => obj.key === 'EXEC')) {
                this.allowExec = true;
            }
        });

        this.subscription = this._dataService.getData().subscribe((message) => {
            this.isLoading = true;
            this.getInitData(message.id);
        });
    }

    ngOnDestroy(): void {
        this.subscription.unsubscribe();
    }

    ngOnInit() {
        this.getInitData(this._dataService.getDauMoiBepId());
    }

    getInitData(id: any): void {
        this.selection.clear();

        this._inventoryService
            .getAllWarehouse({
                maxResultCount: 999999999,
                criterias: [
                    {
                        propertyName: 'dauMoiBepId',
                        operation: 0,
                        value: id,
                    },
                ],
            })
            .subscribe((res) => {
                this.dataSource.data = res.result.items;
                this.dataSource.paginator = this.paginator;

                setTimeout(() => {
                    this.isLoading = false;
                }, 200);
            });

        this.userService.getUserOrganization().subscribe((res) => {
            let organization = res.result.filter(
                (item) => item.groupCode === 'DonVi',
            );

            this._commonService
                .callDataAPIShort('/api/services/read/DauMoiBep/GetAll', {
                    maxResultCount: 2147483647,
                    donViId: organization[0].id,
                })
                .subscribe((response) => {
                    this.listDauMoiBep = response.result.items.map((item) => ({
                        key: item.id,
                        name: item.tenBep,
                    }));
                });
        });
    }

    /** Whether the number of selected elements matches the total number of rows. */
    isAllSelected() {
        const numSelected = this.selection.selected.length;
        const numRows = this.dataSource.data.length;
        return numSelected === numRows;
    }

    /** Selects all rows if they are not all selected; otherwise clear selection. */
    toggleAllRows() {
        if (this.isAllSelected()) {
            this.selection.clear();
            return;
        }

        this.selection.select(...this.dataSource.data);
    }

    /** The label for the checkbox on the passed row */
    checkboxLabel(row?: any): string {
        if (!row) {
            return `${this.isAllSelected() ? 'deselect' : 'select'} all`;
        }
        return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${
            row.index + 1
        }`;
    }

    guid = () => {
        let s4 = () => {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        };
        //return id of format 'aaaaaaaa'-'aaaa'-'aaaa'-'aaaa'-'aaaaaaaaaaaa'
        return (
            s4() +
            s4() +
            '-' +
            s4() +
            '-' +
            s4() +
            '-' +
            s4() +
            '-' +
            s4() +
            s4() +
            s4()
        );
    };

    openDialog(dataDialog: any): void {
        let fields = [
            {
                type: 'TEXT',
                referenceValue: 'maKho',
                name: 'Mã kho',
                defaultValue: '',
                required: '1',
                pattern: '^[a-zA-Z0-9-]*$',
                icon: 'code',
                css: 'col-12 col-lg-6',
            },
            {
                type: 'TEXT',
                referenceValue: 'tenKho',
                name: 'Tên kho',
                defaultValue: '',
                required: '1',
                icon: 'store',
                css: 'col-12 col-lg-6',
            },
            // {
            //     type: 'SELECT',
            //     referenceValue: 'dauMoiBepId',
            //     name: 'Chọn đầu mối bếp',
            //     defaultValue: this._dataService.getDauMoiBepId(),
            //     options: [...this.listDauMoiBep],
            //     css: 'col-12 col-lg-6',
            //     search: '1',
            //     searchCtrl: 'searchOrganizationCtrl',
            //     required: '1',
            // },
            {
                type: 'RADIO',
                referenceValue: 'status',
                name: 'Trạng thái',
                defaultValue: 'ENABLE',
                required: '0',
                options: [
                    { key: 'ENABLE', name: 'Hoạt động' },
                    { key: 'DISABLE', name: 'Tạm ngưng' },
                ],
                css: 'col-12 col-lg-6',
            },
            {
                type: 'TEXTAREA',
                referenceValue: 'moTa',
                name: 'Mô tả',
                defaultValue: '',
                required: '0',
                css: 'col-12 col-lg-12',
            },
        ];

        let dialogRef = this.dialog.open(DynamicDialogComponent, {
            // disableClose: true,
            width: '70%',
            data: {
                datas: dataDialog,
                fields: fields,
                typeDialog: 'inventory',
            },
            panelClass: 'my-custom-dialog-class',
        });

        dialogRef.componentInstance.onSave.subscribe((formValues) => {
            const isSubmitted = !!formValues;

            let dataSubmit = {
                ...dataDialog,
                ...(formValues = {
                    ...formValues,
                    dauMoiBepId: this._dataService.getDauMoiBepId(),
                }),
            };

            if (!dataDialog) {
                this._inventoryService.createWarehouse(dataSubmit).subscribe(
                    (res) => dialogRef.close(isSubmitted),
                    (err) => this._toastrService.errorServer(err),
                );
            } else {
                this._inventoryService.updateWarehouse(dataSubmit).subscribe(
                    (res) => dialogRef.close(isSubmitted),
                    (err) => this._toastrService.errorServer(err),
                );
            }
        });

        dialogRef.componentInstance.handleDelete.subscribe((dataDelete) => {
            const isDeleted = !!dataDelete;
        });

        dialogRef.afterClosed().subscribe((result) => {
            if (!result) return;
            this.getInitData(this._dataService.getDauMoiBepId());
            if (!dataDialog) {
                this._toastrService.success('', 'Thêm thành công');
            } else {
                this._toastrService.success('', 'Cập nhật thành công');
            }
        });
    }

    createAction(): void {
        this.openDialog(null);
    }

    editAction(element: any): void {
        this.openDialog(element);
    }

    deleteAction(element: any): void {
        const dialogRef = this.dialog.open(DeleteConfirmDialogComponent, {
            width: '500px',
            data: element,
        });

        dialogRef.componentInstance.handleDelete.subscribe((dataDelete) => {
            const isSubmitted = !!dataDelete;

            this._inventoryService.deleteWarehouse(dataDelete).subscribe(
                (res) => dialogRef.close(isSubmitted),
                (err) => this._toastrService.errorServer(err),
            );
        });

        dialogRef.afterClosed().subscribe((result) => {
            if (!result) return;

            this.getInitData(this._dataService.getDauMoiBepId());
            this._toastrService.success('', 'Xóa thành công');
        });
    }

    deleteList(): void {
        const dialogRef = this.dialog.open(DeleteConfirmDialogComponent, {
            width: '500px',
            data: this.selection.selected,
        });

        dialogRef.componentInstance.handleDelete.subscribe((dataDelete) => {
            const isSubmitted = !!dataDelete;

            const numSelected = this.selection.selected;

            for (let item of numSelected) {
                let request = {
                    id: item.id,
                };

                this._inventoryService.deleteWarehouse(request).subscribe(
                    (res) => dialogRef.close(isSubmitted),
                    (err) => this._toastrService.errorServer(err),
                );
            }
        });

        dialogRef.afterClosed().subscribe((result) => {
            if (!result) return;

            this.getInitData(this._dataService.getDauMoiBepId());
            this._toastrService.success('', 'Xóa thành công');
        });
    }
}
