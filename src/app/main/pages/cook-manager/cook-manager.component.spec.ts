import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CookManagerComponent } from './cook-manager.component';

describe('CookManagerComponent', () => {
  let component: CookManagerComponent;
  let fixture: ComponentFixture<CookManagerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CookManagerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CookManagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
