import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanGuaranteeAddEditComponent } from './plan-guarantee-add-edit.component';

describe('PlanGuaranteeAddEditComponent', () => {
  let component: PlanGuaranteeAddEditComponent;
  let fixture: ComponentFixture<PlanGuaranteeAddEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlanGuaranteeAddEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanGuaranteeAddEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
