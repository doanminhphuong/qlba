import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable, OnInit } from '@angular/core';
import { environment } from 'environments/environment';
import { Observable } from 'rxjs';
import { TokenStorageService } from './token.service';
import { UtilsService } from './utils.service';

const TOKEN_KEY = 'auth-token';
const USER_KEY = 'auth-user';

@Injectable()
export class FieldService implements OnInit {
    BASE_URL = `${environment.SWAGGER_URL}/api/services/`;
    ENDPOINT = 'read/Category/GetList';

    constructor(
        private http: HttpClient,
        private utils: UtilsService,
        private _tokenStorageService: TokenStorageService,
    ) {}

    ngOnInit(): void {}

    getAllField(data: any): Observable<any> {
        let token = this._tokenStorageService.getToken();
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
        });

        let options = { headers: headers };

        const END_POINT = 'read/Field/GetAll';

        return this.http.post<any>(
            `${this.BASE_URL}${END_POINT}`,
            data,
            options,
        );
    }

    createField(data: any): Observable<any> {
        let token = this._tokenStorageService.getToken();
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
        });

        let options = { headers: headers };

        const END_POINT = 'write/Field/Create';

        return this.http.post<any>(
            `${this.BASE_URL}${END_POINT}`,
            data,
            options,
        );
    }

    updateField(data: any): Observable<any> {
        let token = this._tokenStorageService.getToken();
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
        });

        let options = { headers: headers };

        const END_POINT = 'write/Field/Update';

        return this.http.post<any>(
            `${this.BASE_URL}${END_POINT}`,
            data,
            options,
        );
    }

    deleteField(data: any): Observable<any> {
        let token = this._tokenStorageService.getToken();
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
        });

        let options = { headers: headers };

        const END_POINT = 'write/Field/Delete';

        return this.http.post<any>(
            `${this.BASE_URL}${END_POINT}`,
            data,
            options,
        );
    }
}
