import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EaterInfoDialogComponent } from './eater-info-dialog.component';

describe('EaterInfoDialogComponent', () => {
  let component: EaterInfoDialogComponent;
  let fixture: ComponentFixture<EaterInfoDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EaterInfoDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EaterInfoDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
