import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KitchenOperationManagementComponent } from './kitchen-operation-management.component';

describe('KitchenOperationManagementComponent', () => {
  let component: KitchenOperationManagementComponent;
  let fixture: ComponentFixture<KitchenOperationManagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KitchenOperationManagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KitchenOperationManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
