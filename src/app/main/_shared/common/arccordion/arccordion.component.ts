import {
    Component,
    ElementRef,
    Input,
    OnInit,
    Output,
    ViewChild,
    ViewChildren,
} from '@angular/core';
import { QueryList } from '@angular/core/src/render3';
import { MatAccordion, MatDialog, MatExpansionPanel } from '@angular/material';
import { EventEmitter } from '@angular/core';

@Component({
    selector: 'app-arccordion',
    templateUrl: './arccordion.component.html',
    styleUrls: ['./arccordion.component.scss'],
})
export class ArccordionComponent implements OnInit {
    @ViewChild(MatAccordion) accordion: MatAccordion;
    @ViewChild('panel') panel: MatExpansionPanel;
    // @ViewChild('') isConfirmDeleteGroup: boolean = false;
    // @ViewChildren("panel") public panel: QueryList<MatExpansionPanel>;
    @Input() filedList: any; // decorate the property with @Input()
    @Input() index: number;
    @Output() itemGroup: any = new EventEmitter();
    @Output() itemField: any = new EventEmitter();

    isConfirmDeleteGroup: boolean[] = [];
    step: number = 0;
    indexExpanded: number = -1;

    constructor(public dialog: MatDialog) {}

    setStep(index: number) {
        this.step = index;
    }

    editGroup() {
        console.log('Edit');
    }

    clickEvent(event) {
        console.log(event);
    }

    togglePanels(index: number) {
        this.indexExpanded = index == this.indexExpanded ? -1 : index;
    }

    handlePanel(panel) {
        // this.panel.toggle();
        console.log('panel', panel);
    }

    createField(item: any) {
        // console.log("item::", item);
        let data = {
            name: 'Field 16',
            code: 'Fi-16',
            type: 'Field',
            css: '{"size":"4"}',
            index: 4,
            referenceValue: '',
            required: '0',
            format: null,
            validate: null,
            dependField: null,
            formula: null,
            formulaView:
                '{"title": "Phân loại", "showInList": "true", "icon": ""}',
            category: 'ho-hap',
        };

        item.items.push(data);
        this.itemGroup.emit(item);

        // console.log("itemAfterClick::", item);
    }

    onHandleField(item: any, type: string, i: number) {
        if (type.toUpperCase() === 'DELETE_ALL_FIELD') {
            // filter to delete all field in this group (item)
            item.items = item.items.filter((data) => data.id == item.items.id);
        } else if (type.toUpperCase() === 'ADD') {
            this.index = i;
            this.indexExpanded = this.index;
        }
        let data = { item, type };
        this.itemGroup.emit(data);
        this.isConfirmDeleteGroup.fill(false);
    }

    handleItemFieldChild(element: any, dataItem) {
        // this.itemField.emit(element);
        console.log('element::', element);
        console.log('dataItem::', dataItem);

        let { type, data } = element;

        if (type.toUpperCase() === 'DELETE_FIELD') {
            dataItem.items = dataItem.items.filter(
                (dataValue) => dataValue.id !== data.id,
            );

            // console.log('dataItem::', dataItem);

            this.itemGroup.emit({ item: dataItem, type });
            this.isConfirmDeleteGroup.fill(false);
        } else if (type.toUpperCase() === 'UPDATE_FIELD') {
            let indexElement = dataItem.items.findIndex(
                (item) => item.id === element.data.id,
            );
            // console.log('indexElement::', indexElement);
            dataItem.items[indexElement] = element.data;
            // console.log('dataItem 2::', dataItem);

            this.itemGroup.emit({ item: dataItem, type });
            this.isConfirmDeleteGroup.fill(false);
        } else {
            console.log('data::', data);
        }
    }

    toggleConfirmDeleteGroup(index: number) {
        this.isConfirmDeleteGroup[index] = !this.isConfirmDeleteGroup[index];
    }

    resetDeleteGroup(index: number) {
        this.isConfirmDeleteGroup[index] = false;
    }

    ngOnInit() {
        this.filedList = this.filedList.sort((a, b) => a.index - b.index); // DESC List Tab

        for (let data of this.filedList) {
            let indexElement = this.filedList.findIndex(
                (item) => item.id === data.id,
            );
            this.isConfirmDeleteGroup[indexElement] = false;
        }
    }

    ngAfterViewInit() {
        // console.log(this.panel);
    }
}
