import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MaintenanceInventoryComponent } from './maintenance-inventory.component';

describe('MaintenanceInventoryComponent', () => {
  let component: MaintenanceInventoryComponent;
  let fixture: ComponentFixture<MaintenanceInventoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MaintenanceInventoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MaintenanceInventoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
