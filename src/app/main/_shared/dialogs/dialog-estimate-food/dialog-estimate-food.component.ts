import { SelectionModel } from '@angular/cdk/collections';
import {
    AfterViewChecked,
    AfterViewInit,
    ChangeDetectorRef,
    Component,
    EventEmitter,
    Inject,
    OnInit,
    ViewChild,
} from '@angular/core';
import {
    MatDialogRef,
    MatPaginator,
    MatSort,
    MatTableDataSource,
    MAT_DIALOG_DATA,
} from '@angular/material';
import { CategoryService } from '@core/services/category.service';
import { FoodFuelService } from '@core/services/food-fuel.service';
import { PriceService } from '@core/services/price.service';
import { Subject, Subscription } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';
import { forkJoin, Observable } from 'rxjs';

import {
    MomentDateAdapter,
    MAT_MOMENT_DATE_ADAPTER_OPTIONS,
} from '@angular/material-moment-adapter';
import {
    DateAdapter,
    MAT_DATE_FORMATS,
    MAT_DATE_LOCALE,
} from '@angular/material/core';
import { InventoryService } from '@core/services/inventory.service';
import { CommonService } from '@core/services/common.service';
import { I } from '@angular/cdk/keycodes';

export const MY_FORMATS = {
    parse: {
        dateInput: 'DD/MM/YYYY', // this is how your date will be parsed from Input
    },
    display: {
        dateInput: 'DD/MM/YYYY', // this is how your date will get displayed on the Input
        monthYearLabel: 'MMMM YYYY',
        dateA11yLabel: 'LL',
        monthYearA11yLabel: 'MMMM YYYY',
    },
};

export interface MaterialElement {
    select?: any;
    id: string;
    code: string;
    name: string;
    group: string;
    unit: string;
    type: string;
    status: boolean;
    giaMuaNgoai?: number;
    giaTGSX?: number;
    ghiChu?: number;
}

@Component({
    selector: 'app-dialog-estimate-food',
    templateUrl: './dialog-estimate-food.component.html',
    styleUrls: ['./dialog-estimate-food.component.scss'],
    providers: [
        // `MomentDateAdapter` can be automatically provided by importing `MomentDateModule` in your
        // application's root module. We provide it at the component level here, due to limitations of
        // our example generation script.
        {
            provide: DateAdapter,
            useClass: MomentDateAdapter,
            deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS],
        },

        { provide: MAT_DATE_LOCALE, useValue: 'vi-VI' },
        { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
        {
            provide: MAT_MOMENT_DATE_ADAPTER_OPTIONS,
            useValue: { useUtc: true },
        },
    ],
})
export class DialogEstimateFoodComponent
    implements OnInit, AfterViewInit, AfterViewChecked
{
    displayedColumnsAdjustment: string[] = [
        'select',
        'maLttpChatDot',
        'tenLttpChatDot',
        'donViTinh',
        'giaThiTruong',
        'giaTangGiaSanXuat',
        'value10',
    ];

    displayedColumnsInventory: string[] = [
        'select',
        'maLttpChatDot',
        'tenLttpChatDot',
        'phanLoai',
        'donViTinh',
        'soluongTonKho',
        'donGia',
        'ngayHetHan',
    ];

    displayedColumnsInventoryImport: string[] = [
        'select',
        'maLttpChatDot',
        'tenLttpChatDot',
        'phanLoai',
        'donViTinh',
        'soLuongThucNhap',
        'giaLe',
        'ngayHetHan',
        'ghiChu',
    ];

    displayedColumnsInventoryTransfer: string[] = [
        'select',
        'tenLttpChatDot',
        'phanLoai',
        'donViTinh',
        'soLuongThucXuat',
        'donGia',
        'soluongTonKho',
    ];

    displayedColumnsInventoryExport: string[] = [
        'select',
        'tenLttpChatDot',
        'phanLoai',
        'donViTinh',
        'soLuongThucXuat',
        'soLuongPhaiXuat',
        'donGia',
        'soluongTonKho',
    ];

    displayedColumnsPurchase: string[] = [
        'select',
        'maLttpChatDot',
        'tenLttpChatDot',
        'phanLoai',
        'donViTinh',
        'soLuong',
    ];

    displayedColumnsFoody: string[] = [
        'select',
        'tenLttpChatDot',
        'tenNhomLttpChatDot',
        'donViTinh',
        'soluongTonKho',
        'donGia',
        'khoId',
    ];

    currentDate: Date = new Date();

    dataSource = new MatTableDataSource<any>([]);
    selection = new SelectionModel<any>(true, []);
    onSave = new EventEmitter();

    @ViewChild(MatPaginator) paginator!: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    private subscription: Subscription;

    priceAdjustmentList: any[] = [];
    foodFuelList: any[] = [];
    unitOptions: any[] = [];
    typeOptions: any[] = [];
    isPurchasePrice: boolean = false;
    availableList: any[] = [];

    isShow: boolean = false;
    warehouseList: any[] = [];
    inventoryList: any[] = [];

    constructor(
        private readonly changeDetectorRef: ChangeDetectorRef,
        public dialogRef: MatDialogRef<DialogEstimateFoodComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any,
        private _categoryService: CategoryService,
        private _foodFuelService: FoodFuelService,
        private _priceService: PriceService,
        private _inventoryService: InventoryService,
        private _commonService: CommonService,
    ) {}

    ngOnInit() {
        this.handleSelectWarehouse();
    }

    ngAfterViewInit(): void {}

    ngAfterViewChecked(): void {
        this.changeDetectorRef.detectChanges();
    }

    getAllFoodFuel(): void {
        const bodyFoodFuel = {
            maxResultCount: 9999,
            skipCount: 0,
            sorting: 'Code',
        };

        this._foodFuelService.getAllFoodFuel(bodyFoodFuel).subscribe((res) => {
            this.foodFuelList = res.result.items.filter(
                (item) => item.status === 'ENABLE',
            );
            this.getInitData();
        });

        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
    }

    getInitData(): void {
        if (this.data.type === 'INVENTORY' || this.data.type === 'FOODY') {
            this.availableList = this.foodFuelList;
            this.dataSource.data = this.foodFuelList;
            this.getAvailableList();
        }
    }

    getAvailableList(): void {
        if (this.data.currentTable.length === 0) return;
        const materialIdList = this.data.currentTable.map(
            (item) => item.lttpChatDotId,
        );

        this.availableList = this.dataSource.data.filter(
            (item) => !materialIdList.includes(item.id),
        );
        this.dataSource.data = this.availableList;
    }

    getMultiCategories() {
        this.getUnitOptions();
        this.getTypeOptions();
    }

    getBodyCategory(groupCode: string) {
        return {
            maxResultCount: 10,
            skipCount: 0,
            sorting: 'Code',
            criterias: [
                {
                    propertyName: 'GroupCode',
                    operation: 6,
                    value: groupCode,
                },
            ],
        };
    }

    getUnitOptions() {
        this._categoryService
            .getAllCategory(this.getBodyCategory('don-vi-tinh'))
            .subscribe((res) => {
                this.unitOptions = res.result.map((item) => ({
                    key: item.id,
                    name: item.name,
                }));
            });
    }

    getTypeOptions() {
        this._commonService
            .callDataAPIShort('/api/services/read/NhomLttpChatDot/GetAll', {})
            .subscribe((res) => {
                this.typeOptions = res.result.items.map((item) => ({
                    key: item.lttpChatDotId,
                    name: item.tenNhomLttpChatDot,
                }));
            });
    }

    getObjectElementUser(key, type) {
        let data;
        let dataList = [];

        if (this.unitOptions.length > 0 && this.typeOptions.length > 0) {
            switch (type) {
                case 'UNIT':
                    dataList = this.unitOptions;
                    break;
                case 'TYPE':
                    dataList = this.typeOptions;
                    break;
            }
            let item = dataList.find((item) => item.key === key);
            data = item && item.name ? item.name : '';
        }

        return data;
    }

    getDataOfItem(id, objKey) {
        let result = '';

        if (this.inventoryList.length > 0) {
            const dataFound = this.inventoryList.find(
                (item) => item.lttpChatDotId === id,
            );

            result = dataFound ? dataFound[objKey] : '';
        }

        return result;
    }

    getInventoryOfItem(id) {
        let result = '';

        if (this.inventoryList.length > 0) {
            const dataFounds = this.inventoryList.filter(
                (item) => item.lttpChatDotId === id,
            );

            result = dataFounds.reduce(
                (acc, item) => acc + item.soluongTonKho,
                0,
            );
        }

        return result;
    }

    getInfoFoodFuel(id, objKey) {
        let result = '';

        if (this.foodFuelList.length > 0) {
            const dataFound = this.foodFuelList.find((item) => item.id === id);

            result = dataFound ? dataFound[objKey] : '';
        }

        return result;
    }

    /** Whether the number of selected elements matches the total number of rows. */
    isAllSelected() {
        const numSelected = this.selection.selected.length;
        const numRows = this.dataSource.data.length;
        return numSelected === numRows;
    }

    /** Selects all rows if they are not all selected; otherwise clear selection. */
    toggleAllRows() {
        if (this.isAllSelected()) {
            this.selection.clear();
            this.dataSource.data.forEach((item) => (item.isChecked = false));
        } else {
            this.selection.select(...this.dataSource.data);
            this.dataSource.data.forEach((item) => (item.isChecked = true));
        }
    }

    /** The label for the checkbox on the passed row */
    checkboxLabel(row?: MaterialElement): string {
        if (!row) {
            return `${this.isAllSelected() ? 'deselect' : 'select'} all`;
        }
        // return `${this.selection.isSelected(row) ? "deselect" : "select"} row ${row.position + 1}`;
        return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${
            row.select + 1
        }`;
    }

    // Input Filter
    handleInputFilter(filterValue) {
        this.dataSource.data = this.availableList.filter((item) =>
            item.tenLttpChatDot
                .toLowerCase()
                .includes(filterValue.trim().toLowerCase()),
        );
    }

    handleSelectWarehouse() {
        // const body = {
        //     criterias: [
        //         {
        //             propertyName: 'khoId',
        //             operation: 0,
        //             value: event.value,
        //         },
        //     ],
        // };
        const body = {
            criterias: [
                {
                    propertyName: 'lttpChatDotId',
                    operation: 0,
                    value: this.data.lttpChatDotId,
                },
            ],
        };

        this._commonService
            .callDataAPIShort('/api/services/read/TonKho/GetAll', body)
            .subscribe((res) => {
                // const materialIds = res.result.items.map(
                //     (item) => item.lttpChatDotId,
                // );

                // this.availableList = this.foodFuelList.filter((item) =>
                //     materialIds.includes(item.id),
                // );

                this.dataSource.data = res.result.items;
                if (this.data.dataList.length > 0) {
                    console.log('dataList pass::', this.data.dataList);
                    // this.selection.selected = this.data.dataList;
                    // this.dataSource.data = [
                    //     ...this.dataSource.data,
                    //     ...this.data.dataList,
                    // ];

                    // let dataTemp = this.dataSource.data.map((item) => ({
                    //     ...item,
                    //     isChecked: this.data.dataList.some(
                    //         (itemSub) => itemSub.isChecked === true,
                    //     ),
                    // }));
                    this.dataSource.data.forEach((row) => {
                        if (
                            this.data.dataList.some(
                                (item) => item.id === row.id,
                            )
                        ) {
                            this.selection.select(row);
                        }
                    });

                    console.log('dataSource Modal::', this.dataSource.data);
                }

                const values = res.result.items.map((item) => ({
                    key: item.lttpChatDotId,
                    name: item.tenNhomLttpChatDot,
                }));

                const ids = values.map((o) => o.key);
                const filtered = values.filter(
                    ({ key }, index) => !ids.includes(key, index + 1),
                );

                this.typeOptions = filtered;
            });
    }

    getDataFilter(filterValue) {
        this.dataSource.filter = filterValue.trim().toLowerCase();
        let dataSearch = {
            maxResultCount: 100,
            criterias: [
                {
                    propertyName: 'Search',
                    operation: 'OrEquals',
                    value: `[{"propertyName":"Name","operation":"Contains","value":"${filterValue}"}]`,
                },
            ],
            sorting: 'Number1',
            tenantId: 1,
            language: 'vi',
        };
        this._categoryService
            .searchFieldFormName(dataSearch)
            .subscribe((res: any) => {
                this.dataSource = new MatTableDataSource<MaterialElement>(
                    res.result,
                );
                this.dataSource.paginator = this.paginator;
            });

        if (this.dataSource.paginator) {
            this.dataSource.paginator.firstPage();
        }
    }

    onCloseDialog(data?: any): void {
        this.dialogRef.close(data);
    }

    selectCurrent(isChecked: boolean, dataRow: any): void {
        this.selection.toggle(dataRow);

        if (isChecked) {
            dataRow.isChecked = true;
        } else {
            dataRow.isChecked = false;
        }
    }

    handleValidatePurchase(event: any, dataRow: any) {
        const valuePurchase = event.target.value;

        if (valuePurchase !== '') {
            dataRow.isEnable = true;
        } else {
            dataRow.isEnable = false;

            if (dataRow.isChecked) {
                dataRow.isChecked = false;
                this.selection.toggle(dataRow);
            }
        }
    }

    handleValidateAmount(event: any, dataRow: any) {
        const valueAmount = event.target.value;

        if (valueAmount !== '') {
            dataRow.isHaveAmount = true;
        } else {
            dataRow.isHaveAmount = false;

            if (dataRow.isChecked) {
                dataRow.isChecked = false;
                this.selection.toggle(dataRow);
            }
        }
    }

    enableToggleAllCheckbox(): boolean {
        return this.dataSource.data.every((item) => item.isEnable);
    }

    guid = () => {
        let s4 = () => {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        };
        //return id of format 'aaaaaaaa'-'aaaa'-'aaaa'-'aaaa'-'aaaaaaaaaaaa'
        return (
            s4() +
            s4() +
            '-' +
            s4() +
            '-' +
            s4() +
            '-' +
            s4() +
            '-' +
            s4() +
            s4() +
            s4()
        );
    };

    onSubmit() {
        // this.selection.selected.forEach((item) => {
        //     if (!item.lttpChatDotId) {
        //         item.lttpChatDotId = item.id;
        //         delete item.id;
        //     }

        //     if (!item.giaTangGiaSanXuat) {
        //         item.giaTangGiaSanXuat = 0;
        //     }
        // });

        this.onSave.emit({
            lttpChatDotId: this.data.lttpChatDotId,
            selection: this.selection.selected,
        });
    }

    applyFilter(event: Event) {
        const filterValue = (event.target as HTMLInputElement).value;
        this.dataSource.filter = filterValue.trim().toLowerCase();
    }

    // Select Filter
    handleSelectFilter(event: any) {
        if (!event.value) {
            this.dataSource.filter = '';
        } else {
            const filterValue = event.value;
            this.dataSource.filter = filterValue.trim().toLowerCase();
        }
    }
}
