import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HeaderComponent } from './header/header.component';

import { CoreModule } from '../../core/core.module';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {
    PerfectScrollbarConfigInterface,
    PerfectScrollbarModule,
    PERFECT_SCROLLBAR_CONFIG,
} from 'ngx-perfect-scrollbar';
import { FooterComponent } from './footer/footer.component';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatDatepickerModule,
    MatDialogModule,
    MatDividerModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatMenuModule,
    MatPaginatorModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatSelectModule,
    MatSnackBarModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatListModule,
} from '@angular/material';

import { MatDatepickerModule as MatDatepickerModuleCustom } from '@angular/material/datepicker';

import { MatNativeDateModule } from '@angular/material/core';
import { LoadingBarModule } from '@ngx-loading-bar/core';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from '@shared/shared.module';
import { CardComponent } from './card/card.component';
import { ArccordionComponent } from './common/arccordion/arccordion.component';
import { FieldItemComponent } from './common/arccordion/field-item/field-item.component';
import { BasicSnackbarComponent } from './common/basic-snackbar/basic-snackbar/basic-snackbar.component';
import { ButtonComponent } from './common/button/button.component';
import { FormGroupComponent } from './common/form-group/form-group.component';
import { FormItemComponent } from './common/form-group/form-item/form-item.component';
import { SearchComponent } from './common/search/search.component';
import { DeleteConfirmDialogComponent } from './dialogs/delete-confirm-dialog/delete-confirm-dialog.component';
import { QldkDialogComponent } from './dialogs/qldk-dialog/qldk-dialog.component';
import { CheckBoxComponent } from './dynamic-form-builder/atoms/checkbox';
import { DateTimeComponent } from './dynamic-form-builder/atoms/datetime';
import { EmailComponent } from './dynamic-form-builder/atoms/email';
import { MultiselectComponent } from './dynamic-form-builder/atoms/multiselect';
import { NumberComponent } from './dynamic-form-builder/atoms/number';
import { RadioComponent } from './dynamic-form-builder/atoms/radio';
import { SelectComponent } from './dynamic-form-builder/atoms/select';
import { TextComponent } from './dynamic-form-builder/atoms/text';
import { PasswordComponent } from './dynamic-form-builder/atoms/password';
import { TextareaComponent } from './dynamic-form-builder/atoms/textarea';
import { DynamicFormBuilderComponent } from './dynamic-form-builder/dynamic-form-builder.component';
import { DynamicFormRenderComponent } from './dynamic-form-builder/dynamic-form-render/dynamic-form-render.component';
import { FieldBuilderComponent } from './dynamic-form-builder/field-builder/field-builder.component';
import { AddTabDialogComponent } from './header/dialog/add-tab-dialog/add-tab-dialog.component';
import { FieldDialogComponent } from './header/dialog/field-dialog/field-dialog.component';
import { RenderFormDialogComponent } from './header/dialog/render-form-dialog/render-form-dialog.component';
import { UpdateFieldDialogComponent } from './header/dialog/update-field-dialog/update-field-dialog.component';
import { MenuItemComponent } from './header/menu/menu-item/menu-item.component';
import { MenuComponent } from './header/menu/menu.component';
import { SearchAdvancedComponent } from './common/search-advanced/search-advanced.component';
import { FileComponent } from './dynamic-form-builder/atoms/file';
import { DialogRejectStatusComponent } from './dialogs/dialog-reject-status/dialog-reject-status.component';
import { DialogQuantitativeComponent } from './dialogs/dialog-quantitative/dialog-quantitative.component';
import { DeleteConfirmAllDialogComponent } from './dialogs/delete-confirm-all-dialog/delete-confirm-all-dialog.component';
import { DialogEstimateFoodComponent } from './dialogs/dialog-estimate-food/dialog-estimate-food.component';
import { DialogSwapUnitComponent } from './dialogs/dialog-swap-unit/dialog-swap-unit.component';
import { DialogMealRateComponent } from './dialogs/dialog-meal-rate/dialog-meal-rate.component';
import { DialogCookManagerComponent } from './dialogs/dialog-cook-manager/dialog-cook-manager.component';
import { DialogEatobjectComponent } from './dialogs/dialog-eatobject/dialog-eatobject.component';
import { DialogQuanntitiveRegulationComponent } from './dialogs/dialog-quanntitive-regulation/dialog-quanntitive-regulation.component';
import { Radio2Component } from './dynamic-form-builder/atoms/radio2';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
    // suppressScrollX: true
};

@NgModule({
    declarations: [
        HeaderComponent,
        FooterComponent,
        AddTabDialogComponent,
        ArccordionComponent,
        FieldDialogComponent,
        ArccordionComponent,
        BasicSnackbarComponent,
        FieldItemComponent,
        RenderFormDialogComponent,
        FormGroupComponent,
        FormItemComponent,
        MenuComponent,
        MenuItemComponent,

        TextComponent,
        PasswordComponent,
        TextareaComponent,
        EmailComponent,
        DateTimeComponent,
        CheckBoxComponent,
        NumberComponent,
        SelectComponent,
        MultiselectComponent,
        RadioComponent,
        Radio2Component,
        FileComponent,

        ButtonComponent,
        QldkDialogComponent,
        SearchComponent,
        DynamicFormBuilderComponent,
        DynamicFormRenderComponent,
        FieldBuilderComponent,
        CardComponent,
        DeleteConfirmDialogComponent,
        SearchAdvancedComponent,
        DialogRejectStatusComponent,
        DialogQuantitativeComponent,
        DeleteConfirmAllDialogComponent,
        DialogEstimateFoodComponent,
        DialogSwapUnitComponent,
        DialogMealRateComponent,
        DialogCookManagerComponent,
        DialogEatobjectComponent,
        DialogQuanntitiveRegulationComponent,
        UpdateFieldDialogComponent,
    ],
    exports: [
        HeaderComponent,
        FooterComponent,
        ArccordionComponent,
        DynamicFormBuilderComponent,
        DynamicFormRenderComponent,
        ButtonComponent,
        SearchComponent,
        SearchAdvancedComponent,
        FieldItemComponent,
        CardComponent,
        MenuComponent,
        DialogRejectStatusComponent,
        DialogQuantitativeComponent,
        DeleteConfirmAllDialogComponent,
        DialogEstimateFoodComponent,
        DialogSwapUnitComponent,
        DialogMealRateComponent,
        DialogCookManagerComponent,
        DialogEatobjectComponent,
        DialogQuanntitiveRegulationComponent,
        MenuComponent,
        // UpdateFieldDialogComponent,
    ],
    providers: [
        {
            provide: PERFECT_SCROLLBAR_CONFIG,
            useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG,
        },
    ],

    imports: [
        CommonModule,
        RouterModule,
        CoreModule,
        PerfectScrollbarModule,
        NgbModule,
        FormsModule,
        ReactiveFormsModule,
        TranslateModule.forChild(),
        LoadingBarModule,
        SharedModule,
        MatCardModule,
        MatDialogModule,
        MatDividerModule,
        MatDatepickerModule,
        MatDialogModule,
        MatButtonModule,
        MatInputModule,
        MatChipsModule,
        MatIconModule,
        MatSelectModule,
        MatExpansionModule,
        MatTabsModule,
        MatFormFieldModule,
        MatSnackBarModule,
        MatMenuModule,
        MatButtonToggleModule,
        MatTableModule,
        MatCheckboxModule,
        MatProgressSpinnerModule,
        ReactiveFormsModule,
        MatProgressSpinnerModule,
        ReactiveFormsModule,
        MatIconModule,
        MatRadioModule,
        MatToolbarModule,
        MatNativeDateModule,
        MatDatepickerModuleCustom,
        MatPaginatorModule,
        MatListModule,
    ],
    entryComponents: [],
})
export class LayoutModule {}
