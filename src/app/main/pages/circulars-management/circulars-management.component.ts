import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatPaginator, MatTableDataSource } from '@angular/material';
import { DynamicDialogComponent } from '@app/components/dynamic-dialog-component/dynamic-dialog-component.component';
import { DeleteConfirmDialogComponent } from '@app/_shared/dialogs/delete-confirm-dialog/delete-confirm-dialog.component';
import { CircularService } from '@core/services/circular-service';
import { CommonService } from '@core/services/common.service';
import { ToastrService } from '@core/services/toastr.service';

export const DATA_EXAMPLE = [
    {
        soThongTu: '168/2021/TT-BQP',
        tenThongTu: 'Quy định tiêu chuẩn, định lượng ăn và mức ...',
        ngayBanHanh: '29/08/2022',
        trangThai: '1',
    },
    {
        soThongTu: '168/2021/TT-BQP',
        tenThongTu: 'Quy định tiêu chuẩn, định lượng ăn và mức ...',
        ngayBanHanh: '29/08/2022',
        trangThai: '1',
    },
];

@Component({
    selector: 'app-circulars-management',
    templateUrl: './circulars-management.component.html',
    styleUrls: ['./circulars-management.component.scss'],
})
export class CircularsManagementComponent implements OnInit {
    fieldSearch: any[] = [
        {
            type: 'SELECT',
            referenceValue: 'trangThai',
            name: 'Trạng thái',
            defaultValue: '',
            required: '0',
            css: 'col-12 col-lg-6',
            options: [
                { key: 1, name: 'Hoạt động' },
                { key: 0, name: 'Tạm ngưng' },
            ],
            appearance: 'legacy',
        },
        {
            type: 'DATETIME',
            referenceValue: 'ngayCoHieuLuc',
            name: 'Ngày ban hành',
            defaultValue: '',
            required: '0',
            css: 'col-12 col-lg-6',
            appearance: 'legacy',
        },
    ];

    displayedColumnsOfField = [
        'soThongTu',
        'tenThongTu',
        'ngayBanHanh',
        'trangThai',
        'actions',
    ];

    dataSource = new MatTableDataSource<any>([]);
    @ViewChild(MatPaginator) paginator!: MatPaginator;
    allowExec: boolean = false;
    currentTable: any[] = [];

    constructor(
        public dialog: MatDialog,
        private _toastrService: ToastrService,
        private circularService: CircularService,
        private commonService: CommonService,
    ) {
        let request = {
            id: localStorage.getItem('idMenu'),
            language: 'vi',
        };

        this.commonService.checkPermission(request).subscribe((response) => {
            if (JSON.parse(response.result).some((obj) => obj.key === 'EXEC')) {
                this.allowExec = true;
            }
        });
    }

    ngOnInit() {
        this.getInitData();
    }

    getInitData() {
        this.circularService.getAllCirculars({}).subscribe((response) => {
            if (response.success) {
                this.currentTable = response.result.items;
                this.dataSource = new MatTableDataSource<any>(
                    this.currentTable,
                );
                this.dataSource.paginator = this.paginator;
            }
        });
    }

    openDialog(data: any, type: string) {
        let fields = [
            {
                type: 'TEXT',
                referenceValue: 'soThongTu',
                name: 'Số thông tư',
                defaultValue: '',
                required: '1',
                css: 'col-12 col-lg-6',
                appearance: 'legacy',
            },
            {
                type: 'TEXT',
                referenceValue: 'tenThongTu',
                name: 'Tên thông tư',
                defaultValue: '',
                required: '1',
                css: 'col-12 col-lg-6',
                appearance: 'legacy',
            },
            {
                type: 'DATETIME',
                referenceValue: 'ngayCoHieuLuc',
                name: 'Ngày ban hành',
                defaultValue: '',
                required: '1',
                css: 'col-12 col-lg-6',
                appearance: 'legacy',
            },
            {
                type: 'RADIO',
                referenceValue: 'trangThai',
                name: 'Trạng thái',
                defaultValue: 1,
                options: [
                    { key: 1, name: 'Hoạt động' },
                    { key: 0, name: 'Tạm ngưng' },
                ],
                css: 'col-12 col-lg-6',
            },
        ];

        let dialogRef = this.dialog.open(DynamicDialogComponent, {
            // disableClose: true,
            width: '70%',
            data: {
                datas: data,
                fields: fields,
                typeDialog: type,
            },
            panelClass: 'my-custom-dialog-class',
        });

        dialogRef.componentInstance.onSave.subscribe((formValues) => {
            const isSubmitted = !!formValues;

            let dataSubmit = {
                ...data,
                ...formValues,
            };

            if (!data) {
                this.circularService.createCirculars(dataSubmit).subscribe(
                    (res) => dialogRef.close(isSubmitted),
                    (err) => this._toastrService.errorServer(err),
                );
            } else {
                this.circularService.updateCirculars(dataSubmit).subscribe(
                    (res) => dialogRef.close(isSubmitted),
                    (err) => this._toastrService.errorServer(err),
                );
            }
        });

        dialogRef.afterClosed().subscribe((result) => {
            if (!result) return;

            if (!data) {
                this._toastrService.success('', 'Thêm thành công');
            } else {
                this._toastrService.success('', 'Cập nhật thành công');
            }
            this.getInitData();
        });
    }

    deleteData(data: any) {
        let dataValue = data;

        const dialogRef = this.dialog.open(DeleteConfirmDialogComponent, {
            width: '500px',
            data: data,
        });

        dialogRef.componentInstance.handleDelete.subscribe((dataDelete) => {
            const isSubmitted = !!dataDelete;

            let bodySubmit = {
                maxResultCount: 2147483647,
                criterias: [
                    {
                        propertyName: 'thongTuId',
                        operation: 0,
                        value: dataValue.id,
                    },
                ],
            };
            this.commonService
                .callDataAPIShort(
                    '/api/services/read/DinhLuongAn/GetAll',
                    bodySubmit,
                )
                .subscribe((response) => {
                    if (response.result.totalCount > 0) {
                        this._toastrService.error(
                            '',
                            'Thông tư này còn đang sử dụng',
                        );
                    } else {
                        this.circularService
                            .deleteCirculars(dataDelete)
                            .subscribe(
                                (res) => dialogRef.close(isSubmitted),
                                (err) => this._toastrService.errorServer(err),
                            );
                    }
                });
        });

        dialogRef.afterClosed().subscribe((result) => {
            if (!result) return;

            this.getInitData();
            this._toastrService.success('', 'Xóa thành công');
        });
    }

    getStatus(num: any) {
        if (num === 1) {
            return 'Hoạt động';
        } else if (num === 0) {
            return 'Tạm ngưng';
        }
    }

    download(data: any) {
        let dataDownload = JSON.parse(data.hideValue);

        for (let i of dataDownload.attachments) {
            setTimeout(function () {
                window.open(i.fileUrl, '_blank');
            }, 1000);
        }
    }

    handleSearch(data: any) {
        this.dataSource.data = this.currentTable.filter((item) =>
            item.soThongTu.toLowerCase().includes(data.trim().toLowerCase()),
        );
    }

    handleSearchAdvanced(data: any) {
        if (data.key === 'reset') {
            this.getInitData();
        } else {
            let bodySubmit = {
                criterias: [
                    {
                        propertyName: data.ngayCoHieuLuc ? 'ngayCoHieuLuc' : '',
                        operation: data.ngayCoHieuLuc ? 'Equals' : 'Contains',
                        value: data.ngayCoHieuLuc
                            ? new Date(data.ngayCoHieuLuc._d).toISOString()
                            : '',
                    },
                    {
                        propertyName:
                            data.trangThai !== null && data.trangThai !== ''
                                ? 'trangThai'
                                : '',
                        operation:
                            data.trangThai !== null && data.trangThai !== ''
                                ? 'Equals'
                                : 'Contains',
                        value:
                            data.trangThai !== null && data.trangThai !== ''
                                ? data.trangThai
                                : '',
                    },
                ],
            };
            this.circularService
                .getAllCirculars(bodySubmit)
                .subscribe((response) => {
                    if (response.success) {
                        this.dataSource = new MatTableDataSource<any>(
                            response.result.items,
                        );
                        this.dataSource.paginator = this.paginator;
                    }
                });
        }
    }
}
