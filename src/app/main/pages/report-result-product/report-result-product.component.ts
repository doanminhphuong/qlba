import { Component, OnInit } from '@angular/core';
import {
    animate,
    state,
    style,
    transition,
    trigger,
} from '@angular/animations';
import { resolve } from 'url';
import {
    FormBuilder,
    FormControl,
    FormGroup,
    Validators,
} from '@angular/forms';
import { Observable } from 'rxjs';
import { map, mergeMap, pairwise, startWith } from 'rxjs/operators';
import {
    DateAdapter,
    MatDatepickerInputEvent,
    MatDialog,
    MatTableDataSource,
    MAT_DATE_FORMATS,
    MAT_DATE_LOCALE,
} from '@angular/material';
import {
    MAT_MOMENT_DATE_ADAPTER_OPTIONS,
    MomentDateAdapter,
} from '@angular/material-moment-adapter';
import { MY_FORMATS } from '../inventory-import/import-add-edit/import-add-edit.component';
import { type } from 'os';
import { ExportExcelService } from '@core/services/export-excel.service';
import { CommonService } from '@core/services/common.service';
import { HttpErrorResponse } from '@angular/common/http';
import { ClassGetter } from '@angular/compiler/src/output/output_ast';
import { HoSoSucKhoeService } from '@core/services/ho-so-suc-khoe.service';
import { DataService } from '@core/services/data-service';
import { data } from 'jquery';
import * as moment from 'moment';
import { MONTH_MODE_FORMATS } from '../lttp-report/lttp-report.component';

@Component({
    selector: 'app-report-result-product',
    templateUrl: './report-result-product.component.html',
    styleUrls: ['./report-result-product.component.scss'],
    animations: [
        trigger('detailExpand', [
            state('collapsed', style({ height: '0px', minHeight: '0' })),
            state('expanded', style({ height: '*' })),
            transition(
                'expanded <=> collapsed',
                animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)'),
            ),
        ]),
    ],
    providers: [
        {
            provide: DateAdapter,
            useClass: MomentDateAdapter,
            deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS],
        },

        { provide: MAT_DATE_LOCALE, useValue: 'vi-VI' },
        { provide: MAT_DATE_FORMATS, useValue: MONTH_MODE_FORMATS },
        {
            provide: MAT_MOMENT_DATE_ADAPTER_OPTIONS,
            useValue: { useUtc: true },
        },
    ],
})
export class ReportResultProductComponent implements OnInit {
    dataSource = ELEMENT_DATA;
    columnsToDisplay = ['name', 'weight', 'symbol', 'position', 'description'];
    dataReport = new MatTableDataSource<any>([]);
    dataReportExportExcel;
    thanhTienNgayTruoc = 0;
    thanhTienCongNhap = 0;
    thanhTienCongXuat = 0;
    danhSachThu = 0;
    danhSachThuList = [];
    listUnit = [];
    listDayinMonth: number[] = [];
    reportResultCook: ReportResultCook[] = [];
    reportResultCookDefaultCheck: number[] = [];
    _tempDataTable: ReportResultCook[] = [];
    reportResultCookHeader: ReportResultCook;
    isLoadingResults: boolean = false;

    dataFinancialOnDay = [];
    initialData = new Array(19);
    rfSearch: FormGroup;
    options: string[] = ['One', 'Two', 'Three'];
    headerTable: string[] = [
        'id',
        'tenLTTP',
        'dvt',
        'donGia',
        'soLuong',
        'thanhTien',
        'trenDBTT',
        'trenDBTG',
        'donViTT',
        'donViTg',
        'nhapTrongNgayThanhTien',
        'SoLuongCN',
        'ThanhTienCN',
        // 'SoLuongXA',
        // 'ThanhTienXA',
        // 'SoLuongXK',
        // 'ThanhTienXK',
        // 'SoLuongCX',
        // 'ThanhTienCX',
    ];
    foods: Food[] = [
        { value: 'steak-0', viewValue: 'Steak' },
        { value: 'pizza-1', viewValue: 'Pizza' },
        { value: 'tacos-2', viewValue: 'Tacos' },
    ];

    statisticals: Statistical[] = [
        {
            value: this.thanhTienNgayTruoc,
            title: 'Ngày trước chuyển qua',
            icon: ' attach_money',
        },
        {
            value: this.thanhTienCongNhap,
            title: 'Tổng nhập',
            icon: 'show_chart',
        },
        {
            value: this.thanhTienCongXuat,
            title: 'Tổng xuất',
            icon: 'show_chart',
            class: 'custom-icon',
        },
        { value: '923', title: 'Quân số ăn', icon: 'stars' },
    ];

    displayedColumns: string[] = ['position', 'name', 'weight', 'symbol'];
    dataToDisplay = [...ELEMENT_DATA];

    displayedColumnsMeal = ['meal', 'cost', 'price', 'total'];
    // transactionsMeal: TransactionMeal[] = [
    //     { meal: 'Sáng', cost: 44, price: 13000 },
    //     { meal: 'Trưa', cost: 50, price: 220000 },
    //     { meal: 'Chiều', cost: 12, price: 490000 },
    // ];

    filteredOptions: Observable<string[]>;
    minDate;

    constructor(
        private fb: FormBuilder,
        private exportExcelService: ExportExcelService,
        private commonService: CommonService,
        public _hssk: HoSoSucKhoeService,
        private _dataService: DataService,
        public dialog: MatDialog,
    ) { }

    ngOnInit() {
        this.getInitForm();
        this.getInitData();

        // this.onRfSearchChanges();
    }

    getInitData() {
        this.getDataCurrentOrganizations();
        this.getCurrentCookHoodUserUse();
        this.getBaoCaoData();
    }

    getBaoCaoData(): void {
        let formValue = this.rfSearch.value;
        let { nameCook, fromDay } = formValue;
        let monthUseCook = moment(fromDay).month() + 1;
        let yearUseCook = moment(fromDay).year();

        if (this.rfSearch.valid) {
            this.isLoadingResults = true;

            let body = {
                maxResultCount: 2147483647,
                criterias: [
                    {
                        propertyName: 'key',
                        operation: 6,
                        value: 'REPORT_DINHLUONG_NHIETLUONG',
                    },
                ],
            };
            this.commonService
                .callDataAPIFromTASPShort(
                    '/api/services/Data/read/Setting/GetList',
                    body,
                )
                .pipe(
                    mergeMap((dataMerge) => {
                        let resultData = dataMerge.result[0];
                        const _URL =
                            '/api/services/read/BaoCao/GetBaoCaoDinhLuongNhietLuong';
                        let valueSeting = resultData.value;
                        let bodyMerge = {
                            dauMoiBepId: nameCook,
                            thang: monthUseCook,
                            nam: yearUseCook,
                            setting: valueSeting,
                        };
                        return this.commonService.callDataAPIShort(
                            _URL,
                            bodyMerge,
                        );
                    }),
                )
                .pipe(map((data) => {
                    return data.result.danhSach;
                }))
                .subscribe(
                    (response) => {
                        this.isLoadingResults = false;
                        this.listDayinMonth = this.getDays(
                            yearUseCook,
                            monthUseCook,
                        );
                        let _reportResultCook =
                            this.convertDataRenderForm(response);
                        this.reportResultCook = _reportResultCook;
                        this.reportResultCookDefaultCheck = new Array(
                            _reportResultCook.length,
                        ).fill(0);
                        this.subListName();
                    },
                    (err) => {
                        console.log(err);
                        this.isLoadingResults = false;
                    },
                );
        }
    }

    getDataCurrentOrganizations() {
        this._hssk
            .getCurrentOrganizations()
            .pipe(
                mergeMap((dataMerge) => {
                    const _URL = '/api/services/read/DauMoiBep/GetAll';
                    let resultData = dataMerge.result[0];
                    let id = (resultData && resultData.id) || 0;
                    let body = {
                        donViId: id,
                    };
                    return this.commonService.callDataAPIShort(_URL, body);
                }),
            )
            .pipe(map((res) => res.result && res.result.items))
            .subscribe((data) => {
                this.listUnit = data;
            });
    }

    getCurrentCookHoodUserUse() {
        let currentHood = this._dataService.getDauMoiBepId();
        if (currentHood) {
            this.rfSearch.patchValue({
                nameCook: currentHood,
            });
        }
    }

    // getMindate() {
    //     let fromDay = this.rfSearch.get('fromDay').value;
    //     this.minDate = fromDay;
    // }

    addEvent(type: string, event: MatDatepickerInputEvent<Date>) {
        console.log(event);
    }

    isCheckValue = (value) => !value;

    onRfSearchChanges() {
        this.rfSearch
            .get('fromDay')
            .valueChanges.pipe(startWith(null as string), pairwise())
            .subscribe(([prev, next]: [any, any]) => {
                if (!this.isCheckValue(next)) {
                    this.rfSearch.get('toDay').enable();
                    this.minDate = new Date(next);
                }
            });
    }

    getInitForm() {
        this.rfSearch = this.fb.group({
            nameCook: ['', [Validators.required]],
            typeTime: '',
            unit: '',
            time: '',
            fromDay: [moment().format('YYYY-MM-DD'), [Validators.required]],
            toDay: [{ value: '', disabled: true }],
        });
    }

    onSubmit() {
        this.getBaoCaoData();

        // example body data
        // {
        //   dauMoiBepId: 27,
        //   thang: 3,
        //   nam: 2023,
        //   setting: valueSeting,
        // }

        // this.dataReportExportExcel = this.dataReport.data;
        // this.commonService
        //     .callDataAPIShort(
        //         '/api/services/read/BaoCao/GetBaoCaoCongKhaiTaiChinh',
        //         {
        //             dauMoiBepId: 5,
        //             ngayBaoCao: '2022-12-12',
        //         },
        //     )
        //     .subscribe(
        //         (data) => {
        //             let { result } = data;
        //             this.dataReportExportExcel = result;

        //             let { congKhaiTaiChinh, nhapXuatTrongNgay } = result;
        //             let { danhSachThu } = congKhaiTaiChinh;
        //             this.danhSachThuList =
        //                 this.converToTransactionsMealObj(danhSachThu);
        //             if (nhapXuatTrongNgay) {
        //                 this.dataReport = nhapXuatTrongNgay;

        //                 //Change data export Excel
        //                 this.thanhTienNgayTruoc = this.sumInitial(
        //                     nhapXuatTrongNgay,
        //                     'thanhTienNgayTruoc',
        //                 );
        //                 this.thanhTienCongNhap = this.sumInitial(
        //                     nhapXuatTrongNgay,
        //                     'thanhTienCongNhap',
        //                 );
        //                 this.thanhTienCongXuat = this.sumInitial(
        //                     nhapXuatTrongNgay,
        //                     'thanhTienCongXuat',
        //                 );

        //                 //Change data layout
        //                 this.statisticals[0].value = this.thanhTienNgayTruoc;
        //                 this.statisticals[1].value = this.thanhTienCongNhap;
        //                 this.statisticals[2].value = this.thanhTienCongXuat;
        //                 this.statisticals[3].value =
        //                     this.sumInitialUser(danhSachThu);
        //             }
        //             this.headerTable = [
        //                 'id',
        //                 'tenLttpChatDot',
        //                 'donViTinh',
        //                 'donGia',
        //                 'soLuongNgayTruoc',
        //                 'thanhTienNgayTruoc',
        //                 'nhapTrenBdMuaTT',
        //                 'nhapTrenBdTGCB',
        //                 'nhapDonViBdMuaTT',
        //                 'nhapDonViBdTGCB',
        //                 'thanhTienNhap',
        //                 'soLuongCongNhap',
        //                 'thanhTienCongNhap',
        //                 'soLuongXuatAn',
        //                 'thanhTienXuatAn',
        //                 'soLuongXuatKhac',
        //                 'thanhTienXuatKhac',
        //                 'soLuongCongXuat',
        //                 'thanhTienCongXuat',
        //             ];
        //         },
        //         (err: HttpErrorResponse) => {
        //             console.log(err);
        //             alert(err.message);
        //         },
        //     );
    }
    resetForm() {
        this.rfSearch.reset();
    }

    changeCookDefault(data) {
        this.reportResultCookDefaultCheck = data;
    }

    /**
     * Returns x New list have key "ketQua" use check day empty data
     *
     * @param {ReportResultCook} dataTable The origin data from API (Add more key "ketQua" from Origin Object)
     * @return {ReportResultCook} x New list have key "ketQua" use check day emptydata
     */
    convertDataRenderForm(dataTable) {
        let _dataTable = dataTable;
        for (let listItem of _dataTable) {
            let result = [];
            let elementItem = listItem.danhSach;
            this.listDayinMonth.forEach((day) => {
                const itemDay = elementItem.find((item) => item.ngay === day);

                if (itemDay) {
                    result.push(itemDay);
                } else {
                    result.push({
                        ngay: day,
                        xuatAnThucTe: 0,
                        dinhLuongBinhQuan: 0,
                    });
                }
            });
            listItem.ketQua = result;
        }

        return _dataTable;
    }

    subListName() {
        let data = this.reportResultCook.filter(
            (item) =>
                !item.ten.toLowerCase().includes('qs') &&
                !item.ten.toLowerCase().includes('nhiệt'),
        );

        this._tempDataTable = data;
    }

    getDays = (year, month) => {
        let dayInCurrentMonth = new Date(year, month, 0).getDate();
        let allDayInMonth = [];
        for (let index = 1; index <= dayInCurrentMonth; index++) {
            allDayInMonth.push(index);
        }

        return allDayInMonth;

        // return new Date(year, month, 0).getDate();
    };

    // getDaysByMonth(year, month){
    //   let totoDay = this.getDays(year, month)
    // }

    exportExcel() {
        let formValue = this.rfSearch.value;
        let { nameCook, fromDay } = formValue;

        let bodyDataReportExcel = {
            dataCook: { ...this.reportResultCook },
            defaultCookCheck: this.reportResultCookDefaultCheck,
            // thanhTienNgayTruoc: this.thanhTienNgayTruoc,
            // thanhTienCongNhap: this.thanhTienCongNhap,
            // thanhTienCongXuat: this.thanhTienCongXuat,
            thoiGian: new Date(fromDay),
        };

        this.exportExcelService.generateExportQuantitiveRegulationExcel(
            bodyDataReportExcel,
            'KetQuaDamBaoLTTPChatDot',
        );
    }

    getFinancialInfo(valueForm) {
        let data = this.rfSearch.get(valueForm).value;
        if (valueForm === 'fromDay' || valueForm === 'toDay') {
            if (!this.isCheckValue(data)) {
                return new Date(data).toLocaleDateString();
            } else {
                return '';
            }
        } else {
            return data;
        }
    }

    getTotalCost(array) {
        return array.reduce((acc, value) => acc + value.total, 0);
    }

    sumInitial(listItem, key) {
        const initialValue = 0;
        const sumWithInitial = listItem.reduce(
            (accumulator, currentValue) => accumulator + currentValue[key],
            initialValue,
        );

        return sumWithInitial;
    }

    sumInitialUser(listItem) {
        const initialValue = 0;
        const sumWithInitial = listItem.reduce(
            (accumulator, currentValue) =>
                accumulator +
                (currentValue.thanhTienChieu +
                    currentValue.thanhTienSang +
                    currentValue.thanhTienTrua) /
                currentValue.mucTienAn,
            initialValue,
        );

        return sumWithInitial;
    }

    converToTransactionsMealObj(listItem) {
        let result = listItem.map((item) => {
            return {
                mucTienAn: item.mucTienAn,
                transactionsMeal: [
                    {
                        meal: 'Sáng',
                        cost: item.quanSoSang,
                        price: item.tienSang,
                        total: item.thanhTienSang,
                    },
                    {
                        meal: 'Trưa',
                        cost: item.quanSoTrua,
                        price: item.tienTrua,
                        total: item.thanhTienTrua,
                    },
                    {
                        meal: 'Chiều',
                        cost: item.quanSoChieu,
                        price: item.tienChieu,
                        total: item.thanhTienChieu,
                    },
                ],
            };
        });

        return [...result];
    }

    applyFilter(event: Event) {
        const filterValue = (event.target as HTMLInputElement).value;
        console.log(filterValue);
        this.dataReport.filter = filterValue.trim().toLowerCase();
    }
}

export interface PeriodicElement {
    name: string;
    position: number;
    weight: number;
    symbol: string;
    description: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
    {
        position: 1,
        name: 'Hydrogen',
        weight: 1.0079,
        symbol: 'H',
        description: `Hydrogen is a chemical element with symbol H and atomic number 1. With a standard
    atomic weight of 1.008, hydrogen is the lightest element on the periodic table.`,
    },
    {
        position: 2,
        name: 'Helium',
        weight: 4.0026,
        symbol: 'He',
        description: `Helium is a chemical element with symbol He and atomic number 2. It is a
    colorless, odorless, tasteless, non-toxic, inert, monatomic gas, the first in the noble gas
    group in the periodic table. Its boiling point is the lowest among all the elements.`,
    },
    {
        position: 3,
        name: 'Lithium',
        weight: 6.941,
        symbol: 'Li',
        description: `Lithium is a chemical element with symbol Li and atomic number 3. It is a soft,
    silvery-white alkali metal. Under standard conditions, it is the lightest metal and the
    lightest solid element.`,
    },
    {
        position: 4,
        name: 'Beryllium',
        weight: 9.0122,
        symbol: 'Be',
        description: `Beryllium is a chemical element with symbol Be and atomic number 4. It is a
    relatively rare element in the universe, usually occurring as a product of the spallation of
    larger atomic nuclei that have collided with cosmic rays.`,
    },
    {
        position: 5,
        name: 'Boron',
        weight: 10.811,
        symbol: 'B',
        description: `Boron is a chemical element with symbol B and atomic number 5. Produced entirely
    by cosmic ray spallation and supernovae and not by stellar nucleosynthesis, it is a
    low-abundance element in the Solar system and in the Earth's crust.`,
    },
    {
        position: 6,
        name: 'Carbon',
        weight: 12.0107,
        symbol: 'C',
        description: `Carbon is a chemical element with symbol C and atomic number 6. It is nonmetallic
    and tetravalent—making four electrons available to form covalent chemical bonds. It belongs
    to group 14 of the periodic table.`,
    },
    {
        position: 7,
        name: 'Nitrogen',
        weight: 14.0067,
        symbol: 'N',
        description: `Nitrogen is a chemical element with symbol N and atomic number 7. It was first
    discovered and isolated by Scottish physician Daniel Rutherford in 1772.`,
    },
    {
        position: 8,
        name: 'Oxygen',
        weight: 15.9994,
        symbol: 'O',
        description: `Oxygen is a chemical element with symbol O and atomic number 8. It is a member of
     the chalcogen group on the periodic table, a highly reactive nonmetal, and an oxidizing
     agent that readily forms oxides with most elements as well as with other compounds.`,
    },
    {
        position: 9,
        name: 'Fluorine',
        weight: 18.9984,
        symbol: 'F',
        description: `Fluorine is a chemical element with symbol F and atomic number 9. It is the
    lightest halogen and exists as a highly toxic pale yellow diatomic gas at standard
    conditions.`,
    },
    {
        position: 10,
        name: 'Neon',
        weight: 20.1797,
        symbol: 'Ne',
        description: `Neon is a chemical element with symbol Ne and atomic number 10. It is a noble gas.
    Neon is a colorless, odorless, inert monatomic gas under standard conditions, with about
    two-thirds the density of air.`,
    },
];

interface Food {
    value: string;
    viewValue: string;
}

interface Transaction {
    item: string;
    cost: number;
}

type TransactionMeal = {
    meal: string;
    cost: number;
    price: number;
};

export type Statistical = {
    value: number | string;
    title: string;
    icon: string;
    class?: string;
};

export interface ReportResultCook {
    ten: string;
    donViTinh: string;
    congBinhQuan: number;
    danhSach: ReportResultCookList[];
    ketQua?: ReportResultCookList[];
}

export interface ReportResultCookList {
    ngay: number;
    xuatAnThucTe: number;
    dinhLuongBinhQuan: number;
}

// {
//   "ten": "Thịt bò",
//   "donViTinh": "g",
//   "danhSach": [
//       {
//           "ngay": 1,
//           "xuatAnThucTe": 0,
//           "dinhLuongBinhQuan": 0
//       }
//   ],
//   "congBinhQuan": 0
// }
