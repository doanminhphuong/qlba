import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Params, Router } from '@angular/router';

@Component({
    selector: 'app-field-export',
    templateUrl: './field-export.component.html',
    styleUrls: ['./field-export.component.scss'],
})
export class FieldExportComponent implements OnInit {
    constructor(
        private _router: Router,
        private _activatedRoute: ActivatedRoute,
    ) {}

    ngOnInit() {
        this._activatedRoute.params.subscribe((data: Params) => {
            let lyDoSuDung = 'XK';
            let dataParam = data['name'];

            if (dataParam.includes('chi ăn') || dataParam.includes('chi')) {
                lyDoSuDung = 'XCA';
            }
            this._router.navigate([
                'app/kho/xuat-kho/xuat-kho/create',
                { lyDoSuDung },
            ]);
        });
    }
}
