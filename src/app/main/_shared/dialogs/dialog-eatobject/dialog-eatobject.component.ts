import {
    ChangeDetectorRef,
    Component,
    EventEmitter,
    Inject,
    OnInit,
} from '@angular/core';
import {
    FormArray,
    FormBuilder,
    FormControl,
    FormGroup,
    Validators,
} from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DynamicDialogComponent } from '@app/components/dynamic-dialog-component/dynamic-dialog-component.component';
import { CategoryService } from '@core/services/category.service';
import { CommonService } from '@core/services/common.service';
import { forkJoin } from 'rxjs';
import { DialogData } from '../category-dialog/category-dialog.component';

@Component({
    selector: 'app-dialog-eatobject',
    templateUrl: './dialog-eatobject.component.html',
    styleUrls: ['./dialog-eatobject.component.scss'],
})
export class DialogEatobjectComponent implements OnInit {
    onSave = new EventEmitter();
    form: FormGroup;
    listData = [];
    listObject = [];
    constructor(
        public dialog: MatDialog,
        public dialogRef: MatDialogRef<DynamicDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: DialogData,
        private readonly changeDetectorRef: ChangeDetectorRef,
        private commonService: CommonService,
        private categoryService: CategoryService,
    ) {}

    ngOnInit() {
        this.getInitForm();
    }

    ngAfterViewInit(): void {
        this.getInitData();
    }

    ngAfterViewChecked(): void {
        this.changeDetectorRef.detectChanges();
    }

    getInitForm() {
        let fieldsCtrls = {};

        for (let f of this.data.fields) {
            if (f.type === 'NUMBER') {
                fieldsCtrls[f.referenceValue] = new FormControl(
                    { value: f.defaultValue, disabled: f.disabled },
                    f.required === '1' ? [Validators.required] : [],
                );
            } else if (f.type !== 'CHECKBOX') {
                let validators = [
                    Validators.minLength(f.minLength),
                    Validators.maxLength(f.maxLength),
                    Validators.pattern(f.pattern),
                ];

                if (f.type === 'EMAIL') {
                    validators = [...validators, Validators.email];
                }

                fieldsCtrls[f.referenceValue] = new FormControl(
                    { value: f.defaultValue, disabled: f.disabled },
                    f.required === '1'
                        ? [...validators, Validators.required]
                        : [...validators],
                );
            } else {
                //if checkbox, it need multiple
                let opts = {};
                for (let opt of f.options) {
                    opts[opt.key] = new FormControl({
                        value: opt.value,
                        disabled: f.disabled,
                    });
                }
                fieldsCtrls[f.referenceValue] = new FormGroup(opts);
            }
        }

        this.form = new FormGroup(fieldsCtrls);
    }

    getInitData(): void {
        let body = {
            maxResultCount: 999999,
            skipCount: 0,
            sorting: 'Code',
            criterias: [
                {
                    propertyName: 'GroupCode',
                    operation: 6,
                    value: 'doi-tuong-an',
                },
            ],
        };

        let dataEdit = this.data.datas;
        if (!dataEdit) return;

        this.commonService
            .callDataAPIShort('/api/services/read/DinhLuongAn/GetCurrent', {
                id: dataEdit,
            })
            .subscribe((response) => {
                // response.result.find(
                //     (item) =>
                //         (this.listObject = JSON.parse(item.nhomDoiTuongId)),
                // );
                console.log('response', response);

                console.log('data2', this.listObject);
            });
        this.categoryService.getAllCategory(body).subscribe((response) => {
            this.listData = response.result.map((item) => ({
                key: item.id,
                name: item.name,
            }));
        });
    }
    compareIdToNameObject(id: any) {
        let data;
        let dataList = this.listData;

        if (this.listObject.length > 0) {
            let item = dataList.find((item) => item.key === id);
            data = item && item.name ? item.name : '';
        }
        return data;
    }

    getFields() {
        return this.data.fields;
    }

    replaceWhiteSpace(): void {
        for (let f of this.data.fields) {
            if (f.type === 'TEXT' || f.type === 'TEXTAREA') {
                const initField = this.form.get(f.referenceValue);

                if (initField.value === null) {
                    initField.setValue('');
                } else {
                    const valueAfterReplace = initField.value
                        .replace(/\s+/g, ' ')
                        .trim();
                    initField.setValue(valueAfterReplace);
                }
            }
        }
    }

    onCloseDialog(data?: any): void {
        this.dialogRef.close(data);
    }

    onSubmit(): void {
        this.replaceWhiteSpace();

        let formValues = this.form.value;
        this.onSave.emit(formValues);
    }
}
