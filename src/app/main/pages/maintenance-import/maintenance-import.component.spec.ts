import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MaintenanceImportComponent } from './maintenance-import.component';

describe('MaintenanceImportComponent', () => {
  let component: MaintenanceImportComponent;
  let fixture: ComponentFixture<MaintenanceImportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MaintenanceImportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MaintenanceImportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
