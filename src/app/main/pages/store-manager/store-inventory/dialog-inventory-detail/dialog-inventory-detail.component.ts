import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import {
    MatDialogRef,
    MatPaginator,
    MatTableDataSource,
    MAT_DIALOG_DATA,
} from '@angular/material';
import { Router } from '@angular/router';
import { MaterialDialogComponent } from '@app/_shared/dialogs/material-dialog/material-dialog.component';
import { InventoryService } from '@core/services/inventory.service';

export const DATA_EXAMPLE = [
    {
        licenseType: 'Tồn kho',
        licenseDate: '02/07/2023',
        licenseNumber: 'TK',
        unit: 'KG',
        amountIn: '0',
        price: '15.000',
        amount: '100',
    },
    {
        licenseType: 'Nhập kho',
        licenseDate: '02/07/2023',
        licenseNumber: 'NK02072022',
        unit: 'KG',
        amountIn: '100',
        price: '15.000',
        amount: '100',
    },
    {
        licenseType: 'Xuất kho',
        licenseDate: '03/07/2023',
        licenseNumber: 'NK02072022',
        unit: 'KG',
        amountIn: '100',
        price: '15.000',
        amount: '100',
    },
    {
        licenseType: 'Xuất kho',
        licenseDate: '03/07/2023',
        licenseNumber: 'NK02072022',
        unit: 'KG',
        amountIn: '100',
        price: '15.000',
        amount: '100',
    },
    {
        licenseType: 'Xuất kho',
        licenseDate: '03/07/2023',
        licenseNumber: 'NK02072022',
        unit: 'KG',
        amountIn: '100',
        price: '15.000',
        amount: '100',
    },
];
@Component({
    selector: 'app-dialog-inventory-detail',
    templateUrl: './dialog-inventory-detail.component.html',
    styleUrls: ['./dialog-inventory-detail.component.scss'],
})
export class DialogInventoryDetailComponent implements OnInit {
    displayedColumnsOfField: string[] = [
        'licenseType',
        'licenseDate',
        'licenseNumber',
        'unit',
        'yesterday',
        'deviant',
        'amount',
        'note',
        'action',
    ];

    dataSource = new MatTableDataSource<any>([]);
    @ViewChild(MatPaginator) paginator!: MatPaginator;

    isPicker: boolean = false;
    dateStart = new FormControl('');
    dateEnd = new FormControl('');
    codeName = new FormControl('');
    defaultSelected = 'all';
    minDate;

    constructor(
        @Inject(MAT_DIALOG_DATA) public data: any,
        public dialogRef: MatDialogRef<MaterialDialogComponent>,
        private inventoryService: InventoryService,
        private _router: Router,
    ) {
        if (this.data.type === 'dashboard') {
            this.displayedColumnsOfField.splice(8, 1);
        }
    }

    ngOnInit() {
        console.log('data:: ', this.data);
        this.getInitData();
    }

    getInitData() {
        let bodyRequest = {
            maxResultCount: 9999999,
            sorting: 'creationTime ASC',
            criterias: [
                {
                    propertyName: 'tonKhoId',
                    operation: 'Equals',
                    value: this.data.id,
                },
            ],
        };
        this.inventoryService
            .getHistoryProductOfInventory(bodyRequest)
            .subscribe((response) => {
                this.dataSource = new MatTableDataSource<any>(
                    response.result.items,
                );
                this.dataSource.paginator = this.paginator;
            });
    }

    enablePicker(event) {
        if (event.value) {
            this.isPicker = true;
            this.minDate = event.value;
        }
    }

    checkPositiveNumbers(number) {
        if (number > 0) {
            return '+' + number;
        } else if (number < 0) {
            return number;
        } else return '';
    }

    onCloseDialog(data?: any): void {
        this.dialogRef.close(data);
    }

    navigateDetail(element, type) {
        let typeAction = 3;
        if (type === 'tinhKho') {
            this._router.navigate([
                'app/kho/bien-ban-tinh-kho/detail/' + element.phieuId,
            ]);
        } else if (type === 'xuatKho') {
            this._router.navigate([
                'app/kho/xuat-kho/edit/' + element.phieuId,
                { typeAction },
            ]);
        } else if (type === 'nhapKho') {
            this._router.navigate([
                'app/kho/nhap-kho/edit/' + element.phieuId,
                { typeAction },
            ]);
        }
        this.onCloseDialog();
    }

    filter() {
        let bodyRequest = {
            maxResultCount: 9999999,
            sorting: 'creationTime ASC',
            criterias: [
                {
                    propertyName: 'tonKhoId',
                    operation: 'Equals',
                    value: this.data.id,
                },
                // Search maPhieu
                {
                    propertyName: this.codeName.value ? 'maPhieu' : '',
                    operation: 'Contains',
                    value: this.codeName.value ? this.codeName.value : '',
                },
                // Search loaiPhieu
                {
                    propertyName:
                        this.defaultSelected !== 'all' ? 'loaiPhieu' : '',
                    operation:
                        this.defaultSelected !== 'all' ? 'Equals' : 'Contains',
                    value:
                        this.defaultSelected !== 'all'
                            ? this.defaultSelected
                            : '',
                },
                // Search tuNgay
                {
                    propertyName: this.dateStart.value ? 'ngay' : '',
                    operation: this.dateStart.value
                        ? 'GreaterThanOrEqual'
                        : 'Contains',
                    value: this.dateStart.value
                        ? new Date(this.dateStart.value)
                              .toISOString()
                              .replace('T17', 'T00')
                        : '',
                },
                // Search denNgay
                {
                    propertyName: this.dateEnd.value ? 'ngay' : '',
                    operation: this.dateEnd.value
                        ? 'LessThanOrEqual'
                        : 'Contains',
                    value: this.dateEnd.value
                        ? new Date(this.dateEnd.value)
                              .toISOString()
                              .replace('T17', 'T00')
                        : '',
                },
            ],
        };

        this.inventoryService
            .getHistoryProductOfInventory(bodyRequest)
            .subscribe((response) => {
                this.dataSource = new MatTableDataSource<any>(
                    response.result.items,
                );
                this.dataSource.paginator = this.paginator;
            });
    }

    reset() {
        this.defaultSelected = 'all';
        this.codeName.reset();
        this.dateStart.reset();
        this.dateEnd.reset();
        this.getInitData();
    }

    handleSelectFilter(dataSelected: any) {
        this.defaultSelected = dataSelected.value;
    }
}
