import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreateImportToolManagerComponent } from './list-import-tool-manager/create-import-tool-manager/create-import-tool-manager.component';
import { ListImportToolManagerComponent } from './list-import-tool-manager/list-import-tool-manager.component';
import { ToolManagerMainComponent } from './tool-manager-main/tool-manager-main.component';
import { ToolManagerStoreComponent } from './tool-manager-store/tool-manager-store.component';

const routes: Routes = [
    {
        path: '',
        redirectTo: 'ds-dung-cu-cap-duong',
        pathMatch: 'full',
    },
    {
        path: 'ds-dung-cu-cap-duong',
        component: ToolManagerMainComponent,
    },
    {
        path: 'kho-dung-cu-cap-duong',
        component: ToolManagerStoreComponent,
    },
    // {
    //     path: 'tao-phieu-nhap-dccd',
    //     component: CreateImportToolManagerComponent,
    // },
    {
        path: 'ds-pnk-dccd',
        // component: ListImportToolManagerComponent,
        children: [
            {
                path: '',
                component: ListImportToolManagerComponent,
            },
            {
                path: 'create',
                component: CreateImportToolManagerComponent,
            },
            {
                path: 'edit/:id',
                component: CreateImportToolManagerComponent,
            },
        ],
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class ToolManagerRoutingModule {}
