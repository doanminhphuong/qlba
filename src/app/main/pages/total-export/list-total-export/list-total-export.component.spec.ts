/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { ListTotalExportComponent } from './list-total-export.component';

describe('ListTotalExportComponent', () => {
  let component: ListTotalExportComponent;
  let fixture: ComponentFixture<ListTotalExportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListTotalExportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListTotalExportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
