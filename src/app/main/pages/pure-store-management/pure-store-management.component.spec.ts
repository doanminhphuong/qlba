import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PureStoreManagementComponent } from './pure-store-management.component';

describe('PureStoreManagementComponent', () => {
  let component: PureStoreManagementComponent;
  let fixture: ComponentFixture<PureStoreManagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PureStoreManagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PureStoreManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
