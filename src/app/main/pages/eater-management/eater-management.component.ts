import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatPaginator, MatTableDataSource } from '@angular/material';
import { Router } from '@angular/router';
import { DeleteConfirmDialogComponent } from '@app/_shared/dialogs/delete-confirm-dialog/delete-confirm-dialog.component';
import { CommonService } from '@core/services/common.service';
import { DataService } from '@core/services/data-service';
import { ToastrService } from '@core/services/toastr.service';
import { Subject, Subscription } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';
@Component({
    selector: 'app-eater-management',
    templateUrl: './eater-management.component.html',
    styleUrls: ['./eater-management.component.scss'],
})
export class EaterManagementComponent implements OnInit {
    @ViewChild(MatPaginator) paginator!: MatPaginator;

    displayedColumnsOfField: string[] = [
        'ten',
        'quanSo',
        'tuNgay',
        'denNgay',
        'trangThai',
        'actions',
    ];

    dataSource = new MatTableDataSource<any>([]);

    applyFilter = new Subject<KeyboardEvent>();
    subscription: Subscription;
    isLoading: boolean = false;
    constructor(
        private router: Router,
        public dialog: MatDialog,
        private _toastrService: ToastrService,
        private commonService: CommonService,
        private _dataService: DataService,
    ) {
        this.subscription = this.applyFilter
            .pipe(
                map((event) => (event.target as HTMLInputElement).value),
                debounceTime(500),
                distinctUntilChanged(),
            )
            .subscribe((filterValue) => this.handleSearch(filterValue));

        this.subscription = this._dataService.getData().subscribe((message) => {
            this.isLoading = true;
            this.getInitData(message.id);
        });
    }

    ngOnInit() {
        this.getInitData(this._dataService.getDauMoiBepId());
    }

    action(element: any) {
        if (element) {
            this.router.navigate([
                'manage/app/category/quan-ly-quan-so-an/edit/' + element.id,
            ]);
        } else {
            this.router.navigate([
                'manage/app/category/quan-ly-quan-so-an/create',
            ]);
        }
    }

    getInitData(id: number) {
        this.commonService
            .callDataAPIShort('/api/services/read/DotQuanSoAn/GetAll', {
                maxResultCount: 999999999,
                criterias: [
                    {
                        propertyName: 'dauMoiBepId',
                        operation: 0,
                        value: id,
                    },
                ],
            })
            .subscribe((response) => {
                if (response.success) {
                    this.dataSource = new MatTableDataSource<any>(
                        response.result.items,
                    );
                    this.dataSource.paginator = this.paginator;

                    setTimeout(() => {
                        this.isLoading = false;
                    }, 200);
                }
            });
    }

    handleSearch(dataInput: any) {
        this.dataSource.filter = dataInput.toLowerCase();
    }

    deleteEaterList(dataDelete: any) {
        let dataValue = dataDelete;
        const dialogRef = this.dialog.open(DeleteConfirmDialogComponent, {
            width: '500px',
            data: dataDelete,
        });

        dialogRef.componentInstance.handleDelete.subscribe((dataDelete) => {
            const isSubmitted = !!dataDelete;
            if (dataValue.trangThai === true) {
                this._toastrService.error(
                    '',
                    'Quân số ăn này còn đang sử dụng',
                );
            } else {
                this.commonService
                    .callDataAPIShort(
                        '/api/services/write/DotQuanSoAn/Delete',
                        dataDelete,
                    )
                    .subscribe(
                        (response) => dialogRef.close(isSubmitted),
                        (err) => this._toastrService.errorServer(err),
                    );
            }
        });

        dialogRef.afterClosed().subscribe((result) => {
            if (!result) return;
            this.getInitData(this._dataService.getDauMoiBepId());
            this._toastrService.success('', 'Xóa thành công');
        });
    }
}
