import { query } from '@angular/animations';
import { SelectionModel } from '@angular/cdk/collections';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatPaginator, MatTableDataSource } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { CategoryService } from '@core/services/category.service';
import { CommonService } from '@core/services/common.service';
import { DataService } from '@core/services/data-service';
import { FoodFuelService } from '@core/services/food-fuel.service';
import { InventoryService } from '@core/services/inventory.service';
import { Subscription } from 'rxjs';
import { DialogInventoryDetailComponent } from './dialog-inventory-detail/dialog-inventory-detail.component';

export const DATA_EXAMPLE = [
    {
        name: 'Gạo tẻ',
        nameMaterial: 'K001',
        unit: 'KG',
        amount: '10',
        amountIn: '100',
        amountOut: '90',
    },
    {
        name: 'Gạo tẻ',
        nameMaterial: 'THỰC PHẨM',
        unit: 'KG',
        amount: '10',
        amountIn: '100',
        amountOut: '90',
    },
    {
        name: 'Gạo tẻ',
        nameMaterial: 'THỰC PHẨM',
        unit: 'KG',
        amount: '10',
        amountIn: '100',
        amountOut: '90',
    },
    {
        name: 'Gạo tẻ',
        nameMaterial: 'THỰC PHẨM',
        unit: 'KG',
        amount: '10',
        amountIn: '100',
        amountOut: '90',
    },
    {
        name: 'Gạo tẻ',
        nameMaterial: 'THỰC PHẨM',
        unit: 'KG',
        amount: '10',
        amountIn: '100',
        amountOut: '90',
    },
];

@Component({
    selector: 'app-store-inventory',
    templateUrl: './store-inventory.component.html',
    styleUrls: ['./store-inventory.component.scss'],
})
export class StoreInventoryComponent implements OnInit {
    displayedColumnsOfField: string[] = [
        'name',
        'nameMaterial',
        'unit',
        'inventory',
        'import',
        'export',
        'amount',
        'price',
        'actions',
    ];

    formSearch: FormGroup;

    fieldSearch: any[] = [
        {
            type: 'TEXT',
            referenceValue: 'tenLttpChatDot',
            name: 'Tìm kiếm theo tên vật tư, hàng hoá',
            defaultValue: '',
            required: '0',
            icon: 'search',
            css: 'col-12 col-lg-6',
            appearance: 'legacy',
        },
        {
            type: 'SELECT',
            referenceValue: 'tenNhomLttpChatDot',
            name: 'Nhóm nguyên vật liệu',
            defaultValue: '',
            options: [
                {
                    key: '',
                    name: '',
                },
            ],
            required: '0',
            css: 'col-12 col-lg-6',
            appearance: 'legacy',
        },
    ];

    nameWarehouse;
    idInventory;

    foodFuelList;
    typeOptions: any[] = [];

    dataSource = new MatTableDataSource<any>([]);
    @ViewChild(MatPaginator) paginator!: MatPaginator;

    allowExec: boolean = false;

    dauMoiBepId: number;
    subscription: Subscription;
    isLoading: boolean = true;

    constructor(
        public dialog: MatDialog,
        private _activatedroute: ActivatedRoute,
        private _inventoryService: InventoryService,
        private _foodFuelService: FoodFuelService,
        private _categoryService: CategoryService,
        private _router: Router,
        private _commonService: CommonService,
        private _dataService: DataService,
    ) {
        this._commonService.checkPermission2().subscribe((response) => {
            if (!response.result) return;

            if (JSON.parse(response.result).some((obj) => obj.key === 'EXEC')) {
                this.allowExec = true;
            }
        });

        this.subscription = this._dataService.getData().subscribe((message) => {
            this.isLoading = true;
            this.getDataOnInit(message.id);
        });
    }

    ngOnInit() {
        this.dauMoiBepId = this._dataService.getDauMoiBepId();

        this._activatedroute.queryParams.subscribe((params) => {
            this.nameWarehouse = params.name;
            this.idInventory = params.id;
            this.getTypeOptions();
        });
        this.initForm();
    }

    onDeleteSearchInfo(): void {
        let valueForm = this.formSearch.value;
        let keys = Object.keys(valueForm); // Get array of keys
        let result = {
            key: 'reset',
        };
        // Iterate all keys
        keys.forEach((key) => {
            result[key] = valueForm[key] = null;
        });

        this.formSearch.reset();

        this.handleSearch(result);
    }

    getFields() {
        return this.fieldSearch;
    }

    getDataOnInit(id: number) {
        this._foodFuelService
            .getAllFoodFuel({ maxResultCount: 9999999 })
            .subscribe((res) => {
                this.foodFuelList = res.result.items;
                let request = {
                    maxResultCount: 9999999,
                    sorting: 'creationTime DESC',
                    criterias: [
                        {
                            propertyName: 'ton',
                            operation: 2,
                            value: 0,
                        },
                    ],
                    dauMoiBepId: id,
                };

                this._inventoryService
                    .getAllInventory(request)
                    .subscribe((response) => {
                        this.dataSource = new MatTableDataSource<any>(
                            response.result.items,
                        );
                        this.dataSource.paginator = this.paginator;

                        this.fieldSearch = [
                            {
                                type: 'TEXT',
                                referenceValue: 'tenLttpChatDot',
                                name: 'Tìm kiếm theo tên vật tư, hàng hoá',
                                defaultValue: '',
                                required: '0',
                                icon: 'search',
                                css: 'col-12 col-lg-6',
                                appearance: 'legacy',
                            },
                            {
                                type: 'SELECT',
                                referenceValue: 'tenNhomLttpChatDot',
                                name: 'Nhóm nguyên vật liệu',
                                defaultValue: this.typeOptions[0],
                                options: [...this.typeOptions],
                                required: '0',
                                css: 'col-12 col-lg-6',
                                appearance: 'legacy',
                                search: '1',
                                searchCtrl: 'searchCtrl',
                            },
                        ];

                        setTimeout(() => {
                            this.isLoading = false;
                        }, 200);
                    });
            });
    }

    initForm() {
        let fieldsCtrls = {};
        let fieldList = [];

        for (let f of this.fieldSearch) {
            fieldList = [...fieldList, f];

            if (f.type === 'NUMBER') {
                fieldsCtrls[f.referenceValue] = new FormControl(
                    { value: f.defaultValue, disabled: f.disabled },
                    f.required === '1' ? [Validators.required] : [],
                );
            } else if (f.type !== 'CHECKBOX') {
                let validators = [
                    Validators.minLength(f.minLength),
                    Validators.maxLength(f.maxLength),
                    Validators.pattern(f.pattern),
                ];

                if (f.type === 'EMAIL') {
                    validators = [...validators, Validators.email];
                }

                fieldsCtrls[f.referenceValue] = new FormControl(
                    {
                        value: f.defaultValue || '',
                        disabled: f.disabled,
                    },

                    f.required === '1'
                        ? [...validators, Validators.required]
                        : [...validators],
                );

                if (f.type === 'DATETIME') {
                    fieldsCtrls[f.referenceValue].valueChanges.subscribe(
                        (val) => {
                            const dependField = fieldList.find(
                                (x) => x.dependenField === f.referenceValue,
                            );

                            if (!dependField) return;

                            dependField.min = val;
                            fieldsCtrls[dependField.referenceValue].enable();
                        },
                    );
                }
            } else {
                //if checkbox, it need multiple
                let opts = {};
                for (let opt of f.options) {
                    opts[opt.key] = new FormControl({
                        value: opt.value,
                        disabled: f.disabled,
                    });
                }
                fieldsCtrls[f.referenceValue] = new FormGroup(opts);
            }
        }

        this.formSearch = new FormGroup(fieldsCtrls);
    }

    getTypeOptions() {
        let request = {
            maxResultCount: 999999,
            skipCount: 0,
            criterias: [
                {
                    propertyName: 'status',
                    operation: 0,
                    value: 'ENABLE',
                },
            ],
        };
        this._commonService
            .callDataAPIShort(
                '/api/services/read/NhomLttpChatDot/GetAll',
                request,
            )
            .subscribe((res) => {
                this.typeOptions = res.result.items.map((item) => ({
                    key: item.tenNhomLttpChatDot,
                    name: item.tenNhomLttpChatDot,
                }));

                setTimeout(
                    () =>
                        this.getDataOnInit(this._dataService.getDauMoiBepId()),
                    1000,
                );
            });
    }

    getDataFromId(id, objKey) {
        let dataInit = '';

        if (objKey === 'phanLoai') {
            if (this.foodFuelList.length > 0) {
                const dataFound = this.foodFuelList.find(
                    (item) => item.id === id,
                );

                let codeData = dataFound[objKey];

                const dataFound2 = this.typeOptions.find(
                    (item) => item.key === codeData,
                );
                dataInit = dataFound2.name;
            }

            return dataInit;
        } else {
            if (this.foodFuelList.length > 0) {
                const dataFound = this.foodFuelList.find(
                    (item) => item.id === id,
                );

                dataInit = dataFound[objKey];
            }

            return dataInit;
        }
    }

    navigate() {
        this._router.navigate([`app/kho/ton-kho-dau-ky/${this.idInventory}`], {
            queryParams: {
                nameInventory: this.nameWarehouse,
                idInventory: this.idInventory,
            },
        });
    }

    showDetail(element) {
        let dialogRef = this.dialog.open(DialogInventoryDetailComponent, {
            width: '80%',
            data: {
                name: element.tenLttpChatDot,
                id: element.id,
                khoId: element.khoId,
                donGia: element.donGia,
                donViTinh: element.donViTinh,
            },
            panelClass: 'my-custom-dialog-class',
        });
    }

    handleSearch(dataSearch: any) {
        if (dataSearch.key === 'reset') {
            this.getDataOnInit(this._dataService.getDauMoiBepId());
        } else {
            let bodySearch = {
                criterias: [
                    {
                        propertyName: 'ton',
                        operation: 2,
                        value: 0,
                    },
                ],
                tenLttpChatDot: dataSearch.tenLttpChatDot
                    ? dataSearch.tenLttpChatDot
                    : '',
                tenNhomLttpChatDot: dataSearch.tenNhomLttpChatDot
                    ? dataSearch.tenNhomLttpChatDot
                    : '',
                dauMoiBepId: this.dauMoiBepId,
            };

            this._inventoryService
                .getAllInventory(bodySearch)
                .subscribe((response) => {
                    this.dataSource = new MatTableDataSource<any>(
                        response.result.items,
                    );
                    this.dataSource.paginator = this.paginator;
                });
        }
    }
}
