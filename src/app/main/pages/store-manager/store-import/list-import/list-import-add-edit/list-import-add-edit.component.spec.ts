import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListImportAddEditComponent } from './list-import-add-edit.component';

describe('ListImportAddEditComponent', () => {
  let component: ListImportAddEditComponent;
  let fixture: ComponentFixture<ListImportAddEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListImportAddEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListImportAddEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
