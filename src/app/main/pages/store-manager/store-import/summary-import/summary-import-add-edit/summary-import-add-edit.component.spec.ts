import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SummaryImportAddEditComponent } from './summary-import-add-edit.component';

describe('SummaryImportAddEditComponent', () => {
  let component: SummaryImportAddEditComponent;
  let fixture: ComponentFixture<SummaryImportAddEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SummaryImportAddEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SummaryImportAddEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
