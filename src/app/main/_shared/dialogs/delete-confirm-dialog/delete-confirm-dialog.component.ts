import { Component, EventEmitter, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
    selector: 'app-delete-confirm-dialog',
    templateUrl: './delete-confirm-dialog.component.html',
    styleUrls: ['./delete-confirm-dialog.component.scss'],
})
export class DeleteConfirmDialogComponent implements OnInit {
    handleDelete = new EventEmitter();

    constructor(
        public dialogRef: MatDialogRef<DeleteConfirmDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any,
    ) {}

    ngOnInit() {}

    onCloseDialog(data?: any): void {
        this.dialogRef.close(data);
    }

    onConfirmDelete(): void {
        const dataDelete = this.data
            ? {
                  id: this.data.id,
              }
            : null;

        this.handleDelete.emit(dataDelete);
    }
}
