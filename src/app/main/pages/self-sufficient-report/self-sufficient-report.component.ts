import { Component, OnInit } from '@angular/core';
import {
    animate,
    state,
    style,
    transition,
    trigger,
} from '@angular/animations';
import { resolve } from 'url';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, pairwise, startWith } from 'rxjs/operators';
import {
    DateAdapter,
    MatDatepicker,
    MatDatepickerInputEvent,
    MatTableDataSource,
    MAT_DATE_FORMATS,
    MAT_DATE_LOCALE,
} from '@angular/material';
import {
    MAT_MOMENT_DATE_ADAPTER_OPTIONS,
    MomentDateAdapter,
} from '@angular/material-moment-adapter';
import { MY_FORMATS } from '../inventory-import/import-add-edit/import-add-edit.component';
import { type } from 'os';
import { ExportExcelService } from '@core/services/export-excel.service';
import { CommonService } from '@core/services/common.service';
import { HttpErrorResponse } from '@angular/common/http';
import { ClassGetter } from '@angular/compiler/src/output/output_ast';
import { DataService } from '@core/services/data-service';
import { Moment } from 'moment';

export const MONTH_MODE_FORMATS = {
    parse: {
        dateInput: 'MM/YYYY',
    },
    display: {
        dateInput: 'MM/YYYY',
        monthYearLabel: 'MMM YYYY',
        dateA11yLabel: 'LL',
        monthYearA11yLabel: 'MMMM YYYY',
    },
};

@Component({
    selector: 'app-self-sufficient-report',
    templateUrl: './self-sufficient-report.component.html',
    styleUrls: ['./self-sufficient-report.component.scss'],
    animations: [
        trigger('detailExpand', [
            state('collapsed', style({ height: '0px', minHeight: '0' })),
            state('expanded', style({ height: '*' })),
            transition(
                'expanded <=> collapsed',
                animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)'),
            ),
        ]),
    ],
    providers: [
        {
            provide: DateAdapter,
            useClass: MomentDateAdapter,
            deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS],
        },

        { provide: MAT_DATE_LOCALE, useValue: 'vi-VI' },
        { provide: MAT_DATE_FORMATS, useValue: MONTH_MODE_FORMATS },
        {
            provide: MAT_MOMENT_DATE_ADAPTER_OPTIONS,
            useValue: { useUtc: true },
        },
    ],
})
export class SelfSufficientReportComponent implements OnInit {
    dauMoiBepId: number;
    columnsToDisplay = ['name', 'weight', 'symbol', 'position', 'description'];
    dataReport = new MatTableDataSource<any>([]);
    dataReportExportExcel;

    rfSearch: FormGroup;

    fileName: string;

    // headerTable: string[] = [
    //     'tenLTTP',
    //     'dvt',
    //     'donGia',
    //     'soLuong',
    //     'thanhTien',
    //     'trenDBTT',
    //     'trenDBTG',
    //     'donViTT',
    //     'donViTg',
    //     'nhapTrongNgayThanhTien',
    //     'SoLuongCN',
    //     'ThanhTienCN',
    // ];

    // headerTable: string[] = [
    //     'tenLttpChatDot',
    //     'donViTinh',
    //     'soLuongThangTruoc',
    //     'nhapTrenBdMuaTT',
    //     'nhapTrenBdTGCB',
    //     'nhapDonViBdMuaTT',
    //     'nhapDonViBdTGCB',
    //     'soLuongCongNhap',
    //     'soLuongXuatAn',
    //     'soLuongXuatKhac',
    //     'soLuongCongXuat',
    //     'chuyenSangThangSau',
    // ];

    headerTable: string[] = [
        'index',
        'tenLttpChatDot',
        'donViTinh',
        'soLuong',
        'soLuongTGSX',
        'soLuongMuaTT',
        'tiLeTuTuc',
        'donGiaTGSX',
        'donGiaMuaTT',
        'thanhTienTGSX',
        'thanhTienMuaTT',
        'tienChenhLech',
    ];

    pickerMonth = new FormControl(new Date(), Validators.required);
    filteredOptions: Observable<string[]>;
    minDate;

    constructor(
        private fb: FormBuilder,
        private exportExcelService: ExportExcelService,
        private commonService: CommonService,
        private dataService: DataService,
    ) { }

    ngOnInit() {
        this.dauMoiBepId = this.dataService.getDauMoiBepId();
        this.getInitData(this.dauMoiBepId);
        this.getInitForm();

        // this.onRfSearchChanges();
    }

    getInitData(dauMoiBepId: number) {
        this.fileName = `${new Date(this.pickerMonth.value).getMonth() + 1}`;
        this.commonService
            .callDataAPIShort(
                '/api/services/read/BaoCao/GetBaoCaoTuTucThucPhamThang',
                {
                    dauMoiBepId,
                    thang: new Date(this.pickerMonth.value).getMonth() + 1,
                    nam: new Date(this.pickerMonth.value).getFullYear(),
                },
            )
            .subscribe(
                (data) => {
                    this.dataReport.data = data.result.map((item, index) => ({
                        index: index + 1,
                        ...item,
                    }));
                },
                (err: HttpErrorResponse) => {
                    console.log(err);
                    alert(err.message);
                },
            );
    }

    // getMindate() {
    //     let fromDay = this.rfSearch.get('fromDay').value;
    //     this.minDate = fromDay;
    // }

    addEvent(type: string, event: MatDatepickerInputEvent<Date>) {
        console.log(event);
    }

    isCheckValue = (value) => !value;

    onRfSearchChanges() {
        this.rfSearch
            .get('fromDay')
            .valueChanges.pipe(startWith(null as string), pairwise())
            .subscribe(([prev, next]: [any, any]) => {
                if (!this.isCheckValue(next)) {
                    this.rfSearch.get('toDay').enable();
                    this.minDate = new Date(next);
                }
            });
    }

    getInitForm() {
        this.rfSearch = this.fb.group({
            typeTime: '',
            unit: '',
            time: '',
            fromDay: '',
            toDay: [{ value: '', disabled: true }],
        });
    }

    onSubmit() {
        this.getInitData(this.dauMoiBepId);
    }
    resetForm() {
        this.rfSearch.reset();
        this.pickerMonth.reset();
    }

    exportExcel() {
        const thoiGian = new Date(this.pickerMonth.value);

        let bodyDataReportExcel = {
            chiTiet: this.dataReport.data,
            thoiGian,
            // thanhTienNgayTruoc: this.thanhTienNgayTruoc,
            // thanhTienCongNhap: this.thanhTienCongNhap,
            // thanhTienCongXuat: this.thanhTienCongXuat,
        };

        const tenBaoCao = 'Tự túc thực phẩm tháng';
        const _thang = ("0" + (thoiGian.getMonth() + 1)).slice(-2);
        const _nam = thoiGian.getFullYear().toString();

        this.exportExcelService.generateSelfSufficientReportExcel(
            bodyDataReportExcel,
            `${tenBaoCao}`,
        );
    }

    _monthSelectedHandler(
        chosenMonthDate: Moment,
        datepicker: MatDatepicker<Moment>,
    ) {
        datepicker.close();
        this.pickerMonth.setValue(chosenMonthDate);
    }

    renderToText(string: string) {
        let result = '';
        switch (string) {
            case 'tenLttpChatDot':
                result = 'Tên thực phẩm chất đốt';
                break;
            case 'donViTinh':
                result = 'ĐVT';
                break;
            case 'soLuong':
                result = 'Số lượng';
                break;
        }
        return result;
    }
}
