import { SelectionModel } from '@angular/cdk/collections';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatPaginator, MatTableDataSource } from '@angular/material';
import { Router } from '@angular/router';
import { DeleteConfirmDialogComponent } from '@app/_shared/dialogs/delete-confirm-dialog/delete-confirm-dialog.component';
import { CommonService } from '@core/services/common.service';
import { DataService } from '@core/services/data-service';
import { MenuListService } from '@core/services/menu-list.service';
import { ToastrService } from '@core/services/toastr.service';
import { Subject, Subscription } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';
export const DATA = [
    {
        ma: 'TĐ001',
        thucDonTuan: 'Tuần 3',
        doiTuong: 'Học viên',
        ngayBatDau: '22/08/2022',
        ngayKetThuc: '28/8/2022',
        trangThai: 'Hoạt động',
    },
];
@Component({
    selector: 'app-menu-list-management',
    templateUrl: './menu-list-management.component.html',
    styleUrls: ['./menu-list-management.component.scss'],
})
export class MenuListManagementComponent implements OnInit {
    @ViewChild(MatPaginator) paginator!: MatPaginator;

    currentDate: Date = new Date();

    displayedColumnsOfField: string[] = [
        'ma',
        'thucDonTuan',
        'ngayBatDau',
        'ngayKetThuc',
        'actions',
    ];

    dataSource = new MatTableDataSource<any>([]);

    applyFilter = new Subject<KeyboardEvent>();
    subscription: Subscription;

    allowExec: boolean = false;
    isLoading: boolean = true;

    constructor(
        private _router: Router,
        public dialog: MatDialog,
        private _toastrService: ToastrService,
        private menuListService: MenuListService,
        private _commonService: CommonService,
        private _dataService: DataService,
    ) {
        this._commonService.checkPermission2().subscribe((response) => {
            if (!response.result) return;

            const permissions = JSON.parse(response.result);
            console.log('permissions:: ', permissions);
            this.allowExec = permissions.some((obj) => obj.key === 'EXEC');
        });

        this.subscription = this.applyFilter
            .pipe(
                map((event) => (event.target as HTMLInputElement).value),
                debounceTime(500),
                distinctUntilChanged(),
            )
            .subscribe((filterValue) => this.handleSearch(filterValue));

        this.subscription = this._dataService.getData().subscribe((message) => {
            this.isLoading = true;
            this.getInitData(message.id);
        });
    }

    ngOnInit() {
        this.getInitData(this._dataService.getDauMoiBepId());
    }

    getInitData(id: any) {
        this.menuListService
            .getAllMenuList({
                maxResultCount: 9999999,
                criterias: [
                    {
                        propertyName: 'dauMoiBepId',
                        operation: 0,
                        value: id,
                    },
                ],
            })
            .subscribe((response) => {
                if (response.success) {
                    this.dataSource = new MatTableDataSource<any>(
                        response.result.items,
                    );
                    this.dataSource.paginator = this.paginator;
                    setTimeout(() => {
                        this.isLoading = false;
                    }, 200);
                }
            });
    }

    action(data: any) {
        if (data) {
            this._router.navigate([
                `manage/app/category/danh-sach-thuc-don/edit/${data.id}`,
            ]);
        } else {
            this._router.navigate([
                'manage/app/category/danh-sach-thuc-don/create',
            ]);
        }
    }

    viewDetails(data: any): void {
        this._router.navigate([
            `manage/app/category/danh-sach-thuc-don/edit/${data.id}`,
            { type: 'VIEW' },
        ]);
    }

    handleSearch(dataInput: any) {
        this.dataSource.filter = dataInput.toLowerCase();
    }

    deleteMenuList(data: any) {
        const dialogRef = this.dialog.open(DeleteConfirmDialogComponent, {
            width: '500px',
            data: data,
        });

        dialogRef.componentInstance.handleDelete.subscribe((dataDelete) => {
            const isSubmitted = !!dataDelete;

            this.menuListService.deleteMenuList(dataDelete).subscribe(
                (response) => dialogRef.close(isSubmitted),
                (err) => this._toastrService.errorServer(err),
            );
        });

        dialogRef.afterClosed().subscribe((result) => {
            if (!result) return;

            this.getInitData(this._dataService.getDauMoiBepId());
            this._toastrService.success('', 'Xóa thành công');
        });
    }
}
