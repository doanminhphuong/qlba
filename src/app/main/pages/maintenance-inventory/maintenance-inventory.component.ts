import {
    AfterViewInit,
    Component,
    OnDestroy,
    OnInit,
    ViewChild,
} from '@angular/core';
import { MatDialog, MatPaginator, MatTableDataSource } from '@angular/material';
import { Router } from '@angular/router';
import { CommonService } from '@core/services/common.service';
import { DataService } from '@core/services/data-service';
import { forkJoin, Subscription } from 'rxjs';
import { InventoryDetailsDialogComponent } from './inventory-details-dialog/inventory-details-dialog.component';

@Component({
    selector: 'app-maintenance-inventory',
    templateUrl: './maintenance-inventory.component.html',
    styleUrls: ['./maintenance-inventory.component.scss'],
})
export class MaintenanceInventoryComponent
    implements OnInit, AfterViewInit, OnDestroy {
    @ViewChild(MatPaginator) paginator!: MatPaginator;
    subscription: Subscription;

    // Table
    displayedColumnsOfField: string[] = [
        'maDungCuCapDuong',
        'tenDungCuCapDuong',
        'donViTinh',
        'phanHang',
        'tonDauKy',
        'ton',
        'tongNhap',
        'actions',
    ];
    dataSource = new MatTableDataSource<any>([]);

    // Init
    dauMoiBepId: number;
    khoNhanId: number;

    // Boolean
    allowExec: boolean = false;
    isLoading: boolean = true;

    constructor(
        public dialog: MatDialog,
        private _router: Router,
        private _dataService: DataService,
        private _commonService: CommonService,
    ) {
        this._commonService.checkPermission2().subscribe((response) => {
            if (!response.result) return;

            if (JSON.parse(response.result).some((obj) => obj.key === 'EXEC')) {
                this.allowExec = true;
            }
        });

        // This event is excecuted when Bep changes
        this.subscription = this._dataService.getData().subscribe((message) => {
            this.isLoading = true;
            this.getInitData(message.id);
        });
    }

    ngOnInit() {
        this.dauMoiBepId = this._dataService.getDauMoiBepId();
        this.getInitData(this.dauMoiBepId);
    }

    ngAfterViewInit(): void { }

    ngOnDestroy(): void {
        this.subscription.unsubscribe();
    }

    getInitData(dauMoiBepId: number): void {
        this.dauMoiBepId = dauMoiBepId;

        const dataKhoDccd = this._commonService.callDataAPIShort(
            '/api/services/read/KhoDccd/GetAll',
            {
                maxResultCount: 9999,
                // sorting: 'Id',
                criterias: [
                    {
                        propertyName: 'dauMoiBepId',
                        operation: 0,
                        value: dauMoiBepId,
                    },
                ],
            },
        );

        forkJoin([dataKhoDccd]).subscribe((results) => {
            const currentKhoDccd = results[0].result.items[0];
            this.khoNhanId = currentKhoDccd['id'];

            this.getMaintenaceInventory();
        });
    }

    getMaintenaceInventory(): void {
        this._commonService
            .callDataAPIShort('/api/services/read/TonKhoDccd/GetAll', {
                maxResultCount: 9999,
                // sorting: 'Id',
                criterias: [
                    {
                        propertyName: 'khoId',
                        operation: 0,
                        value: this.khoNhanId,
                    },
                ],
            })
            .subscribe((res) => {
                this.dataSource.data = res.result.items;
                this.dataSource.paginator = this.paginator;
                setTimeout(() => (this.isLoading = false), 200);
            });
    }

    navigateToImport() {
        this._router.navigate([`manage/app/category/nhap-kho-dccd`]);
    }

    navigateToBefore() {
        this._router.navigate([`manage/app/category/ton-kho-dccd-dau-ky`]);
    }

    showDetail(element) {
        let dialogRef = this.dialog.open(InventoryDetailsDialogComponent, {
            width: '80%',
            data: {
                id: element.id,
                name: element.tenDccd,
                khoId: element.khoId,
                phanHang: element.phanHang,
                donViTinh: element.donViTinh,
                allowExec: this.allowExec,
            },
            panelClass: 'my-custom-dialog-class',
        });
    }

    handleInputFilter(filterValue: string) {
        const tenVatTu = filterValue.trim().toLowerCase();

        if (!tenVatTu) {
            this.getInitData(this.dauMoiBepId);
        } else {
            this._commonService
                .callDataAPIShort('/api/services/read/TonKhoDccd/GetAll', {
                    maxResultCount: 9999,
                    criterias: [
                        {
                            propertyName: 'khoId',
                            operation: 0,
                            value: this.khoNhanId,
                        },
                    ],
                    tenVatTu,
                })
                .subscribe((res) => {
                    this.dataSource.data = res.result.items;
                });
        }
    }
}
