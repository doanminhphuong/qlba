import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
    selector: 'fb-file',
    template: `
        <div [formGroup]="form">
            <ng-container
                *ngIf="!isFlatForm && tabCtrl"
                [formGroupName]="tabCtrl"
            >
                <ng-container [formGroupName]="groupCtrl">
                    <mat-form-field
                        [appearance]="field.appearance || 'outline'"
                    >
                        <mat-label>{{ field.name }}</mat-label>
                        <input
                            matInput
                            type="text"
                            [placeholder]="getPlaceholder()"
                            [value]="field.defaultValue"
                            [id]="field.referenceValue"
                            [name]="field.referenceValue"
                            [formControlName]="getFormControlName()"
                            [required]="field.required === '1' ? true : false"
                            spellcheck="false"
                            [pattern]="field.pattern"
                            [maxLength]="
                                field.maxLength ? field.maxLength : 999
                            "
                        />
                        <mat-icon matSuffix>{{ field.icon }}</mat-icon>
                        <!-- VALIDATORS -->
                        <mat-error>{{ getErrorMessage() }}</mat-error>
                    </mat-form-field>
                </ng-container>
            </ng-container>

            <mat-form-field
                *ngIf="isFlatForm || !tabCtrl"
                [appearance]="field.appearance || 'outline'"
            >
                <mat-label>{{ field.name }}</mat-label>
                <input
                    matInput
                    type="text"
                    [placeholder]="getPlaceholder()"
                    [value]="field.defaultValue"
                    [id]="field.referenceValue"
                    [name]="field.referenceValue"
                    [formControlName]="getFormControlName()"
                    [required]="field.required === '1' ? true : false"
                    spellcheck="false"
                    [readonly]="true"
                    [pattern]="field.pattern"
                    [maxLength]="field.maxLength ? field.maxLength : 999"
                />
                <input
                    type="file"
                    hidden
                    #f_input
                    (change)="handleFileInputChange($event)"
                />
                <button
                    class="file-btn"
                    matSuffix
                    (click)="f_input.click()"
                >
                    <mat-icon>attach_file</mat-icon>
                </button>
                <!-- VALIDATORS -->
                <mat-error>{{ getErrorMessage() }}</mat-error>
            </mat-form-field>

            <div class="image__block">
                <img [src]="imageUrl || defaultImage" alt="" width="200">
                <button
                    *ngIf="imageUrl"
                    class="clear-img-btn"
                    (click)="handleRemoveImage()"
                >
                    x
                </button>
            </div>
        </div>
    `,
    styles: [
        `
            [hidden] {
                display: none !important;
            }

            mat-form-field {
                width: 100%;
            }

            .image__block {
                display: flex;
                align-items: center;
                justify-content: center;
            }

            .file-btn {
                background-color: #66a2d5;
                border-radius: 50px;
                border: none;
                color: #fff;
                box-shadow: 1px 2px 2px rgba(0, 0, 0, 0.2);
            }

            .file-btn:hover {
                background-color: #3279b6;

            }

            .clear-img-btn {
                display: flex;
                align-items: center;
                justify-content: center;
                border-radius: 50%;
                border: none;
                font-size: 16px;
                font-weight: bold;
                color: #fff;
                background-color: #e17b82;
                box-shadow: 1px 2px 2px rgba(0, 0, 0, 0.2);
                margin-left: 14px;
            }

            .clear-img-btn:hover {
                background-color: #e83d48;
            }
        `,
    ],
})
export class FileComponent implements OnInit {
    @Input() field: any = {};
    @Input() form: FormGroup;
    @Input() tabCtrl: any;
    @Input() groupCtrl: any;
    @Input() isFlatForm: boolean;

    imageUrl: any;
    defaultImage: string = "/assets/images/no-picture.png";

    get fieldName() {
        if (!this.isFlatForm && this.tabCtrl) {
            return this.form
                .get(this.tabCtrl)
                .get(this.groupCtrl)
                .get(this.field.referenceValue);
        } else if (this.isFlatForm && this.tabCtrl) {
            return this.form.controls[
                `${this.tabCtrl}.${this.groupCtrl}.${this.field.referenceValue}`
            ];
        } else {
            return this.form.controls[this.field.referenceValue];
        }
    }

    get isValid() {
        return this.fieldName.valid;
    }
    get isDirty() {
        return this.fieldName.dirty;
    }
    get isTouched() {
        return this.fieldName.touched;
    }

    constructor(
        private sanitizer: DomSanitizer
    ) { }

    ngOnInit() { }

    getSantizeUrl(url: string) {
        return this.sanitizer.bypassSecurityTrustUrl(url);
    }

    handleFileInputChange(event: any) {
        const file = event.target.files[0];
        console.log("file:: ", file);

        this.fieldName.patchValue(file.name);
        file.preview = URL.createObjectURL(file);
        this.imageUrl = this.getSantizeUrl(file.preview);

        event.target.value = null;
    }

    handleRemoveImage(): void {
        this.imageUrl = null;
        this.fieldName.reset();
    }

    getPlaceholder() {
        return `Nhập vào ${this.field.name.toLowerCase()}`;
    }

    getFormControlName() {
        if (this.isFlatForm) {
            return `${this.tabCtrl}.${this.groupCtrl}.${this.field.referenceValue}`;
        } else {
            return this.field.referenceValue;
        }
    }

    getErrorMessage() {
        let errorMessage = '';

        if (this.fieldName.hasError('required')) {
            errorMessage = `${this.field.name} không được để trống.`;
        }

        return errorMessage;
    }
}
