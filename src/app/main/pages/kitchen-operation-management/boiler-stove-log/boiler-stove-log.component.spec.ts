import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BoilerStoveLogComponent } from './boiler-stove-log.component';

describe('BoilerStoveLogComponent', () => {
  let component: BoilerStoveLogComponent;
  let fixture: ComponentFixture<BoilerStoveLogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BoilerStoveLogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BoilerStoveLogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
