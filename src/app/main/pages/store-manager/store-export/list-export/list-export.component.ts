import { SelectionModel } from '@angular/cdk/collections';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatPaginator, MatTableDataSource } from '@angular/material';
import { Router } from '@angular/router';
import { DynamicDialogComponent } from '@app/components/dynamic-dialog-component/dynamic-dialog-component.component';
import { DeleteConfirmDialogComponent } from '@app/_shared/dialogs/delete-confirm-dialog/delete-confirm-dialog.component';
import { CategoryService } from '@core/services/category.service';
import { ToastrService } from '@core/services/toastr.service';
import { MaterialDialogComponent } from '@app/_shared/dialogs/material-dialog/material-dialog.component';
import { PriceService } from '@core/services/price.service';
import { UserService } from '@core/services/user.service';
import { forkJoin, Observable, Subject, Subscription } from 'rxjs';
import { PhieuXuatKhoService } from '@core/services/phieu-xuat-kho.service';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';
import { DataService } from '@core/services/data-service';

@Component({
    selector: 'app-list-export',
    templateUrl: './list-export.component.html',
    styleUrls: ['./list-export.component.scss'],
})
export class ListExportComponent implements OnInit {
    @ViewChild(MatPaginator) paginator!: MatPaginator;

    currentDate: Date = new Date();

    displayedColumnsOfField: string[] = [
        'ngayXuatKho',
        'maPhieuXuatKho',
        'thanhTien',
        'maNguoiDuyet',
        'actions',
    ];

    dataSource = new MatTableDataSource<any>([]);
    selection = new SelectionModel<any>(true, []);
    userList: any[] = [];
    exportWarehouseList: any[] = [];

    inventoryList: any[] = [];
    totalPrice: number = 0;
    currentTable: any[] = [];
    unitOptions: any[] = [];
    typeOptions: any[] = [];

    fieldSearch: any[] = [
        {
            type: 'DATETIME',
            referenceValue: 'ngayXuatKho',
            name: 'Ngày xuất',
            defaultValue: '',
            required: '0',
            css: 'col-12 col-lg-6',
            appearance: 'legacy',
        },
        {
            type: 'SELECT',
            referenceValue: 'trangThai',
            name: 'Trạng thái',
            defaultValue: '',
            required: '0',
            css: 'col-12 col-lg-6',
            options: [
                { key: 0, name: 'Lưu nháp' },
                { key: 3, name: 'Xuất kho' },
            ],
            appearance: 'legacy',
        },
    ];

    applyFilter = new Subject<KeyboardEvent>();
    subscription: Subscription;
    isLoading: boolean = true;

    constructor(
        private _router: Router,
        public dialog: MatDialog,
        private _toastrService: ToastrService,
        private _categoryService: CategoryService,
        private _priceService: PriceService,
        private _userService: UserService,
        private _phieuXuatKhoService: PhieuXuatKhoService,
        private _dataService: DataService,
    ) {
        this.subscription = this.applyFilter
            .pipe(
                map((event) => (event.target as HTMLInputElement).value),
                debounceTime(500),
                distinctUntilChanged(),
            )
            .subscribe((filterValue) => this.handleSearchName(filterValue));

        this.subscription = this._dataService.getData().subscribe((message) => {
            this.isLoading = true;
            this.getInitData(message.id);
        });
    }

    ngOnInit() {
        this.getMultiCategories();
        setTimeout(() => {
            this.getInitData(this._dataService.getDauMoiBepId());
        }, 1000);
    }

    getInitData(id: number): void {
        const dataUser = this._userService.getPageUsers({
            maxResultCount: 2147483647,
        });
        const dataExportInventory =
            this._phieuXuatKhoService.getAllPhieuXuatKhoOrigin({
                maxResultCount: 2147483647,
                dauMoiBepId: id,
            });

        forkJoin([dataUser, dataExportInventory]).subscribe((results) => {
            this.userList = results[0].result.items.map((item) => ({
                key: item.id,
                name: item.surname + item.name,
            }));

            this.exportWarehouseList = results[1].result.items;
            this.dataSource.data = this.exportWarehouseList;

            setTimeout(() => {
                this.isLoading = false;
            }, 200);
        });
    }

    ngAfterViewInit() {
        this.dataSource.paginator = this.paginator;
        this.paginator._intl.itemsPerPageLabel = 'Items/Page:';
    }

    getMultiCategories() {
        this.getUnitOptions();
        this.getTypeOptions();
    }

    getBodyCategory(groupCode: string) {
        return {
            maxResultCount: 10,
            skipCount: 0,
            sorting: 'Code',
            criterias: [
                {
                    propertyName: 'GroupCode',
                    operation: 6,
                    value: groupCode,
                },
            ],
        };
    }

    getUnitOptions() {
        this._categoryService
            .getAllCategory(this.getBodyCategory('don-vi-tinh'))
            .subscribe((res) => {
                this.unitOptions = res.result.map((item) => ({
                    key: item.codeData,
                    name: item.name,
                }));
            });
    }

    getTypeOptions() {
        this._categoryService
            .getAllCategory(this.getBodyCategory('phan-loai-lttp-chat-dot'))
            .subscribe((res) => {
                this.typeOptions = res.result.map((item) => ({
                    key: item.codeData,
                    name: item.name,
                }));
            });
    }

    getUserName(id) {
        let result = '';

        if (this.userList.length > 0) {
            const dataFound = this.userList.find((user) => user.key === id);

            result = dataFound ? `${dataFound.name}` : '';
        }

        return result;
    }

    /** Whether the number of selected elements matches the total number of rows. */
    isAllSelected() {
        const numSelected = this.selection.selected.length;
        const numRows = this.dataSource.data.length;
        return numSelected === numRows;
    }

    /** Selects all rows if they are not all selected; otherwise clear selection. */
    toggleAllRows() {
        if (this.isAllSelected()) {
            this.selection.clear();
            return;
        }

        this.selection.select(...this.dataSource.data);
    }

    /** The label for the checkbox on the passed row */
    checkboxLabel(row?: any): string {
        if (!row) {
            return `${this.isAllSelected() ? 'deselect' : 'select'} all`;
        }
        return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${
            row.index + 1
        }`;
    }

    guid = () => {
        let s4 = () => {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        };
        //return id of format 'aaaaaaaa'-'aaaa'-'aaaa'-'aaaa'-'aaaaaaaaaaaa'
        return (
            s4() +
            s4() +
            '-' +
            s4() +
            '-' +
            s4() +
            '-' +
            s4() +
            '-' +
            s4() +
            s4() +
            s4()
        );
    };

    openDialog(dataDialog: any): void {
        let fields = [
            {
                type: 'TEXT',
                referenceValue: 'code',
                name: 'Mã VT, HH',
                defaultValue: '',
                required: '1',
                pattern: '^[a-zA-Z0-9-]*$',
                icon: 'code',
                css: 'col-12 col-lg-6',
            },
            {
                type: 'TEXT',
                referenceValue: 'name',
                name: 'Tên VT, HH',
                defaultValue: '',
                required: '1',
                icon: 'restaurant',
                css: 'col-12 col-lg-6',
            },
            {
                type: 'SELECT',
                referenceValue: 'type',
                name: 'Phân loại nguyên vật liệu',
                defaultValue: '',
                required: '0',
                options: [...this.typeOptions],
                css: 'col-12 col-lg-6',
            },
            {
                type: 'SELECT',
                referenceValue: 'unit',
                name: 'Đơn vị tính',
                defaultValue: '',
                required: '0',
                options: [...this.unitOptions],
                css: 'col-12 col-lg-6',
            },
            {
                type: 'NUMBER',
                referenceValue: 'amount',
                name: 'Số lượng',
                defaultValue: '',
                required: '1',
                icon: 'filter_vintage',
                css: 'col-12 col-lg-6',
            },
            {
                type: 'NUMBER',
                referenceValue: 'price',
                name: 'Đơn giá',
                defaultValue: '',
                required: '1',
                icon: 'filter_vintage',
                css: 'col-12 col-lg-6',
            },
        ];

        let dialogRef = this.dialog.open(DynamicDialogComponent, {
            // disableClose: true,
            width: '70%',
            data: {
                datas: dataDialog,
                fields: fields,
            },
            panelClass: 'my-custom-dialog-class',
        });

        dialogRef.componentInstance.onSave.subscribe((formValues) => {
            const isSubmitted = !!formValues;

            let dataSubmit = {
                ...dataDialog,
                ...formValues,
            };

            if (!dataDialog) {
                dataSubmit.id = this.guid();
                dataSubmit.creationTime = this.currentDate;

                this.dataSource.data = [...this.dataSource.data, dataSubmit];
            } else {
                const dataIndex = this.dataSource.data.findIndex(
                    (item) => item.id === dataDialog.id,
                );

                const currentData = [...this.dataSource.data];
                currentData.splice(dataIndex, 1, dataSubmit);
                this.dataSource.data = currentData;
                this.getTotalPrice();
            }

            dialogRef.close(isSubmitted);
        });

        dialogRef.componentInstance.handleDelete.subscribe((dataDelete) => {
            const isDeleted = !!dataDelete;
        });

        dialogRef.afterClosed().subscribe((result) => {
            if (!result) return;

            if (!dataDialog) {
                this._toastrService.success('', 'Thêm thành công');
            } else {
                this._toastrService.success('', 'Cập nhật thành công');
            }
        });
    }

    getTotalPrice() {
        this.dataSource.data.forEach((item) => {
            item.total = item.amount * item.price;
        });

        this.totalPrice = this.dataSource.data.reduce(
            (acc, item) => acc + item.total,
            0,
        );
    }

    createAction(): void {
        this._router.navigate(['app/kho/xuat-kho/xuat-kho/create']);
    }

    handleAction(element: any, typeAction): void {
        this._router.navigate([
            `app/kho/xuat-kho/xuat-kho/edit/${element.id}`,
            { typeAction },
        ]);
    }

    deleteAction(element: any): void {
        const dialogRef = this.dialog.open(DeleteConfirmDialogComponent, {
            width: '500px',
            data: element,
        });

        dialogRef.componentInstance.handleDelete.subscribe((dataDelete) => {
            const isSubmitted = !!dataDelete;
            this._phieuXuatKhoService.deletePhieuXuatKho(dataDelete).subscribe(
                (res) => dialogRef.close(isSubmitted),
                (err) => this._toastrService.errorServer(err),
            );
        });

        dialogRef.afterClosed().subscribe((result) => {
            if (!result) return;
            this.getInitData(this._dataService.getDauMoiBepId());
            this._toastrService.success('', 'Xóa thành công');
        });
    }

    // HANDLE SEARCH
    handleSearchInfo(event: any): void {
        if (event.key === 'reset') {
            this.getInitData(this._dataService.getDauMoiBepId());
        } else {
            let body = {
                maxResultCount: 9999999,
                criterias: [
                    {
                        propertyName:
                            event.trangThai > -1 &&
                            event.trangThai !== '' &&
                            event.trangThai !== null
                                ? 'trangThai'
                                : '',
                        operation:
                            event.trangThai > -1 &&
                            event.trangThai !== '' &&
                            event.trangThai !== null
                                ? 'Equals'
                                : 'Contains',
                        value:
                            event.trangThai > -1 &&
                            event.trangThai !== '' &&
                            event.trangThai !== null
                                ? event.trangThai
                                : '',
                    },
                    {
                        propertyName: event.ngayXuatKho ? 'ngayXuatKho' : '',
                        operation: event.ngayXuatKho ? 'Equals' : 'Contains',
                        value: event.ngayXuatKho
                            ? new Date(event.ngayXuatKho).toISOString()
                            : '',
                    },
                ],
            };

            this._phieuXuatKhoService
                .getAllPhieuXuatKhoOrigin(body)
                .subscribe((data) => {
                    this.exportWarehouseList = data.result.items;
                    this.dataSource.data = this.exportWarehouseList;
                });
        }
    }

    handleSearchName(dataSearch: any): void {
        this.dataSource.filter = dataSearch.trim().toLowerCase();
    }

    addTotalExport() {
        this._router.navigate(['app/kho/xuat-kho/tong-xuat-kho/create']);
    }
}
