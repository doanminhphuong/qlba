import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProvinceService {
  BASE_URL = "https://provinces.open-api.vn/api/";

  constructor(
    private http: HttpClient,
  ) { }

  getAllProvinces(): Observable<any> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
    });

    let options = { headers: headers };

    return this.http.get<any>(`${this.BASE_URL}`, options);
  }
}
