export class AppConsts {
  static remoteServiceBaseUrl: string;
  static cmsServiceBaseUrl: string;
  static loginUrl: string;
  static appBaseHref: string;
  static applicationId: string;
  static domain: string;

  static IdZero: string = "00000000-0000-0000-0000-000000000000";
  static MCE_API_KEY: string;

  static localeMappings: any = [];

  static readonly userManagement = {
    defaultAdminUserName: "admin"
  };

  static readonly localization = {
    defaultLocalizationSourceName: "EM"
  };

  static readonly authorization = {
    encrptedAuthTokenName: "enc_auth_token",
    abpAuthToken: "Abp.AuthToken"
  };
}
