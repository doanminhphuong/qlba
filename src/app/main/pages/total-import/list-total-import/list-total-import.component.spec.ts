/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { ListTotalImportComponent } from './list-total-import.component';

describe('ListTotalImportComponent', () => {
  let component: ListTotalImportComponent;
  let fixture: ComponentFixture<ListTotalImportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListTotalImportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListTotalImportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
