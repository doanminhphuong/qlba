﻿import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AppConsts } from '@shared/AppConsts';
import { environment } from 'environments/environment';
import { Observable } from 'rxjs';

interface User {
    userNameOrEmailAddress: string;
    password: string;
}
const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
};

@Injectable({
    providedIn: 'root',
})
export class AppAuthService {
    constructor(private http: HttpClient) {}
    BASE_URL = `${environment.SSO_URL}/api/`;

    setSession() {
        console.log('token here');
    }

    login(userNameOrEmailAddress: string, password: string): Observable<User> {
        return this.http.post<User>(
            `${this.BASE_URL}TokenAuth/Authenticate`,
            {
                userNameOrEmailAddress,
                password,
            },
            httpOptions,
        );
        // .pipe(tap((res) => this.setSession));

        // .subscribe((res: any) => {
        //   console.log("res data::", res);
        //   console.log("token::", res.result.accessToken);
        //   console.log("User is logged in");
        //   this.router.navigateByUrl("/form");
        // });
        // this is just the HTTP call,
        // we still need to handle the reception of the token
        // .shareReplay()
    }

    logout(reload?: boolean): void {
        abp.auth.clearToken();
        if (reload !== false) {
            location.href = AppConsts.appBaseHref;
        }
    }
}
