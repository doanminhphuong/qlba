import { SelectionModel } from '@angular/cdk/collections';
import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatPaginator, MatTableDataSource } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { DeleteConfirmDialogComponent } from '@app/_shared/dialogs/delete-confirm-dialog/delete-confirm-dialog.component';
import { DialogRejectStatusComponent } from '@app/_shared/dialogs/dialog-reject-status/dialog-reject-status.component';
import { MaterialDialogComponent } from '@app/_shared/dialogs/material-dialog/material-dialog.component';
import { CategoryService } from '@core/services/category.service';
import { DataService } from '@core/services/data-service';
import { ExportExcelService } from '@core/services/export-excel.service';
import { FoodFuelService } from '@core/services/food-fuel.service';
import { InventoryService } from '@core/services/inventory.service';
import { PhieuXuatKhoService } from '@core/services/phieu-xuat-kho.service';
import { PriceService } from '@core/services/price.service';
import { ToastrService } from '@core/services/toastr.service';
import { UserService } from '@core/services/user.service';
import { forkJoin, Subject, Subscription } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';

@Component({
    selector: 'app-list-export-add-edit',
    templateUrl: './list-export-add-edit.component.html',
    styleUrls: ['./list-export-add-edit.component.scss'],
})
export class ListExportAddEditComponent implements OnInit {
    public applyFilter = new Subject<KeyboardEvent>();
    private subscription: Subscription;
    @ViewChild(MatPaginator) paginator!: MatPaginator;

    titleOnEdit: string;

    form: FormGroup;
    fields: any[] = [];

    displayedColumns: string[] = [
        'mavthh',
        'tenvthh',
        'donViTinh',
        // 'ngayNhap',
        'phaiXuat',
        'thucXuat',
        'donGia',
        'thanhTien',
        'actions',
    ];

    dataSource = new MatTableDataSource<any>([]);
    selection = new SelectionModel<any>(true, []);

    currentTable: any[] = [];
    purchaseId: any;
    typeAction: number | string;
    dataEdit: any;
    priceAdjustmentList: any[] = [];
    allWarehouse: any[] = [];
    allWarehouseIn: any[] = [];
    allWarehouseOut: any[] = [];
    allUsers: any[] = [];
    isStatusExportStore: boolean = false;

    // Boolean
    isEdit: boolean = false;

    totalPrice: number = 0;
    typeOfPrice: string;
    adjustmentInfo: any[] = [];
    priceList: any[] = [];
    foodFuelList: any[] = [];

    // Categories
    userList: any[] = [];
    organizationOptions: any[] = [];
    userOptions: any[] = [];
    supplierOptions: any[] = [];
    chiTietList: any[] = [];
    listReason: any[] = [];

    isActive: boolean = false;
    isActiveExportExcel: boolean = false;
    warehouseExportId: number;
    reasonEatId: string;

    idOrganizationOfUserLogined;

    constructor(
        private readonly changeDetectorRef: ChangeDetectorRef,
        private _activatedRoute: ActivatedRoute,
        private _toastrService: ToastrService,
        private _router: Router,
        public dialog: MatDialog,
        private _priceService: PriceService,
        private _categoryService: CategoryService,
        private _userService: UserService,
        private _foodFuelService: FoodFuelService,
        private _phieuXuatKhoService: PhieuXuatKhoService,
        private _inventoryService: InventoryService,
        private _exportService: ExportExcelService,
        private _dataService: DataService,
    ) {
        this.subscription = this.applyFilter
            .pipe(
                map((event) => (event.target as HTMLInputElement).value),
                debounceTime(500),
                distinctUntilChanged(),
            )
            .subscribe((filterValue) => this.handleInputFilter(filterValue));

        this.subscription = this._dataService.getData().subscribe((message) => {
            if (this.isEdit === false) {
                this._dataService.changeDauMoiBep(message.id);
                this.getInitData(message.id);
            }
        });
    }

    ngOnInit() {
        // Check if this is Create or Edit page
        this._activatedRoute.paramMap.subscribe((param) => {
            this.reasonEatId = param.get('lyDoSuDung');

            console.log('reasonEatId', this.reasonEatId);
            this.purchaseId = +param.get('id');
            this.typeAction = +param.get('typeAction');
            if (!this.purchaseId) return;
            this.isEdit = true;
        });

        this.getAllFoodFuel();
        this.getMultiCategories();
        this.getAccountOrganization();
        setTimeout(() => {
            this.getInitData(this._dataService.getDauMoiBepId());
        }, 1000);
    }

    ngAfterViewChecked(): void {
        this.changeDetectorRef.detectChanges();
    }

    ngAfterViewInit(): void {}

    getAllFoodFuel(): void {
        this._foodFuelService
            .getAllFoodFuel({ maxResultCount: 999999999 })
            .subscribe((res) => {
                this.foodFuelList = res.result.items;
            });
    }

    getAccountOrganization() {
        this._userService.getUserOrganization().subscribe((response) => {
            let result = response.result.filter(
                (item) =>
                    item.groupCode === 'DonVi' || item.groupCode === '1-donvi',
            );

            this.idOrganizationOfUserLogined = result[0].id;
        });
    }

    getMultiCategories() {
        this.getSupplierOptions();
        this.getReasonOptions();
    }

    compareIdToCode(id: number | string) {
        let result = '';
        if (this.foodFuelList.length > 0) {
            const dataFound = this.foodFuelList.find((item) => item.id === id);
            result = dataFound ? dataFound.maLttpChatDot : '';
        }

        return result;
    }

    getBodyCategory(groupCode: string) {
        return {
            maxResultCount: 10,
            skipCount: 0,
            sorting: 'Code',
            criterias: [
                {
                    propertyName: 'GroupCode',
                    operation: 6,
                    value: groupCode,
                },
            ],
        };
    }

    getSupplierOptions() {
        this._categoryService
            .getAllCategory(this.getBodyCategory('nha-cung-cap'))
            .subscribe((res) => {
                this.supplierOptions = res.result.map((item) => ({
                    key: item.codeData,
                    name: item.name,
                }));
            });
    }

    getReasonOptions() {
        this._categoryService
            .getAllCategory(this.getBodyCategory('ly-do-xuat-hang'))
            .subscribe((res) => {
                this.listReason = res.result.map((item) => ({
                    key: item.codeData,
                    name: item.name,
                }));
            });
    }

    getInitData(id: number): void {
        this.isActive = false;
        const dataPriceAdjust = this._priceService.getAllPriceAdjustment({});
        const getAllWarehouse = this._inventoryService.getAllWarehouse({
            maxResultCount: 999999999,
            criterias: [
                {
                    propertyName: 'dauMoiBepId',
                    operation: 0,
                    value: id,
                },
            ],
        });

        forkJoin([dataPriceAdjust, getAllWarehouse]).subscribe((results) => {
            this.priceAdjustmentList = results[0].result.items.map((item) => ({
                key: item.id,
                name: item.tenDotDieuChinhGia,
            }));

            this.allWarehouse = results[1].result.items.map((item) => ({
                key: item.id,
                name: item.tenKho,
                status: item.status,
            }));
            this.allWarehouseIn = [...this.allWarehouse];
            this.allWarehouseOut = [...this.allWarehouse];

            this.getInitForm();
        });

        this.dataSource.paginator = this.paginator;
    }

    getInitForm(): void {
        this.fields = [
            {
                type: 'DATETIME',
                referenceValue: 'ngayXuatKho',
                name: 'Ngày xuất kho',
                defaultValue: '',
                required: '1',
                css: 'col-12 col-lg-6',
                appearance: 'legacy',
                disabled: this.typeAction === 3 ? true : false,
            },
            {
                type: 'DATETIME',
                referenceValue: 'ngayHetHan',
                name: 'Ngày hết hạn',
                defaultValue: '',
                required: '1',
                css: 'col-12 col-lg-6',
                appearance: 'legacy',
                dependenField: 'ngayXuatKho',
                disabled: true,
            },
            {
                type: 'TEXT',
                referenceValue: 'maPhieuXuatKho',
                name: 'Mã phiếu xuất kho',
                defaultValue: '',
                required: '1',
                css: 'col-12 col-lg-6',
                appearance: 'legacy',
                disabled: this.typeAction === 3 ? true : false,
            },
            {
                type: 'SELECT',
                referenceValue: 'khoXuatId',
                name: '---- Kho muốn xuất hàng',
                defaultValue: '',
                options: [...this.allWarehouseOut],
                required: '1',
                disabled: this.typeAction === 3 ? true : false,
                css: 'col-12 col-lg-6',
                appearance: 'legacy',
            },
            {
                type: 'TEXT',
                referenceValue: 'nguoiNhan',
                name: 'Họ tên người nhận',
                defaultValue: this._dataService.getNguoiNhan(),
                required: '0',
                css: 'col-12 col-lg-4',
                appearance: 'legacy',
                disabled: this.typeAction === 3 ? true : false,
            },
            {
                type: 'TEXT',
                referenceValue: 'donViBoPhan',
                name: 'Đơn vị (bộ phận)',
                defaultValue: this._dataService.getDonViThuocQuanLy().name,
                required: '0',
                css: 'col-12 col-lg-4',
                appearance: 'legacy',
                disabled: this.typeAction === 3 ? true : false,
            },
            {
                type: 'TEXT',
                referenceValue: 'nhanTaiKho',
                name: 'Nhận tại kho',
                defaultValue: '',
                required: '0',
                disabled: this.typeAction === 3 ? true : false,
                css: 'col-12 col-lg-4',
                appearance: 'legacy',
            },
            {
                type: 'SELECT',
                referenceValue: 'lyDoSuDung',
                name: 'Lý do sử dụng',
                defaultValue: this.reasonEatId,
                required: '1',
                options: [...this.listReason],
                disabled:
                    this.typeAction === 3
                        ? true
                        : this.reasonEatId !== null
                        ? true
                        : false,
                css: 'col-12 col-lg-12',
                appearance: 'legacy',
            },
        ];

        let fieldsCtrls = {};
        let fieldList = [];

        for (let f of this.fields) {
            fieldList = [...fieldList, f];

            if (f.type === 'NUMBER') {
                fieldsCtrls[f.referenceValue] = new FormControl(
                    { value: f.defaultValue, disabled: f.disabled },
                    f.required === '1' ? [Validators.required] : [],
                );
            } else if (f.type !== 'CHECKBOX') {
                let validators = [
                    Validators.minLength(f.minLength),
                    Validators.maxLength(f.maxLength),
                    Validators.pattern(f.pattern),
                ];

                if (f.type === 'EMAIL') {
                    validators = [...validators, Validators.email];
                }

                fieldsCtrls[f.referenceValue] = new FormControl(
                    {
                        value: f.defaultValue || '',
                        disabled: f.disabled,
                    },
                    f.required === '1'
                        ? [...validators, Validators.required]
                        : [...validators],
                );

                if (f.type === 'SELECT') {
                    if (f.referenceValue === 'khoXuatId') {
                        fieldsCtrls[f.referenceValue].valueChanges.subscribe(
                            (value) => {
                                if (!value) return;

                                this.warehouseExportId = value;
                                this.isActive = true;
                            },
                        );
                    }
                }
                if (f.type === 'DATETIME') {
                    if (f.referenceValue === 'ngayXuatKho') {
                        fieldsCtrls[f.referenceValue].valueChanges.subscribe(
                            (val) => {
                                if (this.isEdit && !this.isStatusExportStore) {
                                    this.isStatusExportStore = true;
                                    return;
                                }

                                const KEY = 'PXK';
                                let date = new Date(val);
                                let part3 = date
                                    .toLocaleDateString('en-GB')
                                    .replace(/[^0-9]/g, '');
                                let part4;

                                if (localStorage.getItem('maXK') === null) {
                                    localStorage.setItem(
                                        'maXK',
                                        JSON.stringify({
                                            currentDate: new Date()
                                                .toISOString()
                                                .slice(0, 10),
                                            index: 1,
                                        }),
                                    );

                                    let dataLocal = JSON.parse(
                                        localStorage.getItem('maXK'),
                                    );

                                    part4 = this.pad(dataLocal.index);
                                } else {
                                    let currentDate = new Date()
                                        .toISOString()
                                        .slice(0, 10);

                                    let dataLocal = JSON.parse(
                                        localStorage.getItem('maXK'),
                                    );

                                    if (currentDate === dataLocal.currentDate) {
                                        part4 = this.pad(dataLocal.index);
                                    } else {
                                        localStorage.setItem(
                                            'maXK',
                                            JSON.stringify({
                                                currentDate: new Date()
                                                    .toISOString()
                                                    .slice(0, 10),
                                                index: 1,
                                            }),
                                        );

                                        let dataLocal = JSON.parse(
                                            localStorage.getItem('maXK'),
                                        );

                                        part4 = this.pad(dataLocal.index);
                                    }
                                }

                                let fullPath = KEY + '.' + part3 + '.' + part4;
                                this.form.patchValue({
                                    maPhieuXuatKho: fullPath,
                                });

                                const dependField = fieldList.find(
                                    (x) => x.dependenField === f.referenceValue,
                                );

                                if (!dependField) return;

                                dependField.min = val;

                                if (this.typeAction === 3) {
                                    fieldsCtrls[
                                        dependField.referenceValue
                                    ].disable();
                                } else {
                                    fieldsCtrls[
                                        dependField.referenceValue
                                    ].enable();
                                }
                            },
                        );
                    }
                }
            } else {
                //if checkbox, it need multiple
                let opts = {};
                for (let opt of f.options) {
                    opts[opt.key] = new FormControl({
                        value: opt.value,
                        disabled: f.disabled,
                    });
                }
            }
        }

        this.form = new FormGroup(fieldsCtrls);

        if (this.isEdit) {
            this.getDataEdit();
        }
    }

    pad(d) {
        return d < 10 ? '0' + d.toString() : d.toString();
    }

    get khoXuatId() {
        return this.form.get('khoXuatId');
    }

    get khoNhanId() {
        return this.form.get('khoNhanId');
    }

    getDataEdit(): void {
        const dataEditId = {
            id: this.purchaseId,
        };
        this._phieuXuatKhoService
            .getPhieuXuatKhoById(dataEditId)
            .subscribe((res) => {
                this.dataEdit = res.result;
                // Get Title
                if (this.dataEdit.trangThai === 3) {
                    this.titleOnEdit = `Chi tiết phiếu xuất kho '${this.dataEdit.maPhieuXuatKho}'`;
                } else {
                    this.titleOnEdit = `Chỉnh sửa phiếu xuất kho '${this.dataEdit.maPhieuXuatKho}'`;
                }

                // Patch data to Form
                this.form.patchValue({
                    ...this.dataEdit,
                    nguoiNhan: this.dataEdit.nguoiNhan,
                    donViBoPhan: this.dataEdit.donViBoPhan,
                });

                // Patch data to Table (Details)
                this.currentTable = this.dataEdit.chiTiet.map((item) => ({
                    ...item,
                    ...item.lttpChatDot,
                }));

                // Remove id on each item
                this.currentTable.forEach((item) => {
                    delete item.id;
                });

                this.dataSource.data = this.currentTable;

                this.totalPrice = this.dataEdit.tongTien;
            });
    }

    // Handle Input Filter
    handleInputFilter(filterValue) {
        this.dataSource.data = this.currentTable.filter((item) =>
            item.tenLttpChatDot
                .toLowerCase()
                .includes(filterValue.trim().toLowerCase()),
        );
    }

    /** Whether the number of selected elements matches the total number of rows. */
    isAllSelected() {
        const numSelected = this.selection.selected.length;
        const numRows = this.dataSource.data.length;
        return numSelected === numRows;
    }

    /** Selects all rows if they are not all selected; otherwise clear selection. */
    toggleAllRows() {
        if (this.isAllSelected()) {
            this.selection.clear();
            return;
        }

        this.selection.select(...this.dataSource.data);
    }

    /** The label for the checkbox on the passed row */
    checkboxLabel(row?: any): string {
        if (!row) {
            return `${this.isAllSelected() ? 'deselect' : 'select'} all`;
        }
        return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${
            row.index + 1
        }`;
    }

    // Get Price of Item (GIÁ MUA NGOÀI or GIÁ TGSX)
    getPriceOfItem(id) {
        let price = 0;

        const item = this.priceList.find((item) => item.id === id);
        price = item ? item.price : 0;

        return price;
    }

    getTotalPrice() {
        this.dataSource.data.forEach((item) => {
            item.thanhTien = item.soLuongThucXuat
                ? item.soLuongThucXuat * item.donGia
                : 0;
        });

        this.totalPrice = this.dataSource.data.reduce(
            (acc, item) => acc + item.thanhTien,
            0,
        );
    }

    // GUID
    guid = () => {
        let s4 = () => {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        };
        //return id of format 'aaaaaaaa'-'aaaa'-'aaaa'-'aaaa'-'aaaaaaaaaaaa'
        return (
            s4() +
            s4() +
            '-' +
            s4() +
            '-' +
            s4() +
            '-' +
            s4() +
            '-' +
            s4() +
            s4() +
            s4()
        );
    };

    // Add row
    addRow(): void {
        const dialogRef = this.dialog.open(MaterialDialogComponent, {
            // disableClose: true,
            width: '70%',
            data: {
                type: 'INVENTORY_EXPORT',
                khoXuatId: this.warehouseExportId,
                currentTable: this.currentTable,
            },
            panelClass: 'add-field-dialog-class',
        });

        dialogRef.componentInstance.onSave.subscribe((data) => {
            const isSubmitted = !!data;

            this.currentTable = [...data, ...this.dataSource.data];
            this.dataSource.data = this.currentTable;
            this.getTotalPrice();
            dialogRef.close(isSubmitted);
        });

        dialogRef.afterClosed().subscribe((result) => {
            if (!result) return;
            this._toastrService.success('', 'Thêm thành công');
        });
    }

    // Delete row
    deleteAction(element: any): void {
        const dialogRef = this.dialog.open(DeleteConfirmDialogComponent, {
            width: '500px',
            data: element,
        });

        dialogRef.componentInstance.handleDelete.subscribe((dataDelete) => {
            const isSubmitted = !!dataDelete;
            dialogRef.close(isSubmitted);
        });

        dialogRef.afterClosed().subscribe((result) => {
            if (!result) return;

            const index: number = this.currentTable.indexOf(element);
            if (index !== -1) {
                this.currentTable.splice(index, 1);
            }

            this.dataSource.data = this.currentTable;

            this.getTotalPrice();

            this._toastrService.success('', 'Xóa thành công');
        });
    }

    // Back to list
    onBack(): void {
        this._router.navigate(['app/kho/xuat-kho']);
    }

    // Submit data
    onSubmit(): void {
        const dataSubmit = {
            ...this.dataEdit,
            ...this.form.value,
            tongTien: this.totalPrice,
            chiTiet: [...this.currentTable],
            trangThai: 3,
            donViId: this.idOrganizationOfUserLogined,
        };

        if (!this.isEdit) {
            this._phieuXuatKhoService.createPhieuXuatKho(dataSubmit).subscribe(
                (res) => {
                    if (res.success) {
                        let dataLocal = JSON.parse(
                            localStorage.getItem('maXK'),
                        );

                        localStorage.setItem(
                            'maXK',
                            JSON.stringify({
                                currentDate: new Date()
                                    .toISOString()
                                    .slice(0, 10),
                                index: dataLocal.index + 1,
                            }),
                        );

                        this._router.navigate(['app/kho/xuat-kho']);
                        this._toastrService.success(
                            '',
                            'Tạo phiếu xuất kho thành công',
                        );
                    }
                },
                (err) => this._toastrService.errorServer(err),
            );
        } else {
            this._phieuXuatKhoService.updatePhieuXuatKho(dataSubmit).subscribe(
                (res) => {
                    this._router.navigate(['app/kho/xuat-kho']);
                    this._toastrService.success('', 'Cập nhật thành công');
                },
                (err) => this._toastrService.errorServer(err),
            );
        }
    }

    onSave(status = 0): void {
        const dataSubmit = {
            ...this.dataEdit,
            ...this.form.value,
            tongTien: this.totalPrice,
            chiTiet: [...this.currentTable],
            trangThai: status,
            donViId: this.idOrganizationOfUserLogined,
        };
        if (!this.isEdit) {
            this._phieuXuatKhoService.createPhieuXuatKho(dataSubmit).subscribe(
                (res) => {
                    if (res.success) {
                        let dataLocal = JSON.parse(
                            localStorage.getItem('maXK'),
                        );

                        localStorage.setItem(
                            'maXK',
                            JSON.stringify({
                                currentDate: new Date()
                                    .toISOString()
                                    .slice(0, 10),
                                index: dataLocal.index + 1,
                            }),
                        );

                        if (this.isActiveExportExcel === false) {
                            this._router.navigate(['app/kho/xuat-kho']);
                        }
                        this._toastrService.success(
                            '',
                            'Lưu nháp phiếu xuất kho thành công',
                        );
                    }
                },
                (err) => this._toastrService.errorServer(err),
            );
        } else {
            this._phieuXuatKhoService.updatePhieuXuatKho(dataSubmit).subscribe(
                (res) => {
                    this._router.navigate(['app/kho/xuat-kho']);
                    this._toastrService.success('', 'Cập nhật thành công');
                },
                (err) => this._toastrService.errorServer(err),
            );
        }
    }

    exportExcelAndSave() {
        this.isActiveExportExcel = true;
        this.onSave(0);
        this.exportExcel();
    }

    printContent() {
        window.print();
    }

    rejectAction() {
        const dialogRef = this.dialog.open(DialogRejectStatusComponent, {
            width: '500px',
            data:
                this.dataEdit && this.dataEdit.notificationTitle
                    ? this.dataEdit.notificationTitle
                    : '',

            //notificationTitle: hiện tại thay lí do reject ở value1
        });

        dialogRef.afterClosed().subscribe((result) => {
            if (result.length) {
                //Reject change status = 2
                // let optionsData = {
                //     notificationTitle: result,
                // };
                const dataSubmit = {
                    ...this.dataEdit,
                    ...this.form.value,
                    tongTien: this.totalPrice,
                    chiTiet: [...this.currentTable],
                    // notificationTitle: result,
                    value1: result, //notificationTitle: hiện tại thay lí do reject ở value1
                    trangThai: 2,
                };
                this._phieuXuatKhoService
                    .updatePhieuXuatKho(dataSubmit)
                    .subscribe(
                        (res) => {
                            this._router.navigate(['app/kho/xuat-kho']);
                            this._toastrService.success(
                                '',
                                'Cập nhật thành công',
                            );
                        },
                        (err) => this._toastrService.errorServer(err),
                    );
                // this.onSave(2, optionsData);

                // this._router.navigate(['app/kho/xuat-kho']);
            }
        });
    }

    reviewAction() {
        console.log('reviewAction');
    }

    handleSelectkhoNhanId(e) {
        let tempWare = [...this.allWarehouse];
        // this.allWarehouseOut = tempWare.filter((item) => item.key !== e.value);
        this.form.get('khoNhanId').valueChanges.subscribe((item) => {
            this.allWarehouseOut = tempWare.filter(
                (itemE) => itemE.key !== item.value,
            );
        });
    }

    exportExcel() {
        let bodyDataExcel = {
            ...this.dataEdit,
            lyDoSuDung: this.listReason.filter(
                (reason) => reason.key === this.dataEdit.lyDoSuDung,
            ),
            chiTiet: [...this.currentTable],
        };

        this._exportService.generateExportWarehouseExcel(
            bodyDataExcel,
            `Xuat Kho ${this.form.get('maPhieuXuatKho').value}.xlsx`,
        );
    }
}
