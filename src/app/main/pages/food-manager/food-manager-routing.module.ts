import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DynamicCreateMenuComponent } from './dynamic-create-menu/dynamic-create-menu.component';
import { FoodAddEditComponent } from './food-manager-main/food-add-edit/food-add-edit.component';
import { FoodManagerMainComponent } from './food-manager-main/food-manager-main.component';
import { MenuAddEditComponent } from './menu-list-manager/menu-add-edit/menu-add-edit.component';
import { MenuListManagerComponent } from './menu-list-manager/menu-list-manager.component';

const routes: Routes = [
    {
        path: '',
        redirectTo: 'quan-ly-mon-an',
        pathMatch: 'full',
    },
    {
        path: 'quan-ly-mon-an',
        // component: FoodManagerMainComponent,
        children: [
            {
                path: '',
                component: FoodManagerMainComponent,
            },
            {
                path: 'create',
                component: FoodAddEditComponent,
            },
            {
                path: 'edit/:id',
                component: FoodAddEditComponent,
            },
        ],
    },
    {
        path: 'danh-sach-thuc-don',
        // component: MenuListManagerComponent,
        children: [
            {
                path: '',
                component: MenuListManagerComponent,
            },
            {
                path: 'create',
                component: MenuAddEditComponent,
            },
            {
                path: 'edit/:id',
                component: MenuAddEditComponent,
            },
        ],
    },
    {
        path: 'thiet-lap-td-tu-dong',
        component: DynamicCreateMenuComponent,
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class FoodManagerRoutingModule {}
