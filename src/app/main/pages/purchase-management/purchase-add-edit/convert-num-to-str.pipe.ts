import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'convertNumToStr',
})
export class ConvertNumToStrPipe implements PipeTransform {
    transform(value: number, args?: any): any {
        return this.readMoney(value);
    }

    convertVNDToWords(amount: number): string {
        const numberWords: string[] = [
            'không',
            'một',
            'hai',
            'ba',
            'bốn',
            'năm',
            'sáu',
            'bảy',
            'tám',
            'chín',
        ];

        const unitWords: string[] = [
            '',
            'nghìn',
            'triệu',
            'tỷ',
            'nghìn tỷ',
            'triệu tỷ',
        ];

        if (amount === 0) {
            return 'không';
        }

        let words = '';

        if (amount < 0) {
            words += 'âm ';
            amount = Math.abs(amount);
        }

        let i = 0;

        while (amount > 0) {
            const current = amount % 1000;

            if (current > 0) {
                if (words !== '') {
                    words = ' ' + words;
                }

                if (current < 10) {
                    words =
                        numberWords[current] + ' ' + unitWords[i] + ' ' + words;
                } else if (current < 100) {
                    words =
                        numberWords[Math.floor(current / 10)] +
                        ' mươi ' +
                        (current % 10 > 0 ? numberWords[current % 10] : '') +
                        ' ' +
                        unitWords[i] +
                        '' +
                        words;
                } else {
                    words =
                        numberWords[Math.floor(current / 100)] +
                        ' trăm ' +
                        (current % 100 > 0
                            ? this.convertVNDToWords(current % 100)
                            : '') +
                        ' ' +
                        unitWords[i] +
                        '' +
                        words;
                }
            }

            i++;
            amount = Math.floor(amount / 1000);
        }

        return words;
    }

    // Hàm chuyển đổi của anh Tuấn đưa
    readGroup(group) {
        let readDigit = [
            ' Không',
            ' Một',
            ' Hai',
            ' Ba',
            ' Bốn',
            ' Năm',
            ' Sáu',
            ' Bảy',
            ' Tám',
            ' Chín',
        ];
        var temp = '';
        if (group == '000') return '';
        temp = readDigit[parseInt(group.substring(0, 1))] + ' Trăm';
        if (group.substring(1, 2) == '0')
            if (group.substring(2, 3) == '0') return temp;
            else {
                temp += ' Lẻ' + readDigit[parseInt(group.substring(2, 3))];
                return temp;
            }
        else temp += readDigit[parseInt(group.substring(1, 2))] + ' Mươi';
        if (group.substring(2, 3) == '5') temp += ' Lăm';
        else if (group.substring(2, 3) != '0')
            temp += readDigit[parseInt(group.substring(2, 3))];
        return temp;
    }

    readMoney(num) {
        if (num == null || num == '') return '';
        let temp = '';
        while (num.length < 18) {
            num = '0' + num;
        }
        let g1 = num.substring(0, 3);
        let g2 = num.substring(3, 6);
        let g3 = num.substring(6, 9);
        let g4 = num.substring(9, 12);
        let g5 = num.substring(12, 15);
        let g6 = num.substring(15, 18);

        if (g1 != '000') {
            temp = this.readGroup(g1);
            temp += ' Triệu';
        }
        if (g2 != '000') {
            temp += this.readGroup(g2);
            temp += ' Nghìn';
        }
        if (g3 != '000') {
            temp += this.readGroup(g3);
            temp += ' Tỷ';
        } else if ('' != temp) {
            temp += ' Tỷ';
        }
        if (g4 != '000') {
            if (g3 != '000' && g4.charAt(0) === '0') {
                temp += ' không trăm';
            }
            temp += this.readGroup(g4);
            temp += ' Triệu';
        }
        if (g5 != '000') {
            if (g5.charAt(0) === '0') {
                temp += ' không trăm';
            }
            temp += this.readGroup(g5);
            temp += ' Nghìn';
        }
        if (g6 !== '000') {
            if (g6.charAt(0) === '0') {
                temp += ' không trăm';
            }
        }

        temp = temp + this.readGroup(g6);
        temp = temp.replace(/Một Mươi/g, 'Mười');
        temp = temp.trim();
        temp = temp.replace(/Không Trăm/g, '');
        temp = temp.trim();
        temp = temp.replace(/Mười Không/g, 'Mười');
        temp = temp.trim();
        temp = temp.replace(/Mươi Không/g, 'Mươi');
        temp = temp.trim();
        if (temp.indexOf('Lẻ') == 0) temp = temp.substring(2);
        temp = temp.trim();
        temp = temp.replace(/Mươi Một/g, 'Mươi Mốt');
        temp = temp.trim();
        let result =
            temp.substring(0, 1).toUpperCase() + temp.substring(1).toLowerCase();
        return (result == '' ? 'Không' : result) + ' đồng';
    }

    // numValue: any[] = 'không;một;hai;ba;bốn;năm;sáu;bảy;tám;chín'.split(';');

    // readDozen(so: number, daydu: boolean) {
    //     console.log('so', so);
    //     console.log('daydu', daydu);

    //     let chuoi = '';

    //     //Hàm để lấy số hàng chục ví dụ 21/10 = 2
    //     let hangChuc = Math.floor(so / 10);

    //     //Lấy số hàng đơn vị bằng phép chia 21 % 10 = 1
    //     let hangDonVi = so % 10;

    //     //Nếu số hàng chục tồn tại tức >=20
    //     if (hangChuc > 1) {
    //         chuoi = ' ' + this.numValue[hangChuc] + ' mươi';
    //         if (hangDonVi == 1) {
    //             chuoi += ' mốt';
    //         }
    //     } else if (hangChuc == 1) {
    //         //Số hàng chục từ 10-19
    //         chuoi = ' mười';
    //         if (hangDonVi == 1) {
    //             chuoi += ' một';
    //         }
    //     } else if (daydu && hangDonVi > 0) {
    //         //Nếu hàng đơn vị khác 0 và có các số hàng trăm ví dụ 101 => thì biến daydu = true => và sẽ đọc một trăm lẻ một
    //         chuoi = ' lẻ';
    //     }
    //     if (hangDonVi == 5 && hangChuc >= 1) {
    //         //Nếu đơn vị là số 5 và có hàng chục thì chuỗi sẽ là " lăm" chứ không phải là " năm"
    //         chuoi += ' lăm';
    //     } else if (hangDonVi > 1 || (hangDonVi == 1 && hangChuc == 0)) {
    //         chuoi += ' ' + this.numValue[hangDonVi];
    //     }
    //     console.log('trăm đồng', chuoi);
    //     return chuoi;
    // }

    // readHundred(so: number, daydu: boolean) {
    //     let chuoi = '';

    //     //Lấy số hàng trăm ví du 434 / 100 = 4 (hàm Floor sẽ làm tròn số nguyên bé nhất)
    //     let tram = Math.floor(so / 100);

    //     //Lấy phần còn lại của hàng trăm 434 % 100 = 34 (dư 34)
    //     so = so % 100;

    //     if (daydu || tram > 0) {
    //         chuoi = ' ' + this.numValue[tram] + ' trăm';
    //         chuoi += this.readDozen(so, true);
    //     } else {
    //         chuoi = this.readDozen(so, false);
    //     }
    //     return chuoi;
    // }

    // readMillion(so: number, daydu: boolean) {
    //     let chuoi = '';
    //     //Lấy số hàng triệu
    //     let trieu = Math.floor(so / 1000000);

    //     //Lấy phần dư sau số hàng triệu ví dụ 2,123,000 => so = 123,000
    //     so = so % 1000000;

    //     if (trieu > 0) {
    //         daydu = true;
    //         chuoi = this.readHundred(trieu, daydu) + ' triệu';
    //     }
    //     //Lấy số hàng nghìn
    //     let nghin = Math.floor(so / 1000);
    //     //Lấy phần dư sau số hàng nghin
    //     so = so % 1000;
    //     if (nghin > 0) {
    //         daydu = true;
    //         chuoi += this.readHundred(nghin, daydu) + ' nghìn';
    //     }
    //     if (so > 0) {
    //         chuoi += this.readHundred(nghin, daydu);
    //     }
    //     return chuoi;
    // }

    // convertNumToStr(num: any) {
    //     if (num == 0) {
    //         return this.numValue[0];
    //     }
    //     let chuoi = '',
    //         hauto = '';
    //     let ty;
    //     do {
    //         //Lấy số hàng tỷ
    //         ty = Math.floor(num / 1000000000);
    //         //Lấy phần dư sau số hàng tỷ
    //         num = num % 1000000000;
    //         if (ty > 0) {
    //             chuoi = this.readMillion(num, true) + hauto + chuoi;
    //         } else {
    //             chuoi = this.readMillion(num, false) + hauto + chuoi;
    //         }
    //         hauto = ' tỷ';
    //     } while (ty > 0);

    //     console.log('chuoi:', chuoi);
    //     return (
    //         chuoi.charAt(1).toUpperCase() +
    //         chuoi.substring(2).toLowerCase() +
    //         ' đồng'
    //     );
    // }
}
