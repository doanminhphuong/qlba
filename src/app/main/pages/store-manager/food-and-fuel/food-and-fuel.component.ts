import { SelectionModel } from '@angular/cdk/collections';
import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import {
    MatDialog,
    MatPaginator,
    MatTableDataSource,
    PageEvent,
} from '@angular/material';
import { DynamicDialogComponent } from '@app/components/dynamic-dialog-component/dynamic-dialog-component.component';
import { DeleteConfirmDialogComponent } from '@app/_shared/dialogs/delete-confirm-dialog/delete-confirm-dialog.component';
import { CategoryService } from '@core/services/category.service';
import { CommonService } from '@core/services/common.service';
import { FoodFuelService } from '@core/services/food-fuel.service';
import { ToastrService } from '@core/services/toastr.service';
import { UserService } from '@core/services/user.service';
import { forkJoin } from 'rxjs';

@Component({
    selector: 'app-food-and-fuel',
    templateUrl: './food-and-fuel.component.html',
    styleUrls: ['./food-and-fuel.component.scss'],
})
export class FoodAndFuelComponent implements OnInit {
    @ViewChild(MatPaginator) paginator!: MatPaginator;

    unitOptions: any[] = [];
    groupOptions: any[] = [];
    typeOptions: any[] = [];
    classifyOptions: any[] = [];

    displayedColumnsOfField: string[] = [
        'select',
        'maLttpChatDot',
        'tenLttpChatDot',
        'tenNhomLttpChatDot',
        'donViTinhId',
        'classify',
        'phanLoaiNhom',
        'actions',
    ];

    dataSource = new MatTableDataSource<any>([]);
    selection = new SelectionModel<any>(true, []);

    totalRows = 0;
    pageSize = 10;
    currentPage = 0;
    pageSizeOptions: number[] = [5, 10, 25, 50];
    dataSkipped: number = 0;

    fieldSearch: any[] = [];

    isFiltering: boolean = false;
    isFilteringAdvance: boolean = false;
    tenLttpChatDot: string;
    nhomLttpChatDotId: number | string;
    userOrganization: any;

    allowExec: boolean = false;

    constructor(
        public dialog: MatDialog,
        private _toastrService: ToastrService,
        private _categoryService: CategoryService,
        private _foodFuelService: FoodFuelService,
        private _userService: UserService,
        private _commonService: CommonService,
    ) {
        let request = {
            id: localStorage.getItem('idMenu'),
            language: 'vi',
        };

        this._commonService.checkPermission(request).subscribe((response) => {
            if (JSON.parse(response.result).some((obj) => obj.key === 'EXEC')) {
                this.allowExec = true;
            } else {
                this.displayedColumnsOfField.splice(0, 1);
                this.displayedColumnsOfField.splice(6, 1);
            }
        });
    }

    ngOnInit() {
        this._userService.getUserOrganization().subscribe((response) => {
            this.userOrganization = response.result[0].id;

            this.getInitData();
        });
        this.getMultiCategories();
    }

    ngAfterViewInit() {
        this.dataSource.paginator = this.paginator;
    }

    getInitData(): void {
        const body = {
            maxResultCount: this.pageSize,
            skipCount: this.dataSkipped,
            sorting: 'creationTime DESC',
            donViId: this.userOrganization,
        };

        this._foodFuelService.getAllFoodFuel(body).subscribe((res) => {
            this.dataSource.data = res.result.items;
            setTimeout(() => {
                this.paginator.pageIndex = this.currentPage;
                this.paginator.length = res.result.totalCount;
            });

            this.selection.clear();
        });
    }

    pageChanged(event: PageEvent) {
        if (this.isFiltering) return;

        this.currentPage = event.pageIndex;
        this.dataSkipped = event.pageIndex * 10;

        this.getInitData();
    }

    getMultiCategories() {
        this.getUnitOptions();
        this.getTypeOptions();
        this.getAllGroupFoodFuel();
    }

    getBodyCategory(groupCode: string) {
        return {
            maxResultCount: 10,
            skipCount: 0,
            sorting: 'Code',
            criterias: [
                {
                    propertyName: 'GroupCode',
                    operation: 6,
                    value: groupCode,
                },
            ],
        };
    }

    getUnitOptions() {
        this._categoryService
            .getAllCategory(this.getBodyCategory('don-vi-tinh'))
            .subscribe((res) => {
                this.unitOptions = res.result.map((item) => ({
                    key: item.id,
                    name: item.name,
                }));
            });
    }

    getTypeOptions() {
        this._categoryService
            .getAllCategory(this.getBodyCategory('phan-loai-nhom'))
            .subscribe((res) => {
                this.classifyOptions = res.result.map((item) => ({
                    key: item.codeData,
                    name: item.name,
                }));
            });
    }

    getAllGroupFoodFuel() {
        const body = {
            maxResultCount: 9999,
            skipCount: 0,
            sorting: 'Id',
        };

        const dataType = this._categoryService.getAllCategory(
            this.getBodyCategory('phan-loai-lttp-chat-dot'),
        );

        const dataGroup = this._foodFuelService.getAllGroupOfFoodFuel(body);

        forkJoin([dataGroup, dataType]).subscribe((res) => {
            this.groupOptions = res[0].result.items
                .filter((item) => item.status === 'ENABLE')
                .map((item) => ({
                    key: item.id,
                    name: item.tenNhomLttpChatDot,
                }));

            this.typeOptions = res[1].result.map((item) => ({
                key: item.codeData,
                name: item.name,
            }));

            this.fieldSearch = [
                {
                    type: 'SELECT',
                    referenceValue: 'phanLoai',
                    name: 'Phân loại LTTP, chất đốt',
                    defaultValue: '',
                    required: '0',
                    options: [...this.typeOptions],
                    css: 'col-12 col-lg-6',
                    appearance: 'legacy',
                },
                {
                    type: 'SELECT',
                    referenceValue: 'nhomLttpChatDotId',
                    name: 'Nhóm LTTP, CĐ',
                    defaultValue: '',
                    required: '0',
                    options: [],
                    css: 'col-12 col-lg-6',
                    appearance: 'legacy',
                    disabled: true,
                    dependenField: 'phanLoai',
                },
            ];
        });
    }

    getObjectElementUser(key, type) {
        let data;
        let dataList = [];

        if (
            this.unitOptions.length > 0 &&
            this.groupOptions.length > 0 &&
            this.typeOptions.length > 0
        ) {
            switch (type) {
                case 'UNIT':
                    dataList = this.unitOptions;
                    break;
                case 'GROUP':
                    dataList = this.groupOptions;
                    break;
                case 'TYPE':
                    dataList = this.typeOptions;
                    break;
                case 'CLASSIFY':
                    dataList = this.classifyOptions;
                    break;
            }
            let item = dataList.find((item) => item.key === key);
            data = item && item.name ? item.name : '';
        }

        return data;
    }

    /** Whether the number of selected elements matches the total number of rows. */
    isAllSelected() {
        const numSelected = this.selection.selected.length;
        const numRows = this.dataSource.data.length;
        return numSelected === numRows;
    }

    /** Selects all rows if they are not all selected; otherwise clear selection. */
    toggleAllRows() {
        if (this.isAllSelected()) {
            this.selection.clear();
            return;
        }

        this.selection.select(...this.dataSource.data);
    }

    /** The label for the checkbox on the passed row */
    checkboxLabel(row?: any): string {
        if (!row) {
            return `${this.isAllSelected() ? 'deselect' : 'select'} all`;
        }
        return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${
            row.index + 1
        }`;
    }

    guid = () => {
        let s4 = () => {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        };
        //return id of format 'aaaaaaaa'-'aaaa'-'aaaa'-'aaaa'-'aaaaaaaaaaaa'
        return (
            s4() +
            s4() +
            '-' +
            s4() +
            '-' +
            s4() +
            '-' +
            s4() +
            '-' +
            s4() +
            s4() +
            s4()
        );
    };

    openDialog(dataDialog: any): void {
        const fields = [
            {
                type: 'TEXT',
                referenceValue: 'tenLttpChatDot',
                name: 'Tên thực phẩm, chất đốt',
                defaultValue: '',
                required: '1',
                icon: 'restaurant',
                css: 'col-12 col-lg-6',
            },
            {
                type: 'TEXT',
                referenceValue: 'maLttpChatDot',
                name: 'Mã thực phẩm, chất đốt',
                defaultValue: '',
                required: '1',
                icon: 'code',
                css: 'col-12 col-lg-6',
                dependenField: 'tenLttpChatDot',
            },
            {
                type: 'SELECT',
                referenceValue: 'phanLoai',
                name: 'Danh mục thực phẩm, chất đốt',
                defaultValue: '',
                required: '1',
                options: [...this.typeOptions],
                css: 'col-12 col-lg-6',
            },
            {
                type: 'SELECT',
                referenceValue: 'nhomLttpChatDotId',
                name: 'Nhóm thực phẩm',
                defaultValue: '',
                required: '1',
                options: [],
                css: 'col-12 col-lg-6',
                disabled: true,
                dependenField: 'phanLoai',
            },
            {
                type: 'SELECT',
                referenceValue: 'donViTinhId',
                name: 'Đơn vị tính',
                defaultValue: '',
                required: '1',
                options: [...this.unitOptions],
                css: 'col-12 col-lg-6',
            },
            {
                type: 'SELECT',
                referenceValue: 'value1',
                name: 'Loại nhóm',
                defaultValue: '',
                required: '0',
                options: [...this.classifyOptions],
                css: 'col-12 col-lg-6',
            },
            {
                type: 'RADIO',
                referenceValue: 'status',
                name: 'Trạng thái',
                defaultValue: 'ENABLE',
                required: '0',
                options: [
                    { key: 'ENABLE', name: 'Hoạt động' },
                    { key: 'DISABLE', name: 'Tạm ngưng' },
                ],
                css: 'col-12 col-lg-6',
            },
        ];

        let dialogRef = this.dialog.open(DynamicDialogComponent, {
            // disableClose: true,
            width: '70%',
            data: {
                datas: dataDialog,
                fields: fields,
                typeDialog: 'lttp',
            },
            panelClass: 'my-custom-dialog-class',
        });

        dialogRef.componentInstance.onSave.subscribe((formValues) => {
            const isSubmitted = !!formValues;

            let dataSubmit = {
                ...dataDialog,
                ...formValues,
            };

            if (!dataDialog) {
                this._foodFuelService
                    .createFoodFuel({
                        ...dataSubmit,
                        donViId: this.userOrganization,
                    })
                    .subscribe(
                        (res) => dialogRef.close(isSubmitted),
                        (err) => this._toastrService.errorServer(err),
                    );
            } else {
                this._foodFuelService.updateFoodFuel(dataSubmit).subscribe(
                    (res) => dialogRef.close(isSubmitted),
                    (err) => this._toastrService.errorServer(err),
                );
            }
        });

        dialogRef.componentInstance.handleDelete.subscribe((dataDelete) => {
            const isDeleted = !!dataDelete;
        });

        dialogRef.afterClosed().subscribe((result) => {
            if (!result) return;

            if (this.isFiltering) {
                const propertyName = 'tenLttpChatDot';
                const operation = 6;
                const value = this.tenLttpChatDot;

                this.getAllDataFilter(propertyName, operation, value);
            } else if (this.isFilteringAdvance) {
                const propertyName = 'nhomLttpChatDotId';
                const operation = 0;
                const value = this.nhomLttpChatDotId;

                this.getAllDataFilter(propertyName, operation, value);
            } else {
                this.getInitData();
            }

            if (!dataDialog) {
                this._toastrService.success('', 'Thêm thành công');
            } else {
                this._toastrService.success('', 'Cập nhật thành công');
            }
        });
    }

    createAction(): void {
        this.openDialog(null);
    }

    editAction(element: any): void {
        this.openDialog(element);
    }

    deleteAction(element: any): void {
        const dialogRef = this.dialog.open(DeleteConfirmDialogComponent, {
            width: '500px',
            data: element,
        });

        dialogRef.componentInstance.handleDelete.subscribe((dataDelete) => {
            const isSubmitted = !!dataDelete;

            this._foodFuelService
                .deleteFoodFuel({
                    lttpChatDotId: dataDelete.id,
                    donViId: this.userOrganization,
                })
                .subscribe(
                    (res) => dialogRef.close(isSubmitted),
                    (err) => this._toastrService.errorServer(err),
                );
        });

        dialogRef.afterClosed().subscribe((result) => {
            if (!result) return;

            this.getInitData();
            this._toastrService.success('', 'Xóa thành công');
        });
    }

    deleteMultiAction(): void {
        const dialogRef = this.dialog.open(DeleteConfirmDialogComponent, {
            width: '500px',
        });

        dialogRef.componentInstance.handleDelete.subscribe((dataDelete) => {
            const isSubmitted = !dataDelete;

            this.selection.selected.forEach((item) => {
                const dataDelete = {
                    lttpChatDotId: item.id,
                    donViId: this.userOrganization,
                };

                this._foodFuelService.deleteFoodFuel(dataDelete).subscribe(
                    (res) => dialogRef.close(isSubmitted),
                    (err) => this._toastrService.errorServer(err),
                );
            });
        });

        dialogRef.afterClosed().subscribe((result) => {
            if (!result) return;

            this.getInitData();
            this._toastrService.success('', 'Xóa thành công');
        });
    }

    getAllDataFilter(
        propertyName: string,
        operation: number | string,
        value: any,
    ) {
        const bodyFilter = {
            maxResultCount: 9999,
            skipCount: 0,
            sorting: 'Id',
            criterias: [
                {
                    propertyName,
                    operation,
                    value,
                },
            ],
        };

        this._foodFuelService.getAllFoodFuel(bodyFilter).subscribe((res) => {
            this.dataSource.data = res.result.items;
        });
    }

    handleInputFilter(filterValue: string) {
        this.tenLttpChatDot = filterValue.trim().toLowerCase();

        if (!this.tenLttpChatDot) {
            this.isFiltering = false;
            this.getInitData();
        } else {
            const propertyName = 'tenLttpChatDot';
            const operation = 6;
            const value = this.tenLttpChatDot;

            this.getAllDataFilter(propertyName, operation, value);
            this.isFiltering = true;
        }
    }

    handleSearchAdvanced(dataSearchAdvanced: any) {
        const { phanLoai, nhomLttpChatDotId, key } = dataSearchAdvanced;

        if (!key) {
            const bodyFilter = {
                maxResultCount: 9999,
                skipCount: 0,
                sorting: 'Id',
                criterias: [
                    {
                        propertyName: nhomLttpChatDotId
                            ? 'nhomLttpChatDotId'
                            : '',
                        operation: nhomLttpChatDotId ? 0 : 'Contains',
                        value: nhomLttpChatDotId,
                    },
                    {
                        propertyName: phanLoai ? 'phanLoai' : '',
                        operation: phanLoai ? 0 : 'Contains',
                        value: phanLoai,
                    },
                ],
            };

            this._foodFuelService
                .getAllFoodFuel(bodyFilter)
                .subscribe((res) => {
                    this.isFiltering = true;
                    this.dataSource.data = res.result.items;
                });
        } else {
            this.getInitData();
            this.isFilteringAdvance = false;
        }
    }
}
