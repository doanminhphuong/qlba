import { SelectionModel } from '@angular/cdk/collections';
import {
    AfterContentChecked,
    AfterViewChecked,
    AfterViewInit,
    ChangeDetectorRef,
    Component,
    EventEmitter,
    Inject,
    OnChanges,
    OnInit,
    QueryList,
    SimpleChanges,
    ViewChild,
    ViewChildren,
} from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import {
    MatDialogRef,
    MatPaginator,
    MatTableDataSource,
    MAT_DIALOG_DATA,
} from '@angular/material';
import { MaterialDialogComponent } from '@app/_shared/dialogs/material-dialog/material-dialog.component';
import { CategoryService } from '@core/services/category.service';
import { DataService } from '@core/services/data-service';
import { MenuListService } from '@core/services/menu-list.service';
import { ToastrService } from '@core/services/toastr.service';
import { forkJoin } from 'rxjs';
export const DATA = [
    {
        ma: 'TH',
        tenMonAn: 'Thịt heo kho trứng cút',
        donViTinh: 'Phần',
        loaiMonAn: 'Món ăn chính',
        buoiAn: 'Sáng',
    },
];
@Component({
    selector: 'app-food-list-dialog',
    templateUrl: './food-list-dialog.component.html',
    styleUrls: ['./food-list-dialog.component.scss'],
})
export class FoodListDialogComponent implements OnInit, AfterViewInit {
    displayedColumnsOfField: string[] = [
        'select',
        'ma',
        'tenMonAn',
        'loaiMonAn',
        'buoiAn',
    ];

    tables: any[] = [];

    @ViewChildren(MatPaginator) paginator = new QueryList<MatPaginator>();

    onSave = new EventEmitter();
    form: FormGroup;
    formFilter: FormGroup;
    dataSource = new MatTableDataSource<any>([]);
    selection = new SelectionModel<any>(true, []);

    selected: string = 'all';
    listTypeOfFood: any[] = [];
    listUnits: any[] = [];
    tabSelected = new FormControl(0);

    chiTietNgay = [
        {
            buoiAn: 'Sáng',
            danhSach: [],
        },
        {
            buoiAn: 'Trưa',
            danhSach: [],
        },
        {
            buoiAn: 'Chiều',
            danhSach: [],
        },
    ];

    selectFilter: string = '';
    public filterCtrl: FormControl = new FormControl();
    constructor(
        public dialogRef: MatDialogRef<MaterialDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any,
        private categoryService: CategoryService,
        private menuListService: MenuListService,
        private readonly changeDetectorRef: ChangeDetectorRef,
        private _toastrService: ToastrService,
        private _dataService: DataService,
    ) {}

    ngOnInit() {
        [1, 2, 3].forEach((tableNumber) => {
            this.tables.push({
                columnsToDisplay: [
                    'select',
                    'ma',
                    'tenMonAn',
                    'loaiMonAn',
                    'buoiAn',
                ],
                expandedElement: null,
                dataSource: new MatTableDataSource<any>(),
                paginator: null,
                sort: null,
            });
        });
        this.getListFood();
        this.getTypeOfFood();
        this.getUnits();
        this.getInitForm();
        this.selection.clear();
    }

    ngAfterViewInit(): void {}

    /** Whether the number of selected elements matches the total number of rows. */
    isAllSelected() {
        const numSelected = this.selection.selected.length;
        const numRows = this.dataSource.data.length;
        return numSelected === numRows;
    }

    /** Selects all rows if they are not all selected; otherwise clear selection. */
    toggleAllRows() {
        if (this.isAllSelected()) {
            this.selection.clear();
            return;
        }

        this.selection.select(...this.dataSource.data);
        this.addFood();
    }

    toggleRow(event: any, row: any) {
        if (event.checked === true) {
            this.selection.toggle(row);
            this.addFood();
        } else if (event.checked === false) {
            this.selection.deselect(row);

            this.chiTietNgay[this.tabSelected.value].danhSach =
                this.chiTietNgay[this.tabSelected.value].danhSach.filter(
                    (item) => item.monAnId !== row.id,
                );
        }
    }

    /** The label for the checkbox on the passed row */
    checkboxLabel(row?: any): string {
        if (!row) {
            return `${this.isAllSelected() ? 'deselect' : 'select'} all`;
        }
        return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${
            row.index + 1
        }`;
    }

    getInitForm() {
        let fieldsCtrls = {};

        for (let f of this.data.fields) {
            if (f.type === 'NUMBER') {
                fieldsCtrls[f.referenceValue] = new FormControl(
                    { value: f.defaultValue, disabled: f.disabled },
                    f.required === '1' ? [Validators.required] : [],
                );
            } else if (f.type !== 'CHECKBOX') {
                let validators = [
                    Validators.minLength(f.minLength),
                    Validators.maxLength(f.maxLength),
                    Validators.pattern(f.pattern),
                ];

                if (f.type === 'EMAIL') {
                    validators = [...validators, Validators.email];
                }

                fieldsCtrls[f.referenceValue] = new FormControl(
                    { value: f.defaultValue || '', disabled: f.disabled },
                    f.required === '1'
                        ? [...validators, Validators.required]
                        : [...validators],
                );
            } else {
                //if checkbox, it need multiple
                let opts = {};
                for (let opt of f.options) {
                    opts[opt.key] = new FormControl({
                        value: opt.value,
                        disabled: f.disabled,
                    });
                }
                fieldsCtrls[f.referenceValue] = new FormGroup(opts);
            }
        }

        this.form = new FormGroup(fieldsCtrls);

        this.getInitData();
    }

    getInitData(): void {
        let dataEdit = this.data.datas;
        if (!dataEdit) return;
        this.form.patchValue(dataEdit.ngayAn);
    }

    getListFood() {
        let bodySubmit = {
            maxResultCount: 9999999,
            criterias: [
                {
                    propertyName: 'status',
                    operation: 'Equals',
                    value: 'ENABLE',
                },
                {
                    propertyName: 'dauMoiBepId',
                    operation: 0,
                    value: this._dataService.getDauMoiBepId(),
                },
            ],
        };
        let listFood = this.menuListService.getAllFoodList(bodySubmit);

        forkJoin([listFood]).subscribe((res) => {
            let data = res[0].result.items.map((item) => ({
                ...item,
                dinhLuong: '',
            }));

            for (let i = 0; i < this.tables.length; i++) {
                this.tables[i].dataSource = new MatTableDataSource<any>(data);
                this.tables[i].dataSource.paginator =
                    this.paginator.toArray()[i];

                if (this.tables.length - 1 === i) {
                    if (this.data.datas) {
                        if (this.data.datas.chiTietNgay.length > 0) {
                            this.chiTietNgay = this.data.datas.chiTietNgay;

                            this.chiTietNgay[
                                this.tabSelected.value
                            ].danhSach.forEach((item) => {
                                this.tables[
                                    this.tabSelected.value
                                ].dataSource.data.forEach((row) => {
                                    if (item.monAnId === row.id) {
                                        row.dinhLuong = item.dinhLuong;
                                    }
                                });
                            });

                            this.filter(0);
                        }
                    }
                }
            }
        });
    }

    getFields() {
        return this.data.fields;
    }

    getTypeOfFood() {
        let bodyRequest = {
            sorting: 'Code',
            criterias: [
                {
                    propertyName: 'GroupCode',
                    operation: 6,
                    value: 'loai-mon-an',
                },
            ],
        };
        this.categoryService
            .getAllCategory(bodyRequest)
            .subscribe((response) => {
                if (response.success) {
                    let dataResponse = [];
                    dataResponse = response.result.map((item) => ({
                        key: item.number1,
                        name: item.name,
                    }));

                    this.listTypeOfFood.push(
                        {
                            key: 'all',
                            name: 'Tất cả',
                        },
                        ...dataResponse,
                    );
                }
            });
    }

    getUnits() {
        let bodyRequest = {
            sorting: 'Code',
            criterias: [
                {
                    propertyName: 'GroupCode',
                    operation: 6,
                    value: 'don-vi-tinh',
                },
            ],
        };
        this.categoryService
            .getAllCategory(bodyRequest)
            .subscribe((response) => {
                if (response.success) {
                    this.listUnits = response.result.map((item) => ({
                        key: item.id,
                        name: item.name,
                    }));
                }
            });
    }

    compareObject(key, type) {
        let data;
        let dataList = [];

        if (this.listTypeOfFood.length > 0 && this.listUnits.length > 0) {
            switch (type) {
                case 'FOOD':
                    dataList = this.listTypeOfFood;
                    break;
                case 'UNIT':
                    dataList = this.listUnits;
                    break;
            }
            let item = dataList.find((item) => item.key === key);
            data = item && item.name ? item.name : '';
        }

        return data;
    }

    handleSelectFilter(dataSelected: any) {
        this.selectFilter = dataSelected.value;
    }

    filter(index: any) {
        let bodySubmit =
            this.selectFilter !== 'all' && parseInt(this.selectFilter) >= 0
                ? {
                      maxResultCount: 9999999,
                      sorting: 'Code',
                      criterias: [
                          {
                              propertyName: 'status',
                              operation: 'Equals',
                              value: 'ENABLE',
                          },
                          {
                              propertyName: this.filterCtrl.value
                                  ? 'tenMonAn'
                                  : '',
                              operation: 'Contains',
                              value: this.filterCtrl.value,
                          },
                          {
                              propertyName: 'dauMoiBepId',
                              operation: 0,
                              value: this._dataService.getDauMoiBepId(),
                          },
                      ],
                      loaiMonAn: this.selectFilter,
                  }
                : {
                      maxResultCount: 9999999,
                      sorting: 'Code',
                      criterias: [
                          {
                              propertyName: 'status',
                              operation: 'Equals',
                              value: 'ENABLE',
                          },
                          {
                              propertyName: this.filterCtrl.value
                                  ? 'tenMonAn'
                                  : '',
                              operation: 'Contains',
                              value: this.filterCtrl.value,
                          },
                          {
                              propertyName: 'dauMoiBepId',
                              operation: 0,
                              value: this._dataService.getDauMoiBepId(),
                          },
                      ],
                  };

        this.menuListService
            .getAllFoodList(bodySubmit)
            .subscribe((response) => {
                if (response.success) {
                    if (this.data.datas) {
                        let data = response.result.items.map((item) => ({
                            ...item,
                            dinhLuong: '',
                        }));

                        for (let i = 0; i < this.tables.length; i++) {
                            this.tables[i].dataSource =
                                new MatTableDataSource<any>(data);
                            this.tables[i].dataSource.paginator =
                                this.paginator.toArray()[i];
                        }
                        if (this.data.datas.chiTietNgay.length > 0) {
                            this.chiTietNgay = this.data.datas.chiTietNgay;

                            this.chiTietNgay[
                                this.tabSelected.value
                            ].danhSach.forEach((item) => {
                                this.tables[index].dataSource.data.forEach(
                                    (row) => {
                                        if (this.tabSelected.value === index) {
                                            if (item.monAnId === row.id) {
                                                this.selection.select(row);
                                                row.dinhLuong = item.dinhLuong;
                                            }
                                        }
                                    },
                                );
                            });
                        } else {
                            this.chiTietNgay[
                                this.tabSelected.value
                            ].danhSach.forEach((item) => {
                                this.tables[index].dataSource.data.forEach(
                                    (row) => {
                                        if (this.tabSelected.value === index) {
                                            if (item.monAnId === row.id) {
                                                this.selection.select(row);
                                                row.dinhLuong = item.dinhLuong;
                                            }
                                        }
                                    },
                                );
                            });
                        }
                    } else {
                        let data = response.result.items.map((item) => ({
                            ...item,
                            dinhLuong: '',
                        }));

                        for (let i = 0; i < this.tables.length; i++) {
                            this.tables[i].dataSource =
                                new MatTableDataSource<any>(data);
                            this.tables[i].dataSource.paginator =
                                this.paginator.toArray()[i];
                        }

                        this.chiTietNgay[
                            this.tabSelected.value
                        ].danhSach.forEach((item) => {
                            this.tables[index].dataSource.data.forEach(
                                (row) => {
                                    if (this.tabSelected.value === index) {
                                        if (item.monAnId === row.id) {
                                            this.selection.select(row);
                                            row.dinhLuong = item.dinhLuong;
                                        }
                                    }
                                },
                            );
                        });
                    }
                }
            });
    }

    onKey(event) {
        this.chiTietNgay[this.tabSelected.value].danhSach =
            this.selection.selected.map((item) => ({
                monAnId: item.id,
                dinhLuong: item.dinhLuong,
            }));
    }

    onCloseDialog(data?: any): void {
        this.dialogRef.close(data);
    }

    convertToNumber(key: any) {
        if (key === 'Sáng') {
            return 0;
        } else if (key === 'Trưa') {
            return 1;
        } else if (key === 'Chiều') {
            return 2;
        }
    }

    convertToMeal(key: any) {
        if (key === 0 || key === '0') {
            return 'Sáng';
        } else if (key === 1 || key === '1') {
            return 'Trưa';
        } else if (key === 2 || key === '2') {
            return 'Chiều';
        }
    }

    addFood() {
        this.chiTietNgay[this.tabSelected.value].danhSach =
            this.selection.selected.map((item) => ({
                monAnId: item.id,
                dinhLuong: item.dinhLuong,
            }));
    }

    tabSelectionChanged(event: any) {
        this.tables[this.tabSelected.value].dataSource.data.forEach(
            (row) => (row.dinhLuong = ''),
        );
        this.tabSelected = new FormControl(event.index);
        this.selection.clear();

        this.chiTietNgay[this.tabSelected.value].danhSach.forEach((item) => {
            this.tables[this.tabSelected.value].dataSource.data.forEach(
                (row) => {
                    if (item.monAnId === row.id) {
                        this.selection.select(row);
                        row.dinhLuong = item.dinhLuong;
                    }
                },
            );
        });
    }

    checkMealHasFood(input: unknown[] | string) {
        const inputArray = Array.isArray(input) ? input : null;

        if (inputArray) {
            return inputArray.map((str) => ' ' + str);
        }

        return input;
    }

    onSubmit(): void {
        let formValues = {};

        let mealNotFood = [];

        for (let chiTiet of this.chiTietNgay) {
            if (chiTiet.danhSach.length === 0) {
                mealNotFood.push(chiTiet.buoiAn);
            }
        }

        let nameOfMeal = this.checkMealHasFood(mealNotFood);

        if (mealNotFood.length > 0) {
            this._toastrService.error(
                '',
                'Buổi ' + nameOfMeal + ' chưa chọn món ăn',
            );
        } else {
            let object = {
                ngayAn: this.form.value.ngayAn,
                chiTietNgay: this.chiTietNgay,
            };

            formValues = {
                ...object,
            };

            this.onSave.emit(formValues);
        }
    }
}
