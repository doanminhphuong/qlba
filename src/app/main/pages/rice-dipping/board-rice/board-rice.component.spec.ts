import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BoardRiceComponent } from './board-rice.component';

describe('BoardRiceComponent', () => {
  let component: BoardRiceComponent;
  let fixture: ComponentFixture<BoardRiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BoardRiceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BoardRiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
