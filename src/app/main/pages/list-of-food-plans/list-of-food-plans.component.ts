import { SelectionModel } from '@angular/cdk/collections';
import {
    AfterViewInit,
    Component,
    OnDestroy,
    OnInit,
    ViewChild,
} from '@angular/core';
import { MatDialog, MatPaginator, MatTableDataSource } from '@angular/material';
import { Router } from '@angular/router';
import { DynamicDialogComponent } from '@app/components/dynamic-dialog-component/dynamic-dialog-component.component';
import { DeleteConfirmDialogComponent } from '@app/_shared/dialogs/delete-confirm-dialog/delete-confirm-dialog.component';
import { CategoryService } from '@core/services/category.service';
import { ToastrService } from '@core/services/toastr.service';
import { MaterialDialogComponent } from '@app/_shared/dialogs/material-dialog/material-dialog.component';
import { PriceService } from '@core/services/price.service';
import { UserService } from '@core/services/user.service';
import { forkJoin, Observable, Subject, Subscription } from 'rxjs';
import { PhieuXuatKhoService } from '@core/services/phieu-xuat-kho.service';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { CommonService } from '@core/services/common.service';
import { DataService } from '@core/services/data-service';

@Component({
    selector: 'app-list-of-food-plans',
    templateUrl: './list-of-food-plans.component.html',
    styleUrls: ['./list-of-food-plans.component.scss'],
})
export class ListOfFoodPlansComponent
    implements OnInit, AfterViewInit, OnDestroy
{
    @ViewChild(MatPaginator) paginator!: MatPaginator;

    currentDate: Date = new Date();
    dauMoiBepId: number;

    displayedColumnsOfField: string[] = [
        'maPhieuXuatKho',
        'maNguoiDuyet',
        'ngayXuatKho',
        'actions',
    ];

    dataSource = new MatTableDataSource<any>([]);
    selection = new SelectionModel<any>(true, []);
    userList: any[] = [];
    exportWarehouseList: any[] = [];

    inventoryList: any[] = [];
    totalPrice: number = 0;
    currentTable: any[] = [];
    unitOptions: any[] = [];
    typeOptions: any[] = [];

    fieldSearch: any[] = [
        {
            type: 'TEXT',
            referenceValue: 'name',
            name: 'Tên người mua',
            defaultValue: '',
            required: '1',
            css: 'col-12 col-lg-12',
            appearance: 'legacy',
        },
        {
            type: 'DATETIME',
            referenceValue: 'startDate',
            name: 'Từ ngày',
            defaultValue: '',
            required: '0',
            css: 'col-12 col-lg-6',
            appearance: 'legacy',
        },
        {
            type: 'DATETIME',
            referenceValue: 'endDate',
            name: 'Đến ngày',
            defaultValue: '',
            required: '0',
            dependenField: 'startDate',
            css: 'col-12 col-lg-6',
            appearance: 'legacy',
        },
    ];

    allowExec: boolean = false;
    subscription: Subscription;
    isLoading: boolean = false;

    constructor(
        private _router: Router,
        public dialog: MatDialog,
        private _toastrService: ToastrService,
        private _categoryService: CategoryService,
        private _priceService: PriceService,
        private _userService: UserService,
        private _phieuXuatKhoService: PhieuXuatKhoService,
        private _commonService: CommonService,
        private _dataService: DataService,
    ) {
        let request = {
            id: localStorage.getItem('idMenu'),
            language: 'vi',
        };

        this._commonService.checkPermission(request).subscribe((response) => {
            if (JSON.parse(response.result).some((obj) => obj.key === 'EXEC')) {
                this.allowExec = true;
            }
        });

        this.subscription = this._dataService.getData().subscribe((message) => {
            this.isLoading = true;
            this.getInitData(message.id);
        });
    }

    ngOnInit() {
        this.dauMoiBepId = this._dataService.getDauMoiBepId();
        this.getMultiCategories();
        setTimeout(() => this.getInitData(this.dauMoiBepId), 1000);
    }

    ngAfterViewInit() {
        this.dataSource.paginator = this.paginator;
        this.paginator._intl.itemsPerPageLabel = 'Items/Page:';
    }

    ngOnDestroy(): void {
        this.subscription.unsubscribe();
    }

    getInitData(dauMoiBepId: number): void {
        this.dauMoiBepId = dauMoiBepId;

        this._commonService
            .callDataAPIShort('/api/services/read/DuXuat/GetAll', {
                maxResultCount: 2147483647,
                criterias: [
                    {
                        propertyName: 'dauMoiBepId',
                        operation: 0,
                        value: dauMoiBepId,
                    },
                ],
            })
            .subscribe((response) => {
                this.dataSource.data = response.result.items;
                this.dataSource.paginator = this.paginator;
                setTimeout(() => (this.isLoading = false), 200);
            });
    }

    getMultiCategories() {
        this.getUnitOptions();
        this.getTypeOptions();
    }

    getBodyCategory(groupCode: string) {
        return {
            maxResultCount: 10,
            skipCount: 0,
            sorting: 'Code',
            criterias: [
                {
                    propertyName: 'GroupCode',
                    operation: 6,
                    value: groupCode,
                },
            ],
        };
    }

    getUnitOptions() {
        this._categoryService
            .getAllCategory(this.getBodyCategory('don-vi-tinh'))
            .subscribe((res) => {
                this.unitOptions = res.result.map((item) => ({
                    key: item.codeData,
                    name: item.name,
                }));
            });
    }

    getTypeOptions() {
        this._categoryService
            .getAllCategory(this.getBodyCategory('phan-loai-lttp-chat-dot'))
            .subscribe((res) => {
                this.typeOptions = res.result.map((item) => ({
                    key: item.codeData,
                    name: item.name,
                }));
            });
    }

    getUserName(id) {
        let result = '';

        if (this.userList.length > 0) {
            const dataFound = this.userList.find((user) => user.id === id);

            result = dataFound ? `${dataFound.surname} ${dataFound.name}` : '';
        }

        return result;
    }

    /** Whether the number of selected elements matches the total number of rows. */
    isAllSelected() {
        const numSelected = this.selection.selected.length;
        const numRows = this.dataSource.data.length;
        return numSelected === numRows;
    }

    /** Selects all rows if they are not all selected; otherwise clear selection. */
    toggleAllRows() {
        if (this.isAllSelected()) {
            this.selection.clear();
            return;
        }

        this.selection.select(...this.dataSource.data);
    }

    /** The label for the checkbox on the passed row */
    checkboxLabel(row?: any): string {
        if (!row) {
            return `${this.isAllSelected() ? 'deselect' : 'select'} all`;
        }
        return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${
            row.index + 1
        }`;
    }

    getTotalPrice() {
        this.dataSource.data.forEach((item) => {
            item.total = item.amount * item.price;
        });

        this.totalPrice = this.dataSource.data.reduce(
            (acc, item) => acc + item.total,
            0,
        );
    }

    createAction(element: any): void {
        if (element) {
            this._router.navigate([
                `manage/app/du-chi-ngay-ke-tiep/edit/${element.id}`,
            ]);
        } else {
            this._router.navigate(['manage/app/du-chi-ngay-ke-tiep/create']);
        }
    }

    handleAction(element: any, typeAction): void {
        this._router.navigate([
            `manage/app/du-chi-ngay-ke-tiep/edit/${element.id}`,
            { typeAction },
        ]);
    }

    deleteAction(element: any): void {
        const dialogRef = this.dialog.open(DeleteConfirmDialogComponent, {
            width: '500px',
            data: element,
        });

        dialogRef.componentInstance.handleDelete.subscribe((dataDelete) => {
            const isSubmitted = !!dataDelete;

            this._commonService
                .callDataAPIShort('/api/services/write/DuXuat/Delete', {
                    id: element.id,
                })
                .subscribe(
                    (res) => dialogRef.close(isSubmitted),
                    (err) => this._toastrService.errorServer(err),
                );
        });

        dialogRef.afterClosed().subscribe((result) => {
            if (!result) return;
            this.getInitData(this.dauMoiBepId);
            this._toastrService.success('', 'Xóa thành công');
        });
    }

    // HANDLE SEARCH
    handleInputFilter(filterValue: string): void {
        const searchValue = filterValue.trim().toLowerCase();

        if (!searchValue) {
            this.getInitData(this.dauMoiBepId);
        } else {
            this._commonService
                .callDataAPIShort('/api/services/read/DuXuat/GetAll', {
                    maxResultCount: 2147483647,
                    criterias: [
                        {
                            propertyName: 'dauMoiBepId',
                            operation: 0,
                            value: this.dauMoiBepId,
                        },
                        {
                            propertyName: 'tenDuXuat',
                            operation: 6,
                            value: searchValue,
                        },
                    ],
                })
                .subscribe((res) => {
                    this.dataSource.data = res.result.items;
                });
        }
    }
}
