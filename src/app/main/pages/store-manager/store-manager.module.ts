import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StoreManagerRoutingModule } from './store-manager-routing.module';
import { StoreListComponent } from './store-list/store-list.component';
import { FoodAndFuelComponent } from './food-and-fuel/food-and-fuel.component';
import { StoreInventoryComponent } from './store-inventory/store-inventory.component';
import { LayoutModule } from '@app/_shared/layout.module';
import {
    MatButtonModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatDividerModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatOptionModule,
    MatPaginatorModule,
    MatProgressSpinnerModule,
    MatSelectModule,
    MatTableModule,
} from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SearchAdvancedComponent } from '@app/_shared/common/search-advanced/search-advanced.component';
import { SearchComponent } from '@app/_shared/common/search/search.component';
import { DialogInventoryDetailComponent } from './store-inventory/dialog-inventory-detail/dialog-inventory-detail.component';

@NgModule({
    declarations: [
        StoreListComponent,
        FoodAndFuelComponent,
        StoreInventoryComponent,
        DialogInventoryDetailComponent,
    ],
    imports: [
        CommonModule,
        StoreManagerRoutingModule,
        LayoutModule,
        MatIconModule,
        MatDividerModule,
        MatCheckboxModule,
        MatPaginatorModule,
        MatProgressSpinnerModule,
        FormsModule,
        ReactiveFormsModule,
        MatFormFieldModule,
        MatInputModule,
        MatTableModule,
        MatSelectModule,
        MatOptionModule,
        MatDatepickerModule,
        MatButtonModule,
        MatTableModule,
    ],
    entryComponents: [
        SearchAdvancedComponent,
        SearchComponent,
        DialogInventoryDetailComponent,
    ],
})
export class StoreManagerModule {}
