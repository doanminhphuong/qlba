import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SwapunitComponent } from './swapunit.component';

describe('SwapunitComponent', () => {
  let component: SwapunitComponent;
  let fixture: ComponentFixture<SwapunitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SwapunitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SwapunitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
