import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatPaginator, MatTableDataSource } from '@angular/material';
import { Router } from '@angular/router';
import { CommonService } from '@core/services/common.service';
import { DataService } from '@core/services/data-service';
import { UserService } from '@core/services/user.service';
import { forkJoin, Subscription } from 'rxjs';

@Component({
    selector: 'app-maintenance-pure-store',
    templateUrl: './maintenance-pure-store.component.html',
    styleUrls: ['./maintenance-pure-store.component.scss'],
})
export class MaintenancePureStoreComponent implements OnInit, OnDestroy {
    @ViewChild(MatPaginator) paginator!: MatPaginator;
    subscription: Subscription;

    // Table
    displayedColumnsOfField = [
        'ngayTao',
        'tenBienBan',
        'nguoiLapId',
        'actions',
    ];
    dataSource = new MatTableDataSource<any>([]);

    // Search Advance
    fieldSearch: any[] = [
        {
            type: 'DATETIME',
            referenceValue: 'ngayTao',
            name: 'Ngày tạo',
            defaultValue: '',
            required: '0',
            css: 'col-12 col-lg-6',
            appearance: 'legacy',
        },
    ];

    // Init
    userList: any[] = [];
    dauMoiBepId: number;

    // Boolean
    allowExec: boolean = false;
    isLoading: boolean = true;

    constructor(
        private _router: Router,
        public dialog: MatDialog,
        private commonService: CommonService,
        private _userService: UserService,
        private _dataService: DataService,
    ) {
        this.commonService.checkPermission2().subscribe((response) => {
            if (!response.result) return;

            if (JSON.parse(response.result).some((obj) => obj.key === 'EXEC')) {
                this.allowExec = true;
            }
        });

        // This event is excecuted when Bep changes
        this.subscription = this._dataService.getData().subscribe((message) => {
            this.isLoading = true;
            this.getInitData(message.id);
        });
    }

    ngOnInit() {
        this.getAllUsers();
        this.dauMoiBepId = this._dataService.getDauMoiBepId();
        this.getInitData(this.dauMoiBepId);
    }

    ngOnDestroy(): void {
        this.subscription.unsubscribe();
    }

    getInitData(dauMoiBepId: number) {
        this.dauMoiBepId = dauMoiBepId;

        const dataTinhKho = this.commonService.callDataAPIShort(
            '/api/services/read/BienBanTinhKhoDccd/GetAll',
            {
                maxResultCount: 99999999,
                dauMoiBepId: dauMoiBepId,
            },
        );

        forkJoin([dataTinhKho]).subscribe((response) => {
            console.log('dataTinhKho:: ', response[0].result.items);

            this.dataSource.data = response[0].result.items;
            this.dataSource.paginator = this.paginator;

            setTimeout(() => (this.isLoading = false), 300);
        });
    }

    getAllUsers(): void {
        this._userService.getUsers({}).subscribe((res) => {
            this.userList = res.result;
        });
    }

    getUserNameById(userId: number) {
        let fullName;
        if (this.userList.length > 0) {
            const dataFound = this.userList.find((user) => user.id === userId);

            fullName = dataFound
                ? `${dataFound.surname} ${dataFound.name}`
                : '';
        }
        return fullName;
    }

    handleInputFilter(dataSearch: any) {
        this.dataSource.filter = dataSearch.trim().toLowerCase();
    }

    handleSearchAdvanced(dataAdvanced: any) {
        if (dataAdvanced.key === 'reset') {
            this.getInitData(this.dauMoiBepId);
        } else {
            let bodySubmit = {
                maxResultCount: 99999999,
                criterias: [
                    {
                        propertyName: dataAdvanced.ngayTao ? 'ngayTao' : '',
                        operation: dataAdvanced.ngayTao ? 'Equals' : 'Contains',
                        value: dataAdvanced.ngayTao
                            ? new Date(dataAdvanced.ngayTao._d).toISOString()
                            : '',
                    },
                ],
                dauMoiBepId: this.dauMoiBepId,
            };

            this.commonService
                .callDataAPIShort(
                    '/api/services/read/BienBanTinhKhoDccd/GetAll',
                    bodySubmit,
                )
                .subscribe((response) => {
                    this.dataSource = new MatTableDataSource<any>(
                        response.result.items,
                    );
                    this.dataSource.paginator = this.paginator;
                });
        }
    }

    action(element: any) {
        if (element) {
            this._router.navigate([
                `manage/app/category/tinh-kho-dccd/detail/${element.id}`,
            ]);
        } else {
            this._router.navigateByUrl(
                'manage/app/category/tinh-kho-dccd/create',
            );
        }
    }
}
