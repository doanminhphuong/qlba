import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FoodAndFuelComponent } from './food-and-fuel.component';

describe('FoodAndFuelComponent', () => {
  let component: FoodAndFuelComponent;
  let fixture: ComponentFixture<FoodAndFuelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FoodAndFuelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FoodAndFuelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
