import { Component, OnInit } from '@angular/core';
import {
    animate,
    state,
    style,
    transition,
    trigger,
} from '@angular/animations';
import { resolve } from 'url';
import {
    FormBuilder,
    FormControl,
    FormGroup,
    Validators,
} from '@angular/forms';
import { Observable } from 'rxjs';
import { map, mergeMap, pairwise, startWith } from 'rxjs/operators';
import {
    DateAdapter,
    MatDatepickerInputEvent,
    MAT_DATE_FORMATS,
    MAT_DATE_LOCALE,
} from '@angular/material';
import {
    MAT_MOMENT_DATE_ADAPTER_OPTIONS,
    MomentDateAdapter,
} from '@angular/material-moment-adapter';
import { MY_FORMATS } from '../inventory-import/import-add-edit/import-add-edit.component';
import { type } from 'os';
import { ExportExcelService } from '@core/services/export-excel.service';
import { CurrencyPipe } from '@angular/common';
import { CommonService } from '@core/services/common.service';
import { HttpErrorResponse } from '@angular/common/http';
import { DataService } from '@core/services/data-service';
import { HoSoSucKhoeService } from '@core/services/ho-so-suc-khoe.service';
import * as moment from 'moment';

@Component({
    selector: 'app-financial-report',
    templateUrl: './financial-report.component.html',
    styleUrls: ['./financial-report.component.scss'],
    animations: [
        trigger('detailExpand', [
            state('collapsed', style({ height: '0px', minHeight: '0' })),
            state('expanded', style({ height: '*' })),
            transition(
                'expanded <=> collapsed',
                animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)'),
            ),
        ]),
    ],
    providers: [
        {
            provide: DateAdapter,
            useClass: MomentDateAdapter,
            deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS],
        },

        { provide: MAT_DATE_LOCALE, useValue: 'vi-VI' },
        { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
        {
            provide: MAT_MOMENT_DATE_ADAPTER_OPTIONS,
            useValue: { useUtc: true },
        },
    ],
})
export class FinancialReportComponent implements OnInit {
    dataSource = ELEMENT_DATA;
    columnsToDisplay = ['name', 'weight', 'symbol', 'position', 'description'];
    dataReport = [];
    dataReportExportExcel;
    thanhTienNgayTruoc = 0;
    thanhTienCongNhap = 0;
    thanhTienCongXuat = 0;
    danhSachThu = 0;
    danhSachThuList = [];
    listUnit = [];
    dataFinancialOnDay = [];
    initialData = new Array(19);
    currentOrganization;
    rfSearch: FormGroup;

    resultsLength = 0;
    isLoadingResults = false;
    isRateLimitReached = false;

    options: string[] = ['One', 'Two', 'Three'];
    headerTable: string[] = [
        'id',
        'tenLTTP',
        'dvt',
        'donGia',
        'soLuong',
        'thanhTien',
        'trenDBTT',
        'trenDBTG',
        'donViTT',
        'donViTg',
        'nhapTrongNgayThanhTien',
        'SoLuongCN',
        'ThanhTienCN',
        'SoLuongXA',
        'ThanhTienXA',
        'SoLuongXK',
        'ThanhTienXK',
        'SoLuongCX',
        'ThanhTienCX',
    ];
    foods: Food[] = [
        { value: 'steak-0', viewValue: 'Steak' },
        { value: 'pizza-1', viewValue: 'Pizza' },
        { value: 'tacos-2', viewValue: 'Tacos' },
    ];

    statisticals: Statistical[] = [
        {
            value: this.thanhTienNgayTruoc,
            title: 'Ngày trước chuyển qua',
            icon: ' attach_money',
        },
        {
            value: this.thanhTienCongNhap,
            title: 'Tổng nhập',
            icon: 'show_chart',
        },
        {
            value: this.thanhTienCongXuat,
            title: 'Tổng xuất',
            icon: 'show_chart',
            class: 'custom-icon',
        },
        { value: '923', title: 'Quân số ăn', icon: 'stars' },
    ];

    displayedColumns = ['item', 'cost'];
    transactions: Transaction[] = [
        { item: 'Beach ball', cost: 4 },
        { item: 'Towel', cost: 5 },
        { item: 'Frisbee', cost: 2 },
        { item: 'Sunscreen', cost: 4 },
        { item: 'Cooler', cost: 25 },
        { item: 'Swim suit', cost: 15 },
    ];

    displayedColumnsMeal = ['meal', 'cost', 'price', 'total'];
    // transactionsMeal: TransactionMeal[] = [
    //     { meal: 'Sáng', cost: 44, price: 13000 },
    //     { meal: 'Trưa', cost: 50, price: 220000 },
    //     { meal: 'Chiều', cost: 12, price: 490000 },
    // ];

    filteredOptions: Observable<string[]>;
    minDate;

    dauMoiBepId: number;

    constructor(
        private fb: FormBuilder,
        private exportExcelService: ExportExcelService,
        private commonService: CommonService,
        private _dataService: DataService,
        public _hssk: HoSoSucKhoeService,
    ) { }

    ngOnInit() {
        this.dauMoiBepId = this._dataService.getDauMoiBepId();
        this.getInitForm();
        this.getInitData();
    }

    getInitData() {
        this.getDataCurrentOrganizations();
        // this.getAllCookerHood();

        // return new Promise((resolve) => {
        //     setTimeout(() => {
        //         fetch('https://634926fd0b382d796c7ee027.mockapi.io/LTTP')
        //             .then((data) => data.json())
        //             .then((data) => {
        //                 this.dataReport = data;
        //                 resolve(this.dataReport);
        //             });
        //     }, 1000);
        // });
        // const dataCurrentOrganization = this._hssk.getCurrentOrganizations();

        this.getReportData(this.dauMoiBepId);
    }

    // getMindate() {
    //     let fromDay = this.rfSearch.get('fromDay').value;
    //     this.minDate = fromDay;
    // }

    getReportData(dauMoiBepId: number): void {
        let formValue = this.rfSearch.value;

        //Example data test: nameCook: 27 , formValue '2022-12-12'
        this.commonService
            .callDataAPIShort(
                '/api/services/read/BaoCao/GetBaoCaoCongKhaiTaiChinh',
                {
                    dauMoiBepId,
                    ngayBaoCao: formValue.fromDay,
                },
            )
            // .pipe(this.commonService.observerLoadingState())
            .subscribe(
                (data) => {
                    this.isLoadingResults = false;

                    let { result } = data;
                    this.dataReportExportExcel = result;

                    let { congKhaiTaiChinh, nhapXuatTrongNgay } = result;
                    let { danhSachThu } = congKhaiTaiChinh;
                    this.danhSachThuList =
                        this.converToTransactionsMealObj(danhSachThu);
                    if (nhapXuatTrongNgay) {
                        this.dataReport = nhapXuatTrongNgay;

                        //Change data export Excel
                        this.thanhTienNgayTruoc = this.sumInitial(
                            nhapXuatTrongNgay,
                            'thanhTienNgayTruoc',
                        );
                        this.thanhTienCongNhap = this.sumInitial(
                            nhapXuatTrongNgay,
                            'thanhTienCongNhap',
                        );
                        this.thanhTienCongXuat = this.sumInitial(
                            nhapXuatTrongNgay,
                            'thanhTienCongXuat',
                        );

                        //Change data layout
                        this.statisticals[0].value =
                            this.thanhTienNgayTruoc;
                        this.statisticals[1].value = this.thanhTienCongNhap;
                        this.statisticals[2].value = this.thanhTienCongXuat;
                        this.statisticals[3].value =
                            this.sumInitialUser(danhSachThu);
                    }
                    this.headerTable = [
                        'id',
                        'tenLttpChatDot',
                        'donViTinh',
                        'donGia',
                        'soLuongNgayTruoc',
                        'thanhTienNgayTruoc',
                        'nhapTrenBdMuaTT',
                        'nhapTrenBdTGCB',
                        'nhapDonViBdMuaTT',
                        'nhapDonViBdTGCB',
                        'thanhTienNhap',
                        'soLuongCongNhap',
                        'thanhTienCongNhap',
                        'soLuongXuatAn',
                        'thanhTienXuatAn',
                        'soLuongXuatKhac',
                        'thanhTienXuatKhac',
                        'soLuongCongXuat',
                        'thanhTienCongXuat',
                    ];
                },
                (err: HttpErrorResponse) => {
                    console.log(err);
                    this.isLoadingResults = false;

                    // alert(err.message);
                },
            );
    }

    getAllCookerHood(currentOrganizationId) {
        const _URL = '/api/services/read/DauMoiBep/GetAll';
        let body = {
            donViId: 'currentOrganizationId',
        };
        this.commonService.callDataAPIShort(_URL, body);
        // .pipe(map((res) => res.result && res.result.items))
        // .subscribe(
        //     (data) => console.log(data),
        //     (error) => console.log(error),
        // );
    }

    getDataCurrentOrganizations() {
        this._hssk
            .getCurrentOrganizations()
            .pipe(
                mergeMap((dataMerge) => {
                    let resultData = dataMerge.result[0];
                    const _URL = '/api/services/read/DauMoiBep/GetAll';
                    let id = (resultData && resultData.id) || 0;
                    let body = {
                        donViId: id,
                    };
                    return this.commonService.callDataAPIShort(_URL, body);
                }),
            )
            .pipe(map((res) => res.result && res.result.items))
            .subscribe(
                (data) => {
                    this.listUnit = data;
                },
                (error) => console.log(error),
            );
    }

    addEvent(type: string, event: MatDatepickerInputEvent<Date>) {
        console.log(event);
    }

    isCheckValue = (value) => !value;

    onRfSearchChanges() {
        this.rfSearch
            .get('fromDay')
            .valueChanges.pipe(startWith(null as string), pairwise())
            .subscribe(([prev, next]: [any, any]) => {
                if (!this.isCheckValue(next)) {
                    this.rfSearch.get('toDay').enable();
                    this.minDate = new Date(next);
                }
            });
    }

    getInitForm() {
        this.rfSearch = this.fb.group({
            nameCook: [this.dauMoiBepId, [Validators.required]],
            typeTime: '',
            unit: '',
            time: '',
            fromDay: [moment().format('YYYY-MM-DD'), [Validators.required]],
            toDay: [{ value: '', disabled: true }],
        });
    }

    convertToVND(price) {
        let currencyPipeString = this.commonService.convertToVND(price);
        return currencyPipeString;
    }

    onSubmit() {
        this.getReportData(this.dauMoiBepId);
    }
    resetForm() {
        this.rfSearch.reset();
    }

    exportExcel() {
        const formValue = this.rfSearch.value;

        let bodyDataReportExcel = {
            thoiGian: formValue['fromDay'],
            ...this.dataReportExportExcel,
            thanhTienNgayTruoc: this.thanhTienNgayTruoc,
            thanhTienCongNhap: this.thanhTienCongNhap,
            thanhTienCongXuat: this.thanhTienCongXuat,
        };

        this.exportExcelService.generateFinancialReportExcel(
            bodyDataReportExcel,
            'Bao cao tai chinh',
        );
    }

    getFinancialInfo(valueForm) {
        let data = this.rfSearch.get(valueForm).value;
        // switch (valueForm) {
        //     case 'fromDay':
        //     case 'toDay':
        //         return new Date(data).toLocaleDateString();
        //     case 'nameCook':
        //         let currentItem = this.listUnit.find(
        //             (item) => item.id === data,
        //         );
        //         return currentItem.tenBep;

        //     default:
        //         return data;
        // }
        if (valueForm === 'fromDay' || valueForm === 'toDay') {
            if (!this.isCheckValue(data)) {
                return new Date(data).toLocaleDateString();
            } else {
                return '';
            }
        } else {
            return data;
        }
    }

    getTotalCost(array) {
        return array.reduce((acc, value) => acc + value.total, 0);
    }

    sumInitial(listItem, key) {
        const initialValue = 0;
        const sumWithInitial = listItem.reduce(
            (accumulator, currentValue) => accumulator + currentValue[key],
            initialValue,
        );

        return sumWithInitial;
    }

    sumInitialUser(listItem) {
        const initialValue = 0;
        const sumWithInitial = listItem.reduce(
            (accumulator, currentValue) =>
                accumulator +
                (currentValue.thanhTienChieu +
                    currentValue.thanhTienSang +
                    currentValue.thanhTienTrua) /
                currentValue.mucTienAn,
            initialValue,
        );

        return sumWithInitial;
    }

    converToTransactionsMealObj(listItem) {
        let result = listItem.map((item) => {
            return {
                mucTienAn: item.mucTienAn,
                transactionsMeal: [
                    {
                        meal: 'Sáng',
                        cost: item.quanSoSang,
                        price: item.tienSang,
                        total: item.thanhTienSang,
                    },
                    {
                        meal: 'Trưa',
                        cost: item.quanSoTrua,
                        price: item.tienTrua,
                        total: item.thanhTienTrua,
                    },
                    {
                        meal: 'Chiều',
                        cost: item.quanSoChieu,
                        price: item.tienChieu,
                        total: item.thanhTienChieu,
                    },
                ],
            };
        });

        return [...result];
    }
}

export interface PeriodicElement {
    name: string;
    position: number;
    weight: number;
    symbol: string;
    description: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
    {
        position: 1,
        name: 'Hydrogen',
        weight: 1.0079,
        symbol: 'H',
        description: `Hydrogen is a chemical element with symbol H and atomic number 1. With a standard
        atomic weight of 1.008, hydrogen is the lightest element on the periodic table.`,
    },
    {
        position: 2,
        name: 'Helium',
        weight: 4.0026,
        symbol: 'He',
        description: `Helium is a chemical element with symbol He and atomic number 2. It is a
        colorless, odorless, tasteless, non-toxic, inert, monatomic gas, the first in the noble gas
        group in the periodic table. Its boiling point is the lowest among all the elements.`,
    },
    {
        position: 3,
        name: 'Lithium',
        weight: 6.941,
        symbol: 'Li',
        description: `Lithium is a chemical element with symbol Li and atomic number 3. It is a soft,
        silvery-white alkali metal. Under standard conditions, it is the lightest metal and the
        lightest solid element.`,
    },
    {
        position: 4,
        name: 'Beryllium',
        weight: 9.0122,
        symbol: 'Be',
        description: `Beryllium is a chemical element with symbol Be and atomic number 4. It is a
        relatively rare element in the universe, usually occurring as a product of the spallation of
        larger atomic nuclei that have collided with cosmic rays.`,
    },
    {
        position: 5,
        name: 'Boron',
        weight: 10.811,
        symbol: 'B',
        description: `Boron is a chemical element with symbol B and atomic number 5. Produced entirely
        by cosmic ray spallation and supernovae and not by stellar nucleosynthesis, it is a
        low-abundance element in the Solar system and in the Earth's crust.`,
    },
    {
        position: 6,
        name: 'Carbon',
        weight: 12.0107,
        symbol: 'C',
        description: `Carbon is a chemical element with symbol C and atomic number 6. It is nonmetallic
        and tetravalent—making four electrons available to form covalent chemical bonds. It belongs
        to group 14 of the periodic table.`,
    },
    {
        position: 7,
        name: 'Nitrogen',
        weight: 14.0067,
        symbol: 'N',
        description: `Nitrogen is a chemical element with symbol N and atomic number 7. It was first
        discovered and isolated by Scottish physician Daniel Rutherford in 1772.`,
    },
    {
        position: 8,
        name: 'Oxygen',
        weight: 15.9994,
        symbol: 'O',
        description: `Oxygen is a chemical element with symbol O and atomic number 8. It is a member of
         the chalcogen group on the periodic table, a highly reactive nonmetal, and an oxidizing
         agent that readily forms oxides with most elements as well as with other compounds.`,
    },
    {
        position: 9,
        name: 'Fluorine',
        weight: 18.9984,
        symbol: 'F',
        description: `Fluorine is a chemical element with symbol F and atomic number 9. It is the
        lightest halogen and exists as a highly toxic pale yellow diatomic gas at standard
        conditions.`,
    },
    {
        position: 10,
        name: 'Neon',
        weight: 20.1797,
        symbol: 'Ne',
        description: `Neon is a chemical element with symbol Ne and atomic number 10. It is a noble gas.
        Neon is a colorless, odorless, inert monatomic gas under standard conditions, with about
        two-thirds the density of air.`,
    },
];

interface Food {
    value: string;
    viewValue: string;
}

interface Transaction {
    item: string;
    cost: number;
}

type TransactionMeal = {
    meal: string;
    cost: number;
    price: number;
};

export type Statistical = {
    value: number | string | any;
    title: string;
    icon: string;
    class?: string;
};
