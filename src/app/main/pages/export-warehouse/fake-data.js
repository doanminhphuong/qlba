// {

//     "maPhieuXuatKho": "SochungTu3",
//     "khoNhanId": "string",
//     "khoXuatId": "string",
//     "ngayHetHan": "2022-10-04T07:46:27.457Z",
//     "lyDoSuDung": "string",
//     "maNguoiDuyet": 0,
//     "maNguoiViet": 0,
//     "maNguoiNhan": 0,
//     "maThuKho": 0,
//     "ngayGiaoNhan": "2022-10-04T07:46:27.457Z",
//     "ngayDuyet": "2022-10-04T07:46:27.457Z",
//     "trangThai": 3,
//     "chiTiet": [
//     ]
//   }

export default data = [
    {
        id: UUIDGeneratorBrowser(),
        ngayXuat: '2022-10-04T07:46:27.457Z',
        SoChungTu: '00016-20-102020',
        soLuong: '2,681',
        thanhTien: '26,710',
        nguoiXuat: 'Nguyễn Văn A',
        status: 0,
        chiTiet: [],
    },
    {
        id: UUIDGeneratorBrowser(),
        ngayXuat: '2022-10-04T07:46:27.457Z',
        SoChungTu: '00016-20-102020',
        soLuong: '2,681',
        thanhTien: '26,710',
        nguoiXuat: 'Nguyễn Văn A',
        status: 0,
        chiTiet: [],
    },
    {
        id: UUIDGeneratorBrowser(),
        ngayXuat: '2022-10-04T07:46:27.457Z',
        SoChungTu: '00016-20-102020',
        soLuong: '2,681',
        thanhTien: '26,710',
        nguoiXuat: 'Nguyễn Văn A',
        status: 1,
        chiTiet: [],
    },
    {
        id: UUIDGeneratorBrowser(),
        ngayXuat: '2022-10-04T07:46:27.457Z',
        SoChungTu: '00016-20-102020',
        soLuong: '2,681',
        thanhTien: '26,710',
        nguoiXuat: 'Nguyễn Văn A',
        status: 2,
        chiTiet: [],
    },
    {
        id: UUIDGeneratorBrowser(),
        ngayXuat: '2022-10-04T07:46:27.457Z',
        SoChungTu: '00016-20-102020',
        soLuong: '2,681',
        thanhTien: '26,710',
        nguoiXuat: 'Nguyễn Văn A',
        status: 3,
        chiTiet: [],
    },
];

const UUIDGeneratorBrowser = () =>
    ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, (c) =>
        (
            c ^
            (crypto.getRandomValues(new Uint8Array(1))[0] & (15 >> (c / 4)))
        ).toString(16),
    );
