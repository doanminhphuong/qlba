import { SelectionModel } from '@angular/cdk/collections';
import { Component, OnInit, ViewChild } from '@angular/core';
import {
    MatDialog,
    MatPaginator,
    MatTableDataSource,
    PageEvent,
} from '@angular/material';
import { Router } from '@angular/router';
import { DeleteConfirmDialogComponent } from '@app/_shared/dialogs/delete-confirm-dialog/delete-confirm-dialog.component';
import { CategoryService } from '@core/services/category.service';
import { CommonService } from '@core/services/common.service';
import { PriceService } from '@core/services/price.service';
import { ToastrService } from '@core/services/toastr.service';

export const MY_DATA = [
    {
        id: 'fdf3f0fb-74ca-7b47-29ae-08f32f8080a8',
        maDieuChinh: 'DC001',
        soChungTu: '1359/TB- HĐG',
        tenDieuChinh: 'Điều chỉnh giá TP Tháng 7',
        ngayBatDau: '2022-09-02T17:00:00.000Z',
        status: true,
    },
    {
        id: '0eab70bc-0d3e-8c27-6ea6-6583c138aa02',
        maDieuChinh: 'DC002',
        soChungTu: '1359/TB- HĐG',
        tenDieuChinh: 'Điều chỉnh giá TP Tháng 7',
        ngayBatDau: '2022-09-02T17:00:00.000Z',
        status: true,
    },
    {
        id: '9a202041-f4b5-8ef3-952f-730e28779d15',
        maDieuChinh: 'DC003',
        soChungTu: '1359/TB- HĐG',
        tenDieuChinh: 'Điều chỉnh giá TP Tháng 7',
        ngayBatDau: '2022-09-02T17:00:00.000Z',
        status: true,
    },
    {
        id: '794e51ed-1c1d-15f2-498b-42bc395da479',
        maDieuChinh: 'DC004',
        soChungTu: '1359/TB- HĐG',
        tenDieuChinh: 'Điều chỉnh giá TP Tháng 7',
        ngayBatDau: '2022-09-02T17:00:00.000Z',
        status: false,
    },
    {
        id: '27e22893-1019-2384-d54d-fd144579908d',
        maDieuChinh: 'DC005',
        soChungTu: '1359/TB- HĐG',
        tenDieuChinh: 'Điều chỉnh giá TP Tháng 7',
        ngayBatDau: '2022-09-02T17:00:00.000Z',
        status: false,
    },
];

@Component({
    selector: 'app-price-management',
    templateUrl: './price-management.component.html',
    styleUrls: ['./price-management.component.scss'],
})
export class PriceManagementComponent implements OnInit {
    @ViewChild(MatPaginator) paginator!: MatPaginator;

    displayedColumnsOfField: string[] = [
        'maDotDieuChinhGia',
        'tenDotDieuChinhGia',
        'ngayHieuLuc',
        'status',
        'actions',
    ];

    dataSource = new MatTableDataSource<any>([]);
    selection = new SelectionModel<any>(true, []);

    adjustmentList: any[] = [];

    fieldSearch: any[] = [
        {
            type: 'DATETIME',
            referenceValue: 'ngayHieuLuc',
            name: 'Ngày áp dụng',
            defaultValue: '',
            required: '0',
            css: 'col-12 col-lg-6',
            appearance: 'legacy',
        },
        {
            type: 'SELECT',
            referenceValue: 'status',
            name: 'Trạng thái',
            defaultValue: '',
            required: '0',
            options: [
                { key: 'ENABLE', name: 'Hoạt động' },
                { key: 'DISABLE', name: 'Tạm ngưng' },
            ],
            css: 'col-12 col-lg-6',
            appearance: 'legacy',
        },
    ];

    // Pagination
    totalRows = 0;
    pageSize = 10;
    currentPage = 0;
    pageSizeOptions: number[] = [5, 10, 25, 50];
    dataSkipped: number = 0;

    isFiltering: boolean = false;

    allowExec: boolean = false;

    constructor(
        private _router: Router,
        public dialog: MatDialog,
        private _toastrService: ToastrService,
        private _categoryService: CategoryService,
        private _priceService: PriceService,
        private _commonService: CommonService,
    ) {
        this._commonService.checkPermission2().subscribe(
            (response) => {
                if (!response.result) return;

                const permissions = JSON.parse(response.result);
                console.log('permissions:: ', permissions);
                this.allowExec = permissions.some((obj) => obj.key === 'EXEC');

                if (!this.allowExec) {
                    this.displayedColumnsOfField.splice(4, 1);
                }
            },
            (error) => {
                this.displayedColumnsOfField.splice(4, 1);
            },
        );
    }

    ngOnInit() {
        this.getInitData();
    }

    ngAfterViewInit() {
        this.dataSource.paginator = this.paginator;
    }

    getInitData(): void {
        const body = {
            maxResultCount: this.pageSize,
            skipCount: this.dataSkipped,
            sorting: 'creationTime ASC',
        };

        this._priceService.getAllPriceAdjustment(body).subscribe((res) => {
            this.adjustmentList = res.result.items;
            this.dataSource.data = this.adjustmentList;
            setTimeout(() => {
                this.paginator.pageIndex = this.currentPage;
                this.paginator.length = res.result.totalCount;
            });
        });
    }

    pageChanged(event: PageEvent) {
        this.currentPage = event.pageIndex;
        this.dataSkipped = event.pageIndex * 10;

        this.getInitData();
    }

    /** Whether the number of selected elements matches the total number of rows. */
    isAllSelected() {
        const numSelected = this.selection.selected.length;
        const numRows = this.dataSource.data.length;
        return numSelected === numRows;
    }

    /** Selects all rows if they are not all selected; otherwise clear selection. */
    toggleAllRows() {
        if (this.isAllSelected()) {
            this.selection.clear();
            return;
        }

        this.selection.select(...this.dataSource.data);
    }

    /** The label for the checkbox on the passed row */
    checkboxLabel(row?: any): string {
        if (!row) {
            return `${this.isAllSelected() ? 'deselect' : 'select'} all`;
        }
        return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${
            row.index + 1
        }`;
    }

    createAction(): void {
        this._router.navigate(
            ['manage/app/category/gia-hoi-dong-gia/create'],
            // {
            //   queryParams: {
            //     RecentlyAdjustment: this.adjustmentList.length > 0 ? this.adjustmentList[0].id : null,
            //   },
            // },
        );
    }

    editAction(element: any): void {
        this._router.navigate([
            `manage/app/category/gia-hoi-dong-gia/edit/${element.id}`,
        ]);
    }

    deleteAction(element: any): void {
        const dialogRef = this.dialog.open(DeleteConfirmDialogComponent, {
            width: '500px',
            data: element,
        });

        dialogRef.componentInstance.handleDelete.subscribe((dataDelete) => {
            const isSubmitted = !!dataDelete;

            this._priceService.deletePriceAdjustment(dataDelete).subscribe(
                (res) => dialogRef.close(isSubmitted),
                (err) => this._toastrService.errorServer(err),
            );
        });

        dialogRef.afterClosed().subscribe((result) => {
            if (!result) return;

            this.getInitData();
            this._toastrService.success('', 'Xóa thành công');
        });
    }

    // HANDLE SEARCH
    handleSearch(filterValue: string): void {
        if (!filterValue) {
            this.isFiltering = false;
            this.getInitData();
        } else {
            const bodyFilter = {
                maxResultCount: 9999,
                skipCount: 0,
                sorting: 'Id',
                criterias: [
                    {
                        propertyName: 'tenDotDieuChinhGia',
                        operation: 6,
                        value: filterValue.trim().toLowerCase(),
                    },
                ],
            };

            this._priceService
                .getAllPriceAdjustment(bodyFilter)
                .subscribe((res) => {
                    this.isFiltering = true;
                    this.dataSource.data = res.result.items;
                });
        }
    }

    handleSearchAdvanced(dataSearchAdvanced: any) {
        if (dataSearchAdvanced.key === 'reset') {
            this.getInitData();
        } else {
            let bodySubmit = {
                maxResultCount: this.pageSize,
                skipCount: this.dataSkipped,
                criterias: [
                    {
                        propertyName: dataSearchAdvanced.ngayHieuLuc
                            ? 'ngayHieuLuc'
                            : '',
                        operation: dataSearchAdvanced.ngayHieuLuc
                            ? 'Equals'
                            : 'Contains',
                        value: dataSearchAdvanced.ngayHieuLuc
                            ? new Date(
                                  dataSearchAdvanced.ngayHieuLuc._d,
                              ).toISOString()
                            : '',
                    },
                    {
                        propertyName:
                            dataSearchAdvanced.status !== null &&
                            dataSearchAdvanced.status !== ''
                                ? 'status'
                                : '',
                        operation:
                            dataSearchAdvanced.status !== null &&
                            dataSearchAdvanced.status !== ''
                                ? 'Equals'
                                : 'Contains',
                        value:
                            dataSearchAdvanced.status !== null &&
                            dataSearchAdvanced.status !== ''
                                ? dataSearchAdvanced.status
                                : '',
                    },
                ],
            };

            this._priceService
                .getAllPriceAdjustment(bodySubmit)
                .subscribe((res) => {
                    this.adjustmentList = res.result.items;
                    this.dataSource.data = this.adjustmentList;
                    setTimeout(() => {
                        this.paginator.pageIndex = this.currentPage;
                        this.paginator.length = res.result.totalCount;
                    });
                });
        }
    }
}
