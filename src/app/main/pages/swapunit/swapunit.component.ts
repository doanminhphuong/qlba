import { SelectionModel } from '@angular/cdk/collections';
import { Component, OnInit, ViewChild } from '@angular/core';
import {
    MatDialog,
    MatPaginator,
    MatTableDataSource,
    PageEvent,
} from '@angular/material';
import { Router } from '@angular/router';
import { DynamicDialogComponent } from '@app/components/dynamic-dialog-component/dynamic-dialog-component.component';
import { DeleteConfirmDialogComponent } from '@app/_shared/dialogs/delete-confirm-dialog/delete-confirm-dialog.component';
import { CategoryService } from '@core/services/category.service';
import { ToastrService } from '@core/services/toastr.service';
import { UserService } from '@core/services/user.service';
import { forkJoin } from 'rxjs';
import { FieldService } from '@core/services/field.service';
import { SwapunitService } from '@core/services/swapunit.service';
import { CommonService } from '@core/services/common.service';

@Component({
    selector: 'app-swapunit',
    templateUrl: './swapunit.component.html',
    styleUrls: ['./swapunit.component.scss'],
})
export class SwapunitComponent implements OnInit {
    @ViewChild(MatPaginator) paginator!: MatPaginator;

    displayedColumns: string[] = ['from', 'to', 'rate', 'actions'];

    currentDate: Date = new Date();

    displayedColumnsOfField: string[] = ['from', 'to', 'rate', 'actions'];

    dataSource = new MatTableDataSource<any>([]);
    selection = new SelectionModel<any>(true, []);
    userList: any[] = [];
    dataList: any;
    inventoryList: any[] = [];
    totalPrice: number = 0;
    currentTable: any[] = [];
    swapunitOptions: any[] = [];
    typeOptions: any[] = [];
    unitOptions: any[] = [];
    valueData: string;

    // Pagination
    totalRows = 0;
    pageSize = 10;
    currentPage = 0;
    pageSizeOptions: number[] = [5, 10, 25, 50];
    dataSkipped: number = 0;

    typeDialog: string;
    isFiltering: boolean = false;
    indexUnit: number;
    allowExec: boolean = false;
    userOrganization: any;

    constructor(
        private _router: Router,
        public dialog: MatDialog,
        private _fieldService: FieldService,
        private _toastrService: ToastrService,
        private _categoryService: CategoryService,
        private _swapunitService: SwapunitService,
        private _userService: UserService,
        private _commonService: CommonService,
    ) {
        let request = {
            id: localStorage.getItem('idMenu'),
            language: 'vi',
        };

        this._commonService.checkPermission(request).subscribe((response) => {
            if (JSON.parse(response.result).some((obj) => obj.key === 'EXEC')) {
                this.allowExec = true;
            } else {
                this.displayedColumnsOfField.splice(3, 1);
            }

            this.getInitData();
        });
    }

    ngOnInit() {
        this.getInitData();
        this.getMultiCategories();
    }

    ngAfterViewInit() {
        this.dataSource.paginator = this.paginator;
    }

    getInitData(): void {
        this._userService.getUserOrganization().subscribe((response) => {
            this.userOrganization = response.result[0].id;

            this.getAllSwapunit();
        });
    }

    getAllSwapunit(): void {
        const bodySwapunit = {
            maxResultCount: this.pageSize,
            skipCount: this.dataSkipped,
            donViId: this.userOrganization,
        };

        this._swapunitService.getAllSwapunit(bodySwapunit).subscribe((res) => {
            this.dataSource.data = res.result.items;
            // if (this.dataSource.data.length > 0) {
            //     this.indexUnit =
            //         this.dataSource.data[this.dataSource.data.length - 1].index;
            // } else {
            //     this.indexUnit = 0;
            // }
            setTimeout(() => {
                this.paginator.pageIndex = this.currentPage;
                this.paginator.length = res.result.totalCount;
            });
        });
    }

    pageChanged(event: PageEvent) {
        this.currentPage = event.pageIndex;
        this.dataSkipped = event.pageIndex * 10;

        this.getAllSwapunit();
    }

    getMultiCategories() {
        this.getUnitOptions();
        this.getTypeOptions();
    }

    getBodyCategory(groupCode: string) {
        return {
            maxResultCount: 10,
            skipCount: 0,
            sorting: 'Code',
            criterias: [
                {
                    propertyName: 'GroupCode',
                    operation: 6,
                    value: groupCode,
                },
            ],
        };
    }

    getUnitOptionsbyId(id) {
        let result = '';
        if (this.unitOptions.length > 0) {
            let currentUnit = this.unitOptions.find((item) => item.key === id);
            result = currentUnit ? currentUnit.name : '';
        }
        return result;
    }

    getUnitOptions() {
        this._categoryService
            .getAllCategory(this.getBodyCategory('don-vi-tinh'))
            .subscribe((res) => {
                this.unitOptions = res.result.map((item) => ({
                    key: item.id,
                    name: item.name,
                }));
            });
    }

    getTypeOptions() {
        this._categoryService
            .getAllCategory(this.getBodyCategory('don-vi-tinh'))
            .subscribe((res) => {
                this.typeOptions = res.result.map((item) => ({
                    key: item.codeData,
                    name: item.name,
                }));
            });
    }

    guid = () => {
        let s4 = () => {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        };
        //return id of format 'aaaaaaaa'-'aaaa'-'aaaa'-'aaaa'-'aaaaaaaaaaaa'
        return (
            s4() +
            s4() +
            '-' +
            s4() +
            '-' +
            s4() +
            '-' +
            s4() +
            '-' +
            s4() +
            s4() +
            s4()
        );
    };

    openDialog(dataDialog: any): void {
        let fields = [];
        this.typeDialog = 'swapunit';
        fields = [
            {
                type: 'SELECT',
                referenceValue: 'from',
                name: 'Đơn vị tính cơ bản',
                defaultValue: '',
                required: '1',
                css: 'col-12 col-lg-4',
                options: [...this.unitOptions],
            },
            {
                type: 'SELECT',
                referenceValue: 'to',
                name: 'Đơn vị quy đổi',
                defaultValue: '',
                required: '1',
                css: 'col-12 col-lg-4',
                options: [...this.unitOptions],
            },
            {
                type: 'NUMBER',
                referenceValue: 'rate',
                name: 'Giá trị quy đổi',
                defaultValue: '',
                required: '1',
                css: 'col-12 col-lg-4',
            },
        ];
        // DialogSwapunitComponent
        let dialogRef = this.dialog.open(DynamicDialogComponent, {
            // disableClose: true,
            width: '70%',
            data: {
                datas: dataDialog,
                fields: fields,
                typeDialog: this.typeDialog,
            },
            panelClass: 'my-custom-dialog-class',
        });

        dialogRef.componentInstance.onSave.subscribe((formValues) => {
            const isSubmitted = !!formValues;
            let dataSubmit = {
                ...dataDialog,
                ...formValues,
            };

            if (dataSubmit.from === dataSubmit.to) {
                this._toastrService.error(
                    '',
                    'Không được chọn trùng đơn vị quy đổi',
                );
            } else {
                // Handle dialog for Category
                if (!dataDialog) {
                    this._swapunitService
                        .createSwapunit({
                            ...dataSubmit,
                            donViId: this.userOrganization,
                        })
                        .subscribe(
                            (res) => dialogRef.close(isSubmitted),
                            (err) => this._toastrService.errorServer(err),
                        );
                } else {
                    this._swapunitService.updateSwapunit(dataSubmit).subscribe(
                        (res) => dialogRef.close(isSubmitted),
                        (err) => this._toastrService.errorServer(err),
                    );
                }
            }
        });

        dialogRef.afterClosed().subscribe((result) => {
            if (!result) return;

            this.getInitData();

            if (!dataDialog) {
                this._toastrService.success('', 'Thêm thành công');
            } else {
                this._toastrService.success('', 'Cập nhật thành công');
            }
        });
    }

    editAction(element: any): void {
        this.openDialog(element);
    }

    createAction(): void {
        this.openDialog(null);
    }

    deleteAction(element: any): void {
        const dialogRef = this.dialog.open(DeleteConfirmDialogComponent, {
            width: '500px',
            data: element,
        });

        dialogRef.componentInstance.handleDelete.subscribe((dataDelete) => {
            const isSubmitted = !!dataDelete;

            this._swapunitService
                .deleteSwapunit({
                    quyDoiDonViId: dataDelete.id,
                    donViId: this.userOrganization,
                })
                .subscribe(
                    (res) => dialogRef.close(isSubmitted),
                    (err) => this._toastrService.errorServer(err),
                );
        });

        dialogRef.afterClosed().subscribe((result) => {
            if (!result) return;

            this.getInitData();
            this._toastrService.success('', 'Xóa thành công');
        });
    }
}
