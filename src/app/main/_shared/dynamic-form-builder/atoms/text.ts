import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
    selector: 'fb-text',
    template: `
        <div [formGroup]="form">
            <ng-container
                *ngIf="!isFlatForm && tabCtrl"
                [formGroupName]="tabCtrl"
            >
                <ng-container [formGroupName]="groupCtrl">
                    <mat-form-field
                        [appearance]="field.appearance || 'outline'"
                    >
                        <mat-label>{{ field.name }}</mat-label>
                        <input
                            matInput
                            type="text"
                            [placeholder]="getPlaceholder()"
                            [value]="field.defaultValue"
                            [id]="field.referenceValue"
                            [name]="field.referenceValue"
                            [formControlName]="getFormControlName()"
                            [required]="field.required === '1' ? true : false"
                            spellcheck="false"
                            [pattern]="field.pattern"
                            [maxLength]="
                                field.maxLength ? field.maxLength : 999
                            "
                        />
                        <mat-icon matSuffix>{{ field.icon }}</mat-icon>
                        <!-- VALIDATORS -->
                        <mat-error>{{ getErrorMessage() }}</mat-error>
                    </mat-form-field>
                </ng-container>
            </ng-container>

            <mat-form-field
                *ngIf="isFlatForm || !tabCtrl"
                [appearance]="field.appearance || 'outline'"
            >
                <mat-label>{{ field.name }}</mat-label>
                <input
                    matInput
                    type="text"
                    [placeholder]="getPlaceholder()"
                    [value]="field.defaultValue"
                    [id]="field.referenceValue"
                    [name]="field.referenceValue"
                    [formControlName]="getFormControlName()"
                    [required]="field.required === '1' ? true : false"
                    spellcheck="false"
                    [readonly]="field.readonly"
                    [pattern]="field.pattern"
                    [maxLength]="field.maxLength ? field.maxLength : 999"
                    [disabled]="field.disabled"
                />
                <mat-icon matSuffix>{{ field.icon }}</mat-icon>
                <!-- VALIDATORS -->
                <mat-error>{{ getErrorMessage() }}</mat-error>
            </mat-form-field>
        </div>
    `,
    styles: [
        `
            mat-form-field {
                width: 100%;
            }
        `,
    ],
})
export class TextComponent implements OnInit {
    @Input() field: any = {};
    @Input() form: FormGroup;
    @Input() tabCtrl: any;
    @Input() groupCtrl: any;
    @Input() isFlatForm: boolean;

    get fieldName() {
        if (!this.isFlatForm && this.tabCtrl) {
            return this.form
                .get(this.tabCtrl)
                .get(this.groupCtrl)
                .get(this.field.referenceValue);
        } else if (this.isFlatForm && this.tabCtrl) {
            return this.form.controls[
                `${this.tabCtrl}.${this.groupCtrl}.${this.field.referenceValue}`
            ];
        } else {
            return this.form.controls[this.field.referenceValue];
        }
    }

    get isValid() {
        return this.fieldName.valid;
    }
    get isDirty() {
        return this.fieldName.dirty;
    }
    get isTouched() {
        return this.fieldName.touched;
    }

    constructor() {}

    ngOnInit() {}

    getPlaceholder() {
        return `Nhập vào ${this.field.name.toLowerCase()}`;
    }

    getFormControlName() {
        if (this.isFlatForm) {
            return `${this.tabCtrl}.${this.groupCtrl}.${this.field.referenceValue}`;
        } else {
            return this.field.referenceValue;
        }
    }

    getErrorMessage() {
        let errorMessage = '';

        if (this.fieldName.hasError('required')) {
            errorMessage = `${this.field.name} không được để trống.`;
        } else if (this.fieldName.hasError('minlength')) {
            errorMessage = 'Phải nhập tối thiểu 5 ký tự';
        } else if (this.fieldName.hasError('maxlength')) {
            errorMessage = 'Nhập quá số ký tự cho phép.';
        } else if (this.fieldName.hasError('pattern')) {
            errorMessage = `${this.field.name} không hợp lệ.`;
        }

        return errorMessage;
    }
}
