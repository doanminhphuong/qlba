import { E, S } from '@angular/cdk/keycodes';
import { Component, OnInit } from '@angular/core';
import { MatDialog, MatTableDataSource } from '@angular/material';
import { DeleteConfirmDialogComponent } from '@app/_shared/dialogs/delete-confirm-dialog/delete-confirm-dialog.component';
import { DialogCookManagerComponent } from '@app/_shared/dialogs/dialog-cook-manager/dialog-cook-manager.component';
import { CommonService } from '@core/services/common.service';
import { ToastrService } from '@core/services/toastr.service';
import { UserService } from '@core/services/user.service';
import { forkJoin } from 'rxjs';
import { map, filter, mergeMap } from 'rxjs/operators';

export interface PeriodicElement {
    name: string;
    position: number;
    weight: number;
    symbol: string;
}

export interface Cook {
    id: number;
    code: string;
    name: string;
    unitManager: string;
    status: boolean;
}

const COOK_DATA: Cook[] = [
    {
        id: 0,
        code: 'TSQ.BepAnN0',
        name: 'Bếp ăn N0',
        unitManager: 'Tiểu đoàn 30',
        status: true,
    },
    {
        id: 1,
        code: 'TSQ.BepAnN1',
        name: 'Bếp ăn N1',
        unitManager: 'Tiểu đoàn 30',
        status: true,
    },
    {
        id: 2,
        code: 'TSQ.BepAnN2',
        name: 'Bếp ăn N2',
        unitManager: 'Tiểu đoàn 30',
        status: true,
    },
    {
        id: 3,
        code: 'TSQ.BepAnN3',
        name: 'Bếp ăn N3',
        unitManager: 'Tiểu đoàn 30',
        status: true,
    },
    {
        id: 4,
        code: 'TSQ.BepAnN4',
        name: 'Bếp ăn N4',
        unitManager: 'Tiểu đoàn 30',
        status: true,
    },
    {
        id: 5,
        code: 'TSQ.BepAnN5',
        name: 'Bếp ăn N5',
        unitManager: 'Tiểu đoàn 30',
        status: true,
    },
    {
        id: 6,
        code: 'TSQ.BepAnN6',
        name: 'Bếp ăn N6',
        unitManager: 'Tiểu đoàn 30',
        status: true,
    },
];

@Component({
    selector: 'app-cook-manager',
    templateUrl: './cook-manager.component.html',
    styleUrls: ['./cook-manager.component.scss'],
})
export class CookManagerComponent implements OnInit {
    displayedColumns: string[] = [
        'tenBep',
        'donViThuocQuanLy',
        'trangThai',
        'actions',
    ];
    dataCook = new MatTableDataSource<Cook>([]);
    currentUnitUser;
    listUserOnUnit;
    listChildOrganization;
    totalUserOnCook = 0;
    allowExec: boolean = false;
    // dataCook: Cook[] = [];
    // dataTable: = []
    constructor(
        public dialog: MatDialog,
        private toastrService: ToastrService,
        private commonService: CommonService,
        private userService: UserService,
    ) {
        // let request = {
        //     id: localStorage.getItem('idMenu'),
        //     language: 'vi',
        // };
        // this.commonService.checkPermission(request).subscribe((response) => {
        //     console.log('response.result', response);
        //     if (JSON.parse(response.result).some((obj) => obj.key === 'EXEC')) {
        //         this.allowExec = true;
        //     } else {
        //         this.displayedColumns.splice(3, 1);
        //     }
        // });
    }

    ngOnInit() {
        this.userService.getCurrentUsers().subscribe((response) => {
            this.getInitData(response.result.id);
        });
    }

    getInitData(userId: number) {
        let getUserOrganization = this.userService
            .getUserOrganization()
            .pipe(
                map((data) =>
                    data.result.filter((item) =>
                        item.groupCode.toLowerCase().includes('donvi'),
                    ),
                ),
            )
            .pipe(
                mergeMap((user) => {
                    this.currentUnitUser = user[0];

                    let bodySubmit = {
                        tenantId: 1,
                        userId: userId,
                    };

                    let bodySubmitCook = {
                        donViId: this.currentUnitUser.id,
                    };

                    let GetDataUnitId =
                        this.userService.getChildOrganization(bodySubmit);

                    let GetDataCook = this.commonService.callDataAPIShort(
                        '/api/services/read/DauMoiBep/GetAll',
                        bodySubmitCook,
                    );
                    return forkJoin([GetDataUnitId, GetDataCook]);
                }),
            );

        getUserOrganization.subscribe((data) => {
            const { result: resultOrganization } = data[0];
            const { result: resultUser } = data[1];

            this.listChildOrganization = resultOrganization.filter(
                (item) => item.groupCode === 'DonVi',
            );

            // const { valueData } = resultOrganization;
            this.totalUserOnCook = resultUser.totalCount;
            // this.listUserOnUnit = JSON.parse(valueData);
            this.dataCook.data = resultUser.items;
        });
        // return new Promise((resolve) => {
        //     setTimeout(() => {
        //         this.dataCook.data = COOK_DATA;
        //         resolve(this.dataCook);
        //     }, 3000);
        // });
    }

    getUnititalCook() {
        let bodySubmitCook = {
            donViId: this.currentUnitUser.id,
        };

        this.commonService
            .callDataAPIShort(
                '/api/services/read/DauMoiBep/GetAll',
                bodySubmitCook,
            )
            .subscribe((data) => (this.dataCook.data = data.result.items));
    }

    convertIdToName(id: any) {
        let result = '';
        if (this.listChildOrganization.length > 0) {
            let itemFinded = this.listChildOrganization.find(
                (item) => item.id === id,
            );
            result = itemFinded && itemFinded.name ? itemFinded.name : '';
        }

        return result;
    }

    createAction() {
        let initialData = {
            id: '',
            code: '',
            name: '',
            unitManager: '',
            status: [true],
            stocker: '',
            unitHead: '',
            assistant: '',
            manager: '',
        };
        this.openDialogAction(initialData, 'ADD', 'Tạo thông tin bếp mới');
    }

    editAction(element) {
        this.openDialogAction(element, 'EDIT', 'Sửa thông tin bếp ăn');
    }

    deleteAction(element: any): void {
        const dialogRef = this.dialog.open(DeleteConfirmDialogComponent, {
            width: '500px',
            data: element,
        });

        dialogRef.componentInstance.handleDelete.subscribe((dataDelete) => {
            const isSubmitted = !!dataDelete;

            this.commonService
                .callDataAPIShort('/api/services/write/DauMoiBep/Delete', {
                    id: element.id,
                })
                .subscribe(
                    (res) => {
                        this.getUnititalCook();
                        dialogRef.close(isSubmitted);
                    },
                    (err) => this.toastrService.errorServer(err),
                );
        });

        dialogRef.afterClosed().subscribe((result) => {
            if (!result) return;

            this.toastrService.success('', 'Xóa thành công');
        });
    }

    openDialogAction(dataDialogValue, type, title): void {
        let dataDialog = {
            title,
            dataDialogValue,
            currentUnitUser: this.currentUnitUser,
            listChildOrganization: this.listChildOrganization,
            listUserOnUnit: this.listChildOrganization,
            type,
        };
        const dialogRef = this.dialog.open(DialogCookManagerComponent, {
            width: '50%',
            data: dataDialog,
            panelClass: 'add-field-dialog-class',
        });

        dialogRef.afterClosed().subscribe((result) => {
            // console.log('The dialog was closed', result);
            // this.animal = result;
        });

        dialogRef.componentInstance.onSave.subscribe((formValues) => {
            let { dataDialog, type } = formValues;
            const isSubmitted = !!formValues;

            if (type === 'ADD') {
                // let newList = [...this.dataCook.data, dataDialog];
                // this.dataCook.data = newList;
                if (localStorage.getItem('isNewCook') === 'true') {
                    this.commonService
                        .callDataAPIShort(
                            '/api/services/write/DauMoiBep/Create',
                            dataDialog,
                        )
                        .subscribe((data) => {
                            localStorage.removeItem('isNewCook');
                            window.location.reload();
                        });
                } else {
                    this.commonService
                        .callDataAPIShort(
                            '/api/services/write/DauMoiBep/Create',
                            dataDialog,
                        )
                        .subscribe((data) => this.getUnititalCook());
                }
            }
            if (type === 'EDIT') {
                this.commonService
                    .callDataAPIShort(
                        '/api/services/write/DauMoiBep/Update',
                        dataDialog,
                    )
                    .subscribe((data) => this.getUnititalCook());
            }
        });
    }
}
