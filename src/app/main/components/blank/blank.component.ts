import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-blank',
    templateUrl: './blank.component.html',
    styleUrls: ['./blank.component.scss'],
})
export class BlankComponent implements OnInit {
    constructor(private _activatedroute: ActivatedRoute) {}

    ngOnInit() {
        this._activatedroute.paramMap.subscribe((params) => {
            console.log(params);
        });
    }
}
