import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogEstimateFoodComponent } from './dialog-estimate-food.component';

describe('DialogEstimateFoodComponent', () => {
    let component: DialogEstimateFoodComponent;
    let fixture: ComponentFixture<DialogEstimateFoodComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [DialogEstimateFoodComponent],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(DialogEstimateFoodComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
