import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BASE_URL } from './data-service';
import { TokenStorageService } from './token.service';

@Injectable({
    providedIn: 'root',
})
export class PhieuXuatKhoService {
    constructor(
        private http: HttpClient,
        private _tokenStorageService: TokenStorageService,
    ) {}

    getAllPhieuXuatKho(data: any): Observable<any> {
        const END_POINT = '/read/PhieuXuatKho/GetAll';

        return this.http.post<any>(`${BASE_URL}${END_POINT}`, data);
    }

    getAllPhieuXuatKhoOrigin(data: any): Observable<any> {
        let token = this._tokenStorageService.getToken();
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
        });

        let options = { headers: headers };
        const END_POINT = '/read/PhieuXuatKho/GetAll';

        return this.http.post<any>(`${BASE_URL}${END_POINT}`, data, options);
    }

    createPhieuXuatKho(data: any): Observable<any> {
        const END_POINT = '/write/PhieuXuatKho/Create';

        return this.http.post<any>(`${BASE_URL}${END_POINT}`, data);
    }

    updatePhieuXuatKho(data: any): Observable<any> {
        const END_POINT = '/write/PhieuXuatKho/Update';

        return this.http.post<any>(`${BASE_URL}${END_POINT}`, data);
    }

    getPhieuXuatKhoById(id: any): Observable<any> {
        const END_POINT = '/read/PhieuXuatKho/Get';

        return this.http.post<any>(`${BASE_URL}${END_POINT}`, id);
    }

    deletePhieuXuatKho(id: string): Observable<any> {
        const END_POINT = '/write/PhieuXuatKho/Delete';

        return this.http.post<any>(`${BASE_URL}${END_POINT}`, id);
    }
}
