import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MaintenanceBeforeComponent } from './maintenance-before.component';

describe('MaintenanceBeforeComponent', () => {
  let component: MaintenanceBeforeComponent;
  let fixture: ComponentFixture<MaintenanceBeforeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MaintenanceBeforeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MaintenanceBeforeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
