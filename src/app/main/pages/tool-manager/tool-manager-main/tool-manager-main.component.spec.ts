import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ToolManagerMainComponent } from './tool-manager-main.component';

describe('ToolManagerMainComponent', () => {
  let component: ToolManagerMainComponent;
  let fixture: ComponentFixture<ToolManagerMainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ToolManagerMainComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ToolManagerMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
