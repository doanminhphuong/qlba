import { NgModule } from "@angular/core";
import { HTTP_INTERCEPTORS } from "@angular/common/http";
import { AbpHttpInterceptor } from "@abp/abpHttpInterceptor";

// import { Interceptor } from "@core/models/interceptor";

import * as CmsApiServiceProxies from "./cms-service-proxies";

@NgModule({
  providers: [
    //cms
    CmsApiServiceProxies.PostClientServiceProxy,

    { provide: HTTP_INTERCEPTORS, useClass: AbpHttpInterceptor, multi: true }
    // { provide: HTTP_INTERCEPTORS, useClass: Interceptor, multi: true }
  ]
})
export class ServiceProxyModule {}
