import {
    AfterViewChecked,
    AfterViewInit,
    ChangeDetectorRef,
    Component,
    EventEmitter,
    Inject,
    OnInit,
} from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import {
    MatDialog,
    MatDialogRef,
    MAT_DIALOG_DATA,
    MAT_FORM_FIELD_DEFAULT_OPTIONS,
} from '@angular/material';
import {
    DialogData,
    DynamicDialogComponent,
} from '@app/components/dynamic-dialog-component/dynamic-dialog-component.component';

@Component({
    selector: 'app-eater-info-dialog',
    templateUrl: './eater-info-dialog.component.html',
    styleUrls: ['./eater-info-dialog.component.scss'],
    providers: [
        {
            provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
            useValue: { floatLabel: 'always' },
        },
    ],
})
export class EaterInfoDialogComponent
    implements OnInit, AfterViewInit, AfterViewChecked
{
    onSave = new EventEmitter();
    form: FormGroup;
    constructor(
        public dialog: MatDialog,
        public dialogRef: MatDialogRef<DynamicDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: DialogData,
        private readonly changeDetectorRef: ChangeDetectorRef,
    ) {}

    ngOnInit() {
        this.getInitForm();
    }

    ngAfterViewInit(): void {
        this.getInitData();
    }

    ngAfterViewChecked(): void {
        this.changeDetectorRef.detectChanges();
    }

    getInitForm() {
        let fieldsCtrls = {};

        for (let f of this.data.fields) {
            if (f.type === 'NUMBER') {
                fieldsCtrls[f.referenceValue] = new FormControl(
                    { value: f.defaultValue, disabled: f.disabled },
                    f.required === '1' ? [Validators.required] : [],
                );
            } else if (f.type !== 'CHECKBOX') {
                let validators = [
                    Validators.minLength(f.minLength),
                    Validators.maxLength(f.maxLength),
                    Validators.pattern(f.pattern),
                ];

                if (f.type === 'EMAIL') {
                    validators = [...validators, Validators.email];
                }

                fieldsCtrls[f.referenceValue] = new FormControl(
                    { value: f.defaultValue, disabled: f.disabled },
                    f.required === '1'
                        ? [...validators, Validators.required]
                        : [...validators],
                );
            } else {
                //if checkbox, it need multiple
                let opts = {};
                for (let opt of f.options) {
                    opts[opt.key] = new FormControl({
                        value: opt.value,
                        disabled: f.disabled,
                    });
                }
                fieldsCtrls[f.referenceValue] = new FormGroup(opts);
            }
        }

        this.form = new FormGroup(fieldsCtrls);
    }

    getInitData(): void {
        let dataEdit = this.data.datas;
        if (!dataEdit) return;

        this.form.patchValue(dataEdit);
    }

    getFields() {
        return this.data.fields;
    }

    replaceWhiteSpace(): void {
        for (let f of this.data.fields) {
            if (f.type === 'TEXT' || f.type === 'TEXTAREA') {
                const initField = this.form.get(f.referenceValue);

                if (initField.value === null) {
                    initField.setValue('');
                } else {
                    const valueAfterReplace = initField.value
                        .replace(/\s+/g, ' ')
                        .trim();
                    initField.setValue(valueAfterReplace);
                }
            }
        }
    }

    onCloseDialog(data?: any): void {
        this.dialogRef.close(data);
    }

    onSubmit(): void {
        this.replaceWhiteSpace();

        let formValues = this.form.value;
        this.onSave.emit(formValues);
    }
}
