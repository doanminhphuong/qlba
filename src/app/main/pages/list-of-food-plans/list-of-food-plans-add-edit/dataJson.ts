export interface GroupFood {
    name: string;
    position: number;
    weight: number;
    unit: number;
    foodLevel: FoodLevel[];
}
export interface GroupFoodUpgrade {
    id: number | string;
    name: string;
    unit: number | string;
    unitPrice: number | string;
    store: string;
    currentTotal: number | string;
    expTotal: number | string;
    totalMoney: number | string;
    createdAt: string;
}

export interface FoodLevel {
    title: string;
    totalUnit: number;
    totalPerson: number;
}

export interface Unit {
    index: number;
    name: string;
    codeData: string;
}

export interface PriceUserDetail {
    time: string;
    totalUser: string | number;
    price: string | number;
}

export interface TotalPriceUser {
    name: string;
    priceRate: string | number;
    detailt: PriceUserDetail[];
}

export const GROUP_FOOD: GroupFood[] = [
    {
        position: 1,
        name: 'Thịt xô lọc',
        weight: 1.0079,
        unit: 1,
        foodLevel: [
            {
                title: 'Mức ăn - 65000',
                totalUnit: 10,
                totalPerson: 100,
            },
            {
                title: 'Mức ăn - 81000',
                totalUnit: 10,
                totalPerson: 100,
            },
            {
                title: 'Mức ăn - 85000',
                totalUnit: 10,
                totalPerson: 100,
            },
        ],
    },

    {
        position: 2,
        name: 'Rau xanh',
        weight: 1.0079,
        unit: 2,
        foodLevel: [
            {
                title: 'Mức ăn - 65000',
                totalUnit: 10,
                totalPerson: 100,
            },
            {
                title: 'Mức ăn - 81000',
                totalUnit: 10,
                totalPerson: 100,
            },
            {
                title: 'Mức ăn - 85000',
                totalUnit: 10,
                totalPerson: 100,
            },
        ],
    },
    {
        position: 3,
        name: 'Nước mắm',
        weight: 1.0079,
        unit: 3,
        foodLevel: [
            {
                title: 'Mức ăn - 65000',
                totalUnit: 10,
                totalPerson: 100,
            },
            {
                title: 'Mức ăn - 81000',
                totalUnit: 10,
                totalPerson: 100,
            },
            {
                title: 'Mức ăn - 85000',
                totalUnit: 10,
                totalPerson: 100,
            },
        ],
    },
    {
        position: 4,
        name: 'Nước mắm',
        weight: 1.0079,
        unit: 1,
        foodLevel: [
            {
                title: 'Mức ăn - 65000',
                totalUnit: 10,
                totalPerson: 100,
            },
            {
                title: 'Mức ăn - 81000',
                totalUnit: 10,
                totalPerson: 100,
            },
            {
                title: 'Mức ăn - 85000',
                totalUnit: 10,
                totalPerson: 100,
            },
        ],
    },
    {
        position: 5,
        name: 'Nước mắm',
        weight: 1.0079,
        unit: 1,
        foodLevel: [
            {
                title: 'Mức ăn - 65000',
                totalUnit: 10,
                totalPerson: 100,
            },
            {
                title: 'Mức ăn - 81000',
                totalUnit: 10,
                totalPerson: 100,
            },
            {
                title: 'Mức ăn - 85000',
                totalUnit: 10,
                totalPerson: 100,
            },
        ],
    },
    {
        position: 6,
        name: 'Nước mắm',
        weight: 1.0079,
        unit: 1,
        foodLevel: [
            {
                title: 'Mức ăn - 65000',
                totalUnit: 10,
                totalPerson: 100,
            },
            {
                title: 'Mức ăn - 81000',
                totalUnit: 10,
                totalPerson: 100,
            },
            {
                title: 'Mức ăn - 85000',
                totalUnit: 10,
                totalPerson: 100,
            },
        ],
    },
];

export const GROUP_FOOD_UPGRADE: GroupFoodUpgrade[] = [
    {
        createdAt: '2022-11-14T08:57:09.340Z',
        name: 'Kendra DuBuque',
        unit: 1,
        unitPrice: '116.00',
        store: 'Antone Mountain',
        currentTotal: 45368,
        expTotal: 15819,
        totalMoney: '638.00',
        id: '1',
    },
    {
        createdAt: '2022-11-14T09:10:23.456Z',
        name: 'Barry Beer',
        unit: 1,
        unitPrice: '339.00',
        store: 'Dare Camp',
        currentTotal: 25050,
        expTotal: 7297,
        totalMoney: '265.00',
        id: '2',
    },
    {
        createdAt: '2022-11-15T00:35:33.024Z',
        name: "Patsy O'Kon",
        unit: 1,
        unitPrice: '125.00',
        store: 'Collins Neck',
        currentTotal: 18077,
        expTotal: 22792,
        totalMoney: '162.00',
        id: '3',
    },
    {
        createdAt: '2022-11-14T06:08:50.288Z',
        name: 'Jasmine Jast',
        unit: 1,
        unitPrice: '892.00',
        store: 'Rogahn Rue',
        currentTotal: 69389,
        expTotal: 37178,
        totalMoney: '750.00',
        id: '4',
    },
    {
        createdAt: '2022-11-14T21:36:37.108Z',
        name: 'Miranda Shields',
        unit: 1,
        unitPrice: '26.00',
        store: 'Eudora Plaza',
        currentTotal: 89704,
        expTotal: 54348,
        totalMoney: '406.00',
        id: '5',
    },
    {
        createdAt: '2022-11-14T19:21:58.675Z',
        name: 'Roman Mills',
        unit: 1,
        unitPrice: '528.00',
        store: 'Antonia Land',
        currentTotal: 20581,
        expTotal: 69561,
        totalMoney: '987.00',
        id: '6',
    },
    {
        createdAt: '2022-11-14T02:10:19.050Z',
        name: 'Sherman Kuphal',
        unit: 1,
        unitPrice: '657.00',
        store: 'Felicity Prairie',
        currentTotal: 11682,
        expTotal: 14002,
        totalMoney: '79.00',
        id: '7',
    },
    {
        createdAt: '2022-11-14T04:43:38.299Z',
        name: 'Kellie Leffler',
        unit: 1,
        unitPrice: '359.00',
        store: 'Barrows Underpass',
        currentTotal: 19641,
        expTotal: 24466,
        totalMoney: '332.00',
        id: '8',
    },
    {
        createdAt: '2022-11-14T05:53:50.741Z',
        name: 'Joann Braun',
        unit: 1,
        unitPrice: '476.00',
        store: 'Urban Curve',
        currentTotal: 18315,
        expTotal: 80200,
        totalMoney: '862.00',
        id: '9',
    },
    {
        createdAt: '2022-11-14T05:32:24.344Z',
        name: 'Mr. Abraham Koss',
        unit: 1,
        unitPrice: '265.00',
        store: 'Rolando Extension',
        currentTotal: 26489,
        expTotal: 59078,
        totalMoney: '671.00',
        id: '10',
    },
    {
        createdAt: '2022-11-15T01:02:23.534Z',
        name: 'Joanne Gottlieb',
        unit: 1,
        unitPrice: '385.00',
        store: 'Roob Pass',
        currentTotal: 79355,
        expTotal: 94795,
        totalMoney: '438.00',
        id: '11',
    },
    {
        createdAt: '2022-11-15T00:34:18.989Z',
        name: 'Orville Dare PhD',
        unit: 1,
        unitPrice: '898.00',
        store: 'Hessel Way',
        currentTotal: 92729,
        expTotal: 49160,
        totalMoney: '572.00',
        id: '12',
    },
    {
        createdAt: '2022-11-14T09:54:18.330Z',
        name: 'Chris Franecki',
        unit: 1,
        unitPrice: '714.00',
        store: 'Neha Square',
        currentTotal: 2386,
        expTotal: 34080,
        totalMoney: '794.00',
        id: '13',
    },
    {
        createdAt: '2022-11-14T16:49:58.830Z',
        name: 'Lola Sanford',
        unit: 1,
        unitPrice: '206.00',
        store: 'Reichert Shore',
        currentTotal: 32948,
        expTotal: 29348,
        totalMoney: '362.00',
        id: '14',
    },
    {
        createdAt: '2022-11-14T12:48:26.641Z',
        name: 'Caroline Wintheiser',
        unit: 1,
        unitPrice: '864.00',
        store: 'Turcotte Prairie',
        currentTotal: 11287,
        expTotal: 51017,
        totalMoney: '573.00',
        id: '15',
    },
    {
        createdAt: '2022-11-14T18:25:54.840Z',
        name: 'Mr. Natasha Cronin',
        unit: 1,
        unitPrice: '714.00',
        store: 'Fahey Junction',
        currentTotal: 87294,
        expTotal: 97084,
        totalMoney: '681.00',
        id: '16',
    },
    {
        createdAt: '2022-11-14T15:02:09.722Z',
        name: 'Jermaine Blanda',
        unit: 1,
        unitPrice: '824.00',
        store: 'Adams Parks',
        currentTotal: 34426,
        expTotal: 20770,
        totalMoney: '639.00',
        id: '17',
    },
    {
        createdAt: '2022-11-14T01:54:03.019Z',
        name: 'Marianne Rolfson',
        unit: 1,
        unitPrice: '940.00',
        store: 'Mosciski Rapid',
        currentTotal: 6222,
        expTotal: 38209,
        totalMoney: '944.00',
        id: '18',
    },
    {
        createdAt: '2022-11-14T22:15:53.535Z',
        name: 'Devin Schaden',
        unit: 1,
        unitPrice: '462.00',
        store: 'Bobbie Fall',
        currentTotal: 13672,
        expTotal: 2248,
        totalMoney: '111.00',
        id: '19',
    },
    {
        createdAt: '2022-11-14T13:48:01.321Z',
        name: 'Michael Lowe',
        unit: 1,
        unitPrice: '940.00',
        store: 'Emanuel Courts',
        currentTotal: 24110,
        expTotal: 94733,
        totalMoney: '179.00',
        id: '20',
    },
    {
        createdAt: '2022-11-14T10:47:31.223Z',
        name: 'Muriel Cassin I',
        unit: 1,
        unitPrice: '465.00',
        store: 'Bayer Canyon',
        currentTotal: 13032,
        expTotal: 99865,
        totalMoney: '589.00',
        id: '21',
    },
    {
        createdAt: '2022-11-14T17:15:47.264Z',
        name: 'Sherry Willms',
        unit: 1,
        unitPrice: '762.00',
        store: 'Coralie Meadows',
        currentTotal: 27154,
        expTotal: 47185,
        totalMoney: '492.00',
        id: '22',
    },
    {
        createdAt: '2022-11-14T14:44:27.103Z',
        name: 'June Mosciski',
        unit: 1,
        unitPrice: '594.00',
        store: 'Randal Spurs',
        currentTotal: 94236,
        expTotal: 58958,
        totalMoney: '882.00',
        id: '23',
    },
    {
        createdAt: '2022-11-14T05:10:06.871Z',
        name: 'Agnes Corkery',
        unit: 1,
        unitPrice: '126.00',
        store: 'Cristian Isle',
        currentTotal: 70994,
        expTotal: 41264,
        totalMoney: '794.00',
        id: '24',
    },
    {
        createdAt: '2022-11-15T00:14:37.781Z',
        name: 'Inez Abernathy',
        unit: 1,
        unitPrice: '427.00',
        store: 'Jonatan Club',
        currentTotal: 52843,
        expTotal: 55969,
        totalMoney: '728.00',
        id: '25',
    },
    {
        createdAt: '2022-11-14T13:45:33.848Z',
        name: 'Claude Boehm',
        unit: 1,
        unitPrice: '726.00',
        store: 'Leffler Drive',
        currentTotal: 43094,
        expTotal: 57603,
        totalMoney: '497.00',
        id: '26',
    },
];

export const TOTAL_PRICE_USER: TotalPriceUser[] = [
    {
        name: 'Bộ binh',
        priceRate: '65.000đ/người/ngày',
        detailt: [
            {
                time: 'Sáng',
                totalUser: 44,
                price: 13000,
            },
            {
                time: 'Trưa',
                totalUser: 57,
                price: 26000,
            },
            {
                time: 'Chiều',
                totalUser: 54,
                price: 26000,
            },
        ],
    },
    {
        name: 'Bộ binh - Mức 1',
        priceRate: '81.000đ/người/ngày',
        detailt: [
            {
                time: 'Sáng',
                totalUser: 44,
                price: 13000,
            },
            {
                time: 'Trưa',
                totalUser: 57,
                price: 26000,
            },
            {
                time: 'Chiều',
                totalUser: 54,
                price: 26000,
            },
        ],
    },
    {
        name: 'Bộ binh - Mức 2',
        priceRate: '85.000đ/người/ngày',
        detailt: [
            {
                time: 'Sáng',
                totalUser: 44,
                price: 13000,
            },
            {
                time: 'Trưa',
                totalUser: 57,
                price: 26000,
            },
            {
                time: 'Chiều',
                totalUser: 54,
                price: 26000,
            },
        ],
    },
];

//Data fake from API http://bepan.tasp.vn/api/services/read/Category/GetList
// {
//   "maxResultCount": 2147483647,
//   "skipCount": 2147483647,
//   "criterias": [
//     {
//       "propertyName": "GroupCode",
//       "operation": 6,
//       "value": "don-vi-tinh"
//     }
//   ],

// }
export const LIST_UNIT: Unit[] = [
    {
        index: 9,
        name: '%',
        codeData: '%',
    },
    {
        index: 8,
        name: 'Bình',
        codeData: 'Binh',
    },
    {
        index: 7,
        name: 'Mililit',
        codeData: 'Ml',
    },
    {
        index: 6,
        name: 'Gram',
        codeData: 'Gram',
    },
    {
        index: 5,
        name: 'Bộ',
        codeData: 'Bo',
    },
    {
        index: 4,
        name: 'Cái',
        codeData: 'Cai',
    },
    {
        index: 3,
        name: 'Thùng',
        codeData: 'Thung',
    },
    {
        index: 2,
        name: 'Lít',
        codeData: 'L',
    },
    {
        index: 1,
        name: 'Kg',
        codeData: 'Kg',
    },
];
