import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogQuanntitiveRegulationComponent } from './dialog-quanntitive-regulation.component';

describe('DialogQuanntitiveRegulationComponent', () => {
  let component: DialogQuanntitiveRegulationComponent;
  let fixture: ComponentFixture<DialogQuanntitiveRegulationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogQuanntitiveRegulationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogQuanntitiveRegulationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
