import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatPaginator, MatTableDataSource } from '@angular/material';
import { Router } from '@angular/router';
import { DeleteConfirmDialogComponent } from '@app/_shared/dialogs/delete-confirm-dialog/delete-confirm-dialog.component';
import { CommonService } from '@core/services/common.service';
import { DataService } from '@core/services/data-service';
import { ToastrService } from '@core/services/toastr.service';
import { Subscription } from 'rxjs';

@Component({
    selector: 'app-food-plan-component',
    templateUrl: './food-plan-component.component.html',
    styleUrls: ['./food-plan-component.component.scss'],
})
export class FoodPlanComponentComponent implements OnInit {
    @ViewChild(MatPaginator) paginator!: MatPaginator;

    displayedColumnsOfField: string[] = [
        'maPhieuXuatKho',
        'maNguoiDuyet',
        'ngayXuatKho',
        'actions',
    ];

    dataSource = new MatTableDataSource<any>([]);
    isLoading: boolean = false;
    allowExec: boolean = false;
    subscription: Subscription;

    constructor(
        private _router: Router,
        private _commonService: CommonService,
        private _dataService: DataService,
        public dialog: MatDialog,
        private _toastrService: ToastrService,
    ) {
        let request = {
            id: localStorage.getItem('idMenu'),
            language: 'vi',
        };

        this._commonService.checkPermission(request).subscribe((response) => {
            if (JSON.parse(response.result).some((obj) => obj.key === 'EXEC')) {
                this.allowExec = true;
            }
        });

        this.subscription = this._dataService.getData().subscribe((message) => {
            this.isLoading = true;
            this.getInitData(message.id);
        });
    }

    ngOnInit() {
        setTimeout(
            () => this.getInitData(this._dataService.getDauMoiBepId()),
            1000,
        );
    }

    getInitData(id: number) {
        let bodySubmit = {
            maxResultCount: 2147483647,
            sorting: 'ngayKeHoach DESC',
            criterias: [
                {
                    propertyName: 'dauMoiBepId',
                    operation: 0,
                    value: id,
                },
            ],
        };
        this._commonService
            .callDataAPIShort(
                '/api/services/read/KeHoachLttp/GetAll',
                bodySubmit,
            )
            .subscribe((response) => {
                this.dataSource = new MatTableDataSource<any>(
                    response.result.items,
                );
                this.dataSource.paginator = this.paginator;
                setTimeout(() => (this.isLoading = false), 200);
            });
    }

    createAction(element: any): void {
        if (element) {
            this._router.navigate([
                `manage/app/category/ke-hoach-LTTP-chat-dot/edit/${element.id}`,
            ]);
        } else {
            this._router.navigate([
                'manage/app/category/ke-hoach-LTTP-chat-dot/add',
            ]);
        }
    }

    // HANDLE SEARCH
    handleInputFilter(filterValue: string): void {
        const searchValue = filterValue.trim().toLowerCase();

        if (!searchValue) {
            this.getInitData(this._dataService.getDauMoiBepId());
        } else {
            this._commonService
                .callDataAPIShort('/api/services/read/KeHoachLttp/GetAll', {
                    maxResultCount: 2147483647,
                    criterias: [
                        {
                            propertyName: 'dauMoiBepId',
                            operation: 0,
                            value: this._dataService.getDauMoiBepId(),
                        },
                        {
                            propertyName: 'tenKeHoach',
                            operation: 6,
                            value: searchValue,
                        },
                    ],
                })
                .subscribe((res) => {
                    this.dataSource.data = res.result.items;
                    this.dataSource.paginator = this.paginator;
                });
        }
    }

    deleteAction(element: any) {
        const dialogRef = this.dialog.open(DeleteConfirmDialogComponent, {
            width: '500px',
            data: element,
        });

        dialogRef.componentInstance.handleDelete.subscribe((dataDelete) => {
            const isSubmitted = !!dataDelete;

            this._commonService
                .callDataAPIShort('/api/services/write/KeHoachLttp/Delete', {
                    id: element.id,
                })
                .subscribe(
                    (res) => dialogRef.close(isSubmitted),
                    (err) => this._toastrService.errorServer(err),
                );
        });

        dialogRef.afterClosed().subscribe((result) => {
            if (!result) return;
            this.getInitData(this._dataService.getDauMoiBepId());
            this._toastrService.success('', 'Xóa thành công');
        });
    }
}
