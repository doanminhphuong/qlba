import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ToolManagerStoreComponent } from './tool-manager-store.component';

describe('ToolManagerStoreComponent', () => {
  let component: ToolManagerStoreComponent;
  let fixture: ComponentFixture<ToolManagerStoreComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ToolManagerStoreComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ToolManagerStoreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
