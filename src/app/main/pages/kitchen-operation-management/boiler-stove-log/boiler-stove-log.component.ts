import { SelectionModel } from '@angular/cdk/collections';
import {
    AfterViewInit,
    Component,
    OnDestroy,
    OnInit,
    ViewChild,
} from '@angular/core';
import {
    MatDialog,
    MatPaginator,
    MatTableDataSource,
    PageEvent,
} from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { DeleteConfirmDialogComponent } from '@app/_shared/dialogs/delete-confirm-dialog/delete-confirm-dialog.component';
import { CommonService } from '@core/services/common.service';
import { ToastrService } from '@core/services/toastr.service';
import { forkJoin } from 'rxjs';

export interface INhatKyVanHanh {
    id: string;
    bepId: number;
    chiTiet: string;
    ngayGhiChep: string;
    nhienLieuDot: number;
    quanSoAn: number;
}

@Component({
    selector: 'app-boiler-stove-log',
    templateUrl: './boiler-stove-log.component.html',
    styleUrls: ['./boiler-stove-log.component.scss'],
})
export class BoilerStoveLogComponent
    implements OnInit, AfterViewInit, OnDestroy {
    @ViewChild(MatPaginator) paginator!: MatPaginator;

    // Table
    displayedColumns: string[] = [];
    dataSource = new MatTableDataSource<any>([]);
    selection = new SelectionModel<any>(true, []);

    // Pagination
    totalRows = 0;
    pageSize = 10;
    currentPage = 0;
    pageSizeOptions: number[] = [5, 10, 25, 50];
    dataSkipped: number = 0;

    // Search Advanced
    fieldSearch: any[] = [
        {
            type: 'SELECT',
            referenceValue: 'trangThai',
            name: 'Chọn trạng thái',
            defaultValue: 'all',
            required: '0',
            options: [
                { key: 'all', name: 'Tất cả' },
                { key: true, name: 'Bình thường' },
                { key: false, name: 'Sự cố' },
            ],
            css: 'col-12 col-lg-4',
            appearance: 'legacy',
        },
        {
            type: 'DATETIME',
            referenceValue: 'startDate',
            name: 'Từ ngày',
            defaultValue: '',
            required: '0',
            css: 'col-12 col-lg-4',
            appearance: 'legacy',
        },
        {
            type: 'DATETIME',
            referenceValue: 'endDate',
            name: 'Đến ngày',
            defaultValue: '',
            required: '0',
            dependenField: 'startDate',
            css: 'col-12 col-lg-4',
            appearance: 'legacy',
        },
    ];

    // Init
    pageTitle: string;
    bepLogList: INhatKyVanHanh[] = [];

    // Params
    bepId: number;
    loaiBepParam: string;
    chungLoaiBepParam: string;

    // Boolean
    isLoading: boolean = false;
    isFiltering: boolean = false;
    allowExec: boolean = true;

    constructor(
        private _router: Router,
        private _activatedRoute: ActivatedRoute,
        public dialog: MatDialog,
        private _toastrService: ToastrService,
        private _commonService: CommonService,
    ) {
        this._commonService.checkPermission2().subscribe((response) => {
            if (!response.result) return;

            const permissions = JSON.parse(response.result);
            console.log('permissions:: ', permissions);
            this.allowExec = permissions.some((obj) => obj.key === 'EXEC');
        });
    }

    ngOnInit() {
        // Check if this is Create or Edit page
        this._activatedRoute.paramMap.subscribe((param) => {
            this.bepId = +param.get('bepId');
            this.loaiBepParam = param.get('loaiBep');
            this.chungLoaiBepParam = param.get('chungLoaiBep');
        });

        this.getInitData();
    }

    ngAfterViewInit() {
        this.dataSource.paginator = this.paginator;
    }

    ngOnDestroy(): void { }

    getInitData(): void {
        this.displayedColumns =
            this.loaiBepParam === 'Bếp lò hơi cơ khí'
                ? [
                    'ngayGhiChep',
                    'nhienLieuDot',
                    'quanSoAn',
                    'sang',
                    'trua',
                    'chieu',
                    'actions',
                ]
                : ['ngayGhiChep', 'sang', 'trua', 'chieu', 'actions'];

        if (this.allowExec === false) {
            this.loaiBepParam === 'Bếp lò hơi cơ khí'
                ? this.displayedColumns.splice(6, 1)
                : this.displayedColumns.splice(4, 1);
        }

        this.pageTitle = `${this.loaiBepParam} "${this.chungLoaiBepParam}"`;

        const dataNhatKyVanHanh = this._commonService.callDataAPIShort(
            '/api/services/read/NhatKyVanHanh/GetAll',
            {
                maxResultCount: this.pageSize,
                skipCount: this.dataSkipped,
                sorting: 'ngayGhiChep DESC',
                criterias: [
                    {
                        propertyName: 'bepId',
                        operation: 0,
                        value: this.bepId,
                    },
                ],
            },
        );

        forkJoin([dataNhatKyVanHanh]).subscribe((responses) => {
            this.bepLogList = responses[0].result.items;
            this.dataSource.data = this.bepLogList;
            setTimeout(() => {
                this.paginator.pageIndex = this.currentPage;
                this.paginator.length = responses[0].result.totalCount;
                this.isLoading = false;
            }, 200);
        });
    }

    getTrangThaiBep(index: number, buoi: string): string {
        let result = '';

        if (this.bepLogList.length > 0) {
            const chiTiet = JSON.parse(this.bepLogList[index]['chiTiet']);

            const caNau = chiTiet.find((item) => item.CaNau === buoi);

            result =
                caNau['TrangThaiHoatDong'] === true ? 'Bình thường' : 'Sự cố';
        }

        return result;
    }

    pageChanged(event: PageEvent) {
        if (this.isFiltering) return;

        this.currentPage = event.pageIndex;
        this.dataSkipped = event.pageIndex * 10;

        this.getInitData();
    }

    createAction(): void {
        this._router.navigate([
            `manage/app/category/nhat-ky-van-hanh-bep/${this.bepId}/create`,
            {
                chungLoaiBep: this.chungLoaiBepParam,
                loaiBep: this.loaiBepParam,
            },
        ]);
    }

    editAction(element: any): void {
        const nhatKyId = element['id'];

        this._router.navigate([
            `manage/app/category/nhat-ky-van-hanh-bep/${this.bepId}/edit`,
            {
                nhatKyId,
                chungLoaiBep: this.chungLoaiBepParam,
                loaiBep: this.loaiBepParam,
            },
        ]);
    }

    deleteAction(element: any): void {
        const dialogRef = this.dialog.open(DeleteConfirmDialogComponent, {
            width: '500px',
            data: element,
        });

        dialogRef.componentInstance.handleDelete.subscribe((dataDelete) => {
            const isSubmitted = !!dataDelete;

            this._commonService
                .callDataAPIShort(
                    '/api/services/write/NhatKyVanHanh/Delete',
                    dataDelete,
                )
                .subscribe(
                    (res) => dialogRef.close(isSubmitted),
                    (err) => this._toastrService.errorServer(err),
                );
        });

        dialogRef.afterClosed().subscribe((result) => {
            if (!result) return;

            this.getInitData();
            this._toastrService.success('', 'Xóa thành công');
        });
    }

    handleSearch(data: any): void {
        const { startDate, endDate, trangThai } = data;

        if (data.key === 'reset') {
            this.isFiltering = false;
            this.getInitData();
        } else {
            const tuNgay = !startDate ? '' : startDate._d;
            const denNgay = !endDate ? '' : endDate._d;

            let bodyRequest = {
                maxResultCount: 9999999,
                sorting: 'ngayGhiChep DESC',
                criterias: [
                    // bepId
                    {
                        propertyName: 'bepId',
                        operation: 0,
                        value: this.bepId,
                    },

                    // tuNgay
                    {
                        propertyName: tuNgay ? 'ngayGhiChep' : '',
                        operation: tuNgay ? 'GreaterThanOrEqual' : 'Contains',
                        value: tuNgay
                            ? tuNgay.toISOString().replace('T17', 'T00')
                            : '',
                    },

                    // denNgay
                    {
                        propertyName: denNgay ? 'ngayGhiChep' : '',
                        operation: denNgay ? 'LessThanOrEqual' : 'Contains',
                        value: denNgay
                            ? denNgay.toISOString().replace('T17', 'T00')
                            : '',
                    },
                ],
                trangThai:
                    trangThai !== null && trangThai !== 'all'
                        ? trangThai
                        : null,
            };

            this._commonService
                .callDataAPIShort(
                    '/api/services/read/NhatKyVanHanh/GetAll',
                    bodyRequest,
                )
                .subscribe((response) => {
                    this.bepLogList = response.result.items;
                    this.dataSource.data = this.bepLogList;
                    this.dataSource.paginator = this.paginator;
                    this.isFiltering = true;
                });
        }
    }

    navigate(category: string): void {
        this._router.navigate([`manage/app/category/${category}`]);
    }
}
