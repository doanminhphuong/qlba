import { SelectionModel } from '@angular/cdk/collections';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import {
    MatDialog,
    MatPaginator,
    MatTableDataSource,
    PageEvent,
} from '@angular/material';
import { Router } from '@angular/router';
import { DynamicDialogComponent } from '@app/components/dynamic-dialog-component/dynamic-dialog-component.component';
import { DeleteConfirmDialogComponent } from '@app/_shared/dialogs/delete-confirm-dialog/delete-confirm-dialog.component';
import { CommonService } from '@core/services/common.service';
import { DataService } from '@core/services/data-service';
import { FoodyService } from '@core/services/foody.service';
import { HoSoSucKhoeService } from '@core/services/ho-so-suc-khoe.service';
import { ToastrService } from '@core/services/toastr.service';
import { UserService } from '@core/services/user.service';
import { forkJoin, Subscription } from 'rxjs';

@Component({
    selector: 'app-board-rice',
    templateUrl: './board-rice.component.html',
    styleUrls: ['./board-rice.component.scss'],
})
export class BoardRiceComponent implements OnInit {
    @ViewChild(MatPaginator) paginator!: MatPaginator;
    currentDate = new Date();

    currentUser: any;
    currentOrganization: any;
    dauMoiBepOptions: any[] = [];
    unitOptions: any[] = [];
    groupOptions: any[] = [];
    typeOptions: any[] = [];
    userList: any[] = [];

    displayedColumnsOfField: string[] = [
        'tenChamCom',
        'creationTime',
        'creatorUserId',
        'actions',
    ];

    dataSource = new MatTableDataSource<any>([]);
    selection = new SelectionModel<any>(true, []);

    form: FormGroup;
    fileName: any;
    fieldList: any[] = [];

    fieldSearch: any[] = [
        {
            type: 'DATETIME',
            referenceValue: 'startDate',
            name: 'Từ ngày',
            defaultValue: '',
            required: '0',
            css: 'col-12 col-lg-6',
            appearance: 'legacy',
        },
        {
            type: 'DATETIME',
            referenceValue: 'endDate',
            name: 'Đến ngày',
            defaultValue: '',
            required: '0',
            dependenField: 'startDate',
            disabled: true,
            css: 'col-12 col-lg-6',
            appearance: 'legacy',
        },
    ];

    monthOptions: any[] = [];

    // Pagination
    totalRows = 0;
    pageSize = 10;
    currentPage = 0;
    pageSizeOptions: number[] = [5, 10, 25, 50];
    dataSkipped: number = 0;

    fields: any[] = [];

    allowExec: boolean = false;
    isLoading: boolean = false;
    subscription: Subscription;

    constructor(
        private _router: Router,
        public dialog: MatDialog,
        private _toastrService: ToastrService,
        private _foodyService: FoodyService,
        public _hssk: HoSoSucKhoeService,
        public _userService: UserService,
        private _commonService: CommonService,
        private _dataService: DataService,
    ) {
        let request = {
            id: localStorage.getItem('idMenu'),
            language: 'vi',
        };

        this._commonService.checkPermission(request).subscribe((response) => {
            if (!response.result) return;

            if (JSON.parse(response.result).some((obj) => obj.key === 'EXEC')) {
                this.allowExec = true;
            }
        });

        this.subscription = this._dataService.getData().subscribe((message) => {
            this.isLoading = true;
            this.getInitData(message.id);
        });
    }

    ngOnInit() {
        this.getAllUser();
        this.getCurrentUser();
        this.getInitData(this._dataService.getDauMoiBepId());
    }

    getAllUser(): void {
        let body = {
            maxResultCount: 1000,
            skipCount: 0,
            sorting: 'Code',
            criterias: [
                {
                    propertyName: 'Code',
                    operation: 6,
                    value: '',
                },
            ],
        };

        this._userService.getUsers(body).subscribe((res) => {
            this.userList = res.result;
        });
    }

    getUserCreator(id: number) {
        let result = '';

        if (this.userList.length > 0) {
            const dataFound = this.userList.find((user) => user.id === id);

            result = dataFound ? dataFound.fullName : '';
        }

        return result;
    }

    getInitForm(): void {
        let fieldsCtrls = {};
        let fieldList = [];

        for (let f of this.fields) {
            fieldList = [...fieldList, f];

            if (f.type === 'NUMBER') {
                fieldsCtrls[f.referenceValue] = new FormControl(
                    { value: f.defaultValue, disabled: f.disabled },
                    f.required === '1' ? [Validators.required] : [],
                );
            } else if (f.type !== 'CHECKBOX') {
                let validators = [
                    Validators.minLength(f.minLength),
                    Validators.maxLength(f.maxLength),
                    Validators.pattern(f.pattern),
                ];

                if (f.type === 'EMAIL') {
                    validators = [...validators, Validators.email];
                }

                fieldsCtrls[f.referenceValue] = new FormControl(
                    {
                        value: f.defaultValue || '',
                        disabled: f.disabled,
                    },

                    f.required === '1'
                        ? [...validators, Validators.required]
                        : [...validators],
                );
            } else {
                //if checkbox, it need multiple
                let opts = {};
                for (let opt of f.options) {
                    opts[opt.key] = new FormControl({
                        value: opt.value,
                        disabled: f.disabled,
                    });
                }
                fieldsCtrls[f.referenceValue] = new FormGroup(opts);
            }
        }

        this.form = new FormGroup(fieldsCtrls);
    }

    getCurrentUser() {
        const dataCurrentUser = this._hssk.getCurrentUsers();
        const dataCurrentOrganization = this._hssk.getCurrentOrganizations();

        forkJoin([dataCurrentUser, dataCurrentOrganization]).subscribe(
            (results) => {
                this.currentUser = results[0].result;

                if (results[1].result.length === 0) return;

                this.currentOrganization = results[1].result.find((item) =>
                    item.groupCode.toUpperCase().includes('DONVI'),
                );

                this.getAllDauMoiBep();
            },
        );
    }

    getAllDauMoiBep() {
        const body = {
            maxResultCount: 9999,
            sorting: 'Id',
            donViId: this.currentOrganization.id,
        };
        this._foodyService.getAllDauMoiBep(body).subscribe((res) => {
            this.dauMoiBepOptions = res.result.items.map((item) => ({
                key: item.id,
                name: item.tenBep,
            }));
        });
    }

    getInitData(id: number): void {
        const body = {
            maxResultCount: 9999999,
            skipCount: this.dataSkipped,
            criterias: [
                {
                    propertyName: 'dauMoiBepId',
                    operation: 0,
                    value: id,
                },
            ],
        };

        this._foodyService.getAllChamCom(body).subscribe((res) => {
            this.dataSource.data = res.result.items;
            this.dataSource.paginator = this.paginator;

            let getMonth = [];

            res.result.items.forEach((element) =>
                getMonth.push({
                    month: new Date(element.ngayKetThuc).getMonth(),
                    year: new Date(element.ngayKetThuc).getFullYear(),
                }),
            );

            for (let i = 0; i <= 11; i++) {
                const dataMonth = {
                    key: i,
                    name: `Tháng ${i + 1}`,
                };

                this.monthOptions.push(dataMonth);
            }

            getMonth.forEach((data) => {
                if (data.year === new Date().getFullYear()) {
                    this.monthOptions = this.monthOptions.filter(
                        (option) => option.key !== data.month,
                    );
                } else return;
            });

            if (this.monthOptions.length > 0) {
                this.fields = [
                    {
                        type: 'SELECT',
                        referenceValue: 'thangChamCom',
                        name: 'Chọn tháng chấm cơm',
                        defaultValue: '',
                        required: '1',
                        options: [...this.monthOptions],
                        css: 'col-12 col-lg-12',
                        appearance: 'legacy',
                    },
                ];

                this.getInitForm();
            }
            setTimeout(() => {
                this.paginator.pageIndex = this.currentPage;
                this.paginator.length = res.result.totalCount;
            });

            setTimeout(() => {
                this.isLoading = false;
            }, 200);
        });
    }

    pageChanged(event: PageEvent) {
        this.currentPage = event.pageIndex;
        this.dataSkipped = event.pageIndex * 10;

        this.getInitData(this._dataService.getDauMoiBepId());
    }

    guid = () => {
        let s4 = () => {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        };
        //return id of format 'aaaaaaaa'-'aaaa'-'aaaa'-'aaaa'-'aaaaaaaaaaaa'
        return (
            s4() +
            s4() +
            '-' +
            s4() +
            '-' +
            s4() +
            '-' +
            s4() +
            '-' +
            s4() +
            s4() +
            s4()
        );
    };

    formatDate(date: Date) {
        const dd = ('0' + date.getDate()).slice(-2);
        const mm = ('0' + (date.getMonth() + 1)).slice(-2);
        const yyyy = date.getFullYear();

        return `${yyyy}-${mm}-${dd}`;
    }

    onSubmit(): void {
        const { thangChamCom } = this.form.value;

        const tenChamCom =
            ('0' + (thangChamCom + 1)).slice(-2) +
            '-' +
            this.currentDate.getFullYear();

        const firstDayOfMonth = new Date(
            this.currentDate.getFullYear(),
            thangChamCom,
            1,
        );

        const lastDayOfMonth = new Date(
            this.currentDate.getFullYear(),
            thangChamCom + 1,
            0,
        );

        let dataSubmit = {
            ...this.form.value,
            chamKhongAn: true,
            dauMoiBepId: this._dataService.getDauMoiBepId(),
            tenChamCom,
            ngayBatDau: this.formatDate(firstDayOfMonth),
            ngayKetThuc: this.formatDate(lastDayOfMonth),
        };

        this._foodyService.createChamCom(dataSubmit).subscribe(
            (res) => {
                if (res.success) {
                    this.getInitData(this._dataService.getDauMoiBepId());
                    this._toastrService.success('', 'Tạo thành công');
                }
            },
            (err) => this._toastrService.errorServer(err),
        );
    }

    editAction(element: any): void {
        this._router.navigate([`app/cham-com/quan-ly-cham-com/${element.id}`]);
    }
}
