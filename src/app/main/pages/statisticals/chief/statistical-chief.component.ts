import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatPaginator, MatTableDataSource } from '@angular/material';
import { Statistical } from '@app/pages/financial-report/financial-report.component';
import { DialogInventoryDetailComponent } from '@app/pages/inventory-management/dialog-inventory-detail/dialog-inventory-detail.component';
import { CommonService } from '@core/services/common.service';
import { DataService } from '@core/services/data-service';
import { FoodFuelService } from '@core/services/food-fuel.service';
import { FoodyService } from '@core/services/foody.service';
import { HoSoSucKhoeService } from '@core/services/ho-so-suc-khoe.service';
import { UserService } from '@core/services/user.service';
import { map } from 'lodash';
import { forkJoin, Subscription } from 'rxjs';

@Component({
    selector: 'app-statistical-chief',
    templateUrl: './statistical-chief.component.html',
    styleUrls: ['./statistical-chief.component.scss'],
})
export class StatisticalChiefComponent implements OnInit {
    statisticals: Statistical[] = [];

    multi: any[] = [
        {
            name: 'Tổng nhập',
            series: [
                {
                    name: '1',
                    value: 0,
                },
                {
                    name: '2',
                    value: 62000000,
                },
                {
                    name: '3',
                    value: 73000000,
                },
                {
                    name: '4',
                    value: 89400000,
                },
                {
                    name: '5',
                    value: 73000000,
                },
                {
                    name: '6',
                    value: 89400000,
                },
                {
                    name: '7',
                    value: 89400000,
                },
                {
                    name: '8',
                    value: 62000000,
                },
                {
                    name: '9',
                    value: 73000000,
                },
                {
                    name: '10',
                    value: 89400000,
                },
                {
                    name: '11',
                    value: 62000000,
                },
                {
                    name: '12',
                    value: 89400000,
                },
            ],
        },

        {
            name: 'Tổng xuất',
            series: [
                {
                    name: '1',
                    value: 0,
                },
                {
                    name: '2',
                    value: 250000000,
                },
                {
                    name: '3',
                    value: 309000000,
                },
                {
                    name: '4',
                    value: 311000000,
                },
                {
                    name: '5',
                    value: 89400000,
                },
                {
                    name: '6',
                    value: 89400000,
                },
                {
                    name: '7',
                    value: 309000000,
                },
                {
                    name: '8',
                    value: 250000000,
                },
                {
                    name: '9',
                    value: 309000000,
                },
                {
                    name: '10',
                    value: 311000000,
                },
                {
                    name: '11',
                    value: 89400000,
                },
                {
                    name: '12',
                    value: 89400000,
                },
            ],
        },
    ];

    displayedColumns: string[] = ['mucTienAn', 'an', 'khongAn', 'chua'];
    dataTable = new MatTableDataSource<any>([]);
    emptyData = new MatTableDataSource([{ empty: 'row' }]);

    listCook: any[] = [];
    defaultCook: string;

    view: any[] = [1200, 500];

    subcription: Subscription;
    isLoading: boolean = false;
    currentYear: number;
    idSuperiorUnit: any;
    today: string;

    // options
    legend: boolean = true;
    showLabels: boolean = false;
    animations: boolean = true;
    xAxis: boolean = true;
    yAxis: boolean = true;
    showYAxisLabel: boolean = true;
    showXAxisLabel: boolean = true;
    xAxisLabel: string = '';
    yAxisLabel: string = '';
    legendTitle: string = 'Chi tiết';
    timeline: boolean = false;

    colorScheme = {
        domain: [
            '#5AA454',
            '#E44D25',
            '#CFC0BB',
            '#7aa3e5',
            '#a8385d',
            '#aae3f5',
        ],
    };

    constructor(
        private _commonService: CommonService,
        private _foodFuelService: FoodFuelService,
        private _dataService: DataService,
        public dialog: MatDialog,
        private hsskService: HoSoSucKhoeService,
        private _foodyService: FoodyService,
    ) {
        Object.assign(this, this.multi);
        this.currentYear = new Date().getFullYear();

        this.today = `Ngày ${new Date().getDate()} tháng ${
            new Date().getMonth() + 1
        } năm ${new Date().getFullYear()}`;
    }

    ngOnInit() {
        this.isLoading = true;
        // this.getDataBeforeRenderForm();
        setTimeout(() => {
            this.idSuperiorUnit =
                this._dataService.getTenDonViCapTrenTrucTiep();
            this.getDataBeforeRenderForm();
        }, 500);

        let heightElement;
        if (heightElement > 1000) {
            heightElement = window.innerHeight - 600;
        } else {
            heightElement = window.innerHeight - 300;
        }
        this.view = [window.innerWidth - 500, heightElement];
    }

    getDataBeforeRenderForm() {
        const body = {
            maxResultCount: 9999,
            sorting: 'Id',
            donViId: this.idSuperiorUnit.id,
        };

        let dataCook = this._foodyService.getAllDauMoiBep(body);

        let dataStatistical = this._commonService.callDataAPIShort(
            '/api/services/read/Dashboard/GetAll',
            {
                level: 2,
                donViId: this.idSuperiorUnit.id,
            },
        );

        let dataFood = this._commonService.callDataAPIShort(
            '/api/services/read/TonKho/Count',
            {
                level: 2,
                donViId: this.idSuperiorUnit.id,
            },
        );

        let dataExportImportInYear = this._commonService.callDataAPIShort(
            '/api/services/read/BaoCao/GetBaoCaoNhapXuatTrongNam',
            {
                level: 2,
                donViId: this.idSuperiorUnit.id,
                nam: this.currentYear,
            },
        );

        forkJoin([
            dataStatistical,
            dataFood,
            dataExportImportInYear,
            dataCook,
        ]).subscribe((result) => {
            this.multi = [];

            this.statisticals = [
                {
                    value: result[1].result,
                    title: 'Lương thực thực phẩm',
                    icon: 'widgets',
                },
                {
                    value: result[0].result.tongChi,
                    title: 'Tổng chi',
                    icon: 'monetization_on',
                },
                {
                    value: result[0].result.tongNhap,
                    title: 'Tổng nhập',
                    icon: 'trending_down',
                },
                {
                    value: result[0].result.tongXuat,
                    title: 'Tổng xuất',
                    icon: 'trending_up',
                    class: 'custom-icon',
                },
            ];

            this.multi.push(
                {
                    name: 'Tổng nhập',
                    series: result[2].result.danhSach.map((value) => ({
                        name: value.thang,
                        value: value.nhap,
                    })),
                },
                {
                    name: 'Tổng xuất',
                    series: result[2].result.danhSach.map((value) => ({
                        name: value.thang,
                        value: value.xuat,
                    })),
                },
            );

            this.listCook = result[3].result.items.map((item) => ({
                value: item.id,
                viewValue: item.tenBep,
            }));

            this.defaultCook =
                this.listCook.length > 0 ? this.listCook[0].value : 0;

            setTimeout(() => {
                this.isLoading = false;
            }, 200);
        });
    }

    onSelect(data): void {
        console.log('Item clicked', JSON.parse(JSON.stringify(data)));
    }

    onActivate(data): void {
        console.log('Activate', JSON.parse(JSON.stringify(data)));
    }

    onDeactivate(data): void {
        console.log('Deactivate', JSON.parse(JSON.stringify(data)));
    }

    selectFormChange(event) {
        if (event.isUserInput) {
            this.getDataEaterNumber(event.source.value);
        }
    }

    getTotal(amount: any) {
        return this.dataTable.data
            .map((t) => t[amount])
            .reduce((acc, value) => acc + value, 0);
    }

    getDataEaterNumber(value: any) {
        let body = {
            dauMoiBepId: value,
            ngay: new Date().toISOString().slice(0, 10),
        };

        this._commonService
            .callDataAPIShort(
                '/api/services/read/BaoCao/GetThongKeQuanSoAn',
                body,
            )
            .subscribe((response) => {
                this.dataTable = new MatTableDataSource<any>(
                    response.result.danhSach,
                );
            });
    }
}
