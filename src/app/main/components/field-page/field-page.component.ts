import { SelectionModel } from '@angular/cdk/collections';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatPaginator, MatTableDataSource } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { FieldConfigDialogComponent } from '@app/_shared/dialogs/field-config-dialog/field-config-dialog.component';
import { FieldService } from '@core/services/field.service';
import { ToastrService } from '@core/services/toastr.service';
import { TokenStorageService } from '@core/services/token.service';

@Component({
    selector: 'app-field-page',
    templateUrl: './field-page.component.html',
    styleUrls: ['./field-page.component.scss'],
})
export class FieldPageComponent implements OnInit {
    @ViewChild(MatPaginator) paginator: MatPaginator;

    fieldList: any[] = [];

    displayedColumns: string[] = [
        'select',
        'code',
        'codeData',
        'name',
        'type',
        'defaultValue',
        'referenceValue',
        'dependenCodes',
        'actions',
    ];
    dataSource = new MatTableDataSource<any>([]);
    selection = new SelectionModel<any>(true, []);

    valueData: string;
    data: any;
    listData: any;

    buttons = [
        {
            type: 'button',
            matType: 'stroked',
            title: 'Xóa nhiều',
            iconLeft: 'fa fa-trash',
            color: 'warn',
            isDisabled: false,
            onClick: () => this.createAction(),
        },
        {
            type: 'button',
            matType: 'flat',
            title: 'Thêm mới',
            iconLeft: 'fa fa-plus',
            color: 'warn',
            isDisabled: false,
            onClick: () => this.createAction(),
        },
    ];

    constructor(
        private _activatedroute: ActivatedRoute,
        private _router: Router,
        public dialog: MatDialog,
        private _fieldService: FieldService,
        private _toastrService: ToastrService,
        private _tokenStorageService: TokenStorageService,
    ) {}

    ngOnInit() {
        this.getInitData();

        // console.log("this.data", this.data);
        // console.log("environment url", environment.baseUrl);
    }

    getInitData(): void {
        let data = {
            maxResultCount: 1000,
            skipCount: 0,
            sorting: 'code',
            criterias: [
                {
                    propertyName: 'Code',
                    operation: 6,
                    value: '',
                },
            ],
        };

        this._fieldService.getAllField(data).subscribe((data) => {
            this.fieldList = data.result.items;
            this.dataSource = new MatTableDataSource<any>(this.fieldList);
            this.dataSource.paginator = this.paginator;
        });
    }

    /** Whether the number of selected elements matches the total number of rows. */
    isAllSelected() {
        const numSelected = this.selection.selected.length;
        const numRows = this.dataSource.data.length;
        return numSelected === numRows;
    }

    /** Selects all rows if they are not all selected; otherwise clear selection. */
    toggleAllRows() {
        if (this.isAllSelected()) {
            this.selection.clear();
            return;
        }

        this.selection.select(...this.dataSource.data);
    }

    /** The label for the checkbox on the passed row */
    checkboxLabel(row?: any): string {
        if (!row) {
            return `${this.isAllSelected() ? 'deselect' : 'select'} all`;
        }
        return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${
            row.index + 1
        }`;
    }

    openDialog(element: any): void {
        console.log('Element: ', element);

        const fields = [
            {
                type: 'TEXT',
                name: 'code',
                label: 'Mã',
                placeholder: 'Nhập vào mã',
                value: '',
                spellcheck: false,
                required: true,
                pattern: '^[a-zA-Z0-9-]*$',
                icon: 'code',
                css: 'col-12 col-lg-4',
                tab: 'THONGTIN',
            },
            {
                type: 'TEXT',
                name: 'codeData',
                label: 'Mã ghi nhớ',
                placeholder: 'Nhập vào mã ghi nhớ',
                value: '',
                spellcheck: false,
                required: false,
                icon: 'code',
                css: 'col-12 col-lg-4',
                tab: 'THONGTIN',
            },
            {
                type: 'SELECT',
                name: 'type',
                label: 'Loại',
                value: null,
                required: true,
                options: [
                    { key: 'TEXT', label: 'Text' },
                    { key: 'TEXTAREA', label: 'Textarea' },
                    { key: 'DATETIME', label: 'Datetime' },
                    { key: 'CHECKBOX', label: 'Checkbox' },
                    { key: 'RADIO', label: 'Radio' },
                    { key: 'SELECT', label: 'Select' },
                ],
                css: 'col-12 col-lg-4',
                tab: 'THONGTIN',
            },
            {
                type: 'TEXT',
                name: 'name',
                label: 'Tên',
                placeholder: 'Nhập vào tên',
                value: '',
                spellcheck: false,
                required: true,
                icon: 'title',
                css: 'col-12 col-lg-6',
                tab: 'THONGTIN',
            },
            {
                type: 'NUMBER',
                name: 'number1',
                label: 'Vị trí',
                placeholder: 'Nhập vào vị trí',
                value: 0,
                spellcheck: false,
                required: false,
                icon: 'format_list_numbered',
                css: 'col-12 col-lg-3',
                tab: 'THONGTIN',
            },
            {
                type: 'SELECT',
                name: 'status',
                label: 'Trạng thái',
                value: 'DEFAULT',
                required: false,
                options: [
                    { key: 'DEFAULT', label: 'Mặc định' },
                    { key: 'LOCK', label: 'Khóa' },
                ],
                css: 'col-12 col-lg-3',
                tab: 'THONGTIN',
            },
            {
                type: 'TEXT',
                name: 'hideValue',
                label: 'Từ khóa',
                placeholder: 'Nhập vào từ khóa',
                value: '',
                spellcheck: false,
                required: false,
                icon: 'code',
                css: 'col-12 col-lg-12',
                tab: 'THONGTIN',
            },
            {
                type: 'TEXTAREA',
                name: 'language',
                label: 'Ngôn ngữ',
                placeholder: 'Nhập vào ngôn ngữ',
                value: '',
                spellcheck: false,
                required: false,
                css: 'col-12 col-lg-12',
                tab: 'THONGTIN',
            },

            {
                type: 'TEXT',
                name: 'defaultValue',
                label: 'Giá trị mặc định',
                placeholder: 'Nhập vào giá trị mặc định',
                value: '',
                spellcheck: false,
                required: false,
                icon: 'texture',
                css: 'col-12 col-lg-6',
                tab: 'CAIDAT',
            },
            {
                type: 'TEXT',
                name: 'referenceValue',
                label: 'Giá trị liên kết',
                placeholder: 'Nhập vào giá trị liên kết',
                value: '',
                spellcheck: false,
                required: false,
                icon: 'link',
                css: 'col-12 col-lg-6',
                tab: 'CAIDAT',
            },
            {
                type: 'SELECT',
                name: 'required',
                label: 'Bắt buộc',
                value: '0',
                required: false,
                options: [
                    { key: '0', label: 'Không' },
                    { key: '1', label: 'Có' },
                ],
                css: 'col-12 col-lg-6',
                tab: 'CAIDAT',
            },
            {
                type: 'TEXT',
                name: 'format',
                label: 'Định dạng',
                placeholder: 'Nhập vào định dạng',
                value: '',
                spellcheck: false,
                required: false,
                icon: 'link',
                css: 'col-12 col-lg-6',
                tab: 'CAIDAT',
            },
            {
                type: 'TEXT',
                name: 'validate',
                label: 'Tính toàn vẹn',
                placeholder: 'Nhập vào tính toàn vẹn',
                value: '',
                spellcheck: false,
                required: false,
                icon: 'texture',
                css: 'col-12 col-lg-6',
                tab: 'CAIDAT',
            },
            {
                type: 'TEXT',
                name: 'dependenCodes',
                label: 'Field phụ thuộc',
                placeholder: 'Nhập vào field phụ thuộc',
                value: '',
                spellcheck: false,
                required: false,
                icon: 'texture',
                css: 'col-12 col-lg-6',
                tab: 'CAIDAT',
            },
            {
                type: 'TEXT',
                name: 'formula',
                label: 'Công thức',
                placeholder: 'Nhập vào công thức',
                value: '',
                spellcheck: false,
                required: false,
                icon: 'texture',
                css: 'col-12 col-lg-12',
                tab: 'CAIDAT',
            },
            {
                type: 'TEXTAREA',
                name: 'css',
                label: 'CSS',
                placeholder: 'Nhập vào CSS',
                value: '',
                spellcheck: false,
                required: false,
                icon: 'texture',
                css: 'col-12 col-lg-12',
                tab: 'CAIDAT',
            },
        ];

        const dialogRef = this.dialog.open(FieldConfigDialogComponent, {
            // disableClose: true,
            width: '70%',
            data: {
                fields: fields,
                datas: element,
            },
            panelClass: 'my-custom-dialog-class',
        });

        dialogRef.afterClosed().subscribe((result) => {
            if (!result) return;

            this.getInitData();
            this._toastrService.success('Cập nhật thành công', 'Success');
        });
    }

    createAction(): void {
        this.openDialog(null);
    }

    editAction(element: any): void {
        this.openDialog(element);
    }

    deleteListItem() {
        const numSelected = this.selection.selected;

        console.log('Check: ', numSelected);

        for (let item of numSelected) {
            let dataDelete = {
                id: item.id,
            };

            this._fieldService.deleteField(dataDelete).subscribe(
                (data) => {
                    this.getInitData();
                    this._toastrService.success(
                        'Cập nhật thành công',
                        'Success',
                    );
                },
                (err) => {
                    this._toastrService.errorServer(err);
                },
            );
        }
    }
}
