import { Component, Input, OnInit } from '@angular/core';
import { FormArray, FormGroup } from '@angular/forms';

@Component({
    selector: 'dynamic-form-builder',
    templateUrl: './dynamic-form-builder.component.html',
    styleUrls: ['./dynamic-form-builder.component.scss'],
})
export class DynamicFormBuilderComponent implements OnInit {
    @Input() form: FormGroup;
    @Input() fields: any[] = [];
    @Input() data: any;
    @Input() groupCode: string;

    isEnableDelete: boolean = false;

    formArrayField: string;
    formArrayValue: any;
    optionsValue: any;

    objTest: any = {
        id: 1,
        firstName: 'Doan',
        lastName: 'Vu',
        email: 'doanmeo267@gmail.com',
        description: 'Pro vip 123',
        country: 1,
        address: 'Phu Yen',
        gender: true,
        hobby: [1, 4],
    };

    constructor() {}

    ngOnInit() {}

    get formArrayElement() {
        return this.form.get(this.formArrayField) as FormArray;
    }

    getObjById(): void {
        // Handle Checkbox on Patch
        const arrayValue = this.formArrayElement.value
            .map((item, index) => this.optionsValue[index].key)
            .map((item, index) =>
                this.objTest[this.formArrayField].includes(item) ? true : false,
            );

        console.log('Check: ', this.optionsValue);
        this.form.patchValue(this.objTest);
        this.formArrayElement.patchValue(arrayValue);
    }

    onSubmit(): void {
        let formValues;

        // Handle Checkbox on Submit
        if (this.formArrayElement) {
            const selectedCheckboxes = this.formArrayElement.value
                .map((checked, index) =>
                    checked ? this.optionsValue[index].key : null,
                )
                .filter((checked) => checked !== null);

            formValues = {
                ...this.form.value,
                [this.formArrayField]: selectedCheckboxes,
            };
        } else {
            formValues = {
                ...this.form.value,
            };
        }

        return formValues;
        // this.submitForm.emit(formValues);
    }
}
