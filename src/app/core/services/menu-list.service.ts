import { Observable } from 'rxjs';
import { Injectable, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { LogData } from '../interfaces/log-data';
import { UtilsService } from './utils.service';
import { tap } from 'rxjs/operators';
import { TokenStorageService } from './token.service';
import { BASE_URL } from './data-service';

const TOKEN_KEY = 'auth-token';
const USER_KEY = 'auth-user';

@Injectable()
export class MenuListService implements OnInit {
    constructor(
        private http: HttpClient,
        private utils: UtilsService,
        private _tokenStorageService: TokenStorageService,
    ) {}

    ngOnInit(): void {}

    getAllMenuList(data: any): Observable<any> {
        const END_POINT = '/read/ThucDon/GetAll';

        return this.http.post<any>(`${BASE_URL}${END_POINT}`, data);
    }

    getMenuList(data: any): Observable<any> {
        const END_POINT = '/read/ThucDon/Get';

        return this.http.post<any>(`${BASE_URL}${END_POINT}`, data);
    }

    createMenuList(data: any): Observable<any> {
        const END_POINT = '/write/ThucDon/Create';

        return this.http.post<any>(`${BASE_URL}${END_POINT}`, data);
    }

    updateMenuList(data: any): Observable<any> {
        const END_POINT = '/write/ThucDon/Update';

        return this.http.post<any>(`${BASE_URL}${END_POINT}`, data);
    }

    deleteMenuList(data: any): Observable<any> {
        const END_POINT = '/write/ThucDon/Delete';

        return this.http.post<any>(`${BASE_URL}${END_POINT}`, data);
    }

    getAllFoodList(data: any): Observable<any> {
        const END_POINT = '/read/MonAn/GetAll';

        return this.http.post<any>(`${BASE_URL}${END_POINT}`, data);
    }

    getObjectEat(): Observable<any> {
        const END_POINT = '/read/DinhLuongAn/GetCurrent';

        return this.http.post<any>(`${BASE_URL}${END_POINT}`, null);
    }
}
