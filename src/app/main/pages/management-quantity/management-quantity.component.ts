import { SelectionModel } from '@angular/cdk/collections';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatPaginator, MatTableDataSource } from '@angular/material';
import { Router } from '@angular/router';
import { DynamicDialogComponent } from '@app/components/dynamic-dialog-component/dynamic-dialog-component.component';
import { DeleteConfirmDialogComponent } from '@app/_shared/dialogs/delete-confirm-dialog/delete-confirm-dialog.component';
import { CategoryService } from '@core/services/category.service';
import { ToastrService } from '@core/services/toastr.service';
import { MaterialDialogComponent } from '@app/_shared/dialogs/material-dialog/material-dialog.component';
import { PriceService } from '@core/services/price.service';
import { UserService } from '@core/services/user.service';
import { forkJoin, Observable } from 'rxjs';
import { PhieuXuatKhoService } from '@core/services/phieu-xuat-kho.service';
import { QuantityService } from '@core/services/quantity.service';
import {
    ElementSelect,
    mealList,
    quantityList,
} from './management-quantity-add-edit/management-quantity-add-edit.component';
import { CircularService } from '@core/services/circular-service';
import { CommonService } from '@core/services/common.service';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { InventoryService } from '@core/services/inventory.service';
import { DialogQuantitativeComponent } from '@app/_shared/dialogs/dialog-quantitative/dialog-quantitative.component';
import { DialogEatobjectComponent } from '@app/_shared/dialogs/dialog-eatobject/dialog-eatobject.component';

@Component({
    selector: 'app-management-quantity',
    templateUrl: './management-quantity.component.html',
    styleUrls: ['./management-quantity.component.scss'],
})
export class ManagementQuantityComponent implements OnInit {
    @ViewChild(MatPaginator) paginator!: MatPaginator;

    currentDate: Date = new Date();

    displayedColumnsOfField: string[] = [
        'tienAnCb',
        'tienAnOm',
        'tienAnLe',
        'tienAnLamNv',
        'dinhLuongAn',
        'actions',
    ];

    dataSource = new MatTableDataSource<any>([]);
    selection = new SelectionModel<any>(true, []);
    userList: any[] = [];
    exportWarehouseList: any[] = [];

    nameEatObject: string;
    nameCirculars: string | number;
    nameEatObjectEdit: string;
    mucTienAn: string | number;
    mucTienAnLe: string | number;
    mucTienAnOm: string | number;
    mucTienAnThemNv: string | number;
    circular: any;
    listEatObject: ElementSelect[] = [];
    listCirculars: ElementSelect[] = [];
    quantityCreateList: quantityList[] = [];
    mealRateCreateList: mealList[] = [];
    quantiveItemCurrent;
    listQuantityItem = [];
    listEatObjectDisable = [];

    isEdit: boolean = false;
    isEditDisabled: boolean = false;
    isEditReactiveForm: boolean = false;
    isSpinnerAddQuantive: boolean = false;
    isConfirmDeleteGroup: boolean[] = [];

    inventoryList: any[] = [];
    totalPrice: number = 0;
    currentTable: any[] = [];
    unitOptions: any[] = [];
    typeOptions: any[] = [];

    formQuantive = this._fb.group({
        quantity: this._fb.array([]),
    });

    allowExec: boolean = false;

    constructor(
        private _router: Router,
        public dialog: MatDialog,
        private _toastrService: ToastrService,
        private _categoryService: CategoryService,
        private _priceService: PriceService,
        private _userService: UserService,
        private _phieuXuatKhoService: PhieuXuatKhoService,
        private _quantityService: QuantityService,
        private _circularService: CircularService,
        private _commonService: CommonService,
        private _fb: FormBuilder,
        private _inventoryService: InventoryService,
    ) {}

    ngOnInit() {
        let request = {
            id: localStorage.getItem('idMenu'),
            language: 'vi',
        };

        this._commonService.checkPermission(request).subscribe((response) => {
            if (JSON.parse(response.result).some((obj) => obj.key === 'EXEC')) {
                this.allowExec = true;
            } else {
                this.displayedColumnsOfField.splice(5, 1);
            }

            this.getInitData(this.allowExec);
        });
    }

    getInitData(allowExec: boolean): void {
        const bodyOrganization = {
            criterias: [
                {
                    propertyName: 'Code',
                    operation: 6,
                    value: '',
                },
            ],
        };

        let bodyEatObject = {
            maxResultCount: 10,
            skipCount: 0,
            sorting: 'Code',
            criterias: [
                {
                    propertyName: 'GroupCode',
                    operation: 6,
                    value: 'doi-tuong-an',
                },
            ],
        };

        const dataEatObject =
            this._categoryService.getAllCategory(bodyEatObject);
        const dataCirculars = this._circularService.getAllCirculars({});

        const dataOrganization =
            this._userService.getOrganization(bodyOrganization);
        const dataPriceAdjust = this._priceService.getAllPriceAdjustment({});
        const getAllWarehouse = this._inventoryService.getAllWarehouse({});
        // const getUserService = this._userService.getUsers({});
        const getDataRaw = this._commonService.callDataAPIShort(
            '/api/services/read/DinhLuongAn/GetAll',
            {},
        );
        const getDataQuantity = this._commonService.callDataAPIShort(
            '/api/services/read/DinhLuongAn/GetCurrent',
            {},
        );

        forkJoin([
            dataOrganization,
            dataPriceAdjust,
            getAllWarehouse,
            // getUserService,
            dataEatObject, //listEatObject
            dataCirculars,
            getDataRaw,
            getDataQuantity,
        ]).subscribe((results) => {
            this.dataSource.data = results[6].result.sort(
                (a, b) =>
                    <any>new Date(b.creationTime) -
                    <any>new Date(a.creationTime),
            );

            this.circular = results[4].result.items.find(
                (item) => item.trangThai === 1,
            );
            // console.log('results::', results);
            // console.log('results[4].result::', results[4].result);
            // let rawlistEatObject = results[4].result;
            let rawListCirculars = results[4].result.items;
            // this.listQuantityItem = results[7].result.items;
            // console.log('circular::', this.circular);
            // this._commonService.covertDataToKeyValueInSelect(
            //     rawlistEatObject,
            //     this.listEatObject,
            //     'id',
            //     'name',
            // );
            this._commonService.covertDataToKeyValueInSelect(
                rawListCirculars,
                this.listCirculars,
                'id',
                'tenThongTu',
            );

            //Default Circulars
            if (this.listCirculars.length > 0) {
                this.nameCirculars = this.listCirculars[0].key;
            }

            // results[6].result.items.forEach((quantive) => {
            //     if (quantive.thongTuId === this.nameCirculars) {
            //         this.listQuantityItem.push(quantive);
            //     }
            // });
        });

        this.dataSource.paginator = this.paginator;
    }

    ngAfterViewInit() {
        this.dataSource.paginator = this.paginator;
        this.paginator._intl.itemsPerPageLabel = 'Items/Page:';
    }

    guid = () => {
        let s4 = () => {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        };
        //return id of format 'aaaaaaaa'-'aaaa'-'aaaa'-'aaaa'-'aaaaaaaaaaaa'
        return (
            s4() +
            s4() +
            '-' +
            s4() +
            '-' +
            s4() +
            '-' +
            s4() +
            '-' +
            s4() +
            s4() +
            s4()
        );
    };

    get quantity() {
        return this.formQuantive.get('quantity') as FormArray;
    }
    showQuantity(idDinhLuong): void {
        // let gUID = this._commonService.guid();

        const dialogRef = this.dialog.open(DialogQuantitativeComponent, {
            maxWidth: '1500px',
            width: '80%',
            data: {
                datas: idDinhLuong,
            },
            panelClass: 'my-custom-dialog-class',
        });
    }

    showEatObject(idDoiTuong): void {
        let fields = [];
        const dialogRef = this.dialog.open(DialogEatobjectComponent, {
            maxWidth: '1500px',
            width: '25%',
            data: {
                datas: idDoiTuong,
                fields: fields,
            },
            panelClass: 'my-custom-dialog-class',
        });
        dialogRef.componentInstance.onSave.subscribe((dialogValue) => {
            const isSubmitted = !!dialogValue;
            this.listEatObject = dialogValue;
            dialogRef.close(isSubmitted);
        });
    }

    createAction(): void {
        this._router.navigate([
            'manage/app/category/quan-ly-tieu-chuan-dinh-luong/create',
            { thongTuId: this.nameCirculars },
        ]);
    }

    handleAction(element: any): void {
        this._router.navigate([
            `manage/app/category/quan-ly-tieu-chuan-dinh-luong/edit/${element.id}`,
            { thongTuId: this.nameCirculars },
        ]);
    }

    resetData() {
        this.mucTienAn = 0;
        this.mucTienAnOm = 0;
        this.mucTienAnLe = 0;
        this.mucTienAnThemNv = 0;
        // this.quantityCreateList = [];
        // this.mealRateCreateList = [];
        this.isEditDisabled = false;
    }
    createQuantitive(quantitive): FormGroup {
        return this._fb.group({
            nhomDoiTuong: [quantitive.nhomDoiTuong],
            nhomDoiTuongId: [
                {
                    value: quantitive.nhomDoiTuongId,
                    disabled: true,
                },
            ],
            mucTienAn: [{ value: quantitive.mucTienAn, disabled: true }],
            mucTienAnOm: [{ value: quantitive.mucTienAnOm, disabled: true }],
            mucTienAnLe: [{ value: quantitive.mucTienAnLe, disabled: true }],
            mucTienAnThemNv: [
                { value: quantitive.mucTienAnThemNv, disabled: true },
            ],
            id: [quantitive.id],
        });
    }

    getDataQuantity(idQuantity?: any) {
        let body = {
            criterias: [
                {
                    propertyName: 'thongTuId',
                    operation: 0,
                    value: idQuantity,
                },
            ],
        };
        this._commonService
            .callDataAPIShort('/api/services/read/DinhLuongAn/GetAll', body)
            .subscribe((res) => {
                this.listQuantityItem = res.result.items;
                this.dataSource.data = res.result.items;

                this.formQuantive = this._fb.group({
                    quantity: this._fb.array(
                        this.listQuantityItem.map((quantive) =>
                            this.createQuantitive(quantive),
                        ),
                    ),
                });
            });
    }

    changeCirculars(value) {
        // console.log('value change select::', value);
        this.nameCirculars = value;
        this.getDataQuantity(value);
        this.resetData();
    }

    // handleAction(element: any, typeAction): void {
    //     // this._router.navigate([ manage/app/category/quan-ly-tieu-chuan-dinh-luong/edit/:id
    //     //     `manage/app/category/xuat-kho/edit/${element.id}`,
    //     //     { typeAction },
    //     // ]);
    // }

    deleteAction(element: any): void {
        const dialogRef = this.dialog.open(DeleteConfirmDialogComponent, {
            width: '500px',
            data: element,
        });
        dialogRef.componentInstance.handleDelete.subscribe((dataDelete) => {
            const isSubmitted = !!dataDelete;
            this._quantityService.deleteDinhLuongAn(dataDelete).subscribe(
                (res) => dialogRef.close(isSubmitted),
                (err) => this._toastrService.errorServer(err),
            );
        });
        dialogRef.afterClosed().subscribe((result) => {
            if (!result) return;
            this.getInitData(this.allowExec);
            this._toastrService.success('', 'Xóa thành công');
        });
    }

    // HANDLE SEARCH
    handleSearch(event: any): void {
        console.log('Search Info:: ', event);
    }

    //New
    applyFilter(event: Event) {
        const filterValue = (event.target as HTMLInputElement).value;
        console.log('event.target::', event.target);
        console.log('filterValue::', filterValue);
        this.dataSource.filter = filterValue;
    }
}
