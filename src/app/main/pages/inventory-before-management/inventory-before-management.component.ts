import { SelectionModel } from '@angular/cdk/collections';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatPaginator, MatTableDataSource } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { DynamicDialogComponent } from '@app/components/dynamic-dialog-component/dynamic-dialog-component.component';
import { DeleteConfirmDialogComponent } from '@app/_shared/dialogs/delete-confirm-dialog/delete-confirm-dialog.component';
import { CategoryService } from '@core/services/category.service';
import { ToastrService } from '@core/services/toastr.service';
import { MaterialDialogComponent } from '@app/_shared/dialogs/material-dialog/material-dialog.component';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { InventoryService } from '@core/services/inventory.service';
import { FoodFuelService } from '@core/services/food-fuel.service';
import { DataService } from '@core/services/data-service';
import { Subscription } from 'rxjs';

@Component({
    selector: 'app-inventory-before-management',
    templateUrl: './inventory-before-management.component.html',
    styleUrls: ['./inventory-before-management.component.scss'],
})
export class InventoryBeforeManagementComponent implements OnInit {
    @ViewChild(MatPaginator) paginator!: MatPaginator;

    currentDate: Date = new Date();

    displayedColumnsOfField: string[] = [
        'maLttpChatDot',
        'tenLttpChatDot',
        'soluongTonKho',
        'donViTinh',
        'donGia',
        'thanhTien',
        'actions',
    ];

    dataSource = new MatTableDataSource<any>([]);
    selection = new SelectionModel<any>(true, []);
    form: FormGroup;
    fields: any[] = [];
    warehouseList: any[] = [];
    foodFuelList: any[] = [];

    inventoryList: any[] = [];
    totalPrice: number = 0;
    currentTable: any[] = [];
    unitOptions: any[] = [];
    typeOptions: any[] = [];

    idInventory: number;
    nameWarehouse;
    subscription: Subscription;
    isLoading: boolean = false;

    constructor(
        private _router: Router,
        public dialog: MatDialog,
        private _toastrService: ToastrService,
        private _categoryService: CategoryService,
        private _inventoryService: InventoryService,
        private _foodFuelService: FoodFuelService,
        private _activatedroute: ActivatedRoute,
        private _dataService: DataService,
    ) {
        this.subscription = this._dataService.getData().subscribe((message) => {
            this.isLoading = true;
            this.getInitData(message.id);
        });
    }

    ngOnInit() {
        this.getMultiCategories();
        this.getInitData(this._dataService.getDauMoiBepId());
        this._activatedroute.queryParams.subscribe((params) => {
            this.idInventory = parseInt(params.idInventory);
            this.nameWarehouse = params.nameInventory;
        });
    }

    getMultiCategories() {
        this.getUnitOptions();
        this.getTypeOptions();
    }

    getBodyCategory(groupCode: string) {
        return {
            maxResultCount: 10,
            skipCount: 0,
            sorting: 'Code',
            criterias: [
                {
                    propertyName: 'GroupCode',
                    operation: 6,
                    value: groupCode,
                },
            ],
        };
    }

    getUnitOptions() {
        this._categoryService
            .getAllCategory(this.getBodyCategory('don-vi-tinh'))
            .subscribe((res) => {
                this.unitOptions = res.result.map((item) => ({
                    key: item.codeData,
                    name: item.name,
                }));
            });
    }

    getTypeOptions() {
        this._categoryService
            .getAllCategory(this.getBodyCategory('phan-loai-lttp-chat-dot'))
            .subscribe((res) => {
                this.typeOptions = res.result.map((item) => ({
                    key: item.codeData,
                    name: item.name,
                }));
            });
    }

    getInitData(id: number): void {
        this._inventoryService
            .getAllWarehouse({
                maxResultCount: 999999999,
                criterias: [
                    {
                        propertyName: 'dauMoiBepId',
                        operation: 0,
                        value: id,
                    },
                ],
            })
            .subscribe((res) => {
                this.warehouseList = res.result.items
                    .filter((item) => item.status === 'ENABLE')
                    .map((item) => ({ key: item.id, name: item.tenKho }));
                setTimeout(() => {
                    this.isLoading = false;
                }, 200);
                this.getInitForm();
            });
    }

    getInitForm(): void {
        if (this.idInventory) {
            this.fields = [
                {
                    type: 'DATETIME',
                    referenceValue: 'ngayNhap',
                    name: 'Ngày nhập tồn',
                    defaultValue: '',
                    required: '1',
                    css: 'col-12 col-lg-6',
                    appearance: 'legacy',
                },
            ];
        } else {
            this.fields = [
                {
                    type: 'SELECT',
                    referenceValue: 'maKho',
                    name: 'Kho tồn hàng',
                    defaultValue: '',
                    options: [...this.warehouseList],
                    required: '1',
                    css: 'col-12 col-lg-6',
                    appearance: 'legacy',
                },
                {
                    type: 'DATETIME',
                    referenceValue: 'ngayNhap',
                    name: 'Ngày nhập tồn',
                    defaultValue: '',
                    required: '1',
                    css: 'col-12 col-lg-6',
                    appearance: 'legacy',
                },
            ];
        }

        let fieldsCtrls = {};
        let fieldList = [];

        for (let f of this.fields) {
            fieldList = [...fieldList, f];

            if (f.type === 'NUMBER') {
                fieldsCtrls[f.referenceValue] = new FormControl(
                    { value: f.defaultValue, disabled: f.disabled },
                    f.required === '1' ? [Validators.required] : [],
                );
            } else if (f.type !== 'CHECKBOX') {
                let validators = [
                    Validators.minLength(f.minLength),
                    Validators.maxLength(f.maxLength),
                    Validators.pattern(f.pattern),
                ];

                if (f.type === 'EMAIL') {
                    validators = [...validators, Validators.email];
                }

                fieldsCtrls[f.referenceValue] = new FormControl(
                    {
                        value: f.defaultValue || '',
                        disabled: f.disabled,
                    },

                    f.required === '1'
                        ? [...validators, Validators.required]
                        : [...validators],
                );
            } else {
                //if checkbox, it need multiple
                let opts = {};
                for (let opt of f.options) {
                    opts[opt.key] = new FormControl({
                        value: opt.value,
                        disabled: f.disabled,
                    });
                }
            }
        }

        this.form = new FormGroup(fieldsCtrls);
    }

    /** Whether the number of selected elements matches the total number of rows. */
    isAllSelected() {
        const numSelected = this.selection.selected.length;
        const numRows = this.dataSource.data.length;
        return numSelected === numRows;
    }

    /** Selects all rows if they are not all selected; otherwise clear selection. */
    toggleAllRows() {
        if (this.isAllSelected()) {
            this.selection.clear();
            return;
        }

        this.selection.select(...this.dataSource.data);
    }

    /** The label for the checkbox on the passed row */
    checkboxLabel(row?: any): string {
        if (!row) {
            return `${this.isAllSelected() ? 'deselect' : 'select'} all`;
        }
        return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${
            row.index + 1
        }`;
    }

    guid = () => {
        let s4 = () => {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        };
        //return id of format 'aaaaaaaa'-'aaaa'-'aaaa'-'aaaa'-'aaaaaaaaaaaa'
        return (
            s4() +
            s4() +
            '-' +
            s4() +
            '-' +
            s4() +
            '-' +
            s4() +
            '-' +
            s4() +
            s4() +
            s4()
        );
    };

    openDialog(dataDialog: any): void {
        console.log('dataDialog:: ', dataDialog);
        let fields = [
            {
                type: 'TEXT',
                referenceValue: 'maLttpChatDot',
                name: 'Mã VT, HH',
                defaultValue: '',
                required: '1',
                pattern: '^[a-zA-Z0-9-]*$',
                icon: 'code',
                disabled: true,
                css: 'col-12 col-lg-6',
            },
            {
                type: 'TEXT',
                referenceValue: 'tenLttpChatDot',
                name: 'Tên VT, HH',
                defaultValue: '',
                required: '1',
                icon: 'restaurant',
                disabled: true,
                css: 'col-12 col-lg-6',
            },
            {
                type: 'SELECT',
                referenceValue: 'phanLoai',
                name: 'Phân loại nguyên vật liệu',
                defaultValue: '',
                required: '0',
                options: [...this.typeOptions],
                disabled: true,
                css: 'col-12 col-lg-6',
            },
            {
                type: 'SELECT',
                referenceValue: 'donViTinh',
                name: 'Đơn vị tính',
                defaultValue: '',
                required: '0',
                options: [...this.unitOptions],
                disabled: true,
                css: 'col-12 col-lg-6',
            },
            {
                type: 'NUMBER',
                referenceValue: 'soluongTonKho',
                name: 'Số lượng',
                defaultValue: '',
                required: '1',
                icon: 'filter_vintage',
                css: 'col-12 col-lg-6',
            },
            {
                type: 'NUMBER',
                referenceValue: 'donGia',
                name: 'Đơn giá',
                defaultValue: '',
                required: '1',
                icon: 'filter_vintage',
                css: 'col-12 col-lg-6',
            },
        ];

        let dialogRef = this.dialog.open(DynamicDialogComponent, {
            // disableClose: true,
            width: '70%',
            data: {
                datas: dataDialog,
                fields: fields,
                typeDialog: 'INVENTORY_BEFORE',
            },
            panelClass: 'my-custom-dialog-class',
        });

        dialogRef.componentInstance.onSave.subscribe((formValues) => {
            const isSubmitted = !!formValues;

            let dataSubmit = {
                ...dataDialog,
                ...formValues,
            };

            if (!dataDialog) {
                dataSubmit.id = this.guid();
                dataSubmit.creationTime = this.currentDate;

                this.dataSource.data = [...this.dataSource.data, dataSubmit];
            } else {
                const dataIndex = this.dataSource.data.findIndex(
                    (item) => item.id === dataDialog.id,
                );

                const currentData = [...this.dataSource.data];
                currentData.splice(dataIndex, 1, dataSubmit);
                this.dataSource.data = currentData;
                this.getTotalPrice();
            }
            dialogRef.close(isSubmitted);
        });

        dialogRef.componentInstance.handleDelete.subscribe((dataDelete) => {
            const isDeleted = !!dataDelete;
        });

        dialogRef.afterClosed().subscribe((result) => {
            if (!result) return;

            if (!dataDialog) {
                this._toastrService.success('', 'Thêm thành công');
            } else {
                this._toastrService.success('', 'Cập nhật thành công');
            }
        });
    }

    addRow(): void {
        const dialogRef = this.dialog.open(MaterialDialogComponent, {
            // disableClose: true,
            width: '70%',
            data: {
                type: 'INVENTORY',
                currentTable: this.currentTable,
            },
            panelClass: 'add-field-dialog-class',
        });

        dialogRef.componentInstance.onSave.subscribe((data) => {
            const isSubmitted = !!data;

            this.currentTable = [...data, ...this.dataSource.data].map(
                (item) => ({
                    ...item,
                    ton: item.soluongTonKho,
                    tonDauKy: item.soluongTonKho,
                }),
            );
            this.dataSource.data = this.currentTable;
            this.dataSource.paginator = this.paginator;

            this.getTotalPrice();

            dialogRef.close(isSubmitted);
        });

        dialogRef.afterClosed().subscribe((result) => {
            if (!result) return;

            this._toastrService.success('', 'Thêm thành công');
        });
    }

    getTotalPrice() {
        this.dataSource.data.forEach((item) => {
            item.thanhTien = item.ton * item.donGia;
        });

        this.totalPrice = this.dataSource.data.reduce(
            (acc, item) => acc + item.thanhTien,
            0,
        );
    }

    editAction(element: any) {
        this.openDialog(element);
    }

    deleteAction(element: any): void {
        const dialogRef = this.dialog.open(DeleteConfirmDialogComponent, {
            width: '500px',
            data: element,
        });

        dialogRef.componentInstance.handleDelete.subscribe((dataDelete) => {
            const isSubmitted = !!dataDelete;
            dialogRef.close(isSubmitted);
        });

        dialogRef.afterClosed().subscribe((result) => {
            if (!result) return;

            const index: number = this.currentTable.indexOf(element);
            if (index !== -1) {
                this.currentTable.splice(index, 1);
            }

            this.dataSource.data = this.currentTable;
            this.getTotalPrice();
            this._toastrService.success('', 'Xóa thành công');
        });
    }

    onBack(): void {
        if (this.idInventory) {
            this._router.navigate(
                ['manage/app/category/ton-kho/' + this.idInventory],
                {
                    queryParams: {
                        id: this.idInventory,
                        name: this.nameWarehouse,
                    },
                },
            );
        }
    }

    onSubmit(): void {
        let warehouse;

        if (this.idInventory) {
            this.currentTable.forEach((item) => {
                item.khoId = this.idInventory;
                item.ngayNhap = this.form.value.ngayNhap;
            });

            warehouse = this.warehouseList.find(
                (item) => item.key === this.idInventory,
            );
        } else {
            this.currentTable.forEach((item) => {
                item.khoId = this.form.value.maKho;
                item.ngayNhap = this.form.value.ngayNhap;
            });

            warehouse = this.warehouseList.find(
                (item) => item.key === this.form.value.maKho,
            );
        }

        this._inventoryService.createListInventory(this.currentTable).subscribe(
            (res) => {
                this.getInitData(this._dataService.getDauMoiBepId());
                this._toastrService.success('', 'Thêm thành công');
                this._router.navigate(
                    ['manage/app/category/ton-kho/' + warehouse.key],
                    {
                        queryParams: {
                            id: warehouse.key,
                            name: warehouse.name,
                        },
                    },
                );
            },
            (err) => this._toastrService.errorServer(err),
        );
    }
}
