import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListOfFoodPlansComponent } from './list-of-food-plans.component';

describe('ListOfFoodPlansComponent', () => {
    let component: ListOfFoodPlansComponent;
    let fixture: ComponentFixture<ListOfFoodPlansComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ListOfFoodPlansComponent],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ListOfFoodPlansComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
