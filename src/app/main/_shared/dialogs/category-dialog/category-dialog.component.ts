import {
    AfterViewChecked,
    AfterViewInit,
    ChangeDetectorRef,
    Component, Inject,
    OnInit
} from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { CategoryService } from '@core/services/category.service';

export interface DialogData {
    name: string;
    fields: any[];
    datas: any;
    groupCode: string;
}

@Component({
    selector: 'app-category-dialog',
    templateUrl: './category-dialog.component.html',
    styleUrls: ['./category-dialog.component.scss'],
})
export class CategoryDialogComponent
    implements OnInit, AfterViewInit, AfterViewChecked {
    lyDoOptions: any[] = [];

    constructor(
        private readonly changeDetectorRef: ChangeDetectorRef,
        private _categoryService: CategoryService,
        public dialog: MatDialog,
        public dialogRef: MatDialogRef<CategoryDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: DialogData,
    ) { }

    ngOnInit() {
        console.log("data:: ", this.data);
        this.getAllLydo();
    }

    ngAfterViewInit() { }

    ngAfterViewChecked(): void {
        this.changeDetectorRef.detectChanges();
    }

    getAllLydo(): void {
        const bodyLydo = {
            sorting: 'Code',
            criterias: [
                {
                    propertyName: 'GroupCode',
                    operation: 6,
                    value: 'quan-so-khong-an',
                },
            ],
        };

        this._categoryService.getAllCategory(bodyLydo).subscribe(
            (res) => {
                this.lyDoOptions = res.result.map((item) => ({
                    key: item.id,
                    name: item.name,
                }))
                console.log(this.getLyDoKhongAn(this.data['lyDoKhongAn']))
            }
        )
    }

    getLyDoKhongAn(lyDoId: any) {
        let result;

        if (this.lyDoOptions.length > 0) {
            const dataFound = this.lyDoOptions.find(
                (item) => item.key === lyDoId
            )

            result = dataFound ? dataFound['name'] : '';
        }

        return result;
    }
}
