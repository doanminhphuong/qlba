import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {
    CanActivate,
    ActivatedRouteSnapshot,
    RouterStateSnapshot,
    UrlTree,
    Router,
} from '@angular/router';
import { HoSoSucKhoeService } from '@core/services/ho-so-suc-khoe.service';
import { TokenStorageService } from '@core/services/token.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

const TOKEN_KEY = 'auth-token';

@Injectable({
    providedIn: 'root', // you can change to any level if needed
})
export class CanHomepageCallAPIGuard implements CanActivate {
    currentUser;
    constructor(
        private _tokenStorageService: TokenStorageService,
        private _router: Router,
        public _hssk: HoSoSucKhoeService,
    ) {}
    canActivate(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot,
    ):
        | Observable<boolean | UrlTree>
        | Promise<boolean | UrlTree>
        | boolean
        | UrlTree {
        let userInfo = this._hssk.getCurrentUsers();
        userInfo.subscribe(
            (data) => {},
            (err) => {
                this.currentUser = 'Here';
                this._tokenStorageService.signOut();
                this._router.navigateByUrl('/login');
            },
        );

        if (this.currentUser) {
            return false;
        } else {
            return true;
        }
    }

    getCurrentUser() {
        const TOKEN_KEY = 'auth-token';

        this._hssk.getCurrentUsers().subscribe(
            (data) => {
                this.currentUser = data.result;
            },
            (err: HttpErrorResponse) => {},
        );
    }
}
