import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BASE_URL } from './data-service';
import { TokenStorageService } from './token.service';
@Injectable({
    providedIn: 'root',
})
export class QuantityService {
    constructor(
        private http: HttpClient,
        private _tokenStorageService: TokenStorageService,
    ) {}

    getAllQuantity(data: any): Observable<any> {
        const END_POINT = '/read/DinhLuongAn/GetAll';

        return this.http.post<any>(`${BASE_URL}${END_POINT}`, data);
    }

    getAllDinhLuongAnOrigin(data: any): Observable<any> {
        let token = this._tokenStorageService.getToken();
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
        });

        let options = { headers: headers };
        const END_POINT = '/read/DinhLuongAn/GetAll';

        return this.http.post<any>(`${BASE_URL}${END_POINT}`, data, options);
    }

    createDinhLuongAn(data: any): Observable<any> {
        const END_POINT = '/write/DinhLuongAn/Create';

        return this.http.post<any>(`${BASE_URL}${END_POINT}`, data);
    }

    updateDinhLuongAn(data: any): Observable<any> {
        const END_POINT = '/write/DinhLuongAn/Update';

        return this.http.post<any>(`${BASE_URL}${END_POINT}`, data);
    }

    getDinhLuongAnById(id: any): Observable<any> {
        const END_POINT = '/read/DinhLuongAn/Get';

        return this.http.post<any>(`${BASE_URL}${END_POINT}`, id);
    }

    deleteDinhLuongAn(id: string): Observable<any> {
        const END_POINT = '/write/DinhLuongAn/Delete';

        return this.http.post<any>(`${BASE_URL}${END_POINT}`, id);
    }
}
