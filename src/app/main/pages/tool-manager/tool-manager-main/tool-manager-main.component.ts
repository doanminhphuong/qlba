import { SelectionModel } from '@angular/cdk/collections';
import {
    AfterViewInit,
    Component,
    OnDestroy,
    OnInit,
    ViewChild,
} from '@angular/core';
import {
    MatDialog,
    MatPaginator,
    MatTableDataSource,
    PageEvent,
} from '@angular/material';
import { Router } from '@angular/router';
import { DynamicDialogComponent } from '@app/components/dynamic-dialog-component/dynamic-dialog-component.component';
import { DeleteConfirmDialogComponent } from '@app/_shared/dialogs/delete-confirm-dialog/delete-confirm-dialog.component';
import { CategoryService } from '@core/services/category.service';
import { CommonService } from '@core/services/common.service';
import { DataService } from '@core/services/data-service';
import { ToastrService } from '@core/services/toastr.service';
import { Subscription } from 'rxjs';
@Component({
    selector: 'app-tool-manager-main',
    templateUrl: './tool-manager-main.component.html',
    styleUrls: ['./tool-manager-main.component.scss'],
})
export class ToolManagerMainComponent implements OnInit {
    @ViewChild(MatPaginator) paginator!: MatPaginator;
    subscription: Subscription;

    // Table
    displayedColumnsOfField: string[] = [
        'maDungCuCapDuong',
        'tenDungCuCapDuong',
        'donViTinhId',
        'actions',
    ];
    dataSource = new MatTableDataSource<any>([]);
    selection = new SelectionModel<any>(true, []);

    // Pagination
    totalRows = 0;
    pageSize = 10;
    currentPage = 0;
    pageSizeOptions: number[] = [5, 10, 25, 50];
    dataSkipped: number = 0;

    // Init
    dauMoiBepId: number;
    tenDungCuCapDuong: string;
    unitOptions: any[] = [];

    // Boolean
    isLoading: boolean = true;
    isFiltering: boolean = false;

    constructor(
        private _router: Router,
        public dialog: MatDialog,
        private _toastrService: ToastrService,
        private _categoryService: CategoryService,
        private _commonService: CommonService,
        private _dataService: DataService,
    ) {
        this.subscription = this._dataService.getData().subscribe((message) => {
            this.isLoading = true;
            this.getInitData(message.id);
        });
    }

    ngOnInit() {
        this.dauMoiBepId = this._dataService.getDauMoiBepId();
        this.getInitData(this.dauMoiBepId);
        this.getUnitOptions();
    }

    ngAfterViewInit() {
        this.dataSource.paginator = this.paginator;
    }

    ngOnDestroy(): void {
        this.subscription.unsubscribe();
    }

    getBodyCategory(groupCode: string) {
        return {
            maxResultCount: 10,
            skipCount: 0,
            sorting: 'Code',
            criterias: [
                {
                    propertyName: 'GroupCode',
                    operation: 6,
                    value: groupCode,
                },
            ],
        };
    }

    getUnitOptions() {
        this._categoryService
            .getAllCategory(this.getBodyCategory('don-vi-tinh'))
            .subscribe((res) => {
                this.unitOptions = res.result.map((item) => ({
                    key: item.id,
                    name: item.name,
                }));
            });
    }

    getInitData(dauMoiBepId: number): void {
        this.dauMoiBepId = dauMoiBepId;

        const body = {
            maxResultCount: this.pageSize,
            skipCount: this.dataSkipped,
            sorting: 'Id',
            criterias: [
                {
                    propertyName: 'dauMoiBepId',
                    operation: 0,
                    value: dauMoiBepId,
                },
            ],
        };

        this._commonService
            .callDataAPIShort('/api/services/read/DungCuCapDuong/GetAll', body)
            .subscribe((res) => {
                this.dataSource.data = res.result.items;
                setTimeout(() => {
                    this.paginator.pageIndex = this.currentPage;
                    this.paginator.length = res.result.totalCount;
                    this.isLoading = false;
                }, 200);
            });
    }

    pageChanged(event: PageEvent) {
        if (this.isFiltering) return;

        this.currentPage = event.pageIndex;
        this.dataSkipped = event.pageIndex * 10;

        this.getInitData(this.dauMoiBepId);
    }

    openDialog(dataDialog: any): void {
        console.log('dataDialog:: ', dataDialog);

        const fields = [
            {
                type: 'TEXT',
                referenceValue: 'maDungCuCapDuong',
                name: 'Mã dụng cụ cấp dưỡng',
                defaultValue: '',
                required: '1',
                icon: 'code',
                css: 'col-12 col-lg-6',
            },
            {
                type: 'SELECT',
                referenceValue: 'donViTinhId',
                name: 'Đơn vị tính',
                defaultValue: '',
                required: '1',
                options: [...this.unitOptions],
                css: 'col-12 col-lg-6',
            },
            {
                type: 'TEXT',
                referenceValue: 'tenDungCuCapDuong',
                name: 'Tên dụng cụ cấp dưỡng',
                defaultValue: '',
                required: '1',
                icon: 'restaurant',
                css: 'col-12 col-lg-6',
            },
        ];

        let dialogRef = this.dialog.open(DynamicDialogComponent, {
            // disableClose: true,
            width: '70%',
            data: {
                datas: dataDialog,
                fields: fields,
                typeDialog: 'lttp',
            },
            panelClass: 'my-custom-dialog-class',
        });

        dialogRef.componentInstance.onSave.subscribe((formValues) => {
            const isSubmitted = !!formValues;

            let dataSubmit = {
                ...dataDialog,
                ...formValues,
                dauMoiBepId: this.dauMoiBepId,
            };

            console.log('dataSubmit:: ', dataSubmit);

            if (!dataDialog) {
                this._commonService
                    .callDataAPIShort(
                        '/api/services/write/DungCuCapDuong/Create',
                        dataSubmit,
                    )
                    .subscribe(
                        (res) => dialogRef.close(isSubmitted),
                        (err) => this._toastrService.errorServer(err),
                    );
            } else {
                this._commonService
                    .callDataAPIShort(
                        '/api/services/write/DungCuCapDuong/Update',
                        dataSubmit,
                    )
                    .subscribe(
                        (res) => dialogRef.close(isSubmitted),
                        (err) => this._toastrService.errorServer(err),
                    );
            }
        });

        dialogRef.componentInstance.handleDelete.subscribe((dataDelete) => {
            const isDeleted = !!dataDelete;
        });

        dialogRef.afterClosed().subscribe((result) => {
            if (!result) return;

            if (this.isFiltering) {
                const propertyName = 'tenDungCuCapDuong';
                const operation = 6;
                const value = this.tenDungCuCapDuong;

                this.getAllDataFilter(propertyName, operation, value);
            } else {
                this.getInitData(this.dauMoiBepId);
            }

            if (!dataDialog) {
                this._toastrService.success('', 'Thêm thành công');
            } else {
                this._toastrService.success('', 'Cập nhật thành công');
            }
        });
    }

    createAction(): void {
        this.openDialog(null);
    }

    editAction(element: any): void {
        this.openDialog(element);
    }

    deleteAction(element: any): void {
        const dialogRef = this.dialog.open(DeleteConfirmDialogComponent, {
            width: '500px',
            data: element,
        });

        dialogRef.componentInstance.handleDelete.subscribe((dataDelete) => {
            const isSubmitted = !!dataDelete;

            this._commonService
                .callDataAPIShort(
                    '/api/services/write/DungCuCapDuong/Delete',
                    dataDelete,
                )
                .subscribe(
                    (res) => dialogRef.close(isSubmitted),
                    (err) => this._toastrService.errorServer(err),
                );
        });

        dialogRef.afterClosed().subscribe((result) => {
            if (!result) return;

            this.getInitData(this.dauMoiBepId);
            this._toastrService.success('', 'Xóa thành công');
        });
    }

    getAllDataFilter(
        propertyName: string,
        operation: number | string,
        value: any,
    ) {
        const bodyFilter = {
            maxResultCount: 9999,
            skipCount: 0,
            sorting: 'Id',
            criterias: [
                {
                    propertyName: 'dauMoiBepId',
                    operation: 0,
                    value: this.dauMoiBepId,
                },
                {
                    propertyName,
                    operation,
                    value,
                },
            ],
        };

        this._commonService
            .callDataAPIShort(
                '/api/services/read/DungCuCapDuong/GetAll',
                bodyFilter,
            )
            .subscribe((res) => {
                this.dataSource.data = res.result.items;
            });
    }

    handleInputFilter(filterValue: string) {
        this.tenDungCuCapDuong = filterValue.trim().toLowerCase();

        if (!this.tenDungCuCapDuong) {
            this.isFiltering = false;
            this.getInitData(this.dauMoiBepId);
        } else {
            const propertyName = 'tenDungCuCapDuong';
            const operation = 6;
            const value = this.tenDungCuCapDuong;

            this.getAllDataFilter(propertyName, operation, value);
            this.isFiltering = true;
        }
    }
}
