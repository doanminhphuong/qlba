import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteConfirmAllDialogComponent } from './delete-confirm-all-dialog.component';

describe('DeleteConfirmAllDialogComponent', () => {
  let component: DeleteConfirmAllDialogComponent;
  let fixture: ComponentFixture<DeleteConfirmAllDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeleteConfirmAllDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteConfirmAllDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
