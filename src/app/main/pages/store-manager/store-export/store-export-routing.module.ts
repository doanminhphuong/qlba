import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CustomerExportComponent } from './customer-export/customer-export.component';
import { FieldExportComponent } from './field-export/field-export.component';
import { ListExportAddEditComponent } from './list-export/list-export-add-edit/list-export-add-edit.component';
import { ListExportComponent } from './list-export/list-export.component';
import { SummaryExportAddEditComponent } from './summary-export/summary-export-add-edit/summary-export-add-edit.component';
import { SummaryExportComponent } from './summary-export/summary-export.component';

const routes: Routes = [
    {
        path: '',
        redirectTo: 'xuat-kho',
        pathMatch: 'full',
    },
    {
        path: 'xuat-kho',
        children: [
            {
                path: '',
                component: ListExportComponent,
            },
            {
                path: 'create',
                component: ListExportAddEditComponent,
                pathMatch: 'full',
            },

            {
                path: 'edit/:id',
                component: ListExportAddEditComponent,
            },
        ],
    },
    {
        path: 'tao-phieu-xuat-kho',
        component: FieldExportComponent,
        pathMatch: 'full',
    },
    {
        path: 'tao-phieu-xuat-khac',
        component: FieldExportComponent,
        pathMatch: 'full',
    },
    {
        path: 'tong-xuat-kho',
        // component: SummaryExportComponent,
        children: [
            {
                path: '',
                component: SummaryExportComponent,
            },
            {
                path: 'create',
                component: SummaryExportAddEditComponent,
            },
            {
                path: 'detail/:id',
                component: SummaryExportAddEditComponent,
            },
        ],
    },

    // {
    //     path: 'create',
    //     component: ListExportAddEditComponent,
    // },
    // {
    //     path: 'edit/:id',
    //     component: ListExportAddEditComponent,
    // },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class StoreExportRoutingModule {}
