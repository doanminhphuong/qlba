import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExportWarehouseAddEditComponent } from './export-warehouse-add-edit.component';

describe('ExportWarehouseAddEditComponent', () => {
  let component: ExportWarehouseAddEditComponent;
  let fixture: ComponentFixture<ExportWarehouseAddEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExportWarehouseAddEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExportWarehouseAddEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
