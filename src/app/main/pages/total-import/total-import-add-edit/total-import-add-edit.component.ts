import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatPaginator } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { CommonService } from '@core/services/common.service';
import { DataService } from '@core/services/data-service';
import { ExportExcelService } from '@core/services/export-excel.service';
import { InventoryService } from '@core/services/inventory.service';
import { ToastrService } from '@core/services/toastr.service';
import { forkJoin, Subscription } from 'rxjs';

@Component({
    selector: 'app-total-import-add-edit',
    templateUrl: './total-import-add-edit.component.html',
    styleUrls: ['./total-import-add-edit.component.scss'],
})
export class TotalImportAddEditComponent implements OnInit {
    form: FormGroup;
    field: any[] = [];
    warehouseList: any[] = [];
    listLTTP: any[] = [];
    dataDetail: any;
    dataInfo: any;
    dataEdit: any;
    isEdit: boolean = false;
    isExportExcel: boolean = false;

    id: number = 0;
    typeAction: number;
    idPhieu: number;
    subscription: Subscription;
    isLoading: boolean = false;

    constructor(
        private _router: Router,
        private _inventoryService: InventoryService,
        private _commonService: CommonService,
        private _toastrService: ToastrService,
        private _activatedRoute: ActivatedRoute,
        private _exportService: ExportExcelService,
        private _dataService: DataService,
    ) {
        this.subscription = this._dataService.getData().subscribe((message) => {
            if (this.isEdit === false) {
                this._dataService.changeDauMoiBep(message.id);
                this.getInitData(message.id);
                this.isLoading = true;
            }
        });
    }

    ngOnInit() {
        this._activatedRoute.paramMap.subscribe((param) => {
            this.idPhieu = +param.get('id');
            this.typeAction = +param.get('typeAction');
            if (!this.idPhieu) return;
            this.isEdit = true;
        });
        setTimeout(() => {
            this.getInitData(this._dataService.getDauMoiBepId());
        }, 1000);
    }

    getInitData(id: number) {
        const dataWarehouse = this._inventoryService.getAllWarehouse({
            maxResultCount: 2147483647,
            criterias: [
                {
                    propertyName: 'dauMoiBepId',
                    operation: 0,
                    value: id,
                },
            ],
        });

        const dataLTTP = this._commonService.callDataAPIShort(
            '/api/services/read/LttpChatDot/GetAll',
            { maxResultCount: 2147483647 },
        );

        forkJoin([dataWarehouse, dataLTTP]).subscribe((results) => {
            this.warehouseList = results[0].result.items
                .filter((item) => item.status === 'ENABLE')
                .map((item) => ({
                    key: item.id,
                    name: item.tenKho,
                }));

            this.listLTTP = results[1].result.items
                .filter((item) => item.status === 'ENABLE')
                .map((item) => ({
                    key: item.id,
                    name: item.tenLttpChatDot,
                    donViTinh: item.donViTinh,
                }));

            if (this.warehouseList.length > 0) {
                this.field = [
                    {
                        type: 'DATETIME',
                        referenceValue: 'ngayTaoPhieu',
                        name: 'Ngày tổng nhập',
                        defaultValue: '',
                        required: '1',
                        css: 'col-12 col-lg-3',
                        appearance: 'legacy',
                        min: new Date(),
                        disabled:
                            this.typeAction === 3 || this.isEdit ? true : false,
                    },
                    {
                        type: 'TEXT',
                        referenceValue: 'maPhieu',
                        name: 'Mã phiếu tổng nhập',
                        defaultValue: '',
                        required: '1',
                        css: 'col-12 col-lg-3',
                        appearance: 'legacy',
                        disabled:
                            this.typeAction === 3 || this.isEdit ? true : false,
                    },
                    {
                        type: 'DATETIME',
                        referenceValue: 'ngayHetHan',
                        name: 'Ngày hết hạn',
                        defaultValue: '',
                        required: '1',
                        css: 'col-12 col-lg-3',
                        appearance: 'legacy',
                        dependenField: 'ngayTaoPhieu',
                        disabled:
                            this.typeAction === 3
                                ? true
                                : this.isEdit === true
                                    ? false
                                    : true,
                    },
                    {
                        type: 'SELECT',
                        referenceValue: 'khoNhanId',
                        name: 'Chọn kho nhận hàng',
                        defaultValue: '',
                        options: [...this.warehouseList],
                        search: '1',
                        required: '1',
                        searchCtrl: 'searchCtrl',
                        css: 'col-12 col-lg-3',
                        appearance: 'legacy',
                        selectionChange: ($event) =>
                            this.showData($event.value),
                        disabled:
                            this.typeAction === 3
                                ? true
                                : this.isEdit === true
                                    ? false
                                    : true,
                    },
                    {
                        type: 'TEXT',
                        referenceValue: 'nguoiVietPhieu',
                        name: 'Người viết phiếu',
                        defaultValue: this._dataService.getNguoiVietPhieu(),
                        required: '0',
                        css: 'col-12 col-lg-3',
                        appearance: 'legacy',
                        disabled: this.typeAction === 3 ? true : false,
                    },
                    {
                        type: 'TEXT',
                        referenceValue: 'trucBan',
                        name: 'Trực ban',
                        defaultValue: this._dataService.getTrucBan(),
                        required: '0',
                        css: 'col-12 col-lg-3',
                        appearance: 'legacy',
                        disabled: this.typeAction === 3 ? true : false,
                    },
                    {
                        type: 'TEXT',
                        referenceValue: 'nguoiNhan',
                        name: 'Người nhận',
                        defaultValue: this._dataService.getNguoiNhan(),
                        required: '0',
                        css: 'col-12 col-lg-3',
                        appearance: 'legacy',
                        disabled: this.typeAction === 3 ? true : false,
                    },
                    {
                        type: 'TEXT',
                        referenceValue: 'nguoiDuyet',
                        name: 'Người duyệt',
                        defaultValue: this._dataService.getNguoiDuyet(),
                        required: '0',
                        css: 'col-12 col-lg-3',
                        appearance: 'legacy',
                        disabled: this.typeAction === 3 ? true : false,
                    },
                ];
                this.initForm();

                setTimeout(() => {
                    this.isLoading = false;
                }, 200);
            }
        });
    }

    initForm() {
        let fieldsCtrls = {};
        let fieldList = [];

        for (let f of this.field) {
            fieldList = [...fieldList, f];

            if (f.type === 'NUMBER') {
                fieldsCtrls[f.referenceValue] = new FormControl(
                    { value: f.defaultValue, disabled: f.disabled },
                    f.required === '1' ? [Validators.required] : [],
                );
            } else if (f.type !== 'CHECKBOX') {
                let validators = [
                    Validators.minLength(f.minLength),
                    Validators.maxLength(f.maxLength),
                    Validators.pattern(f.pattern),
                ];

                if (f.type === 'EMAIL') {
                    validators = [...validators, Validators.email];
                }

                fieldsCtrls[f.referenceValue] = new FormControl(
                    {
                        value: f.defaultValue || '',
                        disabled: f.disabled,
                    },

                    f.required === '1'
                        ? [...validators, Validators.required]
                        : [...validators],
                );

                if (f.type === 'DATETIME') {
                    if (f.referenceValue === 'ngayTaoPhieu') {
                        fieldsCtrls[f.referenceValue].valueChanges.subscribe(
                            (val) => {
                                const KEY = 'PNK';
                                if (this.isEdit) {
                                    return;
                                }
                                let date = new Date(val);
                                let part3 = date
                                    .toLocaleDateString('en-GB')
                                    .replace(/[^0-9]/g, '');

                                let fullPath = KEY + '.' + part3;
                                this.form.patchValue({
                                    maPhieu: fullPath,
                                });

                                const dependField = fieldList.find(
                                    (x) => x.dependenField === f.referenceValue,
                                );

                                if (!dependField) return;

                                dependField.min = val;
                                fieldsCtrls[
                                    dependField.referenceValue
                                ].enable();
                                fieldsCtrls['khoNhanId'].enable();
                            },
                        );
                    }
                }
            } else {
                //if checkbox, it need multiple
                let opts = {};
                for (let opt of f.options) {
                    opts[opt.key] = new FormControl({
                        value: opt.value,
                        disabled: f.disabled,
                    });
                }
            }
        }

        this.form = new FormGroup(fieldsCtrls);

        if (this.isEdit) {
            this.getDataDetail();
        }
    }

    getDataDetail() {
        this._commonService
            .callDataAPIShort('/api/services/read/PhieuTongNhap/Get', {
                id: this.idPhieu,
            })
            .subscribe((response) => {
                console.log('response::', response);
                this.dataEdit = response.result;

                this.dataDetail = this.dataEdit.chiTiet;

                this.form.patchValue({
                    ...this.dataEdit,
                    nguoiVietPhieu: this.dataEdit.nguoiVietPhieu,
                    trucBan: this.dataEdit.trucBan,
                    nguoiNhan: this.dataEdit.nguoiNhan,
                    nguoiDuyet: this.dataEdit.nguoiDuyet,
                });
            });
    }

    showData(data: number) {
        let bodySubmit = {
            khoNhanId: data,
            ngayTaoPhieu: this.isEdit
                ? this.dataEdit.ngayTaoPhieu
                : this.form.value.ngayTaoPhieu,
        };

        this._commonService
            .callDataAPIShort(
                '/api/services/read/PhieuTongNhap/GetPhieuTong',
                bodySubmit,
            )
            .subscribe((response) => {
                this.dataDetail = response.result.chiTiet;
            });
    }

    convertIdToResult(id: number, type: string) {
        let result;
        if (this.listLTTP.length > 0) {
            const dataFound = this.listLTTP.find((item) => item.key === id);

            switch (type) {
                case 'dvt':
                    result = dataFound ? dataFound.donViTinh : '';
                    break;
                case 'name':
                    result = dataFound ? dataFound.name : '';
                    break;
            }
        }
        return result;
    }

    // Trình lên phía trên: trangThai = 3
    onSubmit() {
        let cong = this.dataDetail.reduce(
            (acc, item) => acc + item.thanhTien,
            0,
        );

        let soLuongMatHang = this.dataDetail.reduce((acc, item) => {
            acc++;
            return acc;
        }, 0);

        let bodySubmit = {
            ...this.dataEdit,
            ...this.form.value,
            chiTiet: this.dataDetail,
            trangThai: 3,
            cong: cong,
            soLuongMatHang: soLuongMatHang,
        };

        if (!this.isEdit) {
            this._commonService
                .callDataAPIShort(
                    '/api/services/write/PhieuTongNhap/Create',
                    bodySubmit,
                )
                .subscribe((response) => {
                    if (response.success) {
                        this._router.navigateByUrl(
                            'manage/app/category/tong-nhap-kho',
                        );
                        this._toastrService.success(
                            '',
                            'Trình phiếu tổng nhập thành công',
                        );
                    }
                });
        } else {
            this._commonService
                .callDataAPIShort(
                    '/api/services/write/PhieuTongNhap/Update',
                    bodySubmit,
                )
                .subscribe((response) => {
                    if (response.success) {
                        this._router.navigateByUrl(
                            'manage/app/category/tong-nhap-kho',
                        );
                        this._toastrService.success(
                            '',
                            'Cập nhật phiếu tổng nhập thành công',
                        );
                    }
                });
        }
    }

    // Lưu nháp trước khi trình lên: trangThai = 0
    saveDraft() {
        let cong = this.dataDetail.reduce(
            (acc, item) => acc + item.thanhTien,
            0,
        );

        let soLuongMatHang = this.dataDetail.reduce((acc, item) => {
            acc++;
            return acc;
        }, 0);

        let bodySubmit = {
            ...this.dataEdit,
            ...this.form.value,
            chiTiet: this.dataDetail,
            trangThai: 0,
            cong: cong,
            soLuongMatHang: soLuongMatHang,
        };

        if (!this.isEdit) {
            this._commonService
                .callDataAPIShort(
                    '/api/services/write/PhieuTongNhap/Create',
                    bodySubmit,
                )
                .subscribe((response) => {
                    if (response.success) {
                        if (!this.isExportExcel) {
                            this._router.navigateByUrl(
                                'manage/app/category/tong-nhap-kho',
                            );
                        }
                        this._toastrService.success(
                            '',
                            'Lưu nháp phiếu tổng nhập thành công',
                        );
                    }
                });
        } else {
            this._commonService
                .callDataAPIShort(
                    '/api/services/write/PhieuTongNhap/Update',
                    bodySubmit,
                )
                .subscribe((response) => {
                    if (response.success) {
                        if (!this.isExportExcel) {
                            this._router.navigateByUrl(
                                'manage/app/category/tong-nhap-kho',
                            );
                        }
                        this._toastrService.success(
                            '',
                            'Cập nhật phiếu tổng nhập thành công',
                        );
                    }
                });
        }
    }

    onBack() {
        this._router.navigateByUrl('manage/app/category/tong-nhap-kho');
    }

    exportExcel() {
        if (this.typeAction !== 3) {
            this.isExportExcel = true;
            this.saveDraft();
        }
        let maPhieuTongNhapKho = this.isEdit
            ? this.dataEdit.maPhieu
            : this.form.value.maPhieu;
        let bodyDataExcel = this.isEdit
            ? {
                ...this.dataEdit,
                ...this.form.value,
                chiTiet: this.dataDetail.map((item) => ({
                    ...item,
                    tenLttpChatDot: this.convertIdToResult(
                        item.lttpChatDotId,
                        'name',
                    ),
                })),
                khoNhanName: this.warehouseList.find(
                    (item) => item.key === parseInt(this.dataEdit.khoNhanId),
                ),
            }
            : {
                ...this.form.value,
                chiTiet: this.dataDetail.map((item) => ({
                    ...item,
                    tenLttpChatDot: this.convertIdToResult(
                        item.lttpChatDotId,
                        'name',
                    ),
                })),
                khoNhanName: this.warehouseList.find(
                    (item) => item.key === this.form.get('khoNhanId').value,
                ),
            };

        this._exportService.generateImportTotalReportExcel(
            bodyDataExcel,
            `TongNhapKho ${maPhieuTongNhapKho}.xlsx`,
        );
    }
}
