import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagementQuantityComponent } from './management-quantity.component';

describe('ManagementQuantityComponent', () => {
  let component: ManagementQuantityComponent;
  let fixture: ComponentFixture<ManagementQuantityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManagementQuantityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagementQuantityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
