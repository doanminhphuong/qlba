import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddPureStoreComponent } from './add-pure-store.component';

describe('AddPureStoreComponent', () => {
  let component: AddPureStoreComponent;
  let fixture: ComponentFixture<AddPureStoreComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddPureStoreComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddPureStoreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
