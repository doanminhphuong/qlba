import { Component, OnInit } from '@angular/core';
import { version } from '../../../../../package.json';

@Component({
    selector: 'app-document-page',
    templateUrl: './document-page.component.html',
    styleUrls: ['./document-page.component.scss'],
})
export class DocumentPageComponent implements OnInit {
    constructor() {}

    public version: string = version;
    markdown = `## Markdown __rulez__!
    ---

    ### Syntax highlight
    \`\`\`typescript
    const language = 'typescript';
    \`\`\`

    ### Lists
    1. Ordered list
    2. Another bullet point
       - Unordered list
       - Another unordered bullet

    ### Blockquote
    > Blockquote to the max`;
    ngOnInit() {}
}
