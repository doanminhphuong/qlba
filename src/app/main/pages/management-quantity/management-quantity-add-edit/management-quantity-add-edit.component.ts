import { SelectionModel } from '@angular/cdk/collections';
import {
    AfterViewInit,
    ChangeDetectorRef,
    Component,
    OnInit,
    ViewChild,
    ViewChildren,
    QueryList,
    OnChanges,
    SimpleChanges,
    ElementRef,
} from '@angular/core';
import {
    FormArray,
    FormBuilder,
    FormControl,
    FormGroup,
    Validators,
} from '@angular/forms';
import {
    MatDialog,
    MatPaginator,
    MatSort,
    MatTableDataSource,
} from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { DeleteConfirmDialogComponent } from '@app/_shared/dialogs/delete-confirm-dialog/delete-confirm-dialog.component';
import { MaterialDialogComponent } from '@app/_shared/dialogs/material-dialog/material-dialog.component';
import { CategoryService } from '@core/services/category.service';
import { PriceService } from '@core/services/price.service';
import { ToastrService } from '@core/services/toastr.service';
import { Subject, Subscription } from 'rxjs';
import { debounceTime, distinctUntilChanged, map, tap } from 'rxjs/operators';
import { forkJoin, Observable } from 'rxjs';
import { UserService } from '@core/services/user.service';
import { FoodFuelService } from '@core/services/food-fuel.service';
import { InventoryService } from '@core/services/inventory.service';
import { CommonService } from '@core/services/common.service';
import {
    animate,
    state,
    style,
    transition,
    trigger,
} from '@angular/animations';
import { DatePipe, formatDate } from '@angular/common';
import { CircularService } from '@core/services/circular-service';
import { DeleteConfirmAllDialogComponent } from '@app/_shared/dialogs/delete-confirm-all-dialog/delete-confirm-all-dialog.component';
import { QuantityService } from '@core/services/quantity.service';

export interface ElementSelect {
    key: string;
    value: string;
}

export interface quantityList {
    dinhLuongAnId: string;
    nhomLttpChatDotId: number;
    donViTinhId: string;
    loaiDinhLuong?: string;
    dinhLuong: number;
    id?: string | number;
}

export interface mealList {
    mucTienAn: string | number;
    mucTienAnLe: string | number;
    mucTienAnOm: string | number;
    mucTienAnThemNv: string | number;
}

@Component({
    selector: 'app-management-quantity-add-edit',
    templateUrl: './management-quantity-add-edit.component.html',
    styleUrls: ['./management-quantity-add-edit.component.scss'],
    animations: [
        trigger('detailExpand', [
            state('collapsed', style({ height: '0px', minHeight: '0' })),
            state('expanded', style({ height: '*' })),
            transition(
                'expanded <=> collapsed',
                animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)'),
            ),
        ]),
    ],
    providers: [DatePipe],
})
export class ManagementQuantityAddEditComponent
    implements OnInit, OnChanges, AfterViewInit
{
    public applyFilter = new Subject<KeyboardEvent>();
    private subscription: Subscription;
    @ViewChildren('paginatorList') paginator = new QueryList<MatPaginator>();
    @ViewChild('paginatorLTTP') paginatorLTTP!: MatPaginator;
    // @ViewChild(MatPaginator) paginator!: MatPaginator;

    titleOnEdit: string;

    form: FormGroup;
    formQuantity: FormGroup;
    fields: any[] = [];
    fieldsQuantity: any[] = [];
    displayedColumnsOfField: string[] = [
        'lttpcd',
        'dvTinh',
        'dinhLuong',
        'actions',
    ];
    dataSource = new MatTableDataSource<any>([]);
    selection = new SelectionModel<any>(true, []);
    currentTable: any[] = [];
    quantiveId: any;
    typeAction: number | string;
    dataEdit: any;
    nameEatObject: string;
    nameCirculars: string | number;
    nameEatObjectEdit: string;
    mucTienAn: string | number;
    mucTienAnLe: string | number;
    mucTienAnOm: string | number;
    mucTienAnThemNv: string | number;
    circular: Object;
    listEatObject: ElementSelect[] = [];
    listCirculars: ElementSelect[] = [];
    quantityCreateList: quantityList[] = [];
    mealRateCreateList: mealList[] = [];
    quantiveItemCurrent;
    listQuantityItem = [];
    listEatObjectDisable = [];

    listObjectEat: any[] = [];

    // Boolean
    isEdit: boolean = false;
    isEditDisabled: boolean = false;
    isEditReactiveForm: boolean = false;
    isSpinnerAddQuantive: boolean = false;
    isConfirmDeleteGroup: boolean[] = [];

    totalPrice: number = 0;
    cont: number = 0;
    typeOfPrice: string;
    adjustmentInfo: any[] = [];
    priceList: any[] = [];
    foodFuelList: any[] = [];

    listPrice: any[] = [];
    eatObject: any;
    listMealRate: any[] = [];

    thongTuId: number = 0;

    // Categories
    userList: any[] = [];
    organizationOptions: any[] = [];
    userOptions: any[] = [];
    supplierOptions: any[] = [];
    chiTietList: any[] = [];

    isActive: boolean = false;
    warehouseExportId: number;
    url_foodQuantification = '/api/services/write/DinhLuongAn/Create';
    formQuantive = this._fb.group({
        quantity: this._fb.array([]),
    });
    valueSelected: any;

    // định lượng
    nameLTTP: string;
    donViTinhId: string;
    dinhLuong: string;
    listFoodFuel: any[] = [];
    listUnitCtegory: ElementSelect[] = [];

    constructor(
        private readonly changeDetectorRef: ChangeDetectorRef,
        private _activatedRoute: ActivatedRoute,
        private _toastrService: ToastrService,
        private _router: Router,
        public dialog: MatDialog,
        private _priceService: PriceService,
        private _categoryService: CategoryService,
        private _userService: UserService,
        private _foodFuelService: FoodFuelService,
        private _dinhLuongAnService: QuantityService,
        private _inventoryService: InventoryService,
        private _commonService: CommonService,
        private _circularService: CircularService,
        private _fb: FormBuilder,
        private fb: FormBuilder,
    ) {
        this.subscription = this.applyFilter
            .pipe(
                map((event) => (event.target as HTMLInputElement).value),
                debounceTime(500),
                distinctUntilChanged(),
            )
            .subscribe((filterValue) =>
                this.handleInputFilter(
                    filterValue,
                    // this.dataSourceGroupFoodUpgrade,
                ),
            );
    }
    ngOnChanges(changes: SimpleChanges): void {
        console.log('changes', changes);
    }

    get quantity() {
        return this.formQuantive.get('quantity') as FormArray;
    }

    ngOnInit() {
        // Check if this is Create or Edit page
        this._activatedRoute.paramMap.subscribe((param) => {
            this.thongTuId = +param.get('thongTuId');
            this.quantiveId = +param.get('id');
            this.typeAction = +param.get('typeAction');
            if (this.quantiveId) {
                this.isEdit = true;
            }
        });
        // console.log('getEnvironment::', this._commonService.getEnvironment());
        // this.getAllFoodFuel();
        this.getInitData();
        // this.getInitForm();
        // this.resetData();
        // this.isConfirmDeleteGroup.fill(false);
    }

    createQuantitive(quantitive): FormGroup {
        return this.fb.group({
            ...quantitive,
            nhomLttpChatDotId: [quantitive.nhomLttpChatDotId],
            donViTinhId: [quantitive.donViTinhId],
            dinhLuong: [quantitive.dinhLuong],
            id: [quantitive.id] || 0,
            tenantId: 1,
        });
    }

    //

    ngAfterViewChecked(): void {
        this.changeDetectorRef.detectChanges();
    }

    ngAfterViewInit(): void {}

    getAllFoodFuel(): void {
        this._foodFuelService
            .getAllFoodFuel({})
            .subscribe((res) => (this.foodFuelList = res.result.items));
    }

    // getBodyCategory(groupCode: string) {
    //     return {
    //         maxResultCount: 10,
    //         skipCount: 0,
    //         sorting: 'Code',
    //         criterias: [
    //             {
    //                 propertyName: 'GroupCode',
    //                 operation: 6,
    //                 value: groupCode,
    //             },
    //         ],
    //     };
    // }

    getInitData(): void {
        const bodyOrganization = {
            criterias: [
                {
                    propertyName: 'Code',
                    operation: 6,
                    value: '',
                },
            ],
        };

        // let bodyEatObject = {
        //     maxResultCount: 10,
        //     skipCount: 0,
        //     sorting: 'Code',
        //     criterias: [
        //         {
        //             propertyName: 'GroupCode',
        //             operation: 6,
        //             value: 'doi-tuong-an',
        //         },
        //     ],
        // };

        let bodydonViTinhId = {
            maxResultCount: 10,
            skipCount: 0,
            sorting: 'Code',
            criterias: [
                {
                    propertyName: 'GroupCode',
                    operation: 6,
                    value: 'don-vi-tinh',
                },
            ],
        };

        let bodyMealRateId = {
            maxResultCount: 10,
            skipCount: 0,
            sorting: 'Code',
            criterias: [
                {
                    propertyName: 'GroupCode',
                    operation: 6,
                    value: 'muc-tien-an',
                },
            ],
        };
        const dataAllFoodFuel = this._foodFuelService.getAllGroupOfFoodFuel({
            maxResultCount: 2147483647,
        });
        const dataUnitCtegory =
            this._categoryService.getAllCategory(bodydonViTinhId);

        const dataMealRate =
            this._categoryService.getAllCategory(bodyMealRateId);
        const currentMealRate = this._commonService.callDataAPIShort(
            '/api/services/read/DinhLuongAn/GetCurrent',
            {},
        );

        forkJoin([
            dataAllFoodFuel,
            dataUnitCtegory,
            dataMealRate,
            currentMealRate,
        ]).subscribe((results) => {
            // console.log('results::', results[0].result.items);
            let rawListFoodFuel = results[0].result.items;
            let rawListUnitCtegory = results[1].result;
            // let rawListMealRate = results[2].result;
            let rawDinhLuong = [];
            let rawMealRate = results[2].result.map((item) => ({
                key: item.number1,
                name: item.name,
            }));

            rawDinhLuong = results[3].result.map((data) => ({
                key: data.mucTienAn,
                name: 'Mức tiền ăn' + data.mucTienAn,
            }));

            this._commonService.covertDataToKeyValueInSelect(
                rawListFoodFuel,
                this.listFoodFuel,
                'id',
                'tenNhomLttpChatDot',
            );
            this._commonService.covertDataToKeyValueInSelect(
                rawListUnitCtegory,
                this.listUnitCtegory,
                'id',
                'name',
            );
            this.listMealRate = rawMealRate.filter(
                ({ key: id1 }) =>
                    !rawDinhLuong.some(({ key: id2 }) => id2 === id1),
            );
            if (this.listMealRate.length > 0) {
                this.getInitForm();
            }
        });

        // const dataEatObject =
        //     this._categoryService.getAllCategory(bodyEatObject);
        const dataCirculars = this._circularService.getAllCirculars({});

        const dataOrganization =
            this._userService.getOrganization(bodyOrganization);
        const dataPriceAdjust = this._priceService.getAllPriceAdjustment({});
        const getAllWarehouse = this._inventoryService.getAllWarehouse({});
        // const getUserService = this._userService.getUsers({});
        const getDataRaw = this._commonService.callDataAPIShort(
            '/api/services/read/ChiTietDinhLuongAn/GetAll',
            {},
        );
        const getDataQuantity = this._commonService.callDataAPIShort(
            '/api/services/read/DinhLuongAn/GetAll',
            {},
        );

        forkJoin([
            dataOrganization,
            dataPriceAdjust,
            getAllWarehouse,
            // getUserService,
            // dataEatObject, //listEatObject
            dataCirculars,
            // getDataRaw,
            getDataQuantity,
        ]).subscribe((results) => {
            // console.log('results::', results);
            // console.log('results[4].result::', results[4].result);
            // let rawlistEatObject = results[4].result;
            // let rawListCirculars = results[5].result.items;
            this.circular = results[3].result;
            // this.listQuantityItem = results[7].result.items;
            // console.log('circular::', this.circular);
            // this._commonService.covertDataToKeyValueInSelect(
            //     rawlistEatObject,
            //     this.listEatObject,
            //     'id',
            //     'name',
            // );

            //Default Circulars
            if (this.listCirculars.length > 0) {
                this.nameCirculars = this.listCirculars[0].key;
            }

            results[4].result.items.forEach((quantive) => {
                if (quantive.thongTuId === this.nameCirculars) {
                    this.listQuantityItem.push(quantive);
                }
            });
        });
    }

    // getDataQuantity(idQuantity?: any) {
    //     let body = {
    //         criterias: [
    //             {
    //                 propertyName: 'thongTuId',
    //                 operation: 0,
    //                 value: idQuantity,
    //             },
    //         ],
    //     };
    //     this._commonService
    //         .callDataAPIShort('/api/services/read/DinhLuongAn/GetAll', body)
    //         .subscribe((res) => {
    //             this.listQuantityItem = res.result.items;

    //             this.formQuantive = this._fb.group({
    //                 quantity: this._fb.array(
    //                     this.listQuantityItem.map((quantive) =>
    //                         this.createQuantitive(quantive),
    //                     ),
    //                 ),
    //             });
    //         });
    // }

    getInitForm(): void {
        this.fields = [
            {
                type: 'SELECT',
                referenceValue: 'mucTienAn',
                name: 'Chọn mức tiền ăn',
                defaultValue: '',
                required: '1',
                css: 'col-12 col-lg-6',
                appearance: 'legacy',
                options: [...this.listMealRate],
            },
            {
                type: 'NUMBER',
                referenceValue: 'mucTienAnLe',
                name: 'Nhập mức tiền ăn lễ',
                defaultValue: '',
                required: '1',
                css: 'col-12 col-lg-6',
                appearance: 'legacy',
            },
            {
                type: 'NUMBER',
                referenceValue: 'mucTienAnOm',
                name: 'Nhập mức tiền ăn ốm',
                defaultValue: '',
                required: '1',
                css: 'col-12 col-lg-6',
                appearance: 'legacy',
            },
            {
                type: 'NUMBER',
                referenceValue: 'mucTienAnThemNv',
                name: 'Nhập mức tiền ăn thêm nhiệm vụ',
                defaultValue: '',
                required: '1',
                css: 'col-12 col-lg-6',
                appearance: 'legacy',
            },
        ];
        let fieldsCtrls = {};
        let fieldList = [];

        for (let f of this.fields) {
            fieldList = [...fieldList, f];

            if (f.type === 'NUMBER') {
                fieldsCtrls[f.referenceValue] = new FormControl(
                    { value: f.defaultValue, disabled: f.disabled },
                    f.required === '1' ? [Validators.required] : [],
                );
            } else if (f.type !== 'CHECKBOX') {
                let validators = [
                    Validators.minLength(f.minLength),
                    Validators.maxLength(f.maxLength),
                    Validators.pattern(f.pattern),
                ];

                if (f.type === 'EMAIL') {
                    validators = [...validators, Validators.email];
                }

                fieldsCtrls[f.referenceValue] = new FormControl(
                    {
                        value: f.defaultValue || '',
                        disabled: f.disabled,
                    },
                    f.required === '1'
                        ? [...validators, Validators.required]
                        : [...validators],
                );
                if (f.type === 'TEXT') {
                    fieldsCtrls[f.referenceValue].valueChanges.subscribe(
                        (val) => {
                            if (val !== '') {
                                const dependedField = fieldList.find(
                                    (x) => x.dependenField === f.referenceValue,
                                );
                                if (!dependedField) return;

                                if (val.length > 10) {
                                    fieldsCtrls[
                                        dependedField.referenceValue
                                    ].patchValue(
                                        val
                                            .normalize('NFD')
                                            .replace(/[\u0300-\u036f]/g, '')
                                            .match(/(^|\s)\w[0-9]{0,3}/g)
                                            .join('')
                                            .replace(/\s/g, '')
                                            .toUpperCase(),
                                    );
                                } else {
                                    fieldsCtrls[
                                        dependedField.referenceValue
                                    ].patchValue(
                                        val
                                            .normalize('NFD')
                                            .replace(/[\u0300-\u036f]/g, '')
                                            .replace(/\s/g, '')
                                            .toUpperCase(),
                                    );
                                }
                            }
                        },
                    );
                }
            } else {
                //if checkbox, it need multiple
                let opts = {};
                for (let opt of f.options) {
                    opts[opt.key] = new FormControl({
                        value: opt.value,
                        disabled: f.disabled,
                    });
                }
            }
        }
        this.form = new FormGroup(fieldsCtrls);
        if (this.isEdit) {
            this.getDataEdit();
        }
    }

    addQuantitative() {
        const quantitativeForm = this.fb.group({
            nhomLttpChatDotId: [this.nameLTTP, Validators.required],
            donViTinhId: [this.donViTinhId, Validators.required],
            dinhLuong: [
                this.dinhLuong,
                [
                    Validators.required,
                    Validators.pattern('^[0-9]+(.[0-9]{0,2})?$'),
                ],
            ],
            tenantId: 1,
            id: 0,
            // dinhLuongAnId: this.data, //
        });
        // this.quantity.push(quantitativeForm);
        this.quantity.insert(0, quantitativeForm);
        this.resetValueData();
        // if (this.isEdit) {
        //     this.getDataEdit();
        // }
        // console.log(this.quantity.value);
    }
    resetValueData() {
        this.nameLTTP = '';
        this.donViTinhId = '';
        this.dinhLuong = '';
    }
    deleteQuantitative(index: number) {
        this.quantity.removeAt(index);
    }
    clearAll() {
        let curentLength = this.quantity.value;
        for (let index = 0; index < curentLength.length; index++) {
            this.deleteQuantitative(0);
        }
    }
    updateQuantitative(lessonIndex: number) {
        // console.log('data', this.quantity.get(lessonIndex.toString()).value);
        let value = this.quantity.get(lessonIndex.toString()).value;
        this.nameLTTP = value.nhomLttpChatDotId;
        this.donViTinhId = value.donViTinhId;
        this.dinhLuong = value.dinhLuong;
    }
    getDataEdit(): void {
        const dataEditId = {
            id: this.quantiveId,
        };
        this._dinhLuongAnService
            .getDinhLuongAnById(dataEditId)
            .subscribe((res) => {
                this.dataEdit = res.result;
                this.listMealRate.push({
                    key: this.dataEdit.mucTienAn,
                    name: 'Mức tiền ăn ' + this.dataEdit.mucTienAn,
                });

                this.fields[0].options = [...this.listMealRate];
                // Get Title
                this.titleOnEdit = `Chỉnh sửa tiêu chuẩn, định lượng`;
                // Patch data to Form
                // this.formQuantive.patchValue({
                //     quantity: this.dataEdit.chiTiet,
                // });
                // this.nameEatObject = JSON.parse(this.dataEdit.nhomDoiTuongId);
                this.form.patchValue(this.dataEdit);
                this.formQuantive = this._fb.group({
                    quantity: this._fb.array(
                        this.dataEdit.chiTiet.map((quantive) =>
                            this.createQuantitive(quantive),
                        ),
                    ),
                });
                this.formQuantive.patchValue({
                    quantity: this.listQuantityItem,
                });
            });
    }

    // Handle Input Filter
    handleInputFilter(filterValue) {
        this.dataSource.data = this.currentTable.filter((item) => {
            const tenLttpChatDot = this.getDataFromId(
                item.lttpChatDotId,
                'tenLttpChatDot',
            );
            return tenLttpChatDot
                .toLowerCase()
                .includes(filterValue.trim().toLowerCase());
        });
    }

    // Get Info of Food Fuel
    getDataFromId(id, objKey) {
        let dataInit = '';

        if (this.foodFuelList.length > 0) {
            const dataFound = this.foodFuelList.find((item) => item.id === id);

            dataInit = dataFound[objKey];
        }

        return dataInit;
    }

    // Back to list
    onBack(): void {
        this._router.navigate([
            'manage/app/category/quan-ly-tieu-chuan-dinh-luong',
        ]);
    }
    checkMealNotInput(input: unknown[] | string) {
        const inputArray = Array.isArray(input) ? input : null;

        if (inputArray) {
            return inputArray.map((str) => ' ' + str);
        }

        return input;
    }
    // Submit data
    onSubmit(): void {
        const dataSubmit = {
            ...this.dataEdit,
            nhomDoiTuongId: '[]',
            ...this.form.value,
            thongTuId: this.thongTuId,
            chiTiet: this.quantity.value,
        };

        if (!this.isEdit) {
            this._dinhLuongAnService.createDinhLuongAn(dataSubmit).subscribe(
                (res) => {
                    this._router.navigate([
                        'manage/app/category/quan-ly-tieu-chuan-dinh-luong',
                    ]);
                    this._toastrService.success(
                        '',
                        'Tạo tiêu chuẩn định lượng thành công',
                    );
                },
                (err) => this._toastrService.errorServer(err),
            );
        } else {
            this._dinhLuongAnService.updateDinhLuongAn(dataSubmit).subscribe(
                (res) => {
                    this._router.navigate([
                        'manage/app/category/quan-ly-tieu-chuan-dinh-luong',
                    ]);
                    this._toastrService.success('', 'Cập nhật thành công');
                },
                (err) => this._toastrService.errorServer(err),
            );
        }
    }

    changeAction(event, index) {
        this.valueSelected = event.value;
    }

    // checkDisableMatOption(eatObject) {
    //     return this.listQuantityItem.some(
    //         (item) => item.nhomDoiTuongId === eatObject,
    //     );
    // }

    checkDisableMatOption1(unit) {
        return this.quantity.value.some(
            (item) => item.nhomLttpChatDotId === unit,
        );
    }

    reviewAction() {
        console.log('reviewAction');
    }

    formatCurrency_TaxableValue(event) {
        let uy = new Intl.NumberFormat('vi-VI', {
            style: 'currency',
            currency: 'VND',
        }).format(event.target.value);
        this.mucTienAn = uy;
    }

    formatCurrency_blur(event) {
        console.log('blur', event.value);
    }
}
