import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HoSoSucKhoeComponent } from './ho-so-suc-khoe.component';

describe('HoSoSucKhoeComponent', () => {
  let component: HoSoSucKhoeComponent;
  let fixture: ComponentFixture<HoSoSucKhoeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HoSoSucKhoeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HoSoSucKhoeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
