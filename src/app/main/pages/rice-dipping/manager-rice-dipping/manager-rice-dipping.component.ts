import {
    ChangeDetectionStrategy,
    Component,
    OnInit,
    ViewChild,
} from '@angular/core';
import { MatDialog, MatPaginator, MatTableDataSource } from '@angular/material';
import { Router } from '@angular/router';
import { DeleteConfirmDialogComponent } from '@app/_shared/dialogs/delete-confirm-dialog/delete-confirm-dialog.component';
import { CommonService } from '@core/services/common.service';
import { DataService } from '@core/services/data-service';
import { ToastrService } from '@core/services/toastr.service';
import { Subject, Subscription } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';

@Component({
    selector: 'app-manager-rice-dipping',
    templateUrl: './manager-rice-dipping.component.html',
    styleUrls: ['./manager-rice-dipping.component.scss'],
})
export class ManagerRiceDippingComponent implements OnInit {
    @ViewChild(MatPaginator) paginator!: MatPaginator;

    displayedColumnsOfField: string[] = [
        'ten',
        'quanSo',
        'tuNgay',
        'denNgay',
        'trangThai',
        'actions',
    ];

    dataSource = new MatTableDataSource<any>([]);
    applyFilter = new Subject<KeyboardEvent>();
    subscription: Subscription;
    isLoading: boolean = false;

    constructor(
        private router: Router,
        public dialog: MatDialog,
        private _toastrService: ToastrService,
        private commonService: CommonService,
        private _dataService: DataService,
    ) {
        this.subscription = this.applyFilter
            .pipe(
                map((event) => (event.target as HTMLInputElement).value),
                debounceTime(500),
                distinctUntilChanged(),
            )
            .subscribe((filterValue) => this.handleSearch(filterValue));

        // this.subscription = this._dataService.getData().subscribe((message) => {
        //     this.isLoading = true;
        //     this.getInitData(message.id);
        // });
    }

    ngOnInit() {
        this._dataService.behaviorSubjectBepID$.subscribe(
            (data: { id: number | string }) => {
                this.isLoading = true;
                this.getInitData(data.id);
            },
        );

        // this.subscription = this._dataService.getData().subscribe((message) => {
        //     console.log('message::', message);
        //     this.isLoading = true;
        //     this.getInitData(message.id);
        // });
    }

    getInitData(id: number | string) {
        this.commonService
            .callDataAPIShort('/api/services/read/DotQuanSoAn/GetAll', {
                maxResultCount: 999999999,
                criterias: [
                    {
                        propertyName: 'dauMoiBepId',
                        operation: 0,
                        value: id,
                    },
                ],
            })
            .subscribe((response) => {
                if (response.success) {
                    this.dataSource = new MatTableDataSource<any>(
                        response.result.items,
                    );
                    this.dataSource.paginator = this.paginator;
                    this.isLoading = false;
                    // setTimeout(() => {
                    //     this.isLoading = false;
                    // }, 200);
                }
            });
    }

    handleSearch(dataInput: any) {
        this.dataSource.filter = dataInput.toLowerCase();
    }
    action(element: any) {
        if (element) {
            this.router.navigate([
                'app/cham-com/quan-ly-quan-so-an/edit/' + element.id,
            ]);
        } else {
            this.router.navigate(['app/cham-com/quan-ly-quan-so-an/create']);
        }
    }

    deleteEaterList(dataDelete: any) {
        let dataValue = dataDelete;
        const dialogRef = this.dialog.open(DeleteConfirmDialogComponent, {
            width: '500px',
            data: dataDelete,
        });

        dialogRef.componentInstance.handleDelete.subscribe((dataDelete) => {
            const isSubmitted = !!dataDelete;
            if (dataValue.trangThai === true) {
                this._toastrService.error(
                    '',
                    'Quân số ăn này còn đang sử dụng',
                );
            } else {
                this.commonService
                    .callDataAPIShort(
                        '/api/services/write/DotQuanSoAn/Delete',
                        dataDelete,
                    )
                    .subscribe(
                        (response) => dialogRef.close(isSubmitted),
                        (err) => this._toastrService.errorServer(err),
                    );
            }
        });

        dialogRef.afterClosed().subscribe((result) => {
            if (!result) return;
            this.getInitData(this._dataService.getDauMoiBepId());
            this._toastrService.success('', 'Xóa thành công');
        });
    }
}
