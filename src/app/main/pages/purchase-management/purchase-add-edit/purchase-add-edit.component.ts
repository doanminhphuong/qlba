import { SelectionModel } from '@angular/cdk/collections';
import { isNgTemplate } from '@angular/compiler';
import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatPaginator, MatTableDataSource } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { DeleteConfirmDialogComponent } from '@app/_shared/dialogs/delete-confirm-dialog/delete-confirm-dialog.component';
import { DialogRejectStatusComponent } from '@app/_shared/dialogs/dialog-reject-status/dialog-reject-status.component';
import { MaterialDialogComponent } from '@app/_shared/dialogs/material-dialog/material-dialog.component';
import { CategoryService } from '@core/services/category.service';
import { CommonService } from '@core/services/common.service';
import { DataService } from '@core/services/data-service';
import { ExportExcelService } from '@core/services/export-excel.service';
import { PriceService } from '@core/services/price.service';
import { ToastrService } from '@core/services/toastr.service';
import { UserService } from '@core/services/user.service';
import { forkJoin, Subject, Subscription } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';

@Component({
    selector: 'app-purchase-add-edit',
    templateUrl: './purchase-add-edit.component.html',
    styleUrls: ['./purchase-add-edit.component.scss'],
})
export class PurchaseAddEditComponent implements OnInit {
    public applyFilter = new Subject<KeyboardEvent>();
    private subscription: Subscription;
    @ViewChild(MatPaginator) paginator!: MatPaginator;
    currentDate: Date = new Date();
    userOrganization;
    userOrganizationName: string;
    titleOnEdit: string;

    form: FormGroup;
    fields: any[] = [];

    displayedColumns: string[] = [
        'select',
        'maLttpChatDot',
        'tenLttpChatDot',
        'soLuong',
        'donViTinh',
        'diaChiMua',
        'donGia',
        'thanhTien',
        'actions',
    ];

    dataSource = new MatTableDataSource<any>([]);
    selection = new SelectionModel<any>(true, []);

    currentTable: any[] = [];
    purchaseId: any;
    dataEdit: any;
    priceAdjustmentList: any[] = [];
    priceAdjustmentId: number = 0;
    isActive: boolean = false;

    // Boolean
    isEdit: boolean = false;
    isPrinter: boolean = false;
    typeAction: number | string;

    totalPrice: number = 0;
    typeOfPrice: string;
    adjustmentInfo: any[] = [];
    priceList: any[] = [];
    foodFuelList: any[] = [];

    // Categories
    userList: any[] = [];
    organizationOptions: any[] = [];
    userOptions: any[] = [];
    supplierOptions: any[] = [];
    chiTietList: any[] = [];
    listAddress: any[] = [];

    currentNhaCungCapId: string;

    dateBuy: any;
    userBuy: any;
    organizationName: any;

    constructor(
        private readonly changeDetectorRef: ChangeDetectorRef,
        private _activatedRoute: ActivatedRoute,
        private _toastrService: ToastrService,
        private _router: Router,
        public dialog: MatDialog,
        private _priceService: PriceService,
        private _categoryService: CategoryService,
        private _userService: UserService,
        private _exportService: ExportExcelService,
        private _commonService: CommonService,
        private _dataService: DataService,
    ) {
        this.subscription = this.applyFilter
            .pipe(
                map((event) => (event.target as HTMLInputElement).value),
                debounceTime(500),
                distinctUntilChanged(),
            )
            .subscribe((filterValue) => this.handleInputFilter(filterValue));
    }

    ngOnInit() {
        // Check if this is Create or Edit page
        this._activatedRoute.paramMap.subscribe((param) => {
            this.purchaseId = +param.get('id');
            this.typeAction = +param.get('typeAction');

            if (this.typeAction === 3 || this.typeAction === 1) {
                this.displayedColumns.splice(0, 1);
                this.displayedColumns.splice(7, 1);
            }
            if (!this.purchaseId) return;
            this.isEdit = true;
        });
        // this.getListNameOfBuyer('');
        this.getOrganizationUserLogin();
        this.getMultiCategories();
        this.getInitData();
    }

    ngAfterViewChecked(): void {
        this.changeDetectorRef.detectChanges();
    }

    ngAfterViewInit(): void {}

    getMultiCategories() {
        this.getSupplierOptions();
        this.getAddressOptions();
    }

    getBodyCategory(groupCode: string) {
        return {
            maxResultCount: 10,
            skipCount: 0,
            sorting: 'Code',
            criterias: [
                {
                    propertyName: 'GroupCode',
                    operation: 6,
                    value: groupCode,
                },
            ],
        };
    }

    getSupplierOptions() {
        this._categoryService
            .getAllCategory(this.getBodyCategory('nha-cung-cap'))
            .subscribe((res) => {
                this.supplierOptions = res.result.map((item) => ({
                    key: item.id,
                    name: item.name,
                    number1: item.number1,
                }));
            });
    }

    getOrganizationUserLogin() {
        this._userService.getUserOrganization().subscribe((response) => {
            let organization = response.result.filter(
                (item) =>
                    item.groupCode === 'DonVi' || item.groupCode === '1-donvi',
            );

            if (organization.length > 0) {
                this.userOrganization = organization[0].id;
                this.userOrganizationName = organization[0].name;
            }
        });
    }

    getInitData(): void {
        const bodyOrganization = {
            criterias: [
                {
                    propertyName: 'Code',
                    operation: 6,
                    value: '',
                },
            ],
        };

        const dataOrganization =
            this._userService.getOrganization(bodyOrganization);
        const dataPriceAdjust = this._priceService.getAllPriceAdjustment({});

        const dataPriceAdjustCurrent = this._commonService.callDataAPIShort(
            '/api/services/read/DotDieuChinhGia/GetCurrent',
            {},
        );

        forkJoin([
            dataOrganization,
            dataPriceAdjust,
            dataPriceAdjustCurrent,
        ]).subscribe((results) => {
            this.organizationOptions = results[0].result.map((item) => ({
                key: item.id,
                name: item.name,
            }));

            this.priceAdjustmentList = results[1].result.items
                .filter((item) => item.status === 'ENABLE')
                .map((item) => ({
                    key: item.id,
                    name: item.tenDotDieuChinhGia,
                }));

            this.priceAdjustmentId = results[2].result.id;

            if (this.organizationOptions.length > 0) {
                this.getInitForm();
            }
        });

        this.dataSource.paginator = this.paginator;
    }

    getInitForm(): void {
        this.fields = [
            {
                type: 'TEXT',
                referenceValue: 'boPhan',
                name: 'Bộ phận',
                defaultValue: `${
                    this._dataService.getDonViThuocQuanLy().name
                }, ${this._dataService.getTenDonViCapTrenTrucTiep().name}`,
                required: '1',
                disabled: this.typeAction === 1 || this.typeAction === 3,
                css: 'col-12 col-lg-4',
                appearance: 'legacy',
            },
            {
                type: 'TEXT',
                referenceValue: 'nguoiMua',
                name: 'Họ và tên người mua',
                defaultValue: this._dataService.getThuKho(),
                required: '1',
                css: 'col-12 col-lg-4',
                disabled: this.typeAction === 1 || this.typeAction === 3,
                appearance: 'legacy',
            },
            {
                type: 'DATETIME',
                referenceValue: 'ngayMua',
                name: 'Ngày mua hàng',
                defaultValue: '',
                required: '1',
                disabled: this.typeAction === 1 || this.typeAction === 3,
                css: 'col-12 col-lg-4',
                appearance: 'legacy',
            },
            {
                type: 'TEXT',
                referenceValue: 'maPhieuMuaHang',
                name: 'Mã phiếu mua',
                defaultValue: '',
                required: '1',
                dependenField: 'ngayMua',
                disabled: this.typeAction === 1 || this.typeAction === 3,
                css: 'col-12 col-lg-4',
                appearance: 'legacy',
            },
            {
                type: 'SELECT',
                referenceValue: 'dotDieuChinhGiaId',
                name: 'Đợt điều chỉnh giá',
                defaultValue: this.priceAdjustmentId,
                options: [...this.priceAdjustmentList],
                required: '1',
                disabled: this.typeAction === 1 || this.typeAction === 3,
                css: 'col-12 col-lg-4',
                appearance: 'legacy',
            },
        ];

        let fieldsCtrls = {};
        let fieldList = [];

        for (let f of this.fields) {
            fieldList = [...fieldList, f];

            if (f.type === 'NUMBER') {
                fieldsCtrls[f.referenceValue] = new FormControl(
                    { value: f.defaultValue, disabled: f.disabled },
                    f.required === '1' ? [Validators.required] : [],
                );
            } else if (f.type !== 'CHECKBOX') {
                let validators = [
                    Validators.minLength(f.minLength),
                    Validators.maxLength(f.maxLength),
                    Validators.pattern(f.pattern),
                ];

                if (f.type === 'EMAIL') {
                    validators = [...validators, Validators.email];
                }

                fieldsCtrls[f.referenceValue] = new FormControl(
                    {
                        value: f.defaultValue || '',
                        disabled: f.disabled,
                    },
                    f.required === '1'
                        ? [...validators, Validators.required]
                        : [...validators],
                );

                if (f.type === 'SELECT') {
                    fieldsCtrls[f.referenceValue].valueChanges.subscribe(
                        (value) => {
                            if (f.referenceValue === 'dotDieuChinhGiaId') {
                                this.priceAdjustmentId = value;
                                this._priceService
                                    .getPriceAdjustmentById({
                                        id: this.priceAdjustmentId,
                                    })
                                    .subscribe((res) => {
                                        this.chiTietList = res.result.chiTiet;

                                        const dependField = fieldList.find(
                                            (field) =>
                                                field.dependenField ===
                                                f.referenceValue,
                                        );

                                        if (!dependField) return;

                                        if (
                                            this.typeAction !== 1 &&
                                            this.typeAction !== 3
                                        ) {
                                            fieldsCtrls[
                                                dependField.referenceValue
                                            ].enable();
                                        }
                                    });
                            }

                            if (f.referenceValue === 'nhaCungCapId') {
                                if (value) {
                                    const dataFound = f.options.find(
                                        (item) => item.key === value,
                                    );

                                    this.priceList = this.chiTietList.map(
                                        (item) => ({
                                            id: item.lttpChatDotId,
                                            price:
                                                dataFound.value === 0
                                                    ? item['giaTangGiaSanXuat']
                                                    : item['giaThiTruong'],
                                        }),
                                    );

                                    // this.getTotalPrice();
                                    this.isActive = true;
                                }
                            }
                        },
                    );
                }

                if (f.type === 'DATETIME') {
                    if (f.referenceValue === 'ngayMua') {
                        fieldsCtrls[f.referenceValue].valueChanges.subscribe(
                            (value) => {
                                if (this.isEdit === false) {
                                    const KEY = 'MH';

                                    let date = new Date(value);
                                    let part3 = date
                                        .toLocaleDateString('en-GB')
                                        .replace(/[^0-9]/g, '');
                                    let part4;

                                    if (localStorage.getItem('maMH') === null) {
                                        localStorage.setItem(
                                            'maMH',
                                            JSON.stringify({
                                                currentDate: new Date()
                                                    .toISOString()
                                                    .slice(0, 10),
                                                index: 1,
                                            }),
                                        );

                                        let dataLocal = JSON.parse(
                                            localStorage.getItem('maMH'),
                                        );

                                        part4 = this.pad(dataLocal.index);
                                    } else {
                                        let currentDate = new Date()
                                            .toISOString()
                                            .slice(0, 10);

                                        let dataLocal = JSON.parse(
                                            localStorage.getItem('maMH'),
                                        );

                                        if (
                                            currentDate ===
                                            dataLocal.currentDate
                                        ) {
                                            part4 = this.pad(dataLocal.index);
                                        } else {
                                            localStorage.setItem(
                                                'maMH',
                                                JSON.stringify({
                                                    currentDate: new Date()
                                                        .toISOString()
                                                        .slice(0, 10),
                                                    index: 1,
                                                }),
                                            );

                                            let dataLocal = JSON.parse(
                                                localStorage.getItem('maMH'),
                                            );

                                            part4 = this.pad(dataLocal.index);
                                        }
                                    }

                                    let fullPath =
                                        KEY + '.' + part3 + '.' + part4;

                                    this.form.patchValue({
                                        maPhieuMuaHang: fullPath,
                                    });
                                }
                            },
                        );
                    }
                }
            } else {
                //if checkbox, it need multiple
                let opts = {};
                for (let opt of f.options) {
                    opts[opt.key] = new FormControl({
                        value: opt.value,
                        disabled: f.disabled,
                    });
                }
            }
        }

        this.form = new FormGroup(fieldsCtrls);

        if (this.isEdit) {
            this.getDataEdit();
        }
    }

    pad(d) {
        return d < 10 ? '0' + d.toString() : d.toString();
    }

    getAddressOptions() {
        this._categoryService
            .getAllCategory(this.getBodyCategory('dia-chi-mua-hang'))
            .subscribe((res) => {
                this.listAddress = res.result.map((item) => ({
                    key: item.id,
                    name: item.name,
                }));
            });
    }

    get nhaCungCapId() {
        return this.form.get('nhaCungCapId');
    }

    handlenhaCungCapId(e) {
        this.currentNhaCungCapId = e.value;
    }

    getDataEdit(): void {
        const dataEditId = {
            id: this.purchaseId,
        };

        this._priceService.getPurchaseById(dataEditId).subscribe((res) => {
            this.dataEdit = res.result;

            console.log('this.dataEdit', this.dataEdit);
            // Get Title
            switch (this.dataEdit.trangThai) {
                case 1:
                case 3:
                case 4:
                    this.titleOnEdit = `Chi tiết phiếu mua hàng '${this.dataEdit.maPhieuMuaHang}'`;
                    break;
                default:
                    this.titleOnEdit = `Chỉnh sửa phiếu mua hàng '${this.dataEdit.maPhieuMuaHang}'`;
            }

            // Patch data to Table (Details)
            this.currentTable = this.dataEdit.chiTiet.map((item) => ({
                ...item,
                ...item.lttpChatDot,
            }));

            // Remove id on each item
            this.currentTable.forEach((item) => {
                delete item.id;
            });

            this.dataSource.data = this.currentTable;

            this.totalPrice = this.dataEdit.tongTien;

            this.currentNhaCungCapId = this.dataEdit.nhaCungCapId;

            this.dateBuy =
                'Ngày ' +
                new Date(this.dataEdit.ngayMua).getDate() +
                ' tháng ' +
                (new Date(this.dataEdit.ngayMua).getMonth() + 1) +
                ' năm ' +
                new Date(this.dataEdit.ngayMua).getFullYear();

            // Patch data to Form
            this.form.patchValue({
                ...this.dataEdit,
                boPhan: this.dataEdit.boPhan,
                nguoiMua: this.dataEdit.nguoiMua,
            });
        });
    }

    convertNameById(id: any, type: any) {
        let data;
        let dataList = [];

        if (this.listAddress.length > 0) {
            switch (type) {
                case 'Address':
                    dataList = this.listAddress;
                    break;
            }
            let item = dataList.find((item) => item.key === id);
            data = item && item.name ? item.name : '';
        }

        return data;
    }

    // Handle Input Filter
    handleInputFilter(filterValue) {
        this.dataSource.data = this.currentTable.filter((item) =>
            item.tenLttpChatDot
                .toLowerCase()
                .includes(filterValue.trim().toLowerCase()),
        );
    }

    getDataNameById(id: number | string, option: string) {
        let result = '';
        let dataOptions = [];

        if (
            this.supplierOptions.length > 0 &&
            this.organizationOptions.length > 0 &&
            this.userList.length > 0
        ) {
            switch (option) {
                case 'SUPPLIER':
                    dataOptions = this.supplierOptions;
                    break;
                case 'ORGANIZATION':
                    dataOptions = this.organizationOptions;
                    break;
                case 'USER':
                    dataOptions = this.userList;
                    break;
            }

            const dataFound = dataOptions.find((item) => item.key === id);
            result = dataFound ? dataFound.name : '';
        }

        return result;
    }

    // Get Price of Item (GIÁ MUA NGOÀI or GIÁ TGSX)
    getPriceOfItem(id) {
        let price = 0;
        const item = this.priceList.find((item) => item.id === id);
        price = item ? item.price : 0;

        return price;
    }

    getPrice(item) {
        let currentTypePrice = this.supplierOptions.find(
            (itemE) => itemE.key === this.currentNhaCungCapId,
        );

        let { giaThiTruong, giaTangGiaSanXuat } = item;
        let isNumber1 = currentTypePrice && currentTypePrice.number1;
        let typeOfPrice = isNumber1
            ? currentTypePrice.number1 === 1
                ? giaThiTruong
                : giaTangGiaSanXuat
            : 0;

        return typeOfPrice;
    }

    getTotalPrice() {
        this.dataSource.data.forEach((item) => {
            if (item.handleType === 'Khac') {
                item.thanhTien = item.soLuong * item.donGia;
            } else {
                // item.donGia = this.getPrice(item);
                item.thanhTien = item.soLuong * item.donGia;
            }
        });

        this.totalPrice = this.dataSource.data.reduce(
            (acc, item) => acc + item.thanhTien,
            0,
        );
    }

    // GUID
    guid = () => {
        let s4 = () => {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        };
        //return id of format 'aaaaaaaa'-'aaaa'-'aaaa'-'aaaa'-'aaaaaaaaaaaa'
        return (
            s4() +
            s4() +
            '-' +
            s4() +
            '-' +
            s4() +
            '-' +
            s4() +
            '-' +
            s4() +
            s4() +
            s4()
        );
    };

    // Add row
    addRow(): void {
        const chiTietIds = this.chiTietList.map((item) => item.lttpChatDotId);

        const dialogRef = this.dialog.open(MaterialDialogComponent, {
            // disableClose: true,
            width: '70%',
            data: {
                type: 'ADJUSTMENT_PURCHASE_UPGRADE',
                chiTietIds,
                currentTable: this.currentTable,
                isShow: true,
            },
            panelClass: 'add-field-dialog-class',
        });

        dialogRef.componentInstance.onSave.subscribe((data) => {
            const isSubmitted = !!data;

            this.currentTable = [...data, ...this.dataSource.data];

            dialogRef.close(isSubmitted);
        });

        dialogRef.afterClosed().subscribe((result) => {
            if (!result) return;
            this._toastrService.success('', 'Thêm thành công');
            this.dataSource.data = this.currentTable;
            this.getTotalPrice();
        });
    }

    // Delete row
    deleteAction(element: any, index: any): void {
        const dialogRef = this.dialog.open(DeleteConfirmDialogComponent, {
            width: '500px',
            data: element,
        });

        dialogRef.componentInstance.handleDelete.subscribe((dataDelete) => {
            const isSubmitted = !!dataDelete;
            dialogRef.close(isSubmitted);
        });

        dialogRef.afterClosed().subscribe((result) => {
            if (!result) return;

            const index: number = this.currentTable.indexOf(element);
            if (index !== -1) {
                this.currentTable.splice(index, 1);
            }

            this.dataSource.data = this.currentTable;
            this.getTotalPrice();

            this._toastrService.success('', 'Xóa thành công');
        });
    }

    // Back to list
    onBack(): void {
        this._router.navigate(['manage/app/category/bang-ke-mua-hang']);
    }

    // Submit data
    onSubmit(): void {
        const dataSubmit = {
            ...this.dataEdit,
            ...this.form.value,
            tongTien: this.totalPrice,
            chiTiet: [...this.currentTable],
            trangThai: 1,
            dauMoiBepId: this._dataService.getDauMoiBepId(),
        };

        if (!this.isEdit) {
            this._priceService.createPurchase(dataSubmit).subscribe(
                (res) => {
                    if (res.success) {
                        let dataLocal = JSON.parse(
                            localStorage.getItem('maMH'),
                        );

                        localStorage.setItem(
                            'maMH',
                            JSON.stringify({
                                currentDate: new Date()
                                    .toISOString()
                                    .slice(0, 10),
                                index: dataLocal.index + 1,
                            }),
                        );

                        this._router.navigate([
                            'manage/app/category/bang-ke-mua-hang',
                        ]);
                        this._toastrService.success(
                            '',
                            'Trình duyệt phiếu thành công',
                        );
                    }
                },
                (err) => this._toastrService.errorServer(err),
            );
        } else {
            this._priceService.updatePurchase(dataSubmit).subscribe(
                (res) => {
                    this._router.navigate([
                        'manage/app/category/bang-ke-mua-hang',
                    ]);
                    this._toastrService.success('', 'Cập nhật thành công');
                },
                (err) => this._toastrService.errorServer(err),
            );
        }
    }

    onSave(status = 0): void {
        const dataSubmit = {
            ...this.dataEdit,
            ...this.form.value,
            tongTien: this.totalPrice,
            chiTiet: [...this.currentTable],
            trangThai: status,
            dauMoiBepId: this._dataService.getDauMoiBepId(),
        };

        if (!this.isEdit) {
            this._priceService.createPurchase(dataSubmit).subscribe(
                (res) => {
                    if (res.success) {
                        let dataLocal = JSON.parse(
                            localStorage.getItem('maMH'),
                        );

                        localStorage.setItem(
                            'maMH',
                            JSON.stringify({
                                currentDate: new Date()
                                    .toISOString()
                                    .slice(0, 10),
                                index: dataLocal.index + 1,
                            }),
                        );

                        this._router.navigate([
                            'manage/app/category/bang-ke-mua-hang',
                        ]);
                        this._toastrService.success(
                            '',
                            'Tạo phiếu mua hàng thành công',
                        );
                    }
                },
                (err) => this._toastrService.errorServer(err),
            );
        } else {
            this._priceService.updatePurchase(dataSubmit).subscribe(
                (res) => {
                    this._router.navigate([
                        'manage/app/category/bang-ke-mua-hang',
                    ]);
                    this._toastrService.success('', 'Cập nhật thành công');
                },
                (err) => this._toastrService.errorServer(err),
            );
        }
    }

    /** Whether the number of selected elements matches the total number of rows. */
    isAllSelected() {
        const numSelected = this.selection.selected.length;
        const numRows = this.dataSource.data.length;
        return numSelected === numRows;
    }

    /** Selects all rows if they are not all selected; otherwise clear selection. */
    toggleAllRows() {
        if (this.isAllSelected()) {
            this.selection.clear();
            return;
        }

        this.selection.select(...this.dataSource.data);
    }

    /** The label for the checkbox on the passed row */
    checkboxLabel(row?: any): string {
        if (!row) {
            return `${this.isAllSelected() ? 'deselect' : 'select'} all`;
        }
        return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${
            row.index + 1
        }`;
    }

    deleteList() {
        const dialogRef = this.dialog.open(DeleteConfirmDialogComponent, {
            width: '500px',
            data: this.selection.selected,
        });

        dialogRef.componentInstance.handleDelete.subscribe((dataDelete) => {
            const isSubmitted = !!dataDelete;
            for (let item of this.selection.selected) {
                const index: number = this.currentTable.indexOf(item);

                if (index !== -1) {
                    this.currentTable.splice(index, 1);
                }
            }
            dialogRef.close(isSubmitted);
        });

        dialogRef.afterClosed().subscribe((result) => {
            if (!result) return;
            this.selection.clear();
            this.dataSource.data = this.currentTable;
            this.getTotalPrice();
            this._toastrService.success('', 'Xóa thành công');
        });
    }

    printContent(tableId: string, name?: string) {
        let printContents, popupWin;
        printContents = document.getElementById(tableId).innerHTML;
        popupWin = window.open(
            '',
            '_blank',
            'top=0,left=0,height=auto,width=auto',
        );
        popupWin.document.open();
        popupWin.document.write(
            `
                <html>
                <style type="text/css">
                    body {font-family: 'Times New Roman', Times, serif, font-size: 14px}

                    table th, table td {
                        border:1px solid #000;
                        padding: 5px;
                    }

                    table {
                        margin:0;
                        padding:0;
                        border-spacing: 0;
                        border-collapse: collapse;
                    }

                    .text-left {
                        text-align: left;
                    }

                    .no-color {
                        color: transparent
                    }

                    .mt-10 {
                        margin-top: 10px
                    }

                    .float-left {
                        floadt: left;
                    }

                    .header {
                        margin-bottom: 10px
                    }

                    .px-20 {
                        padding: 0 20px;
                    }

                    .mt-20 {
                        margin-top: 20px
                    }

                    .h-30 {
                        max-height: 30%
                    }

                    .box-sig {
                        height: 100px; width: 100%
                    }

                    .mb-20 {
                        margin-bottom: 20px
                    }

                    .flex-column {
                        flex-direction: column
                    }

                    .px-80 {
                        padding: 0 80px
                    }

                    .text-right {
                        text-align: right;
                    }

                    .p-20-5 {
                        padding: 20px 5px
                    }

                    .note:after {
                        border-bottom: 1px dotted black;
                        content: '';
                        flex: 1;
                    }

                    .max-w {
                        width: 100%
                    }

                    .ml-5 {
                        margin-left: 5px
                    }

                    .col-9, .col-md-9 {
                        width: 75%
                    }

                    .col-3, .col-md-3 {
                        width: 25%
                    }

                    .underline {
                        border-bottom: 1px dashed #999;
                        text-decoration: none;
                        width:100%;
                        border-bottom-style: double;
                    }

                    .my-10 {
                        margin-top: 10px;
                        margin-bottom: 10px;
                    }

                    .bold {
                        font-weight: bold
                    }

                    .text-center {
                        text-align: center
                    }

                    .d-flex {
                        display: flex;
                    }

                    .justify-space-between {
                        justify-content: space-between;
                    }

                    .m-0 {
                        margin: 0;
                    }

                    .italic {
                        font-style: italic;
                    }

                    .mb-10 {
                        margin-bottom: 10px
                    }

                    .w-60 {
                        width: 60%
                    }

                    .w-30 {
                        width: 30%
                    }

                    .align-center {
                        align-items: center;
                    }


                    @media print { @page { size: A4 portrait; } }
                </style>
                <head>
                    <title></title>
                </head>
                <body onload="window.print();window.close()">${printContents}</body>
                </html>`,
        );
        popupWin.document.close();
    }

    rejectAction() {
        const dialogRef = this.dialog.open(DialogRejectStatusComponent, {
            width: '500px',
            data:
                this.dataEdit && this.dataEdit.lyDoTuChoi
                    ? this.dataEdit.lyDoTuChoi
                    : '',
        });

        dialogRef.afterClosed().subscribe((result) => {
            if (result.length) {
                //Reject change status = 2
                // let optionsData = {
                //     notificationTitle: result,
                // };
                const dataSubmit = {
                    ...this.dataEdit,
                    ...this.form.value,
                    tongTien: this.totalPrice,
                    chiTiet: [...this.currentTable],
                    lyDoTuChoi: result,
                    trangThai: 2,
                };

                this._priceService.updatePurchase(dataSubmit).subscribe(
                    (res) => {
                        this._router.navigate([
                            'manage/app/category/bang-ke-mua-hang',
                        ]);
                        this._toastrService.success('', 'Cập nhật thành công');
                    },
                    (err) => this._toastrService.errorServer(err),
                );

                // this._router.navigate(['manage/app/category/xuat-kho']);
            }
        });
    }

    reviewAction() {
        console.log('reviewAction');
    }
    exportExcelCreatePage() {
        this.exportExcel(this.form.value);
    }
    exportExcelEditPage() {
        this.exportExcel(this.dataEdit);
    }

    exportExcel(dataInit) {
        //donViId
        let _currentTable = this.currentTable.map((item) => {
            return {
                ...item,
                diaChiMuaHangName: this.convertNameById(
                    item.diaChiMuaHangId,
                    'Address',
                ),
            };
        });
        let bodyDataExcel = {
            ...dataInit,
            chiTiet: [..._currentTable],
        };

        let nameFile = this.form.get('maPhieuMuaHang').value;

        this._exportService.generatePurchaseManagementExcel(
            bodyDataExcel,
            `Phieu mua hang ${nameFile}`,
        );
    }
}
