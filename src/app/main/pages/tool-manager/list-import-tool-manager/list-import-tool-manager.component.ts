import { SelectionModel } from '@angular/cdk/collections';
import {
    AfterViewInit,
    Component,
    OnDestroy,
    OnInit,
    ViewChild,
} from '@angular/core';
import {
    MatDialog,
    MatPaginator,
    MatTableDataSource,
    PageEvent,
} from '@angular/material';
import { Router } from '@angular/router';
import { DeleteConfirmDialogComponent } from '@app/_shared/dialogs/delete-confirm-dialog/delete-confirm-dialog.component';
import { CommonService } from '@core/services/common.service';
import { DataService } from '@core/services/data-service';
import { ToastrService } from '@core/services/toastr.service';
import { Subscription } from 'rxjs';

export interface IMaintenanceImport {
    id: number | string;
    ngayNhap: string;
    maPhieuNhap: string;
    soLuong: number;
    nguoiGiao: string;
    nguoiNhan: string;
    trangThai: number;
}

export const DATAS: IMaintenanceImport[] = [
    {
        id: 1,
        ngayNhap: '2023-02-03T03:49:35.423Z',
        maPhieuNhap: 'DCCD.ASP.0101',
        soLuong: 100,
        nguoiGiao: 'Nguyễn Văn Tiến',
        nguoiNhan: 'Nguyễn Mậu Cường',
        trangThai: 0,
    },
    {
        id: 2,
        ngayNhap: '2023-02-03T03:49:35.423Z',
        maPhieuNhap: 'DCCD.ASP.0101',
        soLuong: 100,
        nguoiGiao: 'Nguyễn Văn Tiến',
        nguoiNhan: 'Nguyễn Mậu Cường',
        trangThai: 0,
    },
    {
        id: 3,
        ngayNhap: '2023-02-03T03:49:35.423Z',
        maPhieuNhap: 'DCCD.ASP.0101',
        soLuong: 100,
        nguoiGiao: 'Nguyễn Văn Tiến',
        nguoiNhan: 'Nguyễn Mậu Cường',
        trangThai: 0,
    },
    {
        id: 4,
        ngayNhap: '2023-02-03T03:49:35.423Z',
        maPhieuNhap: 'DCCD.ASP.0101',
        soLuong: 100,
        nguoiGiao: 'Nguyễn Văn Tiến',
        nguoiNhan: 'Nguyễn Mậu Cường',
        trangThai: 0,
    },
    {
        id: 5,
        ngayNhap: '2023-02-03T03:49:35.423Z',
        maPhieuNhap: 'DCCD.ASP.0101',
        soLuong: 100,
        nguoiGiao: 'Nguyễn Văn Tiến',
        nguoiNhan: 'Nguyễn Mậu Cường',
        trangThai: 3,
    },
    {
        id: 6,
        ngayNhap: '2023-02-03T03:49:35.423Z',
        maPhieuNhap: 'DCCD.ASP.0101',
        soLuong: 100,
        nguoiGiao: 'Nguyễn Văn Tiến',
        nguoiNhan: 'Nguyễn Mậu Cường',
        trangThai: 3,
    },
    {
        id: 7,
        ngayNhap: '2023-02-03T03:49:35.423Z',
        maPhieuNhap: 'DCCD.ASP.0101',
        soLuong: 100,
        nguoiGiao: 'Nguyễn Văn Tiến',
        nguoiNhan: 'Nguyễn Mậu Cường',
        trangThai: 3,
    },
];

@Component({
    selector: 'app-list-import-tool-manager',
    templateUrl: './list-import-tool-manager.component.html',
    styleUrls: ['./list-import-tool-manager.component.scss'],
})
export class ListImportToolManagerComponent implements OnInit {
    @ViewChild(MatPaginator) paginator!: MatPaginator;
    subscription: Subscription;

    // Table
    displayedColumnsOfField: string[] = [
        'ngayNhap',
        'maPhieuNhap',
        'nguoiGiao',
        'nguoiNhan',
        'trangThai',
        'actions',
    ];
    dataSource = new MatTableDataSource<any>([]);
    selection = new SelectionModel<any>(true, []);

    // Pagination
    totalRows = 0;
    pageSize = 10;
    currentPage = 0;
    pageSizeOptions: number[] = [5, 10, 25, 50];
    dataSkipped: number = 0;

    // Search Advance
    fieldSearch: any[] = [
        {
            type: 'DATETIME',
            referenceValue: 'ngayNhap',
            name: 'Ngày nhập',
            defaultValue: '',
            required: '0',
            css: 'col-12 col-lg-6',
            appearance: 'legacy',
        },
        {
            type: 'SELECT',
            referenceValue: 'trangThai',
            name: 'Trạng thái',
            defaultValue: '',
            required: '0',
            options: [
                { key: 0, name: 'Lưu nháp' },
                { key: 3, name: 'Nhập kho' },
            ],
            css: 'col-12 col-lg-6',
            appearance: 'legacy',
        },
    ];

    // Init
    dauMoiBepId: number;

    // Boolean
    isFiltering: boolean = false;
    isLoading: boolean = true;

    constructor(
        private _router: Router,
        public dialog: MatDialog,
        private _toastrService: ToastrService,
        private _dataService: DataService,
        private _commonService: CommonService,
    ) {
        // This event is excecuted when Bep changes
        this.subscription = this._dataService.getData().subscribe((message) => {
            this.isLoading = true;
            this.getInitData(message.id);
        });
    }

    ngOnInit() {
        this.dauMoiBepId = this._dataService.getDauMoiBepId();
        this.getInitData(this.dauMoiBepId);
    }

    ngAfterViewInit() {
        this.dataSource.paginator = this.paginator;
    }

    ngOnDestroy(): void {
        this.subscription.unsubscribe();
    }

    getInitData(dauMoiBepId: number): void {
        this.dauMoiBepId = dauMoiBepId;

        const body = {
            maxResultCount: this.pageSize,
            skipCount: this.dataSkipped,
            dauMoiBepId,
        };

        this._commonService
            .callDataAPIShort('/api/services/read/PhieuNhapDccd/GetAll', body)
            .subscribe(
                (res) => {
                    this.dataSource.data = res.result.items;
                    setTimeout(() => {
                        this.paginator.pageIndex = this.currentPage;
                        this.paginator.length = res.result.totalCount;
                        this.isLoading = false;
                    }, 200);
                },
                (err) => this._toastrService.errorServer(err),
            );
    }

    pageChanged(event: PageEvent) {
        if (this.isFiltering) return;

        this.currentPage = event.pageIndex;
        this.dataSkipped = event.pageIndex * 10;

        this.getInitData(this.dauMoiBepId);
    }

    createAction(): void {
        this._router.navigate(['app/dung-cu-cap-duong/ds-pnk-dccd/create']);
    }

    handleAction(element: any, typeAction): void {
        this._router.navigate([
            `app/dung-cu-cap-duong/ds-pnk-dccd/edit/${element.id}`,
            { typeAction },
        ]);
    }

    deleteAction(element: any): void {
        const dialogRef = this.dialog.open(DeleteConfirmDialogComponent, {
            width: '500px',
            data: element,
        });

        dialogRef.componentInstance.handleDelete.subscribe((dataDelete) => {
            console.log('dataDelete:: ', dataDelete);
            const isSubmitted = !!dataDelete;

            this._commonService
                .callDataAPIShort(
                    '/api/services/write/PhieuNhapDccd/Delete',
                    dataDelete,
                )
                .subscribe(
                    (res) => dialogRef.close(isSubmitted),
                    (err) => this._toastrService.errorServer(err),
                );
        });

        dialogRef.afterClosed().subscribe((result) => {
            if (!result) return;

            this.getInitData(this.dauMoiBepId);
            this._toastrService.success('', 'Xóa thành công');
        });
    }

    // HANDLE SEARCH
    handleInputFilter(filterValue) {
        if (!filterValue) {
            this.isFiltering = false;
            this.getInitData(this.dauMoiBepId);
        } else {
            const bodyFilter = {
                maxResultCount: 9999,
                skipCount: 0,
                sorting: 'ngayNhap DESC',
                criterias: [
                    {
                        propertyName: 'maPhieuNhap',
                        operation: 6,
                        value: filterValue.trim().toLowerCase(),
                    },
                ],
                dauMoiBepId: this.dauMoiBepId,
            };

            this._commonService
                .callDataAPIShort(
                    '/api/services/read/PhieuNhapDccd/GetAll',
                    bodyFilter,
                )
                .subscribe((res) => {
                    this.isFiltering = true;
                    this.dataSource.data = res.result.items;
                });
        }
    }

    handleSearchAdvanced(data: any) {
        if (data.key === 'reset') {
            this.isFiltering = false;
            this.getInitData(this.dauMoiBepId);
        } else {
            let bodySubmit = {
                maxResultCount: 9999,
                skipCount: 0,
                sorting: 'ngayNhap DESC',
                criterias: [
                    {
                        propertyName: data.ngayNhap ? 'ngayNhap' : '',
                        operation: data.ngayNhap ? 'Equals' : 'Contains',
                        value: data.ngayNhap
                            ? new Date(data.ngayNhap._d).toISOString()
                            : '',
                    },
                    {
                        propertyName:
                            data.trangThai !== null && data.trangThai !== ''
                                ? 'trangThai'
                                : '',
                        operation:
                            data.trangThai !== null && data.trangThai !== ''
                                ? 'Equals'
                                : 'Contains',
                        value:
                            data.trangThai !== null && data.trangThai !== ''
                                ? data.trangThai
                                : '',
                    },
                ],
                dauMoiBepId: this.dauMoiBepId,
            };

            this._commonService
                .callDataAPIShort(
                    '/api/services/read/PhieuNhapDccd/GetAll',
                    bodySubmit,
                )
                .subscribe((response) => {
                    if (response.success) {
                        this.isFiltering = true;
                        this.dataSource = new MatTableDataSource<any>(
                            response.result.items,
                        );
                        this.dataSource.paginator = this.paginator;
                    }
                });
        }
    }
}
