import {
    AfterContentChecked,
    AfterViewInit,
    Component,
    EventEmitter,
    Input,
    OnInit,
    Output,
} from '@angular/core';
import { Router } from '@angular/router';
import { AppAuthService } from '@core/services/auth.service';
import { HoSoSucKhoeService } from '@core/services/ho-so-suc-khoe.service';
import { MenuService } from '@core/services/menu.service';
import { TokenStorageService } from '@core/services/token.service';
import { version } from '../../../../../package.json';
import { forkJoin } from 'rxjs';

import * as _ from 'lodash';
import { FormGroup } from '@angular/forms';
import { FoodyService } from '@core/services/foody.service';
import { DataService } from '@core/services/data-service';
import { ToastrService } from '@core/services/toastr.service';

export interface Menu {
    codeData: string;
    hideValue: string;
    id: string;
    name: string;
    order: number;
    parentId: string;
    valueData: string;
}

@Component({
    selector: 'm-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit, AfterContentChecked {
    @Input() menuHeader: any[] = [];
    @Input() hasOpenDraw: boolean;
    @Output() idBep = new EventEmitter();
    @Output() isNewCook = new EventEmitter();
    @Output() toggleDraw = new EventEmitter();

    timer: any;
    date = new Date();
    menus: Menu[];
    menuItemList: any;
    version: string = version;
    userCurrentLogin: any;

    form: FormGroup;
    fields: any[] = [];

    dataMenuHeader: any[] = [];
    currentOrganization;
    dauMoiBepOptions: any[] = [];

    menuTop: any[] = [];
    menuBottom: any[] = [];
    isArmy: boolean = false;

    constructor(
        private authService: AppAuthService,
        private _tokenStorageService: TokenStorageService,
        private _toastrService: ToastrService,
        public _menuService: MenuService,
        public _dataService: DataService,
        private router: Router,
        public _hssk: HoSoSucKhoeService,
        private _foodyService: FoodyService,
    ) {}

    ngOnInit() {
        this.timer = this.date;
        this.getInitData();
    }

    ngAfterContentChecked(): void {
        let arrayMenu = Array.from(this.menuHeader);
        this.dataMenuHeader = Array.from(this.menuHeader);
        this.menuTop = arrayMenu.splice(0, 4);
        this.menuBottom = arrayMenu;
    }

    getInitData(): void {
        this._hssk.getCurrentUsers().subscribe((data) => {
            let { result } = data;
            this.userCurrentLogin = result;

            const dataCurrentOrganization =
                this._hssk.getCurrentOrganizations();

            const getPermissionByUser = this._hssk.getPermissionByUser({
                tenantId: 1,
                userId: this.userCurrentLogin.id,
            });

            forkJoin([dataCurrentOrganization, getPermissionByUser]).subscribe(
                (results) => {
                    if (results[0].result.length === 0) {
                        this.notifySignOutToast();
                    } else {
                        this.currentOrganization = results[0].result.find(
                            (item) =>
                                item.groupCode.toUpperCase().includes('DONVI'),
                        );
                        // console.log('currentOrganization:: ', this.currentOrganization);

                        if (!this.currentOrganization) {
                            this.notifySignOutToast();
                        } else {
                            if (this.currentOrganization.codeData === 'BCTT') {
                                this.isArmy = true;
                                let dataCooks = [];

                                this._hssk
                                    .getChildrenOrganizations({
                                        id: results[0].result[0].id,
                                    })
                                    .subscribe((response) => {
                                        response.result.forEach((item) => {
                                            const body = {
                                                maxResultCount: 9999,
                                                sorting: 'Id',
                                                donViId: item.id,
                                            };

                                            this._foodyService
                                                .getAllDauMoiBep(body)
                                                .subscribe((res) => {
                                                    if (
                                                        res.result.items
                                                            .length > 0
                                                    ) {
                                                        dataCooks = [
                                                            {
                                                                nameParent:
                                                                    item.name,
                                                                danhSach: [
                                                                    ...res
                                                                        .result
                                                                        .items,
                                                                ],
                                                            },
                                                        ];

                                                        this.dauMoiBepOptions =
                                                            dataCooks;

                                                        const defaultBep =
                                                            this
                                                                .dauMoiBepOptions[0]
                                                                .danhSach[0];
                                                        this.handleSelectBep(
                                                            defaultBep,
                                                        );
                                                        this.isNewCook.emit(
                                                            false,
                                                        );
                                                    }
                                                });
                                        });
                                    });
                            } else {
                                this.getAllDauMoiBep(
                                    results[1].result.organizations[0]
                                        .maxPermissions[0].name,
                                );
                            }
                        }
                    }
                },
            );
        });
    }

    getAllDauMoiBep(role: string) {
        const body = {
            maxResultCount: 9999,
            sorting: 'Id',
            donViId: this.currentOrganization.id,
        };
        this._foodyService.getAllDauMoiBep(body).subscribe((res) => {
            this.dauMoiBepOptions = res.result.items.filter(
                (item) => item.trangThai,
            );

            // console.log('dauMoiBepOptions:: ', this.dauMoiBepOptions);
            if (this.dauMoiBepOptions.length === 0) {
                if (role === 'Admin') {
                    this.isNewCook.emit(true);
                } else {
                    this.notifySignOutToast();
                }
            } else {
                // Lấy bếp mặc định là bếp đầu tiên
                const defaultBep = this.dauMoiBepOptions[0];
                this.handleSelectBep(defaultBep);
                this.isNewCook.emit(false);
            }
        });
    }

    notifySignOutToast(): void {
        this._toastrService.error(
            '',
            'Tài khoản không có quyền truy cập quản lý bếp!',
        );
        setTimeout(() => this.logout(), 3000);
    }

    logout() {
        this._tokenStorageService.signOut();
        this.router.navigateByUrl('/login');
    }

    goPageVersion() {
        this.router.navigateByUrl('/versions');
    }

    handleSelectBep(dataBep) {
        this._dataService.sendData(dataBep.id);
        this._dataService.saveDataBep(dataBep);
        this.idBep.emit(dataBep.id);
    }

    getNameBep() {
        return this._dataService.getTenBep();
    }

    saveIdMenu(id: string): void {
        localStorage.setItem('idMenu', id);
    }

    navigateToAccountInfo(): void {
        this.router.navigateByUrl('/manage/app/category/thong-tin-tai-khoan');
    }

    toggleDrawHandle() {
        this.toggleDraw.emit(this.hasOpenDraw);
    }
}
