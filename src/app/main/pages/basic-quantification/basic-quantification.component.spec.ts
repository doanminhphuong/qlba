import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BasicQuantificationComponent } from './basic-quantification.component';

describe('BasicQuantificationComponent', () => {
    let component: BasicQuantificationComponent;
    let fixture: ComponentFixture<BasicQuantificationComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [BasicQuantificationComponent],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(BasicQuantificationComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
