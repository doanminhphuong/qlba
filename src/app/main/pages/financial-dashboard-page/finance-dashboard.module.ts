import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FinanceDashboardRoutingModule } from './finance-dashboard-routing.module';
import { FinanceDashboardPageComponent } from './finance-dashboard-page/finance-dashboard-page.component';
import {
    MatDividerModule,
    MatIconModule,
    MatPaginatorModule,
    MatProgressSpinnerModule,
    MatTableModule,
} from '@angular/material';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule } from '@app/_shared/layout.module';

@NgModule({
    declarations: [FinanceDashboardPageComponent],
    imports: [
        CommonModule,
        FinanceDashboardRoutingModule,
        MatIconModule,
        MatDividerModule,
        MatPaginatorModule,
        MatProgressSpinnerModule,
        MatTableModule,

        FormsModule,
        ReactiveFormsModule,
        LayoutModule,
    ],
    exports: [],
})
export class FinanceDashboardModule {}
