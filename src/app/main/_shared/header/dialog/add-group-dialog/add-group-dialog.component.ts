import {
    AfterViewChecked,
    AfterViewInit,
    ChangeDetectorRef,
    Component,
    EventEmitter,
    Inject,
    OnInit,
} from '@angular/core';
import {
    FormArray,
    FormBuilder,
    FormControl,
    FormGroup,
    Validators,
} from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
    selector: 'app-add-group-dialog',
    templateUrl: './add-group-dialog.component.html',
    styleUrls: ['./add-group-dialog.component.scss'],
})
export class AddGroupDialogComponent
    implements OnInit, AfterViewInit, AfterViewChecked
{
    onSave = new EventEmitter();
    title: string;

    form: FormGroup;

    isSubmitted: boolean = false;
    isEnableDelete: boolean = false;

    constructor(
        private readonly changeDetectorRef: ChangeDetectorRef,
        public dialogRef: MatDialogRef<AddGroupDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any,
        private fb: FormBuilder,
    ) {}

    ngOnInit() {
        console.log(this.data);
        this.getTitle();
        this.initForm();

        console.log('Data Type: ', this.data.type);
    }

    ngAfterViewInit() {
        const dataEdit = this.data.data;
        if (dataEdit) {
            this.form.patchValue(this.data.data);
        }
    }

    ngAfterViewChecked(): void {
        this.changeDetectorRef.detectChanges();
    }

    getTitle() {
        if (this.data.type === 'AddGroupData') {
            this.title = 'Thêm Tab';
        } else if (this.data.type === 'AddGroupFieldData') {
            this.title = 'Thêm mới';
        } else {
            this.title = `Chỉnh sửa ${this.data.data.name}`;
        }
    }

    initForm() {
        let fieldsCtrls = {};

        for (let f of this.data.fields) {
            if (f.type === 'NUMBER') {
                fieldsCtrls[f.referenceValue] = new FormControl(
                    f.defaultValue,
                    f.required === '1' ? [Validators.required] : [],
                );
            } else if (f.type !== 'CHECKBOX') {
                let validators = [
                    Validators.minLength(f.minLength),
                    Validators.maxLength(f.maxLength),
                    Validators.pattern(f.pattern),
                ];

                if (f.type === 'EMAIL') {
                    validators = [...validators, Validators.email];
                }

                fieldsCtrls[f.referenceValue] = new FormControl(
                    f.defaultValue || '',
                    f.required === '1'
                        ? [...validators, Validators.required]
                        : [...validators],
                );
            } else {
                //if checkbox, it need multiple
                let opts = [];
                for (let index in f.options) {
                    opts[index] = new FormControl(false);
                }
                fieldsCtrls[f.referenceValue] = new FormArray(
                    opts,
                    f.required === '1' ? [Validators.required] : [],
                );
            }
        }

        this.form = new FormGroup(fieldsCtrls);
    }

    getFields() {
        if (this.form.value.type.toUpperCase() !== 'TABLE') {
            return this.data.fields.filter(
                (item) =>
                    item.referenceValue !== 'header' &&
                    item.referenceValue !== 'fixed',
            );
        } else {
            return this.data.fields;
        }
    }

    onCloseDialog(data?: any): void {
        this.dialogRef.close(data);
    }

    onDelete(): void {
        this.isEnableDelete = true;
    }

    onCancelDelete(): void {
        this.isEnableDelete = false;
    }

    onConfirmDelete() {
        let dataSubmit = {
            data: this.data.data,
            action: 'DELETE',
        };

        this.onSave.emit(dataSubmit);
    }

    guid = () => {
        let s4 = () => {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        };
        //return id of format 'aaaaaaaa'-'aaaa'-'aaaa'-'aaaa'-'aaaaaaaaaaaa'
        return (
            s4() +
            s4() +
            '-' +
            s4() +
            '-' +
            s4() +
            '-' +
            s4() +
            '-' +
            s4() +
            s4() +
            s4()
        );
    };

    replaceWhiteSpace(): void {
        for (let f of this.data.fields) {
            if (f.type === 'TEXT' || f.type === 'TEXTAREA') {
                const initField = this.form.get(f.referenceValue);

                if (initField.value === null) {
                    initField.setValue('');
                } else {
                    const valueAfterReplace = initField.value
                        .replace(/\s+/g, ' ')
                        .trim();
                    initField.setValue(valueAfterReplace);
                }
            }
        }
    }

    onSubmit() {
        this.replaceWhiteSpace();
        let field_ITEM = this.data.data.items;
        let formValues = {};

        if (this.data.type === 'UPDATE_FIELD') {
            formValues = this.form.value;
        } else {
            formValues = {
                id: this.guid(),
                ...this.form.value,
                items:
                    this.data.type === 'AddGroupFieldData'
                        ? []
                        : [...field_ITEM],
            };
        }

        this.onSave.emit(formValues);
    }
}
