import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanSpendComponent } from './plan-spend.component';

describe('PlanSpendComponent', () => {
  let component: PlanSpendComponent;
  let fixture: ComponentFixture<PlanSpendComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlanSpendComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanSpendComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
