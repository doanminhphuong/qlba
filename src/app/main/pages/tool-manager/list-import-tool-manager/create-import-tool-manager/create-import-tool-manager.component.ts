import { SelectionModel } from '@angular/cdk/collections';
import { DatePipe } from '@angular/common';
import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatPaginator, MatTableDataSource } from '@angular/material';
import {
    MAT_MOMENT_DATE_ADAPTER_OPTIONS,
    MomentDateAdapter,
} from '@angular/material-moment-adapter';
import {
    DateAdapter,
    MAT_DATE_FORMATS,
    MAT_DATE_LOCALE,
} from '@angular/material/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DeleteConfirmDialogComponent } from '@app/_shared/dialogs/delete-confirm-dialog/delete-confirm-dialog.component';
import { DialogRejectStatusComponent } from '@app/_shared/dialogs/dialog-reject-status/dialog-reject-status.component';
import { MaintenanceDialogComponent } from '@app/_shared/dialogs/maintenance-dialog/maintenance-dialog.component';
import { CategoryService } from '@core/services/category.service';
import { CommonService } from '@core/services/common.service';
import { DataService } from '@core/services/data-service';
import { ExportExcelService } from '@core/services/export-excel.service';
import { FoodFuelService } from '@core/services/food-fuel.service';
import { InventoryService } from '@core/services/inventory.service';
import { PriceService } from '@core/services/price.service';
import { ToastrService } from '@core/services/toastr.service';
import { UserService } from '@core/services/user.service';
import { forkJoin, Subject, Subscription } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';

export const MY_FORMATS = {
    parse: {
        dateInput: 'DD/MM/YYYY', // this is how your date will be parsed from Input
    },
    display: {
        dateInput: 'DD/MM/YYYY', // this is how your date will get displayed on the Input
        monthYearLabel: 'MMMM YYYY',
        dateA11yLabel: 'LL',
        monthYearA11yLabel: 'MMMM YYYY',
    },
};
@Component({
    selector: 'app-create-import-tool-manager',
    templateUrl: './create-import-tool-manager.component.html',
    styleUrls: ['./create-import-tool-manager.component.scss'],
    providers: [
        {
            provide: DateAdapter,
            useClass: MomentDateAdapter,
            deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS],
        },

        { provide: MAT_DATE_LOCALE, useValue: 'vi-VI' },
        { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
        {
            provide: MAT_MOMENT_DATE_ADAPTER_OPTIONS,
            useValue: { useUtc: true },
        },
        DatePipe,
    ],
})
export class CreateImportToolManagerComponent implements OnInit {
    public applyFilter = new Subject<KeyboardEvent>();
    private subscription: Subscription;
    @ViewChild(MatPaginator) paginator!: MatPaginator;
    currentDate: Date = new Date();

    titleOnEdit: string;

    form: FormGroup;
    fields: any[] = [];
    fieldsClone: any[] = [];

    displayedColumns: string[] = [
        'maDungCuCapDuong',
        'tenDungCuCapDuong',
        'donViTinh',
        'phanHang',
        'soLuong',
        'actions',
    ];

    dataSource = new MatTableDataSource<any>([]);
    selection = new SelectionModel<any>(true, []);

    foodFuelList: any[] = [];

    recentlyTable: any;
    currentTable: any[] = [];
    inventoryImportId: any;
    dataEdit: any;
    isEdit: boolean = false;
    typeAction: number | string;
    isHasAdjustmentBefore: boolean = false;
    isExportData: boolean = false;
    isStatusImportSotre: boolean = false;

    listPhieuXuatKhoHoacMuaId: any[] = [];

    warehouseList: any[] = [];
    purchaseList: any[] = [];

    tempkhoNhanId: number = 0;
    tempPhieuXuatKhoHoacMuaId;
    importExportData: any[] = [];
    exportStorage: any[] = [];
    userList: any[] = [];
    shipperList: any[] = [];
    priceAdjustmentList: any[] = [];
    supplierOptions: any[] = [];
    totalPrice: number = 0;
    toggleLayoutHidden: boolean = false;
    isActiveExportExcel: boolean = false;
    priceAdjustmentId: number = 0;

    cacheId: any[] = [];
    dauMoiBepId: number;
    khoNhanId: number;

    constructor(
        private readonly changeDetectorRef: ChangeDetectorRef,
        private _activatedRoute: ActivatedRoute,
        private _toastrService: ToastrService,
        private _router: Router,
        public dialog: MatDialog,
        private _priceService: PriceService,
        private _foodFuelService: FoodFuelService,
        private _inventoryService: InventoryService,
        private _categoryService: CategoryService,
        private _userService: UserService,
        private _commonService: CommonService,
        private _exportService: ExportExcelService,
        private datePipe: DatePipe,
        private _dataService: DataService,
    ) {
        this.subscription = this.applyFilter
            .pipe(
                map((event) => (event.target as HTMLInputElement).value),
                debounceTime(500),
                distinctUntilChanged(),
            )
            .subscribe((filterValue) => this.handleInputFilter(filterValue));
    }

    ngOnInit() {
        // Check if this is Create or Edit page
        this._activatedRoute.paramMap.subscribe((param) => {
            this.inventoryImportId = +param.get('id');
            this.typeAction = +param.get('typeAction');

            if (this.typeAction === 1 || this.typeAction === 3) {
                this.displayedColumns.splice(5, 1);
            }
            if (!this.inventoryImportId) return;
            this.isEdit = true;
        });

        this.getShipperOptions();
        this.getAllFoodFuel();
        setTimeout(() => {
            this.dauMoiBepId = this._dataService.getDauMoiBepId();
            this.getInitData();
        }, 1000);
    }

    ngAfterViewChecked(): void {
        this.changeDetectorRef.detectChanges();
    }

    ngAfterViewInit(): void {}

    getBodyCategory(groupCode: string) {
        return {
            maxResultCount: 10,
            skipCount: 0,
            sorting: 'Code',
            criterias: [
                {
                    propertyName: 'GroupCode',
                    operation: 6,
                    value: groupCode,
                },
            ],
        };
    }

    getShipperOptions() {
        this._categoryService
            .getAllCategory(this.getBodyCategory('nha-cung-cap'))
            .subscribe((res) => {
                this.shipperList = res.result.map((item) => ({
                    key: item.codeData,
                    name: item.name,
                    type: item.number1,
                }));
            });
    }

    getInitData(): void {
        const dataKhoDccd = this._commonService.callDataAPIShort(
            '/api/services/read/KhoDccd/GetAll',
            {
                maxResultCount: 9999,
                // sorting: 'Id',
                criterias: [
                    {
                        propertyName: 'dauMoiBepId',
                        operation: 0,
                        value: this.dauMoiBepId,
                    },
                ],
            },
        );

        forkJoin([dataKhoDccd]).subscribe((results) => {
            const currentKhoDccd = results[0].result.items[0];
            this.khoNhanId = currentKhoDccd['id'];
            this.fields = [
                {
                    type: 'DATETIME',
                    referenceValue: 'ngayNhap',
                    name: 'Ngày nhập kho',
                    defaultValue: '',
                    required: '1',
                    disabled: this.typeAction === 1 || this.typeAction === 3,
                    css: 'col-12 col-lg-6',
                    appearance: 'legacy',
                },
                {
                    type: 'TEXT',
                    referenceValue: 'maPhieuNhap',
                    name: 'Mã phiếu nhập',
                    defaultValue: '',
                    required: '1',
                    disabled: this.typeAction === 1 || this.typeAction === 3,
                    css: 'col-12 col-lg-6',
                    appearance: 'legacy',
                },
                {
                    type: 'TEXT',
                    referenceValue: 'nguoiGiao',
                    name: 'Người giao',
                    defaultValue: '',
                    // options: [...this.userList],
                    required: '0',
                    search: '1',
                    searchCtrl: 'searchCtrl',
                    disabled: this.typeAction === 1 || this.typeAction === 3,
                    css: 'col-12 col-lg-6',
                    appearance: 'legacy',
                },
                {
                    type: 'TEXT',
                    referenceValue: 'nguoiNhan',
                    name: 'Người nhận',
                    defaultValue: '',
                    // options: [...this.userList],
                    required: '0',
                    search: '1',
                    searchCtrl: 'searchCtrl',
                    disabled: this.typeAction === 1 || this.typeAction === 3,
                    css: 'col-12 col-lg-6',
                    appearance: 'legacy',
                },
            ];

            this.getInitForm();
        });

        this.dataSource.paginator = this.paginator;
    }

    getInitForm(): void {
        let fieldsCtrls = {};
        let fieldList = [];

        let mainFields = !this.toggleLayoutHidden
            ? this.fields
            : this.fieldsClone;
        for (let f of mainFields) {
            fieldList = [...fieldList, f];

            if (f.type === 'NUMBER') {
                fieldsCtrls[f.referenceValue] = new FormControl(
                    { value: f.defaultValue, disabled: f.disabled },
                    f.required === '1' ? [Validators.required] : [],
                );
            } else if (f.type !== 'CHECKBOX') {
                let validators = [
                    Validators.minLength(f.minLength),
                    Validators.maxLength(f.maxLength),
                    Validators.pattern(f.pattern),
                ];

                if (f.type === 'EMAIL') {
                    validators = [...validators, Validators.email];
                }

                fieldsCtrls[f.referenceValue] = new FormControl(
                    {
                        value: f.defaultValue || '',
                        disabled: f.disabled,
                    },

                    f.required === '1'
                        ? [...validators, Validators.required]
                        : [...validators],
                );

                if (f.type === 'DATETIME') {
                    if (f.referenceValue === 'ngayNhap') {
                        fieldsCtrls[f.referenceValue].valueChanges.subscribe(
                            (value) => {
                                const KEY = 'NKDCCD';
                                if (this.isEdit && !this.isStatusImportSotre) {
                                    this.isStatusImportSotre = true;
                                    return;
                                }

                                let date = new Date(value);
                                let part3 = date
                                    .toLocaleDateString('en-GB')
                                    .replace(/[^0-9]/g, '');
                                let part4;

                                if (localStorage.getItem('maNKDCCD') === null) {
                                    localStorage.setItem(
                                        'maNKDCCD',
                                        JSON.stringify({
                                            currentDate: new Date()
                                                .toISOString()
                                                .slice(0, 10),
                                            index: 1,
                                        }),
                                    );

                                    let dataLocal = JSON.parse(
                                        localStorage.getItem('maNKDCCD'),
                                    );

                                    part4 = this.pad(dataLocal.index);
                                } else {
                                    let currentDate = new Date()
                                        .toISOString()
                                        .slice(0, 10);

                                    let dataLocal = JSON.parse(
                                        localStorage.getItem('maNKDCCD'),
                                    );

                                    if (currentDate === dataLocal.currentDate) {
                                        part4 = this.pad(dataLocal.index);
                                    } else {
                                        localStorage.setItem(
                                            'maNKDCCD',
                                            JSON.stringify({
                                                currentDate: new Date()
                                                    .toISOString()
                                                    .slice(0, 10),
                                                index: 1,
                                            }),
                                        );

                                        let dataLocal = JSON.parse(
                                            localStorage.getItem('maNKDCCD'),
                                        );

                                        part4 = this.pad(dataLocal.index);
                                    }
                                }

                                let fullPath = KEY + '.' + part3 + '.' + part4;
                                this.form.patchValue({
                                    maPhieuNhap: fullPath,
                                });

                                const dependField = fieldList.find(
                                    (x) => x.dependenField === f.referenceValue,
                                );

                                if (!dependField) return;

                                dependField.min = value;

                                if (this.typeAction === 3) {
                                    fieldsCtrls[
                                        dependField.referenceValue
                                    ].disable();
                                } else {
                                    fieldsCtrls[
                                        dependField.referenceValue
                                    ].enable();
                                }
                            },
                        );
                    }

                    fieldsCtrls[f.referenceValue].valueChanges.subscribe(
                        (val) => {
                            const dependField = fieldList.find(
                                (x) => x.dependenField === f.referenceValue,
                            );

                            if (!dependField) return;

                            dependField.min = val;
                            if (
                                this.typeAction === 1 ||
                                this.typeAction === 3
                            ) {
                                fieldsCtrls[
                                    dependField.referenceValue
                                ].disable();
                            } else {
                                fieldsCtrls[
                                    dependField.referenceValue
                                ].enable();
                            }
                        },
                    );
                }
                if (f.type === 'SELECT') {
                    if (f.referenceValue === 'khoNhanId') {
                        fieldsCtrls[f.referenceValue].valueChanges.subscribe(
                            (value) => {
                                this.tempkhoNhanId = value;

                                const dependField = fieldList.find(
                                    (field) =>
                                        field.dependenField ===
                                        f.referenceValue,
                                );

                                if (!dependField) return;

                                if (
                                    this.typeAction !== 1 &&
                                    this.typeAction !== 3
                                ) {
                                    fieldsCtrls[
                                        dependField.referenceValue
                                    ].enable();
                                }
                            },
                        );
                    }

                    if (f.referenceValue === 'maNguonCungCap') {
                        fieldsCtrls[f.referenceValue].valueChanges.subscribe(
                            (value) => {
                                if (value !== '') {
                                    const dependField = fieldList.find(
                                        (x) =>
                                            x.dependenField ===
                                            f.referenceValue,
                                    );

                                    if (!dependField) return;

                                    let nhaCungCap = this.shipperList.find(
                                        (shipper) => shipper.key === value,
                                    );

                                    if (nhaCungCap && nhaCungCap.type === 1) {
                                        dependField.name =
                                            'Phiếu xuất hàng từ ' +
                                            nhaCungCap.name;
                                        this.isExportData = true;

                                        fieldsCtrls[
                                            dependField.referenceValue
                                        ].disable();
                                    } else if (
                                        nhaCungCap &&
                                        nhaCungCap.type === 0
                                    ) {
                                        this.isExportData = false;
                                        dependField.name =
                                            'Phiếu mua hàng từ ' +
                                            nhaCungCap.name;

                                        if (this.typeAction === 3) {
                                            fieldsCtrls[
                                                dependField.referenceValue
                                            ].disable();
                                        } else {
                                            fieldsCtrls[
                                                dependField.referenceValue
                                            ].enable();
                                        }
                                    }
                                }
                            },
                        );
                    }
                }
            } else {
                //if checkbox, it need multiple
                let opts = {};
                for (let opt of f.options) {
                    opts[opt.key] = new FormControl({
                        value: opt.value,
                        disabled: f.disabled,
                    });
                }
                fieldsCtrls[f.referenceValue] = new FormGroup(opts);
            }
        }

        this.form = new FormGroup(fieldsCtrls);

        if (this.isEdit) {
            this.getDataEdit();
        }
    }

    pad(d) {
        return d < 10 ? '0' + d.toString() : d.toString();
    }

    transformDate(date) {
        return this.datePipe.transform(date, 'dd-MM-yyyy');
    }

    addData(event, element) {
        return (element.thanhTien = event * element.donGia);
    }

    get phieuXuatNhapHangId() {
        if (this.form && this.form.get('phieuXuatNhapHangId')) {
            return this.form.get('phieuXuatNhapHangId');
        } else {
            return null;
        }
    }

    getDataEdit(): void {
        const dataEditId = {
            id: this.inventoryImportId,
        };

        this._commonService
            .callDataAPIShort(
                '/api/services/read/PhieuNhapDccd/Get',
                dataEditId,
            )
            .subscribe((res) => {
                this.dataEdit = res.result;
                console.log('dataEdit:: ', this.dataEdit);

                switch (this.dataEdit.trangThai) {
                    case 0:
                        this.titleOnEdit = `Chỉnh sửa phiếu nhập dụng cụ cấp dưỡng '${this.dataEdit.maPhieuNhap}'`;
                        break;
                    case 3:
                        this.titleOnEdit = `Chi tiết phiếu nhập dụng cụ cấp dưỡng '${this.dataEdit.maPhieuNhap}'`;
                        break;
                }

                // Patch data to Form
                this.form.patchValue(this.dataEdit);

                // Patch data to Table (Details)
                this.currentTable = this.dataEdit.chiTiet.map((item) => ({
                    ...item,
                    ...item.lttpChatDot,
                }));
                this.dataSource.data = this.currentTable;

                // this.totalPrice = this.dataEdit.tongTien;
            });
    }

    getAllFoodFuel(): void {
        this._foodFuelService
            .getAllFoodFuel({})
            .subscribe((res) => (this.foodFuelList = res.result.items));
    }

    // Handle Input Filter
    handleInputFilter(filterValue) {
        this.dataSource.data = this.currentTable.filter((item) =>
            item.tenDungCuCapDuong
                .toLowerCase()
                .includes(filterValue.trim().toLowerCase()),
        );
    }

    handleTypeSelectPurchase(event) {
        this.form.controls['phieuXuatNhapHangId'].reset(null);
        this.dataSource.data = [];
        this.currentTable = [];
    }

    // Handle Select PMH
    handleSelectPurchase(event: any) {
        let purchaseId = event.value;
        this.dataSource.data = [];
        this.currentTable = [];
        purchaseId.forEach((item) => {
            this._priceService
                .getPurchaseById({ id: item })
                .subscribe((res) => {
                    this.currentTable = [
                        ...this.currentTable,
                        ...res.result.chiTiet.map((item) => ({
                            ...item,
                            ...item.lttpChatDot,
                            soLuongThucNhap: item.soLuong,
                            soLuongPhaiNhap: item.soLuong,
                        })),
                    ];

                    this.currentTable.forEach((item) => {
                        delete item.id;
                        delete item.lttpChatDot;
                    });

                    this.dataSource.data = this.currentTable;

                    this.getTotalPrice();

                    console.log('this.currentTable', this.currentTable);
                });
        });

        // if (this.isExportData) {
        //     purchaseId.forEach((item) => {
        //         this._commonService
        //             .callDataAPIShort(
        //                 '/api/services/read/PhieuXuatKho/Get',
        //                 {
        //                     id: item,
        //                 },
        //             )
        //             .subscribe((res) => {
        //                 this.currentTable = [
        //                     ...this.currentTable,
        //                     ...res.result.chiTiet.map((item) => ({
        //                         ...item,
        //                         ...item.lttpChatDot,
        //                         soLuongThucNhap: item.soLuongThucXuat,
        //                         soLuongPhaiNhap: item.soLuongPhaiXuat,
        //                     })),
        //                 ];

        //                 this.currentTable.forEach((item) => {
        //                     delete item.id;
        //                     delete item.lttpChatDot;
        //                 });

        //                 this.dataSource.data = this.currentTable;

        //                 this.getTotalPrice();
        //             });
        //     });
        // } else {

        // }
    }

    /** Whether the number of selected elements matches the total number of rows. */
    isAllSelected() {
        const numSelected = this.selection.selected.length;
        const numRows = this.dataSource.data.length;
        return numSelected === numRows;
    }

    /** Selects all rows if they are not all selected; otherwise clear selection. */
    toggleAllRows() {
        if (this.isAllSelected()) {
            this.selection.clear();
            return;
        }

        this.selection.select(...this.dataSource.data);
    }

    /** The label for the checkbox on the passed row */
    checkboxLabel(row?: any): string {
        if (!row) {
            return `${this.isAllSelected() ? 'deselect' : 'select'} all`;
        }
        return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${
            row.index + 1
        }`;
    }

    // GUID
    guid = () => {
        let s4 = () => {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        };
        //return id of format 'aaaaaaaa'-'aaaa'-'aaaa'-'aaaa'-'aaaaaaaaaaaa'
        return (
            s4() +
            s4() +
            '-' +
            s4() +
            '-' +
            s4() +
            '-' +
            s4() +
            '-' +
            s4() +
            s4() +
            s4()
        );
    };

    // Add row
    addRow(): void {
        const dialogRef = this.dialog.open(MaintenanceDialogComponent, {
            // disableClose: true,
            width: '70%',
            data: {
                type: 'INVENTORY_IMPORT',
                currentTable: this.currentTable,
                isShow: false,
            },
            panelClass: 'add-field-dialog-class',
        });

        dialogRef.componentInstance.onSave.subscribe((data) => {
            const isSubmitted = !!data;
            this.currentTable = [...this.dataSource.data, ...data];
            this.dataSource.data = this.currentTable;
            console.log('dataSource:: ', this.dataSource.data);
            // this.getTotalPrice();

            dialogRef.close(isSubmitted);
        });

        dialogRef.afterClosed().subscribe((result) => {
            if (!result) return;
            this._toastrService.success('', 'Thêm thành công');
        });
    }

    getTotalPrice() {
        this.dataSource.data.forEach((item) => {
            item.thanhTien = item.donGia * item.soLuongThucNhap || 0;
        });

        this.totalPrice = this.dataSource.data.reduce(
            (acc, item) => acc + item.thanhTien,
            0,
        );
    }

    // Delete row
    deleteAction(element: any): void {
        const dialogRef = this.dialog.open(DeleteConfirmDialogComponent, {
            width: '500px',
            data: element,
        });

        dialogRef.componentInstance.handleDelete.subscribe((dataDelete) => {
            const isSubmitted = !!dataDelete;

            const index: number = this.currentTable.indexOf(element);
            if (index !== -1) {
                this.currentTable.splice(index, 1);
            }

            this.dataSource.data = this.currentTable;
            // this.getTotalPrice();
            dialogRef.close(isSubmitted);
        });

        dialogRef.afterClosed().subscribe((result) => {
            if (!result) return;

            this._toastrService.success('', 'Xóa thành công');
        });
    }

    // Back to list
    onBack(): void {
        this._router.navigate(['app/dung-cu-cap-duong/ds-pnk-dccd']);
    }

    exportExcel(): void {
        console.log('exportExcel');

        // this._exportService.exportExcel(this.currentTable, 'customers');
        let khoNhanName = this.warehouseList.find((item) =>
            item.key === this.isEdit
                ? this.dataEdit.khoNhanId
                : this.form.get('khoNhanId').value,
        );
        let nhaCungCapName = this.shipperList.find((item) =>
            item.key === this.isEdit
                ? this.dataEdit.nhaCungCapId
                : this.form.get('maNguonCungCap').value,
        );

        let maPhieuNhapKhoRaw = this.isEdit
            ? this.dataEdit.maPhieuNhapKho
            : this.form.get('maPhieuNhapKho').value;
        let bodyDataEdit = {
            ...this.dataEdit,
            khoNhanName:
                khoNhanName && khoNhanName.name ? khoNhanName.name : '...',
            nhaCungCapName:
                nhaCungCapName && nhaCungCapName.name
                    ? nhaCungCapName.name
                    : '...',
        };
        let bodyDataExcel = this.isEdit
            ? bodyDataEdit
            : {
                  ...this.dataEdit,
                  ...this.form.value,
                  khoNhanName:
                      khoNhanName && khoNhanName.name
                          ? khoNhanName.name
                          : '...',
                  nhaCungCapName:
                      nhaCungCapName && nhaCungCapName.name
                          ? nhaCungCapName.name
                          : '...',
                  tongTien: this.totalPrice,
                  chiTiet: [...this.currentTable],
                  phieuXuatKhoId: this.isExportData
                      ? this.form.value.phieuXuatNhapHangId
                      : 0,
                  phieuMuaHangId: this.isExportData
                      ? 0
                      : this.form.value.phieuXuatNhapHangId,
              };

        this._exportService.generateExcel(
            bodyDataExcel,
            `NhapKho ${maPhieuNhapKhoRaw}.xlsx`,
        );
    }

    exportExcelAndSave() {
        this.isActiveExportExcel = true;
        this.onSave(0);
        this.exportExcel();
    }

    // Submit data
    onSubmit(): void {
        const dataSubmit = {
            ...this.dataEdit,
            ...this.form.value,
            chiTiet: [...this.currentTable],
            khoNhanId: this.khoNhanId,
            trangThai: 3,
        };

        console.log('dataSubmit on Submit:: ', dataSubmit);

        if (!this.isEdit) {
            this._commonService
                .callDataAPIShort(
                    '/api/services/write/PhieuNhapDccd/Create',
                    dataSubmit,
                )
                .subscribe(
                    (res) => {
                        if (res.success) {
                            let dataLocal = JSON.parse(
                                localStorage.getItem('maNKDCCD'),
                            );

                            localStorage.setItem(
                                'maNKDCCD',
                                JSON.stringify({
                                    currentDate: new Date()
                                        .toISOString()
                                        .slice(0, 10),
                                    index: dataLocal.index + 1,
                                }),
                            );

                            this._router.navigate([
                                'app/dung-cu-cap-duong/ds-pnk-dccd',
                            ]);
                            this._toastrService.success(
                                '',
                                'Tạo phiếu nhập kho thành công',
                            );
                        }
                    },
                    (err) => this._toastrService.errorServer(err),
                );
        } else {
            this._commonService
                .callDataAPIShort(
                    '/api/services/write/PhieuNhapDccd/Update',
                    dataSubmit,
                )
                .subscribe(
                    (res) => {
                        this._router.navigate([
                            'app/dung-cu-cap-duong/ds-pnk-dccd',
                        ]);
                        this._toastrService.success('', 'Cập nhật thành công');
                    },
                    (err) => this._toastrService.errorServer(err),
                );
        }
    }

    onSave(status = 0): void {
        const dataSubmit = {
            ...this.dataEdit,
            ...this.form.value,
            chiTiet: [...this.currentTable],
            khoNhanId: this.khoNhanId,
            trangThai: status,
        };

        console.log('dataSubmit on Save:: ', dataSubmit);

        if (!this.isEdit) {
            this._commonService
                .callDataAPIShort(
                    '/api/services/write/PhieuNhapDccd/Create',
                    dataSubmit,
                )
                .subscribe(
                    (res) => {
                        if (res.success) {
                            let dataLocal = JSON.parse(
                                localStorage.getItem('maNKDCCD'),
                            );

                            localStorage.setItem(
                                'maNKDCCD',
                                JSON.stringify({
                                    currentDate: new Date()
                                        .toISOString()
                                        .slice(0, 10),
                                    index: dataLocal.index + 1,
                                }),
                            );

                            if (this.isActiveExportExcel === false) {
                                this._router.navigate([
                                    'app/dung-cu-cap-duong/ds-pnk-dccd',
                                ]);
                            }
                            this._toastrService.success(
                                '',
                                'Lưu nháp phiếu nhập kho thành công',
                            );
                        }
                    },
                    (err) => this._toastrService.errorServer(err),
                );
        } else {
            this._commonService
                .callDataAPIShort(
                    '/api/services/write/PhieuNhapDccd/Update',
                    dataSubmit,
                )
                .subscribe(
                    (res) => {
                        this._router.navigate([
                            'app/dung-cu-cap-duong/ds-pnk-dccd',
                        ]);
                        this._toastrService.success('', 'Cập nhật thành công');
                    },
                    (err) => this._toastrService.errorServer(err),
                );
        }
    }

    printContent() {
        window.print();
    }

    rejectAction() {
        const dialogRef = this.dialog.open(DialogRejectStatusComponent, {
            width: '500px',
            data:
                this.dataEdit && this.dataEdit.lyDoTuChoi
                    ? this.dataEdit.lyDoTuChoi
                    : '',
        });

        dialogRef.afterClosed().subscribe((result) => {
            if (result && result.length > 0) {
                //Reject change status = 2
                // let optionsData = {
                //     lyDoTuChoi: result,
                // };
                const dataSubmit = {
                    ...this.dataEdit,
                    ...this.form.value,
                    tongTien: this.totalPrice,
                    chiTiet: [...this.currentTable],
                    lyDoTuChoi: result,
                    trangThai: 2,
                };
                this._inventoryService
                    .updateInventoryImport(dataSubmit)
                    .subscribe(
                        (res) => {
                            this._router.navigate([
                                'app/dung-cu-cap-duong/nhap-kho',
                            ]);
                            this._toastrService.success(
                                '',
                                'Cập nhật thành công',
                            );
                        },
                        (err) => this._toastrService.errorServer(err),
                    );
                // this.onSave(2, optionsData);

                // this._router.navigate(['app/dung-cu-cap-duong/xuat-kho']);
            }
        });
    }

    reviewAction() {
        console.log('reviewAction');
    }

    handleToggleLayout() {
        this.toggleLayoutHidden = !this.toggleLayoutHidden;
    }
}
