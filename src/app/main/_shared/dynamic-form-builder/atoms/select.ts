import { Component, Input } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Subject, Subscription } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';

@Component({
    selector: 'fb-select',
    template: `
        <div [formGroup]="form">
            <ng-container
                *ngIf="!isFlatForm && tabCtrl && !subGroupCtrl"
                [formGroupName]="tabCtrl"
            >
                <ng-container [formGroupName]="groupCtrl">
                    <mat-form-field
                        [appearance]="field.appearance || 'outline'"
                        [formGroupName]="getFormControlName()"
                    >
                        <mat-label>{{ field.name }}</mat-label>
                        <mat-select
                            [id]="field.referenceValue"
                            formControlName="Value"
                            [required]="field.required === '1' ? true : false"
                        >
                            <mat-option
                                *ngFor="let opt of field.options"
                                [value]="opt.key"
                                [disabled]="field.status === 'LOCK'"
                            >
                                {{ opt.name }}
                            </mat-option>
                        </mat-select>
                        <!-- VALIDATORS -->
                        <mat-error>{{ getErrorMessage() }}</mat-error>
                    </mat-form-field>
                </ng-container>
            </ng-container>

            <ng-container
                *ngIf="!isFlatForm && tabCtrl && subGroupCtrl"
                [formGroupName]="tabCtrl"
            >
                <ng-container [formGroupName]="groupCtrl">
                    <ng-container [formGroupName]="subGroupCtrl">
                        <mat-form-field
                            [appearance]="field.appearance || 'outline'"
                            [formGroupName]="getFormControlName()"
                        >
                            <mat-label>{{ field.name }}</mat-label>
                            <mat-select
                                [id]="field.referenceValue"
                                formControlName="Value"
                                [required]="
                                    field.required === '1' ? true : false
                                "
                            >
                                <mat-option
                                    *ngFor="let opt of field.options"
                                    [value]="opt.key"
                                    [disabled]="field.status === 'LOCK'"
                                >
                                    {{ opt.name }}
                                </mat-option>
                            </mat-select>
                        </mat-form-field>
                    </ng-container>
                </ng-container>
            </ng-container>

            <mat-form-field
                *ngIf="isFlatForm || !tabCtrl"
                [appearance]="field.appearance || 'outline'"
            >
                <mat-label>{{ field.name }}</mat-label>
                <mat-select
                    [id]="field.referenceValue"
                    [formControlName]="getFormControlName()"
                    [required]="field.required === '1' ? true : false"
                    (selectionChange)="handleSelectionChange($event)"
                >
                    <mat-form-field
                        *ngIf="field.search === '1'"
                        appearance="fill"
                        class="searchInner"
                    >
                        <input
                            class="cdk-text-field-autofill-monitored"
                            matInput
                            placeholder="Tìm kiếm"
                            #inputSearch
                            type="text"
                            (keyup)="
                                applyFilter.next($event); handleInput($event)
                            "
                            (keydown)="handleKeyDown($event)"
                            [formControl]="searchCtrl"
                        />

                        <mat-icon *ngIf="!searchCtrl.value" matSuffix>
                            search
                        </mat-icon>

                        <button
                            *ngIf="searchCtrl.value"
                            matSuffix
                            mat-icon-button
                            aria-label="Clear"
                            (click)="searchCtrl.reset(); search('')"
                        >
                            <mat-icon>close</mat-icon>
                        </button>
                    </mat-form-field>
                    <ng-container *ngIf="field.search !== '1'">
                        <mat-option
                            *ngFor="let opt of field.options"
                            [value]="opt.key"
                        >
                            {{ opt.name }}
                        </mat-option>
                    </ng-container>

                    <ng-container *ngIf="field.search === '1'">
                        <mat-option *ngFor="let opt of arr" [value]="opt.key">
                            {{ opt.name }}
                        </mat-option>
                    </ng-container>
                </mat-select>
                <!-- VALIDATORS -->
                <mat-error>{{ getErrorMessage() }}</mat-error>
            </mat-form-field>
        </div>
    `,
    styles: [
        `
            mat-form-field {
                width: 100%;
            }

            .searchInner {
                top: 0;
                width: 100%;
                z-index: 100;
                font-size: inherit;
                box-shadow: none;
                border-radius: 0;
                -webkit-transform: translate3d(0, 0, 0);
                background-color: #fff;
                position: sticky;
                margin-bottom: 10px;
            }

            ::ng-deep .searchInner > .mat-form-field-wrapper {
                padding-bottom: 0px;
            }

            ::ng-deep
                .searchInner
                > .mat-form-field-wrapper
                > .mat-form-field-underline {
                bottom: 0px;
            }

            .cdk-text-field-autofill-monitored:not(:-webkit-autofill) {
                animation-name: cdk-text-field-autofill-end;
            }
        `,
    ],
})
export class SelectComponent {
    @Input() field: any = {};
    @Input() form: FormGroup;
    @Input() tabCtrl: any;
    @Input() groupCtrl: any;
    @Input() subGroupCtrl: any;
    @Input() isFlatForm: boolean;

    public searchCtrl: FormControl = new FormControl();
    arr;

    ngOnInit() {
        this.arr = this.field.options;
    }

    // Filter
    public applyFilter = new Subject<KeyboardEvent>();
    private subscription: Subscription;

    get fieldName() {
        if (!this.isFlatForm && this.tabCtrl) {
            if (!this.subGroupCtrl) {
                return this.form
                    .get(this.tabCtrl)
                    .get(this.groupCtrl)
                    .get(this.field.referenceValue)
                    .get('Value');
            } else {
                return this.form
                    .get(this.tabCtrl)
                    .get(this.groupCtrl)
                    .get(this.subGroupCtrl)
                    .get(this.field.referenceValue)
                    .get('Value');
            }
        } else if (this.isFlatForm && this.tabCtrl) {
            if (!this.subGroupCtrl) {
                return this.form.controls[
                    `${this.tabCtrl}.${this.groupCtrl}.${this.field.referenceValue}`
                ];
            } else {
                return this.form.controls[
                    `${this.tabCtrl}.${this.groupCtrl}.${this.subGroupCtrl}.${this.field.referenceValue}`
                ];
            }
        } else {
            return this.form.controls[this.field.referenceValue];
        }
    }

    handleKeyDown(event: KeyboardEvent) {
        if (event.which === 32) {
            event.stopPropagation();
        }
    }

    // Filter the states list and send back to populate the selectedStates**
    search(value: string) {
        let filter = value.toLowerCase();
        let result = this.field.options.filter((option) =>
            option.name.toLowerCase().includes(filter),
        );
        return (this.arr = [...result]);
    }

    constructor() {
        this.subscription = this.applyFilter
            .pipe(
                map((event) => (event.target as HTMLInputElement).value),
                debounceTime(500),
                distinctUntilChanged(),
            )
            .subscribe((filterValue) => this.search(filterValue));
    }

    handleInput(event: KeyboardEvent): void {
        event.stopPropagation();
    }

    ngOnDestroy(): void {
        this.subscription.unsubscribe();
    }

    getFormControlName() {
        let formControlName = this.field.referenceValue;

        if (this.isFlatForm) {
            if (!this.subGroupCtrl) {
                formControlName = `${this.tabCtrl}.${this.groupCtrl}.${this.field.referenceValue}`;
            } else {
                formControlName = `${this.tabCtrl}.${this.groupCtrl}.${this.subGroupCtrl}.${this.field.referenceValue}`;
            }
        }

        return formControlName;
    }

    getErrorMessage() {
        let errorMessage = '';

        if (this.fieldName.hasError('required')) {
            errorMessage = `${this.field.name} không được để trống.`;
        }

        return errorMessage;
    }

    handleSelectionChange(event: any) {
        if (!this.field.selectionChange) return;

        this.field.selectionChange(event);
    }
}
