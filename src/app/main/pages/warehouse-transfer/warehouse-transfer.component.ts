import { SelectionModel } from '@angular/cdk/collections';
import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatPaginator, MatTableDataSource } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { DeleteConfirmDialogComponent } from '@app/_shared/dialogs/delete-confirm-dialog/delete-confirm-dialog.component';
import { MaterialDialogComponent } from '@app/_shared/dialogs/material-dialog/material-dialog.component';
import { CommonService } from '@core/services/common.service';
import { DataService } from '@core/services/data-service';
import { InventoryService } from '@core/services/inventory.service';
import { PhieuXuatKhoService } from '@core/services/phieu-xuat-kho.service';
import { PriceService } from '@core/services/price.service';
import { ToastrService } from '@core/services/toastr.service';
import { UserService } from '@core/services/user.service';
import { forkJoin, Subject, Subscription } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';

@Component({
    selector: 'app-warehouse-transfer',
    templateUrl: './warehouse-transfer.component.html',
    styleUrls: ['./warehouse-transfer.component.scss'],
})
export class WarehouseTransferComponent implements OnInit {
    public applyFilter = new Subject<KeyboardEvent>();
    private subscription: Subscription;
    @ViewChild(MatPaginator) paginator!: MatPaginator;

    titleOnEdit: string;

    form: FormGroup;
    fields: any[] = [];

    displayedColumns: string[] = [
        'tenLttpChatDot',
        'donViTinh',
        'soLuong',
        'donGia',
        'thanhTien',
        'actions',
    ];

    dataSource = new MatTableDataSource<any>([]);
    selection = new SelectionModel<any>(true, []);

    warehouseList: any[] = [];
    organizationList: any[] = [];

    currentTable: any[] = [];
    adjustmentId: any;
    dataEdit: any;
    isEdit: boolean = false;
    totalPrice: number = 0;

    isActive: boolean = false;
    warehouseExportId: number;

    constructor(
        private readonly changeDetectorRef: ChangeDetectorRef,
        private _activatedRoute: ActivatedRoute,
        private _toastrService: ToastrService,
        private _router: Router,
        public dialog: MatDialog,
        private _priceService: PriceService,
        private _inventoryService: InventoryService,
        private _phieuXuatKhoService: PhieuXuatKhoService,
        private _userService: UserService,
        private _commonService: CommonService,
        private _dataService: DataService,
    ) {
        this._commonService.checkPermission2().subscribe((response) => {
            console.log('response:: ', response);
        });

        this.subscription = this.applyFilter
            .pipe(
                map((event) => (event.target as HTMLInputElement).value),
                debounceTime(500),
                distinctUntilChanged(),
            )
            .subscribe((filterValue) => this.handleInputFilter(filterValue));

        this.subscription = this._dataService.getData().subscribe((message) => {
            this._dataService.changeDauMoiBep(message.id);
            this.getInitData(message.id);
        });
    }

    ngOnInit() {
        // Check if this is Create or Edit page
        this._activatedRoute.paramMap.subscribe((param) => {
            this.adjustmentId = +param.get('id');
            if (!this.adjustmentId) return;
            this.isEdit = true;
        });

        this.getInitData(this._dataService.getDauMoiBepId());
    }

    ngAfterViewChecked(): void {
        this.changeDetectorRef.detectChanges();
    }

    ngAfterViewInit(): void {}

    getInitData(id: number): void {
        const dataWarehouse = this._inventoryService.getAllWarehouse({
            maxResultCount: 999999999,
            criterias: [
                {
                    propertyName: 'dauMoiBepId',
                    operation: 0,
                    value: id,
                },
            ],
        });
        const dataOrganization = this._userService.getOrganization({});

        forkJoin([dataWarehouse, dataOrganization]).subscribe((results) => {
            this.warehouseList = results[0].result.items.map((item) => ({
                key: item.id,
                name: item.tenKho,
            }));

            this.organizationList = results[1].result.map((item) => ({
                key: item.id,
                name: item.name,
            }));

            if (
                this.warehouseList.length > 0 &&
                this.organizationList.length > 0
            ) {
                this.getInitForm();
            }
        });

        this.dataSource.paginator = this.paginator;
    }

    getInitForm(): void {
        this.fields = [
            {
                type: 'TEXT',
                referenceValue: 'maPhieuChuyenKho',
                name: 'Số chứng từ',
                defaultValue: '',
                required: '1',
                css: 'col-12 col-lg-3',
                appearance: 'legacy',
            },
            // {
            //     type: 'SELECT',
            //     referenceValue: 'maDonVi',
            //     name: 'Mã đơn vị',
            //     defaultValue: '',
            //     options: [...this.organizationList],
            //     required: '1',
            //     search: '1',
            //     searchCtrl: 'searchCtrl',
            //     css: 'col-12 col-lg-4',
            //     appearance: 'legacy',
            // },
            // {
            //     type: 'SELECT',
            //     referenceValue: 'nguoiChuyenKhoId',
            //     name: 'Người chuyển kho',
            //     defaultValue: '',
            //     options: [],
            //     required: '1',
            //     dependenField: 'maDonVi',
            //     disabled: true,
            //     css: 'col-12 col-lg-4',
            //     appearance: 'legacy',
            // },
            {
                type: 'SELECT',
                referenceValue: 'khoXuatId',
                name: 'Kho xuất hàng',
                defaultValue: '',
                options: [...this.warehouseList],
                required: '1',
                css: 'col-12 col-lg-3',
                appearance: 'legacy',
            },
            {
                type: 'SELECT',
                referenceValue: 'khoNhanId',
                name: 'Kho nhận hàng',
                defaultValue: '',
                options: [],
                required: '1',
                disabled: true,
                dependenField: 'khoXuatId',
                css: 'col-12 col-lg-3',
                appearance: 'legacy',
            },
            {
                type: 'DATETIME',
                referenceValue: 'ngayChuyenKho',
                name: 'Ngày chuyển kho',
                defaultValue: '',
                required: '1',
                css: 'col-12 col-lg-3',
                appearance: 'legacy',
            },
            {
                type: 'TEXTAREA',
                referenceValue: 'lyDoSuDung',
                name: 'Lý do sử dụng',
                defaultValue: '',
                required: '1',
                icon: 'assignment',
                css: 'col-12 col-lg-12',
                appearance: 'legacy',
            },
        ];

        let fieldsCtrls = {};
        let fieldList = [];

        for (let f of this.fields) {
            fieldList = [...fieldList, f];

            if (f.type === 'NUMBER') {
                fieldsCtrls[f.referenceValue] = new FormControl(
                    { value: f.defaultValue, disabled: f.disabled },
                    f.required === '1' ? [Validators.required] : [],
                );
            } else if (f.type !== 'CHECKBOX') {
                let validators = [
                    Validators.minLength(f.minLength),
                    Validators.maxLength(f.maxLength),
                    Validators.pattern(f.pattern),
                ];

                if (f.type === 'EMAIL') {
                    validators = [...validators, Validators.email];
                }

                fieldsCtrls[f.referenceValue] = new FormControl(
                    {
                        value: f.defaultValue || '',
                        disabled: f.disabled,
                    },

                    f.required === '1'
                        ? [...validators, Validators.required]
                        : [...validators],
                );

                if (f.type === 'SELECT') {
                    if (f.referenceValue === 'khoXuatId') {
                        fieldsCtrls[f.referenceValue].valueChanges.subscribe(
                            (value) => {
                                if (!value) return;

                                this.warehouseExportId = value;

                                const otherWarehouses =
                                    this.warehouseList.filter(
                                        (item) => item.key !== value,
                                    );

                                const dependField = fieldList.find(
                                    (item) =>
                                        item.dependenField === f.referenceValue,
                                );

                                dependField.options = [...otherWarehouses];
                                fieldsCtrls[
                                    dependField.referenceValue
                                ].enable();
                                this.isActive = true;
                            },
                        );
                    }

                    if (f.referenceValue === 'maDonVi') {
                        fieldsCtrls[f.referenceValue].valueChanges.subscribe(
                            (value) => {
                                const valueData = {
                                    size: '9999',
                                    page: '1',
                                    donViId: value || null,
                                };

                                const bodySubmit = {
                                    code: 'Users',
                                    valueData: JSON.stringify(valueData),
                                };

                                const dependField = fieldList.find(
                                    (field) =>
                                        field.dependenField ===
                                        f.referenceValue,
                                );
                                console.log('dependField:: ', dependField);

                                if (!dependField) return;

                                this._userService
                                    .getDataSearch(bodySubmit)
                                    .subscribe((res) => {
                                        let dataSearch = JSON.parse(
                                            res.result.valueData,
                                        );

                                        const userList = dataSearch.map(
                                            (user) => ({
                                                key: user.userId,
                                                name: `${user.ho} ${user.ten}`,
                                            }),
                                        );
                                        console.log('userList:: ', userList);

                                        fieldsCtrls[
                                            dependField.referenceValue
                                        ].enable();
                                        dependField.options = [...userList];
                                    });
                            },
                        );
                    }
                }
            } else {
                //if checkbox, it need multiple
                let opts = {};
                for (let opt of f.options) {
                    opts[opt.key] = new FormControl({
                        value: opt.value,
                        disabled: f.disabled,
                    });
                }
            }
        }

        this.form = new FormGroup(fieldsCtrls);

        if (this.isEdit) {
            this.getDataEdit();
        }
    }

    getDataEdit(): void {
        const dataEditId = {
            id: this.adjustmentId,
        };

        this._priceService
            .getPriceAdjustmentById(dataEditId)
            .subscribe((res) => {
                this.dataEdit = res.result;

                // Get Title
                this.titleOnEdit = `Chỉnh sửa phiếu điều chỉnh giá '${this.dataEdit.maDotDieuChinhGia}'`;

                // Patch data to Form
                this.form.patchValue(this.dataEdit);

                // Patch data to Table (Details)
                this.currentTable = this.dataEdit.chiTiet;
                this.dataSource.data = this.currentTable;
            });
    }

    // Handle Input Filter
    handleInputFilter(filterValue) {
        this.dataSource.data = this.currentTable.filter((item) =>
            item.tenLttpChatDot
                .toLowerCase()
                .includes(filterValue.trim().toLowerCase()),
        );
    }

    /** Whether the number of selected elements matches the total number of rows. */
    isAllSelected() {
        const numSelected = this.selection.selected.length;
        const numRows = this.dataSource.data.length;
        return numSelected === numRows;
    }

    /** Selects all rows if they are not all selected; otherwise clear selection. */
    toggleAllRows() {
        if (this.isAllSelected()) {
            this.selection.clear();
            return;
        }

        this.selection.select(...this.dataSource.data);
    }

    /** The label for the checkbox on the passed row */
    checkboxLabel(row?: any): string {
        if (!row) {
            return `${this.isAllSelected() ? 'deselect' : 'select'} all`;
        }
        return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${
            row.index + 1
        }`;
    }

    // GUID
    guid = () => {
        let s4 = () => {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        };
        //return id of format 'aaaaaaaa'-'aaaa'-'aaaa'-'aaaa'-'aaaaaaaaaaaa'
        return (
            s4() +
            s4() +
            '-' +
            s4() +
            '-' +
            s4() +
            '-' +
            s4() +
            '-' +
            s4() +
            s4() +
            s4()
        );
    };

    // Add row
    addRow(): void {
        const dialogRef = this.dialog.open(MaterialDialogComponent, {
            // disableClose: true,
            width: '70%',
            data: {
                type: 'INVENTORY_TRANSFER',
                khoXuatId: this.warehouseExportId,
                currentTable: this.currentTable,
            },
            panelClass: 'add-field-dialog-class',
        });

        dialogRef.componentInstance.onSave.subscribe((data) => {
            const isSubmitted = !!data;

            this.currentTable = [...this.dataSource.data, ...data];
            this.dataSource.data = this.currentTable;
            this.getTotalPrice();

            dialogRef.close(isSubmitted);
        });

        dialogRef.afterClosed().subscribe((result) => {
            if (!result) return;
            this._toastrService.success('', 'Thêm thành công');
        });
    }

    getTotalPrice(): void {
        this.dataSource.data.forEach((item) => {
            item.thanhTien = item.donGia * item.soLuong;
        });

        this.totalPrice = this.dataSource.data.reduce(
            (acc, item) => acc + item.thanhTien,
            0,
        );
    }

    // Delete row
    deleteAction(element: any): void {
        const dialogRef = this.dialog.open(DeleteConfirmDialogComponent, {
            width: '500px',
            data: element,
        });

        dialogRef.componentInstance.handleDelete.subscribe((dataDelete) => {
            const isSubmitted = !!dataDelete;
            dialogRef.close(isSubmitted);
        });

        dialogRef.afterClosed().subscribe((result) => {
            if (!result) return;

            this.currentTable = this.dataSource.data.filter(
                (item) => item.lttpChatDotId !== element.lttpChatDotId,
            );
            this.dataSource.data = this.currentTable;
            this.getTotalPrice();
            this._toastrService.success('', 'Xóa thành công');
        });
    }

    // Back to list
    onBack(): void {
        this._router.navigate(['manage/app/category/xuat-kho']);
    }

    // Submit data
    onSubmit(): void {
        const dataSubmit = {
            ...this.form.value,
            chiTiet: [...this.currentTable],
        };

        console.log('dataSubmit:: ', dataSubmit);

        this._inventoryService.createInventoryTransfer(dataSubmit).subscribe(
            (res) => {
                this._router.navigate(['manage/app/category/danh-sach-kho']);
                this._toastrService.success('', 'Tạo thành công');
            },
            (err) => this._toastrService.errorServer(err),
        );
    }
}
