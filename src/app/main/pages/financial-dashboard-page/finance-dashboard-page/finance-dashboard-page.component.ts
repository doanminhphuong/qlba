import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatPaginator, MatTableDataSource } from '@angular/material';
import { Statistical } from '@app/pages/financial-report/financial-report.component';
import { DialogInventoryDetailComponent } from '@app/pages/inventory-management/dialog-inventory-detail/dialog-inventory-detail.component';
import { CommonService } from '@core/services/common.service';
import { DataService } from '@core/services/data-service';
import { FoodFuelService } from '@core/services/food-fuel.service';
import { FoodyService } from '@core/services/foody.service';
import { HoSoSucKhoeService } from '@core/services/ho-so-suc-khoe.service';
import { forkJoin } from 'rxjs';

@Component({
    selector: 'app-finance-dashboard-page',
    templateUrl: './finance-dashboard-page.component.html',
    styleUrls: ['./finance-dashboard-page.component.scss'],
})
export class FinanceDashboardPageComponent implements OnInit {
    isLoading: boolean = false;
    dataTable: any[] = [];
    listMoney: any[] = [];
    listMaterialGroups: any[] = [];
    listInventories: any[] = [];
    listOrganization: any[] = [];
    statisticals: Statistical[] = [];
    basicFields: any[] = [];
    form: FormGroup;
    view: any[] = [1200, 500];
    idDauMoiBep: number | string;
    dataSource = new MatTableDataSource<any>([]);
    yesterday: any;
    displayedColumnsOfField: string[] = [
        'kho',
        'tenVT',
        'nhomNVL',
        'dvt',
        'donGia',
        'tonDau',
        'nhap',
        'xuat',
        'ton',
        'actions',
    ];
    constructor(
        private _commonService: CommonService,
        private _foodFuelService: FoodFuelService,
        private _dataService: DataService,
        public dialog: MatDialog,
        private hsskService: HoSoSucKhoeService,
        private _foodyService: FoodyService,
    ) {
        this.yesterday = `Ngày ${new Date().getDate()} tháng ${
            new Date().getMonth() + 1
        } năm ${new Date().getFullYear()}`;
    }

    @ViewChild(MatPaginator) paginator!: MatPaginator;

    async ngOnInit() {
        console.log('ngOnInit');
        this.isLoading = true;
        console.log('this.idDauMoiBep::', this.idDauMoiBep);
        await this.getDataCookId().subscribe(
            (data: { id: number | string }) => {
                this.idDauMoiBep = data.id;
                this.getDataBeforeRenderForm(data.id);
            },
        );

        let heightElement;
        if (heightElement > 1000) {
            heightElement = window.innerHeight - 600;
        } else {
            heightElement = window.innerHeight - 300;
        }
        this.view = [window.innerWidth - 500, heightElement];
    }

    // ngAfterViewInit() {
    //     console.log('ngAfterViewInit');
    // }
    // this._dataService.behaviorSubjectBepID$.subscribe(
    //     (data: { id: number | string }) => {
    //         console.log('behaviorSubjectBepID::', data);
    //         this.isLoading = true;
    //         this.getInitData(data.id);
    //     },
    // );
    getDataCookId() {
        return this._dataService.behaviorSubjectBepID$;
    }

    getDataBeforeRenderForm(id: number | string) {
        this.listMoney.splice(0, this.listMoney.length);
        const tenChamCom =
            ('0' + (new Date().getMonth() + 1)).slice(-2) +
            '-' +
            new Date().getFullYear();

        const body = {
            maxResultCount: 9999999,
            criterias: [
                {
                    propertyName: 'dauMoiBepId',
                    operation: 0,
                    value: id,
                },
                {
                    propertyName: 'tenChamCom',
                    operation: 0,
                    value: tenChamCom,
                },
            ],
        };

        this._foodyService.getAllChamCom(body).subscribe((res) => {
            var date = new Date();
            // add a day
            date.setDate(date.getDate());

            const body = {
                maxResultCount: 9999999,
                sorting: 'Id',
            };

            let groupOfFoodFuel =
                this._foodFuelService.getAllGroupOfFoodFuel(body);

            let inventory = this._commonService.callDataAPIShort(
                '/api/services/read/Kho/GetAll',
                {
                    maxResultCount: 999999999,
                    criterias: [
                        {
                            propertyName: 'dauMoiBepId',
                            operation: 0,
                            value: id,
                        },
                    ],
                },
            );

            let dataStatistical = this._commonService.callDataAPIShort(
                '/api/services/read/Dashboard/GetAll',
                {
                    level: 3,
                    dauMoiBepId: id,
                },
            );

            let dataFood = this._commonService.callDataAPIShort(
                '/api/services/read/TonKho/Count',
                {
                    level: 3,
                    dauMoiBepId: id,
                },
            );

            let dataInventory = this._commonService.callDataAPIShort(
                '/api/services/read/TonKho/GetAll',
                {
                    maxResultCount: 2147483647,
                    dauMoiBepId: id,
                },
            );

            let foodQuantification = this._commonService.callDataAPIShort(
                '/api/services/read/DinhLuongAn/GetCurrent',
                {},
            );

            let ricePortion = this._commonService.callDataAPIShort(
                '/api/services/read/BaoCao/GetBaoCaoChiaSuatCom',
                {
                    chamComId:
                        res.result.items.length > 0
                            ? res.result.items[0].id
                            : 0,
                    ngayBaoCao: date,
                },
            );

            forkJoin([
                groupOfFoodFuel,
                inventory,
                dataStatistical,
                dataInventory,
                foodQuantification,
                ricePortion,
                dataFood,
            ]).subscribe((result) => {
                this.listMaterialGroups = [
                    { key: 'all', name: 'Tất cả' },
                    ...result[0].result.items.map((item) => ({
                        key: item.tenNhomLttpChatDot,
                        name: item.tenNhomLttpChatDot,
                    })),
                ];

                this.listInventories = [
                    {
                        key: 'all',
                        name: 'Tất cả',
                    },
                    ...result[1].result.items.map((item) => ({
                        key: item.id,
                        name: item.tenKho,
                    })),
                ];

                this.statisticals = [
                    {
                        value: result[6].result,
                        title: 'Lương thực thực phẩm',
                        icon: 'widgets',
                    },
                    {
                        value: result[2].result.tongChi,
                        title: 'Tổng chi',
                        icon: 'monetization_on',
                    },
                    {
                        value: result[2].result.tongNhap,
                        title: 'Tổng nhập',
                        icon: 'trending_down',
                    },
                    {
                        value: result[2].result.tongXuat,
                        title: 'Tổng xuất',
                        icon: 'trending_up',
                        class: 'custom-icon',
                    },
                ];

                this.dataSource.data = [...result[3].result.items];
                this.dataSource.paginator = this.paginator;
                result[4].result.forEach((item) => {
                    this.listMoney.push(item.mucTienAn);
                });

                let dataOrigin = result[5].result
                    .reduce((acc, cur) => {
                        const obj = {
                            donViId: cur['donViId'],
                            chiTiet: Object.keys(cur).map((key, index) => {
                                const test = {
                                    [key]: cur[key],
                                };
                                return test;
                            }, []),
                        };

                        return acc.concat(obj);
                    }, [])
                    .map((item, index) => {
                        const chiTietMerge = item.chiTiet.reduce((acc, cur) => {
                            return {
                                ...acc,
                                ...cur,
                            };
                        }, {});

                        return {
                            ...item,
                            chiTiet: [chiTietMerge],
                        };
                    });

                this.dataTable = this.handleMergeSameDonViId(dataOrigin);

                this.dataTable.forEach((data) => {
                    this.hsskService
                        .getOrganization({ id: data.donViId })
                        .subscribe((res) =>
                            Object.assign(data, { name: res.result.name }),
                        );
                });

                this.dataTable.forEach((data) => {
                    data.chiTiet.forEach((chiTiet) =>
                        chiTiet.danhSachKhongAn.reduce((acc, cur) => {
                            const key = cur['lyDo'];
                            const obj = {
                                [key]: cur['quanSo'],
                            };
                            return Object.assign(chiTiet, {
                                ...obj,
                            });
                        }, {}),
                    );
                });

                this.getInitForm();

                setTimeout(() => {
                    this.isLoading = false;
                }, 200);
            });
        });
    }

    getInitForm(): void {
        this.basicFields = [
            {
                type: 'TEXT',
                referenceValue: 'name',
                name: 'Tìm kiếm theo tên vật tư, hàng hoá',
                defaultValue: '',
                required: '0',
                css: 'col-5 col-lg-5',
                appearance: 'legacy',
            },
            {
                type: 'SELECT',
                referenceValue: 'type',
                name: 'Chọn nhóm nguyên vật liệu',
                defaultValue: this.listMaterialGroups[0].key,
                required: '0',
                options: [...this.listMaterialGroups],
                css: 'col-3 col-lg-3',
                appearance: 'legacy',
                search: '1',
                searchCtrl: 'searchCtrl',
            },
            {
                type: 'SELECT',
                referenceValue: 'inventory',
                name: 'Chọn kho',
                defaultValue: this.listInventories[0].key,
                required: '0',
                options: [...this.listInventories],
                css: 'col-3 col-lg-3',
                appearance: 'legacy',
            },
        ];
        let fieldsCtrls = {};
        let fieldList = [];

        for (let f of this.basicFields) {
            fieldList = [...fieldList, f];

            if (f.type === 'NUMBER') {
                fieldsCtrls[f.referenceValue] = new FormControl(
                    { value: f.defaultValue, disabled: f.disabled },
                    f.required === '1' ? [Validators.required] : [],
                );
            } else if (f.type !== 'CHECKBOX') {
                let validators = [
                    Validators.minLength(f.minLength),
                    Validators.maxLength(f.maxLength),
                    Validators.pattern(f.pattern),
                ];

                if (f.type === 'EMAIL') {
                    validators = [...validators, Validators.email];
                }

                fieldsCtrls[f.referenceValue] = new FormControl(
                    {
                        value: f.defaultValue,
                        disabled: f.disabled,
                    },
                    f.required === '1'
                        ? [...validators, Validators.required]
                        : [...validators],
                );
            } else {
                //if checkbox, it need multiple
                let opts = {};
                for (let opt of f.options) {
                    opts[opt.key] = new FormControl({
                        value: opt.value,
                        disabled: f.disabled,
                    });
                }

                fieldsCtrls[f.referenceValue] = new FormGroup(opts);
            }
        }

        this.form = new FormGroup(fieldsCtrls);
    }

    handleMergeSameDonViId(datas) {
        let output = [];

        datas.forEach((item) => {
            const existingUser = output.filter((v, i) => {
                return v.donViId === item.donViId;
            });

            if (existingUser.length > 0) {
                const existingIndex = output.indexOf(existingUser[0]);
                output[existingIndex].chiTiet = [
                    ...output[existingIndex].chiTiet,
                    ...item.chiTiet,
                ];
            } else {
                output.push(item);
            }
        });

        return output;
    }

    filter() {
        let bodySubmit = {
            maxResultCount: 2147483647,
            criterias: [
                {
                    propertyName:
                        this.form.value.inventory !== 'all' ? 'khoId' : '',
                    operation:
                        this.form.value.inventory !== 'all'
                            ? 'Equals'
                            : 'Contains',
                    value:
                        this.form.value.inventory !== 'all'
                            ? this.form.value.inventory
                            : '',
                },
            ],
            tenLttpChatDot:
                this.form.value.name && this.form.value.name !== ''
                    ? this.form.value.name.trim()
                    : '',
            tenNhomLttpChatDot:
                this.form.value.type !== 'all' ? this.form.value.type : '',
            dauMoiBepId: this.idDauMoiBep,
        };

        this._commonService
            .callDataAPIShort('/api/services/read/TonKho/GetAll', bodySubmit)
            .subscribe((response) => {
                this.dataSource = new MatTableDataSource<any>(
                    response.result.items,
                );
                this.dataSource.paginator = this.paginator;
            });
    }

    handleReset() {
        let bodySubmit = {
            maxResultCount: 2147483647,
            dauMoiBepId: this.idDauMoiBep,
        };
        this._commonService
            .callDataAPIShort('/api/services/read/TonKho/GetAll', bodySubmit)
            .subscribe((response) => {
                this.dataSource = new MatTableDataSource<any>(
                    response.result.items,
                );
                this.dataSource.paginator = this.paginator;
            });
    }

    viewDetails(element: any) {
        let dialogRef = this.dialog.open(DialogInventoryDetailComponent, {
            width: '80%',
            data: {
                name: element.tenLttpChatDot,
                id: element.id,
                khoId: element.khoId,
                donGia: element.donGia,
                donViTinh: element.donViTinh,
                type: 'dashboard',
            },
            panelClass: 'my-custom-dialog-class',
        });
    }
}
