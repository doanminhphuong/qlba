import { Component, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { TestingComponent } from '@app/testing/testing.component';
import { CategoryService } from '@core/services/category.service';
import { HoSoSucKhoeService } from '@core/services/ho-so-suc-khoe.service';
import { QLDKService } from '@core/services/qldk.service';
import { ToastrService } from '@core/services/toastr.service';
import { forkJoin, Observable } from 'rxjs';

@Component({
    selector: 'app-health-examination-form',
    templateUrl: './health-examination-form.component.html',
    styleUrls: ['./health-examination-form.component.scss'],
})
export class HealthExaminationFormComponent implements OnInit {
    userId: string;

    // id đợt khám lấy tỪ API đợt khám
    medicalExaminationId: string;

    // Tự tạo, id của phiếu
    healthExaminationFormId: string;

    form: FormGroup;
    isSubmitted: boolean = false;

    //build option in select
    listMedicalExamination: any[] = [];
    listForm: any[] = [];

    // data use to render form
    formId: string = '';
    formRenderFlat: FormGroup;
    formRenderNested: FormGroup;
    dataRenderForm: any[] = [];
    tabs: any[] = [];
    isFlatForm: boolean = false;

    fields = [];

    isGotServices: boolean = false;

    constructor(
        private _activatedroute: ActivatedRoute,
        private _qldkService: QLDKService,
        private _categoryService: CategoryService,
        private _healthExaminationService: HoSoSucKhoeService,
        private _toastrService: ToastrService,
    ) {}

    ngOnInit() {
        this.initData();
    }

    getInitForm() {
        let fieldsCtrls = {};

        for (let f of this.fields) {
            if (f.type === 'NUMBER') {
                fieldsCtrls[f.referenceValue] = new FormControl(
                    f.defaultValue,
                    f.required === '1' ? [Validators.required] : [],
                );
            } else if (f.type !== 'CHECKBOX') {
                let validators = [
                    Validators.minLength(f.minLength),
                    Validators.maxLength(f.maxLength),
                    Validators.pattern(f.pattern),
                ];

                if (f.type === 'EMAIL') {
                    validators = [...validators, Validators.email];
                }

                fieldsCtrls[f.referenceValue] = new FormControl(
                    f.defaultValue || '',
                    f.required === '1'
                        ? [...validators, Validators.required]
                        : [...validators],
                );
            } else {
                //if checkbox, it need multiple
                let opts = {};
                for (let opt of f.options) {
                    opts[opt.key] = new FormControl(opt.value);
                }
                fieldsCtrls[f.referenceValue] = new FormGroup(opts);
            }
        }

        this.form = new FormGroup(fieldsCtrls);
    }

    initData() {
        this._activatedroute.paramMap.subscribe((params) => {
            this.userId = params.get('id');
            console.log('this.userId', this.userId);
        });

        let body = {
            maxResultCount: 9999,
            skipCount: 0,
            sorting: 'Code',
            criterias: [
                {
                    propertyName: 'Code',
                    operation: 6,
                    value: '',
                },
            ],
        };

        let dataMedical = this._qldkService.getAllQLDK(body);

        let dataForm = this._categoryService.getAllForm(body);

        forkJoin([dataMedical, dataForm]).subscribe((results) => {
            this.isGotServices = true;

            this.listMedicalExamination = results[0].result.items.map(
                (item) => {
                    return { key: item.id, name: item.name };
                },
            );

            this.listForm = results[1].result.map((item) => {
                return { key: item.id, name: item.name };
            });

            this.fields = [
                {
                    type: 'TEXT',
                    referenceValue: 'fullName',
                    name: 'Họ và tên',
                    defaultValue: '',
                    required: '0',
                    icon: 'code',
                    css: 'col-12 col-lg-4',
                },
                {
                    type: 'TEXT',
                    referenceValue: 'position',
                    name: 'Chức vụ',
                    defaultValue: '',
                    required: '0',
                    icon: 'title',
                    css: 'col-12 col-lg-4',
                },
                {
                    type: 'TEXT',
                    referenceValue: 'rank',
                    name: 'Cấp bậc',
                    defaultValue: '',
                    required: '0',
                    icon: 'title',
                    css: 'col-12 col-lg-4',
                },
                {
                    type: 'SELECT',
                    referenceValue: 'medicalExaminationId',
                    name: 'Chọn đợt khám',
                    defaultValue: '',
                    required: '0',
                    options: [...this.listMedicalExamination],
                    css: 'col-12 col-lg-6',
                },
                {
                    type: 'DATETIME',
                    referenceValue: 'examinationTime',
                    name: 'Ngày khám',
                    defaultValue: '',
                    required: '0',
                    icon: 'filter_vintage',
                    css: 'col-12 col-lg-6',
                },
            ];

            this.getInitForm();
        });
    }

    guid = () => {
        let s4 = () => {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        };
        //return id of format 'aaaaaaaa'-'aaaa'-'aaaa'-'aaaa'-'aaaaaaaaaaaa'
        return (
            s4() +
            s4() +
            '-' +
            s4() +
            '-' +
            s4() +
            '-' +
            s4() +
            '-' +
            s4() +
            s4() +
            s4()
        );
    };

    handleSelectionChange(data) {
        this.formId = data.value;

        this.getDataFormById();
    }

    // Get data form when select form in handleSelectionChange
    getDataFormById(): void {
        let dataSubmit = {
            id: this.formId,
        };

        this._categoryService.getFormByID(dataSubmit).subscribe((data) => {
            this.dataRenderForm = data.result;
            this.tabs = JSON.parse(data.result.valueData);
            this.getFlatForm();
            this.getNestedForm();
        });
    }

    getFlatForm() {
        this.isFlatForm = true;

        let fieldsCtrls = {};

        for (let t of this.tabs) {
            for (let g of t.items) {
                for (let f of g.items) {
                    if (f.type === 'NUMBER') {
                        fieldsCtrls[`${t.code}.${g.code}.${f.referenceValue}`] =
                            new FormControl(
                                f.defaultValue,
                                f.required === '1' ? [Validators.required] : [],
                            );
                    } else if (f.type !== 'CHECKBOX') {
                        let validators = [
                            Validators.minLength(f.minLength),
                            Validators.maxLength(f.maxLength),
                            Validators.pattern(f.pattern),
                        ];

                        if (f.type === 'EMAIL') {
                            validators = [...validators, Validators.email];
                        }

                        fieldsCtrls[`${t.code}.${g.code}.${f.referenceValue}`] =
                            new FormControl(
                                f.defaultValue || '',
                                f.required === '1'
                                    ? [...validators, Validators.required]
                                    : [...validators],
                            );
                    } else {
                        let opts = {};
                        for (let opt of f.options) {
                            opts[opt.key] = new FormControl(opt.value);
                        }
                        fieldsCtrls[`${t.code}.${g.code}.${f.referenceValue}`] =
                            new FormGroup(opts);
                    }
                }
            }
        }

        this.formRenderFlat = new FormGroup(fieldsCtrls);
    }

    getNestedForm() {
        let tabsCtrls = {};

        for (let t of this.tabs) {
            let groupCtrls = {};

            for (let g of t.items) {
                let fieldsCtrls = {};

                for (let f of g.items) {
                    if (f.type === 'NUMBER') {
                        fieldsCtrls[f.referenceValue] = new FormControl(
                            f.defaultValue,
                            f.required === '1' ? [Validators.required] : [],
                        );
                    } else if (f.type !== 'CHECKBOX') {
                        let validators = [
                            Validators.minLength(f.minLength),
                            Validators.maxLength(f.maxLength),
                            Validators.pattern(f.pattern),
                        ];

                        if (f.type === 'EMAIL') {
                            validators = [...validators, Validators.email];
                        }

                        fieldsCtrls[f.referenceValue] = new FormControl(
                            f.defaultValue || '',
                            f.required === '1'
                                ? [...validators, Validators.required]
                                : [...validators],
                        );
                    } else {
                        let opts = {};
                        for (let opt of f.options) {
                            opts[opt.key] = new FormControl(opt.value);
                        }
                        fieldsCtrls[f.referenceValue] = new FormGroup(opts);
                    }
                }

                groupCtrls[g.code] = new FormGroup(fieldsCtrls);
            }

            tabsCtrls[t.code] = new FormGroup(groupCtrls);
        }

        this.formRenderNested = new FormGroup(tabsCtrls);
    }
    //

    // Convert flat data to nested data
    traverseAndFlatten(currentNode, target, flattenedKey) {
        for (var key in currentNode) {
            if (currentNode.hasOwnProperty(key)) {
                var newKey;
                if (flattenedKey === undefined) {
                    newKey = key;
                } else {
                    newKey = flattenedKey + '.' + key;
                }

                var value = currentNode[key];
                if (typeof value === 'object') {
                    this.traverseAndFlatten(value, target, newKey);
                } else {
                    target[newKey] = value;
                }
            }
        }
    }

    flatten(obj) {
        var flattenedObject = {};
        this.traverseAndFlatten(obj, flattenedObject, undefined);
        return flattenedObject;
    }
    //

    onSubmit() {
        this.healthExaminationFormId = this.guid();

        // data to create health examination
        const dataCreateHealthExaminationForm = {
            userId: this.userId,
            id: this.healthExaminationFormId,
            medicalExaminationId: this.form.value.medicalExaminationId,
        };

        // data to create health examination detail
        let dataformRenderFlat = JSON.stringify(
            this.flatten(this.formRenderNested.value),
        );

        const dataCreateHealthExaminationFormDetail = {
            healthExaminationFormId: this.healthExaminationFormId,
            listValue: dataformRenderFlat,
            objectValue: JSON.stringify(this.formRenderNested.value),
        };

        // console.log('dataCreateHealthExaminationForm: ', dataCreateHealthExaminationForm);

        // console.log('dataCreateHealthExaminationFormDetail: ', dataCreateHealthExaminationFormDetail);

        // this._healthExaminationService.createHealthExaminationForm(dataCreateHealthExaminationForm).subscribe((result) => {
        //     console.log("Result create health examination:", result)

        //     if (result.success == true) {

        //         this._healthExaminationService.createHealthExaminationFormDetail(dataCreateHealthExaminationFormDetail).subscribe((result) => {
        //             console.log("Result create health examination detail:", result)

        //             this._toastrService.success('', 'Tạo thành công');
        //         })
        //     }
        // }, (err) => {
        //     this._toastrService.error(
        //         err.error.details,
        //         err.error.message,
        //     );
        // })

        // data form render type nested: objectValue
        // console.log("this.formRenderNested", this.formRenderNested.value)

        // data form render type flat: listValue

        // console.log("this.formRenderFlat", dataformRenderFlat);
    }
}
