import { Observable } from 'rxjs';
import { Injectable, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { LogData } from '../interfaces/log-data';
import { UtilsService } from './utils.service';
import { tap } from 'rxjs/operators';
import { TokenStorageService } from './token.service';
import { BASE_URL } from './data-service';

const TOKEN_KEY = 'auth-token';
const USER_KEY = 'auth-user';

@Injectable()
export class CircularService implements OnInit {
    constructor(
        private http: HttpClient,
        private utils: UtilsService,
        private _tokenStorageService: TokenStorageService,
    ) {}

    ngOnInit(): void {}

    getAllCirculars(data: any): Observable<any> {
        const END_POINT = '/read/ThongTu/GetAll';

        return this.http.post<any>(`${BASE_URL}${END_POINT}`, data);
    }

    createCirculars(data: any): Observable<any> {
        const END_POINT = '/write/ThongTu/Create';

        return this.http.post<any>(`${BASE_URL}${END_POINT}`, data);
    }

    updateCirculars(data: any): Observable<any> {
        const END_POINT = '/write/ThongTu/Update';

        return this.http.post<any>(`${BASE_URL}${END_POINT}`, data);
    }

    deleteCirculars(data: any): Observable<any> {
        const END_POINT = '/write/ThongTu/Delete';

        return this.http.post<any>(`${BASE_URL}${END_POINT}`, data);
    }
}
