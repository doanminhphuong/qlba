import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { CommonService } from '@core/services/common.service';
import { DataService } from '@core/services/data-service';
import { ExportExcelService } from '@core/services/export-excel.service';
import { InventoryService } from '@core/services/inventory.service';
import { ToastrService } from '@core/services/toastr.service';
import { forkJoin, Subscription } from 'rxjs';

@Component({
    selector: 'app-total-export-add-edit',
    templateUrl: './total-export-add-edit.component.html',
    styleUrls: ['./total-export-add-edit.component.scss'],
})
export class TotalExportAddEditComponent implements OnInit {
    form: FormGroup;
    field: any[] = [];
    warehouseList: any[] = [];
    reasonList: any[] = [];
    listLTTP: any[] = [];
    dataDetail: any;
    dataInfo: any;
    dataEdit: any;
    isEdit: boolean = false;
    isExportExcel: boolean = false;

    id: number = 0;
    typeAction: number;
    idPhieu: number;
    isLoading: boolean = false;
    subscription: Subscription;

    constructor(
        private _router: Router,
        private _inventoryService: InventoryService,
        private _commonService: CommonService,
        private _toastrService: ToastrService,
        private _activatedRoute: ActivatedRoute,
        private _exportService: ExportExcelService,
        private _dataService: DataService,
    ) {
        this.subscription = this._dataService.getData().subscribe((message) => {
            if (this.isEdit === false) {
                this._dataService.changeDauMoiBep(message.id);
                this.getInitData(message.id);
                this.isLoading = true;
            }
        });
    }

    ngOnInit() {
        this._activatedRoute.paramMap.subscribe((param) => {
            this.idPhieu = +param.get('id');
            this.typeAction = +param.get('typeAction');
            if (!this.idPhieu) return;
            this.isEdit = true;
        });
        setTimeout(() => {
            this.getInitData(this._dataService.getDauMoiBepId());
        }, 1000);
    }

    getInitData(id: number) {
        const dataWarehouse = this._inventoryService.getAllWarehouse({
            maxResultCount: 2147483647,
            criterias: [
                {
                    propertyName: 'dauMoiBepId',
                    operation: 0,
                    value: id,
                },
            ],
        });

        const dataReason = this._commonService.callDataAPIShort(
            '/api/services/read/Category/GetAll',
            {
                maxResultCount: 2147483647,
                criterias: [
                    {
                        propertyName: 'GroupCode',
                        operation: 6,
                        value: 'ly-do-xuat-hang',
                    },
                ],
            },
        );

        const dataLTTP = this._commonService.callDataAPIShort(
            '/api/services/read/LttpChatDot/GetAll',
            { maxResultCount: 2147483647 },
        );

        forkJoin([dataWarehouse, dataLTTP, dataReason]).subscribe((results) => {
            this.warehouseList = results[0].result.items
                .filter((item) => item.status === 'ENABLE')
                .map((item) => ({
                    key: item.id,
                    name: item.tenKho,
                }));

            this.listLTTP = results[1].result.items
                .filter((item) => item.status === 'ENABLE')
                .map((item) => ({
                    key: item.id,
                    name: item.tenLttpChatDot,
                    donViTinh: item.donViTinh,
                }));

            this.reasonList = results[2].result.items.map((item) => ({
                key: item.codeData,
                name: item.name,
            }));

            if (this.warehouseList.length > 0) {
                this.field = [
                    {
                        type: 'DATETIME',
                        referenceValue: 'ngayXuatKho',
                        name: 'Ngày tổng xuất',
                        defaultValue: '',
                        required: '1',
                        css: 'col-12 col-lg-3',
                        appearance: 'legacy',
                        min: new Date(),
                        disabled:
                            this.typeAction === 3 || this.isEdit ? true : false,
                    },
                    {
                        type: 'TEXT',
                        referenceValue: 'maPhieuXuatKho',
                        name: 'Mã phiếu tổng xuất',
                        defaultValue: '',
                        required: '1',
                        css: 'col-12 col-lg-3',
                        appearance: 'legacy',
                        disabled:
                            this.typeAction === 3 || this.isEdit ? true : false,
                    },
                    {
                        type: 'SELECT',
                        referenceValue: 'lyDoSuDung',
                        name: 'Lý do xuất',
                        defaultValue: '',
                        options: [...this.reasonList],
                        required: '1',
                        css: 'col-12 col-lg-3',
                        appearance: 'legacy',
                        disabled:
                            this.typeAction === 3
                                ? true
                                : this.isEdit
                                ? false
                                : true,
                        dependenField: 'ngayXuatKho',
                    },
                    {
                        type: 'SELECT',
                        referenceValue: 'khoXuatId',
                        name: 'Chọn kho xuất hàng',
                        defaultValue: '',
                        options: [...this.warehouseList],
                        search: '1',
                        required: '1',
                        searchCtrl: 'searchCtrl',
                        css: 'col-12 col-lg-3',
                        appearance: 'legacy',
                        selectionChange: ($event) =>
                            this.showData($event.value),
                        disabled:
                            this.typeAction === 3
                                ? true
                                : this.isEdit
                                ? false
                                : true,
                        dependenField: 'lyDoSuDung',
                    },
                    {
                        type: 'TEXT',
                        referenceValue: 'nguoiVietPhieu',
                        name: 'Người viết phiếu',
                        defaultValue: this._dataService.getNguoiVietPhieu(),
                        required: '0',
                        css: 'col-12 col-lg-3',
                        appearance: 'legacy',
                        disabled: this.typeAction === 3 ? true : false,
                    },
                    {
                        type: 'TEXT',
                        referenceValue: 'thuKho',
                        name: 'Thủ kho',
                        defaultValue: this._dataService.getThuKho(),
                        required: '0',
                        css: 'col-12 col-lg-3',
                        appearance: 'legacy',
                        disabled: this.typeAction === 3 ? true : false,
                    },
                    {
                        type: 'TEXT',
                        referenceValue: 'trucBan',
                        name: 'Trực ban',
                        defaultValue: this._dataService.getTrucBan(),
                        required: '0',
                        css: 'col-12 col-lg-3',
                        appearance: 'legacy',
                        disabled: this.typeAction === 3 ? true : false,
                    },
                    {
                        type: 'TEXT',
                        referenceValue: 'nguoiNhan',
                        name: 'Người nhận',
                        defaultValue: this._dataService.getNguoiNhan(),
                        required: '0',
                        css: 'col-12 col-lg-3',
                        appearance: 'legacy',
                        disabled: this.typeAction === 3 ? true : false,
                    },
                    {
                        type: 'TEXT',
                        referenceValue: 'nguoiDuyet',
                        name: 'Người duyệt',
                        defaultValue: this._dataService.getNguoiDuyet(),
                        required: '0',
                        css: 'col-12 col-lg-3',
                        appearance: 'legacy',
                        disabled: this.typeAction === 3 ? true : false,
                    },
                ];
                this.initForm();

                setTimeout(() => {
                    this.isLoading = false;
                }, 200);
            }
        });
    }

    initForm() {
        let fieldsCtrls = {};
        let fieldList = [];

        for (let f of this.field) {
            fieldList = [...fieldList, f];

            if (f.type === 'NUMBER') {
                fieldsCtrls[f.referenceValue] = new FormControl(
                    { value: f.defaultValue, disabled: f.disabled },
                    f.required === '1' ? [Validators.required] : [],
                );
            } else if (f.type !== 'CHECKBOX') {
                let validators = [
                    Validators.minLength(f.minLength),
                    Validators.maxLength(f.maxLength),
                    Validators.pattern(f.pattern),
                ];

                if (f.type === 'EMAIL') {
                    validators = [...validators, Validators.email];
                }

                fieldsCtrls[f.referenceValue] = new FormControl(
                    {
                        value: f.defaultValue || '',
                        disabled: f.disabled,
                    },

                    f.required === '1'
                        ? [...validators, Validators.required]
                        : [...validators],
                );

                if (f.type === 'DATETIME') {
                    fieldsCtrls['ngayXuatKho'].valueChanges.subscribe((val) => {
                        const KEY = 'PXK';
                        if (this.isEdit) {
                            return;
                        }
                        let date = new Date(val);
                        let part3 = date
                            .toLocaleDateString('en-GB')
                            .replace(/[^0-9]/g, '');

                        let fullPath = KEY + '.' + part3;
                        this.form.patchValue({
                            maPhieuXuatKho: fullPath,
                        });

                        const dependField = fieldList.find(
                            (x) => x.dependenField === f.referenceValue,
                        );

                        if (!dependField) return;

                        fieldsCtrls[dependField.referenceValue].enable();
                    });
                } else if (f.type === 'SELECT') {
                    fieldsCtrls['lyDoSuDung'].valueChanges.subscribe((val) => {
                        if (val) {
                            const dependField = fieldList.find(
                                (x) => x.dependenField === f.referenceValue,
                            );

                            if (!dependField) return;

                            if (this.typeAction === 3) {
                                fieldsCtrls[
                                    dependField.referenceValue
                                ].disable();
                            } else {
                                fieldsCtrls[
                                    dependField.referenceValue
                                ].enable();
                            }
                        }
                    });
                }
            } else {
                //if checkbox, it need multiple
                let opts = {};
                for (let opt of f.options) {
                    opts[opt.key] = new FormControl({
                        value: opt.value,
                        disabled: f.disabled,
                    });
                }
            }
        }

        this.form = new FormGroup(fieldsCtrls);

        if (this.isEdit) {
            this.getDataDetail();
        }
    }

    showData(data: number) {
        let bodySubmit = {
            khoXuatId: data,
            ngayXuatKho: this.isEdit
                ? this.dataEdit.ngayXuatKho
                : this.form.value.ngayXuatKho,
            lyDoSuDung: this.isEdit
                ? this.dataEdit.lyDoSuDung
                : this.form.value.lyDoSuDung,
        };

        this._commonService
            .callDataAPIShort(
                '/api/services/read/PhieuTongXuat/GetPhieuTong',
                bodySubmit,
            )
            .subscribe((response) => {
                this.dataDetail = response.result.chiTiet;
            });
    }

    getDataDetail() {
        this._commonService
            .callDataAPIShort('/api/services/read/PhieuTongXuat/Get', {
                id: this.idPhieu,
            })
            .subscribe((response) => {
                this.dataEdit = response.result;
                this.form.patchValue({
                    ...response.result,
                    nguoiDuyet: response.result.nguoiDuyet,
                    nguoiNhan: response.result.nguoiNhan,
                    nguoiVietPhieu: response.result.nguoiVietPhieu,
                    thuKho: response.result.thuKho,
                    trucBan: response.result.trucBan,
                    khoXuatId: response.result.khoXuatId,
                });

                this.dataDetail = response.result.chiTiet;
            });
    }

    // Trình lên phía trên: trangThai = 3
    onSubmit() {
        let cong =
            this.dataDetail.length > 0
                ? this.dataDetail.reduce((acc, item) => acc + item.thanhTien, 0)
                : 0;

        let soLuongMatHang =
            this.dataDetail.length > 0
                ? this.dataDetail.reduce((acc, item) => {
                      acc++;
                      return acc;
                  }, 0)
                : 0;

        let bodySubmit = {
            ...this.dataEdit,
            ...this.form.value,
            chiTiet: this.dataDetail,
            trangThai: 3,
            tongTien: cong,
            soLuongMatHang: soLuongMatHang,
        };

        if (!this.isEdit) {
            this._commonService
                .callDataAPIShort(
                    '/api/services/write/PhieuTongXuat/Create',
                    bodySubmit,
                )
                .subscribe((response) => {
                    if (response.success) {
                        this._router.navigateByUrl(
                            'manage/app/category/tong-xuat-kho',
                        );
                        this._toastrService.success(
                            '',
                            'Trình phiếu tổng xuất thành công',
                        );
                    }
                });
        } else {
            this._commonService
                .callDataAPIShort(
                    '/api/services/write/PhieuTongXuat/Update',
                    bodySubmit,
                )
                .subscribe((response) => {
                    if (response.success) {
                        this._router.navigateByUrl(
                            'manage/app/category/tong-xuat-kho',
                        );
                        this._toastrService.success(
                            '',
                            'Cập nhật phiếu tổng xuất thành công',
                        );
                    }
                });
        }
    }

    saveDraft() {
        let cong = this.dataDetail.reduce(
            (acc, item) => acc + item.thanhTien,
            0,
        );

        let soLuongMatHang = this.dataDetail.reduce((acc, item) => {
            acc++;
            return acc;
        }, 0);

        let bodySubmit = {
            ...this.dataEdit,
            ...this.form.value,
            chiTiet: this.dataDetail,
            trangThai: 0,
            tongTien: cong,
            soLuongMatHang: soLuongMatHang,
        };

        if (!this.isEdit) {
            this._commonService
                .callDataAPIShort(
                    '/api/services/write/PhieuTongXuat/Create',
                    bodySubmit,
                )
                .subscribe((response) => {
                    if (response.success) {
                        if (!this.isExportExcel) {
                            this._router.navigateByUrl(
                                'manage/app/category/tong-xuat-kho',
                            );
                        }
                        this._toastrService.success(
                            '',
                            'Lưu nháp phiếu tổng xuất thành công',
                        );
                    }
                });
        } else {
            this._commonService
                .callDataAPIShort(
                    '/api/services/write/PhieuTongXuat/Update',
                    bodySubmit,
                )
                .subscribe((response) => {
                    if (response.success) {
                        if (!this.isExportExcel) {
                            this._router.navigateByUrl(
                                'manage/app/category/tong-xuat-kho',
                            );
                        }
                        this._toastrService.success(
                            '',
                            'Cập nhật phiếu tổng xuất thành công',
                        );
                    }
                });
        }
    }

    convertIdToResult(id: number, type: string) {
        let result;
        if (this.listLTTP.length > 0) {
            const dataFound = this.listLTTP.find((item) => item.key === id);

            switch (type) {
                case 'dvt':
                    result = dataFound ? dataFound.donViTinh : '';
                    break;
                case 'name':
                    result = dataFound ? dataFound.name : '';
                    break;
            }
        }
        return result;
    }

    exportExcel() {
        if (this.typeAction !== 3) {
            this.isExportExcel = true;
            this.saveDraft();
        }

        let cong = this.dataDetail.reduce(
            (acc, item) => acc + item.thanhTien,
            0,
        );

        let soLuongMatHang = this.dataDetail.reduce((acc, item) => {
            acc++;
            return acc;
        }, 0);
        let maPhieuTongXuatKho = this.isEdit
            ? this.dataEdit.maPhieuXuatKho
            : this.form.value.maPhieuXuatKho;
        let bodyDataExcel = this.isEdit
            ? {
                  ...this.dataEdit,
                  ...this.form.value,
                  lyDoSuDung: this.reasonList.find(
                      (item) => item.key === this.dataEdit.lyDoSuDung,
                  ),
                  chiTiet: this.dataDetail.map((item) => ({
                      ...item,
                      tenLttpChatDot: this.convertIdToResult(
                          item.lttpChatDotId,
                          'name',
                      ),
                  })),
                  khoNhanName: this.warehouseList.find(
                      (item) => item.key === this.dataEdit.khoXuatId,
                  ),
              }
            : {
                  ...this.form.value,
                  lyDoSuDung: this.reasonList.find(
                      (item) => item.key === this.form.get('lyDoSuDung').value,
                  ),
                  chiTiet: this.dataDetail.map((item) => ({
                      ...item,
                      tenLttpChatDot: this.convertIdToResult(
                          item.lttpChatDotId,
                          'name',
                      ),
                  })),
                  khoNhanName: this.warehouseList.find(
                      (item) => item.key === this.form.get('khoXuatId').value,
                  ),
                  tongTien: cong,
                  soLuongMatHang: soLuongMatHang,
              };

        this._exportService.generateExportTotalReportExcel(
            bodyDataExcel,
            `TongXuatKho ${maPhieuTongXuatKho}.xlsx`,
        );
    }

    onBack() {
        this._router.navigateByUrl('manage/app/category/tong-xuat-kho');
    }
}
