import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SummaryImportComponent } from './summary-import.component';

describe('SummaryImportComponent', () => {
  let component: SummaryImportComponent;
  let fixture: ComponentFixture<SummaryImportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SummaryImportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SummaryImportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
