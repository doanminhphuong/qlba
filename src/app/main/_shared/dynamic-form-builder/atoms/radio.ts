import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
    selector: 'fb-radio',
    template: `
        <div [formGroup]="form">
            <ng-container
                *ngIf="!isFlatForm && tabCtrl"
                [formGroupName]="tabCtrl"
            >
                <ng-container [formGroupName]="groupCtrl">
                    <mat-radio-group
                        class="wrapper"
                        [formControlName]="getFormControlName()"
                    >
                        <label>{{ field.name }}</label>
                        <mat-radio-button
                            *ngFor="let opt of field.options"
                            [value]="opt.key"
                            [required]="field.required === '1' ? true : false"
                        >
                            {{ opt.name }}
                        </mat-radio-button>
                    </mat-radio-group>
                </ng-container>
            </ng-container>

            <mat-radio-group
                *ngIf="isFlatForm || !tabCtrl"
                class="wrapper"
                [formControlName]="getFormControlName()"
            >
                <label *ngIf="field.name !== ''">{{ field.name }}</label>
                <div class="content">
                    <mat-radio-button
                        *ngFor="let opt of field.options"
                        [value]="opt.key"
                        [required]="field.required === '1' ? true : false"
                    >
                        {{ opt.name }}
                    </mat-radio-button>
                </div>
            </mat-radio-group>
        </div>
    `,
    styles: [
        `
            .mat-radio-button + .mat-radio-button {
                margin-left: 50px;
            }

            .wrapper {
                display: flex;
                align-items: center;
                justify-content: space-between;
                margin: 1.25em 0 2.25em 0;
            }

            label {
                max-width: 300px;
                font-weight: 600;
            }
        `,
    ],
})
export class RadioComponent {
    @Input() field: any = {};
    @Input() form: FormGroup;
    @Input() tabCtrl: any;
    @Input() groupCtrl: any;
    @Input() isFlatForm: boolean;

    get fieldName() {
        if (!this.isFlatForm && this.tabCtrl) {
            return this.form
                .get(this.tabCtrl)
                .get(this.groupCtrl)
                .get(this.field.referenceValue);
        } else if (this.isFlatForm && this.tabCtrl) {
            return this.form.controls[
                `${this.tabCtrl}.${this.groupCtrl}.${this.field.referenceValue}`
            ];
        } else {
            return this.form.controls[this.field.referenceValue];
        }
    }

    getFormControlName() {
        if (this.isFlatForm) {
            return `${this.tabCtrl}.${this.groupCtrl}.${this.field.referenceValue}`;
        } else {
            return this.field.referenceValue;
        }
    }

    getErrorMessage() {
        let errorMessage = '';

        if (this.fieldName.hasError('required')) {
            errorMessage = `${this.field.name} không được để trống.`;
        }

        return errorMessage;
    }
}
