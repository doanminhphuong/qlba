import {
    CommonModule,
    CurrencyPipe,
    HashLocationStrategy,
    LocationStrategy,
} from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { Injector, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { JsonpModule } from '@angular/http';

import { BsDatepickerModule, ModalModule } from 'ngx-bootstrap';
import { NgxPaginationModule } from 'ngx-pagination';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { AbpModule } from '@abp/abp.module';

import { ServiceProxyModule } from '@shared/service-proxies/service-proxy.module';
import { SharedModule } from '@shared/shared.module';
import { LazyLoadImagesModule } from 'ngx-lazy-load-images';
import { LayoutModule } from './_shared/layout.module';

import { CoreModule } from '@core/core.module';
import { OrderModule } from 'ngx-order-pipe';
import { HomeComponent } from './home/home.component';
import { PostsSlideComponent } from './posts/posts-slide/posts-slide.component';
import { MarkdownModule } from 'ngx-markdown';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { BrowserModule } from '@angular/platform-browser';

// import 'prismjs';
// import 'prismjs/components/prism-typescript.min.js';
// import 'prismjs/plugins/line-numbers/prism-line-numbers.js';
// import 'prismjs/plugins/line-highlight/prism-line-highlight.js';
//More option
import {
    MatAutocompleteModule,
    MatButtonModule,
    MatCardModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatDialogModule,
    MatDividerModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatSelectModule,
    MatSidenavModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatTableModule,
    MatTabsModule,
    MatTooltipModule,
    MAT_DIALOG_DATA,
} from '@angular/material';
import { MatDatepickerModule as MatDatepickerModuleCustom } from '@angular/material/datepicker';

import { MatChipsModule } from '@angular/material/chips';
import { LoginComponent } from './auth/login/login.component';
import { SetTokenComponent } from './auth/set-token/set-token.component';
import { DynamicDialogComponent } from './components/dynamic-dialog-component/dynamic-dialog-component.component';
// import { DynamicRouterComponent } from './components/dynamic-router-component/dynamic-router-component.component';
import { FieldPageComponent } from './components/field-page/field-page.component';
import { FormPageComponent } from './components/form-page/form-page.component';
import { HealthExaminationFormComponent } from './components/ho-so-suc-khoe/health-examination-form/health-examination-form.component';
import { HoSoSucKhoeComponent } from './components/ho-so-suc-khoe/ho-so-suc-khoe.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { QldkComponent } from './components/qldk/qldk.component';
import { FormTableComponent } from './testing/form-table/form-table.component';
import { TestingComponent } from './testing/testing.component';
import { SearchComponent } from './_shared/common/search/search.component';
import { CategoryDialogComponent } from './_shared/dialogs/category-dialog/category-dialog.component';
import { FieldConfigDialogComponent } from './_shared/dialogs/field-config-dialog/field-config-dialog.component';
import { QldkDialogComponent } from './_shared/dialogs/qldk-dialog/qldk-dialog.component';
import { AddFieldDialogComponent } from './_shared/header/dialog/add-field-dialog/add-field-dialog.component';
import { AddGroupDialogComponent } from './_shared/header/dialog/add-group-dialog/add-group-dialog.component';
import { AddTabDialogComponent } from './_shared/header/dialog/add-tab-dialog/add-tab-dialog.component';
import { FieldDialogComponent } from './_shared/header/dialog/field-dialog/field-dialog.component';
import { RenderFormDialogComponent } from './_shared/header/dialog/render-form-dialog/render-form-dialog.component';
import { FoodFuelManagementComponent } from './pages/food-fuel-management/food-fuel-management.component';
import { DeleteConfirmDialogComponent } from './_shared/dialogs/delete-confirm-dialog/delete-confirm-dialog.component';
import { PriceManagementComponent } from './pages/price-management/price-management.component';
import { AdjustmentAddEditComponent } from './pages/price-management/adjustment-add-edit/adjustment-add-edit.component';
import { MaterialDialogComponent } from './_shared/dialogs/material-dialog/material-dialog.component';
import { WarehouseManagementComponent } from './pages/warehouse-management/warehouse-management.component';
import { InventoryBeforeManagementComponent } from './pages/inventory-before-management/inventory-before-management.component';
import { PriceConvertPipe } from '@core/pipes/price-convert.pipe';
import { PurchaseManagementComponent } from './pages/purchase-management/purchase-management.component';
import { PurchaseAddEditComponent } from './pages/purchase-management/purchase-add-edit/purchase-add-edit.component';
import { FoodFuelGroupComponent } from './pages/food-fuel-group/food-fuel-group.component';
import { DialogInventoryDetailComponent } from './pages/inventory-management/dialog-inventory-detail/dialog-inventory-detail.component';
import { InventoryManagementComponent } from './pages/inventory-management/inventory-management.component';
import { InventoryImportComponent } from './pages/inventory-import/inventory-import.component';
// import { BlankComponent } from './components/blank/blank.component';
import { ExportWarehouseComponent } from './pages/export-warehouse/export-warehouse.component';
import { NoopInterceptor } from '@angular/common/http/src/interceptor';
import { ExportWarehouseAddEditComponent } from './pages/export-warehouse/export-warehouse-add-edit/export-warehouse-add-edit.component';
import { ImportAddEditComponent } from './pages/inventory-import/import-add-edit/import-add-edit.component';
import { WarehouseTransferComponent } from './pages/warehouse-transfer/warehouse-transfer.component';
import { FoodyManagementComponent } from './pages/foody-management/foody-management.component';
import { FoodyAddEditComponent } from './pages/foody-management/foody-add-edit/foody-add-edit.component';
import { CircularsManagementComponent } from './pages/circulars-management/circulars-management.component';
import { DialogRejectStatusComponent } from './_shared/dialogs/dialog-reject-status/dialog-reject-status.component';
import { ManagementQuantityComponent } from './pages/management-quantity/management-quantity.component';
import { ManagementQuantityAddEditComponent } from './pages/management-quantity/management-quantity-add-edit/management-quantity-add-edit.component';
import { DialogQuantitativeComponent } from './_shared/dialogs/dialog-quantitative/dialog-quantitative.component';
import { MenuListManagementComponent } from './pages/menu-list-management/menu-list-management.component';
import { MenuListAddEditComponent } from './pages/menu-list-management/menu-list-add-edit/menu-list-add-edit.component';
import { FoodListDialogComponent } from './pages/menu-list-management/menu-list-add-edit/food-list-dialog/food-list-dialog.component';
import { DeleteConfirmAllDialogComponent } from './_shared/dialogs/delete-confirm-all-dialog/delete-confirm-all-dialog.component';
import { MarkRiceComponent } from './pages/mark-rice/mark-rice.component';
import { ConvertNumToStrPipe } from './pages/purchase-management/purchase-add-edit/convert-num-to-str.pipe';
import { ListOfFoodPlansComponent } from './pages/list-of-food-plans/list-of-food-plans.component';
import { ListOfFoodPlansAddEditComponent } from './pages/list-of-food-plans/list-of-food-plans-add-edit/list-of-food-plans-add-edit.component';
import { DialogEstimateFoodComponent } from './_shared/dialogs/dialog-estimate-food/dialog-estimate-food.component';
import { MarkRiceDetailsComponent } from './pages/mark-rice/mark-rice-details/mark-rice-details.component';
import { EaterManagementComponent } from './pages/eater-management/eater-management.component';
import { EaterAddEditComponent } from './pages/eater-management/eater-add-edit/eater-add-edit.component';
import { EaterInfoDialogComponent } from './pages/eater-management/eater-add-edit/eater-info-dialog/eater-info-dialog.component';
import { PureStoreManagementComponent } from './pages/pure-store-management/pure-store-management.component';
import { AddPureStoreComponent } from './pages/pure-store-management/add-pure-store/add-pure-store.component';
import { DialogSwapUnitComponent } from './_shared/dialogs/dialog-swap-unit/dialog-swap-unit.component';
import { DialogMealRateComponent } from './_shared/dialogs/dialog-meal-rate/dialog-meal-rate.component';
import { DocumentPageComponent } from './pages/document-page/document-page.component';
import { SwapunitComponent } from './pages/swapunit/swapunit.component';
import { FinancialReportComponent } from './pages/financial-report/financial-report.component';
import { CookManagerComponent } from './pages/cook-manager/cook-manager.component';
import { CookManagerAddEditComponent } from './pages/cook-manager/cook-manager-add-edit/cook-manager-add-edit.component';
import { DialogCookManagerComponent } from './_shared/dialogs/dialog-cook-manager/dialog-cook-manager.component';
import { DialogEatobjectComponent } from './_shared/dialogs/dialog-eatobject/dialog-eatobject.component';
import { EatObjectsComponent } from './pages/eat-objects/eat-objects.component';
import { TotalImportAddEditComponent } from './pages/total-import/total-import-add-edit/total-import-add-edit.component';
import { ListTotalImportComponent } from './pages/total-import/list-total-import/list-total-import.component';
// import { FinancialDashboardComponent } from './pages/financial-report/financial-dashboard/financial-dashboard.component';
import { ListTotalExportComponent } from './pages/total-export/list-total-export/list-total-export.component';
import { TotalExportAddEditComponent } from './pages/total-export/total-export-add-edit/total-export-add-edit.component';
import { WarehouseReportComponent } from './pages/warehouse-report/warehouse-report.component';
import { LTTPReportComponent } from './pages/lttp-report/lttp-report.component';
import { MaintenanceToolManagementComponent } from './pages/maintenance-tool-management/maintenance-tool-management.component';
import { MaintenanceImportComponent } from './pages/maintenance-import/maintenance-import.component';
import { MaintenanceAddEditComponent } from './pages/maintenance-import/maintenance-add-edit/maintenance-add-edit.component';
import { MaintenanceDialogComponent } from './_shared/dialogs/maintenance-dialog/maintenance-dialog.component';
import { MaintenanceBeforeComponent } from './pages/maintenance-before/maintenance-before.component';
import { MaintenanceInventoryComponent } from './pages/maintenance-inventory/maintenance-inventory.component';
import { InventoryDetailsDialogComponent } from './pages/maintenance-inventory/inventory-details-dialog/inventory-details-dialog.component';
import { MaintenancePureStoreComponent } from './pages/maintenance-pure-store/maintenance-pure-store.component';
import { PureAddEditComponent } from './pages/maintenance-pure-store/pure-add-edit/pure-add-edit.component';
import { KitchenOperationManagementComponent } from './pages/kitchen-operation-management/kitchen-operation-management.component';
import { BoilerStoveLogComponent } from './pages/kitchen-operation-management/boiler-stove-log/boiler-stove-log.component';
import { BoilerAddEditComponent } from './pages/kitchen-operation-management/boiler-stove-log/boiler-add-edit/boiler-add-edit.component';
import {
    NgxMatDatetimePickerModule,
    NgxMatTimepickerModule,
} from '@angular-material-components/datetime-picker';
import { NgxMatMomentModule } from '@angular-material-components/moment-adapter';
import { AccountInfoComponent } from './pages/account-info/account-info.component';
import { ReportResultProductComponent } from './pages/report-result-product/report-result-product.component';
import { TableReportResultComponent } from './pages/report-result-product/table-report-result/table-report-result.component';
import { DialogQuanntitiveRegulationComponent } from './_shared/dialogs/dialog-quanntitive-regulation/dialog-quanntitive-regulation.component';
import { StatisticalChiefComponent } from './pages/statisticals/chief/statistical-chief.component';
import { SelfSufficientReportComponent } from './pages/self-sufficient-report/self-sufficient-report.component';
import { PagesModule } from './pages/pages.module';
import { FoodPlanComponentComponent } from './pages/food-security-plan/food-plan-component/food-plan-component.component';
import { FoodPlanAddEditComponentComponent } from './pages/food-security-plan/food-plan-add-edit-component/food-plan-add-edit-component.component';
import { BasicQuantificationComponent } from './pages/basic-quantification/basic-quantification.component';

@NgModule({
    declarations: [
        AppComponent,
        HomeComponent,
        PostsSlideComponent,
        LoginComponent,
        // DynamicRouterComponent,
        PageNotFoundComponent,
        SetTokenComponent,
        TestingComponent,
        DynamicDialogComponent,
        FieldConfigDialogComponent,
        QldkComponent,
        FieldPageComponent,
        AddGroupDialogComponent,
        AddFieldDialogComponent,
        MaterialDialogComponent,
        MaintenanceDialogComponent,

        FormPageComponent,
        CategoryDialogComponent,
        HoSoSucKhoeComponent,
        HealthExaminationFormComponent,
        FormTableComponent,
        FoodFuelManagementComponent,
        PriceManagementComponent,
        AdjustmentAddEditComponent,
        WarehouseManagementComponent,
        InventoryBeforeManagementComponent,
        PriceConvertPipe,
        PurchaseManagementComponent,
        PurchaseAddEditComponent,
        FoodFuelGroupComponent,
        InventoryManagementComponent,
        DialogInventoryDetailComponent,
        InventoryDetailsDialogComponent,
        InventoryImportComponent,
        ExportWarehouseComponent,
        // BlankComponent,
        ExportWarehouseAddEditComponent,
        ImportAddEditComponent,
        WarehouseTransferComponent,
        FoodyManagementComponent,
        FoodyAddEditComponent,
        CircularsManagementComponent,
        ManagementQuantityComponent,
        ManagementQuantityAddEditComponent,
        MenuListManagementComponent,
        MenuListAddEditComponent,
        FoodListDialogComponent,
        MarkRiceComponent,
        ConvertNumToStrPipe,
        ListOfFoodPlansComponent,
        ListOfFoodPlansAddEditComponent,
        MarkRiceDetailsComponent,
        EaterManagementComponent,
        EaterAddEditComponent,
        EaterInfoDialogComponent,
        PureStoreManagementComponent,
        AddPureStoreComponent,
        DocumentPageComponent,
        SwapunitComponent,
        FinancialReportComponent,
        CookManagerComponent,
        CookManagerAddEditComponent,
        EatObjectsComponent,
        TotalImportAddEditComponent,
        ListTotalImportComponent,
        // FinancialDashboardComponent,
        TotalExportAddEditComponent,
        ListTotalExportComponent,
        WarehouseReportComponent,
        LTTPReportComponent,
        MaintenanceToolManagementComponent,
        MaintenanceImportComponent,
        MaintenanceAddEditComponent,
        MaintenanceBeforeComponent,
        MaintenanceInventoryComponent,
        InventoryDetailsDialogComponent,
        MaintenancePureStoreComponent,
        PureAddEditComponent,
        KitchenOperationManagementComponent,
        BoilerStoveLogComponent,
        BoilerAddEditComponent,
        AccountInfoComponent,
        ReportResultProductComponent,
        TableReportResultComponent,
        StatisticalChiefComponent,
        SelfSufficientReportComponent,
        FoodPlanComponentComponent,
        FoodPlanAddEditComponentComponent,
        BasicQuantificationComponent,
    ],
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        JsonpModule,
        ModalModule.forRoot(),
        BsDatepickerModule.forRoot(),
        // MarkdownModule,

        AbpModule,
        AppRoutingModule,
        ServiceProxyModule,
        SharedModule,
        NgxPaginationModule,
        LayoutModule,
        CoreModule,
        LazyLoadImagesModule,
        OrderModule,
        MatChipsModule,
        MatNativeDateModule,
        MatTableModule,
        MatButtonModule,
        MatFormFieldModule,
        MatInputModule,
        MatIconModule,
        MatDatepickerModule,
        MatSelectModule,
        MatCheckboxModule,
        MatTabsModule,
        MatDialogModule,
        MatProgressSpinnerModule,
        MatSnackBarModule,
        MatPaginatorModule,
        MatCardModule,
        MatRadioModule,
        MatDividerModule,
        MatExpansionModule,
        MatProgressBarModule,
        MatMenuModule,
        MatSidenavModule,
        MatDatepickerModuleCustom,
        MatTooltipModule,
        MatAutocompleteModule,
        MatListModule,
        MatSlideToggleModule,

        // BrowserModule,
        // BrowserAnimationsModule,
        NgxChartsModule,
        NgxMatDatetimePickerModule,
        NgxMatTimepickerModule,
        NgxMatMomentModule,
        PagesModule,
    ],
    exports: [FormTableComponent],
    providers: [
        { provide: LocationStrategy, useClass: HashLocationStrategy },
        { provide: MAT_DIALOG_DATA, useValue: [] },
        CurrencyPipe,
    ],
    entryComponents: [
        AddTabDialogComponent,
        FieldDialogComponent,
        RenderFormDialogComponent,
        DynamicDialogComponent,
        QldkDialogComponent,
        SearchComponent,
        AddGroupDialogComponent,
        AddFieldDialogComponent,
        CategoryDialogComponent,
        FieldConfigDialogComponent,
        DeleteConfirmDialogComponent,
        MaterialDialogComponent,
        MaintenanceDialogComponent,
        DialogInventoryDetailComponent,
        InventoryDetailsDialogComponent,
        DialogRejectStatusComponent,
        DialogQuantitativeComponent,
        FoodListDialogComponent,
        DeleteConfirmAllDialogComponent,
        DialogEstimateFoodComponent,
        EaterInfoDialogComponent,
        DialogSwapUnitComponent,
        DialogMealRateComponent,
        DialogCookManagerComponent,
        DialogEatobjectComponent,
        DialogQuanntitiveRegulationComponent,
    ],
})
export class AppModule {
    constructor(public injector: Injector) {}
}
