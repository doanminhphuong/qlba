import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PlansFoodRoutingModule } from './plans-food-routing.module';
import { PlanSpendComponent } from './plan-spend/plan-spend.component';
import { PlanGuaranteeComponent } from './plan-guarantee/plan-guarantee.component';
import {
    MatDividerModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatOptionModule,
    MatPaginatorModule,
    MatProgressSpinnerModule,
    MatSelectModule,
    MatTableModule,
    MatTabsModule,
    MatTooltipModule,
} from '@angular/material';
import { SearchAdvancedComponent } from '@app/_shared/common/search-advanced/search-advanced.component';
import { LayoutModule } from '@app/_shared/layout.module';
import { PlanSpendAddEditComponent } from './plan-spend/plan-spend-add-edit/plan-spend-add-edit.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PriceConvertPipe } from '@core/pipes/price-convert.pipe';
import { PlanGuaranteeAddEditComponent } from './plan-guarantee/plan-guarantee-add-edit/plan-guarantee-add-edit.component';

@NgModule({
    declarations: [
        PlanSpendComponent,
        PlanGuaranteeComponent,
        PlanSpendAddEditComponent,
        PlanGuaranteeAddEditComponent,
    ],
    imports: [
        CommonModule,
        PlansFoodRoutingModule,
        MatPaginatorModule,
        LayoutModule,
        MatIconModule,
        MatDividerModule,
        MatProgressSpinnerModule,
        MatTableModule,
        MatTabsModule,
        MatExpansionModule,
        FormsModule,
        ReactiveFormsModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatOptionModule,
        MatTooltipModule,
    ],
    entryComponents: [SearchAdvancedComponent],
})
export class PlansFoodModule {}
