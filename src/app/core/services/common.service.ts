import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { AbstractControl, ValidatorFn } from '@angular/forms';
import { BehaviorSubject, defer, Observable } from 'rxjs';
import { TokenStorageService } from './token.service';
import { tap } from 'rxjs/operators';
import { CurrencyPipe } from '@angular/common';

@Injectable({
    providedIn: 'root',
})
export class CommonService {
    constructor(
        private http: HttpClient,
        private _tokenStorageService: TokenStorageService,
        private currencyPipe: CurrencyPipe,
    ) {}
    public state$ = new BehaviorSubject<'Error' | 'Success' | 'Loading'>(
        'Loading',
    );

    covertDataToKeyValueInSelect(
        arrayOrigin,
        newArray,
        keyData = 'id',
        valueData = 'name',
    ) {
        // newArray = [];
        return arrayOrigin.map((item) => {
            let dataPush = {
                key: item[keyData],
                value: item[valueData],
                name: item[valueData],
            };
            newArray.push(dataPush);
        });
    }

    getEnvironment() {
        return environment;
    }

    guid = () => {
        let s4 = () => {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        };
        //return id of format 'aaaaaaaa'-'aaaa'-'aaaa'-'aaaa'-'aaaaaaaaaaaa'
        return (
            s4() +
            s4() +
            '-' +
            s4() +
            '-' +
            s4() +
            '-' +
            s4() +
            '-' +
            s4() +
            s4() +
            s4()
        );
    };

    Uuidv4(): string {
        return ('' + [1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(
            /[018]/g,
            (ch) => {
                let c = Number(ch);
                return (
                    c ^
                    (crypto.getRandomValues(new Uint8Array(1))[0] &
                        (15 >> (c / 4)))
                ).toString(16);
            },
        );
    }

    callDataAPIShort(
        url = '/read/PhieuXuatKho/GetAll',
        data: any = {},
    ): Observable<any> {
        return this.http.post<any>(`${environment.SWAGGER_URL}${url}`, data);
    }

    callDataAPIFromTASPShort(
        url = '/api/services/Data/read/Setting/GetList',
        data: any = {},
    ): Observable<any> {
        return this.http.post<any>(`${environment.DATA_URL}${url}`, data);
    }

    // Cần phải thay đổi domain khi chạy chính thức
    checkPermission(data: any = {}): Observable<any> {
        return this.http.post<any>(
            `${environment.SYS_URL}/api/services/SYS/read/MenuClient/CheckPermissionAccess`,
            data,
        );
    }

    checkPermission2(): Observable<any> {
        let data = {
            id: localStorage.getItem('idMenu'),
            language: 'vi',
        };

        return this.http.post<any>(
            `${environment.SYS_URL}/api/services/SYS/read/MenuClient/CheckPermissionAccess`,
            data,
        );
    }

    noWhitespaceValidator(): ValidatorFn {
        return (control: AbstractControl): { [key: string]: any } => {
            let controlVal = control.value;
            if (typeof controlVal === 'number') {
                controlVal = `${controlVal}`;
            }
            let isWhitespace = (controlVal || '').trim().length === 0;
            let isValid = !isWhitespace;
            return isValid ? null : { whitespace: 'value is only whitespace' };
        };
    }

    observerLoadingState<T>(): (source: Observable<T>) => Observable<T> {
        return (source) => {
            return defer(() => {
                this.state$.next('Loading');
                return source.pipe(
                    tap({
                        next: () => this.state$.next('Success'),
                        error: () => this.state$.next('Error'),
                    }),
                );
            });
        };
    }

    getObserverLoadingState(): Observable<any> {
        return this.state$.asObservable();
    }

    convertToVND(price) {
        let currencyPipeString = this.currencyPipe.transform(
            price,
            'VND',
            'symbol',
            '1.0',
            'vi',
        );

        return currencyPipeString;
    }
}
