import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagementQuantityAddEditComponent } from './management-quantity-add-edit.component';

describe('ManagementQuantityAddEditComponent', () => {
  let component: ManagementQuantityAddEditComponent;
  let fixture: ComponentFixture<ManagementQuantityAddEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManagementQuantityAddEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagementQuantityAddEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
