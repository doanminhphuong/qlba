import { Observable } from 'rxjs';
import { Injectable, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { LogData } from '../interfaces/log-data';
import { UtilsService } from './utils.service';
import { tap } from 'rxjs/operators';
import { TokenStorageService } from './token.service';
import { environment } from 'environments/environment';

const TOKEN_KEY = 'auth-token';
const USER_KEY = 'auth-user';

@Injectable()
//   {
//   providedIn: 'root'
// }
export class SwapunitService {
    BASE_URL = `${environment.SWAGGER_URL}/api/services/`;
    ENDPOINT = 'read/QuyDoiDonVi/GetAll';
    ENDPOINT_FORM = 'read/Form/GetList';
    constructor(
        private http: HttpClient,
        private utils: UtilsService,
        private _tokenStorageService: TokenStorageService,
    ) {}

    ngOnInit(): void {}

    getAllSwapunit(data: any): Observable<any> {
        let token = this._tokenStorageService.getToken();
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
        });

        let options = { headers: headers };

        const END_POINT = 'read/QuyDoiDonVi/GetAll';

        return this.http.post<any>(
            `${this.BASE_URL}${END_POINT}`,
            data,
            options,
        );
    }

    createSwapunit(data: any): Observable<any> {
        let token = this._tokenStorageService.getToken();
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
        });

        let options = { headers: headers };

        const END_POINT = 'write/QuyDoiDonVi/Create';

        return this.http.post<any>(
            `${this.BASE_URL}${END_POINT}`,
            data,
            options,
        );
    }

    updateSwapunit(data: any): Observable<any> {
        let token = this._tokenStorageService.getToken();
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
        });

        let options = { headers: headers };

        const END_POINT = 'write/QuyDoiDonVi/Update';

        return this.http.post<any>(
            `${this.BASE_URL}${END_POINT}`,
            data,
            options,
        );
    }

    deleteSwapunit(id: any): Observable<any> {
        let token = this._tokenStorageService.getToken();
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
        });

        let options = { headers: headers };

        const END_POINT = 'write/QuyDoiDonVi/Delete';

        return this.http.post<any>(`${this.BASE_URL}${END_POINT}`, id, options);
    }

    getAllForm(data: any): Observable<any> {
        let token = this._tokenStorageService.getToken();
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
        });

        let options = { headers: headers };
        return this.http.post<any>(
            `${this.BASE_URL}${this.ENDPOINT_FORM}`,
            data,
            options,
        );
    }

    createForm(data: any): Observable<any> {
        let token = this._tokenStorageService.getToken();
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
        });
        let options = { headers: headers };
        const END_POINT = 'write/Form/Create';

        return this.http.post<any>(
            `${this.BASE_URL}${END_POINT}`,
            data,
            options,
        );
    }

    getFormByID(id: any): Observable<any> {
        let token = this._tokenStorageService.getToken();
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
        });
        let options = { headers: headers };
        const END_POINT = 'read/Form/Get';

        return this.http.post<any>(`${this.BASE_URL}${END_POINT}`, id, options);
    }

    updateForm(data: any): Observable<any> {
        let token = this._tokenStorageService.getToken();
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
        });

        let options = { headers: headers };

        const END_POINT = 'write/Form/Update';

        return this.http.post<any>(
            `${this.BASE_URL}${END_POINT}`,
            data,
            options,
        );
    }

    deleteForm(id: any): Observable<any> {
        let token = this._tokenStorageService.getToken();
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
        });
        let options = { headers: headers };
        const END_POINT = 'write/Form/Delete';

        return this.http.post<any>(`${this.BASE_URL}${END_POINT}`, id, options);
    }

    searchFieldFormName(data: any): Observable<any> {
        let token = this._tokenStorageService.getToken();
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
        });
        let options = { headers: headers };
        const END_POINT = 'read/Field/GetList';

        return this.http.post<any>(
            `${this.BASE_URL}${END_POINT}`,
            data,
            options,
        );
    }
}
