import { CommonModule } from '@angular/common';
import { NgModule, ModuleWithProviders } from '@angular/core';
import { AbpModule } from '@abp/abp.module';
import { RouterModule } from '@angular/router';

import { AppSessionService } from './session/app-session.service';
import { CoreModule } from '@core/core.module';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
    imports: [
        CommonModule,
        AbpModule,
        RouterModule,
        CoreModule,
        HttpClientModule,
    ],
    declarations: [],
    entryComponents: [],
    exports: [CoreModule],
})
export class SharedModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: SharedModule,
            providers: [AppSessionService],
        };
    }
}
