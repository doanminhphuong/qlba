import { Component, EventEmitter, Inject, OnInit } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { COMMA, ENTER } from "@angular/cdk/keycodes";
import { MatChipInputEvent } from "@angular/material";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { UserService } from "@core/services/user.service";

export interface Fruit {
  name: string;
}

@Component({
  selector: "app-field-dialog",
  templateUrl: "./field-dialog.component.html",
  styleUrls: ["./field-dialog.component.scss"]
})
export class FieldDialogComponent implements OnInit {
  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = true;
  confirmDelete: boolean = false;
  readonly separatorKeysCodes: number[] = [ENTER, COMMA];
  fieldForm: FormGroup;
  onSave = new EventEmitter();

  add(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    // Add our fruit
    if ((value || "").trim()) {
      this.data.fruits.push({ name: value.trim() });
    }

    // Reset the input value
    if (input) {
      input.value = "";
    }
  }

  remove(fruit: Fruit): void {
    const index = this.data.fruits.indexOf(fruit);

    if (index >= 0) {
      this.data.fruits.splice(index, 1);
    }
  }

  constructor(
    public dialogRef: MatDialogRef<FieldDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private fb: FormBuilder,
    private _userService: UserService
  ) {}

  ngOnInit() {
    // this.initForm();
    // console.log(this.data);
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  initForm() {
    this.fieldForm = this.fb.group({
      name: ["", Validators.required],
      code: ["", Validators.required],
      memoryCode: ["", Validators.required],
      type: ["", Validators.required],
      index: ["", Validators.required],
      status: ["", Validators.required],
      keyWord: ["", Validators.required],
      language: ["", Validators.required]
    });
  }

  submitForm() {
    let formValue = this.fieldForm.value;
    console.log("Submit form", formValue);
    console.log("Fruits", this.data.fruits);
  }

  deleteButonConfirm() {
    this.confirmDelete = !this.confirmDelete;
  }

  deleteAction(id: string) {
    this.onSave.emit({ type: "delete", data: id });
    // this.dialogRef.close();

    // this._userService.deleteDataField(id).subscribe((res) => {
    //   console.log(res);
    //   alert(`User ${res.name} với ${id} đã bị xóa`);
    //   this.dialogRef.close();
    // });
  }
  handleAction() {
    let formValue = this.data;
    // this.dialogRef.close();

    this.onSave.emit({ type: "addEdit", data: formValue });

    // this.dialogRef.close(id);
    // this._userService.deleteDataField(id).subscribe((res) => {
    //   console.log(res);
    //   alert(`User ${res.name} với ${id} đã bị xóa`);
    //   this.dialogRef.close();
    // });
  }
}
