import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TokenStorageService } from '@core/services/token.service';

@Component({
    selector: 'app-set-token',
    templateUrl: './set-token.component.html',
    styleUrls: ['./set-token.component.scss'],
})
export class SetTokenComponent implements OnInit {
    constructor(
        private tokenStorageService: TokenStorageService,
        private router: Router,
    ) {}

    ngOnInit() {
        let token = window.location.hash.substring(1);
        this.tokenStorageService.saveToken(token);

        // this.router.navigate(['/manage/app/category/trang-chu']);
        this.router.navigate(['/']);

        // if (this.tokenStorageService.getToken() === token) {
        //     this.router.navigate(['/manage/app/category/thuc-don']);
        // }
    }
}
