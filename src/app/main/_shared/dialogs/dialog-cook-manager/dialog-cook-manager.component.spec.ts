import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogCookManagerComponent } from './dialog-cook-manager.component';

describe('DialogCookManagerComponent', () => {
  let component: DialogCookManagerComponent;
  let fixture: ComponentFixture<DialogCookManagerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogCookManagerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogCookManagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
