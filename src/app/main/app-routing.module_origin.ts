import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CanHomepageCallAPIGuard } from '@shared/auth/can-homepage-call-api-guard';
import { CanHomepageGuard } from '@shared/auth/can-homepage-guard';
import { CanLoginpageGuard } from '@shared/auth/can-loginpage-guard';
import { AppComponent } from './app.component';
import { LoginComponent } from './auth/login/login.component';
import { SetTokenComponent } from './auth/set-token/set-token.component';
import { DynamicRouterComponent } from './components/dynamic-router-component/dynamic-router-component.component';
import { FormPageComponent } from './components/form-page/form-page.component';
import { FoodFuelGroupComponent } from './pages/food-fuel-group/food-fuel-group.component';
import { FoodFuelManagementComponent } from './pages/food-fuel-management/food-fuel-management.component';
import { InventoryBeforeManagementComponent } from './pages/inventory-before-management/inventory-before-management.component';
import { ImportAddEditComponent } from './pages/inventory-import/import-add-edit/import-add-edit.component';
import { InventoryManagementComponent } from './pages/inventory-management/inventory-management.component';
import { InventoryImportComponent } from './pages/inventory-import/inventory-import.component';
import { AdjustmentAddEditComponent } from './pages/price-management/adjustment-add-edit/adjustment-add-edit.component';
import { PriceManagementComponent } from './pages/price-management/price-management.component';
import { PurchaseAddEditComponent } from './pages/purchase-management/purchase-add-edit/purchase-add-edit.component';
import { PurchaseManagementComponent } from './pages/purchase-management/purchase-management.component';
import { WarehouseManagementComponent } from './pages/warehouse-management/warehouse-management.component';
import { BlankComponent } from './components/blank/blank.component';
import { ExportWarehouseComponent } from './pages/export-warehouse/export-warehouse.component';
import { ExportWarehouseAddEditComponent } from './pages/export-warehouse/export-warehouse-add-edit/export-warehouse-add-edit.component';
import { WarehouseTransferComponent } from './pages/warehouse-transfer/warehouse-transfer.component';
import { FoodyManagementComponent } from './pages/foody-management/foody-management.component';
import { FoodyAddEditComponent } from './pages/foody-management/foody-add-edit/foody-add-edit.component';
import { CircularsManagementComponent } from './pages/circulars-management/circulars-management.component';
import { ManagementQuantityComponent } from './pages/management-quantity/management-quantity.component';
import { ManagementQuantityAddEditComponent } from './pages/management-quantity/management-quantity-add-edit/management-quantity-add-edit.component';
import { MenuListManagementComponent } from './pages/menu-list-management/menu-list-management.component';
import { MenuListAddEditComponent } from './pages/menu-list-management/menu-list-add-edit/menu-list-add-edit.component';
import { MarkRiceComponent } from './pages/mark-rice/mark-rice.component';
import { ListOfFoodPlansComponent } from './pages/list-of-food-plans/list-of-food-plans.component';
import { ListOfFoodPlansAddEditComponent } from './pages/list-of-food-plans/list-of-food-plans-add-edit/list-of-food-plans-add-edit.component';
import { MarkRiceDetailsComponent } from './pages/mark-rice/mark-rice-details/mark-rice-details.component';
import { EaterManagementComponent } from './pages/eater-management/eater-management.component';
import { EaterAddEditComponent } from './pages/eater-management/eater-add-edit/eater-add-edit.component';
import { PureStoreManagementComponent } from './pages/pure-store-management/pure-store-management.component';
import { AddPureStoreComponent } from './pages/pure-store-management/add-pure-store/add-pure-store.component';
import { DocumentPageComponent } from './pages/document-page/document-page.component';
import { SwapunitComponent } from './pages/swapunit/swapunit.component';
import { FinancialReportComponent } from './pages/financial-report/financial-report.component';
import { CookManagerComponent } from './pages/cook-manager/cook-manager.component';
import { CookManagerAddEditComponent } from './pages/cook-manager/cook-manager-add-edit/cook-manager-add-edit.component';
import { EatObjectsComponent } from './pages/eat-objects/eat-objects.component';
import { CanAccessRouterGuard } from '@shared/auth/can-access-router-guard';
import { ListTotalImportComponent } from './pages/total-import/list-total-import/list-total-import.component';
import { TotalImportAddEditComponent } from './pages/total-import/total-import-add-edit/total-import-add-edit.component';
// import { FinancialDashboardComponent } from './pages/financial-report/financial-dashboard/financial-dashboard.component';
import { ListTotalExportComponent } from './pages/total-export/list-total-export/list-total-export.component';
import { TotalExportAddEditComponent } from './pages/total-export/total-export-add-edit/total-export-add-edit.component';
import { MaintenanceToolManagementComponent } from './pages/maintenance-tool-management/maintenance-tool-management.component';
import { MaintenanceImportComponent } from './pages/maintenance-import/maintenance-import.component';
import { MaintenanceAddEditComponent } from './pages/maintenance-import/maintenance-add-edit/maintenance-add-edit.component';
import { WarehouseReportComponent } from './pages/warehouse-report/warehouse-report.component';
import { LTTPReportComponent } from './pages/lttp-report/lttp-report.component';
import { MaintenanceBeforeComponent } from './pages/maintenance-before/maintenance-before.component';
import { MaintenanceInventoryComponent } from './pages/maintenance-inventory/maintenance-inventory.component';
import { MaintenancePureStoreComponent } from './pages/maintenance-pure-store/maintenance-pure-store.component';
import { PureAddEditComponent } from './pages/maintenance-pure-store/pure-add-edit/pure-add-edit.component';
import { KitchenOperationManagementComponent } from './pages/kitchen-operation-management/kitchen-operation-management.component';
import { BoilerStoveLogComponent } from './pages/kitchen-operation-management/boiler-stove-log/boiler-stove-log.component';
import { BoilerAddEditComponent } from './pages/kitchen-operation-management/boiler-stove-log/boiler-add-edit/boiler-add-edit.component';
import { AccountInfoComponent } from './pages/account-info/account-info.component';
import { ReportResultProductComponent } from './pages/report-result-product/report-result-product.component';
import { SelfSufficientReportComponent } from './pages/self-sufficient-report/self-sufficient-report.component';
import { StatisticalChiefComponent } from './pages/statisticals/chief/statistical-chief.component';

const routes: Routes = [
    {
        path: '',
        component: AppComponent,
        canActivate: [CanHomepageGuard, CanHomepageCallAPIGuard],
        children: [
            //Dynamic
            {
                path: 'manager/:valueData',
                component: DynamicRouterComponent,
                data: {
                    title: 'CMS- Quản lý bếp ăn',
                },
                canActivate: [CanAccessRouterGuard],
            },
            //Testing router
            {
                path: 'manage/app/category/chuyen-kho',
                component: WarehouseTransferComponent,
                data: {
                    title: 'Chuyển kho',
                },
                canActivate: [CanAccessRouterGuard],
            },
            {
                path: 'manage/app/category/danh-sach-kho',
                component: WarehouseManagementComponent,
                data: {
                    title: 'Danh sách Kho',
                },
                canActivate: [CanAccessRouterGuard],
            },
            //Xuất kho
            {
                path: 'manage/app/category/xuat-kho',
                component: ExportWarehouseComponent,
                data: {
                    title: 'Danh sách phiếu xuất kho',
                },
                canActivate: [CanAccessRouterGuard],
            },
            {
                path: 'manage/app/category/xuat-kho/create',
                component: ExportWarehouseAddEditComponent,
                data: {
                    title: 'Tạo phiếu xuất kho',
                },
                canActivate: [CanAccessRouterGuard],
            },
            {
                path: 'manage/app/category/xuat-kho/edit/:id',
                component: ExportWarehouseAddEditComponent,
                data: {
                    title: 'Chỉnh sửa phiếu xuất kho',
                },
                canActivate: [CanAccessRouterGuard],
            },
            // Kế hoạch bảo đảm thưc phẩm
            {
                path: 'manage/app/category/ke-hoach-LTTP-chat-dot',
                component: ListOfFoodPlansComponent,
                data: {
                    title: 'Kế hoạch LTTP Chất Đốt',
                },
                canActivate: [CanAccessRouterGuard],
            },
            {
                path: 'manage/app/category/ke-hoach-LTTP-chat-dot/create',
                component: ListOfFoodPlansAddEditComponent,
                data: {
                    title: 'Tạo kế hoạch LTTP Chất Đốt',
                },
                canActivate: [CanAccessRouterGuard],
            },
            {
                path: 'manage/app/category/ke-hoach-LTTP-chat-dot/edit/:id',
                component: ListOfFoodPlansAddEditComponent,
                data: {
                    title: 'Chỉnh sửa kế hoạch LTTP Chất Đốt',
                },
                canActivate: [CanAccessRouterGuard],
            },

            // Quản lý tiêu chuẩn, định lượng
            {
                path: 'manage/app/category/quan-ly-tieu-chuan-dinh-luong',
                component: ManagementQuantityComponent,
                data: {
                    title: 'Quản lý tiêu chuẩn, định lượng',
                },
                canActivate: [CanAccessRouterGuard],
            },
            {
                path: 'manage/app/category/quan-ly-tieu-chuan-dinh-luong/create',
                component: ManagementQuantityAddEditComponent,
                data: {
                    title: 'Tạo quản lý tiêu chuẩn, định lượng',
                },
                canActivate: [CanAccessRouterGuard],
            },
            {
                path: 'manage/app/category/quan-ly-tieu-chuan-dinh-luong/edit/:id',
                component: ManagementQuantityAddEditComponent,
                data: {
                    title: 'Chỉnh sửa quản lý tiêu chuẩn, định lượng',
                },
                canActivate: [CanAccessRouterGuard],
            },

            // Tồn kho đầu kỳ
            {
                path: 'manage/app/category/ton-kho-dau-ky',
                component: InventoryBeforeManagementComponent,
                data: {
                    title: 'Tồn kho đầu kỳ',
                },
                canActivate: [CanAccessRouterGuard],
            },
            {
                path: 'manage/app/category/ton-kho-dau-ky/:id',
                component: InventoryBeforeManagementComponent,
                data: {
                    title: 'Tồn kho đầu kỳ',
                },
                canActivate: [CanAccessRouterGuard],
            },

            // Tịnh kho
            {
                path: 'manage/app/category/bien-ban-tinh-kho',
                component: PureStoreManagementComponent,
                data: {
                    title: 'Biên bản tịnh kho',
                },
                canActivate: [CanAccessRouterGuard],
            },
            {
                path: 'manage/app/category/bien-ban-tinh-kho/add',
                component: AddPureStoreComponent,
                data: {
                    title: 'Thêm biên bản tịnh kho',
                },
                canActivate: [CanAccessRouterGuard],
            },
            {
                path: 'manage/app/category/bien-ban-tinh-kho/detail/:id',
                component: AddPureStoreComponent,
                data: {
                    title: 'Chỉnh sửa biên bản tịnh kho',
                },
                canActivate: [CanAccessRouterGuard],
            },

            // Tồn kho
            {
                path: 'manage/app/category/ton-kho/:id',
                component: InventoryManagementComponent,
                data: {
                    title: 'Tồn kho',
                },
                canActivate: [CanAccessRouterGuard],
            },

            // Báo Cáo Tồn kho
            {
                path: 'manage/app/category/bao-cao-ton-kho',
                component: WarehouseReportComponent,
                data: {
                    title: 'Báo Cáo Tồn kho',
                },
            },

            // Phiếu nhập kho
            {
                path: 'manage/app/category/nhap-kho',
                component: InventoryImportComponent,
                data: {
                    title: 'Phiếu nhập kho',
                },
                canActivate: [CanAccessRouterGuard],
            },
            {
                path: 'manage/app/category/nhap-kho/create',
                component: ImportAddEditComponent,
                data: {
                    title: 'Tạo phiếu nhập kho ',
                },
                canActivate: [CanAccessRouterGuard],
            },
            {
                path: 'manage/app/category/nhap-kho/edit/:id',
                component: ImportAddEditComponent,
                data: {
                    title: 'Chỉnh sửa Phiếu nhập kho ',
                },
                canActivate: [CanAccessRouterGuard],
            },
            // Danh sách mua hàng
            {
                path: 'manage/app/category/bang-ke-mua-hang',
                component: PurchaseManagementComponent,
                data: {
                    title: 'Phiếu mua hàng',
                },
                canActivate: [CanAccessRouterGuard],
            },
            {
                path: 'manage/app/category/bang-ke-mua-hang/create',
                component: PurchaseAddEditComponent,
                data: {
                    title: 'Tạo Phiếu mua hàng',
                },
                canActivate: [CanAccessRouterGuard],
            },
            {
                path: 'manage/app/category/bang-ke-mua-hang/edit/:id',
                component: PurchaseAddEditComponent,
                data: {
                    title: 'Chỉnh sửa Phiếu mua hàng',
                },
                canActivate: [CanAccessRouterGuard],
            },

            // Hội đồng giá
            {
                path: 'manage/app/category/gia-hoi-dong-gia',
                component: PriceManagementComponent,
                data: {
                    title: 'Hội đồng giá ',
                },
                canActivate: [CanAccessRouterGuard],
            },
            {
                path: 'manage/app/category/gia-hoi-dong-gia/create',
                component: AdjustmentAddEditComponent,
                data: {
                    title: 'Tạo hội đồng giá ',
                },
                canActivate: [CanAccessRouterGuard],
            },
            {
                path: 'manage/app/category/gia-hoi-dong-gia/edit/:id',
                component: AdjustmentAddEditComponent,
                data: {
                    title: 'Chỉnh sửa hội đồng giá ',
                },
                canActivate: [CanAccessRouterGuard],
            },

            // Lương thực thực phẩm, chất đốt
            {
                path: 'manage/app/category/luong-thuc-thuc-pham-chat-dot',
                component: FoodFuelManagementComponent,
                data: {
                    title: 'Lương thực thực phẩm, chất đốt ',
                },
                canActivate: [CanAccessRouterGuard],
            },
            {
                path: 'manage/app/category/nhom-lttp-chat-dot',
                component: FoodFuelGroupComponent,
                data: {
                    title: 'Lương thực thực phẩm, chất đốt',
                },
                canActivate: [CanAccessRouterGuard],
            },

            // Món ăn
            {
                path: 'manage/app/category/quan-ly-mon-an',
                component: FoodyManagementComponent,
                data: {
                    title: 'Quản lý món ăn',
                },
                canActivate: [CanAccessRouterGuard],
            },
            {
                path: 'manage/app/category/quan-ly-mon-an/create',
                component: FoodyAddEditComponent,
                data: {
                    title: 'Quản lý món ăn',
                },
                canActivate: [CanAccessRouterGuard],
            },
            {
                path: 'manage/app/category/quan-ly-mon-an/edit/:id',
                component: FoodyAddEditComponent,
                data: {
                    title: 'Quản lý món ăn',
                },
                canActivate: [CanAccessRouterGuard],
            },

            // Chấm cơm
            {
                path: 'manage/app/category/quan-ly-cham-com',
                component: MarkRiceComponent,
                data: {
                    title: 'Chấm cơm',
                },
                canActivate: [CanAccessRouterGuard],
            },
            {
                path: 'manage/app/category/quan-ly-cham-com/:id',
                component: MarkRiceDetailsComponent,
                data: {
                    title: 'Chấm cơm',
                },
                canActivate: [CanAccessRouterGuard],
            },

            // Quân số ăn
            {
                path: 'manage/app/category/quan-ly-quan-so-an',
                component: EaterManagementComponent,
                data: {
                    title: 'Quân số ăn',
                },
                canActivate: [CanAccessRouterGuard],
            },

            {
                path: 'manage/app/category/quan-ly-quan-so-an/create',
                component: EaterAddEditComponent,
                data: {
                    title: 'Quân số ăn',
                },
                canActivate: [CanAccessRouterGuard],
            },

            {
                path: 'manage/app/category/quan-ly-quan-so-an/edit/:id',
                component: EaterAddEditComponent,
                data: {
                    title: 'Quân số ăn',
                },
                canActivate: [CanAccessRouterGuard],
            },

            // Thông tư
            {
                path: 'manage/app/category/thong-tu',
                component: CircularsManagementComponent,
                data: {
                    title: 'Thông tư',
                },
                canActivate: [CanAccessRouterGuard],
            },

            // Danh sách thực đơn
            {
                path: 'manage/app/category/danh-sach-thuc-don',
                component: MenuListManagementComponent,
                data: {
                    title: 'Danh sách thực đơn',
                },
                canActivate: [CanAccessRouterGuard],
            },
            {
                path: 'manage/app/category/danh-sach-thuc-don/create',
                component: MenuListAddEditComponent,
                data: {
                    title: 'Danh sách thực đơn',
                },
                canActivate: [CanAccessRouterGuard],
            },
            {
                path: 'manage/app/category/danh-sach-thuc-don/edit/:id',
                component: MenuListAddEditComponent,
                data: {
                    title: 'Danh sách thực đơn',
                },
                canActivate: [CanAccessRouterGuard],
            },

            // Báo Cáo Tài Chính
            {
                path: 'manage/app/category/trang-chu',
                // component: FinancialDashboardComponent,
                data: {
                    title: 'Trang chủ',
                },
                canActivate: [CanAccessRouterGuard],
            },
            {
                path: 'manage/app/category/thong-ke-tong-quan',
                component: StatisticalChiefComponent,
                data: {
                    title: 'Thống kê',
                },
                canActivate: [CanAccessRouterGuard],
            },

            // Báo Cáo Tài Chính FinancialDashboardComponent
            {
                path: 'manage/app/category/xuat-nhap-lttp',
                component: FinancialReportComponent,
                data: {
                    title: 'Xuất nhập lương thực, thực phẩm',
                },
                canActivate: [CanAccessRouterGuard],
            },

            // Báo Cáo Sử Dụng LTTP, Chất Đốt
            {
                path: 'manage/app/category/bao-cao-su-dung-lttp',
                component: LTTPReportComponent,
                data: {
                    title: 'Báo Cáo Sử Dụng LTTP, Chất Đốt',
                },
            },
            // Đảm bảo định lượng LTTP, Chất đốt
            {
                path: 'manage/app/category/bao-cao-ke-hoach-su-dung-lttp',
                component: ReportResultProductComponent,
                data: {
                    title: 'Báo Cáo Đảm bảo định lượng LTTP, Chất đốt',
                },
            },

            // Báo Cáo Sử Dụng LTTP, Chất Đốt
            {
                path: 'manage/app/category/ket-qua-tu-tuc-thuc-pham-thang',
                component: SelfSufficientReportComponent,
                data: {
                    title: 'Báo cáo kết quả tự túc thực phẩm',
                },
            },

            // Quản lý bếp ăn
            {
                path: 'manage/app/category/ql-bepan',
                component: CookManagerComponent,
                data: {
                    title: 'Quản lý bếp ăn',
                },
                canActivate: [CanAccessRouterGuard],
            },
            {
                path: 'manage/app/category/ql-bepan/create',
                component: CookManagerAddEditComponent,
                data: {
                    title: 'Thêm bếp ăn',
                },
                canActivate: [CanAccessRouterGuard],
            },
            {
                path: 'manage/app/category/ql-bepan/edit/:id',
                component: CookManagerAddEditComponent,
                data: {
                    title: 'Sửa bếp ăn',
                },
                canActivate: [CanAccessRouterGuard],
            },

            {
                path: 'manager/sys-manager-forms/details/:id',
                component: FormPageComponent,
                data: {
                    title: 'CMS- Quản lý bếp ăn',
                },
                canActivate: [CanAccessRouterGuard],
            },
            {
                path: 'versions',
                component: DocumentPageComponent,
                data: {
                    title: 'Trang tài liệu',
                },
            },

            // SwapUnit
            {
                path: 'manage/app/category/quy-doi-don-vi-tinh',
                component: SwapunitComponent,
                data: {
                    title: 'CMS- Quản lý bếp ăn',
                },
                canActivate: [CanAccessRouterGuard],
            },

            {
                path: 'manage/app/category/doi-tuong-an',
                component: EatObjectsComponent,
                data: {
                    title: 'Đối tượng ăn',
                },
                canActivate: [CanAccessRouterGuard],
            },

            // Tổng nhập kho trong ngày
            {
                path: 'manage/app/category/tong-nhap-kho',
                component: ListTotalImportComponent,
                data: {
                    title: 'Tổng nhập trong ngày',
                },
                canActivate: [CanAccessRouterGuard],
            },

            {
                path: 'manage/app/category/tong-nhap-kho/create',
                component: TotalImportAddEditComponent,
                data: {
                    title: 'Tổng nhập trong ngày',
                },
                canActivate: [CanAccessRouterGuard],
            },

            {
                path: 'manage/app/category/tong-nhap-kho/detail/:id',
                component: TotalImportAddEditComponent,
                data: {
                    title: 'Tổng nhập trong ngày',
                },
                canActivate: [CanAccessRouterGuard],
            },

            // Tổng xuất kho trong ngày
            {
                path: 'manage/app/category/tong-xuat-kho',
                component: ListTotalExportComponent,
                data: {
                    title: 'Tổng xuất trong ngày',
                },
                canActivate: [CanAccessRouterGuard],
            },

            {
                path: 'manage/app/category/tong-xuat-kho/create',
                component: TotalExportAddEditComponent,
                data: {
                    title: 'Tổng xuất trong ngày',
                },
                canActivate: [CanAccessRouterGuard],
            },

            {
                path: 'manage/app/category/tong-xuat-kho/detail/:id',
                component: TotalExportAddEditComponent,
                data: {
                    title: 'Tổng xuất trong ngày',
                },
                canActivate: [CanAccessRouterGuard],
            },

            // Vận hành bếp
            {
                path: 'manage/app/category/nhat-ky-van-hanh-bep',
                component: KitchenOperationManagementComponent,
                data: {
                    title: 'Vận hành bếp',
                },
            },

            {
                path: 'manage/app/category/nhat-ky-van-hanh-bep/:bepId',
                component: BoilerStoveLogComponent,
                data: {
                    title: 'Vận hành bếp',
                },
            },

            {
                path: 'manage/app/category/nhat-ky-van-hanh-bep/:bepId/create',
                component: BoilerAddEditComponent,
                data: {
                    title: 'Vận hành bếp',
                },
            },

            {
                path: 'manage/app/category/nhat-ky-van-hanh-bep/:bepId/edit',
                component: BoilerAddEditComponent,
                data: {
                    title: 'Vận hành bếp',
                },
            },

            // Dụng cụ cấp dưỡng
            {
                path: 'manage/app/category/ds-dung-cu-cap-duong',
                component: MaintenanceToolManagementComponent,
                data: {
                    title: 'Dụng cụ cấp dưỡng',
                },
            },

            {
                path: 'manage/app/category/kho-dung-cu-cap-duong',
                component: MaintenanceInventoryComponent,
                data: {
                    title: 'Tồn kho DCCD',
                },
            },

            {
                path: 'manage/app/category/ton-kho-dccd-dau-ky',
                component: MaintenanceBeforeComponent,
                data: {
                    title: 'Tồn kho đầu kỳ DCCD',
                },
            },

            {
                path: 'manage/app/category/ds-pnk-dccd',
                component: MaintenanceImportComponent,
                data: {
                    title: 'Nhập kho DCCD',
                },
            },

            {
                path: 'manage/app/category/ds-pnk-dccd/create',
                component: MaintenanceAddEditComponent,
                data: {
                    title: 'Nhập kho DCCD',
                },
            },

            {
                path: 'manage/app/category/ds-pnk-dccd/edit/:id',
                component: MaintenanceAddEditComponent,
                data: {
                    title: 'Nhập kho DCCD',
                },
            },

            {
                path: 'manage/app/category/tinh-kho-dccd',
                component: MaintenancePureStoreComponent,
                data: {
                    title: 'Tịnh kho DCCD',
                },
            },

            {
                path: 'manage/app/category/tinh-kho-dccd/create',
                component: PureAddEditComponent,
                data: {
                    title: 'Tịnh kho DCCD',
                },
            },

            {
                path: 'manage/app/category/tinh-kho-dccd/detail/:id',
                component: PureAddEditComponent,
                data: {
                    title: 'Tịnh kho DCCD',
                },
            },

            // Thông tin tài khoản
            {
                path: 'manage/app/category/thong-tin-tai-khoan',
                component: AccountInfoComponent,
                data: {
                    title: 'Thông tin tài khoản',
                },
            },

            {
                path: 'management',
                // loadChildren:
                //   "app/main/managements/managements.module#ManagementsModule", //Lazy load account module
                data: { preload: true },

                // canActivate: [AppRouteGuard],
            },
        ],
        data: {
            title: 'AppComponent ',
        },
    },
    {
        path: 'login',
        component: LoginComponent,
        canActivate: [CanLoginpageGuard],
    },
    {
        path: 'set-token',
        component: SetTokenComponent,
        canActivate: [CanLoginpageGuard],
    },
    {
        path: '',
        redirectTo: '/manage/app/category/trang-chu',
        pathMatch: 'full',
        data: {
            title: 'category ',
        },
    },

    {
        path: '**',
        redirectTo: 'manage/app/category/trang-chu',
    },
    // { path: "**", redirectTo: "manager" }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class AppRoutingModule {}
