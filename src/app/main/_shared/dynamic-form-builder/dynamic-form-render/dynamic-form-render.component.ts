import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
    selector: 'dynamic-form-render',
    templateUrl: './dynamic-form-render.component.html',
    styleUrls: ['./dynamic-form-render.component.scss'],
})
export class DynamicFormRenderComponent implements OnInit {
    @Input() form: FormGroup;
    @Input() fields: any[] = [];
    @Input() data: any;
    @Input() groupCode: string;
    @Input() tabCtrl: any;
    @Input() groupCtrl: any;
    @Input() isFlatForm: boolean;

    isEnableDelete: boolean = false;

    fieldList: any[] = [];

    constructor() { }

    ngOnInit() {
        this.fieldList = this.fields.sort((a, b) => a.number10 - b.number10);
    }
}
