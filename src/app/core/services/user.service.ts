import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { delay, tap } from 'rxjs/operators';
import { MessageData } from '../interfaces/message-data';
import { TokenStorageService } from './token.service';
import { BASE_URL } from './data-service';
import { environment } from 'environments/environment';

export type DataSearch = {
    code: string;
    valueData: string;
};

@Injectable()
export class UserService {
    API_URL: any = 'https://62977bb114e756fe3b313562.mockapi.io';
    API_TAB_URL: any = 'https://62a1567acd2e8da9b0ef52b5.mockapi.io/tab/';
    API_FIELD_URL: any =
        'https://62977bb114e756fe3b313562.mockapi.io/qlsk_field/';
    API_ENDPOINT: any = '/SKQN';

    BASE_URL_SSO: any = `${environment.SSO_URL}/api/services/sso/`;

    constructor(
        private http: HttpClient,
        private _tokenStorageService: TokenStorageService,
    ) { }

    getDataSearch(data: DataSearch): Observable<any> {
        let token = this._tokenStorageService.getToken();
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
        });

        let options = { headers: headers };

        const END_POINT = '/read/Data/GetData';

        return this.http.post<any>(`${BASE_URL}${END_POINT}`, data, options);
    }

    getCurrentUsers(): Observable<any> {
        let token = this._tokenStorageService.getToken();
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
        });

        let options = { headers: headers };

        const END_POINT = 'read/User/GetCurrentUser';

        return this.http.post<any>(
            `${this.BASE_URL_SSO}${END_POINT}`,
            null,
            options,
        );
    }

    getUsers(data: any): Observable<any> {
        let token = this._tokenStorageService.getToken();
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
        });

        let options = { headers: headers };

        const END_POINT = 'read/User/GetUsers';

        return this.http.post<any>(
            `${this.BASE_URL_SSO}${END_POINT}`,
            data,
            options,
        );
    }

    getOrganization(data: any): Observable<any> {
        let token = this._tokenStorageService.getToken();
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
        });

        let options = { headers: headers };

        const END_POINT = 'read/Organization/GetAll';

        return this.http.post<any>(
            `${this.BASE_URL_SSO}${END_POINT}`,
            data,
            options,
        );
    }

    getPositions(data: any): Observable<any> {
        let token = this._tokenStorageService.getToken();
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
        });

        let options = { headers: headers };

        const END_POINT = 'read/Title/GetList';

        return this.http.post<any>(
            `${this.BASE_URL_SSO}${END_POINT}`,
            data,
            options,
        );
    }

    getUserOrganization(): Observable<any> {
        let token = this._tokenStorageService.getToken();
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
        });

        let options = { headers: headers };

        const END_POINT = 'read/Organization/GetCurrentOrganizations';

        return this.http.post<any>(
            `${this.BASE_URL_SSO}${END_POINT}`,
            null,
            options,
        );
    }

    getPageUsers(data: any): Observable<any> {
        let token = this._tokenStorageService.getToken();
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
        });

        let options = { headers: headers };

        const END_POINT = 'read/User/GetPageUsers';

        return this.http.post<any>(
            `${this.BASE_URL_SSO}${END_POINT}`,
            data,
            options,
        );
    }

    getChildOrganization(data: any): Observable<any> {
        let token = this._tokenStorageService.getToken();
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
        });

        let options = { headers: headers };

        const END_POINT = 'read/SSO/GetAllChildrenOrganizations';

        return this.http.post<any>(
            `${this.BASE_URL_SSO}${END_POINT}`,
            data,
            options,
        );
    }

    public getData(): Observable<any> {
        return this.http.get(this.API_URL + this.API_ENDPOINT);
    }

    public getDataTab(): Observable<any> {
        return this.http.get(this.API_TAB_URL);
    }

    public createPost(tab: any): Observable<any> {
        return this.http.post(this.API_TAB_URL, tab, {
            reportProgress: true,
            observe: 'events',
        });
    }

    public getDataGroupById(id): Observable<any> {
        return this.http.get(this.API_TAB_URL + id + '/group');
    }

    public deleteTab(id: string): Observable<any> {
        // console.log(this.API_TAB_URL + id)
        return this.http.delete(this.API_TAB_URL + id);
    }

    public getDataField(): Observable<any> {
        return this.http.get(this.API_FIELD_URL);
    }

    public getDataFieldFromID(id: number): Observable<any> {
        return this.http.get(this.API_FIELD_URL + id);
    }

    public createDataField(data: any): Observable<any> {
        return this.http.post(this.API_FIELD_URL, data, {
            reportProgress: true,
            observe: 'events',
        });
    }

    public updateDataField(id: number, data: any): Observable<any> {
        const url = `${this.API_FIELD_URL}/${id}`;

        return this.http
            .put(url, data, {
                reportProgress: true,
                observe: 'events',
            })
            .pipe(delay(300));
    }

    public deleteDataField(id: string): Observable<any> {
        // console.log(this.API_TAB_URL + id)
        return this.http.delete(this.API_FIELD_URL + id);
    }
}
