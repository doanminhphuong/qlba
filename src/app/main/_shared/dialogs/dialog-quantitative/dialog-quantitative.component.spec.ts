import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogQuantitativeComponent } from './dialog-quantitative.component';

describe('DialogQuantitativeComponent', () => {
  let component: DialogQuantitativeComponent;
  let fixture: ComponentFixture<DialogQuantitativeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogQuantitativeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogQuantitativeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
