import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule, CurrencyPipe } from '@angular/common';

import { MenuConfigService } from './services/menu-config.service';
import { UtilsService } from './services/utils.service';
import { LogsService } from './services/logs.service';
import { TranslationService } from './services/translation.service';
import { UserService } from './services/user.service';
import { MenuService } from './services/menu.service';
import { TokenStorageService } from './services/token.service';
import { AppAuthService } from './services/auth.service';
import { CategoryService } from './services/category.service';
import { FieldService } from './services/field.service';
import { QLDKService } from './services/qldk.service';
import { HoSoSucKhoeService } from './services/ho-so-suc-khoe.service';
import { FoodFuelService } from './services/food-fuel.service';
import { PriceService } from './services/price.service';
import { InventoryService } from './services/inventory.service';
import { PhieuXuatKhoService } from './services/phieu-xuat-kho.service';
import { FoodyService } from './services/foody.service';
import { CircularService } from './services/circular-service';
import { CommonService } from './services/common.service';
import { MenuListService } from './services/menu-list.service';
import { ExportExcelService } from './services/export-excel.service';
import { SwapunitService } from './services/swapunit.service';
import { ConvertNumToStrPipe } from '@app/pages/purchase-management/purchase-add-edit/convert-num-to-str.pipe';
import { QuantityService } from './services/quantity.service';
import { EatobjectsService } from './services/eatobjects.service';

@NgModule({
    imports: [CommonModule],
    declarations: [],
    exports: [],
    providers: [CurrencyPipe],
})
export class CoreModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: CoreModule,
            providers: [
                // template services
                MenuConfigService,
                UtilsService,
                LogsService,
                TranslationService,
                UserService,
                MenuService,
                TokenStorageService,
                AppAuthService,
                CategoryService,
                FieldService,
                QLDKService,
                HoSoSucKhoeService,
                FoodFuelService,
                PriceService,
                InventoryService,
                PhieuXuatKhoService,
                FoodyService,
                CircularService,
                CommonService,
                MenuListService,
                ExportExcelService,
                SwapunitService,
                ConvertNumToStrPipe,
                QuantityService,
                EatobjectsService,
            ],
        };
    }
}
