import { SelectionModel } from '@angular/cdk/collections';
import { Component, Input, OnInit } from '@angular/core';

const COLUMNS_SCHEMA = [
  {
    label: '',
    key: 'isSelected',
    type: 'isSelected',
  },
  {
    label: 'Mã bệnh',
    key: "code",
    type: "text",
  },
  {
    label: 'Tên bệnh',
    key: "name",
    type: "text",
  },
  {
    label: 'Mức độ',
    key: "level",
    type: "text",
  },
  {
    label: 'Số liệu',
    key: "quantity",
    type: "number",
  },
  {
    label: 'Hành động',
    key: "isEdit",
    type: "isEdit",
  }
]

@Component({
  selector: 'app-form-table',
  templateUrl: './form-table.component.html',
  styleUrls: ['./form-table.component.scss']
})
export class FormTableComponent implements OnInit {
  @Input() data: any;

  columnsSchema: any[] = [];
  displayedColumns: string[] = [];

  dataSource = [];
  selection = new SelectionModel<any>(true, []);

  isSelected: boolean = false;
  isAddRow: boolean = false;
  isDisableAdd: boolean = false;

  constructor() { }

  ngOnInit() {
    this.dataSource = JSON.parse(localStorage.getItem('dataSource')) || this.data.items;

    this.columnsSchema = [
      { label: '', key: 'isSelected', type: 'isSelected' },
      ...JSON.parse(this.data.header),
      { label: 'Hành động', key: 'isEdit', type: 'isEdit' },
    ];

    this.displayedColumns = this.columnsSchema.map(col => col.key);

    console.log("ColumnsSchema: ", this.columnsSchema);
    console.log("displayedColumns: ", this.displayedColumns);
  }

  guid = () => {
    let s4 = () => {
      return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
    };
    //return id of format 'aaaaaaaa'-'aaaa'-'aaaa'-'aaaa'-'aaaaaaaaaaaa'
    return (
      s4() +
      s4() +
      '-' +
      s4() +
      '-' +
      s4() +
      '-' +
      s4() +
      '-' +
      s4() +
      s4() +
      s4()
    );
  };

  addRow() {
    let objKeys = JSON.parse(this.data.header).map(row => row.key);

    let newData = {};
    for (let k of objKeys) {
      newData[k] = '';
    }

    const newRow = {
      id: this.guid(),
      ...newData,
      isEdit: true,
    };

    console.log("New Row: ", newRow);

    this.dataSource = [newRow, ...this.dataSource];
  }

  updateRow(element: any) {
    element.isEdit = !element.isEdit;

    localStorage.setItem('dataSource', JSON.stringify(this.dataSource));

    console.log("Data Source after update: ", this.dataSource);
  }

  removeRow(id: string) {
    this.dataSource = this.dataSource.filter((u) => u.id !== id);

    localStorage.setItem('dataSource', JSON.stringify(this.dataSource));
  }

  removeSelectedRows() {
    const numSelected = this.selection.selected;

    for (let item of numSelected) {
      this.removeRow(item.id);
    }

    this.selection.clear();
  }

  handleSelectCheckbox(event: any, element: any) {
    element.isSelected = event.checked;
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  toggleAllRows() {
    if (this.isAllSelected()) {
      this.selection.clear();
      return;
    }

    this.selection.select(...this.dataSource);
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: any): string {
    if (!row) {
      return `${this.isAllSelected() ? 'deselect' : 'select'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.index + 1}`;
  }
}
