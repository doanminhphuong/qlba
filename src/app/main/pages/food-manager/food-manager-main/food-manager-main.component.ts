import { SelectionModel } from '@angular/cdk/collections';
import { Component, OnInit, ViewChild } from '@angular/core';
import {
    MatDialog,
    MatPaginator,
    MatTableDataSource,
    PageEvent,
} from '@angular/material';
import { Router } from '@angular/router';
import { DeleteConfirmDialogComponent } from '@app/_shared/dialogs/delete-confirm-dialog/delete-confirm-dialog.component';
import { CategoryService } from '@core/services/category.service';
import { CommonService } from '@core/services/common.service';
import { DataService } from '@core/services/data-service';
import { FoodyService } from '@core/services/foody.service';
import { ToastrService } from '@core/services/toastr.service';
import { forkJoin, Subscription } from 'rxjs';

@Component({
    selector: 'app-food-manager-main',
    templateUrl: './food-manager-main.component.html',
    styleUrls: ['./food-manager-main.component.scss'],
})
export class FoodManagerMainComponent implements OnInit {
    @ViewChild(MatPaginator) paginator!: MatPaginator;

    unitOptions: any[] = [];
    groupOptions: any[] = [];
    typeOptions: any[] = [];

    displayedColumnsOfField: string[] = [
        'select',
        'maMonAn',
        'tenMonAn',
        'maLoaiMonAn',
        'status',
        'actions',
    ];

    dataSource = new MatTableDataSource<any>([]);
    selection = new SelectionModel<any>(true, []);
    currentTable: any[] = [];
    fieldSearch: any[] = [
        {
            type: 'SELECT',
            referenceValue: 'loaiMonAn',
            name: 'Loại món ăn',
            defaultValue: '',
            required: '0',
            css: 'col-12 col-lg-6',
            options: [...this.typeOptions],
            appearance: 'legacy',
        },
        {
            type: 'SELECT',
            referenceValue: 'status',
            name: 'Trạng thái',
            defaultValue: '',
            required: '0',
            css: 'col-12 col-lg-6',
            options: [
                { key: 'ENABLE', name: 'Hoạt động' },
                { key: 'DISABLE', name: 'Tạm ngưng' },
            ],
            appearance: 'legacy',
        },
    ];

    // Pagination
    totalRows = 0;
    pageSize = 10;
    currentPage = 0;
    pageSizeOptions: number[] = [5, 10, 25, 50];
    dataSkipped: number = 0;

    allowExec: boolean = false;
    subscription: Subscription;
    isLoading: boolean = false;
    isFiltering: boolean = false;
    idDauMoiBep: string | number;

    constructor(
        private _router: Router,
        public dialog: MatDialog,
        private _toastrService: ToastrService,
        private _categoryService: CategoryService,
        private _foodyService: FoodyService,
        private _commonService: CommonService,
        private _dataService: DataService,
    ) {
        this._commonService.checkPermission2().subscribe(
            (response) => {
                if (!response.result) return;

                const permissions = JSON.parse(response.result);
                console.log('permissions:: ', permissions);
                this.allowExec = permissions.some((obj) => obj.key === 'EXEC');

                if (!this.allowExec) {
                    this.displayedColumnsOfField.splice(5, 1);
                }
            },
            (error) => {
                this.displayedColumnsOfField.splice(5, 1);
            },
        );

        this.subscription = this._dataService.getData().subscribe((message) => {
            this.isLoading = true;
            this.getInitData(message.id);
        });
    }

    ngOnInit() {
        this.getDataCookId().subscribe((data: { id: number | string }) => {
            console.log('data::', data);
            this.idDauMoiBep = data.id;
            this.getInitData(data.id);
            this.dataSource.paginator = this.paginator;
        });
    }

    getDataCookId() {
        return this._dataService.behaviorSubjectBepID$;
    }

    ngAfterViewInit() {
        this.dataSource.paginator = this.paginator;
    }

    getInitData(id: number | string): void {
        let getCategory = this._categoryService.getAllCategory(
            this.getBodyCategory('loai-mon-an'),
        );

        forkJoin([getCategory]).subscribe((response) => {
            console.log('response::', response);
            this.typeOptions = response[0].result.map((item) => ({
                key: item.number1,
                name: item.name,
            }));

            this.fieldSearch = [
                {
                    type: 'SELECT',
                    referenceValue: 'loaiMonAn',
                    name: 'Loại món ăn',
                    defaultValue: '',
                    required: '0',
                    css: 'col-12 col-lg-6',
                    options: [...this.typeOptions],
                    appearance: 'legacy',
                },
                {
                    type: 'SELECT',
                    referenceValue: 'status',
                    name: 'Trạng thái',
                    defaultValue: '',
                    required: '0',
                    css: 'col-12 col-lg-6',
                    options: [
                        { key: 'ENABLE', name: 'Hoạt động' },
                        { key: 'DISABLE', name: 'Tạm ngưng' },
                    ],
                    appearance: 'legacy',
                },
            ];

            this.getAllMonAn(id);
        });
    }

    getAllMonAn(id: number | string): void {
        const body = {
            maxResultCount: 999999999,
            criterias: [
                {
                    propertyName: 'dauMoiBepId',
                    operation: 0,
                    value: id,
                },
                {
                    propertyName: 'status',
                    operation: 0,
                    value: 'ENABLE',
                },
            ],
        };

        this._foodyService.getAllMonAn(body).subscribe((res) => {
            this.currentTable = res.result.items;
            this.dataSource.data = this.currentTable;
            this.dataSource.paginator = this.paginator;

            setTimeout(() => {
                this.isLoading = false;
            }, 200);
            this.selection.clear();
        });
    }

    pageChanged(event: PageEvent) {
        if (this.isFiltering) return;

        this.currentPage = event.pageIndex;
        this.dataSkipped = event.pageIndex * 10;

        this.getInitData(this._dataService.getDauMoiBepId());
    }

    getBodyCategory(groupCode: string) {
        return {
            maxResultCount: 10,
            skipCount: 0,
            sorting: 'Code',
            criterias: [
                {
                    propertyName: 'GroupCode',
                    operation: 6,
                    value: groupCode,
                },
            ],
        };
    }

    getTypeOfFood(id) {
        let result = '';

        if (this.typeOptions.length > 0) {
            const dataFound = this.typeOptions.find((item) => item.key === id);
            result = dataFound ? dataFound.name : '';
        }

        return result;
    }

    // Handle Input Filter
    handleInputFilter(filterValue) {
        this.dataSource.data = this.currentTable.filter((item) =>
            item.tenMonAn
                .toLowerCase()
                .includes(filterValue.trim().toLowerCase()),
        );

        setTimeout(() => {
            this.paginator.pageIndex = this.currentPage;
            this.paginator.length = this.dataSource.data.length;
        });
    }

    handleSearchAdvanced(dataSearch: any) {
        if (dataSearch.key === 'reset') {
            this.isFiltering = false;
            this.getInitData(this._dataService.getDauMoiBepId());
        } else {
            this.isFiltering = true;
            const body =
                dataSearch.loaiMonAn !== null && dataSearch.loaiMonAn >= 0
                    ? {
                          maxResultCount: 9999,
                          sorting: 'Code',
                          criterias: [
                              {
                                  propertyName: dataSearch.status
                                      ? 'status'
                                      : '',
                                  operation: dataSearch.status
                                      ? 'Equals'
                                      : 'Contains',
                                  value: dataSearch.status
                                      ? dataSearch.status
                                      : '',
                              },
                              {
                                  propertyName: 'dauMoiBepId',
                                  operation: 0,
                                  value: this._dataService.getDauMoiBepId(),
                              },
                          ],
                          loaiMonAn: dataSearch.loaiMonAn,
                      }
                    : {
                          maxResultCount: 999999,
                          sorting: 'Code',
                          criterias: [
                              {
                                  propertyName: dataSearch.status
                                      ? 'status'
                                      : '',
                                  operation: dataSearch.status
                                      ? 'Equals'
                                      : 'Contains',
                                  value: dataSearch.status
                                      ? dataSearch.status
                                      : '',
                              },
                              {
                                  propertyName: 'dauMoiBepId',
                                  operation: 0,
                                  value: this._dataService.getDauMoiBepId(),
                              },
                          ],
                      };

            this._foodyService.getAllMonAn(body).subscribe((res) => {
                this.currentTable = res.result.items;
                this.dataSource.data = this.currentTable;
                this.dataSource.paginator = this.paginator;
                this.selection.clear();
            });
        }
    }

    /** Whether the number of selected elements matches the total number of rows. */
    isAllSelected() {
        const numSelected = this.selection.selected.length;
        const numRows = this.dataSource.data.length;
        return numSelected === numRows;
    }

    /** Selects all rows if they are not all selected; otherwise clear selection. */
    toggleAllRows() {
        if (this.isAllSelected()) {
            this.selection.clear();
            return;
        }

        this.selection.select(...this.dataSource.data);
    }

    /** The label for the checkbox on the passed row */
    checkboxLabel(row?: any): string {
        if (!row) {
            return `${this.isAllSelected() ? 'deselect' : 'select'} all`;
        }
        return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${
            row.index + 1
        }`;
    }

    guid = () => {
        let s4 = () => {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        };
        //return id of format 'aaaaaaaa'-'aaaa'-'aaaa'-'aaaa'-'aaaaaaaaaaaa'
        return (
            s4() +
            s4() +
            '-' +
            s4() +
            '-' +
            s4() +
            '-' +
            s4() +
            '-' +
            s4() +
            s4() +
            s4()
        );
    };

    createAction(): void {
        this._router.navigate(['app/thuc-don/quan-ly-mon-an/create']);
    }

    editAction(element: any): void {
        this._router.navigate([
            `app/thuc-don/quan-ly-mon-an/edit/${element.id}`,
        ]);
    }

    deleteAction(element: any): void {
        const dialogRef = this.dialog.open(DeleteConfirmDialogComponent, {
            width: '500px',
            data: element,
        });

        dialogRef.componentInstance.handleDelete.subscribe((dataDelete) => {
            const isSubmitted = !!dataDelete;

            this._foodyService.deleteMonAn(dataDelete).subscribe(
                (res) => dialogRef.close(isSubmitted),
                (err) => this._toastrService.errorServer(err),
            );
        });

        dialogRef.afterClosed().subscribe((result) => {
            if (!result) return;

            this.getInitData(this._dataService.getDauMoiBepId());
            this._toastrService.success('', 'Xóa thành công');
        });
    }

    deleteMultiAction(): void {
        const dialogRef = this.dialog.open(DeleteConfirmDialogComponent, {
            width: '500px',
        });

        dialogRef.componentInstance.handleDelete.subscribe((dataDelete) => {
            const isSubmitted = !dataDelete;

            this.selection.selected.forEach((item) => {
                const dataDelete = {
                    id: item.id,
                };

                this._foodyService.deleteMonAn(dataDelete).subscribe(
                    (res) => dialogRef.close(isSubmitted),
                    (err) => this._toastrService.errorServer(err),
                );
            });
        });

        dialogRef.afterClosed().subscribe((result) => {
            if (!result) return;

            this.getInitData(this._dataService.getDauMoiBepId());
            this._toastrService.success('', 'Xóa thành công');
        });
    }
}
