import { SelectionModel } from '@angular/cdk/collections';
import { Component, OnInit, ViewChild } from '@angular/core';
import {
    MatDialog,
    MatPaginator,
    MatTableDataSource,
    PageEvent,
} from '@angular/material';
import { Router } from '@angular/router';
import { DeleteConfirmDialogComponent } from '@app/_shared/dialogs/delete-confirm-dialog/delete-confirm-dialog.component';
import { DataService } from '@core/services/data-service';
import { InventoryService } from '@core/services/inventory.service';
import { ToastrService } from '@core/services/toastr.service';
import { Subscription } from 'rxjs';

@Component({
    selector: 'app-inventory-import',
    templateUrl: './inventory-import.component.html',
    styleUrls: ['./inventory-import.component.scss'],
})
export class InventoryImportComponent implements OnInit {
    @ViewChild(MatPaginator) paginator!: MatPaginator;

    currentDate: Date = new Date();

    displayedColumnsOfField: string[] = [
        'ngayNhapKho',
        'maPhieuNhapKho',
        'tongTien',
        'khoNhanId',
        'actions',
    ];

    dataSource = new MatTableDataSource<any>([]);
    selection = new SelectionModel<any>(true, []);
    userList: any[] = [];

    warehouseList: any[] = [];
    totalPrice: number = 0;
    currentTable: any[] = [];
    unitOptions: any[] = [];
    typeOptions: any[] = [];

    fieldSearch: any[] = [
        {
            type: 'DATETIME',
            referenceValue: 'ngayNhapKho',
            name: 'Ngày nhập',
            defaultValue: '',
            required: '0',
            css: 'col-12 col-lg-6',
            appearance: 'legacy',
        },
        {
            type: 'SELECT',
            referenceValue: 'trangThai',
            name: 'Trạng thái',
            defaultValue: '',
            required: '0',
            options: [
                { key: 0, name: 'Lưu nháp' },
                { key: 3, name: 'Nhập kho' },
            ],
            css: 'col-12 col-lg-6',
            appearance: 'legacy',
        },
    ];

    // Pagination
    totalRows = 0;
    pageSize = 10;
    currentPage = 0;
    pageSizeOptions: number[] = [5, 10, 25, 50];
    dataSkipped: number = 0;

    isFiltering: boolean = false;
    subscription: Subscription;
    isLoading: boolean = true;

    constructor(
        private _router: Router,
        public dialog: MatDialog,
        private _toastrService: ToastrService,
        private _inventoryService: InventoryService,
        private _dataService: DataService,
    ) {
        this.subscription = this._dataService.getData().subscribe((message) => {
            this.isLoading = true;
            this.getInitData(message.id);
        });
    }

    ngOnInit() {
        setTimeout(() => {
            this.getInitData(this._dataService.getDauMoiBepId());
        }, 1000);
    }

    ngAfterViewInit() {
        // this.dataSource.paginator = this.paginator;
    }

    getInitData(id: any): void {
        const body = {
            maxResultCount: 9999999,
            // skipCount: this.dataSkipped,
            sorting: 'ngayNhapKho DESC',
            dauMoiBepId: id,
        };

        this._inventoryService.getAllInventoryImport(body).subscribe(
            (res) => {
                this.getAllWareHouse();
                this.dataSource.data = res.result.items;
                this.dataSource.paginator = this.paginator;
                setTimeout(() => {
                    this.isLoading = false;
                }, 200);
            },
            (err) => this._toastrService.errorServer(err),
        );
    }

    getAllWareHouse(): void {
        this._inventoryService
            .getAllWarehouse({
                maxResultCount: 2147483647,
                criterias: [
                    {
                        propertyName: 'dauMoiBepId',
                        operation: 0,
                        value: this._dataService.getDauMoiBepId(),
                    },
                ],
            })
            .subscribe((res) => {
                this.warehouseList = res.result.items;
            });
    }

    getNameOfWarehouse(warehouseId) {
        let result = '';

        if (this.warehouseList.length > 0) {
            const dataFound = this.warehouseList.find(
                (item) => item.id === warehouseId,
            );

            result = dataFound && dataFound.tenKho ? dataFound.tenKho : '';
        }

        return result;
    }

    pageChanged(event: PageEvent) {
        if (this.isFiltering) return;

        this.currentPage = event.pageIndex;
        this.dataSkipped = event.pageIndex * 10;

        this.getInitData(this._dataService.getDauMoiBepId());
    }

    /** Whether the number of selected elements matches the total number of rows. */
    isAllSelected() {
        const numSelected = this.selection.selected.length;
        const numRows = this.dataSource.data.length;
        return numSelected === numRows;
    }

    /** Selects all rows if they are not all selected; otherwise clear selection. */
    toggleAllRows() {
        if (this.isAllSelected()) {
            this.selection.clear();
            return;
        }

        this.selection.select(...this.dataSource.data);
    }

    /** The label for the checkbox on the passed row */
    checkboxLabel(row?: any): string {
        if (!row) {
            return `${this.isAllSelected() ? 'deselect' : 'select'} all`;
        }
        return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${
            row.index + 1
        }`;
    }

    guid = () => {
        let s4 = () => {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        };
        //return id of format 'aaaaaaaa'-'aaaa'-'aaaa'-'aaaa'-'aaaaaaaaaaaa'
        return (
            s4() +
            s4() +
            '-' +
            s4() +
            '-' +
            s4() +
            '-' +
            s4() +
            '-' +
            s4() +
            s4() +
            s4()
        );
    };

    createAction(): void {
        this._router.navigate(['manage/app/category/nhap-kho/create']);
    }

    handleAction(element: any, typeAction): void {
        this._router.navigate([
            `manage/app/category/nhap-kho/edit/${element.id}`,
            { typeAction },
        ]);
    }

    deleteAction(element: any): void {
        const dialogRef = this.dialog.open(DeleteConfirmDialogComponent, {
            width: '500px',
            data: element,
        });

        dialogRef.componentInstance.handleDelete.subscribe((dataDelete) => {
            const isSubmitted = !!dataDelete;

            this._inventoryService.deleteInventoryImport(dataDelete).subscribe(
                (res) => dialogRef.close(isSubmitted),
                (err) => this._toastrService.errorServer(err),
            );
        });

        dialogRef.afterClosed().subscribe((result) => {
            if (!result) return;

            this.getInitData(this._dataService.getDauMoiBepId());
            this._toastrService.success('', 'Xóa thành công');
        });
    }

    // HANDLE SEARCH
    handleInputFilter(filterValue) {
        if (!filterValue) {
            this.isFiltering = false;
            this.getInitData(this._dataService.getDauMoiBepId());
        } else {
            const bodyFilter = {
                maxResultCount: 9999,
                skipCount: 0,
                sorting: 'Id',
                criterias: [
                    {
                        propertyName: 'maPhieuNhapKho',
                        operation: 6,
                        value: filterValue.trim().toLowerCase(),
                    },
                ],
                dauMoiBepId: this._dataService.getDauMoiBepId(),
            };

            this._inventoryService
                .getAllInventoryImport(bodyFilter)
                .subscribe((res) => {
                    this.isFiltering = true;
                    this.dataSource.data = res.result.items;
                });
        }
    }

    handleSearchAdvanced(data: any) {
        if (data.key === 'reset') {
            this.isFiltering = false;
            this.getInitData(this._dataService.getDauMoiBepId());
        } else {
            let bodySubmit = {
                maxResultCount: 9999,
                criterias: [
                    {
                        propertyName: data.ngayNhapKho ? 'ngayNhapKho' : '',
                        operation: data.ngayNhapKho ? 'Equals' : 'Contains',
                        value: data.ngayNhapKho
                            ? new Date(data.ngayNhapKho._d).toISOString()
                            : '',
                    },
                    {
                        propertyName:
                            data.trangThai !== null && data.trangThai !== ''
                                ? 'trangThai'
                                : '',
                        operation:
                            data.trangThai !== null && data.trangThai !== ''
                                ? 'Equals'
                                : 'Contains',
                        value:
                            data.trangThai !== null && data.trangThai !== ''
                                ? data.trangThai
                                : '',
                    },
                ],
                dauMoiBepId: this._dataService.getDauMoiBepId(),
            };

            this._inventoryService
                .getAllInventoryImport(bodySubmit)
                .subscribe((response) => {
                    if (response.success) {
                        this.isFiltering = true;
                        this.dataSource = new MatTableDataSource<any>(
                            response.result.items,
                        );
                        this.dataSource.paginator = this.paginator;
                    }
                });
        }
    }

    addTotalImport() {
        this._router.navigate(['manage/app/category/tong-nhap-kho/create']);
    }
}
