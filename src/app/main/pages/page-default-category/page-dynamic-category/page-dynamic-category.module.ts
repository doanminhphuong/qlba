import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PageDynamicCategoryRoutingModule } from './page-dynamic-category-routing.module';
import { PageBlankComponent } from './page-blank/page-blank.component';

@NgModule({
    declarations: [PageBlankComponent],
    imports: [CommonModule, PageDynamicCategoryRoutingModule],
})
export class PageDynamicCategoryModule {}
