import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuListManagementComponent } from './menu-list-management.component';

describe('MenuListManagementComponent', () => {
  let component: MenuListManagementComponent;
  let fixture: ComponentFixture<MenuListManagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenuListManagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuListManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
